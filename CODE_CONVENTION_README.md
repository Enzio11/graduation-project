* Cấu trúc File
- Các Entity/Interface/Service tương ứng với Sales/Master sẽ chia về 2 folder là Sales và MstSle tương ứng
- Các bảng tương ứng được chia vào các folder với tên trùng với tên bảng trong Database
- Đối với các Dto của Interface thì để trong folder Dto trong folder tương ứng
* Tạo mới Entity
- Khai báo Entity ở trong tmss.Core: 
	+ Nếu là Sales thì thêm vào folder Sales
	+ Nếu là Master thì thêm vào folder MstSle
- Tên Entity trùng với tên bảng trong Database. Ví dụ SalesCustomer.cs
- Khai báo Db context trong tmssDbContext theo cú pháp: public virtual DbSet<Tên Class Entity> Tên Class Entity + "s" { set; get; }
Ví dụ: public virtual DbSet<SalesCustomer> SalesCustomers { set; get; }
* Tạo mới Interface
- Khởi tạo Interface mới ở tmss.Application.Shared:
	+ Nếu là Sales thì thêm vào foler Sales
	+ Nếu là Master thì thêm vào Folder MstSle
- Đặt tên Interface theo cấu trúc "I" + Tên Bảng Database + "AppService". Ví dụ: ISalesCustomerAppService.cs
- Để Interface phải để Public
- Interface phải implement IApplicationService
* Tạo mới các Dto
- Khởi tạo Interface mới ở tmss.Application.Shared:
	+ Nếu là Sales thì để trong Dto của Sales, để trong Folder tương ứng với tên của bảng trong Database
	+ Nếu là Master thì để trong Dto của MstSle, để trong Folder tương ứng với tên của bảng trong Database
- Các Dto dạng danh sách hoặc chi tiết phải có đuôi Dto phía sau. Ví dụ: SalesCustomerListDto.cs
- Các Dto dạng input phải có đuôi input phía sau. Ví dụ: CreateSalesCustomerInput.cs
* Tạo mới Service
- Tạo mới Service mới ở tmss.Application:
	+ Nếu là Sales thì thêm vào folder Sales
	+ Nếu là Master thì thêm vào folder MstSle
- Tên Service đặt theo cú pháp: Tên bảng trong Database + AppService. Ví dụ: SalesCustomerAppService.cs
- Service phải implement tmssAppServiceBase và Interface tương ứng
- Cách khai báo repository trong Service "_" + tên Bảng trong Database + "Repo". Ví dụ: _SalesCustomerRepo
* Tạo mới Function
- Nếu dữ liệu trả về 1 list không cần phân trang thì sử dụng ListResultDto<>. Ví dụ ListResultDto<ListSalesCustomerDto>
- Nếu dữ liệu trả về 1 list cần phân trang thì sử dụng PagedResultDto<>. Ví dụ PagedResultDto<ListSalesCustomerDto>
- Nếu sử dụng hàm Thêm mới/Sửa/Xóa thì sử dụng Task. Ví dụ Task CreateSalesCustomer<CreateSalesCustomerInput>
* Các cách đặt biến
- Đặt biến dễ hiểu
- Tên biến chữ cái đầu không viết hoa. Ví dụ var salesCustomer
