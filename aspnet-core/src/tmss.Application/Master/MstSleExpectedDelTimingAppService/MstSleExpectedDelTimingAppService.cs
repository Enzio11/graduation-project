﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ExpectedDelTiming;
using tmss.MstSle.Dto.Hotness;

namespace tmss.MstSle.MstSleExpectedDelTimingAppService
{
    public class MstSleExpectedDelTimingAppService : tmssAppServiceBase, IMstSleExpectedDelTimingAppService
    {
        private readonly IRepository<MstSleExpectedDelTiming, long> _MstSleExpectedDelTiming;
        private readonly IRepository<MstSleHotness, long> _MstSleHotnessRepository;

        public MstSleExpectedDelTimingAppService(
            IRepository<MstSleExpectedDelTiming, long> MstSleExpectedDelTiming,
            IRepository<MstSleHotness, long> MstSleHotnessRepository
            )
        {
            _MstSleExpectedDelTiming = MstSleExpectedDelTiming;
            _MstSleHotnessRepository = MstSleHotnessRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Search)]
        public async Task<PagedResultDto<GetMstSleExpectedDelTimingForViewDto>> GetAll(GetMstSleExpectedDelTimingInputDto input)
        {
            var query = from DelTiming in _MstSleExpectedDelTiming.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        join hotness in _MstSleHotnessRepository.GetAll()
                                           on DelTiming.HotnessId equals hotness.Id
                                           into resultHotnesss

                        from resultHotness in resultHotnesss.DefaultIfEmpty()
                        orderby DelTiming.Ordering ascending
                        select new GetMstSleExpectedDelTimingForViewDto()
                        {
                            Id = DelTiming.Id,
                            Code = DelTiming.Code,
                            Description = DelTiming.Description,
                            Vi_Description = DelTiming.Vi_Description,
                            Ordering = DelTiming.Ordering,
                            Status = resultHotness.Description,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredExpectedDelTiming = query.PageBy(input);

            return new PagedResultDto<GetMstSleExpectedDelTimingForViewDto>(
                totalCount,
                await pagedAndFilteredExpectedDelTiming.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleExpectedDelTimingDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Create)]
        protected virtual async Task Create(CreateOrEditMstSleExpectedDelTimingDto input)
        {
            var mtSleExpectedDelTimingCount = _MstSleExpectedDelTiming.GetAll().Where(e => e.Code == input.Code).Count();
            if (mtSleExpectedDelTimingCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var MmtSleExpectedDelTiming = ObjectMapper.Map<MstSleExpectedDelTiming>(input);
                await _MstSleExpectedDelTiming.InsertAsync(MmtSleExpectedDelTiming);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleExpectedDelTimingDto input)
        {
            var mtSleExpectedDelTimingCount = _MstSleExpectedDelTiming.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mtSleExpectedDelTimingCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var MmtSleExpectedDelTiming = await _MstSleExpectedDelTiming.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, MmtSleExpectedDelTiming);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Edit)]
        public async Task<GetMstSleExpectedDelTimingForEditOutput> GetMstSleExpectedDelTimingForEdit(EntityDto<long> input)
        {
            var MmtSleExpectedDelTiming = await _MstSleExpectedDelTiming.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleExpectedDelTimingForEditOutput { MstSleExpectedDelTiming = ObjectMapper.Map<CreateOrEditMstSleExpectedDelTimingDto>(MmtSleExpectedDelTiming) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _MstSleExpectedDelTiming.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _MstSleExpectedDelTiming.DeleteAsync(result.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Search)]
        public async Task<List<GetMstSleHotnessForViewDto>> GetAllHotness()
        {
            var query = from hotness in _MstSleHotnessRepository.GetAll()
                        orderby hotness.Ordering ascending
                        select new GetMstSleHotnessForViewDto()
                        {
                            Id = hotness.Id,
                            Code = hotness.Code,
                            Description = hotness.Description,
                            Vi_Description = hotness.Vi_Description,
                            Ordering = hotness.Ordering,
                        };
            return await query.ToListAsync();
        }
    }
}
