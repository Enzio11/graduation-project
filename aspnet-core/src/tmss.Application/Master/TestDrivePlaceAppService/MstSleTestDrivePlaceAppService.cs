﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.IMstSleTestDrivePlace.Dto;

namespace tmss.MstSle.TestDrivePlaceAppService
{
    public class MstSleTestDrivePlaceAppService : tmssAppServiceBase, IMstSleTestDrivePlaceAppService
    {
        private readonly IRepository<MstSleTestDrivePlace, long> _mstSleTestDrivePlaceRepository;

        public MstSleTestDrivePlaceAppService(IRepository<MstSleTestDrivePlace, long> mstSleTestDrivePlaceRepository)
        {
            _mstSleTestDrivePlaceRepository = mstSleTestDrivePlaceRepository; ;
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleTestDrivePlace_Search)]
        public async Task<PagedResultDto<GetMstSleTestDrivePlaceForViewDto>> GetAll(GetAllMstSleTestDrivePlaceInput input)
        {
            var filteredMstSleTestDrivePlace = _mstSleTestDrivePlaceRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(input.DealerIdFilter != null, e => e.DealerId == input.DealerIdFilter);

            var pagedAndFilteredMstSleTestDrivePlace = filteredMstSleTestDrivePlace.PageBy(input);

            var mstSleTestDrivePlace = from o in pagedAndFilteredMstSleTestDrivePlace
                                       orderby o.Id descending
                                       select new GetMstSleTestDrivePlaceForViewDto()
                                       {
                                           Name = o.Name,
                                           Description = o.Description,
                                           DealerId = o.DealerId,
                                           Id = o.Id
                                       };
            var totalCount = await filteredMstSleTestDrivePlace.CountAsync();

            return new PagedResultDto<GetMstSleTestDrivePlaceForViewDto>(
                totalCount,
                await mstSleTestDrivePlace.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleTestDrivePlaceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleTestDrivePlace_Create)]
        protected virtual async Task Create(CreateOrEditMstSleTestDrivePlaceDto input)
        {
            var mstSleTestDrivePlace = ObjectMapper.Map<MstSleTestDrivePlace>(input);

            await _mstSleTestDrivePlaceRepository.InsertAsync(mstSleTestDrivePlace);
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleTestDrivePlace_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleTestDrivePlaceDto input)
        {
            var mstSleTestDrivePlace = await _mstSleTestDrivePlaceRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleTestDrivePlace);
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleTestDrivePlace_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            await _mstSleTestDrivePlaceRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleTestDrivePlace_Edit)]
        public async Task<GetMstSleTestDrivePlaceForEditOutput> GetMstSleTestDrivePlaceForEdit(EntityDto<long> input)
        {
            var mstSleTestDrivePlace = await _mstSleTestDrivePlaceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleTestDrivePlaceForEditOutput { MstSleTestDrivePlace = ObjectMapper.Map<CreateOrEditMstSleTestDrivePlaceDto>(mstSleTestDrivePlace) };

            return output;
        }
    }
}
