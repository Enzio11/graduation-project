﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ReasonOfNC;

namespace tmss.MstSle
{
    public class MstSleReasonOfNCAppService : tmssAppServiceBase
    {
        private readonly IRepository<MstSleReasonOfNC, long> _MstSleReasonOfNCRepo;

        public MstSleReasonOfNCAppService(
            IRepository<MstSleReasonOfNC, long> mstSleReasonOfNCRepo
            )
        {
            _MstSleReasonOfNCRepo = mstSleReasonOfNCRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNC_Search)]
        public async Task<PagedResultDto<GetMstSleReasonOfNCForViewDto>> GetAll(GetMstSleReasonOfNCInputDto input)
        {
            var query = from ReasonOfNC in _MstSleReasonOfNCRepo.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby ReasonOfNC.Ordering ascending
                        select new GetMstSleReasonOfNCForViewDto()
                        {
                            Id = ReasonOfNC.Id,
                            Code = ReasonOfNC.Code,
                            Description = ReasonOfNC.Description,
                            Vi_Description = ReasonOfNC.Vi_Description,
                            Ordering = ReasonOfNC.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredMstSleReasonOfNC = query.PageBy(input);

            return new PagedResultDto<GetMstSleReasonOfNCForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleReasonOfNC.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleReasonOfNCDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNC_Create)]
        protected virtual async Task Create(CreateOrEditMstSleReasonOfNCDto input)
        {
            var mstSleReasonOfNCCount = _MstSleReasonOfNCRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleReasonOfNCCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfNC = ObjectMapper.Map<MstSleReasonOfNC>(input);
                await _MstSleReasonOfNCRepo.InsertAsync(mstSleReasonOfNC);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNC_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleReasonOfNCDto input)
        {
            var mstSleReasonOfNCCount = _MstSleReasonOfNCRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleReasonOfNCCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfNC = await _MstSleReasonOfNCRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleReasonOfNC);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNC_Edit)]
        public async Task<GetMstSleReasonOfNCForEditOutput> GetMstSleReasonOfNCForEdit(EntityDto<long> input)
        {
            var mstSleReasonOfNC = await _MstSleReasonOfNCRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleReasonOfNCForEditOutput { MstSleReasonOfNC = ObjectMapper.Map<CreateOrEditMstSleReasonOfNCDto>(mstSleReasonOfNC) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNC_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _MstSleReasonOfNCRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _MstSleReasonOfNCRepo.DeleteAsync(result.Id);
        }
    }
}

