﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Bank;
using tmss.MstSle.IMstSleGenDealer.Dto;

namespace tmss.MstSle.MstGenDealerAppService
{
    public class MstGenDealerAppService : tmssAppServiceBase, IMstGenDealerAppService
    {
        private readonly IRepository<MstGenDealer, long> _mstGenDealersRepository;
        private readonly IRepository<MstGenProvince, long> _mstGenProvinceRepository;
        private readonly IRepository<MstSleBank, long> _mstSleBankRepository;


        public MstGenDealerAppService(IRepository<MstGenDealer, long> mstGenDealersRepository
            , IRepository<MstGenProvince, long> mstGenProvinceRepository
            , IRepository<MstSleBank, long> mstSleBankRepository)

        {
            _mstGenDealersRepository = mstGenDealersRepository;
            _mstGenProvinceRepository = mstGenProvinceRepository;
            _mstSleBankRepository = mstSleBankRepository;

        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenDealers_Search)]
        public async Task<PagedResultDto<GetMstGenDealersForViewDto>> GetAll(GetAllMstGenDealersInput input)
        {

            var query = from o in _mstGenDealersRepository.GetAll()
                    .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter)
                    || e.AccountNo.Contains(input.Filter)
                    || e.VnName.Contains(input.Filter) || e.EnName.Contains(input.Filter) || e.Address.Contains(input.Filter)
                    || e.Abbreviation.Contains(input.Filter) || e.ContactPerson.Contains(input.Filter) || e.Phone.Contains(input.Filter)
                    || e.Status.Contains(input.Filter) || e.Fax.Contains(input.Filter) || e.Description.Contains(input.Filter)
                    || e.IsSpecial.Contains(input.Filter) || e.TaxCode.Contains(input.Filter) || e.IsSumDealer.Contains(input.Filter)
                    || e.BiServer.Contains(input.Filter) || e.IpAddress.Contains(input.Filter) || e.Islexus.Contains(input.Filter)
                    || e.IsSellLexusPart.Contains(input.Filter) || e.IsDlrSales.Contains(input.Filter) || e.RecievingAddress.Contains(input.Filter)
                    || e.DlrFooter.Contains(input.Filter) || e.IsPrint.Contains(input.Filter) || e.PasswordSearchVin.Contains(input.Filter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.Contains(input.CodeFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.AccountNoFilter), e => e.AccountNo.Contains(input.AccountNoFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName.Contains(input.VnNameFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName.Contains(input.EnNameFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.AddressFilter), e => e.Address.Contains(input.AddressFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.AbbreviationFilter), e => e.Abbreviation.Contains(input.AbbreviationFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.ContactPersonFilter), e => e.ContactPerson.Contains(input.ContactPersonFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.PhoneFilter), e => e.Phone.Contains(input.PhoneFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status.Contains(input.StatusFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.FaxFilter), e => e.Fax.Contains(input.FaxFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.TaxCodeFilter), e => e.TaxCode.Contains(input.TaxCodeFilter))
                    .WhereIf(!string.IsNullOrWhiteSpace(input.IslexusFilter), e => e.Islexus.Contains(input.IslexusFilter))
                    .WhereIf(input.IsDealerFilter != null, e => e.IsDealer == input.IsDealerFilter)
                    .WhereIf(!string.IsNullOrWhiteSpace(input.RecievingAddressFilter), e => e.RecievingAddress.Contains(input.RecievingAddressFilter))

                        select new GetMstGenDealersForViewDto()
                        {
                            Code = o.Code,
                            AccountNo = o.AccountNo,
                            Bank = o.Bank,
                            VnName = o.VnName,
                            EnName = o.EnName,
                            Address = o.Address,
                            Abbreviation = o.Abbreviation,
                            ContactPerson = o.ContactPerson,
                            Phone = o.Phone,
                            Status = o.Status,
                            Fax = o.Fax,
                            Description = o.Description,
                            IsSpecial = o.IsSpecial,
                            Ordering = o.Ordering,
                            TaxCode = o.TaxCode,
                            IsSumDealer = o.IsSumDealer,
                            BiServer = o.BiServer,
                            TfsAmount = o.TfsAmount,
                            PartLeadtime = o.PartLeadtime,
                            IpAddress = o.IpAddress,
                            Islexus = o.Islexus,
                            IsSellLexusPart = o.IsSellLexusPart,
                            IsDlrSales = o.IsDlrSales,
                            RecievingAddress = o.RecievingAddress,
                            DlrFooter = o.DlrFooter,
                            IsPrint = o.IsPrint,
                            PasswordSearchVin = o.PasswordSearchVin,
                            PortRegion = o.PortRegion,
                            DealerParentsId = o.DealerParentsId,
                            DealerTypeId = o.DealerTypeId,
                            DealerGroupId = o.DealerGroupId,
                            ProvinceId = o.ProvinceId,
                            DistrictId = o.DistrictId,
                            Id = o.Id,
                            IsDealer = o.IsDealer,
                            BankAddress = o.BankAddress,
                            ContractRole = o.ContractRole,
                            AuthorizationInfo = o.AuthorizationInfo,
                            ArrivalGroupId = o.GroupArrivalToId,
                            DispatchGroupId = o.GroupDispatchFromId,
                            SwapGroupId = o.GroupSwapId,
                            GroupManagerId = o.GroupManagerId,
                            Lat = o.Lat,
                            Long = o.Long,
                            MapUrl = o.MapUrl,
                            LinkWebsite = o.LinkWebsite,
                            OperatingTime = o.OperatingTime,
                            IsUsingApp = o.IsUsingApp != null,
                            GroupInsuranceId = o.GroupInsuranceId,
                            GroupLoyaltyId = o.GroupLoyaltyId

                        };
            var totalCount = await query.CountAsync();

            var pagedAndFiltered = query.PageBy(input);

            return new PagedResultDto<GetMstGenDealersForViewDto>(
                totalCount,
                await pagedAndFiltered.ToListAsync()
            );
        }

        public async Task<List<GetMstGenDealerForDropdownDto>> GetDealerName()
        {
            var listDealer = from dealer in _mstGenDealersRepository.GetAll()
                             orderby dealer.Id descending
                             select new GetMstGenDealerForDropdownDto
                             {
                                 Address = dealer.Address,
                                 Abbreviation = dealer.Abbreviation,
                                 Id = dealer.Id
                             };
            return await listDealer.ToListAsync();
        }

        public async Task<List<GetBankNameForMstGenDealer>> GetBankNameForDealer()
        {
            var listBank = from bank in _mstSleBankRepository.GetAll()
                           orderby bank.Id descending
                           select new GetBankNameForMstGenDealer
                           {
                               BankName = bank.BankName,
                               Id = bank.Id,
                               Address = bank.Address,
                           };
            return await listBank.ToListAsync();
        }
        public async Task<List<GetMstGenDealerNameDto>> GetListDealerName()
        {
            var result = from dlr in _mstGenDealersRepository.GetAll().Where(e => e.Id >= 1)
                         select new GetMstGenDealerNameDto
                         {
                             Id = dlr.Id,
                             VnName = dlr.VnName,
                             IsChecked = false
                         };
            return await result.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenDealers_Edit)]
        public async Task<GetMstGenDealersForEditOutput> GetMstGenDealersForEdit(EntityDto<int> input)
        {
            var mstGenDealers = await _mstGenDealersRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetMstGenDealersForEditOutput { MstGenDealers = ObjectMapper.Map<CreateOrEditMstGenDealersDto>(mstGenDealers) };
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditMstGenDealersDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenDealers_Create)]
        protected virtual async Task Create(CreateOrEditMstGenDealersDto input)
        {
            var mstGenDealers = ObjectMapper.Map<MstGenDealer>(input);
            await _mstGenDealersRepository.InsertAsync(mstGenDealers);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenDealers_Edit)]
        protected virtual async Task Update(CreateOrEditMstGenDealersDto input)
        {
            var mstGenDealers = await _mstGenDealersRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstGenDealers);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenDealers_Delete)]
        public async Task Delete(EntityDto<int> input)
        {
            await _mstGenDealersRepository.DeleteAsync(input.Id);
        }
        public async Task<List<DealerLocalStorageDto>> GetDealerCodeToCheck(long? inputId)
        {
            var mstGenDealers = from o in _mstGenDealersRepository.GetAll().WhereIf(inputId != null, e => e.Id != inputId)
                                orderby o.Id descending
                                select new DealerLocalStorageDto()
                                {
                                    TenancyName = o.Abbreviation,
                                    TenantCode = o.Code,
                                };
            return await mstGenDealers.ToListAsync();
        }
        public async Task<List<DealerLocalStorageDto>> GetListDealerParent(EntityDto<long?> input)
        {
            if (input.Id == null)
            {
                var query = from parent in _mstGenDealersRepository.GetAll().Where(e => e.DealerParentsId == null && e.Status == "1")
                            select new DealerLocalStorageDto()
                            {
                                TenancyName = parent.Abbreviation,
                                Id = parent.Id
                            };
                return await query.ToListAsync();
            }
            else
            {
                var query = from parent in _mstGenDealersRepository.GetAll().Where(e => e.DealerParentsId == null && e.Id != input.Id && e.Status == "1")
                            select new DealerLocalStorageDto()
                            {
                                TenancyName = parent.Abbreviation,
                                Id = parent.Id
                            };
                return await query.ToListAsync();
            }
        }

        public async Task<List<GetMstSleBankForViewDto>> GetListBankForEdit()
        {
            var query = from bank in _mstSleBankRepository.GetAll()
                        select new GetMstSleBankForViewDto()
                        {
                            BankName = bank.BankName,
                            Address = bank.Address,
                            Id = bank.Id
                        };
            return await query.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_DealerOrganizationChart_Search)]
        public async Task<PagedResultDto<GetMstGenDealersForOrgChartDto>> GetAllDealerForOrgChart(GetAllMstGenDealersForOrgChartInput input)
        {
            var allDealer = _mstGenDealersRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.EnName.Contains(input.Filter)
                             || e.VnName.Contains(input.Filter) || e.Address.Contains(input.Filter)
                            || e.Abbreviation.Contains(input.Filter));
            var query = from o in allDealer
                        join dealer in _mstGenDealersRepository.GetAll()
                        on o.DealerParentsId equals dealer.Id
                        into resultDealer
                        from e in resultDealer.DefaultIfEmpty()
                        orderby e.Ordering descending
                        select new GetMstGenDealersForOrgChartDto()
                        {
                            DealerParents = e.Abbreviation ?? o.Abbreviation,
                            DealerParentsId = o.DealerParentsId,
                            Abbreviation = o.Abbreviation,
                            VnName = o.VnName,
                            EnName = o.EnName,
                            Address = o.Address,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFiltered = query.PageBy(input);

            return new PagedResultDto<GetMstGenDealersForOrgChartDto>(
                totalCount,
                await pagedAndFiltered.ToListAsync()
            );
        }
    }
}
