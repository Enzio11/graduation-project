﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.IMstSleGrade;
using tmss.MstSle.IMstSleGrade.Dto;

namespace tmss.MstSle.MstSleGradeAppService
{
    public class MstSleGradeAppService : tmssAppServiceBase, IMstSleGradesAppService
    {
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;

        public MstSleGradeAppService(IRepository<MstSleGrades, long> mstSleGradesRepository)
        {
            _mstSleGradesRepository = mstSleGradesRepository;
        }

        public async Task<List<ListGradeNameDto>> GetAllGrades()
        {
            var mstSleGrades = from grade in _mstSleGradesRepository.GetAll()
                               orderby grade.Ordering descending
                               select new ListGradeNameDto()
                               {
                                   MarketingCode = grade.MarketingCode,
                                   ProductionCode = grade.ProductionCode,
                                   ModelId = grade.ModelId,
                                   EnName = grade.EnName,
                                   VnName = grade.VnName,
                                   Id = grade.Id,
                                   SeatNo = grade.SeatNo
                               };

            return await mstSleGrades.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleGradesForViewDto>> GetAllGradeNoFilter()
        {
            var mstSleGrades = from o in _mstSleGradesRepository.GetAll()
                               orderby o.Ordering descending
                               select new GetMstSleGradesForViewDto()
                               {
                                   MarketingCode = o.MarketingCode,
                                   ProductionCode = o.ProductionCode,
                                   EnName = o.EnName,
                                   VnName = o.VnName,
                                   Determiner = o.Determiner,
                                   Status = o.Status,
                                   Ordering = o.Ordering,
                                   IsHasAudio = o.IsHasAudio,
                                   CbuCkd = o.CbuCkd,
                                   ShortModel = o.ShortModel,
                                   Wmi = o.Wmi,
                                   Vds = o.Vds,
                                   FromDate = o.FromDate,
                                   ToDate = o.ToDate,
                                   FullModel = o.FullModel,
                                   Barcode = o.Barcode,
                                   IsFirmColor = o.IsFirmColor,
                                   IsHasFloormat = o.IsHasFloormat,
                                   PriceAmount = o.PriceAmount,
                                   IsShowDeliveryPlan = o.IsShowDeliveryPlan,
                                   OrderPrice = o.OrderPrice,
                                   InsurancePayLoad = o.InsurancePayLoad,
                                   Id = o.Id,
                                   SeatNo = o.SeatNo
                               };
            var totalCount = await mstSleGrades.CountAsync();

            return new PagedResultDto<GetMstSleGradesForViewDto>(
                totalCount,
                await mstSleGrades.ToListAsync()
            );
        }

        public async Task<List<ListGradeNameDto>> GetListGrade()
        {
            var mstSleGrades = from grade in _mstSleGradesRepository.GetAll().Where(e => e.Status == "Y")
                               orderby grade.Ordering descending
                               select new ListGradeNameDto()
                               {
                                   MarketingCode = grade.MarketingCode,
                                   ProductionCode = grade.ProductionCode,
                                   ModelId = grade.ModelId,
                                   EnName = grade.EnName,
                                   VnName = grade.VnName,
                                   Id = grade.Id,
                                   SeatNo = grade.SeatNo
                               };

            return await mstSleGrades.OrderBy(x => x.MarketingCode).ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleGradesForViewDto>> GetAllGradeByModels(GetAllGradesByModelId input)
        {
            var filteredMstSleGrades = _mstSleGradesRepository.GetAll().Where(e => e.ModelId == input.ModelId)
                                       .WhereIf(!string.IsNullOrWhiteSpace(input.GradeFilter), e => false || e.MarketingCode.Contains(input.GradeFilter)
                                       || e.EnName.Contains(input.GradeFilter) || e.VnName.Contains(input.GradeFilter));

            var mstSleGrades = from o in filteredMstSleGrades
                               orderby o.Ordering descending
                               select new GetMstSleGradesForViewDto()
                               {
                                   MarketingCode = o.MarketingCode,
                                   ProductionCode = o.ProductionCode,
                                   EnName = o.EnName,
                                   VnName = o.VnName,
                                   Determiner = o.Determiner,
                                   Status = o.Status,
                                   Ordering = o.Ordering,
                                   IsHasAudio = o.IsHasAudio,
                                   CbuCkd = o.CbuCkd,
                                   ShortModel = o.ShortModel,
                                   Wmi = o.Wmi,
                                   Vds = o.Vds,
                                   FromDate = o.FromDate,
                                   ToDate = o.ToDate,
                                   FullModel = o.FullModel,
                                   Barcode = o.Barcode,
                                   IsFirmColor = o.IsFirmColor,
                                   IsHasFloormat = o.IsHasFloormat,
                                   PriceAmount = o.PriceAmount,
                                   IsShowDeliveryPlan = o.IsShowDeliveryPlan,
                                   OrderPrice = o.OrderPrice,
                                   InsurancePayLoad = o.InsurancePayLoad,
                                   Id = o.Id,
                                   GasolineTypeId = o.GasolineTypeId,
                                   SeatNo = o.SeatNo,
                               };
            var pagedAndFilteredMstSleGrades = mstSleGrades.PageBy(input);

            var totalCount = await filteredMstSleGrades.CountAsync();

            return new PagedResultDto<GetMstSleGradesForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleGrades.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Edit)]
        public async Task<GetMstSleGradesForEditOutput> GetMstSleGradesForEdit(EntityDto input)
        {
            var mstSleGrades = await _mstSleGradesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleGradesForEditOutput { MstSleGrades = ObjectMapper.Map<CreateOrEditMstSleGradesDto>(mstSleGrades) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleGradesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Create)]
        protected virtual async Task Create(CreateOrEditMstSleGradesDto input)
        {
            var transportNameCount = _mstSleGradesRepository.GetAll().Where(e => e.MarketingCode == input.MarketingCode).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisMarketingCodeAlreadyExists"));
            }
            else
            {
                var mstSleGrades = ObjectMapper.Map<MstSleGrades>(input);
                await _mstSleGradesRepository.InsertAsync(mstSleGrades);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleGradesDto input)
        {
            var transportNameCount = _mstSleGradesRepository.GetAll().Where(e => e.MarketingCode == input.MarketingCode && e.Id != input.Id).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisMarketingCodeAlreadyExists"));
            }
            else
            {
                var mstSleGrades = await _mstSleGradesRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, mstSleGrades);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleGradesRepository.DeleteAsync(input.Id);
        }

        public async Task<List<ListGradeNameDto>> GetGradeCode(long? inputId)
        {
            var grade = from g in _mstSleGradesRepository.GetAll().WhereIf(inputId != null, e => e.Id != inputId)
                        orderby g.Ordering descending
                        select new ListGradeNameDto
                        {
                            MarketingCode = g.MarketingCode,
                            ProductionCode = g.ProductionCode,
                        };

            return await grade.ToListAsync();
        }

        public async Task<List<ListGradeCodeDto>> GetListGradeCkdVehicle()
        {
            var listGrade = from grade in _mstSleGradesRepository.GetAll().Where(e => e.CbuCkd == "N")
                            select new ListGradeCodeDto
                            {
                                Id = grade.Id,
                                MarketingCode = grade.MarketingCode
                            };
            return await listGrade.ToListAsync();
        }

        public async Task<List<ListGradeCodeDto>> GetListGradeCbuVehicle()
        {
            var listGrade = from grade in _mstSleGradesRepository.GetAll().Where(e => e.CbuCkd == "L" || e.CbuCkd == "Y")
                            select new ListGradeCodeDto
                            {
                                Id = grade.Id,
                                MarketingCode = grade.MarketingCode
                            };
            return await listGrade.ToListAsync();
        }


    }
}
