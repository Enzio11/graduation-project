﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.MakeCompetitor;
using tmss.MstSle.Dto.MakeCompetitor.Dto;
using tmss.Sales;

namespace tmss.MstSle.MstSleMakeCompetitor
{
    public class MstSleMakeCompetitorAppService : tmssAppServiceBase, IMstSleMakeCompetitorAppService
    {
        private readonly IRepository<MstSleMake, long> _mstSleMakeRepository;
        private readonly IRepository<SalesCustomerVehicleInfo, long> _customerVehicleInfoRepo;

        public MstSleMakeCompetitorAppService
            (
            IRepository<MstSleMake, long> mstSleMakeRepository,
            IRepository<SalesCustomerVehicleInfo, long> customerVehicleInfoRepo
            )
        {
            _mstSleMakeRepository = mstSleMakeRepository;
            _customerVehicleInfoRepo = customerVehicleInfoRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Make_Competitor_Search)]
        public async Task<PagedResultDto<MstSleMakeCompetitorDto>> GetAllMstSleMakeCompetitor(GetAllMstSleMakeCompetitorInput input)
        {
            var query = from make in _mstSleMakeRepository.GetAll()
                            .Where(e => input.TextFilter == null || e.Code.Contains(input.TextFilter) || e.Description.Contains(input.TextFilter))
                            //.OrderByDescending(e => e.Ordering)
                        orderby make.Ordering ascending
                        select new MstSleMakeCompetitorDto()
                        {
                            Id = make.Id,
                            Code = make.Code,
                            Description = make.Description,
                            Ordering = make.Ordering
                        };

            var pagedAndFilterMstSleMakeCompetitor = query.PageBy(input);
            var totalCount = await query.CountAsync();
            return new PagedResultDto<MstSleMakeCompetitorDto>(
                totalCount,
                await pagedAndFilterMstSleMakeCompetitor.ToListAsync()
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Make_Competitor_Edit)]
        public async Task<GetMstSleMakeCompetitorForEditOutput> GetMstSleMakeCompetitorForEdit(EntityDto input)
        {
            var query = await (from make in _mstSleMakeRepository.GetAll().Where(e => e.Id == input.Id)
                               select new GetMstSleMakeCompetitorForEditOutput
                               {
                                   Id = make.Id,
                                   Code = make.Code,
                                   Description = make.Description,
                                   Ordering = make.Ordering
                               }).FirstOrDefaultAsync();
            return query;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleMakeCompetitorDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Make_Competitor_Create)]
        protected virtual async Task Create(CreateOrEditMstSleMakeCompetitorDto input)
        {
            var transportNameCount = _mstSleMakeRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleMakeCompetitor = ObjectMapper.Map<MstSleMake>(input);

                await _mstSleMakeRepository.InsertAsync(mstSleMakeCompetitor);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Make_Competitor_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleMakeCompetitorDto input)
        {
            var mstSleMakeCompetitor = await _mstSleMakeRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleMakeCompetitor);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Make_Competitor_Delete)]
        public async Task Delete(EntityDto input)
        {
            if (_customerVehicleInfoRepo.GetAll().Where(e => e.MakeId == input.Id).Count() > 0)
            {
                throw new UserFriendlyException(00, L("ThisRecordIsInUse"));
            }
            else
            {
                await _mstSleMakeRepository.DeleteAsync(input.Id);
            }
        }
    }
}
