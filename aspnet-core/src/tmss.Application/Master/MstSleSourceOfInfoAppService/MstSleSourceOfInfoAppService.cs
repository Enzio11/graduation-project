﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.SourceOfInfo;

namespace tmss.MstSle.MstSleSourceOfInfoAppService
{
    public class MstSleSourceOfInfoOfInfoAppService : tmssAppServiceBase, IMstSleSourceOfInfoAppService
    {
        private readonly IRepository<MstSleSourceOfInfo, long> _MstSleSourceOfInfoRepository;

        public MstSleSourceOfInfoOfInfoAppService(
            IRepository<MstSleSourceOfInfo, long> MstSleSourceOfInfoRepository
            )
        {
            _MstSleSourceOfInfoRepository = MstSleSourceOfInfoRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SourceOfInfo_Search)]
        public ListResultDto<MstSleSourceOfInfoDto> GetListSourceOfInfo(MstSleSourceOfInfoInput input)
        {
            var listSource = _MstSleSourceOfInfoRepository.GetAll().OrderBy(s => s.Ordering).ToList();
            return new ListResultDto<MstSleSourceOfInfoDto>(ObjectMapper.Map<List<MstSleSourceOfInfoDto>>(listSource));
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SourceOfInfo_Search)]
        public async Task<PagedResultDto<GetMstSleSourceOfInfoForViewDto>> GetAll(GetMstSleSourceOfInfoInputDto input)
        {
            var query = from SourceOfInfo in _MstSleSourceOfInfoRepository.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby SourceOfInfo.Ordering ascending
                        select new GetMstSleSourceOfInfoForViewDto()
                        {
                            Id = SourceOfInfo.Id,
                            Code = SourceOfInfo.Code,
                            Description = SourceOfInfo.Description,
                            Vi_Description = SourceOfInfo.Vi_Description,
                            Ordering = SourceOfInfo.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredSourceOfInfo = query.PageBy(input);

            return new PagedResultDto<GetMstSleSourceOfInfoForViewDto>(
                totalCount,
                await pagedAndFilteredSourceOfInfo.ToListAsync()
            );
        }
        public async Task CreateOrEdit(CreateOrEditMstSleSourceOfInfoDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SourceOfInfo_Create)]
        protected virtual async Task Create(CreateOrEditMstSleSourceOfInfoDto input)
        {
            var mstSleSourceOfInfoCount = _MstSleSourceOfInfoRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleSourceOfInfoCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var MstSleSourceOfInfo = ObjectMapper.Map<MstSleSourceOfInfo>(input);
                await _MstSleSourceOfInfoRepository.InsertAsync(MstSleSourceOfInfo);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SourceOfInfo_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleSourceOfInfoDto input)
        {
            var mstSleSourceOfInfoCount = _MstSleSourceOfInfoRepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleSourceOfInfoCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleSourceOfInfo = await _MstSleSourceOfInfoRepository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleSourceOfInfo);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SourceOfInfo_Edit)]
        public async Task<GetMstSleSourceOfInfoForEditOutput> GetMstSleSourceOfInfoForEdit(EntityDto<long> input)
        {
            var mstSleSourceOfInfo = await _MstSleSourceOfInfoRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleSourceOfInfoForEditOutput { MstSleSourceOfInfo = ObjectMapper.Map<CreateOrEditMstSleSourceOfInfoDto>(mstSleSourceOfInfo) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SourceOfInfo_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _MstSleSourceOfInfoRepository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _MstSleSourceOfInfoRepository.DeleteAsync(result.Id);
        }
    }
}
