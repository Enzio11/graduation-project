﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.BusinessType;

namespace tmss.MstSle
{
    public class MstSleBusinessTypeAppService : tmssAppServiceBase, IMstSleBusinessTypeAppService
    {
        private readonly IRepository<MstSleBusinessType, long> _BusinessTypeRepo;

        public MstSleBusinessTypeAppService(
            IRepository<MstSleBusinessType, long> BusinessTypeRepo
            )

        {
            _BusinessTypeRepo = BusinessTypeRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_BusinessType_Search)]
        public ListResultDto<MstSleBusinessTypeDto> GetListBusinessType()
        {
            var listPlace = _BusinessTypeRepo.GetAll().ToList();
            var res = new ListResultDto<MstSleBusinessTypeDto>(ObjectMapper.Map<List<MstSleBusinessTypeDto>>(listPlace));
            return res;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_BusinessType_Search)]
        public async Task<PagedResultDto<GetMstSleBusinessTypeForViewDto>> GetAll(GetMstSleBusinessTypeInputDto input)
        {
            var query = from BusinessType in _BusinessTypeRepo.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby BusinessType.Ordering ascending
                        select new GetMstSleBusinessTypeForViewDto()
                        {
                            Id = BusinessType.Id,
                            Code = BusinessType.Code,
                            Description = BusinessType.Description,
                            Vi_Description = BusinessType.Vi_Description,
                            Ordering = BusinessType.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredMstSleBusinessType = query.PageBy(input);

            return new PagedResultDto<GetMstSleBusinessTypeForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleBusinessType.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleBusinessTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_BusinessType_Create)]
        protected virtual async Task Create(CreateOrEditMstSleBusinessTypeDto input)
        {
            var mstSleBusinessTypeCount = _BusinessTypeRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleBusinessTypeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleBusinessType = ObjectMapper.Map<MstSleBusinessType>(input);
                await _BusinessTypeRepo.InsertAsync(mstSleBusinessType);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_BusinessType_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleBusinessTypeDto input)
        {
            var mstSleBusinessTypeCount = _BusinessTypeRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleBusinessTypeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleBusinessType = await _BusinessTypeRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleBusinessType);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_BusinessType_Edit)]
        public async Task<GetMstSleBusinessTypeForEditOutput> GetMstSleBusinessTypeForEdit(EntityDto<long> input)
        {
            var mstSleBusinessType = await _BusinessTypeRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleBusinessTypeForEditOutput { MstSleBusinessType = ObjectMapper.Map<CreateOrEditMstSleBusinessTypeDto>(mstSleBusinessType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_BusinessType_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _BusinessTypeRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _BusinessTypeRepo.DeleteAsync(result.Id);
        }
    }
}
