﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ReasonOfLostType;

namespace tmss.MstSle.MstSleReasonOfLostTypeAppService
{
    public class MstSleReasonOfLostTypeAppService : tmssAppServiceBase, IMstSleReasonOfLostTypeAppService
    {
        private readonly IRepository<MstSleReasonOfLostType, long> _mstSleReasonOfLostTypeRepo;
        public MstSleReasonOfLostTypeAppService(
            IRepository<MstSleReasonOfLostType, long> mstSleReasonOfLostTypeRepo
            )
        {
            _mstSleReasonOfLostTypeRepo = mstSleReasonOfLostTypeRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Search)]
        public async Task<PagedResultDto<GetMstSleReasonOfLostTypeForViewDto>> GetAll(GetMstSleReasonOfLostTypeInputDto input)
        {
            var query = from company in _mstSleReasonOfLostTypeRepo.GetAll()
                             .OrderBy(e => e.Ordering)
                        select new GetMstSleReasonOfLostTypeForViewDto()
                        {
                            Id = company.Id,
                            Code = company.Code,
                            Description = company.Description,
                            Vi_Description = company.Vi_Description,
                            Ordering = company.Ordering,
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredMstReasonOfLostType = query.PageBy(input);

            return new PagedResultDto<GetMstSleReasonOfLostTypeForViewDto>(
                totalCount,
                await pagedAndFilteredMstReasonOfLostType.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleReasonOfLostTypeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Create)]
        protected virtual async Task Create(CreateOrEditMstSleReasonOfLostTypeDto input)
        {
            var mstSleReasonOfLostTypeCount = _mstSleReasonOfLostTypeRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleReasonOfLostTypeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfLostType = ObjectMapper.Map<MstSleReasonOfLostType>(input);
                await _mstSleReasonOfLostTypeRepo.InsertAsync(mstSleReasonOfLostType);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleReasonOfLostTypeDto input)
        {
            var mstSleReasonOfLostTypeCount = _mstSleReasonOfLostTypeRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleReasonOfLostTypeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfLostType = await _mstSleReasonOfLostTypeRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleReasonOfLostType);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Edit)]
        public async Task<GetMstSleReasonOfLostTypeForEditOutput> GetMstSleReasonOfLostTypeForEdit(EntityDto<long> input)
        {
            var mstSleReasonOfLostType = await _mstSleReasonOfLostTypeRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleReasonOfLostTypeForEditOutput { MstSleReasonOfLostTypeDto = ObjectMapper.Map<CreateOrEditMstSleReasonOfLostTypeDto>(mstSleReasonOfLostType) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleReasonOfLostTypeRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleReasonOfLostTypeRepo.DeleteAsync(result.Id);
        }
    }
}
