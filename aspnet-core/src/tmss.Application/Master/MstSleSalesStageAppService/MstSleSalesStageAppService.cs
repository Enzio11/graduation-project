﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.SalesStage;

namespace tmss.MstSle.MstSleSalesStageAppService
{
    public class MstSleSalesStageAppService : tmssAppServiceBase, IMstSleSalesStageAppService
    {
        private readonly IRepository<MstSleSalesStage, long> _mstSleSalesStageRepo;
        public MstSleSalesStageAppService(
            IRepository<MstSleSalesStage, long> mstSleSalesStageRepo
            )
        {
            _mstSleSalesStageRepo = mstSleSalesStageRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SalesStage_Search)]
        public async Task<PagedResultDto<GetMstSleSalesStageForViewDto>> GetAll(GetMstSleSalesStageInputDto input)
        {
            var query = from company in _mstSleSalesStageRepo.GetAll()
                           .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                           .OrderBy(e => e.Ordering)
                        select new GetMstSleSalesStageForViewDto()
                        {
                            Id = company.Id,
                            Code = company.Code,
                            Description = company.Description,
                            Vi_Description = company.Vi_Description,
                            Ordering = company.Ordering,
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredMstSalesStage = query.PageBy(input);

            return new PagedResultDto<GetMstSleSalesStageForViewDto>(
                totalCount,
                await pagedAndFilteredMstSalesStage.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleSalesStageDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SalesStage_Create)]
        protected virtual async Task Create(CreateOrEditMstSleSalesStageDto input)
        {
            var mstSleSalesStageCount = _mstSleSalesStageRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleSalesStageCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleSalesStage = ObjectMapper.Map<MstSleSalesStage>(input);
                await _mstSleSalesStageRepo.InsertAsync(mstSleSalesStage);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SalesStage_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleSalesStageDto input)
        {
            var mstSleSalesStageCount = _mstSleSalesStageRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleSalesStageCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleSalesStage = await _mstSleSalesStageRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleSalesStage);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SalesStage_Edit)]
        public async Task<GetMstSleSalesStageForEditOutput> GetMstSleSalesStageForEdit(EntityDto<long> input)
        {
            var mstSleSalesStage = await _mstSleSalesStageRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleSalesStageForEditOutput { mstSleSalesStageDto = ObjectMapper.Map<CreateOrEditMstSleSalesStageDto>(mstSleSalesStage) };

            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SalesStage_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleSalesStageRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleSalesStageRepo.DeleteAsync(result.Id);
        }

    }
}
