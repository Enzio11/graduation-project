﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Purpose;

namespace tmss.MstSle.MstSlePurposeAppService
{
    public class MstSlePurposeAppService : tmssAppServiceBase, IMstSlePurposeAppService
    {
        private readonly IRepository<MstSlePurpose, long> _mstSlePurposeRepo;

        public MstSlePurposeAppService(
            IRepository<MstSlePurpose, long> mstSlePurposeRepo
            )
        {
            _mstSlePurposeRepo = mstSlePurposeRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Purpose_Search)]
        public async Task<PagedResultDto<GetMstSlePurposeForViewDto>> GetAll(GetMstSlePurposeInputDto input)
        {
            var query = from company in _mstSlePurposeRepo.GetAll()
                             .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                             .OrderBy(e => e.Ordering)
                        select new GetMstSlePurposeForViewDto()
                        {
                            Id = company.Id,
                            Code = company.Code,
                            Description = company.Description,
                            Vi_Description = company.Vi_Description,
                            Ordering = company.Ordering,
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredMstPurpose = query.PageBy(input);

            return new PagedResultDto<GetMstSlePurposeForViewDto>(
                totalCount,
                await pagedAndFilteredMstPurpose.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSlePurposeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Purpose_Create)]
        protected virtual async Task Create(CreateOrEditMstSlePurposeDto input)
        {
            var mstSlePurposeCount = _mstSlePurposeRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSlePurposeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSlePurpose = ObjectMapper.Map<MstSlePurpose>(input);
                await _mstSlePurposeRepo.InsertAsync(mstSlePurpose);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Purpose_Edit)]
        protected virtual async Task Update(CreateOrEditMstSlePurposeDto input)
        {
            var mstSlePurposeCount = _mstSlePurposeRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSlePurposeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSlePurpose = await _mstSlePurposeRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSlePurpose);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Purpose_Edit)]
        public async Task<GetMstSlePurposeForEditOutput> GetMstSlePurposeForEdit(EntityDto<long> input)
        {
            var mstSlePurpose = await _mstSlePurposeRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSlePurposeForEditOutput { mstSlePurposeDto = ObjectMapper.Map<CreateOrEditMstSlePurposeDto>(mstSlePurpose) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Purpose_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSlePurposeRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSlePurposeRepo.DeleteAsync(result.Id);
        }
    }
}
