﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.District;
using tmss.MstSle.IMstSleDistrict.Dto;

namespace tmss.MstSle
{
    public class MstSleDistrictAppService : tmssAppServiceBase, IMstSleDistrictAppService
    {
        private readonly IRepository<MstSleDistrict, long> _mstSleDistrictsRepository;
        public MstSleDistrictAppService(
            IRepository<MstSleDistrict, long> mstSleDistrictsRepository)
        {
            _mstSleDistrictsRepository = mstSleDistrictsRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Search)]
        public ListResultDto<MstSleDistrictListDto> GetListDistrict(GetMstSleDistrictInput input)
        {
            var listProvince = _mstSleDistrictsRepository
            .GetAll()
            .WhereIf(
                !input.Filter.IsNullOrEmpty(),
                p => p.Code.Contains(input.Filter) ||
                     p.Description.Contains(input.Filter)
            )
            .ToList();

            return new ListResultDto<MstSleDistrictListDto>(ObjectMapper.Map<List<MstSleDistrictListDto>>(listProvince));
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Search)]
        public async Task<PagedResultDto<GetMstSleDistrictsForViewDto>> GetAll(GetAllMstSleDistrictsInput input)
        {
            var filteredMstSleDistricts = _mstSleDistrictsRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Status.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.Contains(input.CodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status.Contains(input.StatusFilter))
                        .WhereIf(input.OrderingFilter != null, e => e.Ordering >= input.OrderingFilter)
                        .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleBanksDistricts = filteredMstSleDistricts.PageBy(input);

            var mstSleDistricts = from o in pagedAndFilteredMstSleBanksDistricts
                                  select new GetMstSleDistrictsForViewDto()
                                  {
                                      Code = o.Code,
                                      Name = o.Name,
                                      Description = o.Description,
                                      ProvinceId = o.ProvinceId,
                                      DealerId = o.DealerId,
                                      Status = o.Status,
                                      Ordering = o.Ordering,
                                      Id = o.Id
                                  };
            var totalCount = await filteredMstSleDistricts.CountAsync();

            return new PagedResultDto<GetMstSleDistrictsForViewDto>(
                totalCount,
                await mstSleDistricts.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Search)]
        public async Task<PagedResultDto<GetMstSleDistrictsForViewDto>> GetDistrictsByProvinceId(GetFilterDistricts input)
        {
            var filteredMstSleDistricts = _mstSleDistrictsRepository.GetAll().Where(r => r.ProvinceId == input.Id)
                .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status.Contains(input.StatusFilter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleDistricts = filteredMstSleDistricts.PageBy(input);
            var mstSleDistricts = from o in pagedAndFilteredMstSleDistricts
                                  select new GetMstSleDistrictsForViewDto()
                                  {
                                      Id = o.Id,
                                      Code = o.Code,
                                      Ordering = o.Ordering,
                                      Name = o.Name,
                                      Status = o.Status,
                                      Description = o.Description
                                  };

            var totalCount = await filteredMstSleDistricts.CountAsync();

            return new PagedResultDto<GetMstSleDistrictsForViewDto>(
                totalCount,
                await mstSleDistricts.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleDistrictsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Create)]
        protected virtual async Task Create(CreateOrEditMstSleDistrictsDto input)
        {
            var transportNameCount = _mstSleDistrictsRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleDistricts = ObjectMapper.Map<MstSleDistrict>(input);

                await _mstSleDistrictsRepository.InsertAsync(mstSleDistricts);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleDistrictsDto input)
        {
            var transportNameCount = _mstSleDistrictsRepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleDistricts = await _mstSleDistrictsRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, mstSleDistricts);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleDistrictsRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Edit)]
        public async Task<GetMstSleDistrictsForEditOutput> GetMstSleDistrictsForEdit(EntityDto input)
        {
            var mstSleDistricts = await _mstSleDistrictsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleDistrictsForEditOutput { MstSleDistricts = ObjectMapper.Map<CreateOrEditMstSleDistrictsDto>(mstSleDistricts) };

            return output;
        }

        /*
            - Kiểm tra Code nhập vào có trùng với Code có trong DB không.
            - Nếu trong trường hợp Edit thì cần kiểm tra với danh sách Code trừ Code đang được Edit, được phân biệt bởi inputId truyền xuống
         */
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Search)]
        public async Task<List<GetCodeDistrictDto>> GetCodeDistrictToCheck(long? inputId, long provinceId)
        {
            var listCode = from district in _mstSleDistrictsRepository.GetAll().Where(e => e.ProvinceId == provinceId).WhereIf(inputId != null, e => e.Id != inputId)
                           orderby district.Ordering descending
                           select new GetCodeDistrictDto()
                           {
                               Code = district.Code
                           };
            return await listCode.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_District_Search)]
        public async Task<List<GetCodeDistrictDto>> GetDistrictNameByProvinceId(long? Id)
        {
            var district = from d in _mstSleDistrictsRepository.GetAll().Where(e => e.ProvinceId == Id)
                           select new GetCodeDistrictDto()
                           {
                               Name = d.Name,
                               Id = d.Id
                           };
            return await district.ToListAsync();
        }
    }
}
