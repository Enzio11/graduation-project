﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.FAR;

namespace tmss.MstSle.MstSleFarAppService
{
    public class MstSleFarAppService : tmssAppServiceBase, IMstSleFarAppService
    {
        private readonly IRepository<MstSleFAR, long> _mstSleFarRepo;
        public MstSleFarAppService(
            IRepository<MstSleFAR, long> mstSleFarRepo
            )
        {
            _mstSleFarRepo = mstSleFarRepo;

        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_FAR_Search)]
        public async Task<PagedResultDto<GetMstSleFarForViewDto>> GetAll(GetMstSleFarInputDto input)
        {
            var query = from far in _mstSleFarRepo.GetAll()
                             .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                             .OrderBy(e => e.Ordering)
                        select new GetMstSleFarForViewDto()
                        {
                            Id = far.Id,
                            Code = far.Code,
                            Description = far.Description,
                            Vi_Description = far.Vi_Description,
                            Ordering = far.Ordering,
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredMstFar = query.PageBy(input);

            return new PagedResultDto<GetMstSleFarForViewDto>(
                totalCount,
                await pagedAndFilteredMstFar.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleFarDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_FAR_Create)]
        protected virtual async Task Create(CreateOrEditMstSleFarDto input)
        {
            var mstSleFarCount = _mstSleFarRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleFarCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleFar = ObjectMapper.Map<MstSleFAR>(input);
                await _mstSleFarRepo.InsertAsync(mstSleFar);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_FAR_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleFarDto input)
        {
            var mstSleFarCount = _mstSleFarRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleFarCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleFar = await _mstSleFarRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleFar);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_FAR_Edit)]
        public async Task<GetMstSleFarForEditOutput> GetMstSleFarForEdit(EntityDto<long> input)
        {
            var mstSleFar = await _mstSleFarRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleFarForEditOutput { mstSleFar = ObjectMapper.Map<CreateOrEditMstSleFarDto>(mstSleFar) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_FAR_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleFarRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleFarRepo.DeleteAsync(result.Id);
        }
    }
}
