﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ReasonOfChanges;


namespace tmss.MstSle.MstSleReasonOfChangesAppService
{
    public class MstSleReasonOfChangesAppService : tmssAppServiceBase, IMstSleReasonOfChangesAppService
    {
        private readonly IRepository<MstSleReasonOfChange, long> _mstSleReasonOfChangeRepo;

        public MstSleReasonOfChangesAppService(
            IRepository<MstSleReasonOfChange, long> mstSleReasonOfChangeRepo
            )
        {
            _mstSleReasonOfChangeRepo = mstSleReasonOfChangeRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfChange_Search)]
        public async Task<PagedResultDto<GetMstSleReasonOfChangesForViewDto>> GetAll(GetMstSleReasonOfChangesInputDto input)
        {
            var query = from reasonOfChanges in _mstSleReasonOfChangeRepo.GetAll()
                             .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                             .OrderBy(e => e.Ordering)
                        select new GetMstSleReasonOfChangesForViewDto()
                        {
                            Id = reasonOfChanges.Id,
                            Code = reasonOfChanges.Code,
                            Description = reasonOfChanges.Description,
                            Vi_Description = reasonOfChanges.Vi_Description,
                            Ordering = reasonOfChanges.Ordering,
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredMstReasonOfChanges = query.PageBy(input);

            return new PagedResultDto<GetMstSleReasonOfChangesForViewDto>(
                totalCount,
                await pagedAndFilteredMstReasonOfChanges.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleReasonOfChangesDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfChange_Create)]
        protected virtual async Task Create(CreateOrEditMstSleReasonOfChangesDto input)
        {
            var mstSleReasonOfChangesCount = _mstSleReasonOfChangeRepo.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleReasonOfChangesCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfChanges = ObjectMapper.Map<MstSleReasonOfChange>(input);
                await _mstSleReasonOfChangeRepo.InsertAsync(mstSleReasonOfChanges);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfChange_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleReasonOfChangesDto input)
        {
            var mstSleReasonOfChangesCount = _mstSleReasonOfChangeRepo.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleReasonOfChangesCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfChanges = await _mstSleReasonOfChangeRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleReasonOfChanges);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfChange_Edit)]
        public async Task<GetMstSleReasonOfChangesForEditOutput> GetMstSleReasonOfChangesForEdit(EntityDto<long> input)
        {
            var mstSleReasonOfChanges = await _mstSleReasonOfChangeRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleReasonOfChangesForEditOutput { MstSleReasonOfChangesDto = ObjectMapper.Map<CreateOrEditMstSleReasonOfChangesDto>(mstSleReasonOfChanges) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfChange_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleReasonOfChangeRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleReasonOfChangeRepo.DeleteAsync(result.Id);
        }
    }
}
