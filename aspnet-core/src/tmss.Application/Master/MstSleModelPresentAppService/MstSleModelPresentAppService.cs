﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Master;
using tmss.MstSle.IMstSleModelPresent;
using tmss.MstSle.IMstSleModelPresent.Dto;

namespace tmss.MstSle.MstSleModelPresentAppService
{
    public class MstSleModelPresentAppService : tmssAppServiceBase, IMstSleModelPresentAppService
    {
        private readonly IRepository<MstSleModel, long> _mstSleModelRepository;
        private readonly IRepository<MstSleMake, long> _mstSleMakeRepository;
        public MstSleModelPresentAppService(
            IRepository<MstSleModel, long> mstSleModelRepository,
            IRepository<MstSleMake, long> mstSleMakeRepository
            )
        {
            _mstSleModelRepository = mstSleModelRepository;
            _mstSleMakeRepository = mstSleMakeRepository;
        }

        public async Task<PagedResultDto<MstSleModelPresentDto>> GetAllMstSleModelPresent(GetMstSleModelPresentInput input)
        {
            var query = from model in _mstSleModelRepository.GetAll()
                        .Where(e => input.FilterText == null || e.MarketingCode.Contains(input.FilterText) || e.ProductionCode.Contains(input.FilterText) || e.EnName.Contains(input.FilterText) || e.VnName.Contains(input.FilterText) || e.Description.Contains(input.FilterText) || e.Abbreviation.Contains(input.FilterText))
                        .OrderBy(e => e.Ordering)
                        join make in _mstSleMakeRepository.GetAll()
                        on model.MakeId equals make.Id
                        select new MstSleModelPresentDto()
                        {
                            Id = model.Id,
                            MarketingCode = model.MarketingCode,
                            ProductionCode = model.ProductionCode,
                            EnName = model.EnName,
                            VnName = model.VnName,
                            Description = model.Description,
                            Status = model.Status,
                            Ordering = model.Ordering,
                            OrderingRpt = model.OrderingRpt,
                            MakeId = make.Id,
                            Make = make.Code,
                            Abbreviation = model.Abbreviation
                        };
            var totalCount = await query.CountAsync();
            var pageMstSleModelPresent = query.PageBy(input);
            return new PagedResultDto<MstSleModelPresentDto>(
                totalCount,
                await pageMstSleModelPresent.ToListAsync()
                );
        }
        public async Task CreateOrEdit(CreateOrEditMstSleModelPresentDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        protected virtual async Task Create(CreateOrEditMstSleModelPresentDto input)
        {
            var mstSleModelPresentCount = _mstSleModelRepository.GetAll().Where(e => e.ProductionCode == input.ProductionCode).Count();
            if (mstSleModelPresentCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleModelPresent = ObjectMapper.Map<MstSleModel>(input);
                await _mstSleModelRepository.InsertAsync(mstSleModelPresent);
            }
        }
        protected virtual async Task Update(CreateOrEditMstSleModelPresentDto input)
        {
            var mstSleModelPresent = await _mstSleModelRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleModelPresent);
        }
        public async Task<GetMstSleModelPresentForEditOutput> GetMstSleModelPresentForEdit(EntityDto<long> input)
        {
            var mstSleModelPresent = await _mstSleModelRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleModelPresentForEditOutput { mstSleModelPresent = ObjectMapper.Map<CreateOrEditMstSleModelPresentDto>(mstSleModelPresent) };

            return output;
        }
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleModelRepository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleModelRepository.DeleteAsync(result.Id);
        }
        public async Task<List<MstSleModelPresentDto>> GetListMake()
        {
            var listMake = from make in _mstSleMakeRepository.GetAll()
                           select new MstSleModelPresentDto()
                           {
                               MakeId = make.Id,
                               Make = make.Code
                           };
            return await listMake.ToListAsync();
        }
    }
}
