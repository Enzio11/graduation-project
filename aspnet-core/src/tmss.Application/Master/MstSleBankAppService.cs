﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Bank;
using tmss.MstSle.Dto.LookUp;

namespace tmss.MstSle
{
    public class MstSleBankAppService : tmssAppServiceBase, IMstSleBankAppService
    {
        private readonly IRepository<MstSleBank, long> _mstSleBankRepository;
        private readonly IRepository<MstSleLookUp, int> _mstSleLookupsRepository;

        public MstSleBankAppService(
            IRepository<MstSleBank, long> mstSleBankRepository,
            IRepository<MstSleLookUp, int> mstSleLookupsRepository
            )

        {
            _mstSleBankRepository = mstSleBankRepository;
            _mstSleLookupsRepository = mstSleLookupsRepository;
        }

        public ListResultDto<MstSleBankDto> GetListBank()
        {
            var listPlace = _mstSleBankRepository.GetAll().ToList();
            var res = new ListResultDto<MstSleBankDto>(ObjectMapper.Map<List<MstSleBankDto>>(listPlace));
            return res;
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Bank_Search)]
        public async Task<PagedResultDto<GetMstSleBankForViewDto>> GetAll(GetMstSleBankInputDto input)
        {
            var query = from Bank in _mstSleBankRepository.GetAll()
                            .Where(e => input.filterText == null || e.BankCode.Contains(input.filterText) || e.BankName.Contains(input.filterText) || e.Address.Contains(input.filterText))
                        orderby Bank.Ordering ascending
                        select new GetMstSleBankForViewDto()
                        {
                            Id = Bank.Id,
                            BankCode = Bank.BankCode,
                            BankName = Bank.BankName,
                            BankTypeId = Bank.BankTypeId,
                            Ordering = Bank.Ordering,
                            Address = Bank.Address,
                            Status = Bank.Status,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredMstSleBank = query.PageBy(input);

            return new PagedResultDto<GetMstSleBankForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleBank.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleBankDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        //[AbpAuthorize(AppPermissions.Pages_Master_Sales_Bank_Create)]
        protected virtual async Task Create(CreateOrEditMstSleBankDto input)
        {
            var mstSleBankCount = _mstSleBankRepository.GetAll().Where(e => e.BankCode == input.BankCode).Count();
            if (mstSleBankCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleBank = ObjectMapper.Map<MstSleBank>(input);
                await _mstSleBankRepository.InsertAsync(mstSleBank);
            }
        }
        //[AbpAuthorize(AppPermissions.Pages_Master_Sales_Bank_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleBankDto input)
        {
            var mstSleBankCount = _mstSleBankRepository.GetAll().Where(e => e.BankCode == input.BankCode && e.Id != input.Id).Count();
            if (mstSleBankCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleBank = await _mstSleBankRepository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleBank);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Bank_Edit)]
        public async Task<GetMstSleBankForEditOutput> GetMstSleBankForEdit(EntityDto<long> input)
        {
            var mstSleBank = await _mstSleBankRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleBankForEditOutput { MstSleBank = ObjectMapper.Map<CreateOrEditMstSleBankDto>(mstSleBank) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Bank_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleBankRepository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleBankRepository.DeleteAsync(result.Id);
        }

        public async Task<List<GetLookUpValueDto>> GetLookUpValue()
        {
            var result = from lookup in _mstSleLookupsRepository.GetAll().Where(e => e.Code == "bank_type")
                         orderby lookup.Ordering descending
                         select new GetLookUpValueDto()
                         {
                             Name = lookup.Name,
                             Id = lookup.Id
                         };
            return await result.ToListAsync();
        }
        public async Task<PagedResultDto<GetMstSleBankForViewDto>> GetAllMaster(GetMstSleBankInputDto input)
        {
            var query = from Bank in _mstSleBankRepository.GetAll()
                        .Where(e => e.IsFinance == 3)
                            .Where(e => input.filterText == null || e.BankCode.Contains(input.filterText) || e.BankName.Contains(input.filterText) || e.Address.Contains(input.filterText))
                        orderby Bank.Ordering ascending
                        select new GetMstSleBankForViewDto
                        {
                            Id = Bank.Id,
                            BankCode = Bank.BankCode,
                            BankName = Bank.BankName,
                            BankTypeId = Bank.BankTypeId,
                            Ordering = Bank.Ordering,
                            Address = Bank.Address,
                            Status = Bank.Status,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredMstSleBank = query.PageBy(input);

            return new PagedResultDto<GetMstSleBankForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleBank.ToListAsync()
            );
        }



        [HttpGet]
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_CustomApi)]
        public async Task<PagedResultDto<GetMstSleBankForViewDto>> CusAppGetAll(GetMstSleBankInputDto input)
        {
            var query = from Bank in _mstSleBankRepository.GetAll()
                            .Where(e => input.filterText == null || e.BankCode.Contains(input.filterText) || e.BankName.Contains(input.filterText) || e.Address.Contains(input.filterText))
                        orderby Bank.Ordering ascending
                        select new GetMstSleBankForViewDto()
                        {
                            Id = Bank.Id,
                            BankCode = Bank.BankCode,
                            BankName = Bank.BankName,
                            BankTypeId = Bank.BankTypeId,
                            Ordering = Bank.Ordering,
                            Address = Bank.Address,
                            Status = Bank.Status,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredMstSleBank = query.PageBy(input);

            return new PagedResultDto<GetMstSleBankForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleBank.ToListAsync()
            );
        }
        public async Task CreateOrEditMaster(CreateOrEditMstSleBankMasterDto input)
        {
            if (input.Id == null)
            {
                await CreateMaster(input);

            }
            else
            {
                await UpdateMaster(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Bank_Create)]
        protected virtual async Task CreateMaster(CreateOrEditMstSleBankMasterDto input)
        {
            var mstSleBankCount = _mstSleBankRepository.GetAll().Where(e => e.BankCode == input.BankCode).Count();
            if (mstSleBankCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                input.IsFinance = 3;
                var mstSleBank = ObjectMapper.Map<MstSleBank>(input);
                await _mstSleBankRepository.InsertAsync(mstSleBank);
            }
        }
        protected virtual async Task UpdateMaster(CreateOrEditMstSleBankMasterDto input)
        {
            var mstSleBankCount = _mstSleBankRepository.GetAll().Where(e => e.BankCode == input.BankCode && e.Id != input.Id).Count();
            if (mstSleBankCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleBank = await _mstSleBankRepository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleBank);
            }
        }
        public async Task<GetMstSleBankMasterForEditOutput> GetMstSleBankMasterForEdit(EntityDto<long> input)
        {
            var mstSleBankMaster = await _mstSleBankRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleBankMasterForEditOutput { MstSleBankMaster = ObjectMapper.Map<CreateOrEditMstSleBankMasterDto>(mstSleBankMaster) };

            return output;
        }
    }
}
