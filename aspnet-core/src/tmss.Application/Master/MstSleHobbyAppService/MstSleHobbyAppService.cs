﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Hobby;

namespace tmss.MstSle.MstSleHobbyAppService
{
    public class MstSleHobbyAppService : tmssAppServiceBase, IMstSleHobbyAppService
    {
        private readonly IRepository<MstSleHobby, long> _repository;
        public MstSleHobbyAppService(
            IRepository<MstSleHobby, long> repo
            )
        {
            _repository = repo;
        }
        public ListResultDto<MstSleHobbyListDto> GetListHobby(MstSleHobbyInput input)
        {
            var listHobby = _repository.GetAll().ToList();
            return new ListResultDto<MstSleHobbyListDto>(ObjectMapper.Map<List<MstSleHobbyListDto>>(listHobby));
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hobby_Search)]
        public async Task<PagedResultDto<GetMstSleHobbyForViewDto>> GetAll(GetMstSleHobbyInputDto input)
        {
            var query = from hobby in _repository.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby hobby.Ordering ascending
                        select new GetMstSleHobbyForViewDto()
                        {
                            Id = hobby.Id,
                            Code = hobby.Code,
                            Description = hobby.Description,
                            Vi_Description = hobby.Vi_Description,
                            Ordering = hobby.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredHobby = query.PageBy(input);

            return new PagedResultDto<GetMstSleHobbyForViewDto>(
                totalCount,
                await pagedAndFilteredHobby.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleHobbyDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hobby_Create)]
        protected virtual async Task Create(CreateOrEditMstSleHobbyDto input)
        {
            var mstSleHobbyCount = _repository.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleHobbyCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleHobby = ObjectMapper.Map<MstSleHobby>(input);
                await _repository.InsertAsync(mstSleHobby);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hobby_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleHobbyDto input)
        {
            var mstSleHobbyCount = _repository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleHobbyCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleHobby = await _repository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleHobby);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hobby_Edit)]
        public async Task<GetMstSleHobbyForEditOutput> GetMstSleHobbyForEdit(EntityDto<long> input)
        {
            var mstSleHobby = await _repository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleHobbyForEditOutput { MstSleHobby = ObjectMapper.Map<CreateOrEditMstSleHobbyDto>(mstSleHobby) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hobby_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _repository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _repository.DeleteAsync(result.Id);
        }
    }
}
