﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Occupation;

namespace tmss.MstSle.MstSleOccupationAppService
{
    public class MstSleOccupationAppService : tmssAppServiceBase, IMstSleOccupationAppService
    {
        private readonly IRepository<MstSleOccupation, long> repository;

        public MstSleOccupationAppService(
            IRepository<MstSleOccupation, long> repo
            )
        {
            repository = repo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Occupation_Search)]
        public ListResultDto<MstSleOccupationListDto> GetListOccupation(MstSleOccupationInput input)
        {

            var listOccupation = repository
              .GetAll()
             .ToList();
            return new ListResultDto<MstSleOccupationListDto>(ObjectMapper.Map<List<MstSleOccupationListDto>>(listOccupation));

        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Occupation_Search)]
        public async Task<PagedResultDto<GetMstSleOccupationForViewDto>> GetAll(GetMstSleOccupationInputDto input)
        {
            var query = from hobby in repository.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby hobby.Ordering ascending
                        select new GetMstSleOccupationForViewDto()
                        {
                            Id = hobby.Id,
                            Code = hobby.Code,
                            Description = hobby.Description,
                            Vi_Description = hobby.Vi_Description,
                            Ordering = hobby.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredOccupation = query.PageBy(input);

            return new PagedResultDto<GetMstSleOccupationForViewDto>(
                totalCount,
                await pagedAndFilteredOccupation.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleOccupationDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Occupation_Create)]
        protected virtual async Task Create(CreateOrEditMstSleOccupationDto input)
        {
            var mstSleOccupationCount = repository.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleOccupationCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleOccupation = ObjectMapper.Map<MstSleOccupation>(input);
                await repository.InsertAsync(mstSleOccupation);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Occupation_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleOccupationDto input)
        {
            var mstSleHobbyCount = repository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleHobbyCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleHobby = await repository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleHobby);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Occupation_Edit)]
        public async Task<GetMstSleOccupationForEditOutput> GetMstSleOccupationForEdit(EntityDto<long> input)
        {
            var mstSleHobby = await repository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleOccupationForEditOutput { MstSleOccupation = ObjectMapper.Map<CreateOrEditMstSleOccupationDto>(mstSleHobby) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Occupation_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await repository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await repository.DeleteAsync(result.Id);
        }
    }
}
