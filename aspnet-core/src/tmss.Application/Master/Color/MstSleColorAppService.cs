﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.MstSle.Dto.Color;
using tmss.MstSle.IMtSleColor;

namespace tmss.Master
{
    public class MstSleColorAppService : tmssAppServiceBase, IMstSleColorsAppService
    {
        private readonly IRepository<MstSleColors, long> _mstSleColorRepository;

        public MstSleColorAppService(IRepository<MstSleColors, long> mstSleColorRepository)
        {
            _mstSleColorRepository = mstSleColorRepository;
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleColors_Search)]
        public async Task<PagedResultDto<MstSleColorDto>> GetAllMstSleColor(GetAllMstSleColorInput input)
        {
            var filteredMstSleColor = _mstSleColorRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.TextFilter), e => false || e.Code.Contains(input.TextFilter) || e.VnName.Contains(input.TextFilter) || e.EnName.Contains(input.TextFilter) || e.Description.Contains(input.TextFilter) || e.Status.Contains(input.TextFilter));
            var pagedAndFilteredMstSleColor = filteredMstSleColor.PageBy(input);

            var mstSleColors = from o in pagedAndFilteredMstSleColor
                               .OrderBy(e => e.Ordering)
                               select new MstSleColorDto()
                               {
                                   Code = o.Code,
                                   VnName = o.VnName,
                                   EnName = o.EnName,
                                   Description = o.Description,
                                   Status = o.Status,
                                   Ordering = o.Ordering,
                                   OrderingRpt = o.OrderingRpt,
                                   HexCode = o.HexCode,
                                   HexCode2 = o.HexCode2,
                                   Id = o.Id
                               };

            var totalCount = await filteredMstSleColor.CountAsync();

            return new PagedResultDto<MstSleColorDto>(
                totalCount,
                await mstSleColors.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleColors_Edit)]
        public async Task<GetMstSleColorForEditOutput> GetMstSleColorsForEdit(EntityDto input)
        {
            var mstSleColors = await _mstSleColorRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleColorForEditOutput { MstSleColors = ObjectMapper.Map<CreateOrEditMstSleColorDto>(mstSleColors) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleColorDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleColors_Create)]
        protected virtual async Task Create(CreateOrEditMstSleColorDto input)
        {
            var transportNameCount = _mstSleColorRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleColors = ObjectMapper.Map<MstSleColors>(input);

                await _mstSleColorRepository.InsertAsync(mstSleColors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleColors_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleColorDto input)
        {
            var transportNameCount = _mstSleColorRepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleColors = await _mstSleColorRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, mstSleColors);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstSleColors_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleColorRepository.DeleteAsync(input.Id);
        }
        public async Task<List<GetListMstSleColorCodeDto>> GetListColor()
        {
            var result = from color in _mstSleColorRepository.GetAll().Where(e => e.Status == "Y")
                         orderby color.Id descending
                         select new GetListMstSleColorCodeDto
                         {
                             Id = color.Id,
                             Code = color.Code,
                             VnName = color.VnName,
                             EnName = color.EnName,
                             HexCode = color.HexCode,
                         };
            return await result.OrderBy(x => x.Code).ToListAsync();
        }

    }
}
