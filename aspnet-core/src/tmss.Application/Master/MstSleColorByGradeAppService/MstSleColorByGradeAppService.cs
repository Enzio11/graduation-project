﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ColorByGrade;
using tmss.MstSle.Dto.ColorByGrade.Dto;

namespace tmss.MstSle.MstSleColorByGradeAppService
{
    public class MstSleColorByGradeAppService : tmssAppServiceBase, IMstSleColorByGradeAppService
    {
        private readonly IRepository<MstSleGrades, long> _mstSleGradeRepository;
        private readonly IRepository<MstSleGradeProduction, long> _mstSleGradeProducesRepository;
        private readonly IRepository<MstSleModel, long> _mstSleModelRepository;
        private readonly IRepository<MstSleColorGradesProduction, long> _mstSleColorGradeProduceRepository;
        private readonly IRepository<MstSleColors, long> _mstSleColorRepository;
        private readonly IRepository<MstSleInteriorColorGradesProduction, long> _mstSleInteriorColorGradeRepository;
        private readonly IRepository<MstSleLookUp> _mstSleLookUpRepository;

        public MstSleColorByGradeAppService(
            IRepository<MstSleGrades, long> mstSleGradeRepository,
            IRepository<MstSleGradeProduction, long> mstSleGradeProducesRepository,
            IRepository<MstSleModel, long> mstSleModelRepository,
            IRepository<MstSleColorGradesProduction, long> mstSleColorGradeProduceRepository,
            IRepository<MstSleColors, long> mstSleColorRepository,
            IRepository<MstSleInteriorColorGradesProduction, long> mstSleInteriorColorGradeRepository,
            IRepository<MstSleLookUp> mstSleLookUpRepository)

        {
            _mstSleGradeRepository = mstSleGradeRepository;
            _mstSleGradeProducesRepository = mstSleGradeProducesRepository;
            _mstSleModelRepository = mstSleModelRepository;
            _mstSleColorGradeProduceRepository = mstSleColorGradeProduceRepository;
            _mstSleColorRepository = mstSleColorRepository;
            _mstSleInteriorColorGradeRepository = mstSleInteriorColorGradeRepository;
            _mstSleLookUpRepository = mstSleLookUpRepository;
        }

        public async Task<List<GetMstSleGradeDto>> GetListGrade()
        {
            var listMake = from make in _mstSleGradeRepository.GetAll()
                           select new GetMstSleGradeDto()
                           {
                               Id = make.Id,
                               MarketingCode = make.MarketingCode
                           };
            return await listMake.ToListAsync();
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleGradeProduceDto>> GetAllGradesProduces(GetProductionInput input)
        {
            var filteredMstSleGradeProduces = _mstSleGradeProducesRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.ProductionCode.Contains(input.FilterText)
                || e.EnName.Contains(input.FilterText)
                || e.ShortModel.Contains(input.FilterText)
                || e.FullModel.Contains(input.FilterText)
                || e.CaseSize.Contains(input.FilterText)
                || e.Status.Contains(input.FilterText));

            var pagedAndFilteredMstSleGradeProduces = filteredMstSleGradeProduces.PageBy(input);

            var mstSleGradeProduces = from production in pagedAndFilteredMstSleGradeProduces
                                      join Grade in _mstSleGradeRepository.GetAll() on production.GradeId equals Grade.Id into GraPro
                                      from GrPr in GraPro.DefaultIfEmpty()
                                      join model in _mstSleModelRepository.GetAll() on GrPr.ModelId equals model.Id into modelGraPro
                                      from mdGrPr in modelGraPro.DefaultIfEmpty()

                                      orderby production.Ordering descending
                                      select new GetMstSleGradeProduceDto()
                                      {
                                          Id = production.Id,
                                          ProductionCode = production.ProductionCode,
                                          EnName = production.EnName,
                                          VnName = production.VnName,
                                          Ordering = production.Ordering,
                                          IsHasAudio = production.IsHasAudio,
                                          CbuCkd = production.CbuCkd,
                                          ShortModel = production.ShortModel,
                                          Wmi = production.Wmi,
                                          Vds = production.Vds,
                                          FullModel = production.FullModel,
                                          Barcode = production.Barcode,
                                          GasolineTypeId = production.GasolineTypeId,
                                          GradeId = production.GradeId,
                                          Grade = GrPr.MarketingCode,
                                          FromDate = production.FromDate,
                                          ToDate = production.ToDate,
                                          IsFirmColor = production.IsFirmColor,
                                          FrameNoLength = production.FrameNoLength,
                                          ManYear = production.ManYear,
                                          Capacity = production.Capacity,
                                          Length = production.Length,
                                          Width = production.Width,
                                          Weight = production.Weight,
                                          TireSize = production.TireSize,
                                          Payload = production.Payload,
                                          PullingWeight = production.PullingWeight,
                                          SeatNoStanding = production.SeatNoStanding,
                                          SeatNoLying = production.SeatNoLying,
                                          SeatNo = production.SeatNo,
                                          CaseSize = production.CaseSize,
                                          BaseLength = production.BaseLength,
                                          Status = production.Status,
                                          FloId = production.FloId,
                                          OriginFrom = production.OriginFrom,
                                          Fuel = production.Fuel,
                                          Engine = production.Engine,
                                          DesignStyle = production.DesignStyle,
                                          Model = mdGrPr.MarketingCode
                                      };
            var totalResultCount = await filteredMstSleGradeProduces.CountAsync();
            return new PagedResultDto<GetMstSleGradeProduceDto>(
                totalResultCount,
                await mstSleGradeProduces.ToListAsync()
                );
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Search)]
        public async Task<List<GetMstSleColorGradeProductionForViewDto>> GetColorGradesProducesByProduceId(long GradeProductionId)
        {
            var colorGradeProduction = from o in _mstSleColorGradeProduceRepository.GetAll()
                                        .Where(r => r.ProduceId == GradeProductionId)
                                       join color in _mstSleColorRepository.GetAll() on o.ColorId equals color.Id
                                       orderby o.Id descending
                                       select new GetMstSleColorGradeProductionForViewDto()
                                       {
                                           Id = o.Id,
                                           Description = o.Description,
                                           Status = o.Status,
                                           Ordering = o.Ordering,
                                           Color = color.Code,
                                           ColorId = o.ColorId,
                                           ProduceId = o.ProduceId,
                                       };

            return await colorGradeProduction.ToListAsync();
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Search)]
        public async Task<List<GetMstSleInteriorColorGradesProductionForViewDto>> GetInteriorColorGradesProducesByProduceId(long GradeProductionId)
        {
            var listInteriorColor = from o in _mstSleInteriorColorGradeRepository.GetAll().Where(r => r.ProduceId == GradeProductionId)
                                    join color in _mstSleColorRepository.GetAll() on o.ColorId equals color.Id
                                    orderby o.Id descending
                                    select new GetMstSleInteriorColorGradesProductionForViewDto()
                                    {
                                        Ordering = o.Ordering,
                                        ColorId = o.ColorId,
                                        ProduceId = o.ProduceId,
                                        Description = o.Description,
                                        Status = o.Status,
                                        Id = o.Id,
                                        Color = color.Code,
                                    };

            return await listInteriorColor.ToListAsync();
        }

        public async Task<List<GetMstSleGradeProduceDto>> GetListProduction()
        {
            var listProduction = from production in _mstSleGradeProducesRepository.GetAll()
                                 select new GetMstSleGradeProduceDto()
                                 {
                                     Id = production.Id,
                                     ProductionCode = production.ProductionCode
                                 };
            return await listProduction.ToListAsync();
        }
        public async Task<List<GetMstSleLookUpDto>> GetListGasolineType()
        {
            var listGasolineType = from lookUp in _mstSleLookUpRepository.GetAll()
                                    .Where(e => e.Code == "gasoline_type")
                                    .Where(e => e.Status == "Y")
                                   select new GetMstSleLookUpDto()
                                   {
                                       Id = lookUp.Id,
                                       Code = lookUp.Code,
                                       Name = lookUp.Name
                                   };
            return await listGasolineType.ToListAsync();
        }
        public async Task<List<GetMstSleInteriorColorGradesProductionForViewDto>> GetListColor()
        {
            var listColor = from color in _mstSleColorRepository.GetAll().Where(e => e.Status == "Y")
                            select new GetMstSleInteriorColorGradesProductionForViewDto()
                            {
                                Color = color.Code,
                                ColorId = color.Id
                            };
            return await listColor.ToListAsync();
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Edit)]
        public async Task<CreateOrEditMstSleColorGradesProductionDto> GetColorGradesProducesForEdit(EntityDto input)
        {
            var colorForEdit = await _mstSleColorGradeProduceRepository.FirstOrDefaultAsync(input.Id);
            var output = new CreateOrEditMstSleColorGradesProductionDto
            {
                Ordering = colorForEdit.Ordering,
                ColorId = colorForEdit.ColorId,
                ProduceId = colorForEdit.ProduceId,
                Status = colorForEdit.Status,
                Description = colorForEdit.Description,
                Id = colorForEdit.Id
            };
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Edit)]
        public async Task<CreateOrEditMstSleInteriorColorGradesProductionDto> GetInteriorColorGradesProducesForEdit(EntityDto input)
        {
            var interiorColorForEdit = await _mstSleInteriorColorGradeRepository.FirstOrDefaultAsync(input.Id);
            var output = new CreateOrEditMstSleInteriorColorGradesProductionDto
            {
                Ordering = interiorColorForEdit.Ordering,
                ColorId = interiorColorForEdit.ColorId,
                ProduceId = interiorColorForEdit.ProduceId,
                Status = interiorColorForEdit.Status,
                Description = interiorColorForEdit.Description,
                Id = interiorColorForEdit.Id
            };
            return output;
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Delete)]
        public async Task DeleteColor(EntityDto input)
        {
            await _mstSleColorGradeProduceRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Delete)]
        public async Task DeleteInterior(EntityDto input)
        {
            await _mstSleInteriorColorGradeRepository.DeleteAsync(input.Id);
        }
        public async Task CreateOrEditColor(CreateOrEditMstSleColorGradesProductionDto input)
        {
            if (input.Id == null)
            {
                await CreateColor(input);
            }
            else
            {
                await UpdateColor(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Create)]
        protected virtual async Task CreateColor(CreateOrEditMstSleColorGradesProductionDto input)
        {
            var transportNameCount = _mstSleColorGradeProduceRepository.GetAll().Where(e => e.ColorId == input.ColorId && e.ProduceId == input.ProduceId).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisColorCodeAlreadyExists"));
            }
            else
            {
                var mstSleColorGradesProduces = ObjectMapper.Map<MstSleColorGradesProduction>(input);
                await _mstSleColorGradeProduceRepository.InsertAsync(mstSleColorGradesProduces);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Edit)]
        protected virtual async Task UpdateColor(CreateOrEditMstSleColorGradesProductionDto input)
        {
            var mstSleColorGradesProduces = await _mstSleColorGradeProduceRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleColorGradesProduces);
        }
        public async Task CreateOrEditInterior(CreateOrEditMstSleInteriorColorGradesProductionDto input)
        {
            if (input.Id == null)
            {
                await CreateInterior(input);
            }
            else
            {
                await UpdateInterior(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Create)]
        protected virtual async Task CreateInterior(CreateOrEditMstSleInteriorColorGradesProductionDto input)
        {
            var transportNameCount = _mstSleInteriorColorGradeRepository.GetAll().Where(e => e.ColorId == input.ColorId && e.ProduceId == input.ProduceId).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisColorCodeAlreadyExists"));
            }
            else
            {
                var mstSleInteriorColorGradesProduces = ObjectMapper.Map<MstSleInteriorColorGradesProduction>(input);
                await _mstSleInteriorColorGradeRepository.InsertAsync(mstSleInteriorColorGradesProduces);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Color_By_Grade_Edit)]
        protected virtual async Task UpdateInterior(CreateOrEditMstSleInteriorColorGradesProductionDto input)
        {
            var mstSleInteriorColorGradesProduces = await _mstSleInteriorColorGradeRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleInteriorColorGradesProduces);
        }
    }
}
