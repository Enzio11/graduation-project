﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.MstSle.IMstGenProvince;
using tmss.MstSle.IMstGenProvince.Dto;

namespace tmss.Master
{
    public class MstGenProvinceAppService : tmssAppServiceBase, IMstGenProvinceAppService
    {
        private readonly IRepository<MstGenProvince, long> _mstGenProvincesRepository;
        public MstGenProvinceAppService(IRepository<MstGenProvince, long> mstGenProvincesRepository)
        {
            _mstGenProvincesRepository = mstGenProvincesRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenProvinces_Search)]
        public async Task<PagedResultDto<GetMstGenProvincesForViewDto>> GetAll(GetAllMstGenProvincesInput input)
        {
            var filteredMstGenProvinces = _mstGenProvincesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.Contains(input.CodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                        .WhereIf(input.SubRegionIdFilter != null, e => e.SubRegionId == input.SubRegionIdFilter)
                        .WhereIf(input.OrderingFilter != null, e => e.Ordering == input.OrderingFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
                        .WhereIf(input.PopulationAmountMinFilter != null, e => e.PopulationAmount >= input.PopulationAmountMinFilter)
                        .WhereIf(input.PopulationAmountMaxFilter != null, e => e.PopulationAmount <= input.PopulationAmountMaxFilter)
                        .WhereIf(input.SquareAmountMinFilter != null, e => e.SquareAmount >= input.SquareAmountMinFilter)
                        .WhereIf(input.SquareAmountMaxFilter != null, e => e.SquareAmount <= input.SquareAmountMaxFilter)
                        .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstGenProvinces = filteredMstGenProvinces.PageBy(input);

            var mstGenProvinces = from province in pagedAndFilteredMstGenProvinces

                                  select new GetMstGenProvincesForViewDto()
                                  {
                                      Code = province.Code,
                                      Name = province.Name,
                                      Ordering = province.Ordering,
                                      Status = province.Status,
                                      SubRegionId = province.SubRegionId,
                                      PopulationAmount = province.PopulationAmount,
                                      SquareAmount = province.SquareAmount,
                                      RegistrationFee = province.RegistrationFee,
                                      PercentOwnershipTax = province.PercentOwnershipTax,
                                      Id = province.Id,
                                  };

            var totalCount = await filteredMstGenProvinces.CountAsync();

            return new PagedResultDto<GetMstGenProvincesForViewDto>(
                totalCount,
                await mstGenProvinces.ToListAsync()
            );
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_CustomApi)]
        public async Task<PagedResultDto<GetMstGenProvincesForViewDto>> CusAppGetAll(GetAllMstGenProvincesInput input)
        {
            var filteredMstGenProvinces = _mstGenProvincesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.Contains(input.CodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                        .WhereIf(input.SubRegionIdFilter != null, e => e.SubRegionId == input.SubRegionIdFilter)
                        .WhereIf(input.OrderingFilter != null, e => e.Ordering == input.OrderingFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
                        .WhereIf(input.PopulationAmountMinFilter != null, e => e.PopulationAmount >= input.PopulationAmountMinFilter)
                        .WhereIf(input.PopulationAmountMaxFilter != null, e => e.PopulationAmount <= input.PopulationAmountMaxFilter)
                        .WhereIf(input.SquareAmountMinFilter != null, e => e.SquareAmount >= input.SquareAmountMinFilter)
                        .WhereIf(input.SquareAmountMaxFilter != null, e => e.SquareAmount <= input.SquareAmountMaxFilter)
                        .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstGenProvinces = filteredMstGenProvinces.PageBy(input);

            var mstGenProvinces = from province in pagedAndFilteredMstGenProvinces

                                  select new GetMstGenProvincesForViewDto()
                                  {
                                      Code = province.Code,
                                      Name = province.Name,
                                      Ordering = province.Ordering,
                                      Status = province.Status,
                                      SubRegionId = province.SubRegionId,
                                      PopulationAmount = province.PopulationAmount,
                                      SquareAmount = province.SquareAmount,
                                      RegistrationFee = province.RegistrationFee,
                                      PercentOwnershipTax = province.PercentOwnershipTax,
                                      Id = province.Id,
                                  };

            var totalCount = await filteredMstGenProvinces.CountAsync();

            return new PagedResultDto<GetMstGenProvincesForViewDto>(
                totalCount,
                await mstGenProvinces.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenProvinces_Edit)]
        public async Task<GetMstGenProvincesForEditOutput> GetMstGenProvincesForEdit(EntityDto input)
        {
            var mstGenProvinces = await _mstGenProvincesRepository.FirstOrDefaultAsync(input.Id);
            var output = new GetMstGenProvincesForEditOutput { MstGenProvinces = ObjectMapper.Map<CreateOrEditMstGenProvinceDto>(mstGenProvinces) };
            return output;
        }

        public async Task CreateOrEdit(CreateOrEditMstGenProvinceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenProvinces_Create)]
        protected virtual async Task Create(CreateOrEditMstGenProvinceDto input)
        {
            if (input.PercentOwnershipTax > 100)
            {
                throw new UserFriendlyException(00, L("MstGenProvincesPercentOwnershipTaxValid"));
            }
            var transportNameCount = _mstGenProvincesRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstGenProvinces = ObjectMapper.Map<MstGenProvince>(input);
                await _mstGenProvincesRepository.InsertAsync(mstGenProvinces);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenProvinces_Edit)]
        protected virtual async Task Update(CreateOrEditMstGenProvinceDto input)
        {
            if (input.PercentOwnershipTax > 100)
            {
                throw new UserFriendlyException(00, L("MstGenProvincesPercentOwnershipTaxValid"));
            }
            var transportNameCount = _mstGenProvincesRepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstGenProvinces = await _mstGenProvincesRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, mstGenProvinces);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenProvinces_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstGenProvincesRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_MstGenProvinces_Search)]
        public async Task<List<GetNameProvinceDto>> GetNameProvince()
        {
            var provice = from p in _mstGenProvincesRepository.GetAll()
                          select new GetNameProvinceDto()
                          {
                              Id = p.Id,
                              Name = p.Name
                          };
            return await provice.ToListAsync();
        }
    }
}
