﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Linq;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.AgeRange;

namespace tmss.MstSle
{
    public class MstSleAgeRangeAppService : tmssAppServiceBase, IMstSleAgeRangeAppService
    {
        private readonly IRepository<MstSleAgeRange, long> _MstSleAgeRangeRepository;

        public MstSleAgeRangeAppService(IRepository<MstSleAgeRange, long> MstSleAgeRangeRepository)
        {
            _MstSleAgeRangeRepository = MstSleAgeRangeRepository;
        }
        [AbpAuthorize(AppPermissions.Pages_Tenants)]
        public ListResultDto<MstSleAgeRangeListDto> GetListAgeRange(MstSleAgeRangeInput input)
        {
            var listAgeRange = _MstSleAgeRangeRepository.GetAll().ToList();
            return new ListResultDto<MstSleAgeRangeListDto>(ObjectMapper.Map<List<MstSleAgeRangeListDto>>(listAgeRange));
        }
    }
}
