﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.IMstSleReasonOfFreeze;
using tmss.MstSle.IMstSleReasonOfFreeze.Dto;

namespace tmss.MstSle.MstSleReasonOfFreezeAppService
{
    public class MstSleReasonOfFreezeAppService : tmssAppServiceBase, IMstSleReasonOfFreezeAppService
    {
        private readonly IRepository<MstSleReasonOfFreeze, long> _mstSleReasonOfFreezeRepository;

        public MstSleReasonOfFreezeAppService
            (
            IRepository<MstSleReasonOfFreeze, long> mstSleReasonOfFreezeRepository
            )
        {
            _mstSleReasonOfFreezeRepository = mstSleReasonOfFreezeRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Search)]
        public async Task<PagedResultDto<GetMstSleReasonOfFreezeForViewDto>> GetAll(GetMstSleReasonOfFreezeInput input)
        {
            var query = from reasonOfFreeze in _mstSleReasonOfFreezeRepository.GetAll()
                        .Where(e => input.FilterText == null || e.Code.Contains(input.FilterText)
                       || e.Description.Contains(input.FilterText) || e.ViDescription.Contains(input.FilterText))
                        orderby reasonOfFreeze.Ordering ascending

                        select new GetMstSleReasonOfFreezeForViewDto
                        {
                            Id = reasonOfFreeze.Id,
                            Code = reasonOfFreeze.Code,
                            Description = reasonOfFreeze.Description,
                            ViDescription = reasonOfFreeze.ViDescription,
                            Ordering = reasonOfFreeze.Ordering
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredReasonOfFreeze = query.PageBy(input);

            return new PagedResultDto<GetMstSleReasonOfFreezeForViewDto>(
                totalCount,
                await pagedAndFilteredReasonOfFreeze.ToListAsync());
        }

        public async Task CreateOrEdit(CreateOrEditMstSleReasonOfFreezeDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Create)]
        protected virtual async Task Create(CreateOrEditMstSleReasonOfFreezeDto input)
        {
            var mstSleReasonOfFreezeCount = _mstSleReasonOfFreezeRepository.GetAll().Where(e => e.Code == input.Code).Count();

            if (mstSleReasonOfFreezeCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAreadyExists"));
            }
            else
            {
                var mstSleReasonOfFreeze = ObjectMapper.Map<MstSleReasonOfFreeze>(input);
                await _mstSleReasonOfFreezeRepository.InsertAsync(mstSleReasonOfFreeze);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleReasonOfFreezeDto input)
        {
            var mstSleReasonOfFreezeCategoryCount = _mstSleReasonOfFreezeRepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleReasonOfFreezeCategoryCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAreadyExists"));
            }
            else
            {
                var mstSleReasonOfFreezeCategory = await _mstSleReasonOfFreezeRepository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleReasonOfFreezeCategory);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Delete)]
        public async Task DeleteReasonOfFreeze(EntityDto<long> input)
        {
            var result = await _mstSleReasonOfFreezeRepository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleReasonOfFreezeRepository.DeleteAsync(result.Id);
        }

        public async Task<GetMstSleReasonOfFreezeForEditOutput> GetMstSleReasonOfFreezeForEdit(EntityDto<long> input)
        {
            var MstSleReasonOfFreezeCategory = await _mstSleReasonOfFreezeRepository.FirstOrDefaultAsync(input.Id);

            var ouput = new GetMstSleReasonOfFreezeForEditOutput { MstSleReasonOfFreezeDto = ObjectMapper.Map<CreateOrEditMstSleReasonOfFreezeDto>(MstSleReasonOfFreezeCategory) };

            return ouput;
        }
    }
}
