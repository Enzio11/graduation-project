﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Master;
using tmss.MstSle.Dto.VehiclePrice;

namespace tmss.MstSle.VehiclePrice
{
    public class MstSleVehiclePriceAppService : tmssAppServiceBase
    {
        private readonly IRepository<MstSleModel, long> _mstSleModelRepository;
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;
        private readonly IRepository<MstSleGradeProduction, long> _mstSleGradeProductionRepository;
        private readonly IRepository<MstSleVehiclePrice, long> _mstSleVehiclePriceRepository;
        private readonly IRepository<MstSleColors, long> _mstSleColorRepository;
        private readonly IRepository<MstSleColorGradesProduction, long> _mstSleColorGradesProduction;
        private readonly IRepository<MstSleInteriorColorGradesProduction, long> _mstSleInteriorColorGradesProduction;

        public MstSleVehiclePriceAppService(
            IRepository<MstSleModel, long> MstSleModelRepository,
            IRepository<MstSleGrades, long> MstSleGradesRepository,
            IRepository<MstSleVehiclePrice, long> MstSleVehiclePriceRepository,
            IRepository<MstSleGradeProduction, long> MstSleGradeProductionRepository,
            IRepository<MstSleColors, long> mstSleColorRepository,
            IRepository<MstSleColorGradesProduction, long> mstSleColorGradesProduction,
            IRepository<MstSleInteriorColorGradesProduction, long> mstSleInteriorColorGradesProduction
            )
        {
            _mstSleModelRepository = MstSleModelRepository;
            _mstSleGradesRepository = MstSleGradesRepository;
            _mstSleVehiclePriceRepository = MstSleVehiclePriceRepository;
            _mstSleColorRepository = mstSleColorRepository;
            _mstSleColorGradesProduction = mstSleColorGradesProduction;
            _mstSleInteriorColorGradesProduction = mstSleInteriorColorGradesProduction;
            _mstSleGradeProductionRepository = MstSleGradeProductionRepository;
        }

        public async Task UpdateDataVehiclePrice()
        {
            var listVehiclePrice = await _mstSleVehiclePriceRepository.GetAllListAsync();
            foreach (var singleVehiclePrice in listVehiclePrice)
            {
                var gradeProduction = await _mstSleGradeProductionRepository.GetAsync((long)singleVehiclePrice.GradeProductionId);
                long gradeId = (long)gradeProduction.GradeId;
                var grade = await _mstSleGradesRepository.GetAsync(gradeId);
                long modelId = grade.ModelId;
                singleVehiclePrice.ModelId = modelId;
                singleVehiclePrice.GradeId = gradeId;
                await _mstSleVehiclePriceRepository.UpdateAsync(singleVehiclePrice);
            }
        }

        public async Task<PagedResultDto<GetMstSleVehiclePriceForViewDto>> GetAll(GetAllMstSleVehiclePriceInput input)
        {
            var filteredMstSleVehiclePrices = from vehiclePrice in _mstSleVehiclePriceRepository.GetAll()
                 .Where(e => e.Status == "Y")
                //.WhereIf(input.ModelId != null, e => e.ModelId == input.ModelId)
                //.WhereIf(input.GradeId != null, e => e.GradeId == input.GradeId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => false || e.Status == input.StatusFilter)
                .WhereIf(input.GradeProductionIdFilter != null, e => e.GradeProductionId == input.GradeProductionIdFilter)
                .WhereIf(input.ColorIdFilter != null, e => e.ColorId == input.ColorIdFilter)
                .WhereIf(input.TransmissionsFilter != null, e => e.TransmissionId == input.TransmissionsFilter)
                .WhereIf(input.FuelTypeFilter != null, e => e.FuelTypeId == input.FuelTypeFilter)
                .WhereIf(input.EngineTypeFilter != null, e => e.EngineTypeId == input.EngineTypeFilter)
                .WhereIf(input.IsCBUFilter != null, e => e.isCBU == input.IsCBUFilter)
                .WhereIf(input.InteriorColorIdFilter != null, e => e.InteriorColorId == input.InteriorColorIdFilter)
                .WhereIf(input.PriceAmountMinFilter != null, e => e.PriceAmount >= input.PriceAmountMinFilter)
                .WhereIf(input.PriceAmountMaxFilter != null, e => e.PriceAmount <= input.PriceAmountMaxFilter)
                .WhereIf(input.OrderPriceAmountMinFilter != null, e => e.OrderPriceAmount >= input.OrderPriceAmountMinFilter)
                .WhereIf(input.OrderPriceAmountMaxFilter != null, e => e.OrderPriceAmount <= input.OrderPriceAmountMaxFilter)

                                              join model in _mstSleModelRepository.GetAll() on vehiclePrice.ModelId equals model.Id
                                              join gp in _mstSleGradeProductionRepository.GetAll() on vehiclePrice.GradeProductionId equals gp.Id
                                              join grade in _mstSleGradesRepository.GetAll() on gp.GradeId equals grade.Id

                                              orderby vehiclePrice.Id descending
                                              select new GetMstSleVehiclePriceForViewDto()
                                              {
                                                  OrderPriceAmount = vehiclePrice.OrderPriceAmount,
                                                  PriceAmount = vehiclePrice.PriceAmount,
                                                  Id = vehiclePrice.Id,
                                                  GradeProductionId = vehiclePrice.GradeProductionId,
                                                  ColorId = vehiclePrice.ColorId,
                                                  InteriorColorId = vehiclePrice.InteriorColorId,
                                                  Status = vehiclePrice.Status,
                                                  ProductionCode = gp.ProductionCode,
                                                  GradeId = grade.Id,
                                                  MarketingCode = grade.MarketingCode,
                                                  ModelId = grade.ModelId,
                                                  Remarks = vehiclePrice.Remarks,
                                                  IsCBU = vehiclePrice.isCBU,
                                                  SeatNo = gp.SeatNo,
                                                  CommercialName = vehiclePrice.CommercialName,
                                                  ModelCode = model.MarketingCode
                                              };

            var pagedAndFilteredMstSleVehiclePrices = filteredMstSleVehiclePrices.PageBy(input);

            var totalCount = await filteredMstSleVehiclePrices.CountAsync();

            return new PagedResultDto<GetMstSleVehiclePriceForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleVehiclePrices.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleVehiclePriceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }


        protected virtual async Task Create(CreateOrEditMstSleVehiclePriceDto input)
        {

            var MstSleVehiclePriceCount = _mstSleVehiclePriceRepository.GetAll()
                .Where(e => e.ModelId == input.ModelId && e.GradeId == input.GradeId && e.ColorId == input.ColorId && e.GradeProductionId == input.GradeProductionId && e.InteriorColorId == input.InteriorColorId).Count();
            if (MstSleVehiclePriceCount >= 1)
            {
                throw new UserFriendlyException(00, L("MstSleVehiclePriceThrowLoanKeyALreadyPramsByModel"));
            }
            var mstSleVehiclePrices = ObjectMapper.Map<MstSleVehiclePrice>(input);

            await _mstSleVehiclePriceRepository.InsertAsync(mstSleVehiclePrices);
        }

        protected virtual async Task Update(CreateOrEditMstSleVehiclePriceDto input)
        {

            var MstSleVehiclePriceCount = _mstSleVehiclePriceRepository.GetAll()
                .Where(e => e.ModelId == input.ModelId && e.GradeId == input.GradeId && e.ColorId == input.ColorId && e.GradeProductionId == input.GradeProductionId && e.InteriorColorId == input.InteriorColorId && e.Id != input.Id).Count();
            if (MstSleVehiclePriceCount >= 1)
            {
                throw new UserFriendlyException(00, L("MstSleVehiclePriceThrowLoanKeyALreadyPramsByModel"));
            }
            var mstSleVehiclePrices = await _mstSleVehiclePriceRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, mstSleVehiclePrices);
        }

        public async Task Delete(EntityDto input)
        {
            await _mstSleVehiclePriceRepository.DeleteAsync(input.Id);
        }

        public async Task<List<MstSleColorGradesProduction>> GetAllColorGP()
        {
            var colorGP = (from cgp in _mstSleColorGradesProduction.GetAll()
                           orderby cgp.Id descending
                           select new MstSleColorGradesProduction
                           {
                               ProduceId = cgp.ProduceId,
                               ColorId = cgp.ColorId
                           }).ToListAsync();
            return await colorGP;
        }

        public decimal? GetGradeIdByGPId(long gradeProductionId)
        {
            return _mstSleGradeProductionRepository.GetAll().FirstOrDefault(e => e.Id == gradeProductionId).GradeId;
        }

        public async Task<List<MstSleInteriorColorGradesProduction>> GetAllIColorGP()
        {
            var iColorGP = (from icgp in _mstSleInteriorColorGradesProduction.GetAll()
                            orderby icgp.Id descending
                            select new MstSleInteriorColorGradesProduction
                            {
                                ProduceId = icgp.ProduceId,
                                ColorId = icgp.ColorId
                            }).ToListAsync();
            return await iColorGP;
        }

        public async Task<List<GetListMstSleVehiclePriceColorDto>> GetListColor()
        {
            var result = from color in _mstSleColorRepository.GetAll().OrderBy(p => p.VnName)
                         select new GetListMstSleVehiclePriceColorDto
                         {
                             Id = color.Id,
                             Code = color.Code,
                             VnName = color.VnName + " ( " + color.Code + ")",
                             EnName = color.EnName,
                             HexCode = color.HexCode,
                         };
            return await result.ToListAsync();
        }

        public async Task<List<GetAllGradeColorForVehiclePrice>> GetListGradeProduces()
        {
            var result = from production in _mstSleGradeProductionRepository.GetAll()
                         orderby production.Id descending
                         select new GetAllGradeColorForVehiclePrice()
                         {
                             Id = production.Id,
                             ProductionCode = production.ProductionCode,
                             GradeId = production.GradeId
                         };
            return await result.ToListAsync();
        }

        public async Task<List<GetListGradeNameForPriceDto>> GetListGrade()
        {
            var mstSleGrades = from grade in _mstSleGradesRepository.GetAll()
                               orderby grade.Id descending
                               select new GetListGradeNameForPriceDto()
                               {
                                   MarketingCode = grade.MarketingCode,
                                   ProductionCode = grade.ProductionCode,
                                   ModelId = grade.ModelId,
                                   EnName = grade.EnName,
                                   VnName = grade.VnName,
                                   Id = grade.Id
                               };

            return await mstSleGrades.OrderBy(x => x.MarketingCode).ToListAsync();
        }

        public async Task<GetMstSleVehiclePriceForEditOutput> GetMstSleVehiclePriceForEdit(EntityDto<long> input)
        {
            var mstSleVehiclePrice = await _mstSleVehiclePriceRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleVehiclePriceForEditOutput { MstSleVehiclePrice = ObjectMapper.Map<CreateOrEditMstSleVehiclePriceDto>(mstSleVehiclePrice) };

            return output;
        }
    }
}

