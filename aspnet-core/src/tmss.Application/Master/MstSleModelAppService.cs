﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.MakeCompetitor.Dto;
using tmss.MstSle.IMstSleModel.Dto;

namespace tmss.MstSle
{
    public class MstSleModelAppService : tmssAppServiceBase, IMstSleModelAppService
    {
        private readonly IRepository<MstSleModel, long> _mstSleModelsRepository;
        private readonly IRepository<MstSleMake, long> _mstSleMakeRepository;

        public MstSleModelAppService(
            IRepository<MstSleModel, long> mstSleModelsRepository,
            IRepository<MstSleMake, long> mstSleMakeRepository)
        {
            _mstSleModelsRepository = mstSleModelsRepository;
            _mstSleMakeRepository = mstSleMakeRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleModelsForViewDto>> GetAll(GetAllMstSleModelsInput input)
        {
            var filteredMstSleModels = _mstSleModelsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.MarketingCode.Contains(input.Filter)
                        || e.ProductionCode.Contains(input.Filter) || e.VnName.Contains(input.Filter) || e.EnName.Contains(input.Filter)
                        || e.Description.Contains(input.Filter) || e.Abbreviation.Contains(input.Filter))
                        .WhereIf(input.MakeIdFilter != null, e => e.MakeId == input.MakeIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AbbreviationFilter), e => e.Abbreviation.Contains(input.AbbreviationFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MarketingCodeFilter), e => e.MarketingCode.Contains(input.MarketingCodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductionCodeFilter), e => e.ProductionCode.Contains(input.ProductionCodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName.Contains(input.VnNameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName.Contains(input.EnNameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status.Contains(input.StatusFilter))
                        .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleModels = filteredMstSleModels.PageBy(input);

            var mstSleModels = from o in pagedAndFilteredMstSleModels
                               select new GetMstSleModelsForViewDto()
                               {
                                   Abbreviation = o.Abbreviation,
                                   ProductionCode = o.ProductionCode,
                                   MarketingCode = o.MarketingCode,
                                   VnName = o.VnName,
                                   EnName = o.EnName,
                                   Description = o.Description,
                                   Status = o.Status,
                                   Ordering = o.Ordering,
                                   Id = o.Id,
                                   MarketingCodeWeb = o.MarketingCodeWeb
                               };

            var totalCount = await filteredMstSleModels.CountAsync();

            return new PagedResultDto<GetMstSleModelsForViewDto>(
                totalCount,
                await mstSleModels.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleModelsDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Create)]
        protected virtual async Task Create(CreateOrEditMstSleModelsDto input)
        {
            var mstSleModels = ObjectMapper.Map<MstSleModel>(input);

            await _mstSleModelsRepository.InsertAsync(mstSleModels);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleModelsDto input)
        {
            var mstSleModels = await _mstSleModelsRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, mstSleModels);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleModelsRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Edit)]
        public async Task<GetMstSleModelsForEditOutput> GetMstSleModelsForEdit(EntityDto input)
        {
            var mstSleModels = await _mstSleModelsRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleModelsForEditOutput { MstSleModels = ObjectMapper.Map<CreateOrEditMstSleModelsDto>(mstSleModels) };

            return output;
        }

        public async Task<List<MstSleModelsDto>> GetListModelByStatus(string Status, long? makeId)
        {
            var model = from m in _mstSleModelsRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(Status), e => e.Status == Status)
                        .WhereIf(makeId != null, e => e.MakeId == makeId)

                        orderby m.Ordering descending
                        select new MstSleModelsDto
                        {
                            Id = m.Id,
                            MarketingCode = m.MarketingCode,
                            ProductionCode = m.ProductionCode,
                            EnName = m.EnName,
                            VnName = m.VnName
                        };

            return await model.ToListAsync();
        }

        public async Task<List<MstSleModelsDto>> GetModelCode(long? inputId)
        {
            var model = from m in _mstSleModelsRepository.GetAll().WhereIf(inputId != null, e => e.Id != inputId)
                        orderby m.Ordering descending
                        select new MstSleModelsDto
                        {
                            MarketingCode = m.MarketingCode,
                            ProductionCode = m.ProductionCode,
                            Abbreviation = m.Abbreviation,
                            Id = m.Id
                        };

            return await model.ToListAsync();
        }

        public async Task<List<MstSleModelsDto>> GetModelEnable()
        {
            var model = from m in _mstSleModelsRepository.GetAll().Where(e => e.Status == "Y" && e.MakeId == 1)
                        orderby m.Ordering descending
                        select new MstSleModelsDto
                        {
                            MarketingCode = m.MarketingCode,
                            ProductionCode = m.ProductionCode,
                            Id = m.Id
                        };

            return await model.ToListAsync();
        }

        public async Task<List<MstSleModelsDto>> GetListModelForSalesVehicle(string Status)
        {
            var model = from m in _mstSleModelsRepository.GetAll().WhereIf(!string.IsNullOrWhiteSpace(Status), e => e.Status == Status)
                        orderby m.Id descending
                        select new MstSleModelsDto
                        {
                            Id = m.Id,
                            MarketingCode = m.MarketingCode,
                            ProductionCode = m.ProductionCode,
                            EnName = m.EnName,
                            VnName = m.VnName
                        };

            return await model.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<MstSleMakeCompetitorDto>> GetAllMstSleMakeCompetitor(GetAllMstSleMakeCompetitorInput input)
        {
            var query = from make in _mstSleMakeRepository.GetAll().Where(e => e.Id != 1)
                            .Where(e => input.TextFilter == null || e.Code.Contains(input.TextFilter) || e.Description.Contains(input.TextFilter))
                        select new MstSleMakeCompetitorDto()
                        {
                            Id = make.Id,
                            Code = make.Code,
                            Description = make.Description,
                        };

            var pagedAndFilterMstSleMakeCompetitor = query.PageBy(input);

            var totalCount = await query.CountAsync();

            return new PagedResultDto<MstSleMakeCompetitorDto>(
                totalCount,
                await pagedAndFilterMstSleMakeCompetitor.ToListAsync()
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleModelsForViewDto>> GetAllModelFromOtherMake(GetAllMstSleModelsInput input)
        {
            var filteredMstSleModels = _mstSleModelsRepository.GetAll().Where(e => e.MakeId == input.MakeIdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.MarketingCode.Contains(input.Filter)
                        || e.ProductionCode.Contains(input.Filter) || e.VnName.Contains(input.Filter) || e.EnName.Contains(input.Filter)
                        || e.Description.Contains(input.Filter) || e.Abbreviation.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.AbbreviationFilter), e => e.Abbreviation.Contains(input.AbbreviationFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.MarketingCodeFilter), e => e.MarketingCode.Contains(input.MarketingCodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductionCodeFilter), e => e.ProductionCode.Contains(input.ProductionCodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName.Contains(input.VnNameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName.Contains(input.EnNameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status.Contains(input.StatusFilter))
                        .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleModels = filteredMstSleModels.PageBy(input);

            var mstSleModels = from o in pagedAndFilteredMstSleModels
                               select new GetMstSleModelsForViewDto()
                               {
                                   Abbreviation = o.Abbreviation,
                                   ProductionCode = o.ProductionCode,
                                   MarketingCode = o.MarketingCode,
                                   VnName = o.VnName,
                                   EnName = o.EnName,
                                   Description = o.Description,
                                   Status = o.Status,
                                   Ordering = o.Ordering,
                                   Id = o.Id
                               };

            var totalCount = await filteredMstSleModels.CountAsync();

            return new PagedResultDto<GetMstSleModelsForViewDto>(
                totalCount,
                await mstSleModels.ToListAsync()
            );
        }
    }
}
