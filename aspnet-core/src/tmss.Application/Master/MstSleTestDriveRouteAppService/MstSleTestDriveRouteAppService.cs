﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.TestDriveRoute;

namespace tmss.MstSle.MstSleTestDriveRouteAppService
{
    public class MstSleTestDriveRouteAppService : tmssAppServiceBase, IMstSleTestDriveRouteAppService
    {
        private readonly IRepository<MstSleTestDriveRoute, long> _repository;
        private readonly IRepository<MstGenDealer, long> _mstGenDealerRepo;
        public MstSleTestDriveRouteAppService(
            IRepository<MstSleTestDriveRoute, long> repository,
            IRepository<MstGenDealer, long> mstGenDealerRepo
            )
        {
            _repository = repository;
            _mstGenDealerRepo = mstGenDealerRepo;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleTestDriveRouteDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveRoute_Create)]
        protected virtual async Task Create(CreateOrEditMstSleTestDriveRouteDto input)
        {
            var mstSleTestDriveRouteCount = _repository.GetAll().Where(e => e.Name == input.Name && e.DealerId == input.DealerId).Count();
            if (mstSleTestDriveRouteCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisNameAlreadyExists"));
            }
            else
            {
                var mstSleTestDriveRoute = ObjectMapper.Map<MstSleTestDriveRoute>(input);
                await _repository.InsertAsync(mstSleTestDriveRoute);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveRoute_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleTestDriveRouteDto input)
        {
            var mstSleTestDriveRouteCount = _repository.GetAll().Where(e => e.Name == input.Name && e.Id != input.Id && e.DealerId == input.DealerId).Count();
            if (mstSleTestDriveRouteCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisNameAlreadyExists"));
            }
            else
            {
                var mstSleTestDriveRoute = await _repository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleTestDriveRoute);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveRoute_Search)]
        public async Task<PagedResultDto<MstSleTestDriveRouteListDto>> GetListViewTestDriveRoute(GetAllTestDriveRouteInput input)
        {
            var TenantId = AbpSession.TenantId;
            var filteredTestDriveRoute = _repository.GetAll()
                .WhereIf(input.DealerIdFilter != null, e => e.DealerId == input.DealerIdFilter)
                .WhereIf(TenantId != null, e => e.DealerId == TenantId)
                .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => e.Name.Contains(input.FilterText) || e.Description.Contains(input.FilterText));
            var pagedAndFiltereTestDriveRoute = filteredTestDriveRoute.PageBy(input);
            var ListViewTestDriveRoute = from listViewTestDriveRoute in pagedAndFiltereTestDriveRoute
                                         join resultDealer in _mstGenDealerRepo.GetAll() on listViewTestDriveRoute.DealerId equals resultDealer.Id
                                         select new MstSleTestDriveRouteListDto
                                         {
                                             Name = listViewTestDriveRoute.Name,
                                             Description = listViewTestDriveRoute.Description,
                                             Dealer = resultDealer.Abbreviation,
                                             Type = listViewTestDriveRoute.Type,
                                             Id = listViewTestDriveRoute.Id,
                                         };
            var totalCount = await filteredTestDriveRoute.CountAsync();
            return new PagedResultDto<MstSleTestDriveRouteListDto>(
              totalCount,
              await ListViewTestDriveRoute.ToListAsync()
          );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveRoute_Edit)]
        public async Task<GetMstSleTestDriveRouteForEditOutput> GetMstSleTestDriveRouteForEdit(EntityDto<long> input)
        {
            var mstSleTestDriveRoute = await _repository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleTestDriveRouteForEditOutput { MstSleTestDriveRoute = ObjectMapper.Map<CreateOrEditMstSleTestDriveRouteDto>(mstSleTestDriveRoute) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveRoute_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _repository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _repository.DeleteAsync(result.Id);
        }

        public async Task<List<DealerCacheDto>> GetDealerForTestDrive()
        {
            var TenantId = AbpSession.TenantId;
            var mstGenDealers = from resultDealer in _mstGenDealerRepo.GetAll().WhereIf(TenantId != null, e => e.Id == TenantId)
                                select new DealerCacheDto()
                                {
                                    Abbreviation = resultDealer.Abbreviation,
                                    Id = resultDealer.Id,
                                };
            return await mstGenDealers.ToListAsync();
        }
    }
}
