﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Hotness;

namespace tmss.MstSle.MstSleHotnessAppService
{
    public class MstSleHotnessAppService : tmssAppServiceBase, IMstSleHotnessAppService
    {
        private readonly IRepository<MstSleHotness, long> _MstSleHotnessRepository;

        public MstSleHotnessAppService(
            IRepository<MstSleHotness, long> MstSleHotnessRepository
            )
        {
            _MstSleHotnessRepository = MstSleHotnessRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hotness_Search)]
        public async Task<List<ListMstSleHotnessDto>> GetListHotness()
        {
            var hotness = from h in _MstSleHotnessRepository.GetAll()
                          select new ListMstSleHotnessDto()
                          {
                              Description = h.Description,
                              Code = h.Code,
                              Id = h.Id
                          };

            return await hotness.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hotness_Search)]
        public async Task<PagedResultDto<GetMstSleHotnessForViewDto>> GetAll(GetMstSleHotnessInputDto input)
        {
            var query = from hotness in _MstSleHotnessRepository.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby hotness.Ordering ascending
                        select new GetMstSleHotnessForViewDto()
                        {
                            Id = hotness.Id,
                            Code = hotness.Code,
                            Description = hotness.Description,
                            Vi_Description = hotness.Vi_Description,
                            Ordering = hotness.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredHotness = query.PageBy(input);

            return new PagedResultDto<GetMstSleHotnessForViewDto>(
                totalCount,
                await pagedAndFilteredHotness.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleHotnessDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }
        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hotness_Create)]
        protected virtual async Task Create(CreateOrEditMstSleHotnessDto input)
        {
            var mstSleHotnessCount = _MstSleHotnessRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleHotnessCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleHotness = ObjectMapper.Map<MstSleHotness>(input);
                await _MstSleHotnessRepository.InsertAsync(mstSleHotness);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hotness_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleHotnessDto input)
        {
            var mstSleHotnessCount = _MstSleHotnessRepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleHotnessCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleHotness = await _MstSleHotnessRepository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleHotness);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hotness_Edit)]
        public async Task<GetMstSleHotnessForEditOutput> GetMstSleHotnessForEdit(EntityDto<long> input)
        {
            var mstSleHotness = await _MstSleHotnessRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleHotnessForEditOutput { MstSleHotness = ObjectMapper.Map<CreateOrEditMstSleHotnessDto>(mstSleHotness) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Hotness_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _MstSleHotnessRepository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _MstSleHotnessRepository.DeleteAsync(result.Id);
        }
    }
}
