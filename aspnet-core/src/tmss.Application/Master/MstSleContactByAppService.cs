﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ContactBy;

namespace tmss.MstSle
{
    public class MstSleContactByAppService : tmssAppServiceBase, IMstSleContactByAppService
    {
        private readonly IRepository<MstSleContactBy, long> _mstSleContactByRepo;

        public MstSleContactByAppService(
            IRepository<MstSleContactBy, long> mstSleContactByRepo)
        {
            _mstSleContactByRepo = mstSleContactByRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ContactBy_Search)]
        public async Task<PagedResultDto<MstSleContactByDto>> GetAllContactBy(GetAllMstSleContactByInput input)
        {
            var query = from cb in _mstSleContactByRepo.GetAll()
                    .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText),
                    e => false
                    || e.Code.Contains(input.FilterText)
                    || e.Description.Contains(input.FilterText)
                    || e.Vi_Description.Contains(input.FilterText))
                        orderby cb.Ordering descending
                        select new MstSleContactByDto
                        {
                            Id = cb.Id,
                            Code = cb.Code,
                            Description = cb.Description,
                            Vi_Description = cb.Vi_Description,
                            Ordering = cb.Ordering
                        };
            var pagedAndResult = query.PageBy(input);
            var totalCount = await query.CountAsync();

            return new PagedResultDto<MstSleContactByDto>(
            totalCount,
            await pagedAndResult.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ContactBy_Edit)]
        public async Task<GetMstSleContactByForEditOutputDto> GetContactByForEdit(EntityDto input)
        {
            var result = await (from cb in _mstSleContactByRepo.GetAll().Where(e => e.Id == input.Id)
                                select new GetMstSleContactByForEditOutputDto
                                {
                                    Id = cb.Id,
                                    Code = cb.Code,
                                    Description = cb.Description,
                                    Vi_Description = cb.Vi_Description,
                                    Ordering = cb.Ordering
                                }).FirstOrDefaultAsync();
            return result;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleContactByDto input)
        {
            if (input.Id == null)
                await Create(input);
            else
                await Update(input);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ContactBy_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleContactByDto input)
        {
            var unit = await _mstSleContactByRepo.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, unit);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ContactBy_Create)]
        protected virtual async Task Create(CreateOrEditMstSleContactByDto input)
        {
            var codeCount = _mstSleContactByRepo.GetAll()
                .Where(e => e.Code == input.Code).Count();

            if (codeCount > 0)
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            else
            {
                var contactBy = ObjectMapper.Map<MstSleContactBy>(input);
                await _mstSleContactByRepo.InsertAsync(contactBy);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ContactBy_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleContactByRepo.DeleteAsync(input.Id);
        }
    }
}
