﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.TestDriveStall;

namespace tmss.MstSle.MstSleTestDriveStallAppService
{
    public class MstSleTestDriveStallAppService : tmssAppServiceBase, IMstSleTestDriveStallAppService
    {
        private readonly IRepository<MstSleTestDriveStall, long> _repository;
        private readonly IRepository<MstGenDealer, long> _mstGenDealerRepo;
        public MstSleTestDriveStallAppService(
            IRepository<MstSleTestDriveStall, long> repository,
            IRepository<MstGenDealer, long> mstGenDealerRepo
            )
        {
            _repository = repository;
            _mstGenDealerRepo = mstGenDealerRepo;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleTestDriveStallDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveStall_Create)]
        protected virtual async Task Create(CreateOrEditMstSleTestDriveStallDto input)
        {
            var mstSleTestDriveStalllCount = _repository.GetAll().Where(e => e.Name == input.Name && e.DealerId == input.DealerId).Count();
            if (mstSleTestDriveStalllCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisNameAlreadyExists"));
            }
            else
            {
                var mstSleTestDriveStalll = ObjectMapper.Map<MstSleTestDriveStall>(input);
                await _repository.InsertAsync(mstSleTestDriveStalll);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveStall_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleTestDriveStallDto input)
        {
            var mstSleTestDriveStalllCount = _repository.GetAll().Where(e => e.Name == input.Name && e.Id != input.Id && e.DealerId == input.DealerId).Count();
            if (mstSleTestDriveStalllCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisNameAlreadyExists"));
            }
            else
            {
                var mstSleTestDriveStalll = await _repository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleTestDriveStalll);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveStall_Search)]
        public async Task<PagedResultDto<MstSleTestDriveStallListDto>> GetListViewTestDriveStall(GetAllTestDriveStallInput input)
        {
            var TenantId = AbpSession.TenantId;
            var filteredTestDriveStall = _repository.GetAll()
                 .WhereIf(input.DealerIdFilter != null, e => e.DealerId == input.DealerIdFilter)
                 .WhereIf(TenantId != null, e => e.DealerId == TenantId)
                 .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => e.Name.Contains(input.FilterText) || e.Description.Contains(input.FilterText));
            var pagedAndFiltereTestDriveStall = filteredTestDriveStall.PageBy(input);
            var ListViewTestDriveStall = from listViewTestDriveStall in pagedAndFiltereTestDriveStall
                                         join resultDealer in _mstGenDealerRepo.GetAll() on listViewTestDriveStall.DealerId equals resultDealer.Id
                                         select new MstSleTestDriveStallListDto
                                         {
                                             Name = listViewTestDriveStall.Name,
                                             Description = listViewTestDriveStall.Description,
                                             Dealer = resultDealer.Abbreviation,
                                             Id = listViewTestDriveStall.Id,
                                         };
            var totalCount = await filteredTestDriveStall.CountAsync();
            return new PagedResultDto<MstSleTestDriveStallListDto>(
              totalCount,
              await ListViewTestDriveStall.ToListAsync()
          );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveStall_Edit)]
        public async Task<GetMstSleTestDriveStallForEditOutput> GetMstSleTestDriveStallForEdit(EntityDto<long> input)
        {
            var mstSleTestDriveStall = await _repository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleTestDriveStallForEditOutput { MstSleTestDriveStall = ObjectMapper.Map<CreateOrEditMstSleTestDriveStallDto>(mstSleTestDriveStall) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_TestDriveStall_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _repository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _repository.DeleteAsync(result.Id);
        }
    }
}
