﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.LookUp;
using tmss.MstSle.Dto.ReasonOfNA;

namespace tmss.MstSle
{
    public class MstSleReasonOfNAAppService : tmssAppServiceBase, IMstSleReasonOfNAAppService
    {
        private readonly IRepository<MstSleReasonOfNA, long> _mstSleReasonOfNArepository;
        private readonly IRepository<MstSleLookUp, int> _mstSleLookupsRepository;
        public MstSleReasonOfNAAppService(
            IRepository<MstSleReasonOfNA, long> mstSleReasonOfNArepository,
            IRepository<MstSleLookUp, int> mstSleLookupsRepository
            )
        {
            _mstSleReasonOfNArepository = mstSleReasonOfNArepository;
            _mstSleLookupsRepository = mstSleLookupsRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNA_Search)]
        public async Task<PagedResultDto<GetMstSleReasonOfNAForViewDto>> GetAll(GetReasonOfNAInput input)
        {
            var query = from ReasonOfNA in _mstSleReasonOfNArepository.GetAll()
                        .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby ReasonOfNA.Ordering ascending
                        select new GetMstSleReasonOfNAForViewDto()
                        {
                            Id = ReasonOfNA.Id,
                            Type = ReasonOfNA.Type,
                            Code = ReasonOfNA.Code,
                            Description = ReasonOfNA.Description,
                            Vi_Description = ReasonOfNA.Vi_Description,
                            Ordering = ReasonOfNA.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredMstSleReasonOfNA = query.PageBy(input);

            return new PagedResultDto<GetMstSleReasonOfNAForViewDto>(
                totalCount,
                await pagedAndFilteredMstSleReasonOfNA.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleReasonOfNADto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNA_Create)]
        protected virtual async Task Create(CreateOrEditMstSleReasonOfNADto input)
        {
            var mstSleReasonOfNACount = _mstSleReasonOfNArepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleReasonOfNACount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfNA = ObjectMapper.Map<MstSleReasonOfNA>(input);
                await _mstSleReasonOfNArepository.InsertAsync(mstSleReasonOfNA);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNA_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleReasonOfNADto input)
        {
            var mstSleReasonOfNACount = _mstSleReasonOfNArepository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleReasonOfNACount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfNA = await _mstSleReasonOfNArepository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleReasonOfNA);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNA_Edit)]
        public async Task<GetMstSleReasonOfNAForEditOutput> GetMstSleReasonOfNAForEdit(EntityDto<long> input)
        {
            var mstSleReasonOfNA = await _mstSleReasonOfNArepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleReasonOfNAForEditOutput { MstSleReasonOfNA = ObjectMapper.Map<CreateOrEditMstSleReasonOfNADto>(mstSleReasonOfNA) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfNA_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleReasonOfNArepository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleReasonOfNArepository.DeleteAsync(result.Id);
        }

        public async Task<List<GetLookUpValueDto>> GetLookUpValue(string Code)
        {
            var result = from lookup in _mstSleLookupsRepository.GetAll()
                         .Where(e => e.Code.ToLower() == Code.ToLower())
                         orderby lookup.Ordering descending
                         select new GetLookUpValueDto()
                         {
                             Name = lookup.Name,
                             Id = lookup.Id,
                             Ordering = lookup.Ordering
                         };
            return await result.ToListAsync();
        }
    }
}
