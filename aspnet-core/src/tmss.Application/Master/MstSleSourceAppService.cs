﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.Source;

namespace tmss.MstSle
{
    public class MstSleSourceAppService : tmssAppServiceBase, IMstSleSourceAppService
    {
        private readonly IRepository<MstSleSource, long> _MstSleSourceRepository;

        public MstSleSourceAppService(IRepository<MstSleSource, long> MstSleSourceRepository)
        {
            _MstSleSourceRepository = MstSleSourceRepository;
        }
        public ListResultDto<MstSleSourceListDto> GetListSource(MstSleSourceInput input)
        {
            var listSource = _MstSleSourceRepository.GetAll().OrderBy(s => s.Ordering).ToList();
            return new ListResultDto<MstSleSourceListDto>(ObjectMapper.Map<List<MstSleSourceListDto>>(listSource));
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Source_Search)]
        public async Task<PagedResultDto<MstSleSourceDto>> GetAllMstSleSource(GetAllMstSleSourceInput input)
        {
            var query = from source in _MstSleSourceRepository.GetAll()
                            .WhereIf(!string.IsNullOrWhiteSpace(input.TextFilter), e => false || e.Code.Contains(input.TextFilter) || e.Description.Contains(input.TextFilter) || e.Vi_Description.Contains(input.TextFilter))
                            .OrderBy(e => e.Ordering)
                        select new MstSleSourceDto()
                        {
                            Id = source.Id,
                            Code = source.Code,
                            Description = source.Description,
                            Vi_Description = source.Vi_Description,
                            Ordering = source.Ordering,
                            SourceType = source.SourceType
                        };

            var pagedAndFilteredMstSleSource = query.PageBy(input);
            var totalCount = await query.CountAsync();
            return new PagedResultDto<MstSleSourceDto>(
                    totalCount,
                    await pagedAndFilteredMstSleSource.ToListAsync()
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Source_Edit)]
        public async Task<GetMstSleSourceForEditOutput> GetMstSleSourceForEdit(EntityDto input)
        {
            var query = await (from source in _MstSleSourceRepository.GetAll().Where(e => e.Id == input.Id)
                               select new GetMstSleSourceForEditOutput
                               {
                                   Id = source.Id,
                                   Code = source.Code,
                                   Description = source.Description,
                                   Vi_Description = source.Vi_Description,
                                   Ordering = source.Ordering,
                                   SourceType = source.SourceType
                               }).FirstOrDefaultAsync();
            return query;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleSourceDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Source_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _MstSleSourceRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Source_Create)]
        protected virtual async Task Create(CreateOrEditMstSleSourceDto input)
        {
            var transportNameCount = _MstSleSourceRepository.GetAll().Where(e => e.Code == input.Code).Count();
            if (transportNameCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleSource = ObjectMapper.Map<MstSleSource>(input);

                await _MstSleSourceRepository.InsertAsync(mstSleSource);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Source_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleSourceDto input)
        {
            var mstSleSource = await _MstSleSourceRepository.FirstOrDefaultAsync((long)input.Id);
            ObjectMapper.Map(input, mstSleSource);
        }

    }
}
