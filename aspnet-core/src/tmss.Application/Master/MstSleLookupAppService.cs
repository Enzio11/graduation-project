﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.MstSle;
using tmss.MstSle.Dto.LookUp;

namespace tmss.Master.Sales
{
    public class MstSleLookupAppService : tmssAppServiceBase, IMstSleLookupAppService
    {
        private readonly IRepository<MstSleLookUp> _mstSleLookupRepository;

        public MstSleLookupAppService(IRepository<MstSleLookUp> mstSleLookupRepository)
        {
            _mstSleLookupRepository = mstSleLookupRepository;
        }

        public async Task<PagedResultDto<MstSleLookupDto>> GetAll(GetAllMstSleLookupInput input)
        {
            var filteredMstSleLookup = _mstSleLookupRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.Code.Contains(input.Filter) || e.Name.Contains(input.Filter) || e.Status.Contains(input.Filter) || e.Description.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CodeFilter), e => e.Code.Contains(input.CodeFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.NameFilter), e => e.Name.Contains(input.NameFilter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.DescriptionFilter), e => e.Description.Contains(input.DescriptionFilter));

            var pagedAndFilteredMstSleLookup = filteredMstSleLookup.PageBy(input);

            var mstSleLookup = from o in pagedAndFilteredMstSleLookup
                               orderby o.Id descending
                               select new MstSleLookupDto()
                               {
                                   Code = o.Code,
                                   Name = o.Name,
                                   Status = o.Status,
                                   Description = o.Description,
                                   Ordering = o.Ordering,
                                   Value = o.Value,
                                   Id = o.Id
                               };

            var totalCount = await filteredMstSleLookup.CountAsync();

            return new PagedResultDto<MstSleLookupDto>(
                totalCount,
                await mstSleLookup.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Lookup_Search)]
        public async Task<GetMstSleLookupForEditOutput> GetMstSleLookupForEdit(EntityDto input)
        {
            var mstSleLookup = await _mstSleLookupRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleLookupForEditOutput { MstSleLookup = ObjectMapper.Map<CreateOrEditMstSleLookupDto>(mstSleLookup) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleLookupDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Lookup_Create)]
        protected virtual async Task Create(CreateOrEditMstSleLookupDto input)
        {
            var valueCount = _mstSleLookupRepository.GetAll().Where(e => e.Code == input.Code && e.Value == input.Value).Count();
            if (valueCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleLookup = ObjectMapper.Map<MstSleLookUp>(input);
                await _mstSleLookupRepository.InsertAsync(mstSleLookup);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Lookup_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleLookupDto input)
        {
            var valueCount = _mstSleLookupRepository.GetAll().Where(e => e.Code == input.Code && e.Value == input.Value && e.Id != input.Id).Count();
            if (valueCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleLookup = await _mstSleLookupRepository.FirstOrDefaultAsync((int)input.Id);
                ObjectMapper.Map(input, mstSleLookup);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Lookup_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleLookupRepository.DeleteAsync(input.Id);
        }
        public async Task<List<GetListLookUpDto>> GetListLookUp(string Code)
        {
            var listLookUp = from o in _mstSleLookupRepository.GetAll().Where(e => e.Code == Code && e.Status == "Y")
                             orderby o.Id descending
                             select new GetListLookUpDto()
                             {
                                 Name = o.Name,
                                 Value = o.Value,
                             };
            return await listLookUp.ToListAsync();
        }
    }
}