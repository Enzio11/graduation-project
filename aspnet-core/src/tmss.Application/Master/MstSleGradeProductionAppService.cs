﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.IMstSleGradeProduction.Dto;

namespace tmss.MstSle
{
    public class MstSleGradeProductionAppService : tmssAppServiceBase, IMstSleGradeProductionAppService
    {
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;
        private readonly IRepository<MstSleGradeProduction, long> _mstSleGradeProducesRepository;
        private readonly IRepository<MstSleModel, long> _mstSleModelsRepository;
        private readonly IRepository<MstSleColorGradesProduction, long> _mstSleColorGradesProducesRepository;
        private readonly IRepository<MstSleColors, long> _mstSleColorsRepository;

        public MstSleGradeProductionAppService(
            IRepository<MstSleGradeProduction, long> mstSleGradeProducesRepository,
            IRepository<MstSleModel, long> mstSleModelsRepository,
            IRepository<MstSleColorGradesProduction, long> mstSleColorGradesProducesRepository,
            IRepository<MstSleColors, long> mstSleColorsRepository,
            IRepository<MstSleGrades, long> mstSleGradesRepository
            )
        {
            _mstSleGradeProducesRepository = mstSleGradeProducesRepository;
            _mstSleModelsRepository = mstSleModelsRepository;
            _mstSleColorGradesProducesRepository = mstSleColorGradesProducesRepository;
            _mstSleColorsRepository = mstSleColorsRepository;
            _mstSleGradesRepository = mstSleGradesRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleGradeProducesForViewDto>> GetAll(GetAllMstSleGradeProductionInput input)
        {
            var filteredMstSleGradeProduces = _mstSleGradeProducesRepository.GetAll()
                        .WhereIf(!string.IsNullOrWhiteSpace(input.Filter), e => false || e.ProductionCode.Contains(input.Filter) || e.EnName.Contains(input.Filter) || e.VnName.Contains(input.Filter) || e.IsHasAudio.Contains(input.Filter) || e.CbuCkd.Contains(input.Filter) || e.ShortModel.Contains(input.Filter) || e.Wmi.Contains(input.Filter) || e.Vds.Contains(input.Filter) || e.FullModel.Contains(input.Filter) || e.Barcode.Contains(input.Filter) || e.IsFirmColor.Contains(input.Filter) || e.CaseSize.Contains(input.Filter) || e.BaseLength.Contains(input.Filter) || e.Status.Contains(input.Filter))
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ProductionCodeFilter), e => e.ProductionCode == input.ProductionCodeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName == input.EnNameFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName == input.VnNameFilter)
                        .WhereIf(input.OrderingFilter != null, e => e.Ordering == input.OrderingFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.IsHasAudioFilter), e => e.IsHasAudio == input.IsHasAudioFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CbuCkdFilter), e => e.CbuCkd == input.CbuCkdFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.ShortModelFilter), e => e.ShortModel == input.ShortModelFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.WmiFilter), e => e.Wmi == input.WmiFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.VdsFilter), e => e.Vds == input.VdsFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.FullModelFilter), e => e.FullModel == input.FullModelFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BarcodeFilter), e => e.Barcode == input.BarcodeFilter)
                        .WhereIf(input.FromDateFilter != null, e => e.FromDate >= input.FromDateFilter)
                        .WhereIf(input.ToDateFilter != null, e => e.ToDate >= input.ToDateFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.IsFirmColorFilter), e => e.IsFirmColor == input.IsFirmColorFilter)
                        .WhereIf(input.FrameNoLengthFilter != null, e => e.FrameNoLength >= input.FrameNoLengthFilter)
                        .WhereIf(input.ManYearFilter != null, e => e.ManYear >= input.ManYearFilter)
                        .WhereIf(input.CapacityFilter != null, e => e.Capacity >= input.CapacityFilter)
                        .WhereIf(input.LengthFilter != null, e => e.Length >= input.LengthFilter)
                        .WhereIf(input.WidthFilter != null, e => e.Width >= input.WidthFilter)
                        .WhereIf(input.HeightFilter != null, e => e.Height >= input.HeightFilter)
                        .WhereIf(input.WeightFilter != null, e => e.Weight >= input.WeightFilter)
                        .WhereIf(input.TireSizeFilter != null, e => e.TireSize >= input.TireSizeFilter)
                        .WhereIf(input.PayloadFilter != null, e => e.Payload >= input.PayloadFilter)
                        .WhereIf(input.PullingWeightFilter != null, e => e.PullingWeight >= input.PullingWeightFilter)
                        .WhereIf(input.SeatNoStandingFilter != null, e => e.SeatNoStanding >= input.SeatNoStandingFilter)
                        .WhereIf(input.SeatNoLyingFilter != null, e => e.SeatNoLying >= input.SeatNoLyingFilter)
                        .WhereIf(input.SeatNoFilter != null, e => e.SeatNo >= input.SeatNoFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.CaseSizeFilter), e => e.CaseSize == input.CaseSizeFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.BaseLengthFilter), e => e.BaseLength == input.BaseLengthFilter)
                        .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
                        .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleGradeProduces = filteredMstSleGradeProduces.PageBy(input);

            var mstSleGradeProduces = from production in pagedAndFilteredMstSleGradeProduces
                                      join Grade in _mstSleGradesRepository.GetAll() on production.GradeId equals Grade.Id into GraPro
                                      from GrPr in GraPro.DefaultIfEmpty()
                                      join model in _mstSleModelsRepository.GetAll() on GrPr.ModelId equals model.Id into modelGraPro
                                      from mdGrPr in modelGraPro.DefaultIfEmpty()

                                      select new GetMstSleGradeProducesForViewDto()
                                      {
                                          GradeMarketing = GrPr.MarketingCode,
                                          ModelName = mdGrPr.EnName,
                                          ProductionCode = production.ProductionCode,
                                          Ordering = production.Ordering,
                                          IsHasAudio = production.IsHasAudio,
                                          CbuCkd = production.CbuCkd,
                                          ShortModel = production.ShortModel,
                                          Wmi = production.Wmi,
                                          Vds = production.Vds,
                                          GasolineTypeId = production.GasolineTypeId,
                                          FullModel = production.FullModel,
                                          FromDate = production.FromDate,
                                          ToDate = production.ToDate,
                                          FrameNoLength = production.FrameNoLength,
                                          Status = production.Status,
                                          Id = production.Id,
                                          IsFirmColor = production.IsFirmColor,
                                      };

            var totalCount = await filteredMstSleGradeProduces.CountAsync();

            return new PagedResultDto<GetMstSleGradeProducesForViewDto>(
                totalCount,
                await mstSleGradeProduces.ToListAsync()
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleGradeProducesForViewDto>> GetGradesproduceByGradesId(GetGradesProductionByGradesId input)
        {
            var filteredMstSleGradeProduces = _mstSleGradeProducesRepository.GetAll().Where(r => r.GradeId == input.GradeId).OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleGradeProduces = filteredMstSleGradeProduces.PageBy(input);

            var mstSleGradeProduces = from o in pagedAndFilteredMstSleGradeProduces
                                      select new GetMstSleGradeProducesForViewDto()
                                      {
                                          ProductionCode = o.ProductionCode,
                                          GasolineTypeId = o.GasolineTypeId,
                                          Ordering = o.Ordering,
                                          IsHasAudio = o.IsHasAudio,
                                          CbuCkd = o.CbuCkd,
                                          ShortModel = o.ShortModel,
                                          Wmi = o.Wmi,
                                          Vds = o.Vds,
                                          FullModel = o.FullModel,
                                          FromDate = o.FromDate,
                                          ToDate = o.ToDate,
                                          FrameNoLength = o.FrameNoLength,
                                          Status = o.Status,
                                          Id = o.Id,
                                          IsFirmColor = o.IsFirmColor,

                                      };

            var totalCount = await filteredMstSleGradeProduces.CountAsync();

            return new PagedResultDto<GetMstSleGradeProducesForViewDto>(
                totalCount,
                await mstSleGradeProduces.ToListAsync()
            );

        }

        public async Task<List<GetAllCodeColorByGradesId>> GetColorByGradesId(long Id)
        {
            var mstSleGradeProduces = (from GraPro in _mstSleGradeProducesRepository.GetAll().Where(r => r.GradeId == Id)
                                       join ColorGraPro in _mstSleColorGradesProducesRepository.GetAll() on GraPro.Id equals ColorGraPro.ProduceId
                                       select ColorGraPro.ColorId).ToList();

            var colorlist = from color in _mstSleColorsRepository.GetAll().Where(e => mstSleGradeProduces.Contains(e.Id))
                            select new GetAllCodeColorByGradesId()
                            {
                                Code = color.Code,
                                VnName = color.VnName,
                                EnName = color.EnName,
                                Id = color.Id,
                            };

            var totalCount = await _mstSleGradeProducesRepository.GetAll().Where(r => r.GradeId == Id).CountAsync();

            return await colorlist.ToListAsync();
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Edit)]
        public async Task<GetMstSleGradeProductionForEditOutput> GetMstSleGradeProducesForEdit(EntityDto input)
        {
            var mstSleGradeProduces = await _mstSleGradeProducesRepository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleGradeProductionForEditOutput { MstSleGradeProduces = ObjectMapper.Map<CreateOrEditMstSleGradeProductionDto>(mstSleGradeProduces) };

            return output;
        }

        public async Task CreateOrEdit(CreateOrEditMstSleGradeProductionDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Create)]
        protected virtual async Task Create(CreateOrEditMstSleGradeProductionDto input)
        {
            var mstSleGradeProduces = ObjectMapper.Map<MstSleGradeProduction>(input);
            await _mstSleGradeProducesRepository.InsertAsync(mstSleGradeProduces);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleGradeProductionDto input)
        {
            var mstSleGradeProduces = await _mstSleGradeProducesRepository.FirstOrDefaultAsync((int)input.Id);
            ObjectMapper.Map(input, mstSleGradeProduces);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Delete)]
        public async Task Delete(EntityDto input)
        {
            await _mstSleGradeProducesRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_Model_Grade_Search)]
        public async Task<PagedResultDto<GetMstSleGradeProducesForViewDto>> GetAllGradesProduces(GetAllProductionInput input)
        {
            var filteredMstSleGradeProduces = _mstSleGradeProducesRepository.GetAll()
                .WhereIf(!string.IsNullOrWhiteSpace(input.FilterText), e => false || e.ProductionCode.Contains(input.FilterText)
                || e.EnName.Contains(input.FilterText)
                || e.ShortModel.Contains(input.FilterText)
                || e.FullModel.Contains(input.FilterText)
                || e.CaseSize.Contains(input.FilterText)
                || e.Status.Contains(input.FilterText))
                .WhereIf(!string.IsNullOrWhiteSpace(input.ProductionCodeFilter), e => e.ProductionCode.Contains(input.ProductionCodeFilter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.EnNameFilter), e => e.EnName.Contains(input.EnNameFilter))
                .WhereIf(input.OrderingFilter != null, e => e.Ordering == input.OrderingFilter)
                .WhereIf(input.GradeIdFilter != null, e => e.GradeId == input.GradeIdFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.ShortModelFilter), e => e.ShortModel.Contains(input.ShortModelFilter))
                .WhereIf(!string.IsNullOrWhiteSpace(input.FullModelFilter), e => e.FullModel.Contains(input.FullModelFilter))
                .WhereIf(input.FrameNoLengthFilter != null, e => e.FrameNoLength == input.FrameNoLengthFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.StatusFilter), e => e.Status == input.StatusFilter)
                .WhereIf(!string.IsNullOrWhiteSpace(input.VnNameFilter), e => e.VnName.Contains(input.VnNameFilter))
                .OrderByDescending(e => e.Ordering);

            var pagedAndFilteredMstSleGradeProduces = filteredMstSleGradeProduces.PageBy(input);

            var mstSleGradeProduces = from production in pagedAndFilteredMstSleGradeProduces
                                      join Grade in _mstSleGradesRepository.GetAll() on production.GradeId equals Grade.Id into GraPro
                                      from GrPr in GraPro.DefaultIfEmpty()
                                      join model in _mstSleModelsRepository.GetAll() on GrPr.ModelId equals model.Id into modelGraPro
                                      from mdGrPr in modelGraPro.DefaultIfEmpty()

                                      select new GetMstSleGradeProducesForViewDto()
                                      {
                                          GradeMarketing = GrPr.MarketingCode,
                                          ModelName = mdGrPr.EnName,
                                          ProductionCode = production.ProductionCode,
                                          Ordering = production.Ordering,
                                          IsHasAudio = production.IsHasAudio,
                                          CbuCkd = production.CbuCkd,
                                          ShortModel = production.ShortModel,
                                          Wmi = production.Wmi,
                                          Vds = production.Vds,
                                          GasolineTypeId = production.GasolineTypeId,
                                          FullModel = production.FullModel,
                                          FromDate = production.FromDate,
                                          ToDate = production.ToDate,
                                          FrameNoLength = production.FrameNoLength,
                                          Status = production.Status,
                                          Id = production.Id
                                      };

            var totalResultCount = await filteredMstSleGradeProduces.CountAsync();
            return new PagedResultDto<GetMstSleGradeProducesForViewDto>(
                totalResultCount,
                await mstSleGradeProduces.ToListAsync()
                );
        }

        public async Task<List<GetAllCodeColorByGradesId>> GetListGradeProduces()
        {
            var result = from production in _mstSleGradeProducesRepository.GetAll().Where(e => e.Status == "Y")
                         orderby production.Ordering descending
                         select new GetAllCodeColorByGradesId()
                         {
                             Id = production.Id,
                             ProductionCode = production.ProductionCode,
                             GradeId = (int)production.GradeId
                         };
            return await result.ToListAsync();
        }

        public async Task<List<GetAllCodeColorByGradesId>> GetGradeProductionCode(long? inputId)
        {
            var result = from production in _mstSleGradeProducesRepository.GetAll().WhereIf(inputId != null, e => e.Id != inputId)
                         orderby production.Ordering descending
                         select new GetAllCodeColorByGradesId()
                         {
                             ProductionCode = production.ProductionCode,
                         };
            return await result.ToListAsync();
        }
    }
}
