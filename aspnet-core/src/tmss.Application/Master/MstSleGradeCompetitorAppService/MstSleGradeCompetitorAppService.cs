﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Master;
using tmss.MstSle.IMstSleGradeCompetitor;
using tmss.MstSle.IMstSleGradeCompetitor.Dto;

namespace tmss.MstSle.MstSleGradeCompetitorAppService
{
    public class MstSleGradeCompetitorAppService : tmssAppServiceBase, IMstSleGradeCompetitorAppService
    {
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;
        private readonly IRepository<MstSleModel, long> _mstSleModelRepository;
        private readonly IRepository<MstGenDealer, long> _mstGenDealer;

        public MstSleGradeCompetitorAppService(
            IRepository<MstSleGrades, long> mstSleGradesRepository,
            IRepository<MstSleModel, long> mstSleModelRepository,
            IRepository<MstGenDealer, long> mstGenDealer
            )
        {
            _mstSleGradesRepository = mstSleGradesRepository;
            _mstSleModelRepository = mstSleModelRepository;
            _mstGenDealer = mstGenDealer;
        }

        public async Task<List<MstSleGradeCompetitorDto>> GetListModel()
        {
            var listModel = from model in _mstSleModelRepository.GetAll()
                            select new MstSleGradeCompetitorDto()
                            {
                                ModelId = model.Id,
                                Model = model.ProductionCode
                            };
            return await listModel.ToListAsync();
        }

        public async Task<PagedResultDto<MstSleGradeCompetitorDto>> GetAllGradeCompetitor(GetMstSleGradeCompetitorInput input)
        {
            var query = from grade in _mstSleGradesRepository.GetAll()
                            .Where(e => input.ModelId == e.ModelId)
                            .Where(e => input.TextFilter.Contains(e.MarketingCode) || input.TextFilter.Contains(e.ProductionCode) || input.TextFilter.Contains(e.EnName) || input.TextFilter.Contains(e.VnName) || input.TextFilter.Contains(e.Determiner) || input.TextFilter.Contains(e.ShortModel) || input.TextFilter.Contains(e.Wmi) || input.TextFilter.Contains(e.Vds) || input.TextFilter.Contains(e.FullModel))
                            .OrderBy(e => e.Ordering)
                        join dealer in _mstGenDealer.GetAll()
                        on grade.DealerId equals dealer.Id
                        join model in _mstSleModelRepository.GetAll()
                        on grade.ModelId equals model.Id
                        select new MstSleGradeCompetitorDto()
                        {
                            MarketingCode = grade.MarketingCode,
                            ProductionCode = grade.ProductionCode,
                            EnName = grade.EnName,
                            VnName = grade.VnName,
                            Determiner = grade.Determiner,
                            Status = grade.Status,
                            Ordering = grade.Ordering,
                            IsHasAudio = grade.IsHasAudio,
                            CbuCkd = grade.CbuCkd,
                            ShortModel = grade.ShortModel,
                            Wmi = grade.Wmi,
                            Vds = grade.Vds,
                            GasolineTypeId = grade.GasolineTypeId,
                            ModelId = model.Id,
                            Model = model.ProductionCode,
                            FloormatId = grade.FloormatId,
                            FromDate = grade.FromDate,
                            ToDate = grade.ToDate,
                            FullModel = grade.FullModel,
                            Barcode = grade.Barcode,
                            IsFirmColor = grade.IsFirmColor,
                            PriceAmount = grade.PriceAmount,
                            IsShowDeliveryPlan = grade.IsShowDeliveryPlan,
                            OrderPrice = grade.OrderPrice,
                            DealerId = dealer.Id,
                            Dealer = dealer.Abbreviation
                        };
            var totalCount = await query.CountAsync();
            var pageMstSleGradeCompetitor = query.PageBy(input);
            return new PagedResultDto<MstSleGradeCompetitorDto>(
                totalCount,
                await pageMstSleGradeCompetitor.ToListAsync()
                );
        }
    }
}
