﻿using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Master;
using tmss.MstSle.Dto.Grade;
using tmss.MstSle.Dto.Model;
using tmss.MstSle.IMstSleModel;

namespace tmss.MstSle.MstSleModelAppServiceGrade
{
    public class MstSleModelGradeProductAppService : tmssAppServiceBase, IMstSleModelGradeProdcuctAppService
    {
        private readonly IRepository<MstSleModel, long> _MstSleModelRepo;
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;

        public MstSleModelGradeProductAppService
        (
            IRepository<MstSleModel, long> MstSleModelRepo,
            IRepository<MstSleGrades, long> mstSleGradesRepository
        )
        {
            _MstSleModelRepo = MstSleModelRepo;
            _mstSleGradesRepository = mstSleGradesRepository;
        }

        public async Task<List<ModelListDto>> GetAllModel()
        {
            var model = from m in _MstSleModelRepo.GetAll()
                        select new ModelListDto()
                        {
                            Id = m.Id,
                            MarketingCode = m.MarketingCode
                        };

            return await model.ToListAsync();
        }

        public async Task<List<GradeListDto>> GetAllGradeByModelId(long? Id)
        {
            var grade = from g in _mstSleGradesRepository.GetAll().WhereIf(Id != null, e => e.ModelId == Id)
                        select new GradeListDto()
                        {
                            Id = g.Id,
                            MarketingCode = g.MarketingCode
                        };

            return await grade.ToListAsync();
        }
    }
}
