﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.ReasonOfLost;

namespace tmss.MstSle.MstSleReasonofLostAppService
{
    public class MstSleReasonofLostAppService : tmssAppServiceBase, IMstSleReasonofLostAppService
    {
        private readonly IRepository<MstSleReasonOfLost, long> _mstSleReasonOfLostRepo;

        public MstSleReasonofLostAppService(
            IRepository<MstSleReasonOfLost, long> mstSleReasonOfLostRepo
            )
        {
            _mstSleReasonOfLostRepo = mstSleReasonOfLostRepo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Search)]
        public async Task<PagedResultDto<GetMstSleReasonOfLostForViewDto>> GetAll(GetMstSleReasonOfLostInputDto input)
        {
            var query = from reasonOfLost in _mstSleReasonOfLostRepo.GetAll().Where(e => e.TypeId == input.TypeId)
                             .OrderBy(e => e.Ordering)
                        select new GetMstSleReasonOfLostForViewDto()
                        {
                            Id = reasonOfLost.Id,
                            Code = reasonOfLost.Code,
                            Description = reasonOfLost.Description,
                            Vi_Description = reasonOfLost.Vi_Description,
                            Ordering = reasonOfLost.Ordering,
                            TypeId = reasonOfLost.TypeId,
                        };
            var totalCount = await query.CountAsync();
            var pagedAndFilteredMstReasonOfLost = query.PageBy(input);

            return new PagedResultDto<GetMstSleReasonOfLostForViewDto>(
                totalCount,
                await pagedAndFilteredMstReasonOfLost.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleReasonOfLostDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Create)]
        protected virtual async Task Create(CreateOrEditMstSleReasonOfLostDto input)
        {
            var mstSleReasonOfLostCount = _mstSleReasonOfLostRepo.GetAll().Where(e => e.Code == input.Code && e.TypeId == input.TypeId).Count();
            if (mstSleReasonOfLostCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfLost = ObjectMapper.Map<MstSleReasonOfLost>(input);
                await _mstSleReasonOfLostRepo.InsertAsync(mstSleReasonOfLost);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleReasonOfLostDto input)
        {
            var mstSleReasonOfLostCount = _mstSleReasonOfLostRepo.GetAll().Where(e => e.Code == input.Code && e.TypeId == input.TypeId && e.Id != input.Id).Count();
            if (mstSleReasonOfLostCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleReasonOfLost = await _mstSleReasonOfLostRepo.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleReasonOfLost);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await _mstSleReasonOfLostRepo.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await _mstSleReasonOfLostRepo.DeleteAsync(result.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_ReasonOfLost_Edit)]
        public async Task<GetMstSleReasonOfLostForEditOutput> GetMstSleReasonOfLostForEdit(EntityDto<long> input)
        {
            var mstSleReasonOfLost = await _mstSleReasonOfLostRepo.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleReasonOfLostForEditOutput { mstSleReasonOfLostDto = ObjectMapper.Map<CreateOrEditMstSleReasonOfLostDto>(mstSleReasonOfLost) };

            return output;
        }
    }
}
