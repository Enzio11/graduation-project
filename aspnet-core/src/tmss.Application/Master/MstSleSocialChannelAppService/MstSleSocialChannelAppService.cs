﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Master;
using tmss.MstSle.Dto.SocialChannel;

namespace tmss.MstSle.MstSleSocialChannelAppService
{
    public class MstSleSocialChannelAppService : tmssAppServiceBase, IMstSleSocialChannelAppService
    {
        private readonly IRepository<MstSleSocialChannel, long> repository;

        public MstSleSocialChannelAppService(
            IRepository<MstSleSocialChannel, long> repo
            )
        {
            repository = repo;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SocialChannel_Search)]
        public ListResultDto<MstSleSocialChannelListDto> GetListSocialChannel(MstSleSocialChannelInput input)
        {
            var listSocialChannel = repository
            .GetAll()
            .ToList();

            return new ListResultDto<MstSleSocialChannelListDto>(ObjectMapper.Map<List<MstSleSocialChannelListDto>>(listSocialChannel));
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SocialChannel_Search)]
        public async Task<PagedResultDto<GetMstSleSocialChannelForViewDto>> GetAll(GetMstSleSocialChannelInputDto input)
        {
            var query = from SocialChannel in repository.GetAll()
                            .Where(e => input.filterText == null || e.Code.Contains(input.filterText) || e.Description.Contains(input.filterText) || e.Vi_Description.Contains(input.filterText))
                        orderby SocialChannel.Ordering ascending
                        select new GetMstSleSocialChannelForViewDto()
                        {
                            Id = SocialChannel.Id,
                            Code = SocialChannel.Code,
                            Description = SocialChannel.Description,
                            Vi_Description = SocialChannel.Vi_Description,
                            Ordering = SocialChannel.Ordering,
                        };
            var totalCount = await query.CountAsync();

            var pagedAndFilteredSocialChannel = query.PageBy(input);

            return new PagedResultDto<GetMstSleSocialChannelForViewDto>(
                totalCount,
                await pagedAndFilteredSocialChannel.ToListAsync()
            );
        }

        public async Task CreateOrEdit(CreateOrEditMstSleSocialChannelDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SocialChannel_Create)]
        protected virtual async Task Create(CreateOrEditMstSleSocialChannelDto input)
        {
            var mstSleSocialChannelyCount = repository.GetAll().Where(e => e.Code == input.Code).Count();
            if (mstSleSocialChannelyCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleSocialChannel = ObjectMapper.Map<MstSleSocialChannel>(input);
                await repository.InsertAsync(mstSleSocialChannel);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SocialChannel_Edit)]
        protected virtual async Task Update(CreateOrEditMstSleSocialChannelDto input)
        {
            var mstSleSocialChannelyCount = repository.GetAll().Where(e => e.Code == input.Code && e.Id != input.Id).Count();
            if (mstSleSocialChannelyCount >= 1)
            {
                throw new UserFriendlyException(00, L("ThisCodeAlreadyExists"));
            }
            else
            {
                var mstSleSocialChannel = await repository.FirstOrDefaultAsync((long)input.Id);
                ObjectMapper.Map(input, mstSleSocialChannel);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SocialChannel_Edit)]
        public async Task<GetMstSleSocialChannelForEditOutput> GetMstSleSocialChannelForEdit(EntityDto<long> input)
        {
            var mstSleSocialChannel = await repository.FirstOrDefaultAsync(input.Id);

            var output = new GetMstSleSocialChannelForEditOutput { MstSleSocialChannel = ObjectMapper.Map<CreateOrEditMstSleSocialChannelDto>(mstSleSocialChannel) };

            return output;
        }

        [AbpAuthorize(AppPermissions.Pages_Master_Sales_SocialChannel_Delete)]
        public async Task Delete(EntityDto<long> input)
        {
            var result = await repository.GetAll().FirstOrDefaultAsync(e => e.Id == input.Id);
            await repository.DeleteAsync(result.Id);
        }
    }
}
