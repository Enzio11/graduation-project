﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using tmss.Sales;

namespace tmss.BackgroundJob
{
    public interface ISalesCustomerJob
    {
        public void FrezzeCusAuto(int? tenantId);
    }

    [UnitOfWork]
    public class SalesCustomerJob : tmssAppServiceBase, ISalesCustomerJob
    {
        private readonly IRepository<SalesCustomer, long> _SalesCustomerRepository;
        private readonly IRepository<SalesCustomerTransactionInfoContact, long> _SalesCustomerTransactionInfoContactRepository;
        private readonly IRepository<SalesCustomerTransactionInfo, long> _SalesCustomerTransactionInfoRepository;

        public SalesCustomerJob(IRepository<SalesCustomer, long> salesCustomerRepository,
                                IRepository<SalesCustomerTransactionInfoContact, long> salesCustomerTransactionInfoContactRepository,
                                IRepository<SalesCustomerTransactionInfo, long> salesCustomerTransactionInfoRepository)
        {
            _SalesCustomerRepository = salesCustomerRepository;
            _SalesCustomerTransactionInfoContactRepository = salesCustomerTransactionInfoContactRepository;
            _SalesCustomerTransactionInfoRepository = salesCustomerTransactionInfoRepository;
        }

        public void FrezzeCusAuto(int? tenantId)
        {
            if (!(tenantId is null))
            {
                using (CurrentUnitOfWork.SetTenantId((int?)tenantId))
                {
                    // Xét những khách hàng đã liên hệ (ContactResult = 1), Status Chưa đóng băng (9), Chưa mất khách (8), Chưa ký HĐ (6), Chưa giao xe (7)
                    // KH sẽ bị đóng băng nếu: Ngày liên hệ đầu tiên cách ngày hiện tại 120 ngày HOẶC Ngày liên hệ gần nhất cách ngày hiện tại 30 ngày
                    var customer = (from a in _SalesCustomerRepository.GetAll().Where(c => c.ContactResult == 1 && c.Status != 8 && c.Status != 9 && c.Status != 6 && c.Status != 7).ToList()
                                    select new
                                    {
                                        customerId = a.Id,
                                        contactDate = a.ContactDate,
                                        lastResultContact = _SalesCustomerTransactionInfoContactRepository.GetAll().Where(tc => tc.SalesCustomerId == a.Id).Max(c => c.CurrentDate)
                                    }).ToList()
                                    .Where(c => CompareTwoDateWithNum(DateTime.Now.Date, c.lastResultContact, 30) || CompareTwoDateWithNum(DateTime.Now.Date, c.contactDate, 120))
                                    .Select(c => c.customerId).Distinct()
                                    .ToList();

                    int i = 0;

                    foreach (var cus in customer)
                    {
                        i++;
                        var customers = _SalesCustomerRepository.GetAll().FirstOrDefault(c => c.Id == cus);
                        _SalesCustomerTransactionInfoRepository.Delete(c => c.SalesCustomerId == cus);
                        customers.Status = 9;
                        SalesCustomerTransactionInfo trans = new SalesCustomerTransactionInfo
                        {
                            SalesCustomerId = cus,
                            SalesStageId = 9,
                            ReasonLost = "Đóng băng bởi hệ thống",
                            IsShowRoomVisit = false,
                            IsTestDrive = false,
                            ActivityDate = DateTime.Today.Date,
                            StatusApprove = 1,
                            OnPassiveFreezing = true,
                        };
                        _SalesCustomerTransactionInfoRepository.Insert(trans);
                        _SalesCustomerRepository.Update(customers);
                    }
                    CurrentUnitOfWork.SaveChanges();
                }
            }
        }

        public static bool CompareTwoDateWithNum(DateTime? d1, DateTime? d2, long? num)
        {
            if (d2 == null || d1 == null)
            {
                return false;
            }
            else
            {
                var a = d1.Value.Date;
                var b = d2.Value.Date;
                var c = a.Subtract(b).Days;
                return c > num;
            }
        }

    }
}
