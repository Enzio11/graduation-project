﻿using System.Collections.Generic;
using tmss.DataExporting.Excel.NPOI;
using tmss.Dto;
using tmss.Sales.Customer.Import.AssignmentCustomer.Dto;
using tmss.Storage;

namespace tmss.Sales.Customer.Exporting
{
    public class CustomerFileExcelExporter : NpoiExcelExporterBase, ICustomerFileExcelExporter
    {
        public CustomerFileExcelExporter(ITempFileCacheManager tempFileCacheManager) : base(tempFileCacheManager) { }

        public FileDto ExportNewAssignmentCustomerToFile(List<ImportPotentialCustomerDto> customers)
        {
            var fileName = GetTimeDate() + "_InvalidNewAssignmentCustomerImportList.xlsx";

            return CreateExcelPackageAndSaveToFoler(
                fileName,
                excelPackage =>
                {
                    var sheet = excelPackage.CreateSheet("InvalidNewAssignmentCustomerImport");

                    AddHeader(
                        sheet,
                        "No.",
                        "Date",
                        "Time",
                        "Model",
                        "LeadSource",
                        "WhenCustomerWantToBuy",
                        "Dealer",
                        "FirstName",
                        "PhoneNo",
                        "Email",
                        "Username",
                        "Campaign",

                        L("RefuseReason")
                        );

                    AddObjects(
                        sheet, customers,
                        _ => _.No,
                        _ => _.Date,
                        _ => _.Time,
                        _ => _.Model,
                        _ => _.LeadSource,
                        _ => _.WhenCustomerWantToBuy,
                        _ => _.Dealer,
                        _ => _.FirstName,
                        _ => _.PhoneNumber,
                        _ => _.Email,
                        _ => _.Username,
                        _ => _.Campaign,

                        _ => _.Exception
                        );

                    for (var i = 0; i < 15; i++)
                    {
                        sheet.AutoSizeColumn(i);
                    }
                });
        }

        public static string GetTimeDate()
        {
            string DateTime = System.DateTime.Now.ToString("s").Replace(":", "-");
            return DateTime;
        }
    }
}
