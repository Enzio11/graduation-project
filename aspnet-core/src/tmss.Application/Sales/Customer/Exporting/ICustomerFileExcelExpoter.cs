﻿using Abp.Application.Services;
using System.Collections.Generic;
using tmss.Dto;
using tmss.Sales.Customer.Import.AssignmentCustomer.Dto;

namespace tmss.Sales.Customer.Exporting
{
    public interface ICustomerFileExcelExporter : IApplicationService
    {
        FileDto ExportNewAssignmentCustomerToFile(List<ImportPotentialCustomerDto> customers);
    }
}