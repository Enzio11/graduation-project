﻿using Abp;
using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Localization;
using Abp.Localization.Sources;
using Abp.Threading;
using Abp.UI;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using tmss.Authorization.Users;
using tmss.Dto;
using tmss.Master;
using tmss.Notifications;
using tmss.Sales.Customer.Dto;
using tmss.Sales.Customer.Exporting;
using tmss.Sales.Customer.Import.AssignmentCustomer.Dto;
using tmss.Sales.Import;
using tmss.Storage;

namespace tmss.Sales.Customer.Import.AssignmentCustomer
{
    public class ImportNewAssignmentCustomersToExcelJob : BackgroundJob<ImportPotentialCustomersFromExcelJobArgs>, ITransientDependency
    {
        private readonly ICustomerFileExcelExporter _invalidPotentialCustomerExporter;
        private readonly IAppNotifier _appNotifier;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly ILocalizationSource _localizationSource;
        private readonly IRepository<SalesCustomer, long> _salesCustomerRepository;
        private readonly IRepository<MstGenDealer, long> _mstGenDealerRepository;
        private readonly IRepository<MstSleModel, long> _mstSleModelRepository;
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;
        private readonly IRepository<MstSleGradeProduction, long> _mstSleGradeProductionRepository;
        private readonly IRepository<MstSleColorGradesProduction, long> _mstSleColorGradesProductionRepository;
        private readonly IRepository<SalesCustomerVehicleInfo, long> _salesCustomerVehicleInfoRepository;
        private readonly IRepository<MstSleSource, long> _mstSleSourceRepository;
        private readonly IRepository<MstSleSalesPerson, long> _mstSleSalesPersonRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<MstSleExpectedDelTiming, long> _mstSleExpectedDelTimingRepository;
        private readonly IRepository<SalesInvalidImport, long> _salesInvalidImportRepository;
        private readonly IRepository<SalesJobMonitor, long> _salesJobMonitorRepository;

        public ImportNewAssignmentCustomersToExcelJob(
            ICustomerFileExcelExporter invalidPotentialCustomerExporter,
            IAppNotifier appNotifier,
            IBinaryObjectManager binaryObjectManager,
            ILocalizationManager localizationManager,
            IRepository<SalesCustomer, long> salesCustomerRepository,
            IRepository<MstGenDealer, long> mstGenDealerRepository,
            IRepository<SalesCustomerVehicleInfo, long> salesCustomerVehicleInfoRepository,
            IRepository<MstSleSource, long> mstSleSourceRepository,
            IRepository<User, long> userRepository,
            IRepository<MstSleSalesPerson, long> mstSleSalesPersonRepository,
            IRepository<MstSleExpectedDelTiming, long> mstSleExpectedDelTimingRepository,
            IRepository<SalesInvalidImport, long> salesInvalidImportRepository,
            IRepository<MstSleModel, long> mstSleModelRepository,
            IRepository<MstSleGrades, long> mstSleGradesRepository,
            IRepository<MstSleGradeProduction, long> mstSleGradeProductionRepository,
            IRepository<MstSleColorGradesProduction, long> mstSleColorGradesProductionRepository,
            IRepository<SalesJobMonitor, long> salesJobMonitorRepository
            )
        {
            _invalidPotentialCustomerExporter = invalidPotentialCustomerExporter;
            _appNotifier = appNotifier;
            _binaryObjectManager = binaryObjectManager;
            _salesCustomerRepository = salesCustomerRepository;
            _mstGenDealerRepository = mstGenDealerRepository;
            _mstSleModelRepository = mstSleModelRepository;
            _salesCustomerVehicleInfoRepository = salesCustomerVehicleInfoRepository;
            _mstSleSourceRepository = mstSleSourceRepository;
            _userRepository = userRepository;
            _mstSleSalesPersonRepository = mstSleSalesPersonRepository;
            _mstSleExpectedDelTimingRepository = mstSleExpectedDelTimingRepository;
            _localizationSource = localizationManager.GetSource(tmssConsts.LocalizationSourceName);
            _salesInvalidImportRepository = salesInvalidImportRepository;
            _salesJobMonitorRepository = salesJobMonitorRepository;
            _mstSleGradesRepository = mstSleGradesRepository;
            _mstSleColorGradesProductionRepository = mstSleColorGradesProductionRepository;
            _mstSleGradeProductionRepository = mstSleGradeProductionRepository;
        }

        [UnitOfWork]
        public override void Execute(ImportPotentialCustomersFromExcelJobArgs args)
        {
            var creationTime = DateTime.Now;
            try
            {
                using (CurrentUnitOfWork.SetTenantId(args.TenantId))
                {
                    var customers = GetPotentialCustomerListFromExcelOrNull(args, creationTime);
                    if (customers == null || !customers.Any())
                    {
                        SendInvalidExcelNotification(args, creationTime);
                        return;
                    }

                    AsyncHelper.RunSync(() => CreatePotentialCustomers(args, customers, creationTime));
                }
            }
            catch (Exception)
            {
                SendInvalidExcelNotification(args, creationTime);
                return;
            }
        }

        private List<ImportPotentialCustomerDto> GetPotentialCustomerListFromExcelOrNull(ImportPotentialCustomersFromExcelJobArgs args, DateTime creationTime)
        {
            try
            {
                //Read file from AppBinaryObjects
                var file = AsyncHelper.RunSync(() => _binaryObjectManager.GetOrNullAsync(args.BinaryObjectId));

                //string sFileExtension = Path.GetExtension(file.).ToLower();
                //Convert data from BinaryObject to data list to validate
                byte[] buffer = file.Bytes;
                List<ImportPotentialCustomerDto> rowList = new();
                var customer = new ImportPotentialCustomerDto();
                var exceptionMessage = new StringBuilder();
                ISheet sheet;

                using (var stream = new MemoryStream(buffer))
                {
                    stream.Position = 0;
                    XSSFWorkbook hssfwb = new(stream); //This will read 2007 Excel format  
                    sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    IRow headerRow = sheet.GetRow(3);
                    int cellCount = headerRow.LastCellNum;

                    //Check Header to make sure the import file is correct
                    for (int j = 0; j < cellCount; j++)
                    {
                        ICell cell = headerRow.GetCell(j);
                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString())) continue;
                        {
                            // dtTable.Columns.Add(cell.ToString());
                            if (cell.ToString().ToLower().Trim() != "no." & j == 0)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "No.";
                            }

                            if (cell.ToString().ToLower().Trim() != "date" & j == 1)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Date";
                            }

                            if (cell.ToString().ToLower().Trim() != "time" & j == 2)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Time";
                            }

                            if (cell.ToString().ToLower().Trim() != "model" & j == 3)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Model";
                            }

                            if (cell.ToString().ToLower().Trim() != "lead source" & j == 4)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Lead source";
                            }

                            if (cell.ToString().ToLower().Trim() != "when customer want to buy <month>" & j == 5)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "When customer want to buy";
                            }

                            if (cell.ToString().ToLower().Trim() != "dlr" & j == 6)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "DLR";
                            }

                            if (cell.ToString().ToLower().Trim() != "name" & j == 7)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "name";
                            }

                            if (cell.ToString().ToLower().Trim() != "phone no." & j == 8)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Phone no.";
                            }

                            if (cell.ToString().ToLower().Trim() != "email add." & j == 9)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Email add.";
                            }

                            if (cell.ToString().ToLower().Trim() != "dlr acc" & j == 10)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "DLR Acc";
                            }

                            if (cell.ToString().ToLower().Trim() != "campaign" & j == 11)
                            {
                                customer.Exception += _localizationSource.GetString("IncorrectHeaderColunmName") + ": " + "Campaign";
                            }
                        }
                    }

                    if (customer.Exception != null)
                    {
                        rowList.Add(customer);
                    }
                    else
                    {
                        //Check File content to make sure the data is correct 
                        for (int i = (sheet.FirstRowNum + 4); i <= sheet.LastRowNum; i++)
                        {
                            customer = new ImportPotentialCustomerDto();
                            IRow row = sheet.GetRow(i);
                            if (row == null) continue;
                            if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                            try
                            {
                                customer.No = Convert.ToString(row.GetCell(0));
                                customer.Date = Convert.ToString(row.GetCell(1));
                                customer.Time = Convert.ToString(row.GetCell(2));
                                customer.Model = Convert.ToString(row.GetCell(3));
                                customer.LeadSource = Convert.ToString(row.GetCell(4));
                                customer.WhenCustomerWantToBuy = Convert.ToString(row.GetCell(5));
                                customer.Dealer = Convert.ToString(row.GetCell(6));
                                customer.FirstName = Convert.ToString(row.GetCell(7));
                                customer.PhoneNumber = Convert.ToString(row.GetCell(8));
                                customer.Email = Convert.ToString(row.GetCell(9));
                                customer.Username = Convert.ToString(row.GetCell(10));
                                customer.Campaign = Convert.ToString(row.GetCell(11));
                            }
                            catch (Exception exception)
                            {
                                customer.Exception = exception.Message.ToString();
                            }

                            rowList.Add(customer);
                        }
                    }
                }
                //Return to Data list that including All validattion data
                return rowList;
            }
            catch (Exception)
            {
                SendInvalidExcelNotification(args, creationTime);
                return null;
            }
        }

        private async Task CreatePotentialCustomers(ImportPotentialCustomersFromExcelJobArgs args, List<ImportPotentialCustomerDto> customers, DateTime creationTime)
        {
            List<ImportPotentialCustomerDto> invalidCustomers = new();
            List<SalesCustomer> customersImport = new();

            ImportCount count = new()
            {
                InvalidCount = 0,
                ImportedCount = 0,

                TotalCount = customers.Count,
                CreationTime = creationTime
            };

            var today = DateTime.Now.Date;
            var before10Days = DateTime.Now.AddDays(-10).Date;

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MustHaveTenant))
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
                {
                    foreach (var customer in customers)
                    {
                        if (customer.CanBeImported())
                        {
                            try
                            {
                                if (string.IsNullOrWhiteSpace(customer.Dealer))
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("DealerIsRequried"));
                                }

                                if (string.IsNullOrWhiteSpace(customer.Model))
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("ModelIsRequried"));
                                }

                                if (customer.Date == null)
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("DateIsRequried"));
                                }

                                if (string.IsNullOrWhiteSpace(customer.LeadSource))
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("LeadSourceIsRequried"));
                                }

                                if (string.IsNullOrWhiteSpace(customer.WhenCustomerWantToBuy))
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("WhenCustomerWantToBuyIsRequried"));
                                }

                                if (string.IsNullOrWhiteSpace(customer.FirstName))
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("NameIsRequried"));
                                }

                                if (string.IsNullOrWhiteSpace(customer.PhoneNumber))
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("PhoneNumberIsRequried"));
                                }

                                /*
                                   Tạo mới SalesCustomer
                                */
                                SalesCustomer importCustomer = new();
                                SalesCustomerVehicleInfo newSalesCustomerVehicleInfo = new();

                                if (args.TenantId == null)
                                {
                                    var dealer = await _mstGenDealerRepository.FirstOrDefaultAsync(e => e.Abbreviation.ToLower().Trim() == customer.Dealer.ToLower().Trim());
                                    if (dealer != null)
                                    {
                                        importCustomer.DealerId = dealer.Id;
                                        importCustomer.TenantId = (int)dealer.Id;
                                    }

                                    importCustomer.ImportSource = 1;
                                }

                                if (args.TenantId != null)
                                {
                                    var check = _mstGenDealerRepository.GetAll().Where(e => e.Id == args.TenantId).FirstOrDefault();
                                    if (customer.Dealer.ToLower().Trim() == check.Abbreviation.ToLower().Trim())
                                    {
                                        importCustomer.DealerId = check.Id;
                                        importCustomer.TenantId = (int)check.Id;
                                    }

                                    if (customer.Dealer.ToLower().Trim() != check.Abbreviation.ToLower().Trim())
                                    {
                                        throw new UserFriendlyException(_localizationSource.GetString("CanNotImportToOtherDealer") + ": " + customer.Dealer);
                                    }

                                    importCustomer.ImportSource = 2;
                                }

                                if (importCustomer.TenantId == null)
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("CanNotFindDealer") + ": " + customer.Dealer);
                                }
                                customer.PhoneNumber = customer.PhoneNumber.Replace(" ", "").Replace("-", "").Replace("+84", "0");
                                customer.PhoneNumber = Regex.Replace(customer.PhoneNumber, @"[^0-9]", string.Empty);
                                importCustomer.CustomerTel1Decode = customer.PhoneNumber;
                                var PhoneNumber = Encode(customer.PhoneNumber);

                                importCustomer.CustomerTel1 = PhoneNumber;

                                importCustomer.CustomerFirstName = customer.FirstName;
                                importCustomer.CustomerName = customer.FirstName;

                                var model = await _mstSleModelRepository.FirstOrDefaultAsync(e => e.MarketingCode.ToLower().Trim() == customer.Model.ToLower().Trim());
                                if (model != null)
                                {
                                    newSalesCustomerVehicleInfo.ModelId = model.Id;
                                }

                                if (newSalesCustomerVehicleInfo.ModelId == null)
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("CanNotFindModel") + ": " + customer.Model);
                                }

                                long? grade = _mstSleGradesRepository.GetAll().Where(e => e.ModelId == newSalesCustomerVehicleInfo.ModelId).FirstOrDefault()?.Id;
                                long? gradeProduction = _mstSleGradeProductionRepository.GetAll().Where(e => e.GradeId == grade).FirstOrDefault()?.Id;
                                long? color = _mstSleColorGradesProductionRepository.GetAll().Where(e => e.ProduceId == gradeProduction).FirstOrDefault()?.ColorId;

                                newSalesCustomerVehicleInfo.GradeId = grade;
                                newSalesCustomerVehicleInfo.ColorId = color;

                                var checkDuplicateInCustomer = _salesCustomerRepository.GetAll().Where(e => e.CustomerName == importCustomer.CustomerName && e.CustomerTel1 == importCustomer.CustomerTel1 && e.TenantId == importCustomer.TenantId)
                                                                                                .Where(e => e.CreationTime.Date >= before10Days)
                                                                                                .Where(e => e.CreationTime.Date <= today)
                                                                                                .ToList();
                                if (checkDuplicateInCustomer.Count > 0)
                                {
                                    foreach (var cus in checkDuplicateInCustomer)
                                    {
                                        /*
                                            Key sẽ bao gồm:
                                            Tên KH + số đt + xe quan tâm + đại lý
                                            VÀ
                                            Nguồn import ( TMV/DLR/TVBH )
                                            VÀ
                                            Khoảng thời gian: trong vòng 10 ngày so với ngày imported (creationTime)
                                            Nếu thuộc KEY trên sẽ chặn ko cho vào
                                        */
                                        var modelDuplicate = _salesCustomerVehicleInfoRepository.GetAll().Where(e => e.SalesCustomerId == cus.Id && e.ModelId == newSalesCustomerVehicleInfo.ModelId && e.VehicleType == 1).FirstOrDefault();

                                        if (modelDuplicate != null)
                                        {
                                            throw new UserFriendlyException(_localizationSource.GetString("SalesCustomerThisCustomerIsDuplicate") + ": " + importCustomer.CustomerName);
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(customer.Username))
                                {
                                    var users = _userRepository.GetAll().Where(e => e.TenantId == importCustomer.TenantId && e.UserName == customer.Username).FirstOrDefault();
                                    if (users == null)
                                    {
                                        throw new UserFriendlyException(_localizationSource.GetString("CanNotFindUsername") + ": " + customer.Username);
                                    }

                                    if (users != null)
                                    {
                                        importCustomer.CreatorUserId = users.Id;
                                        var assignPerson = _mstSleSalesPersonRepository.GetAll().Where(e => e.UserId == args.User.UserId).FirstOrDefault();
                                        var salesPerson = _mstSleSalesPersonRepository.GetAll().Where(e => e.UserId == users.Id).FirstOrDefault();
                                        if (salesPerson == null)
                                        {
                                            throw new UserFriendlyException(_localizationSource.GetString("ThisUserIsNotASalesPerson"));
                                        }

                                        if (salesPerson != null)
                                        {
                                            importCustomer.AssignPersonId = assignPerson.Id;
                                            importCustomer.SalesPersonId = salesPerson.Id;
                                            importCustomer.AssignDate = DateTime.Now;
                                            importCustomer.CreatorUserId = users.Id;
                                        }
                                    }
                                }

                                if (string.IsNullOrWhiteSpace(customer.Username))
                                {
                                    var salesPerson = _mstSleSalesPersonRepository.GetAll().Where(e => e.TenantId == importCustomer.DealerId && e.IsReceiveAssignmentList == true).FirstOrDefault();
                                    if (salesPerson == null)
                                    {
                                        throw new UserFriendlyException(_localizationSource.GetString("CanNotFindRepresentatives"));
                                    }

                                    if (salesPerson != null)
                                    {
                                        importCustomer.CreatorUserId = salesPerson.UserId;
                                        importCustomer.AssignPersonId = salesPerson.Id;
                                    }
                                }

                                var source = await _mstSleSourceRepository.FirstOrDefaultAsync(e => e.Description.ToLower().Trim() == customer.LeadSource.ToLower().Trim() || e.Vi_Description.ToLower().Trim() == customer.LeadSource.ToLower().Trim());
                                if (source != null)
                                {
                                    if (args.TenantId == null && source.Description.Substring(0, 3) != "TMV")
                                    {
                                        // Khi TMV import, thì sẽ chỉ cho chọn source của TMV.
                                        throw new UserFriendlyException(_localizationSource.GetString("CanNotImportOtherSourceTMV") + ": " + customer.LeadSource);
                                    }

                                    if (args.TenantId != null && source.Description.Substring(0, 3) != "DLR")
                                    {
                                        // Khi DLR import, thì sẽ chỉ cho chọn source của DLR.
                                        throw new UserFriendlyException(_localizationSource.GetString("CanNotImportOtherSourceTMV") + ": " + customer.LeadSource);
                                    }

                                    importCustomer.SourceId = source.Id;
                                }

                                if (importCustomer.SourceId == null)
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("CanNotFindSource") + ": " + customer.LeadSource);
                                }

                                var expectedDelTiming = await _mstSleExpectedDelTimingRepository.FirstOrDefaultAsync(e => e.Description.ToLower().Trim() == customer.WhenCustomerWantToBuy.ToLower().Trim() || e.Vi_Description.ToLower().Trim() == customer.WhenCustomerWantToBuy.ToLower().Trim());
                                if (expectedDelTiming != null)
                                {
                                    importCustomer.HotnessId = expectedDelTiming.HotnessId;
                                    importCustomer.ExpectedDelTimingId = expectedDelTiming.Id;
                                }

                                if (importCustomer.ExpectedDelTimingId == null)
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("CanNotFindExpectedDelTiming") + ": " + customer.WhenCustomerWantToBuy);
                                }

                                importCustomer.Email = customer.Email;

                                if (customer.Time != null)
                                {
                                    var splitedTime = customer.Time.Split(new char[] { ':' });

                                    var hour = long.Parse(splitedTime[0]);
                                    var minute = long.Parse(splitedTime[1]);
                                    importCustomer.Time = hour * 60 + minute * 3600;
                                }
                                else
                                {
                                    throw new UserFriendlyException(_localizationSource.GetString("CanNotFindTime"));
                                }
                                var Date = DateTime.ParseExact(customer.Date, "dd/MM/yyyy", null);
                                importCustomer.Date = Date;
                                importCustomer.CustomerTypeId = 1;
                                importCustomer.Status = 0;
                                importCustomer.Campaign = customer.Campaign;

                                var DLR = customer.Dealer;
                                DateTime currentDay = DateTime.Today;
                                string now = currentDay.ToString("yyyyMMdd");
                                var customerCount = (_salesCustomerRepository.GetAll().Where(c => c.CreationTime.Date == currentDay && c.TenantId == importCustomer.TenantId).Count() + 1);
                                importCustomer.No = DLR + now + customerCount.ToString("D6");

                                //lấy về Id của bảng SalesCustomer và insert vào bảng SalesCustomerVehicleInfo
                                long getIdCustomer = await _salesCustomerRepository.InsertAndGetIdAsync(importCustomer);

                                newSalesCustomerVehicleInfo.SalesCustomerId = getIdCustomer;
                                newSalesCustomerVehicleInfo.IsFavourite = true;
                                newSalesCustomerVehicleInfo.VehicleType = 1;
                                newSalesCustomerVehicleInfo.CreatorUserId = args.User.UserId;
                                newSalesCustomerVehicleInfo.TenantId = importCustomer.TenantId;

                                await _salesCustomerVehicleInfoRepository.InsertAsync(newSalesCustomerVehicleInfo);

                                customersImport.Add(importCustomer);
                            }
                            catch (UserFriendlyException exception)
                            {
                                customer.Exception = exception.Message;
                                invalidCustomers.Add(customer);
                            }
                            catch (Exception exception)
                            {
                                customer.Exception = exception.Message.ToString();
                                invalidCustomers.Add(customer);
                            }
                        }
                        else
                        {
                            invalidCustomers.Add(customer);
                        }
                    }
                }
            }

            count.InvalidCount = invalidCustomers.Count;
            count.ImportedCount = customersImport.Count;

            AsyncHelper.RunSync(() => ProcessImportPotentialCustomersResultAsync(args, invalidCustomers, count));
        }

        private static string Encode(string Name)
        {
            string inputString = Name;
            var valueBytes = Encoding.UTF8.GetBytes(inputString);
            return Convert.ToBase64String(valueBytes);
        }

        private async Task ProcessImportPotentialCustomersResultAsync(ImportPotentialCustomersFromExcelJobArgs args, List<ImportPotentialCustomerDto> invalidCustomers, ImportCount count)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                if (invalidCustomers.Any())
                {
                    var file = _invalidPotentialCustomerExporter.ExportNewAssignmentCustomerToFile(invalidCustomers);

                    SalesInvalidImport invalidImport = new()
                    {
                        Type = "NewAssignmentCustomer",
                        TotalCount = count.TotalCount,
                        InvalidCount = count.InvalidCount,
                        ImportedCount = count.ImportedCount,
                        CreatorUserId = args.User.UserId,
                        UserId = (int)args.User.UserId,
                        TenantId = args.TenantId,
                        CreationTime = count.CreationTime
                    };

                    if (count.TotalCount == count.ImportedCount)
                    {
                        invalidImport.Status = 1; // Success
                    }

                    if (count.TotalCount == count.InvalidCount)
                    {
                        invalidImport.Status = 2; // Failed
                    }

                    if (count.ImportedCount < count.TotalCount && count.ImportedCount > 0)
                    {
                        invalidImport.Status = 3; // Some Invalid
                    }

                    invalidImport.Source = file.FileName;

                    await _salesInvalidImportRepository.InsertAsync(invalidImport);

                    _salesJobMonitorRepository.Delete(e => e.Id == (long)args.JobMonitorId);

                    await _appNotifier.SomeUsersCouldntBeImported(args.User, file.FileToken, file.FileType, file.FileName);
                }
                else
                {
                    SalesInvalidImport invalidImport = new()
                    {
                        Type = "NewAssignmentCustomer",
                        TotalCount = count.TotalCount,
                        InvalidCount = count.InvalidCount,
                        ImportedCount = count.ImportedCount,
                        CreatorUserId = args.User.UserId,
                        UserId = (int)args.User.UserId,
                        TenantId = args.TenantId,
                        CreationTime = count.CreationTime
                    };

                    if (invalidImport.TotalCount == invalidImport.ImportedCount)
                    {
                        invalidImport.Status = 1; // Success
                    }
                    await _salesInvalidImportRepository.InsertAsync(invalidImport);

                    _salesJobMonitorRepository.Delete(e => e.Id == (long)args.JobMonitorId);
                    await _appNotifier.SendMessageAsync(
                        args.User,
                        _localizationSource.GetString("AllDataSuccessfullyImportedFromExcel"),
                        Abp.Notifications.NotificationSeverity.Success);
                }
            }
        }

        private void SendInvalidExcelNotification(ImportPotentialCustomersFromExcelJobArgs args, DateTime creationTime)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                SalesInvalidImport invalidImport = new()
                {
                    Type = "NewAssignmentCustomer",
                    TotalCount = 0,
                    InvalidCount = 0,
                    ImportedCount = 0,
                    CreatorUserId = args.User.UserId,
                    UserId = (int)args.User.UserId,
                    TenantId = args.TenantId,
                    CreationTime = creationTime
                };

                if (invalidImport.TotalCount == invalidImport.ImportedCount)
                {
                    invalidImport.Status = 3; // Failed
                }
                _salesInvalidImportRepository.Insert(invalidImport);

                _salesJobMonitorRepository.Delete(e => e.Id == (long)args.JobMonitorId);

                AsyncHelper.RunSync(() => _appNotifier.SendMessageAsync(
                    args.User,
                    _localizationSource.GetString("FileCantBeConvertedToList"),
                    Abp.Notifications.NotificationSeverity.Warn)
                );
            }
        }
    }
}
