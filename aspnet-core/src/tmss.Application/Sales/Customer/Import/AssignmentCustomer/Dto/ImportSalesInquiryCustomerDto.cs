﻿using System;

namespace tmss.Sales.Customer.Import.AssignmentCustomer.Dto
{
    public class ImportSalesInquiryCustomerDto
    {
        public DateTime Date { get; set; }
        public long? Time { get; set; }
        public long? ModelId { get; set; }
        public long? LeadSource { get; set; }
        public long? HotnessId { get; set; }
        public long? DealerId { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CustomerName { get; set; }
        public int? TenantId { get; set; }
        public long? CreatorUserId { get; set; }
        public long? SalesPersonId { get; set; }
        public string Username { get; set; }
        public int? ImportSource { set; get; }
    }
}
