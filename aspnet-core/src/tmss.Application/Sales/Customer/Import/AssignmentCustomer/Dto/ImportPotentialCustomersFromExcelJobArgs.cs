﻿using Abp;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class ImportPotentialCustomersFromExcelJobArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }
        public long? JobMonitorId { get; set; }
    }
}
