﻿namespace tmss.Sales.Customer.Import.AssignmentCustomer.Dto
{
    public class ImportPotentialCustomerDto
    {
        public string No { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Model { get; set; }
        public string LeadSource { get; set; }
        public string WhenCustomerWantToBuy { get; set; }
        public string Dealer { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Campaign { get; set; }

        public string Exception { get; set; }

        public bool CanBeImported()
        {
            return string.IsNullOrEmpty(Exception);
        }
    }
}
