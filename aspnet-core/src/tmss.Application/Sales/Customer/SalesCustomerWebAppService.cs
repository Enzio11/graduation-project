﻿using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Authorization.Users;
using tmss.Master;
using tmss.MstSle.Color;
using tmss.MstSle.IMstSleGenDealer.Dto;
using tmss.MstSle.IMstSleGrade.Dto;
using tmss.MstSle.IMstSleModel.Dto;
using tmss.MstSle.IMtSleColor.Dto;
using tmss.MstSle.Make;
using tmss.MultiTenancy;
using tmss.Notifications;
using tmss.Sales.Customer.Dto;
using tmss.Sales.Customer.Dto.Api;
using tmss.Sales.SalesPerson.Dtos;

namespace tmss.Sales.Customer
{
    public class SalesCustomerWebAppService : tmssAppServiceBase
    {
        #region Repository
        private readonly IAppNotifier _appNotifier;
        private readonly IRepository<SalesCustomer, long> _customerRepository;
        private readonly IRepository<SalesCustomerTransactionInfo, long> _customerTransactionInfoRepository;
        private readonly IRepository<SalesCustomerVehicleInfo, long> _customerVehicleInfoRepository;
        private readonly IRepository<SalesCustomerTransactionInfoContact, long> _customerTransactionInfoContactRepository;
        private readonly IRepository<SalesCustomerHotness, long> _customerHotnessRepository;
        private readonly IRepository<SalesCustomerManagerComment, long> _customerManagerCommentRepository;
        private readonly IRepository<MstSleSalesPerson, long> _salesPersonRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<User, long> _userRepository;

        private readonly IRepository<MstSlePurpose, long> _mstSlePurposeRepository;
        private readonly IRepository<MstSleVehiclePrice, long> _mstSleVehiclePriceRepository;
        private readonly IRepository<MstSleSalesStage, long> _mstSleSalesStageRepository;
        private readonly IRepository<MstSleHotness, long> _mstSleHotnessRepository;
        private readonly IRepository<MstSleExpectedDelTiming, long> _mstSleExpectedDelTimingRepository;
        private readonly IRepository<MstGenDealer, long> _mstGenDealerRepository;
        private readonly IRepository<Tenant, int> _tenantRepository;
        private readonly IRepository<MstSleSource, long> _mstSleSourceRepository;
        private readonly IRepository<MstSleModel, long> _mstSleModelsRepository;
        private readonly IRepository<MstSleGrades, long> _mstSleGradesRepository;
        private readonly IRepository<MstSleColors, long> _mstSleColorsRepository;
        private readonly IRepository<MstSleFAR, long> _mstSleFARRepository;
        private readonly IRepository<MstSleGradeProduction, long> _mstSleGradeProductionRepository;
        private readonly IRepository<MstSleContactBy, long> _mstSleContactByRepository;
        private readonly IRepository<MstSleMake, long> _mstSleMakeRepository;
        private readonly IRepository<MstSleColorGradesProduction, long> _mstSleColorGradesProductionRepository;
        private readonly IRepository<MstSleReasonOfNC, long> _mstSleReasonOfNCRepository;
        private readonly IRepository<MstSleReasonOfFreeze, long> _mstSleReasonOfFreezeRepository;
        private readonly IRepository<MstSleHobby, long> _mstSleHobbyRepository;
        private readonly IRepository<MstSleSourceOfInfo, long> _mstSleSourceOfInfoRepository;
        private readonly IRepository<MstSleOccupation, long> _mstSleOccupationRepository;

        public SalesCustomerWebAppService(
            IAppNotifier appNotifier,
            IRepository<SalesCustomer, long> customerRepository,
            IRepository<SalesCustomerTransactionInfo, long> customerTransactionInfoRepository,
            IRepository<SalesCustomerVehicleInfo, long> customerVehicleInfoRepository,
            IRepository<SalesCustomerTransactionInfoContact, long> customerTransactionInfoContactRepository,
            IRepository<SalesCustomerHotness, long> customerHotnessRepository,
            IRepository<SalesCustomerManagerComment, long> customerManagerCommentRepository,
            IRepository<MstSleSalesPerson, long> salesPersonRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<User, long> userRepository,

            IRepository<MstSlePurpose, long> mstSlePurposeRepository,
            IRepository<MstSleVehiclePrice, long> mstSleVehiclePriceRepository,
            IRepository<MstSleSalesStage, long> mstSleSalesStageRepository,
            IRepository<MstSleExpectedDelTiming, long> mstSleExpectedDelTimingRepository,
            IRepository<MstSleHotness, long> mstSleHotnessRepository,
            IRepository<MstGenDealer, long> mstGenDealerRepository,
            IRepository<MstSleSource, long> mstSleSourceRepository,
            IRepository<MstSleModel, long> mstSleModelsRepository,
            IRepository<MstSleGrades, long> mstSleGradesRepository,
            IRepository<MstSleColors, long> mstSleColorsRepository,
            IRepository<MstSleFAR, long> mstSleFARRepository,
            IRepository<MstSleGradeProduction, long> mstSleGradeProductionRepository,
            IRepository<MstSleContactBy, long> mstSleContactByRepository,
            IRepository<Tenant, int> tenantRepository,
            IRepository<MstSleMake, long> mstSleMakeRepository,
            IRepository<MstSleColorGradesProduction, long> mstSleColorGradesProductionRepository,
            IRepository<MstSleReasonOfNC, long> mstSleReasonOfNCRepository,
            IRepository<MstSleReasonOfFreeze, long> mstSleReasonOfFreezeRepository,
            IRepository<MstSleHobby, long> mstSleHobbyRepository,
            IRepository<MstSleSourceOfInfo, long> mstSleSourceOfInfoRepository,
            IRepository<MstSleOccupation, long> mstSleOccupationRepository
        )
        {
            _appNotifier = appNotifier;
            _customerRepository = customerRepository;
            _customerTransactionInfoRepository = customerTransactionInfoRepository;
            _customerVehicleInfoRepository = customerVehicleInfoRepository;
            _customerTransactionInfoContactRepository = customerTransactionInfoContactRepository;
            _customerHotnessRepository = customerHotnessRepository;
            _customerManagerCommentRepository = customerManagerCommentRepository;
            _salesPersonRepository = salesPersonRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _userRepository = userRepository;

            _mstSlePurposeRepository = mstSlePurposeRepository;
            _mstSleVehiclePriceRepository = mstSleVehiclePriceRepository;
            _mstSleSalesStageRepository = mstSleSalesStageRepository;
            _mstSleExpectedDelTimingRepository = mstSleExpectedDelTimingRepository;
            _mstSleHotnessRepository = mstSleHotnessRepository;
            _mstGenDealerRepository = mstGenDealerRepository;
            _mstSleSourceRepository = mstSleSourceRepository;
            _mstSleModelsRepository = mstSleModelsRepository;
            _mstSleGradesRepository = mstSleGradesRepository;
            _mstSleColorsRepository = mstSleColorsRepository;
            _mstSleFARRepository = mstSleFARRepository;
            _mstSleGradeProductionRepository = mstSleGradeProductionRepository;
            _mstSleContactByRepository = mstSleContactByRepository;
            _tenantRepository = tenantRepository;
            _mstSleMakeRepository = mstSleMakeRepository;
            _mstSleColorGradesProductionRepository = mstSleColorGradesProductionRepository;
            _mstSleReasonOfNCRepository = mstSleReasonOfNCRepository;
            _mstSleReasonOfFreezeRepository = mstSleReasonOfFreezeRepository;
            _mstSleHobbyRepository = mstSleHobbyRepository;
            _mstSleSourceOfInfoRepository = mstSleSourceOfInfoRepository;
            _mstSleOccupationRepository = mstSleOccupationRepository;
        }
        #endregion

        #region Mã Hóa
        // Mã hóa số điện thoại
        private static string Encode(string Name)
        {
            string inputString = Name;
            var valueBytes = Encoding.UTF8.GetBytes(inputString);
            return Convert.ToBase64String(valueBytes);
        }

        // Giải mã số điện thoại
        private static string DecodeAndGet(string Name)
        {
            try
            {
                var base64EncodedBytes = Convert.FromBase64String(Name);
                return Encoding.UTF8.GetString(base64EncodedBytes);
            }
            catch
            {
                if (Name == null)
                {
                    return "";
                }
                return Name;
            }
        }

        // Tạo mã KH
        private string CreateCustomerNo()
        {
            int? currentTenantId = AbpSession.TenantId;
            string customerNo = "";

            var tenantName = TenantManager.Tenants.FirstOrDefault(t => t.Id == currentTenantId).TenancyName;
            DateTime currentDay = DateTime.Today;
            string now = currentDay.ToString("yyyyMMdd");
            long customerCount = 0;

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
            {
                customerCount = (_customerRepository.GetAll().Where(c => c.CreationTime.Date == currentDay && c.TenantId == currentTenantId).Count() + 1);
            }

            customerNo = $"{tenantName}{now}{customerCount:D6}";
            return customerNo;
        }
        #endregion

        #region Master Data
        public async Task<CustomerCarAttetionMasterDto> GetCustomerDetailMasterData(long? MakeId)
        {
            var result = new CustomerCarAttetionMasterDto()
            {
                Makes = await GetMakeForSalesCustomer(),
                Models = await GetModelForSalesCustomer(MakeId),
                Grades = await GetGradeForSalesCustomer(MakeId),
                Fars = await GetFARForSalesCustomer(),
                Colors = GetExternalColorByGradeForCustomer(),
                Purposes = await GetPurposeForSalesCustomer(),
                Hotnesses = await GetHotnessForSalesCustomer(),
                Hobbies = await GetHobbyForSalesCustomer(),
                Sources = await GetSourceForSalesCustomer(),
                SourceOfInfos = await GetSourceOfInfoForSalesCustomer(),
                Occupations = await GetOccupationForSalesCustomer(),
                ContactBy = await GetContactByForSalesCustomer(),
                SalesStages = await GetSalesStageForSalesCustomer(),
                ExpectedDelTimings = await GetExpectedDelTimingForSalesCustomer()
            };

            return result;
        }

        public async Task<AssignmentCustomerMasterDataDto> GetAssignmentCustomerMasterData()
        {
            AssignmentCustomerMasterDataDto result = new()
            {
                Models = await GetModelForSalesCustomer(1),
                Hotnesses = await GetHotnessForSalesCustomer(),
                Sources = await GetSourceForSalesCustomer(),
                SalesStages = await GetSalesStageForSalesCustomer(),
                SalesPersons = await GetListSalesPersonName(""),
                AssignPersons = await GetListAssignPerson(),
                Dealers = await GetDealerForSalesCustomer(),
                ExpectedDelTimings = await GetExpectedDelTimingForSalesCustomer()
            };

            return result;
        }

        public async Task<List<ListExpectedDelTimingForSalesCustomer>> GetExpectedDelTimingForSalesCustomer()
        {
            var expectedDelTimings = from expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().OrderByDescending(e => e.Ordering)
                                     select new ListExpectedDelTimingForSalesCustomer()
                                     {
                                         Id = expectedDelTiming.Id,
                                         Description = expectedDelTiming.Vi_Description,
                                         HotnessId = expectedDelTiming.HotnessId
                                     };
            return await expectedDelTimings.ToListAsync();
        }

        private async Task<List<GetMakeForSalesCustomerDto>> GetOccupationForSalesCustomer()
        {
            var occupations = from sourceOfInfo in _mstSleOccupationRepository.GetAll().OrderByDescending(e => e.Ordering)
                              select new GetMakeForSalesCustomerDto()
                              {
                                  Id = sourceOfInfo.Id,
                                  Description = sourceOfInfo.Vi_Description
                              };
            return await occupations.ToListAsync();
        }

        private async Task<List<GetMakeForSalesCustomerDto>> GetSourceOfInfoForSalesCustomer()
        {
            var sourceOfInfos = from sourceOfInfo in _mstSleSourceOfInfoRepository.GetAll().OrderByDescending(e => e.Ordering)
                                select new GetMakeForSalesCustomerDto()
                                {
                                    Id = sourceOfInfo.Id,
                                    Description = sourceOfInfo.Vi_Description
                                };
            return await sourceOfInfos.ToListAsync();
        }

        private async Task<List<GetMakeForSalesCustomerDto>> GetHobbyForSalesCustomer()
        {
            var hobbies = from hobby in _mstSleHobbyRepository.GetAll().OrderByDescending(e => e.Ordering)
                          select new GetMakeForSalesCustomerDto()
                          {
                              Id = hobby.Id,
                              Description = hobby.Vi_Description
                          };
            return await hobbies.ToListAsync();
        }

        private async Task<List<GetMakeForSalesCustomerDto>> GetMakeForSalesCustomer()
        {
            var makes = from make in _mstSleMakeRepository.GetAll().OrderByDescending(e => e.Ordering)
                        select new GetMakeForSalesCustomerDto()
                        {
                            Id = make.Id,
                            Description = make.Description
                        };
            return await makes.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetReasonOfFreezeForSalesCustomer()
        {
            var freezes = from freeze in _mstSleReasonOfFreezeRepository.GetAll().OrderByDescending(e => e.Ordering)
                          select new GetMakeForSalesCustomerDto()
                          {
                              Id = freeze.Id,
                              Description = freeze.ViDescription
                          };
            return await freezes.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetHotnessForSalesCustomer()
        {
            var hotnesses = from hotness in _mstSleHotnessRepository.GetAll().OrderByDescending(e => e.Ordering)
                            select new GetMakeForSalesCustomerDto()
                            {
                                Id = hotness.Id,
                                Description = hotness.Vi_Description
                            };
            return await hotnesses.ToListAsync();
        }

        public async Task<List<GetModelForSalesCustomerDto>> GetModelForSalesCustomer(long? MakeId)
        {
            var models = from model in _mstSleModelsRepository.GetAll().Where(e => MakeId == null || e.MakeId == MakeId).OrderByDescending(e => e.Ordering)
                         select new GetModelForSalesCustomerDto()
                         {
                             Id = model.Id,
                             MarketingCode = model.MarketingCode,
                             MakeId = model.MakeId
                         };
            return await models.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetSourceForSalesCustomer()
        {
            var sources = from source in _mstSleSourceRepository.GetAll().OrderByDescending(e => e.Ordering)
                          select new GetMakeForSalesCustomerDto()
                          {
                              Id = source.Id,
                              Description = source.Vi_Description
                          };
            return await sources.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetFARForSalesCustomer()
        {
            var FARs = from FAR in _mstSleFARRepository.GetAll().OrderByDescending(e => e.Ordering)
                       select new GetMakeForSalesCustomerDto()
                       {
                           Id = FAR.Id,
                           Description = FAR.Vi_Description
                       };
            return await FARs.ToListAsync();
        }

        public async Task<List<GetMstGenDealerForDropdownDto>> GetDealerForSalesCustomer()
        {
            var dealers = from dealer in _mstGenDealerRepository.GetAll().OrderByDescending(e => e.Ordering)
                          select new GetMstGenDealerForDropdownDto()
                          {
                              Id = dealer.Id,
                              Abbreviation = dealer.Abbreviation
                          };
            return await dealers.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetSalesStageForSalesCustomer()
        {
            var stages = from stage in _mstSleSalesStageRepository.GetAll().OrderByDescending(e => e.Ordering)
                         select new GetMakeForSalesCustomerDto()
                         {
                             Id = stage.Id,
                             Description = stage.Vi_Description
                         };
            return await stages.ToListAsync();
        }

        public async Task<List<ListGradeCodeDto>> GetGradeForSalesCustomer(long? ModelId)
        {
            var grades = from grade in _mstSleGradesRepository.GetAll().Where(e => ModelId == null || e.ModelId == ModelId).OrderByDescending(e => e.Ordering)
                         select new ListGradeCodeDto()
                         {
                             ModelId = grade.ModelId,
                             Id = grade.Id,
                             MarketingCode = grade.MarketingCode
                         };
            return await grades.ToListAsync();
        }

        public async Task<List<ListGradeCodeDto>> GetGradeProductionForSalesCustomer(long? GradeId)
        {
            var productions = from production in _mstSleGradeProductionRepository.GetAll().Where(e => GradeId == null || e.GradeId == GradeId).OrderByDescending(e => e.Ordering)
                              select new ListGradeCodeDto()
                              {
                                  Id = production.Id,
                                  MarketingCode = production.ProductionCode
                              };
            return await productions.ToListAsync();
        }

        public async Task<List<GetColorCodeDto>> GetColorForSalesCustomer()
        {
            var colors = from color in _mstSleColorsRepository.GetAll()
                         select new GetColorCodeDto()
                         {
                             Id = color.Id,
                             Code = color.Code
                         };
            return await colors.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetContactByForSalesCustomer()
        {
            var contactBys = from contactBy in _mstSleContactByRepository.GetAll().OrderByDescending(e => e.Ordering)
                             select new GetMakeForSalesCustomerDto()
                             {
                                 Id = contactBy.Id,
                                 Description = contactBy.Vi_Description
                             };
            return await contactBys.ToListAsync();
        }

        public async Task<List<GetMakeForSalesCustomerDto>> GetReasonOfNCForSalesCustomer()
        {
            var reasonOfNC = from reason in _mstSleReasonOfNCRepository.GetAll().OrderBy(e => e.Ordering)
                             select new GetMakeForSalesCustomerDto()
                             {
                                 Id = reason.Id,
                                 Description = reason.Vi_Description
                             };
            return await reasonOfNC.ToListAsync();
        }

        private async Task<List<GetMakeForSalesCustomerDto>> GetPurposeForSalesCustomer()
        {
            var purposes = from purpose in _mstSlePurposeRepository.GetAll().OrderBy(e => e.Ordering)
                           select new GetMakeForSalesCustomerDto()
                           {
                               Id = purpose.Id,
                               Description = purpose.Vi_Description
                           };
            return await purposes.ToListAsync();
        }

        #region Danh sách TVBH
        public async Task<List<GetListSalesPersonNameDto>> GetListAssignPerson()
        {
            var person = from p in _salesPersonRepository.GetAll()

                         select new GetListSalesPersonNameDto()
                         {
                             Id = p.Id,
                             FullName = p.FullName,
                         };

            return await person.ToListAsync();
        }

        private List<GetColorByGradeDto> GetExternalColorByGradeForCustomer()
        {
            var query = from price in _mstSleVehiclePriceRepository.GetAll().AsNoTracking()
                        join color in _mstSleColorsRepository.GetAll().AsNoTracking() on price.ColorId equals color.Id

                        select new GetColorByGradeDto()
                        {
                            GradeId = price.GradeId,
                            ColorId = price.ColorId,
                            Color = color.VnName + ' ' + '(' + color.Code + ')'
                        };
            return query.Distinct().ToList();
        }
        #endregion

        #endregion

        #region Get User

        #region Get OrganiztionUnits By UnitId
        public List<long> GetOrganiztionUnitsByUnitId(long UnitId)
        {
            long currentUserId = (long)AbpSession.UserId;

            List<long> listOrganization = new();
            List<long> listUserID = new();

            IQueryable<UserOrganizationUnit> userOrganization = _userOrganizationUnitRepository.GetAll().AsNoTracking();
            IQueryable<OrganizationUnit> organizationUnit = _organizationUnitRepository.GetAll().AsNoTracking();

            List<long> organization = (from Org in organizationUnit
                                       join UserOrg in userOrganization.Where(r => r.OrganizationUnitId == UnitId) on Org.Id equals UserOrg.OrganizationUnitId
                                       select Org.Id).ToList();

            foreach (var parent in organization)
            {
                var ListParent = organizationUnit.Where(r => r.ParentId == parent).ToList();
                while (ListParent.Any())
                {
                    foreach (var a in ListParent.ToList())
                    {
                        var check = organizationUnit.Where(r => r.ParentId == a.Id).ToList();
                        if (check != null)
                        {
                            foreach (var c in check)
                            {
                                ListParent.Add(c);
                            }
                        }
                        listOrganization.AddIfNotContains(a.Id);
                        ListParent.Remove(a);
                    }
                }
            }

            if (listOrganization != null)
            {
                var a = (from user in userOrganization
                         where listOrganization.Any(a => user.OrganizationUnitId.Equals(a))
                         select user.UserId).ToList();
                listUserID = a;

                var userInUnitInputs = userOrganization.Where(r => r.OrganizationUnitId == UnitId).ToList();
                foreach (var u in userInUnitInputs)
                {
                    listUserID.Add(u.UserId);
                }

                return listUserID;
            }

            var userInUnitInput = userOrganization.Where(r => r.OrganizationUnitId == UnitId).ToList();
            foreach (var u in userInUnitInput)
            {
                listUserID.Add(u.UserId);
            }

            return listUserID;
        }
        #endregion

        #region Get Users with View Customer Permission
        /*
            Kiểm tra User với SalesPerson tương úng xem User đó có quyền IsViewCustomer
            Có: Lấy ra danh sách các User dưới quyền User đang đăng nhập
                Nếu User đang đăng nhập là cấp thấp nhất => lấy ra User đó
            Không: Lấy ra chính User đang đăng nhập
        */
        private List<long> GetListUserBySalesPersonPermission()
        {
            long currentUserId = (long)AbpSession.UserId;
            List<long> listOrganization = new();
            List<long> listUserID = new();

            MstSleSalesPerson salesPerson = _salesPersonRepository.GetAll().AsNoTracking().FirstOrDefault(e => e.UserId == currentUserId);
            if (salesPerson != null)
            {
                if (salesPerson.IsViewCustomer == true)
                {
                    IQueryable<UserOrganizationUnit> userOrganization = _userOrganizationUnitRepository.GetAll().AsNoTracking();
                    IQueryable<OrganizationUnit> organizationUnit = _organizationUnitRepository.GetAll().AsNoTracking();

                    List<long> organization = (from Org in organizationUnit join UserOrg in userOrganization.Where(r => r.UserId == currentUserId) on Org.Id equals UserOrg.OrganizationUnitId select Org.Id).ToList();
                    foreach (var parent in organization)
                    {
                        List<OrganizationUnit> ListParent = organizationUnit.Where(r => r.ParentId == parent).ToList();
                        while (ListParent.Any())
                        {
                            foreach (var a in ListParent.ToList())
                            {
                                List<OrganizationUnit> check = organizationUnit.Where(r => r.ParentId == a.Id).ToList();
                                if (check != null)
                                {
                                    foreach (var c in check)
                                    {
                                        ListParent.Add(c);
                                    }
                                }
                                listOrganization.AddIfNotContains(a.Id);
                                ListParent.Remove(a);
                            }
                        }
                    }
                    if (listOrganization != null)
                    {
                        var a = (from user in userOrganization where listOrganization.Any(a => user.OrganizationUnitId.Equals(a)) select user.UserId).ToList();
                        listUserID = a;
                        listUserID.Add(currentUserId);
                        return listUserID;
                    }
                }

                if (salesPerson.IsViewCustomer != true)
                {
                    listUserID.Add((long)salesPerson.UserId);
                    return listUserID;
                }
            }

            if (salesPerson == null)
            {
                throw new UserFriendlyException(L("YourAccountIsNotASalesPerson"));
            }

            return null;
        }
        #endregion

        #region Get OrganizationUnits by current user
        public List<GetOrganizationUnitsDto> GetOrganiztionUnitsByCurrentUser()
        {
            long currentUserId = (long)AbpSession.UserId;

            List<long> listOrganization = new();
            List<GetOrganizationUnitsDto> listUserID = new();
            List<GetOrganizationUnitsDto> result = new();

            IQueryable<UserOrganizationUnit> userOrganization = _userOrganizationUnitRepository.GetAll().AsNoTracking();
            IQueryable<OrganizationUnit> organizationUnit = _organizationUnitRepository.GetAll().AsNoTracking();

            List<long> organization = (from Org in organizationUnit
                                       join UserOrg in userOrganization.Where(r => r.UserId == currentUserId) on Org.Id equals UserOrg.OrganizationUnitId
                                       select Org.Id).ToList();

            foreach (var parent in organization)
            {
                var ListParent = organizationUnit.Where(r => r.ParentId == parent).ToList();
                while (ListParent.Any())
                {
                    foreach (var a in ListParent.ToList())
                    {
                        var check = organizationUnit.Where(r => r.ParentId == a.Id).ToList();
                        if (check != null)
                        {
                            foreach (var c in check)
                            {
                                ListParent.Add(c);
                            }
                        }
                        listOrganization.AddIfNotContains(a.Id);
                        ListParent.Remove(a);
                    }
                }
            }

            if (listOrganization != null)
            {
                var a = (from user in userOrganization.Where(e => listOrganization.Any(a => a == e.OrganizationUnitId))
                         join u in organizationUnit on user.OrganizationUnitId equals u.Id into UserUnits
                         from UserUnit in UserUnits.DefaultIfEmpty()
                         select new GetOrganizationUnitsDto()
                         {
                             Id = UserUnit.Id,
                             DisplayName = UserUnit.DisplayName
                         }).ToList();

                listUserID = a;

                var userInUnitInputs = (from user in userOrganization.Where(e => e.UserId == currentUserId)
                                        join u in organizationUnit on user.OrganizationUnitId equals u.Id into UserUnits
                                        from UserUnit in UserUnits.DefaultIfEmpty()

                                        select new GetOrganizationUnitsDto()
                                        {
                                            Id = UserUnit.Id,
                                            DisplayName = UserUnit.DisplayName
                                        }).ToList();
                foreach (var u in userInUnitInputs)
                {
                    listUserID.Add(u);
                }
                result = listUserID.DistinctBy(e => e.Id).OrderBy(e => e.Id).ToList();
                return result;
            }

            var userInUnitInput = (from user in userOrganization.Where(e => e.UserId == currentUserId)
                                   join u in organizationUnit on user.OrganizationUnitId equals u.Id into UserUnits
                                   from UserUnit in UserUnits.DefaultIfEmpty()

                                   select new GetOrganizationUnitsDto()
                                   {
                                       Id = UserUnit.Id,
                                       DisplayName = UserUnit.DisplayName
                                   }).ToList();
            foreach (var u in userInUnitInput)
            {
                listUserID.Add(u);
            }

            result = listUserID.DistinctBy(e => e.Id).OrderBy(e => e.Id).ToList();
            return result;
        }
        #endregion

        #region Get User with ViewDuplicateCustomer Permission
        /*
            Kiểm tra User với SalesPerson tương úng xem User đó có quyền IsViewDuplicateCustomer
            Có: Lấy ra danh sách các User dưới quyền User đang đăng nhập
                Nếu User đang đăng nhập là cấp thấp nhất => lấy ra User đó
            Không: Lấy ra chính User đang đăng nhập
        */
        private List<long> GetListUserToViewDuplicateCustomer()
        {
            long currentUserId = (long)AbpSession.UserId;
            List<long> listOrganization = new();
            List<long> listUserID = new();

            MstSleSalesPerson salesPerson = _salesPersonRepository.GetAll().AsNoTracking().FirstOrDefault(e => e.UserId == currentUserId);
            if (salesPerson != null)
            {
                if (salesPerson.IsViewDuplicateCustomer == true)
                {
                    IQueryable<UserOrganizationUnit> userOrganization = _userOrganizationUnitRepository.GetAll().AsNoTracking();
                    IQueryable<OrganizationUnit> organizationUnit = _organizationUnitRepository.GetAll().AsNoTracking();

                    List<long> organization = (from Org in organizationUnit
                                               join UserOrg in userOrganization.Where(r => r.UserId == currentUserId) on Org.Id equals UserOrg.OrganizationUnitId
                                               select Org.Id).ToList();
                    foreach (var parent in organization)
                    {
                        List<OrganizationUnit> ListParent = organizationUnit.Where(r => r.ParentId == parent).ToList();
                        while (ListParent.Any())
                        {
                            foreach (var a in ListParent.ToList())
                            {
                                List<OrganizationUnit> check = organizationUnit.Where(r => r.ParentId == a.Id).ToList();
                                if (check != null)
                                {
                                    foreach (var c in check)
                                    {
                                        ListParent.Add(c);
                                    }
                                }
                                listOrganization.AddIfNotContains(a.Id);
                                ListParent.Remove(a);
                            }
                        }
                    }
                    if (listOrganization != null)
                    {
                        var a = (from user in userOrganization
                                 where listOrganization.Any(a => user.OrganizationUnitId.Equals(a))
                                 select user.UserId).ToList();
                        listUserID = a;
                        listUserID.Add(currentUserId);
                        return listUserID;
                    }
                }

                if (salesPerson.IsViewDuplicateCustomer != true)
                {
                    listUserID.Add((long)salesPerson.UserId);
                    return listUserID;
                }
            }

            if (salesPerson == null)
            {
                throw new UserFriendlyException(L("YourAccountIsNotASalesPerson"));
            }

            return null;
        }
        #endregion

        #endregion

        #region Thông tin KH tiềm năng của đại lý
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<PagedResultDto<GetSalesCustomerForViewDto>> GetAllCustomerForView(GetFilterSalesCustomerDto input)
        {
            long? encoded = 0;

            List<long> ListUserId = GetListUserBySalesPersonPermission();
            List<long> unitUserIds = new();

            if (input.UnitId != null)
            {
                unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
            }

            IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerName)
                               || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName)
                               || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                .Where(e => input.SourceId == null || e.SourceId == input.SourceId)
                .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
                .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
                .Where(e => input.ProvinceId == -1 || e.ProvinceId == input.ProvinceId)
                .Where(e => input.DistrictId == -1 || e.DistrictId == input.DistrictId)
                .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                .Where(c => c.CustomerTypeId == input.CustomerTypeId && ListUserId.Any(l => l == (long)c.CreatorUserId))
                .WhereIf(input.CustomerClass == 1, c => c.Status != 8 && c.Status != 9 && c.Status != 6 && c.ContactResult == 1) // KH tiềm năng
                .WhereIf(input.CustomerClass == 2, c => c.Status == 9) // KH đóng băng
                .WhereIf(input.CustomerClass == 3, c => c.Status == 8) // KH mất khách
                .WhereIf(input.CustomerClass == 4, c => c.Status == 6) // KH xuất hóa đơn
                ;

            IQueryable<SalesCustomerTransactionInfoContact> transactionInfoContacts = _customerTransactionInfoContactRepository.GetAll().AsNoTracking()
                .Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date);

            IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking()
                .Where(e => input.ModelId == null || e.ModelId == input.ModelId)
                .Where(e => input.GradeId == null || e.GradeId == input.GradeId);

            IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();
            IQueryable<MstSleGrades> grades = _mstSleGradesRepository.GetAll().AsNoTracking();
            IQueryable<MstSleColors> colors = _mstSleColorsRepository.GetAll().AsNoTracking();

            IQueryable<GetSalesCustomerForViewDto> result = from customer in salesCustomer.Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))

                                                            join person in _salesPersonRepository.GetAll().AsNoTracking() on customer.SalesPersonId equals person.Id into customerPersons
                                                            from customerPerson in customerPersons.DefaultIfEmpty()

                                                            join userUnit in _userOrganizationUnitRepository.GetAll().AsNoTracking() on customerPerson.UserId equals userUnit.UserId into userUnitPersons
                                                            from userUnitPerson in userUnitPersons.DefaultIfEmpty()

                                                            join unit in _organizationUnitRepository.GetAll().AsNoTracking() on userUnitPerson.OrganizationUnitId equals unit.Id into empDepartments
                                                            from empDepartment in empDepartments.DefaultIfEmpty()

                                                            join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStages
                                                            from customerStage in customerStages.DefaultIfEmpty()

                                                            join hotness in _mstSleHotnessRepository.GetAll().AsNoTracking() on customer.HotnessId equals hotness.Id into customerHotnesses
                                                            from customerHotness in customerHotnesses.DefaultIfEmpty()

                                                            join source in _mstSleSourceRepository.GetAll().AsNoTracking() on customer.SourceId equals source.Id into customerSources
                                                            from customerSource in customerSources.DefaultIfEmpty()

                                                            join transactionInfo in _customerTransactionInfoRepository.GetAll().AsNoTracking() on customer.Id equals transactionInfo.SalesCustomerId into customerTransactionInfos
                                                            from customerTransactionInfo in customerTransactionInfos.DefaultIfEmpty()

                                                            join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                                                            from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                                                            join transactionInfoContact in transactionInfoContacts on customer.Id equals transactionInfoContact.SalesCustomerId into customerTransactionInfoContacts
                                                            from transactionInfoContact in customerTransactionInfoContacts.DefaultIfEmpty()

                                                            join contactBy in _mstSleContactByRepository.GetAll() on transactionInfoContact.ContactById equals contactBy.Id into contactByTransactionJoined
                                                            from contactBy in contactByTransactionJoined.DefaultIfEmpty()

                                                            orderby customer.CreationTime descending
                                                            select new GetSalesCustomerForViewDto()
                                                            {
                                                                SalesCustomerInfomation = new SalesCustomerInfomationDto()
                                                                {
                                                                    Id = customer.Id,
                                                                    CreationTime = customer.CreationTime,
                                                                    CustomerNo = customer.No,
                                                                    Name = customer.CustomerName,
                                                                    DistrictId = customer.DistrictId,
                                                                    ProvinceId = customer.ProvinceId,
                                                                    Address = customer.Details,
                                                                    Source = customerSource.Vi_Description,
                                                                    Status = customerStage.Vi_Description,
                                                                    IsShowRoomVisit = customerTransactionInfo.IsShowRoomVisit,
                                                                    IsTestDrive = customerTransactionInfo.IsTestDrive,
                                                                    ReasonWhyNotTD = customerTransactionInfo.ReasonWhyNotTD,
                                                                    TestDriveDate = customerTransactionInfo.TestDriveDate,
                                                                    FirstContactDate = customer.ContactDate,
                                                                    Hotness = customerHotness.Vi_Description,
                                                                    SalesPerson = customerPerson.FullName,
                                                                    CustomerType = customer.CustomerTypeId == 1 ? "Cá nhân" : "Doanh nghiệp",
                                                                    ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                                                    CustomerTel1 = encoded == 1 ? customer.CustomerTel1Decode : L("Encrypted"),
                                                                    CompanyName = customer.CompanyName,
                                                                    CurrentDate = transactionInfoContact.CurrentDate,
                                                                    CurrentAction = transactionInfoContact.CurrentAction,
                                                                    ContactRemark = transactionInfoContact.Note,
                                                                    NextContact = transactionInfoContact.NextDate,
                                                                    NextPlan = transactionInfoContact.NextAction,
                                                                    ContactBy = contactBy.Vi_Description,
                                                                    ManagerComment = customer.ManagerComment,
                                                                    Department = empDepartment.DisplayName,
                                                                    SalesPersonId = customer.SalesPersonId,
                                                                    ReasonLost = customerTransactionInfo.ReasonLost,
                                                                    ReasonLostDate = customerTransactionInfo.ActivityDate,
                                                                    StatusId = customer.Status,
                                                                    FreezeType = customerTransactionInfo.OnPassiveFreezing
                                                                }
                                                            };

            int totalCount = await result.CountAsync();

            List<GetSalesCustomerForViewDto> paged = await result.PageBy(input).ToListAsync();

            foreach (var page in paged)
            {
                page.CarAttention = (from vehicle in customerVehicleInformation.Where(e => e.SalesCustomerId == page.SalesCustomerInfomation.Id && e.VehicleType == 1)
                                     join model in models.Where(e => e.MakeId == 1) on vehicle.ModelId equals model.Id into vehicleModelJoined
                                     from model in vehicleModelJoined.DefaultIfEmpty()

                                     join grade in grades on vehicle.GradeId equals grade.Id into vehicleGradeJoined
                                     from grade in vehicleGradeJoined.DefaultIfEmpty()

                                     select new GetListCarAttentionDto()
                                     {
                                         Model = model.MarketingCode,
                                         Grade = grade.MarketingCode,
                                     }).ToList();

                page.OtherCarAttention = (from vehicle in customerVehicleInformation.Where(e => e.SalesCustomerId == page.SalesCustomerInfomation.Id && e.VehicleType == 1)

                                          join make in _mstSleMakeRepository.GetAll() on vehicle.MakeId equals make.Id

                                          join model in models.Where(e => e.MakeId != 1) on vehicle.ModelId equals model.Id into vehicleModelJoined
                                          from model in vehicleModelJoined.DefaultIfEmpty()

                                          join grade in grades on vehicle.GradeId equals grade.Id into vehicleGradeJoined
                                          from grade in vehicleGradeJoined.DefaultIfEmpty()

                                          select new GetListCarAttentionDto()
                                          {
                                              Make = make.Description,
                                              Model = model.MarketingCode,
                                              Grade = grade.MarketingCode,
                                          }).ToList();

                page.CarPurchased = (from vehicle in customerVehicleInformation.Where(e => e.SalesCustomerId == page.SalesCustomerInfomation.Id && e.VehicleType == 3)
                                     join make in _mstSleMakeRepository.GetAll() on vehicle.MakeId equals make.Id into vehicleMakeJoined
                                     from make in vehicleMakeJoined.DefaultIfEmpty()

                                     join model in models on vehicle.ModelId equals model.Id into vehicleModelJoined
                                     from model in vehicleModelJoined.DefaultIfEmpty()

                                     join grade in grades on vehicle.GradeId equals grade.Id into vehicleGradeJoined
                                     from grade in vehicleGradeJoined.DefaultIfEmpty()

                                     select new CarPurchasedCustomerDto()
                                     {
                                         Make = make.Description,
                                         ModelPurchase = model.MarketingCode,
                                         GradePurchase = grade.MarketingCode,
                                         YearOfPurchaseDate = vehicle.YearOfPurchaseDate
                                     }).ToList();
            }

            return new PagedResultDto<GetSalesCustomerForViewDto>(
                totalCount,
                paged
            );
        }
        #endregion

        #region Thông tin khách hàng tiềm năng của đại lý nhưng chỉ lấy liên hệ mới nhất
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<PagedResultDto<GetLatestInfoContactCustomerDto>> GetLatestContactCustomerAsync(GetFilterSalesCustomerDto input)
        {
            if (input.ModelId != null)
            {
                return await GetAllCustomerWithModelFilter(input);
            }

            bool? FreezeType = null;

            if (input.FreezeType == 3) FreezeType = null;
            if (input.FreezeType == 1) FreezeType = true; // Bị động
            if (input.FreezeType == 2) FreezeType = false; // Chủ động

            //SysAppConfig checkSeeCusPhone = _sysAppConfigRepository.GetAll().AsNoTracking().Where(e => e.Code == "ACCP").FirstOrDefault();
            //long? encoded = 0;

            //encoded = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId).IsViewPhoneNumber == true ? 1 : 0;

            //if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

            List<long> ListUserId = GetListUserBySalesPersonPermission();
            //ListUserId = GetListUserFromSaleOff(isSaleOff, ListUserId);
            List<long> unitUserIds = new();
            if (input.UnitId != null)
            {
                unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
                //unitUserIds = GetListUserFromSaleOff(isSaleOff, unitUserIds);
            }

            IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1Decode.Contains(input.Filter))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                .Where(e => input.SourceId == null || e.SourceId == input.SourceId)
                .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
                .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
                .Where(e => input.ProvinceId == -1 || e.ProvinceId == input.ProvinceId)
                .Where(e => input.DistrictId == -1 || e.DistrictId == input.DistrictId)
                .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                .Where(c => c.CustomerTypeId == input.CustomerTypeId && ListUserId.Any(l => l == (long)c.CreatorUserId))
                .Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))
                .WhereIf(input.CustomerClass == 1, c => c.Status != 8 && c.Status != 9 && c.Status != 6 && c.ContactResult == 1) // KH tiềm năng
                .WhereIf(input.CustomerClass == 2, c => c.Status == 9) // KH đóng băng
                .WhereIf(input.CustomerClass == 3, c => c.Status == 8) // KH mất khách
                .WhereIf(input.CustomerClass == 4, c => c.Status == 6) // KH xuất hóa đơn
                ;

            IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking();
            var transactionInfoContacts = _customerTransactionInfoContactRepository.GetAll().AsNoTracking()
                .Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date);

            var transactionInfos = _customerTransactionInfoRepository.GetAll().AsNoTracking().Where(e => FreezeType == null || e.OnPassiveFreezing == FreezeType);

            // Lấy ra KH có ngày lịch sử liên hệ là mới nhất tương đương với Id lớn hơn
            var customerLatestContactDate = from customers in salesCustomer
                                            join contacts in transactionInfoContacts on customers.Id equals contacts.SalesCustomerId into customerContactJoined
                                            from contacts in customerContactJoined.DefaultIfEmpty()

                                            join transaction in transactionInfos on customers.Id equals transaction.SalesCustomerId into customerTransactionJoined
                                            from transaction in customerTransactionJoined.DefaultIfEmpty()

                                            group new { customers, contacts, transaction }
                                            by new
                                            {
                                                customers.Id,
                                                customers.SalesPersonId,
                                                customers.CustomerTypeId,
                                                customers.Status,
                                                customers.HotnessId,
                                                customers.SourceId,
                                                customers.ExpectedDelTimingId,
                                                customers.CreationTime,
                                                customers.No,
                                                customers.CustomerName,
                                                customers.DistrictId,
                                                customers.ProvinceId,
                                                customers.Details,
                                                customers.ContactDate,
                                                CustomerTel1 = customers.CustomerTel1Decode,
                                                customers.CompanyName,
                                                customers.ManagerComment,
                                                transaction.IsShowRoomVisit,
                                                transaction.IsTestDrive,
                                                transaction.ReasonWhyNotTD,
                                                transaction.TestDriveDate,
                                                transaction.ReasonLost,
                                                transaction.OnPassiveFreezing,
                                                transaction.ActivityDate
                                            } into grouped
                                            select new
                                            {
                                                grouped.Key.Id,
                                                grouped.Key.SalesPersonId,
                                                grouped.Key.CustomerTypeId,
                                                grouped.Key.Status,
                                                grouped.Key.HotnessId,
                                                grouped.Key.SourceId,
                                                grouped.Key.ExpectedDelTimingId,
                                                grouped.Key.CreationTime,
                                                grouped.Key.No,
                                                grouped.Key.CustomerName,
                                                grouped.Key.DistrictId,
                                                grouped.Key.ProvinceId,
                                                grouped.Key.Details,
                                                grouped.Key.ContactDate,
                                                grouped.Key.CustomerTel1,
                                                grouped.Key.CompanyName,
                                                grouped.Key.ManagerComment,
                                                grouped.Key.IsShowRoomVisit,
                                                grouped.Key.IsTestDrive,
                                                grouped.Key.ReasonWhyNotTD,
                                                grouped.Key.TestDriveDate,
                                                grouped.Key.ReasonLost,
                                                grouped.Key.OnPassiveFreezing,
                                                grouped.Key.ActivityDate,
                                                TransactionInfoContactId = grouped.Max(e => e.contacts.Id),
                                            };

            // Join lại với bảng SalesCustomerTransactionInfoContact để lấy ra các thông tin cần thiết khác
            var transactionInfoContact = from contactLatestDate in customerLatestContactDate
                                         join cont in transactionInfoContacts on contactLatestDate.TransactionInfoContactId equals cont.Id into contactLatestDateJoined
                                         from cont in contactLatestDateJoined.DefaultIfEmpty()

                                         join contactBy in _mstSleContactByRepository.GetAll() on cont.ContactById equals contactBy.Id into contactByTransactionJoined
                                         from contactBy in contactByTransactionJoined.DefaultIfEmpty()

                                         orderby contactLatestDate.Id descending
                                         select new
                                         {
                                             contactLatestDate.Id,
                                             contactLatestDate.SalesPersonId,
                                             contactLatestDate.CustomerTypeId,
                                             contactLatestDate.Status,
                                             contactLatestDate.HotnessId,
                                             contactLatestDate.SourceId,
                                             contactLatestDate.ExpectedDelTimingId,
                                             contactLatestDate.CreationTime,
                                             contactLatestDate.No,
                                             contactLatestDate.CustomerName,
                                             contactLatestDate.DistrictId,
                                             contactLatestDate.ProvinceId,
                                             contactLatestDate.Details,
                                             contactLatestDate.ContactDate,
                                             contactLatestDate.CustomerTel1,
                                             contactLatestDate.CompanyName,
                                             contactLatestDate.ManagerComment,
                                             contactLatestDate.IsShowRoomVisit,
                                             contactLatestDate.IsTestDrive,
                                             contactLatestDate.ReasonWhyNotTD,
                                             contactLatestDate.TestDriveDate,
                                             contactLatestDate.ReasonLost,
                                             contactLatestDate.OnPassiveFreezing,
                                             contactLatestDate.ActivityDate,
                                             cont.CurrentDate,
                                             cont.CurrentAction,
                                             cont.NextDate,
                                             cont.NextAction,
                                             ContactBy = contactBy.Vi_Description,
                                             cont.ContactById
                                         };

            IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();
            IQueryable<MstSleGrades> grades = _mstSleGradesRepository.GetAll().AsNoTracking();

            IQueryable<GetLatestInfoContactCustomerDto> result = from customer in transactionInfoContact
                                                                                  .Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                                                                                  .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                                                                                  .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date)
                                                                                  .Where(e => FreezeType == null || e.OnPassiveFreezing == FreezeType)

                                                                 join person in _salesPersonRepository.GetAll().AsNoTracking() on customer.SalesPersonId equals person.Id
                                                                 //into person from customerPerson in customerPersons.DefaultIfEmpty()

                                                                 join userUnit in _userOrganizationUnitRepository.GetAll().AsNoTracking() on person.UserId equals userUnit.UserId into userUnitPersons
                                                                 from userUnitPerson in userUnitPersons.DefaultIfEmpty()

                                                                 join unit in _organizationUnitRepository.GetAll().AsNoTracking() on userUnitPerson.OrganizationUnitId equals unit.Id into empDepartments
                                                                 from empDepartment in empDepartments.DefaultIfEmpty()

                                                                 join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStages
                                                                 from customerStage in customerStages.DefaultIfEmpty()

                                                                 join hotness in _mstSleHotnessRepository.GetAll().AsNoTracking() on customer.HotnessId equals hotness.Id into customerHotnesses
                                                                 from customerHotness in customerHotnesses.DefaultIfEmpty()

                                                                 join source in _mstSleSourceRepository.GetAll().AsNoTracking() on customer.SourceId equals source.Id into customerSources
                                                                 from customerSource in customerSources.DefaultIfEmpty()

                                                                 join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                                                                 from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                                                                 orderby customer.Id descending
                                                                 select new GetLatestInfoContactCustomerDto()
                                                                 {
                                                                     SalesCustomerInfo = new SalesCustomerInfomationDto()
                                                                     {
                                                                         Id = customer.Id,
                                                                         CreationTime = customer.CreationTime,
                                                                         CustomerNo = customer.No,
                                                                         Name = customer.CustomerName,
                                                                         DistrictId = customer.DistrictId,
                                                                         ProvinceId = customer.ProvinceId,
                                                                         Address = customer.Details,
                                                                         Source = customerSource.Vi_Description,
                                                                         Hotness = customerHotness.Vi_Description,
                                                                         Status = customerStage.Vi_Description,
                                                                         CustomerType = customer.CustomerTypeId == 1 ? "Cá nhân" : "Doanh nghiệp",
                                                                         ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                                                         IsShowRoomVisit = customer.IsShowRoomVisit,
                                                                         IsTestDrive = customer.IsTestDrive,
                                                                         ReasonWhyNotTD = customer.ReasonWhyNotTD,
                                                                         TestDriveDate = customer.TestDriveDate,
                                                                         SalesPerson = person.FullName,
                                                                         Department = empDepartment.DisplayName,
                                                                         ReasonLost = customer.ReasonLost,
                                                                         ReasonLostDate = customer.ActivityDate,
                                                                         SalesPersonId = customer.SalesPersonId,
                                                                         FirstContactDate = customer.ContactDate,
                                                                         CustomerTel1 = customer.CustomerTel1,
                                                                         CompanyName = customer.CompanyName,
                                                                         ManagerComment = customer.ManagerComment,
                                                                         CurrentDate = customer.CurrentDate,
                                                                         CurrentAction = customer.CurrentAction,
                                                                         NextContact = customer.NextDate,
                                                                         NextPlan = customer.NextAction,
                                                                         ContactBy = customer.ContactBy,
                                                                         StatusId = customer.Status,
                                                                         FreezeType = customer.OnPassiveFreezing
                                                                     },
                                                                 };

            int totalCount = await result.CountAsync();

            List<GetLatestInfoContactCustomerDto> paged = await result.PageBy(input).ToListAsync();

            foreach (var page in paged)
            {
                page.CarAttention = (from vehicle in customerVehicleInformation.Where(e => e.SalesCustomerId == page.SalesCustomerInfo.Id && e.VehicleType == 1)
                                     join model in models.Where(e => e.MakeId == 1) on vehicle.ModelId equals model.Id into vehicleModelJoined
                                     from model in vehicleModelJoined.DefaultIfEmpty()

                                     join grade in grades on vehicle.GradeId equals grade.Id into vehicleGradeJoined
                                     from grade in vehicleGradeJoined.DefaultIfEmpty()

                                     select new GetListCarAttentionDto()
                                     {
                                         Model = model.MarketingCode,
                                         Grade = grade.MarketingCode,
                                     }).ToList();
            };

            return new PagedResultDto<GetLatestInfoContactCustomerDto>(
                totalCount,
                paged
            );
        }
        #endregion

        #region Lấy thông tin KH với filter Model - Grade của liên hệ mới nhất
        public async Task<PagedResultDto<GetLatestInfoContactCustomerDto>> GetAllCustomerWithModelFilter(GetFilterSalesCustomerDto input)
        {
            bool? FreezeType = null;

            if (input.FreezeType == 3) FreezeType = null;
            if (input.FreezeType == 1) FreezeType = true; // Bị động
            if (input.FreezeType == 2) FreezeType = false; // Chủ động

            //SysAppConfig checkSeeCusPhone = _sysAppConfigRepository.GetAll().Where(e => e.Code == "ACCP").FirstOrDefault();

            long? encoded = 0;

            encoded = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId).IsViewPhoneNumber == true ? 1 : 0;

            //if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

            List<long> ListUserId = GetListUserBySalesPersonPermission();

            List<long> unitUserIds = new();
            if (input.UnitId != null)
            {
                unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
            }

            IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerName)
                               || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName)
                               || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
                .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
                .Where(e => input.ProvinceId == -1 || e.ProvinceId == input.ProvinceId)
                .Where(e => input.DistrictId == -1 || e.DistrictId == input.DistrictId)
                .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                .Where(c => c.CustomerTypeId == input.CustomerTypeId && ListUserId.Any(l => l == (long)c.CreatorUserId))
                .WhereIf(input.CustomerClass == 1, c => c.Status != 8 && c.Status != 9 && c.Status != 6 && c.ContactResult == 1) // KH tiềm năng
                .WhereIf(input.CustomerClass == 2, c => c.Status == 9) // KH đóng băng
                .WhereIf(input.CustomerClass == 3, c => c.Status == 8) // KH mất khách
                .WhereIf(input.CustomerClass == 4, c => c.Status == 6) // KH xuất hóa đơn
                ;

            IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking()
                .Where(e => input.ModelId == null || e.ModelId == input.ModelId)
                .Where(e => input.GradeId == null || e.GradeId == input.GradeId);

            var transactionInfoContacts = _customerTransactionInfoContactRepository.GetAll().AsNoTracking()
                .Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date);

            var transactionInfos = _customerTransactionInfoRepository.GetAll().AsNoTracking().Where(e => FreezeType == null || e.OnPassiveFreezing == FreezeType);

            // Lấy ra KH có ngày lịch sử liên hệ là mới nhất tương đương với Id lớn hơn
            var customerLatestContactDate = from customers in salesCustomer.Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))
                                            join contacts in transactionInfoContacts on customers.Id equals contacts.SalesCustomerId into customerContactJoined
                                            from contacts in customerContactJoined.DefaultIfEmpty()

                                            join transaction in transactionInfos on customers.Id equals transaction.SalesCustomerId into customerTransactionJoined
                                            from transaction in customerTransactionJoined.DefaultIfEmpty()

                                            group new { customers, contacts, transaction }
                                            by new
                                            {
                                                customers.Id,
                                                customers.SalesPersonId,
                                                customers.CustomerTypeId,
                                                customers.Status,
                                                customers.HotnessId,
                                                customers.SourceId,
                                                customers.ExpectedDelTimingId,
                                                customers.CreationTime,
                                                customers.No,
                                                customers.CustomerName,
                                                customers.DistrictId,
                                                customers.ProvinceId,
                                                customers.Details,
                                                customers.ContactDate,
                                                CustomerTel1 = encoded == 1 ? customers.CustomerTel1Decode : L("Encrypted"),
                                                customers.CompanyName,
                                                customers.ManagerComment,
                                                transaction.IsShowRoomVisit,
                                                transaction.IsTestDrive,
                                                transaction.ReasonWhyNotTD,
                                                transaction.TestDriveDate,
                                                transaction.ReasonLost,
                                                transaction.OnPassiveFreezing,
                                            } into grouped
                                            select new
                                            {
                                                grouped.Key.Id,
                                                grouped.Key.SalesPersonId,
                                                grouped.Key.CustomerTypeId,
                                                grouped.Key.Status,
                                                grouped.Key.HotnessId,
                                                grouped.Key.SourceId,
                                                grouped.Key.ExpectedDelTimingId,
                                                grouped.Key.CreationTime,
                                                grouped.Key.No,
                                                grouped.Key.CustomerName,
                                                grouped.Key.DistrictId,
                                                grouped.Key.ProvinceId,
                                                grouped.Key.Details,
                                                grouped.Key.ContactDate,
                                                grouped.Key.CustomerTel1,
                                                grouped.Key.CompanyName,
                                                grouped.Key.ManagerComment,
                                                grouped.Key.IsShowRoomVisit,
                                                grouped.Key.IsTestDrive,
                                                grouped.Key.ReasonWhyNotTD,
                                                grouped.Key.TestDriveDate,
                                                grouped.Key.ReasonLost,
                                                grouped.Key.OnPassiveFreezing,
                                                TransactionInfoContactId = grouped.Max(e => e.contacts.Id),
                                            };

            // Join lại với bảng SalesCustomerTransactionInfoContact để lấy ra các thông tin cần thiết khác
            var transactionInfoContact = from contactLatestDate in customerLatestContactDate
                                         join cont in transactionInfoContacts on contactLatestDate.TransactionInfoContactId equals cont.Id into contactLatestDateJoined
                                         from cont in contactLatestDateJoined.DefaultIfEmpty()

                                         join contactBy in _mstSleContactByRepository.GetAll() on cont.ContactById equals contactBy.Id into contactByTransactionJoined
                                         from contactBy in contactByTransactionJoined.DefaultIfEmpty()
                                         select new
                                         {
                                             contactLatestDate.Id,
                                             contactLatestDate.SalesPersonId,
                                             contactLatestDate.CustomerTypeId,
                                             contactLatestDate.Status,
                                             contactLatestDate.HotnessId,
                                             contactLatestDate.SourceId,
                                             contactLatestDate.ExpectedDelTimingId,
                                             contactLatestDate.CreationTime,
                                             contactLatestDate.No,
                                             contactLatestDate.CustomerName,
                                             contactLatestDate.DistrictId,
                                             contactLatestDate.ProvinceId,
                                             contactLatestDate.Details,
                                             contactLatestDate.ContactDate,
                                             contactLatestDate.CustomerTel1,
                                             contactLatestDate.CompanyName,
                                             contactLatestDate.ManagerComment,
                                             cont.CurrentDate,
                                             cont.CurrentAction,
                                             cont.NextDate,
                                             cont.NextAction,
                                             ContactBy = contactBy.Vi_Description,
                                             cont.ContactById,
                                             contactLatestDate.IsShowRoomVisit,
                                             contactLatestDate.IsTestDrive,
                                             contactLatestDate.ReasonWhyNotTD,
                                             contactLatestDate.TestDriveDate,
                                             contactLatestDate.ReasonLost,
                                             contactLatestDate.OnPassiveFreezing
                                         };
            IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();
            IQueryable<MstSleGrades> grades = _mstSleGradesRepository.GetAll().AsNoTracking();

            var result = from customer in transactionInfoContact.Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                                                                .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                                                                .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date)
                                                                .Where(e => FreezeType == null || e.OnPassiveFreezing == FreezeType)

                         join person in _salesPersonRepository.GetAll().AsNoTracking() on customer.SalesPersonId equals person.Id into customerPersons
                         from customerPerson in customerPersons.DefaultIfEmpty()

                         join userUnit in _userOrganizationUnitRepository.GetAll().AsNoTracking() on customerPerson.UserId equals userUnit.UserId into userUnitPersons
                         from userUnitPerson in userUnitPersons.DefaultIfEmpty()

                         join unit in _organizationUnitRepository.GetAll().AsNoTracking() on userUnitPerson.OrganizationUnitId equals unit.Id into empDepartments
                         from empDepartment in empDepartments.DefaultIfEmpty()

                         join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStages
                         from customerStage in customerStages.DefaultIfEmpty()

                         join hotness in _mstSleHotnessRepository.GetAll().AsNoTracking() on customer.HotnessId equals hotness.Id into customerHotnesses
                         from customerHotness in customerHotnesses.DefaultIfEmpty()

                         join source in _mstSleSourceRepository.GetAll().AsNoTracking() on customer.SourceId equals source.Id into customerSources
                         from customerSource in customerSources.DefaultIfEmpty()

                         join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                         from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                         join car in customerVehicleInformation on customer.Id equals car.SalesCustomerId

                         select new GetLatestInfoContactCustomerDto()
                         {
                             SalesCustomerInfo = new SalesCustomerInfomationDto()
                             {
                                 Id = customer.Id,
                                 CreationTime = customer.CreationTime,
                                 CustomerNo = customer.No,
                                 Name = customer.CustomerName,
                                 DistrictId = customer.DistrictId,
                                 ProvinceId = customer.ProvinceId,
                                 Address = customer.Details,
                                 Source = customerSource.Vi_Description,
                                 Status = customerStage.Vi_Description,
                                 IsShowRoomVisit = customer.IsShowRoomVisit,
                                 IsTestDrive = customer.IsTestDrive,
                                 ReasonWhyNotTD = customer.ReasonWhyNotTD,
                                 FirstContactDate = customer.ContactDate,
                                 Hotness = customerHotness.Vi_Description,
                                 SalesPerson = customerPerson.FullName,
                                 CustomerType = customer.CustomerTypeId == 1 ? "Cá nhân" : "Doanh nghiệp",
                                 ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                 CustomerTel1 = customer.CustomerTel1,
                                 CompanyName = customer.CompanyName,
                                 ManagerComment = customer.ManagerComment,
                                 Department = empDepartment.DisplayName,
                                 SalesPersonId = customer.SalesPersonId,
                                 ReasonLost = customer.ReasonLost,
                                 CurrentDate = customer.CurrentDate,
                                 CurrentAction = customer.CurrentAction,
                                 NextContact = customer.NextDate,
                                 NextPlan = customer.NextAction,
                                 ContactBy = customer.ContactBy
                             }
                         };

            int totalCount = await result.CountAsync();

            List<GetLatestInfoContactCustomerDto> paged = await result.PageBy(input).ToListAsync();

            foreach (var page in paged)
            {
                page.CarAttention = (from vehicle in customerVehicleInformation.Where(e => e.SalesCustomerId == page.SalesCustomerInfo.Id && e.VehicleType == 1)
                                     join model in models.Where(e => e.MakeId == 1) on vehicle.ModelId equals model.Id

                                     join grade in grades on vehicle.GradeId equals grade.Id into vehicleGradeJoined
                                     from grade in vehicleGradeJoined.DefaultIfEmpty()

                                     select new GetListCarAttentionDto()
                                     {
                                         Model = model.MarketingCode,
                                         Grade = grade.MarketingCode,
                                     }).ToList();
            }

            return new PagedResultDto<GetLatestInfoContactCustomerDto>(
                totalCount,
                paged
            );
        }
        #endregion

        #region Thông tin các KH có số điện thoại trùng của đại lý
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View_Duplicate_Customer)]
        public PagedResultDto<GetLatestInfoContactCustomerDto> GetAllDuplicateCustomer(GetFilterSalesCustomerDto input)
        {
            //SysAppConfig checkSeeCusPhone = _sysAppConfigRepository.GetAll().AsNoTracking().Where(e => e.Code == "ACCP").FirstOrDefault();

            long? encoded = 0;

            encoded = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId).IsViewPhoneNumber == true ? 1 : 0;

            //if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

            List<long> listUserId = GetListUserToViewDuplicateCustomer();

            List<long> unitUserIds = new();
            if (input.UnitId != null)
            {
                unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
            }

            IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerName)
                               || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName)
                               || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                .Where(e => input.SourceId == null || e.SourceId == input.SourceId)
                .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
                .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
                .Where(e => input.ProvinceId == null || e.ProvinceId == input.ProvinceId)
                .Where(e => input.DistrictId == null || e.DistrictId == input.DistrictId)
                .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                .Where(c => c.IsDuplicatePhone != null && c.IsDuplicatePhone != 0)
                .Where(c => listUserId.Any(l => l == (long)c.CreatorUserId));

            IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking();

            IQueryable<SalesCustomerTransactionInfoContact> customerTransactionInfoContact = _customerTransactionInfoContactRepository.GetAll().AsNoTracking();
            IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();

            IQueryable<GetLatestInfoContactCustomerDto> query = from customer in salesCustomer.Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))
                                                                join person in _salesPersonRepository.GetAll().AsNoTracking() on customer.SalesPersonId equals person.Id into customerPersons
                                                                from customerPerson in customerPersons.DefaultIfEmpty()

                                                                join userUnit in _userOrganizationUnitRepository.GetAll().AsNoTracking() on customerPerson.UserId equals userUnit.UserId into userUnitPersons
                                                                from userUnitPerson in userUnitPersons.DefaultIfEmpty()

                                                                join unit in _organizationUnitRepository.GetAll().AsNoTracking() on userUnitPerson.OrganizationUnitId equals unit.Id into empDepartments
                                                                from empDepartment in empDepartments.DefaultIfEmpty()

                                                                join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStages
                                                                from customerStage in customerStages.DefaultIfEmpty()

                                                                join hotness in _mstSleHotnessRepository.GetAll().AsNoTracking() on customer.HotnessId equals hotness.Id into customerHotnesses
                                                                from customerHotness in customerHotnesses.DefaultIfEmpty()

                                                                join source in _mstSleSourceRepository.GetAll().AsNoTracking() on customer.SourceId equals source.Id into customerSources
                                                                from customerSource in customerSources.DefaultIfEmpty()

                                                                join transactionInfo in _customerTransactionInfoRepository.GetAll().AsNoTracking() on customer.Id equals transactionInfo.SalesCustomerId into customerTransactionInfos
                                                                from customerTransactionInfo in customerTransactionInfos.DefaultIfEmpty()

                                                                join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                                                                from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                                                                orderby customer.CustomerTel1, customerPerson.FullName, customer.CreationTime descending
                                                                select new GetLatestInfoContactCustomerDto()
                                                                {
                                                                    Id = customer.Id,
                                                                    SalesCustomerInfo = new SalesCustomerInfomationDto()
                                                                    {
                                                                        Id = customer.Id,
                                                                        CreationTime = customer.CreationTime,
                                                                        CustomerNo = customer.No,
                                                                        Name = customer.CustomerName,
                                                                        DistrictId = customer.DistrictId,
                                                                        ProvinceId = customer.ProvinceId,
                                                                        Address = customer.Details,
                                                                        Source = customerSource.Vi_Description,
                                                                        Status = customerStage.Vi_Description,
                                                                        IsShowRoomVisit = customerTransactionInfo.IsShowRoomVisit,
                                                                        IsTestDrive = customerTransactionInfo.IsTestDrive,
                                                                        ReasonWhyNotTD = customerTransactionInfo.ReasonWhyNotTD,
                                                                        TestDriveDate = customerTransactionInfo.TestDriveDate,
                                                                        FirstContactDate = customer.ContactDate,
                                                                        Hotness = customerHotness.Vi_Description,
                                                                        SalesPerson = customerPerson.FullName,
                                                                        CustomerType = customer.CustomerTypeId == 1 ? "Cá nhân" : "Doanh nghiệp",
                                                                        CustomerTel1 = encoded == 1 ? customer.CustomerTel1Decode : L("Encrypted"),
                                                                        CompanyName = customer.CompanyName,
                                                                        Department = empDepartment.DisplayName,
                                                                        SalesPersonId = customer.SalesPersonId,
                                                                        ManagerComment = customer.ManagerComment,
                                                                        ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                                                        ReasonLost = customerTransactionInfo.ReasonLost
                                                                    },

                                                                    CarAttention = (from customerVehicleInfo in customerVehicleInformation.Where(e => e.SalesCustomerId == customer.Id && e.VehicleType == 1)
                                                                                    join model in models.Where(e => e.MakeId == 1) on customerVehicleInfo.ModelId equals model.Id

                                                                                    select new GetListCarAttentionDto()
                                                                                    {
                                                                                        Model = model.MarketingCode,
                                                                                    }).ToList(),

                                                                    SalesCustomersContactInfo = (from transactionInfoContact in customerTransactionInfoContact.Where(e => e.SalesCustomerId == customer.Id).OrderByDescending(e => e.CurrentDate)
                                                                                                 select new SalesCustomersContactInfoDto()
                                                                                                 {
                                                                                                     CurrentDate = transactionInfoContact.CurrentDate,
                                                                                                     CurrentAction = transactionInfoContact.CurrentAction,
                                                                                                     NextContact = transactionInfoContact.NextDate,
                                                                                                     NextPlan = transactionInfoContact.NextAction,
                                                                                                 }
                                                                       ).FirstOrDefault()
                                                                };

            int totalCount = query.DistinctBy(e => e.Id).ToList().Count;

            List<GetLatestInfoContactCustomerDto> paged = query.DistinctBy(e => e.Id).AsQueryable().PageBy(input).ToList();

            return new PagedResultDto<GetLatestInfoContactCustomerDto>(
                totalCount,
                paged
            );
        }
        #endregion

        #region Lấy thông tin KH với filter Model - Grade của số điện thoại trùng
        public PagedResultDto<GetLatestInfoContactCustomerDto> GetDuplicateCustomerWithModelFilter(GetFilterSalesCustomerDto input)
        {
            //SysAppConfig checkSeeCusPhone = _sysAppConfigRepository.GetAll().Where(e => e.Code == "ACCP").FirstOrDefault();

            long? encoded = 0;

            encoded = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId).IsViewPhoneNumber == true ? 1 : 0;

            //if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

            List<long> unitUserIds = new();
            if (input.UnitId != null)
            {
                unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
            }

            IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerName)
                               || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName)
                               || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
                .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
                .Where(e => input.ProvinceId == null || e.ProvinceId == input.ProvinceId)
                .Where(e => input.DistrictId == null || e.DistrictId == input.DistrictId)
                .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                .Where(c => c.IsDuplicatePhone != null && c.IsDuplicatePhone != 0);

            IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking()
                .Where(e => input.ModelId == null || e.ModelId == input.ModelId)
                .Where(e => input.GradeId == null || e.GradeId == input.GradeId);

            IQueryable<SalesCustomerTransactionInfoContact> customerTransactionInfoContact = _customerTransactionInfoContactRepository.GetAll().AsNoTracking();
            IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();
            IQueryable<MstSleGrades> grades = _mstSleGradesRepository.GetAll().AsNoTracking();

            IQueryable<GetLatestInfoContactCustomerDto> result = from customer in salesCustomer.Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))

                                                                 join person in _salesPersonRepository.GetAll().AsNoTracking() on customer.SalesPersonId equals person.Id into customerPersons
                                                                 from customerPerson in customerPersons.DefaultIfEmpty()

                                                                 join userUnit in _userOrganizationUnitRepository.GetAll().AsNoTracking() on customerPerson.UserId equals userUnit.UserId into userUnitPersons
                                                                 from userUnitPerson in userUnitPersons.DefaultIfEmpty()

                                                                 join unit in _organizationUnitRepository.GetAll().AsNoTracking() on userUnitPerson.OrganizationUnitId equals unit.Id into empDepartments
                                                                 from empDepartment in empDepartments.DefaultIfEmpty()

                                                                 join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStages
                                                                 from customerStage in customerStages.DefaultIfEmpty()

                                                                 join hotness in _mstSleHotnessRepository.GetAll().AsNoTracking() on customer.HotnessId equals hotness.Id into customerHotnesses
                                                                 from customerHotness in customerHotnesses.DefaultIfEmpty()

                                                                 join source in _mstSleSourceRepository.GetAll().AsNoTracking() on customer.SourceId equals source.Id into customerSources
                                                                 from customerSource in customerSources.DefaultIfEmpty()

                                                                 join dealer in _mstGenDealerRepository.GetAll().AsNoTracking() on customer.TenantId equals dealer.Id into customerDealers
                                                                 from customerDealer in customerDealers.DefaultIfEmpty()

                                                                 join transactionInfo in _customerTransactionInfoRepository.GetAll().AsNoTracking() on customer.Id equals transactionInfo.SalesCustomerId into customerTransactionInfos
                                                                 from customerTransactionInfo in customerTransactionInfos.DefaultIfEmpty()

                                                                 join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                                                                 from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                                                                 join car in customerVehicleInformation on customer.Id equals car.SalesCustomerId

                                                                 select new GetLatestInfoContactCustomerDto()
                                                                 {
                                                                     Id = customer.Id,
                                                                     SalesCustomerInfo = new SalesCustomerInfomationDto()
                                                                     {
                                                                         Id = customer.Id,
                                                                         CreationTime = customer.CreationTime,
                                                                         CustomerNo = customer.No,
                                                                         Name = customer.CustomerName,
                                                                         DistrictId = customer.DistrictId,
                                                                         ProvinceId = customer.ProvinceId,
                                                                         Address = customer.Details,
                                                                         Source = customerSource.Vi_Description,
                                                                         Status = customerStage.Vi_Description,
                                                                         IsShowRoomVisit = customerTransactionInfo.IsShowRoomVisit,
                                                                         IsTestDrive = customerTransactionInfo.IsTestDrive,
                                                                         ReasonWhyNotTD = customerTransactionInfo.ReasonWhyNotTD,
                                                                         TestDriveDate = customerTransactionInfo.TestDriveDate,
                                                                         FirstContactDate = customer.ContactDate,
                                                                         Hotness = customerHotness.Vi_Description,
                                                                         SalesPerson = customerPerson.FullName,
                                                                         CustomerType = customer.CustomerTypeId == 1 ? "Cá nhân" : "Doanh nghiệp",
                                                                         ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                                                         CustomerTel1 = encoded == 1 ? customer.CustomerTel1Decode : L("Encrypted"),
                                                                         CompanyName = customer.CompanyName,
                                                                         ManagerComment = customer.ManagerComment,
                                                                         Department = empDepartment.DisplayName,
                                                                         SalesPersonId = customer.SalesPersonId,
                                                                         ReasonLost = customerTransactionInfo.ReasonLost
                                                                     },

                                                                     CarAttention = (from customerVehicleInfo in customerVehicleInformation.Where(e => e.SalesCustomerId == customer.Id && e.VehicleType == 1)
                                                                                     join model in models.Where(e => e.MakeId == 1) on customerVehicleInfo.ModelId equals model.Id

                                                                                     join grade in grades on customerVehicleInfo.GradeId equals grade.Id

                                                                                     select new GetListCarAttentionDto()
                                                                                     {
                                                                                         Model = model.MarketingCode,
                                                                                         Grade = grade.MarketingCode,
                                                                                     }).ToList(),

                                                                     SalesCustomersContactInfo = (from transactionInfoContact in customerTransactionInfoContact.Where(e => e.SalesCustomerId == customer.Id).OrderByDescending(e => e.CurrentDate)
                                                                                                  select new SalesCustomersContactInfoDto()
                                                                                                  {
                                                                                                      CurrentDate = transactionInfoContact.CurrentDate,
                                                                                                      CurrentAction = transactionInfoContact.CurrentAction,
                                                                                                      NextContact = transactionInfoContact.NextDate,
                                                                                                      NextPlan = transactionInfoContact.NextAction,
                                                                                                  }).FirstOrDefault()
                                                                 };

            int totalCount = result.DistinctBy(e => e.Id).ToList().Count;

            List<GetLatestInfoContactCustomerDto> paged = result.DistinctBy(e => e.Id).Skip(input.SkipCount).Take(input.MaxResultCount).ToList();

            return new PagedResultDto<GetLatestInfoContactCustomerDto>(
                totalCount,
                paged
            );
        }
        #endregion

        #region Thông tin KH chuyển giao    
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<PagedResultDto<SalesTransferCustomerForViewDto>> GetAllAssignmentCustomerForView(GetFilterAssignmentCustomerDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MustHaveTenant))
                {
                    if (input.ModelId > 0)
                    {
                        return await GetAssignmentCustomerModelFilterForView(input);
                    }

                    int? tenantId = AbpSession.TenantId;
                    List<long> ListUserId = GetListUserBySalesPersonPermission();

                    /*
                        TenantId != null -> Tenant đăng nhập là Dealer -> chỉ lấy ra thông của đại lý đó,
                        và cần lấy theo danh sách phân cấp User
                    */

                    IQueryable<SalesCustomer> salesCustomer = null;
                    IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = null;
                    if (tenantId != null)
                    {
                        salesCustomer = _customerRepository.GetAll().AsNoTracking()
                       .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter) || e.Campaign.Contains(input.Filter) || e.AgencyLead.Contains(input.Filter))
                       .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                       .Where(e => string.IsNullOrWhiteSpace(input.AgencyLead) || e.AgencyLead == input.AgencyLead)
                       .Where(e => string.IsNullOrWhiteSpace(input.Campaign) || e.Campaign.Contains(input.Campaign))
                       .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()) || e.CustomerName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                       .Where(e => e.TenantId == tenantId)
                       .Where(e => input.HotnessId == 0 || e.HotnessId == input.HotnessId)
                       .Where(e => input.SalesStageId == 0 || e.Status == input.SalesStageId)
                       .Where(e => input.SalesPersonId == 0 || e.SalesPersonId == input.SalesPersonId)
                       .Where(e => input.AssignPersonId == 0 || e.AssignPersonId == input.AssignPersonId)
                       .Where(e => input.SourceId == 0 || e.SourceId == input.SourceId)
                       .Where(e => input.CustomerDropDateFrom == null || e.Date.Value.Date >= input.CustomerDropDateFrom.Value.Date)
                       .Where(e => input.CustomerDropDateTo == null || e.Date.Value.Date <= input.CustomerDropDateTo.Value.Date)
                       .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                       .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                       .Where(e => input.AssignDateFrom == null || e.AssignDate.Value.Date >= input.AssignDateFrom.Value.Date)
                       .Where(e => input.AssignDateTo == null || e.AssignDate.Value.Date <= input.AssignDateTo.Value.Date)
                       .Where(c => ListUserId.Any(l => l == (long)c.CreatorUserId))
                       .Where(c => c.Time != null)
                       .WhereIf(input.IsContacted == 2, e => e.ContactResult == 0 || e.ContactResult == null || e.ContactResult == 1) // tất cả
                       .WhereIf(input.IsContacted == 1, e => e.ContactResult == 0) // Thất bại
                       .WhereIf(input.IsContacted == 3, e => e.ContactResult == 1) // Thành công
                       .WhereIf(input.IsContacted == 0, e => e.ContactDate == null && e.ContactResult == null)
                       .WhereIf(input.ImportSourceId == 0, e => e.ImportSource == 2 || e.ImportSource == 1 || e.ImportSource == 3 || e.ImportSource == 4) // All
                       .WhereIf(input.ImportSourceId == 1, e => e.ImportSource == 1) // TMV
                       .WhereIf(input.ImportSourceId == 2, e => e.ImportSource == 2) // DLR
                       .WhereIf(input.ImportSourceId == 3, e => e.ImportSource == 3) // API
                       .WhereIf(input.ImportSourceId == 4, e => e.ImportSource == 4) // Tạo thủ công
                       ;

                        customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking().Where(e => e.TenantId == tenantId && e.VehicleType == 1);
                    }

                    /*
                        TenantId == null -> Tenant đăng nhập là TMV -> chỉ lấy ra những thông tin với ImportSource = 1 (nguồn import là TMV),
                        không cần lấy theo danh sách phân cấp User
                    */
                    if (tenantId == null)
                    {
                        salesCustomer = _customerRepository.GetAll().DefaultIfEmpty()
                        .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter) || e.Campaign.Contains(input.Filter) || e.AgencyLead.Contains(input.Filter))
                        .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                        .Where(e => string.IsNullOrWhiteSpace(input.Campaign) || e.Campaign.Contains(input.Campaign))
                        .Where(e => string.IsNullOrWhiteSpace(input.AgencyLead) || e.AgencyLead == input.AgencyLead)
                        .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                        .Where(e => input.HotnessId == 0 || e.HotnessId == input.HotnessId)
                        .Where(e => input.SalesStageId == 0 || e.Status == input.SalesStageId)
                        .Where(e => input.DealerId == 0 || e.TenantId == input.DealerId)
                        .Where(e => input.SourceId == 0 || e.SourceId == input.SourceId)
                        .Where(e => input.CustomerDropDateFrom == null || e.Date.Value.Date >= input.CustomerDropDateFrom.Value.Date)
                        .Where(e => input.CustomerDropDateTo == null || e.Date.Value.Date <= input.CustomerDropDateTo.Value.Date)
                        .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                        .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                        .Where(e => input.AssignDateFrom == null || e.AssignDate.Value.Date >= input.AssignDateFrom.Value.Date)
                        .Where(e => input.AssignDateTo == null || e.AssignDate.Value.Date <= input.AssignDateTo.Value.Date)
                        .Where(c => c.Time != null)
                        .WhereIf(input.IsContacted == 2, e => e.ContactResult == 0 || e.ContactDate == null || e.ContactResult == 1)
                        .WhereIf(input.IsContacted == 3, e => e.ContactResult == 1)
                        .WhereIf(input.IsContacted == 1, e => e.ContactResult == 0)
                        .WhereIf(input.IsContacted == 0, e => e.ContactDate == null && e.ContactResult == null)
                        .WhereIf(input.ImportSourceId == 0, e => e.ImportSource == 2 || e.ImportSource == 1 || e.ImportSource == 3 || e.ImportSource == 4) // All
                        .WhereIf(input.ImportSourceId == 1, e => e.ImportSource == 1) // TMV
                        .WhereIf(input.ImportSourceId == 2, e => e.ImportSource == 2) // DLR
                        .WhereIf(input.ImportSourceId == 3, e => e.ImportSource == 3) // API
                        .WhereIf(input.ImportSourceId == 4, e => e.ImportSource == 4) // Tạo thủ công
                        ;

                        customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking().Where(e => e.VehicleType == 1);
                    }

                    long? encoded = 0;

                    encoded = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId).IsViewPhoneNumber == true ? 1 : 0;

                    //if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

                    IQueryable<MstSleSalesPerson> salesPerson = _salesPersonRepository.GetAll().AsNoTracking();
                    IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();

                    IQueryable<SalesTransferCustomerForViewDto> query = from customer in salesCustomer

                                                                        join person in salesPerson on customer.SalesPersonId equals person.Id into customerPersons
                                                                        from customerPerson in customerPersons.DefaultIfEmpty()

                                                                        join assign in salesPerson on customer.AssignPersonId equals assign.Id into customerAssigns
                                                                        from customerAssign in customerAssigns.DefaultIfEmpty()

                                                                        join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                                                                        from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                                                                        join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStageJoined
                                                                        from stage in customerStageJoined.DefaultIfEmpty()

                                                                        join cusTran in _customerTransactionInfoRepository.GetAll().AsNoTracking() on customer.Id equals cusTran.SalesCustomerId into customerCusTrans
                                                                        from customerCusTran in customerCusTrans.DefaultIfEmpty()

                                                                        join reasonOfNC in _mstSleReasonOfNCRepository.GetAll().AsNoTracking() on customerCusTran.ReasonOfNCId equals reasonOfNC.Id into customerReasonOfNCs
                                                                        from customerReasonOfNC in customerReasonOfNCs.DefaultIfEmpty()

                                                                        orderby customer.CreationTime descending
                                                                        select new SalesTransferCustomerForViewDto()
                                                                        {
                                                                            Id = customer.Id,
                                                                            No = customer.No,
                                                                            DealerId = customer.TenantId,
                                                                            CustomerName = customer.CustomerName,
                                                                            CustomerTel1 = encoded == 1 ? customer.CustomerTel1Decode : L("Encrypted"),
                                                                            CreationTime = customer.CreationTime,
                                                                            Date = customer.Date,
                                                                            Time = customer.Time,
                                                                            Email = customer.Email,
                                                                            SourceId = customer.SourceId,
                                                                            HotnessId = customer.HotnessId,
                                                                            ContactDate = customer.ContactDate,
                                                                            ContactResult = customer.ContactResult == 1 ? L("SalesCustomerContactedSuccess") : (customer.ContactResult == 0 ? L("SalesCustomerContactedFail") : L("SalesCustomerNotContacted")),
                                                                            SalesPerson = customerPerson.FullName,
                                                                            AssignPerson = customerAssign.FullName,
                                                                            AssignDate = customer.AssignDate,
                                                                            ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                                                            Campaign = customer.Campaign,
                                                                            Status = customer.Status == 0 ? L("SalesCustomerAssignmentCustomerShort") : stage.Vi_Description,
                                                                            ReasonOfFailContact = customer.ContactResult == 0 ? (customerCusTran.ReasonOfNCId == 5 ? customerCusTran.ReasonOfNCOther : customerReasonOfNC.Vi_Description) : "",
                                                                            AgencyLead = customer.AgencyLead
                                                                        };

                    int totalCount = await query.CountAsync();

                    var paged = await query.PageBy(input).ToListAsync();

                    foreach (var item in paged)
                    {
                        item.CarAttention = "";
                        var listModelAttention = (from vehicle in customerVehicleInformation.Where(e => e.SalesCustomerId == item.Id)
                                                  join model in models.Where(e => e.MakeId == 1) on vehicle.ModelId equals model.Id into modelVehicleJoined
                                                  from model in modelVehicleJoined.DefaultIfEmpty()

                                                  select new { Model = model.MarketingCode }).ToList();

                        if (listModelAttention.Any())
                        {
                            foreach (var m in listModelAttention)
                            {
                                item.CarAttention = item.CarAttention + m.Model + ", ";
                            }

                            if (item.CarAttention.Length > 2)
                            {
                                item.CarAttention = item.CarAttention[0..^2];
                            }
                        }
                    }


                    return new PagedResultDto<SalesTransferCustomerForViewDto>(
                        totalCount,
                        paged
                    );
                }
            }
        }
        #endregion

        #region Thông tin KH chuyển giao với filter Model
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<PagedResultDto<SalesTransferCustomerForViewDto>> GetAssignmentCustomerModelFilterForView(GetFilterAssignmentCustomerDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MustHaveTenant))
                {
                    int? tenantId = AbpSession.TenantId;
                    List<long> ListUserId = GetListUserBySalesPersonPermission();

                    /*
                        TenantId != null -> Tenant đăng nhập là Dealer -> chỉ lấy ra thông của đại lý đó,
                        và cần lấy theo danh sách phân cấp User
                    */

                    IQueryable<SalesCustomer> salesCustomer = null;
                    IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = null;
                    if (tenantId != null)
                    {
                        salesCustomer = _customerRepository.GetAll().AsNoTracking()
                       .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter) || e.Campaign.Contains(input.Filter) || e.AgencyLead.Contains(input.Filter))
                       .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                       .Where(e => string.IsNullOrWhiteSpace(input.AgencyLead) || e.AgencyLead == input.AgencyLead)
                       .Where(e => string.IsNullOrWhiteSpace(input.Campaign) || e.Campaign.Contains(input.Campaign))
                       .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                       .Where(e => e.TenantId == tenantId)
                       .Where(e => input.HotnessId == 0 || e.HotnessId == input.HotnessId)
                       .Where(e => input.SalesStageId == 0 || e.Status == input.SalesStageId)
                       .Where(e => input.SalesPersonId == 0 || e.SalesPersonId == input.SalesPersonId)
                       .Where(e => input.AssignPersonId == 0 || e.AssignPersonId == input.AssignPersonId)
                       .Where(e => input.SourceId == 0 || e.SourceId == input.SourceId)
                       .Where(e => input.CustomerDropDateFrom == null || e.Date.Value.Date >= input.CustomerDropDateFrom.Value.Date)
                       .Where(e => input.CustomerDropDateTo == null || e.Date.Value.Date <= input.CustomerDropDateTo.Value.Date)
                       .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                       .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                       .Where(e => input.AssignDateFrom == null || e.AssignDate.Value.Date >= input.AssignDateFrom.Value.Date)
                       .Where(e => input.AssignDateTo == null || e.AssignDate.Value.Date <= input.AssignDateTo.Value.Date)
                       .Where(c => ListUserId.Any(l => l == (long)c.CreatorUserId))
                       .Where(c => c.Time != null)
                       .WhereIf(input.IsContacted == 2, e => e.ContactResult == 0 || e.ContactResult == null || e.ContactResult == 1) // tất cả
                       .WhereIf(input.IsContacted == 1, e => e.ContactResult == 0) // Thất bại
                       .WhereIf(input.IsContacted == 3, e => e.ContactResult == 1) // Thành công
                       .WhereIf(input.IsContacted == 0, e => e.ContactDate == null && e.ContactResult == null)
                       .WhereIf(input.ImportSourceId == 0, e => e.ImportSource == 2 || e.ImportSource == 1 || e.ImportSource == 3 || e.ImportSource == 4) // All
                       .WhereIf(input.ImportSourceId == 1, e => e.ImportSource == 1) // TMV
                       .WhereIf(input.ImportSourceId == 2, e => e.ImportSource == 2) // DLR
                       .WhereIf(input.ImportSourceId == 3, e => e.ImportSource == 3) // API
                       .WhereIf(input.ImportSourceId == 4, e => e.ImportSource == 4) // Tạo thủ công
                       ;

                        customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking()
                            .Where(e => e.TenantId == tenantId)
                            .Where(e => input.ModelId == 0 || e.ModelId == input.ModelId);
                    }

                    /*
                        TenantId == null -> Tenant đăng nhập là TMV -> chỉ lấy ra những thông tin với ImportSource = 1 (nguồn import là TMV),
                        không cần lấy theo danh sách phân cấp User
                    */
                    if (tenantId == null)
                    {
                        salesCustomer = _customerRepository.GetAll().AsNoTracking()
                        .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter) || e.Campaign.Contains(input.Filter) || e.AgencyLead.Contains(input.Filter))
                        .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                        .Where(e => string.IsNullOrWhiteSpace(input.Campaign) || e.Campaign.Contains(input.Campaign))
                        .Where(e => string.IsNullOrWhiteSpace(input.AgencyLead) || e.AgencyLead == input.AgencyLead)
                        .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                        .Where(e => input.HotnessId == 0 || e.HotnessId == input.HotnessId)
                        .Where(e => input.SalesStageId == 0 || e.Status == input.SalesStageId)
                        .Where(e => input.DealerId == 0 || e.TenantId == input.DealerId)
                        .Where(e => input.SourceId == 0 || e.SourceId == input.SourceId)
                        .Where(e => input.CustomerDropDateFrom == null || e.Date.Value.Date >= input.CustomerDropDateFrom.Value.Date)
                        .Where(e => input.CustomerDropDateTo == null || e.Date.Value.Date <= input.CustomerDropDateTo.Value.Date)
                        .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                        .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                        .Where(e => input.AssignDateFrom == null || e.AssignDate.Value.Date >= input.AssignDateFrom.Value.Date)
                        .Where(e => input.AssignDateTo == null || e.AssignDate.Value.Date <= input.AssignDateTo.Value.Date)
                        .Where(c => c.Time != null)
                        .WhereIf(input.IsContacted == 2, e => e.ContactResult == 0 || e.ContactDate == null || e.ContactResult == 1)
                        .WhereIf(input.IsContacted == 3, e => e.ContactResult == 1)
                        .WhereIf(input.IsContacted == 1, e => e.ContactResult == 0)
                        .WhereIf(input.IsContacted == 0, e => e.ContactDate == null)
                        .WhereIf(input.ImportSourceId == 0, e => e.ImportSource == 2 || e.ImportSource == 1 || e.ImportSource == 3 || e.ImportSource == 4) // All
                        .WhereIf(input.ImportSourceId == 1, e => e.ImportSource == 1) // TMV
                        .WhereIf(input.ImportSourceId == 2, e => e.ImportSource == 2) // DLR
                        .WhereIf(input.ImportSourceId == 3, e => e.ImportSource == 3) // API
                        .WhereIf(input.ImportSourceId == 4, e => e.ImportSource == 4) // Tạo thủ công
                       ;

                        customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking().Where(e => input.ModelId == 0 || e.ModelId == input.ModelId);
                    }

                    long? encoded = 0;

                    encoded = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId).IsViewPhoneNumber == true ? 1 : 0;

                    //if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

                    IQueryable<MstSleSalesPerson> salesPerson = _salesPersonRepository.GetAll().AsNoTracking();
                    IQueryable<MstSleModel> models = _mstSleModelsRepository.GetAll().AsNoTracking();

                    var customerVehicleGroup = from customer in salesCustomer
                                               join vehicle in customerVehicleInformation on customer.Id equals vehicle.SalesCustomerId into customerVehicleJoined

                                               from vehicle in customerVehicleJoined.DefaultIfEmpty()

                                               group new { customer, vehicle }
                                               by new
                                               {
                                                   customer.Id,
                                                   customer.No,
                                                   customer.TenantId,
                                                   customer.CustomerName,
                                                   CustomerTel1 = encoded == 1 ? customer.CustomerTel1Decode : L("Encrypted"),
                                                   customer.CreationTime,
                                                   customer.Date,
                                                   customer.Time,
                                                   customer.Email,
                                                   customer.SourceId,
                                                   customer.HotnessId,
                                                   customer.ContactDate,
                                                   customer.ContactResult,
                                                   customer.AssignDate,
                                                   customer.Campaign,
                                                   customer.SalesPersonId,
                                                   customer.AssignPersonId,
                                                   customer.ExpectedDelTimingId,
                                                   customer.Status,
                                                   customer.AgencyLead,
                                               }
                                               into grouped
                                               select new
                                               {
                                                   grouped.Key.Id,
                                                   grouped.Key.No,
                                                   grouped.Key.TenantId,
                                                   grouped.Key.CustomerName,
                                                   grouped.Key.CustomerTel1,
                                                   grouped.Key.CreationTime,
                                                   grouped.Key.Date,
                                                   grouped.Key.Time,
                                                   grouped.Key.Email,
                                                   grouped.Key.SourceId,
                                                   grouped.Key.HotnessId,
                                                   grouped.Key.ContactDate,
                                                   grouped.Key.ContactResult,
                                                   grouped.Key.AssignDate,
                                                   grouped.Key.Campaign,
                                                   grouped.Key.SalesPersonId,
                                                   grouped.Key.AssignPersonId,
                                                   grouped.Key.ExpectedDelTimingId,
                                                   grouped.Key.Status,
                                                   grouped.Key.AgencyLead,
                                                   VehicleId = grouped.Max(e => e.vehicle.Id)
                                               };

                    IQueryable<SalesTransferCustomerForViewDto> query = from customer in customerVehicleGroup

                                                                        join person in salesPerson on customer.SalesPersonId equals person.Id into customerPersons
                                                                        from customerPerson in customerPersons.DefaultIfEmpty()

                                                                        join assign in salesPerson on customer.AssignPersonId equals assign.Id into customerAssigns
                                                                        from customerAssign in customerAssigns.DefaultIfEmpty()

                                                                        join expectedDelTiming in _mstSleExpectedDelTimingRepository.GetAll().AsNoTracking() on customer.ExpectedDelTimingId equals expectedDelTiming.Id into customerExpectedDelTimings
                                                                        from customerExpectedDelTiming in customerExpectedDelTimings.DefaultIfEmpty()

                                                                        join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on customer.Status equals stage.Id into customerStageJoined
                                                                        from stage in customerStageJoined.DefaultIfEmpty()

                                                                        join cusTran in _customerTransactionInfoRepository.GetAll().AsNoTracking() on customer.Id equals cusTran.SalesCustomerId into customerCusTrans
                                                                        from customerCusTran in customerCusTrans.DefaultIfEmpty()

                                                                        join reasonOfNC in _mstSleReasonOfNCRepository.GetAll().AsNoTracking() on customerCusTran.ReasonOfNCId equals reasonOfNC.Id into customerReasonOfNCs
                                                                        from customerReasonOfNC in customerReasonOfNCs.DefaultIfEmpty()

                                                                        join vehicle in customerVehicleInformation.Where(e => e.VehicleType == 1) on customer.VehicleId equals vehicle.Id

                                                                        join model in models.Where(e => e.MakeId == 1) on vehicle.ModelId equals model.Id into modelVehicleJoined
                                                                        from model in modelVehicleJoined.DefaultIfEmpty()

                                                                        orderby customer.CreationTime descending
                                                                        select new SalesTransferCustomerForViewDto()
                                                                        {
                                                                            Id = customer.Id,
                                                                            No = customer.No,
                                                                            DealerId = customer.TenantId,
                                                                            CustomerName = customer.CustomerName,
                                                                            CustomerTel1 = customer.CustomerTel1,
                                                                            CreationTime = customer.CreationTime,
                                                                            Date = customer.Date,
                                                                            Time = customer.Time,
                                                                            Email = customer.Email,
                                                                            SourceId = customer.SourceId,
                                                                            HotnessId = customer.HotnessId,
                                                                            ContactDate = customer.ContactDate,
                                                                            ContactResult = customer.ContactResult == 1 ? L("SalesCustomerContactedSuccess") : (customer.ContactResult == 0 ? L("SalesCustomerContactedFail") : L("SalesCustomerNotContacted")),
                                                                            SalesPerson = customerPerson.FullName,
                                                                            AssignPerson = customerAssign.FullName,
                                                                            AssignDate = customer.AssignDate,
                                                                            ExpectedDelTiming = customerExpectedDelTiming.Vi_Description,
                                                                            Campaign = customer.Campaign,
                                                                            Status = customer.Status == 0 ? L("SalesCustomerAssignmentCustomerShort") : stage.Vi_Description,
                                                                            ReasonOfFailContact = customer.ContactResult == 0 ? (customerCusTran.ReasonOfNCId == 5 ? customerCusTran.ReasonOfNCOther : customerReasonOfNC.Vi_Description) : "",
                                                                            CarAttention = model.MarketingCode,
                                                                            AgencyLead = customer.AgencyLead
                                                                        };

                    int totalCount = await query.CountAsync();

                    var paged = await query.PageBy(input).ToListAsync();

                    return new PagedResultDto<SalesTransferCustomerForViewDto>(
                        totalCount,
                        paged
                    );
                }
            }
        }
        #endregion

        #region Danh sách NVBH theo cây phân cấp nhân viên
        public async Task<List<GetListSalesPersonNameDto>> GetListSalesPersonName(string Filter)
        {
            List<long> listUserId = GetListUserBySalesPersonPermission();

            var person = from p in _salesPersonRepository.GetAll().AsNoTracking()
                         .Where(e => listUserId.Any(u => u == (long)e.UserId))
                         .Where(e => string.IsNullOrWhiteSpace(Filter) || e.FullName.Contains(Filter) || e.Email.Contains(Filter) || e.Phone.Contains(Filter) || e.Position.Contains(Filter))

                         orderby p.FullName ascending
                         select new GetListSalesPersonNameDto()
                         {
                             Id = p.Id,
                             FullName = p.FullName,
                             Position = p.Position,
                             Phone = p.Phone,
                             Email = p.Email,
                             Gender = p.Gender
                         };

            return await person.ToListAsync();
        }
        #endregion

        #region Lấy thông tin chi tiết của Khách hàng
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<SalesCustomerDetailDto> GetSalesCustomerDetails(long Id)
        {
            var customerDetail = await _customerRepository.FirstOrDefaultAsync(e => e.Id == Id);

            if (customerDetail != null)
            {
                var result = ObjectMapper.Map<SalesCustomerDetailDto>(customerDetail);

                var carAttention = from car in _customerVehicleInfoRepository.GetAll().AsNoTracking().Where(e => e.SalesCustomerId == Id)
                                   select new SalesCustomerCarAttentionDto
                                   {
                                       SalesCustomerId = car.SalesCustomerId,
                                       MakeId = car.MakeId,
                                       ModelId = car.ModelId,
                                       GradeId = car.GradeId,
                                       ColorId = car.ColorId,
                                       FARId = car.FARId,
                                       PurposeId = car.PurposeId,
                                       ReasonToChangeVehicle = car.ReasonToChangeVehicle,
                                       IsFavourite = car.IsFavourite,
                                       Id = car.Id
                                   };

                result.CustomerCarAttentions = await carAttention.ToListAsync();

                result.CustomerTel1 = DecodeAndGet(result.CustomerTel1);
                result.CustomerTel2 = DecodeAndGet(result.CustomerTel2);

                return result;
            }

            return null;
        }

        #region Tạo mới hoặc Cập nhật KH
        public async Task CreateOrEdit(SalesCustomerDetailDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Create)]
        protected virtual async Task Create(SalesCustomerDetailDto input)
        {
            int currentTenantId = (int)AbpSession.TenantId;

            var salesPersonId = _salesPersonRepository.FirstOrDefault(e => e.UserId == AbpSession.UserId)?.Id;

            input.CustomerTel1 = input.CustomerTel1.Replace(" ", "").Replace("-", "").Replace("+84", "0");
            input.CustomerTel1 = Regex.Replace(input.CustomerTel1, @"[^0-9]", string.Empty);

            input.CustomerTel1Decode = input.CustomerTel1;
            input.CustomerTel1 = Encode(input.CustomerTel1);

            if (!string.IsNullOrWhiteSpace(input.CustomerTel2))
            {
                input.CustomerTel2 = input.CustomerTel2.Replace(" ", "").Replace("-", "").Replace("+84", "0");
                input.CustomerTel2 = Regex.Replace(input.CustomerTel2, @"[^0-9]", string.Empty);
                input.CustomerTel2 = Encode(input.CustomerTel2);
            }

            input.No = CreateCustomerNo();

            var salesCustomer = ObjectMapper.Map<SalesCustomer>(input);
            salesCustomer.SalesPersonId = salesPersonId;
            salesCustomer.CustomerName = input.CustomerLastName + " " + input.CustomerMidName + " " + input.CustomerFirstName;
            salesCustomer.ContactResult = 1;
            salesCustomer.Remark = input.Remark ?? "";

            var customerInDB = _customerRepository.GetAll().Where(e => e.Status != 6 && e.Status != 8 && e.Status != 9 && e.CustomerTel1 == salesCustomer.CustomerTel1).ToList();
            if (customerInDB.Any())
            {
                foreach (var cus in customerInDB)
                {
                    if (cus.IsDuplicatePhone > 0)
                    {
                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }

                    if (cus.IsDuplicatePhone == 0 || cus.IsDuplicatePhone == null)
                    {
                        cus.IsDuplicatePhone = cus.Id;
                        _customerRepository.Update(cus);

                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }
                }
            }

            long customerId = await _customerRepository.InsertAndGetIdAsync(salesCustomer);

            if (input.CustomerCarAttentions.Any())
            {
                foreach (var vehicle in input.CustomerCarAttentions)
                {
                    vehicle.SalesCustomerId = customerId;

                    await CreateOrEditCarAttention(vehicle);
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Edit)]
        protected virtual async Task Update(SalesCustomerDetailDto input)
        {
            var salesCustomer = await _customerRepository.FirstOrDefaultAsync((long)input.Id);
            salesCustomer.CustomerName = input.CustomerLastName + " " + input.CustomerMidName + " " + input.CustomerFirstName;

            input.CustomerTel1 = input.CustomerTel1.Replace(" ", "").Replace("-", "").Replace("+84", "0");
            input.CustomerTel1 = Regex.Replace(input.CustomerTel1, @"[^0-9]", string.Empty);

            salesCustomer.CustomerTel1Decode = input.CustomerTel1;
            input.CustomerTel1 = Encode(input.CustomerTel1);
            input.ContactResult = 1;
            salesCustomer.Remark = input.Remark ?? "";

            var customerInDB = _customerRepository.GetAll().Where(e => e.Status != 6 && e.Status != 8 && e.Status != 9 && e.CustomerTel1 == salesCustomer.CustomerTel1 && e.Id != input.Id).ToList();
            if (customerInDB.Any())
            {
                foreach (var cus in customerInDB)
                {
                    if (cus.IsDuplicatePhone > 0)
                    {
                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }

                    if (cus.IsDuplicatePhone == 0 || cus.IsDuplicatePhone == null)
                    {
                        cus.IsDuplicatePhone = cus.Id;
                        _customerRepository.Update(cus);

                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(input.CustomerTel2))
            {
                input.CustomerTel2 = input.CustomerTel2.Replace(" ", "").Replace("-", "").Replace("+84", "0");
                input.CustomerTel2 = Regex.Replace(input.CustomerTel2, @"[^0-9]", string.Empty);
                input.CustomerTel2 = Encode(input.CustomerTel2);
            }

            if (salesCustomer.HotnessId != input.HotnessId)
            {
                await _customerHotnessRepository.InsertAsync(new SalesCustomerHotness
                {
                    CustomerId = input.Id,
                    DateChange = DateTime.Now,
                    HotnessId = input.HotnessId
                });
            }
            else
            {
                input.DateNeedContactHWC = salesCustomer.DateNeedContactHWC;
                input.ResultContactHWC = salesCustomer.ResultContactHWC ?? false;
                salesCustomer.IsSendNotificationHW ??= false;
            }

            if (salesCustomer.Status != input.Status)
            {
                var listTransaction = _customerTransactionInfoRepository.GetAll().AsNoTracking().Where(sc => sc.SalesCustomerId == input.Id).Select(sc => sc.Id).ToList();

                foreach (var transaction in listTransaction)
                {
                    await _customerTransactionInfoRepository.DeleteAsync(transaction);
                }

                if (input.Status == 8 || input.Status == 9)
                {
                    SalesCustomerTransactionInfo trans = new()
                    {
                        SalesCustomerId = salesCustomer.Id,
                        SalesStageId = 9,
                        ReasonLost = "Đóng băng bởi TVBH",
                        IsShowRoomVisit = false,
                        IsTestDrive = false,
                        ActivityDate = DateTime.Today.Date,
                        StatusApprove = 1,
                        OnPassiveFreezing = false,
                    };

                    await _customerTransactionInfoRepository.InsertAsync(trans);

                    //UserIdentifier user = new(AbpSession.TenantId, (long)salesCustomer.CreatorUserId);

                    //var SendUserName = _userRepository.FirstOrDefault(e => e.Id == AbpSession.UserId).FullName;

                    //await _appNotifier.SendMessageAsync(
                    //        user,
                    //        $"Yêu cầu phê duyệt Khách hàng đóng băng từ {SendUserName} về Khách hàng {salesCustomer.CustomerName} có mã {salesCustomer.No}",
                    //        Abp.Notifications.NotificationSeverity.Warn);
                }
                else
                {
                    var salesCustomerTransaction = new SalesCustomerTransactionInfo()
                    {
                        SalesStageId = input.Status,
                        ActivityDate = DateTime.Now,
                        StatusApprove = 1
                    };

                    await _customerTransactionInfoRepository.InsertAsync(salesCustomerTransaction);
                }

            }

            if (input.CustomerCarAttentions.Any())
            {
                foreach (var vehicle in input.CustomerCarAttentions)
                {
                    vehicle.SalesCustomerId = salesCustomer.Id;

                    await CreateOrEditCarAttention(vehicle);
                }
            }

            ObjectMapper.Map(input, salesCustomer);

        }

        private string GetCustomerInfo(long? customerId)
        {
            var customerInfo = _customerRepository.Get(customerId ?? 0);
            var nameCustomer = (customerInfo.CustomerTypeId == 1 ? customerInfo.CustomerName : customerInfo.CompanyName);
            var listModel = (from scv in _customerVehicleInfoRepository.GetAll().Where(c => c.SalesCustomerId == customerId && c.VehicleType == 1)
                             join m in _mstSleModelsRepository.GetAll() on scv.ModelId equals m.Id
                             select m.MarketingCode).ToArray();

            var nameModel = string.Join(", ", listModel.ToArray());
            return $"{nameCustomer} - {nameModel}";
        }
        #endregion

        #endregion

        #region Lấy thông tin Xe quan tâm
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<List<SalesCustomerCarAttentionDto>> GetCustomerCarAttention(long? customerId)
        {
            var carAttention = from car in _customerVehicleInfoRepository.GetAll().Where(e => e.SalesCustomerId == customerId && e.VehicleType == 1).OrderByDescending(e => e.Id)

                               orderby car.CreationTime descending
                               select new SalesCustomerCarAttentionDto()
                               {
                                   Id = car.Id,
                                   MakeId = car.MakeId,
                                   ModelId = car.ModelId,
                                   GradeId = car.GradeId,
                                   ColorId = car.ColorId,
                                   FARId = car.FARId,
                                   PurposeId = car.PurposeId,
                                   ReasonToChangeVehicle = car.ReasonToChangeVehicle,
                                   SalesCustomerId = car.SalesCustomerId,
                                   IsFavourite = car.IsFavourite
                               };

            return await carAttention.ToListAsync();
        }

        #region Tạo mới hoặc cập nhật Xe quan tâm
        public async Task CreateOrEditCarAttention(SalesCustomerCarAttentionDto input)
        {
            if (input.Id == null)
            {
                await CreateCarAttention(input);
            }
            else
            {
                await UpdateCarAttention(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Create)]
        protected virtual async Task CreateCarAttention(SalesCustomerCarAttentionDto input)
        {
            var carAttention = ObjectMapper.Map<SalesCustomerVehicleInfo>(input);
            carAttention.VehicleType = 1;

            await _customerVehicleInfoRepository.InsertAsync(carAttention);
        }

        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Edit)]
        protected virtual async Task UpdateCarAttention(SalesCustomerCarAttentionDto input)
        {
            var carAttention = await _customerVehicleInfoRepository.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, carAttention);
        }
        #endregion

        #region Xóa xe quan tâm
        public async Task DeleteCarAttention(long Id)
        {
            await _customerVehicleInfoRepository.DeleteAsync(Id);
        }
        #endregion

        #endregion

        #region Lấy thông tin Lái thử xe
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<List<SalesCustomerTestDriveDto>> GetCustomerTestDrive(long? customerId)
        {
            var testDrives = from testDrive in _customerTransactionInfoRepository.GetAll().Where(e => e.SalesCustomerId == customerId).OrderByDescending(e => e.Id)
                             select new SalesCustomerTestDriveDto()
                             {
                                 Id = testDrive.Id,
                                 TestDriveDate = testDrive.TestDriveDate,
                                 SupporterId = testDrive.SupporterId,
                                 RouteId = testDrive.RouteId,
                                 SalesCustomerId = testDrive.SalesCustomerId
                             };

            return await testDrives.ToListAsync();
        }

        #region Tạo mới hoặc cập nhật Lái thử xe
        //public async Task CreateOrEditTestDrive(SalesCustomerTestDriveDto input)
        //{
        //    if (input.Id == null)
        //    {
        //        await CreateTestDrive(input);
        //    }
        //    else
        //    {
        //        await UpdateTestDrive(input);
        //    }
        //}

        //[AbpAuthorize(AppPermissions.Pages_SalesCustomer_Create)]
        //protected virtual async Task CreateTestDrive(SalesCustomerTestDriveDto input)
        //{
        //    var currentUserId = AbpSession.UserId;
        //    var salesPerson = _salesPersonRepository.FirstOrDefaultAsync(e => e.UserId == currentUserId);
        //    //if (salesPerson != null)
        //    //{
        //    //    var testDrive = ObjectMapper.Map<SalesToDoList>(input);
        //    //    testDrive.DealerId = AbpSession.TenantId;
        //    //    testDrive.SalePersonId = salesPerson.Id;

        //    //    await _salesToDoListRepository.InsertAsync(testDrive);
        //    //}

        //    if (salesPerson == null)
        //    {
        //        throw new UserFriendlyException(00, L("CannotFindSalesPerson"));
        //    }
        //}

        //[AbpAuthorize(AppPermissions.Pages_SalesCustomer_Edit)]
        //protected virtual async Task UpdateTestDrive(SalesCustomerTestDriveDto input)
        //{
        //    //var testDrive = await _salesToDoListRepository.FirstOrDefaultAsync((long)input.Id);

        //    //ObjectMapper.Map(input, testDrive);
        //}
        #endregion
        #endregion

        #region Thông tin Liên hệ KH
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public async Task<List<SalesCustomerContactInfoDto>> GetCustomerContactInfo(long? customerId)
        {
            var contacts = from contact in _customerTransactionInfoContactRepository.GetAll().Where(e => e.SalesCustomerId == customerId).OrderByDescending(e => e.Id)
                           select new SalesCustomerContactInfoDto()
                           {
                               Id = contact.Id,
                               ContactById = contact.ContactById,
                               CreationTime = contact.CreationTime,
                               CurrentAction = contact.CurrentAction,
                               CurrentDate = contact.CurrentDate,
                               NextAction = contact.NextAction,
                               NextDate = contact.NextDate,
                               NextActionStatus = contact.NextActionStatus,
                               SalesCustomerId = contact.SalesCustomerId
                           };

            return await contacts.ToListAsync();
        }

        #region Tạo mới hoặc cập nhật Thông tin liên hệ của KH
        public async Task CreateOrEditContactHistory(SalesCustomerContactInfoDto input)
        {
            if (input.Id == null)
            {
                await CreateContactHistory(input);
            }
            else
            {
                await UpdateContactHistory(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Create)]
        protected virtual async Task CreateContactHistory(SalesCustomerContactInfoDto input)
        {
            var ContactHistory = ObjectMapper.Map<SalesCustomerTransactionInfoContact>(input);

            await _customerTransactionInfoContactRepository.InsertAsync(ContactHistory);
        }

        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Edit)]
        protected virtual async Task UpdateContactHistory(SalesCustomerContactInfoDto input)
        {
            var ContactHistory = await _customerTransactionInfoContactRepository.FirstOrDefaultAsync((long)input.Id);

            ObjectMapper.Map(input, ContactHistory);
        }
        #endregion

        #endregion

        #region Chỉ định Khách hàng cho SalesPerson
        public async Task AssignCustomerToSalesPerson(AssignCustomerToSalesPersonDto input)
        {
            var tenantId = AbpSession.TenantId;
            var userId = AbpSession.UserId;
            var countCustomer = input.CustomerIds.Count;
            var countPerson = input.SalesPersonIds.Count;

            var quotient = countCustomer / countPerson;
            var remainder = countCustomer % countPerson;

            int i = 0; int j = 0; int customerNumber = quotient;

            if (remainder > 0)
            {
                customerNumber = quotient + 1;
                remainder--;
            }

            foreach (var customerId in input.CustomerIds)
            {
                var creator = _salesPersonRepository.FirstOrDefault(e => e.Id == input.SalesPersonIds[i]).UserId;
                var salesCustomer = _customerRepository.GetAll().FirstOrDefault(e => e.Id == customerId);

                salesCustomer.AssignPersonId = _salesPersonRepository.FirstOrDefault(e => e.UserId == userId).Id;
                salesCustomer.SalesPersonId = input.SalesPersonIds[i];
                salesCustomer.AssignDate = DateTime.Now;
                salesCustomer.CreatorUserId = creator;

                UserIdentifier user = new(tenantId, (long)creator);

                await _appNotifier.SendMessageAsync(
                    user,
                    $"Bạn đã chỉ định chăm sóc khách hàng {salesCustomer.CustomerName}",
                    Abp.Notifications.NotificationSeverity.Info);

                j++;

                if (j == customerNumber)
                {
                    j = 0; i++;
                    customerNumber = quotient;
                    if (remainder > 0)
                    {
                        customerNumber = quotient + 1;
                        remainder--;
                    }
                }
            }
        }
        #endregion

        #region Đếm số KH có yêu cầu mua xe và KH TN
        /// <summary>
        /// Đếm số KH có yêu cầu mua xe và KH TN
        /// </summary>
        /// <param name="GetFilterSalesCustomerDto"></param>
        /// <returns>int</returns>
        public async Task<SalesCustomerFooterCount> CountCarPurchaseRequest(GetFilterSalesCustomerDto input)
        {
            SalesCustomerFooterCount result = new()
            {
                TotalCarAttention = 0,
                TotalInquiryCustomer = 0
            };

            List<long> ListUserId = GetListUserBySalesPersonPermission();
            List<long> unitUserIds = new();
            if (input.UnitId != null)
            {
                unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
            }

            if (input.ModelId != null)
            {
                //return await GetAllCustomerWithModelFilter(input);
            }

            IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1Decode.Contains(input.Filter))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                .Where(e => input.SourceId == null || e.SourceId == input.SourceId)
                .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
                .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
                .Where(e => input.ProvinceId == -1 || e.ProvinceId == input.ProvinceId)
                .Where(e => input.DistrictId == -1 || e.DistrictId == input.DistrictId)
                .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                .Where(c => c.CustomerTypeId == input.CustomerTypeId && ListUserId.Any(l => l == (long)c.CreatorUserId))
                .Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))
                .WhereIf(input.CustomerClass == 1, c => c.Status != 8 && c.Status != 9 && c.Status != 6 && c.ContactResult == 1) // KH tiềm năng
                .WhereIf(input.CustomerClass == 2, c => c.Status == 9) // KH đóng băng
                .WhereIf(input.CustomerClass == 3, c => c.Status == 8) // KH mất khách
                .WhereIf(input.CustomerClass == 4, c => c.Status == 6) // KH xuất hóa đơn
                ;

            var transactionInfoContacts = _customerTransactionInfoContactRepository.GetAll().AsNoTracking()
.Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date);
            IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking()
                            .Where(e => input.ModelId == null || e.ModelId == input.ModelId)
                            .Where(e => input.GradeId == null || e.GradeId == input.GradeId);

            // Lấy ra KH có ngày lịch sử liên hệ là mới nhất tương đương với Id lớn hơn
            var customerLatestContactDate = from customers in salesCustomer.Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId))
                                            join contacts in transactionInfoContacts on customers.Id equals contacts.SalesCustomerId into customerContactJoined
                                            from contacts in customerContactJoined.DefaultIfEmpty()

                                            group new { customers, contacts }
                                            by new
                                            { customers.Id, customers.Status } into grouped
                                            select new
                                            {
                                                grouped.Key.Id,
                                                grouped.Key.Status,
                                                TransactionInfoContactId = grouped.Max(e => e.contacts.Id),
                                            };

            // Join lại với bảng SalesCustomerTransactionInfoContact để lấy ra các thông tin cần thiết khác
            var transactionInfoContact = from contactLatestDate in customerLatestContactDate
                                         join cont in transactionInfoContacts on contactLatestDate.TransactionInfoContactId equals cont.Id into contactLatestDateJoined
                                         from cont in contactLatestDateJoined.DefaultIfEmpty()

                                         join contactBy in _mstSleContactByRepository.GetAll() on cont.ContactById equals contactBy.Id into contactByTransactionJoined
                                         from contactBy in contactByTransactionJoined.DefaultIfEmpty()
                                         select new
                                         {
                                             contactLatestDate.Id,
                                             cont.CurrentDate,
                                             cont.ContactById,
                                             contactLatestDate.Status
                                         };
            var inquiryFilter = transactionInfoContact.Where(e => input.ContactById == 0 || e.ContactById == input.ContactById)
                                                .Where(e => input.ContactHistoryDateFrom == null || e.CurrentDate.Value.Date >= input.ContactHistoryDateFrom.Value.Date)
                                                .Where(e => input.ContactHistoryDateTo == null || e.CurrentDate.Value.Date <= input.ContactHistoryDateTo.Value.Date);

            var carAttentionFilter = from attention in inquiryFilter
                                     join vehicle in customerVehicleInformation on attention.Id equals vehicle.SalesCustomerId
                                     select vehicle.Id;

            int inquiry = await inquiryFilter.CountAsync();

            int carAttention = await carAttentionFilter.CountAsync();

            result.TotalInquiryCustomer = inquiry > 0 ? inquiry : 0;
            result.TotalCarAttention = carAttention > 0 ? carAttention : 0;

            return result;
        }
        #endregion

        #region Bộ đếm khách hàng chuyển giao
        public List<int> GetCountSalesAssignmentCustomer(GetFilterAssignmentCustomerDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MustHaveTenant))
                {
                    int? tenantId = AbpSession.TenantId;
                    List<long> ListUserId = GetListUserBySalesPersonPermission();

                    /*
                        TenantId != null -> Tenant đăng nhập là Dealer -> chỉ lấy ra thông của đại lý đó,
                        và cần lấy theo danh sách phân cấp User
                    */

                    IQueryable<SalesCustomer> salesCustomer = null;
                    IQueryable<SalesCustomerVehicleInfo> customerVehicleInformation = null;
                    if (tenantId != null)
                    {
                        salesCustomer = _customerRepository.GetAll().AsNoTracking()
                       .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter) || e.Campaign.Contains(input.Filter) || e.AgencyLead.Contains(input.Filter))
                       .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                       .Where(e => string.IsNullOrWhiteSpace(input.AgencyLead) || e.AgencyLead == input.AgencyLead)
                       .Where(e => string.IsNullOrWhiteSpace(input.Campaign) || e.Campaign.Contains(input.Campaign))
                       .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                       .Where(e => e.TenantId == tenantId)
                       .Where(e => input.AssignPersonId == 0 || e.AssignPersonId == input.AssignPersonId)
                       .Where(e => input.HotnessId == 0 || e.HotnessId == input.HotnessId)
                       .Where(e => input.SourceId == 0 || e.SourceId == input.SourceId)
                       .Where(e => input.SalesStageId == 0 || e.Status == input.SalesStageId)
                       .Where(e => input.SalesPersonId == 0 || e.SalesPersonId == input.SalesPersonId)
                       .Where(e => input.CustomerDropDateFrom == null || e.Date.Value.Date >= input.CustomerDropDateFrom.Value.Date)
                       .Where(e => input.CustomerDropDateTo == null || e.Date.Value.Date <= input.CustomerDropDateTo.Value.Date)
                       .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                       .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                       .Where(e => input.AssignDateFrom == null || e.AssignDate.Value.Date >= input.AssignDateFrom.Value.Date)
                       .Where(e => input.AssignDateTo == null || e.AssignDate.Value.Date <= input.AssignDateTo.Value.Date)
                       .Where(c => ListUserId.Any(l => l == (long)c.CreatorUserId))
                       .Where(c => c.Time != null)
                       .WhereIf(input.IsContacted == 2, e => e.ContactResult == 0 || e.ContactResult == null || e.ContactResult == 1) // tất cả
                       .WhereIf(input.IsContacted == 1, e => e.ContactResult == 0) // Thất bại
                       .WhereIf(input.IsContacted == 3, e => e.ContactResult == 1) // Thành công
                       .WhereIf(input.IsContacted == 0, e => e.ContactDate == null)
                       .WhereIf(input.ImportSourceId == 0, e => e.ImportSource == 2 || e.ImportSource == 1 || e.ImportSource == 3 || e.ImportSource == 4) // All
                       .WhereIf(input.ImportSourceId == 1, e => e.ImportSource == 1) // TMV
                       .WhereIf(input.ImportSourceId == 2, e => e.ImportSource == 2) // DLR
                       .WhereIf(input.ImportSourceId == 3, e => e.ImportSource == 3) // API
                       .WhereIf(input.ImportSourceId == 4, e => e.ImportSource == 4) // Tạo thủ công
                       ;

                        customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking()
                            .Where(e => e.TenantId == tenantId)
                            .Where(e => input.ModelId == 0 || e.ModelId == input.ModelId);
                    }

                    /*
                        TenantId == null -> Tenant đăng nhập là TMV -> chỉ lấy ra những thông tin với ImportSource = 1 (nguồn import là TMV),
                        không cần lấy theo danh sách phân cấp User
                    */
                    if (tenantId == null)
                    {
                        salesCustomer = _customerRepository.GetAll().DefaultIfEmpty()
                        .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1.Contains(input.Filter) || e.Campaign.Contains(input.Filter) || e.AgencyLead.Contains(input.Filter))
                        .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
                        .Where(e => string.IsNullOrWhiteSpace(input.Campaign) || e.Campaign.Contains(input.Campaign))
                        .Where(e => string.IsNullOrWhiteSpace(input.AgencyLead) || e.AgencyLead == input.AgencyLead)
                        .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
                        .Where(e => input.HotnessId == 0 || e.HotnessId == input.HotnessId)
                        .Where(e => input.SalesStageId == 0 || e.Status == input.SalesStageId)
                        .Where(e => input.DealerId == 0 || e.TenantId == input.DealerId)
                        .Where(e => input.SourceId == 0 || e.SourceId == input.SourceId)
                        .Where(e => input.CustomerDropDateFrom == null || e.Date.Value.Date >= input.CustomerDropDateFrom.Value.Date)
                        .Where(e => input.CustomerDropDateTo == null || e.Date.Value.Date <= input.CustomerDropDateTo.Value.Date)
                        .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
                        .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
                        .Where(e => input.AssignDateFrom == null || e.AssignDate.Value.Date >= input.AssignDateFrom.Value.Date)
                        .Where(e => input.AssignDateTo == null || e.AssignDate.Value.Date <= input.AssignDateTo.Value.Date)
                        .Where(c => c.Time != null)
                        .WhereIf(input.IsContacted == 2, e => e.ContactResult == 0 || e.ContactDate == null || e.ContactResult == 1)
                        .WhereIf(input.IsContacted == 3, e => e.ContactResult == 1)
                        .WhereIf(input.IsContacted == 1, e => e.ContactResult == 0)
                        .WhereIf(input.IsContacted == 0, e => e.ContactDate == null)
                        .WhereIf(input.ImportSourceId == 0, e => e.ImportSource == 2 || e.ImportSource == 1 || e.ImportSource == 3 || e.ImportSource == 4) // All
                        .WhereIf(input.ImportSourceId == 1, e => e.ImportSource == 1) // TMV
                        .WhereIf(input.ImportSourceId == 2, e => e.ImportSource == 2) // DLR
                        .WhereIf(input.ImportSourceId == 3, e => e.ImportSource == 3) // API
                        .WhereIf(input.ImportSourceId == 4, e => e.ImportSource == 4) // Tạo thủ công
                        ;

                        customerVehicleInformation = _customerVehicleInfoRepository.GetAll().AsNoTracking().Where(e => input.ModelId == 0 || e.ModelId == input.ModelId);
                    }

                    IQueryable<MstSleSalesPerson> salesPerson = _salesPersonRepository.GetAll().AsNoTracking();

                    var customerVehicleGroup = from customer in salesCustomer
                                               join vehicle in customerVehicleInformation on customer.Id equals vehicle.SalesCustomerId

                                               group new { customer.Id, customer.ContactResult, VehicleId = vehicle.Id }
                                               by new
                                               {
                                                   customer.Id,
                                                   customer.ContactResult,
                                               }
                                               into grouped
                                               select new
                                               {
                                                   grouped.Key.Id,
                                                   grouped.Key.ContactResult,
                                                   VehicleId = grouped.Max(e => e.VehicleId)
                                               };

                    var result = new List<int>
                    {
                        customerVehicleGroup.Count(),
                        customerVehicleGroup.Where(e => e.ContactResult == 1).Count(),
                        customerVehicleGroup.Where(e => e.ContactResult == 0).Count(),
                        customerVehicleGroup.Where(e => e.ContactResult == null).Count()
                    };

                    return result;
                }
            }
        }
        #endregion

        #region "Lịch sử chi tiết của KH"
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_View)]
        public List<List<SalesCustomerHistoryDto>> GetSalesCustomerHistory(long? salesCustomerId)
        {
            var list = new List<List<SalesCustomerHistoryDto>>();
            var listHotnessHistory = (from cusHot in _customerHotnessRepository.GetAll().AsNoTracking().Where(e => e.CustomerId == salesCustomerId)
                                      join hotness in _mstSleHotnessRepository.GetAll().AsNoTracking() on cusHot.HotnessId equals hotness.Id into hotnessJoined
                                      from hotness in hotnessJoined.DefaultIfEmpty()

                                      select new SalesCustomerHistoryDto()
                                      {
                                          Date = cusHot.DateChange,
                                          Status = hotness.Vi_Description
                                      }).ToList();
            list.Add(listHotnessHistory);

            var listContactHistory = (from cusTranContact in _customerTransactionInfoContactRepository.GetAll().AsNoTracking().Where(e => e.SalesCustomerId == salesCustomerId)
                                      join contactBy in _mstSleContactByRepository.GetAll().AsNoTracking() on cusTranContact.ContactById equals contactBy.Id into contactByJoined
                                      from contactBy in contactByJoined.DefaultIfEmpty()

                                      select new SalesCustomerHistoryDto()
                                      {
                                          Date = cusTranContact.CurrentDate,
                                          Status = contactBy.Vi_Description,
                                          Content = cusTranContact.CurrentAction
                                      }).ToList();
            list.Add(listContactHistory);

            var listTransactionHistory = new List<SalesCustomerHistoryDto>();
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.SoftDelete))
            {
                listTransactionHistory = (from cusTran in _customerTransactionInfoRepository.GetAll().AsNoTracking().Where(e => e.SalesCustomerId == salesCustomerId)
                                          join stage in _mstSleSalesStageRepository.GetAll().AsNoTracking() on cusTran.SalesStageId equals stage.Id into stageJoined
                                          from stage in stageJoined.DefaultIfEmpty()

                                          select new SalesCustomerHistoryDto()
                                          {
                                              Date = cusTran.ActivityDate,
                                              Status = stage.Vi_Description
                                          }).ToList();
            }
            list.Add(listTransactionHistory);

            var listManagerHistory = (from cusManCom in _customerManagerCommentRepository.GetAll().AsNoTracking().Where(e => e.SalesCustomerId == salesCustomerId)
                                      select new SalesCustomerHistoryDto()
                                      {
                                          Date = cusManCom.SeekManagerTime,
                                          Status = cusManCom.SeekContentForManager
                                      }).ToList();
            list.Add(listManagerHistory);
            return list.ToList();
        }
        #endregion

        #region DS KH Đóng băng
        //public async Task<PagedResultDto<GetLatestInfoContactCustomerDto>> GetAllFreezeCustomer(GetFilterSalesCustomerDto input)
        //{
        //    bool? FreezeType = null;

        //    if (input.FreezeType == 3) FreezeType = null;
        //    if (input.FreezeType == 1) FreezeType = true; // Bị động
        //    if (input.FreezeType == 2) FreezeType = false; // Chủ động

        //    SysAppConfig checkSeeCusPhone = _sysAppConfigRepository.GetAll().Where(e => e.Code == "ACCP").FirstOrDefault();
        //    long? encoded = 0;

        //    if (checkSeeCusPhone != null) encoded = checkSeeCusPhone.Value ?? 1;

        //    List<long> ListUserId = GetListUserBySalesPersonPermission();
        //    List<long> unitUserIds = new List<long>();
        //    if (input.UnitId != null)
        //    {
        //        unitUserIds = GetOrganiztionUnitsByUnitId((long)input.UnitId);
        //    }

        //    if (input.ModelId != null)
        //    {
        //        return await GetAllCustomerWithModelFilter(input);
        //    }

        //    IQueryable<SalesCustomer> salesCustomer = _customerRepository.GetAll().AsNoTracking()
        //        .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.CustomerFirstName.Contains(input.Filter) || e.CustomerLastName.Contains(input.Filter) || e.CustomerMidName.Contains(input.Filter) || e.CustomerTel1Decode.Contains(input.Filter))
        //        .Where(e => string.IsNullOrWhiteSpace(input.CustomerTel1) || e.CustomerTel1Decode.Contains(input.CustomerTel1))
        //        .Where(e => string.IsNullOrWhiteSpace(input.CustomerName) || (e.CustomerLastName + e.CustomerMidName + e.CustomerFirstName).Replace(" ", "").ToLower().Contains(input.CustomerName) || e.CompanyName.Replace(" ", "").ToLower().Contains(input.CustomerName.ToLower()))
        //        .Where(e => input.SourceId == null || e.SourceId == input.SourceId)
        //        .Where(e => input.SalesPersonId == null || e.SalesPersonId == input.SalesPersonId)
        //        .Where(e => input.HotnessId == null || e.HotnessId == input.HotnessId)
        //        .Where(e => input.ProvinceId == -1 || e.ProvinceId == input.ProvinceId)
        //        .Where(e => input.DistrictId == -1 || e.DistrictId == input.DistrictId)
        //        .Where(e => input.CreationDateFrom == null || e.CreationTime.Date >= input.CreationDateFrom.Value.Date)
        //        .Where(e => input.CreationDateTo == null || e.CreationTime.Date <= input.CreationDateTo.Value.Date)
        //        .Where(c => c.CustomerTypeId == input.CustomerTypeId && c.Status == 9 && ListUserId.Any(l => l == (long)c.CreatorUserId))
        //        .Where(e => input.UnitId == null || unitUserIds.Any(u => u == (long)e.CreatorUserId));
        //}
        #endregion

        #region Tạo mới - Cập nhật Khách hàng chuyển giao
        [AbpAuthorize(AppPermissions.Pages_SalesCustomer_Edit)]
        public async Task CreateOrEditAssignmentCustomer(CreateOrEditAssignmentCustomerDto input)
        {
            if (input.Id == null)
            {
                await CreateAssignmentCustomer(input);
            }
            else
            {
                await UpdateAssignmentCustomer(input);
            }
        }

        private async Task CreateAssignmentCustomer(CreateOrEditAssignmentCustomerDto input)
        {
            var userId = AbpSession.UserId;
            var tenantId = AbpSession.TenantId;

            //nguoi duoc chuyen giao
            var salesPerson = _salesPersonRepository.FirstOrDefault(e => e.Id == input.AssignPersonId);
            SalesCustomer salesCustomer = new()
            {
                Status = 0,
                IsWarning = false,
                SalesPersonId = _salesPersonRepository.GetAll().Where(sp => sp.UserId == userId).Select(sp => sp.Id).FirstOrDefault(),
                HotnessId = _mstSleHotnessRepository.GetAll().Where(h => h.Code == "NA").Select(h => h.Id).FirstOrDefault(),
                CustomerName = input.CustomerTypeId == 1 ? $"{input.CustomerLastName} {input.CustomerMidName} {input.CustomerFirstName}".Trim() : input.CompanyName.Trim(),
                CompanyName = input.CustomerTypeId == 2 ? input.CompanyName.Trim() : "",
                Telephone = input.PhoneNumber,
                CustomerTel1 = Regex.Replace(input.PhoneNumber, @"[^0-9]", string.Empty),
                CustomerTel1Decode = input.PhoneNumber
            };
            salesCustomer.CustomerTel1 = Encode(salesCustomer.CustomerTel1);
            salesCustomer.CompanyTel = input.CustomerTypeId == 2 ? Encode(salesCustomer.CustomerTel1) : "";

            // neu la tao khach chuyen giao GAN:
            salesCustomer.AssignDate = DateTime.Now;
            salesCustomer.SalesPersonId = salesPerson.Id; // SalesPersonId cua nguoi duoc chuyen giao
            salesCustomer.CreatorUserId = salesPerson.UserId; // CreatorUserId cua nguoi duoc chuyen giao
            salesCustomer.ImportSource = 4; // khách hàng chuyển giao tạo bằng tay

            salesCustomer.AssignPersonId = _salesPersonRepository.FirstOrDefault(e => e.UserId == userId)?.Id; // assignPersonId = salespersonId cua nguoi dang nhap
            salesCustomer.ContactResult = null;
            salesCustomer.Date = input.Date;

            int hour = DateTime.Now.Hour;
            int minute = DateTime.Now.Minute;
            salesCustomer.Time = hour * 60 + minute * 3600;

            // Kiểm tra xem có bao nhiêu KH có trùng sđt với KH này
            var customerInDB = _customerRepository.GetAll().Where(c => c.CustomerTel1 == input.PhoneNumber && c.ContactResult == 1 && c.Status != 6 && c.Status != 8 && c.Status != 9).ToList();

            if (customerInDB.Any())
            {
                foreach (var cus in customerInDB)
                {
                    if (cus.IsDuplicatePhone > 0)
                    {
                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }

                    if (cus.IsDuplicatePhone == 0 || cus.IsDuplicatePhone == null)
                    {
                        cus.IsDuplicatePhone = cus.Id;
                        _customerRepository.Update(cus);

                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }
                }
            }
            var expectedDelTiming = await _mstSleExpectedDelTimingRepository.FirstOrDefaultAsync(e => e.Id == salesCustomer.ExpectedDelTimingId);
            if (expectedDelTiming != null)
            {
                salesCustomer.HotnessId = expectedDelTiming.HotnessId;
                salesCustomer.ExpectedDelTimingId = expectedDelTiming.Id;
            }

            salesCustomer.No = CreateCustomerNo();
            long salesCsutomerId = await _customerRepository.InsertAndGetIdAsync(salesCustomer);

            var today = DateTime.Now.Date;
            var before10Days = DateTime.Now.AddDays(-10).Date;
            SalesCustomerVehicleInfo newSalesCustomerVehicleInfo = new();
            var model = await _mstSleModelsRepository.FirstOrDefaultAsync(e => e.Id == input.ModelId);
            if (model != null)
            {
                newSalesCustomerVehicleInfo.ModelId = model.Id;
            }
            if (newSalesCustomerVehicleInfo.ModelId == null)
            {
                throw new UserFriendlyException(L("CanNotFindModelId") + ": " + input.ModelId);
            }
            long? grade = _mstSleGradesRepository.GetAll().Where(e => e.ModelId == newSalesCustomerVehicleInfo.ModelId).FirstOrDefault()?.Id;
            long? gradeProduction = _mstSleGradeProductionRepository.GetAll().Where(e => e.GradeId == grade).FirstOrDefault()?.Id;
            long? color = _mstSleColorGradesProductionRepository.GetAll().Where(e => e.ProduceId == gradeProduction).FirstOrDefault()?.ColorId;
            newSalesCustomerVehicleInfo.GradeId = grade;
            newSalesCustomerVehicleInfo.ColorId = color;

            // check trùng KH của 1 nguồn
            var checkDuplicateInCustomer = _customerRepository.GetAll().Where(e => e.CustomerName == salesCustomer.CustomerName && e.CustomerTel1 == salesCustomer.CustomerTel1 && e.TenantId == salesCustomer.TenantId)
                                                                                                .Where(e => e.CreationTime.Date >= before10Days)
                                                                                                .Where(e => e.CreationTime.Date <= today)
                                                                                                .ToList();
            if (checkDuplicateInCustomer.Any())
            {
                foreach (var cus in checkDuplicateInCustomer)
                {
                    var modelDuplicate = _customerVehicleInfoRepository.FirstOrDefault(e => e.SalesCustomerId == cus.Id && e.ModelId == newSalesCustomerVehicleInfo.ModelId && e.VehicleType == 1);

                    if (modelDuplicate != null)
                    {
                        throw new UserFriendlyException(L("SalesCustomerThisCustomerIsDuplicate") + ": " + salesCustomer.CustomerName);
                    }
                }
            }

            await _customerHotnessRepository.InsertAsync(new SalesCustomerHotness
            {
                CustomerId = salesCsutomerId,
                DateChange = DateTime.Now,
                HotnessId = salesCustomer.HotnessId
            });
            newSalesCustomerVehicleInfo.SalesCustomerId = salesCsutomerId;
            newSalesCustomerVehicleInfo.IsFavourite = true;
            newSalesCustomerVehicleInfo.VehicleType = 1;
            newSalesCustomerVehicleInfo.CreatorUserId = userId;
            newSalesCustomerVehicleInfo.TenantId = tenantId ?? 0;

            await _customerVehicleInfoRepository.InsertAsync(newSalesCustomerVehicleInfo);
        }

        private async Task UpdateAssignmentCustomer(CreateOrEditAssignmentCustomerDto input)
        {
            var userId = AbpSession.UserId;
            var tenantId = AbpSession.TenantId;

            //nguoi duoc chuyen giao
            var salesPerson = _salesPersonRepository.FirstOrDefault(e => e.UserId == input.AssignPersonId);
            SalesCustomer salesCustomer = new()
            {
                IsWarning = false,
                SalesPersonId = _salesPersonRepository.GetAll().Where(sp => sp.UserId == userId).Select(sp => sp.Id).FirstOrDefault(),
                HotnessId = _mstSleHotnessRepository.GetAll().Where(h => h.Code == "NA").Select(h => h.Id).FirstOrDefault(),
                No = CreateCustomerNo(),
                CustomerName = input.CustomerLastName + ' ' + input.CustomerMidName + ' ' + input.CustomerFirstName,
                Telephone = input.PhoneNumber,

                CustomerTel1 = Regex.Replace(input.PhoneNumber, @"[^0-9]", string.Empty),
                CustomerTel1Decode = input.PhoneNumber
            };
            salesCustomer.CustomerTel1 = Encode(salesCustomer.CustomerTel1);
            salesCustomer.Telephone = Encode(salesCustomer.CustomerTel1);
            salesCustomer.CompanyTel = Encode(salesCustomer.CustomerTel1);

            // neu la tao khach chuyen giao GAN:
            salesCustomer.AssignDate = DateTime.Now;
            salesCustomer.SalesPersonId = salesPerson.Id; // SalesPersonId cua nguoi duoc chuyen giao
            salesCustomer.CreatorUserId = salesPerson.UserId; // CreatorUserId cua nguoi duoc chuyen giao
            salesCustomer.ImportSource = 4; // khách hàng chuyển giao tạo bằng tay

            salesCustomer.AssignPersonId = _salesPersonRepository.FirstOrDefault(e => e.UserId == userId)?.Id; // assignPersonId = salespersonId cua nguoi dang nhap
            salesCustomer.ContactResult = null;
            salesCustomer.Date = DateTime.Parse(input.Date.ToString());
            var splitedTime = input.Date.ToString().Split(new char[] { ':' });

            var hour = long.Parse(splitedTime[0]);
            var minute = long.Parse(splitedTime[1]);
            salesCustomer.Time = hour * 60 + minute * 3600;

            // Kiểm tra xem có bao nhiêu KH có trùng sđt với KH này
            var customerInDB = _customerRepository.GetAll().Where(c => c.CustomerTel1 == input.PhoneNumber && c.ContactResult == 1 && c.Status != 6 && c.Status != 8 && c.Status != 9).ToList();

            if (customerInDB.Any())
            {
                foreach (var cus in customerInDB)
                {
                    if (cus.IsDuplicatePhone > 0)
                    {
                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }

                    if (cus.IsDuplicatePhone == 0 || cus.IsDuplicatePhone == null)
                    {
                        cus.IsDuplicatePhone = cus.Id;
                        _customerRepository.Update(cus);

                        salesCustomer.IsDuplicatePhone = cus.IsDuplicatePhone;
                    }
                }
            }
            var expectedDelTiming = await _mstSleExpectedDelTimingRepository.FirstOrDefaultAsync(e => e.Id == salesCustomer.ExpectedDelTimingId);
            if (expectedDelTiming != null)
            {
                salesCustomer.HotnessId = expectedDelTiming.HotnessId;
                salesCustomer.ExpectedDelTimingId = expectedDelTiming.Id;
            }

            long salesCsutomerId = await _customerRepository.InsertAndGetIdAsync(salesCustomer);

            var today = DateTime.Now.Date;
            var before10Days = DateTime.Now.AddDays(-10).Date;
            SalesCustomerVehicleInfo newSalesCustomerVehicleInfo = new();
            var model = await _mstSleModelsRepository.FirstOrDefaultAsync(e => e.Id == input.ModelId);
            if (model != null)
            {
                newSalesCustomerVehicleInfo.ModelId = model.Id;
            }
            if (newSalesCustomerVehicleInfo.ModelId == null)
            {
                throw new UserFriendlyException(L("CanNotFindModelId") + ": " + input.ModelId);
            }
            long? grade = _mstSleGradesRepository.GetAll().Where(e => e.ModelId == newSalesCustomerVehicleInfo.ModelId).FirstOrDefault()?.Id;
            long? gradeProduction = _mstSleGradeProductionRepository.GetAll().Where(e => e.GradeId == grade).FirstOrDefault()?.Id;
            long? color = _mstSleColorGradesProductionRepository.GetAll().Where(e => e.ProduceId == gradeProduction).FirstOrDefault()?.ColorId;
            newSalesCustomerVehicleInfo.GradeId = grade;
            newSalesCustomerVehicleInfo.ColorId = color;

            // check trùng KH của 1 nguồn
            var checkDuplicateInCustomer = _customerRepository.GetAll().Where(e => e.CustomerName == salesCustomer.CustomerName && e.CustomerTel1 == salesCustomer.CustomerTel1 && e.TenantId == salesCustomer.TenantId)
                                                                                                .Where(e => e.CreationTime.Date >= before10Days)
                                                                                                .Where(e => e.CreationTime.Date <= today)
                                                                                                .ToList();
            if (checkDuplicateInCustomer.Any())
            {
                foreach (var cus in checkDuplicateInCustomer)
                {
                    var modelDuplicate = _customerVehicleInfoRepository.FirstOrDefault(e => e.SalesCustomerId == cus.Id && e.ModelId == newSalesCustomerVehicleInfo.ModelId && e.VehicleType == 1);

                    if (modelDuplicate != null)
                    {
                        throw new UserFriendlyException(L("SalesCustomerThisCustomerIsDuplicate") + ": " + salesCustomer.CustomerName);
                    }
                }
            }

            await _customerHotnessRepository.InsertAsync(new SalesCustomerHotness
            {
                CustomerId = salesCsutomerId,
                DateChange = DateTime.Now,
                HotnessId = salesCustomer.HotnessId
            });
            newSalesCustomerVehicleInfo.SalesCustomerId = salesCsutomerId;
            newSalesCustomerVehicleInfo.IsFavourite = true;
            newSalesCustomerVehicleInfo.VehicleType = 1;
            newSalesCustomerVehicleInfo.CreatorUserId = userId;
            newSalesCustomerVehicleInfo.TenantId = tenantId ?? 0;

            await _customerVehicleInfoRepository.InsertAsync(newSalesCustomerVehicleInfo);
        }
        #endregion

        #region Tình trạng liên hệ Khách hàng chuyển giao
        public async Task AssignmentCustomerContactResult(AssignmentCustomerContactResultDto input)
        {
            var salesCustomer = await _customerRepository.FirstOrDefaultAsync(e => e.Id == input.SalesCustomerId);

            if (salesCustomer.CreatorUserId != AbpSession.UserId)
            {
                throw new UserFriendlyException("Anh/Chị không thể thao tác với KH của TVBH khác!");
            }

            var salesStageId = 0;
            salesCustomer.ContactResult = input.ContactResult;
            if (input.ContactResult == 0)
            {
                if (input.IsMoveFreeze)
                {
                    salesCustomer.Status = 9;
                    salesStageId = 9;

                    await _customerRepository.UpdateAsync(salesCustomer);
                }
                var transactionInfos = await _customerTransactionInfoRepository.GetAll().Where(t => t.SalesCustomerId == input.SalesCustomerId).ToListAsync();

                foreach (var transaction in transactionInfos)
                {
                    await _customerTransactionInfoRepository.DeleteAsync(transaction.Id);
                }

                SalesCustomerTransactionInfo trans = new()
                {
                    SalesCustomerId = input.SalesCustomerId,
                    SalesStageId = salesStageId,
                    ReasonOfNCId = input.ReasonOfNCId,
                    ReasonOfNCOther = input.ReasonOfNCOther,
                    ReasonLost = input.ReasonLost,
                    ReasonOfLostId = input.ReasonOfLostId,
                    IsShowRoomVisit = false,
                    IsTestDrive = false,
                    ActivityDate = DateTime.Today.Date,
                    StatusApprove = 1,
                    ReasonOfFreezeId = input.ReasonOfFreezeId,
                    OnPassiveFreezing = false
                };

                await _customerTransactionInfoRepository.InsertAsync(trans);
            }

        }
        #endregion

        #region Gửi yêu cầu của quản lý tới NVBH
        public async Task SendCommentToSalesPerson(long Id, string ManagerComment, long SalesPersonId)
        {
            var SendUserId = (long)AbpSession.UserId;

            var SendUserName = _userRepository.FirstOrDefault(e => e.Id == (long)AbpSession.UserId)?.FullName;

            var SalesCustomer = await _customerRepository.FirstOrDefaultAsync(e => e.Id == Id);
            SalesCustomer.ManagerComment = ManagerComment;

            await _customerRepository.UpdateAsync(SalesCustomer);

            UserIdentifier user = new(AbpSession.TenantId, (long)SalesCustomer.CreatorUserId);

            await _appNotifier.SendMessageAsync(
                    user,
                    $"Bạn nhận được lưu ý từ {SendUserName} về KH {SalesCustomer.CustomerName}: {ManagerComment}",
                    Abp.Notifications.NotificationSeverity.Warn);
        }
        #endregion
    }
}
