﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using Abp.Organizations;
using Microsoft.EntityFrameworkCore;
using NUglify.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization.Users;
using tmss.Dto;
using tmss.Master;
using tmss.Organizations.Dto;
using tmss.Sales.SalesPerson;
using tmss.Sales.SalesPerson.Dtos;

namespace tmss.Sales.Vehicle
{
    public class SalePersonAppService : tmssAppServiceBase, ISalesPersonAppService
    {
        private readonly IRepository<MstSleSalesPerson, long> _salesPersonRepository;
        private readonly IRepository<OrganizationUnit, long> _organizationUnitRepository;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<MstGenDealer, long> _mstGenDealerRepository;
        private readonly IRepository<User, long> _userRepository;

        public SalePersonAppService(
            IRepository<MstSleSalesPerson, long> salesPersonRepository,
            IRepository<OrganizationUnit, long> organizationUnitRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<MstGenDealer, long> mstGenDealerRepository,
            IRepository<User, long> userRepository
            )
        {
            _salesPersonRepository = salesPersonRepository;
            _organizationUnitRepository = organizationUnitRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _mstGenDealerRepository = mstGenDealerRepository;
            _userRepository = userRepository;
        }

        public PagedResultDto<GetSalesPersonForViewDto> GetAll(GetAllSalesPersonInput input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                IQueryable<GetSalesPersonForViewDto> query;
                List<IQueryable<GetSalesPersonForViewDto>> listSPQuery = new();
                List<GetSalesPersonForViewDto> listSPForCheck = new();
                List<GetSalesPersonForViewDto> listSPForPagedAndFilter = new();

                if (input.OrganizationUnitId == null)
                {
                    query = from person in _salesPersonRepository.GetAll()
                                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.FullName.Contains(input.Filter)
                                    || e.Position.Contains(input.Filter) || e.Abbreviation.Contains(input.Filter) || e.Email.Contains(input.Filter)
                                    || e.Address.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Phone.Contains(input.Filter))
                                .Where(e => string.IsNullOrWhiteSpace(input.FullNameFilter) || e.FullName.Contains(input.FullNameFilter))
                                .Where(e => input.OrderingFilter == null || e.Ordering == input.OrderingFilter)
                                .Where(e => input.DealerId == null || e.TenantId == input.DealerId)
                                .Where(e => string.IsNullOrWhiteSpace(input.PositionFilter) || e.Position.Contains(input.PositionFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.AbbreviationFilter) || e.Abbreviation.Contains(input.AbbreviationFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.StatusFilter) || e.Status == input.StatusFilter)
                                .Where(e => string.IsNullOrWhiteSpace(input.GenderFilter) || e.Gender == input.GenderFilter)
                                .Where(e => string.IsNullOrWhiteSpace(input.EmailFilter) || e.Email.Contains(input.EmailFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.AddressFilter) || e.Address.Contains(input.AddressFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.DescriptionFilter) || e.Description.Contains(input.DescriptionFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.PhoneFilter) || e.Phone.Contains(input.PhoneFilter))
                                .OrderByDescending(e => e.Ordering)

                            select new GetSalesPersonForViewDto()
                            {
                                FullName = person.FullName,
                                Ordering = person.Ordering,
                                Birthday = person.Birthday,
                                Position = person.Position,
                                Abbreviation = person.Abbreviation,
                                Status = person.Status,
                                Gender = person.Gender,
                                Phone = person.Phone,
                                Email = person.Email,
                                Address = person.Address,
                                Description = person.Description,
                                Id = person.Id,
                                UserId = person.UserId,
                                IsContractApprove = person.IsContractApprove,
                                IsProposalApprove = person.IsProposalApprove,
                                IsViewCustomer = person.IsViewCustomer,
                                IsApproveTestDrive = person.IsApproveTestDrive,
                                IsApproveLostFreeze = person.IsApproveLostFreeze,
                                IsReceiveAssignmentList = person.IsReceiveAssignmentList,
                                IsViewDardboard = person.IsViewDardboard,
                                IsReceiveTestDriveList = person.IsReceiveTestDriveList,
                                //IsRepresent = person.IsRepresent,
                                IsViewDuplicateCustomer = person.IsViewDuplicateCustomer,
                                IsCreateTransferCustomer = person.IsCreateTransferCustomer,
                                IsApproveFleet = person.IsApproveFleet,
                                IsReviewFleet = person.IsReviewFleet
                            };
                    listSPQuery.Add(query);
                    listSPQuery.ForEach(e => { e.ForEach(p => listSPForCheck.Add(p)); });
                }

                if (input.OrganizationUnitId != null)
                {
                    foreach (var item in input.OrganizationUnitId)
                    {
                        var userOrganizationUnit = _userOrganizationUnitRepository.GetAll().Where(e => e.OrganizationUnitId == item);

                        query = from person in _salesPersonRepository.GetAll()
                                .Where(e => string.IsNullOrWhiteSpace(input.Filter) || e.FullName.Contains(input.Filter)
                                    || e.Position.Contains(input.Filter) || e.Abbreviation.Contains(input.Filter) || e.Email.Contains(input.Filter)
                                    || e.Address.Contains(input.Filter) || e.Description.Contains(input.Filter) || e.Phone.Contains(input.Filter))
                                .Where(e => string.IsNullOrWhiteSpace(input.FullNameFilter) || e.FullName.Contains(input.FullNameFilter))
                                .Where(e => input.OrderingFilter == null || e.Ordering == input.OrderingFilter)
                                .Where(e => input.DealerId == null || e.TenantId == input.DealerId)
                                .Where(e => string.IsNullOrWhiteSpace(input.PositionFilter) || e.Position.Contains(input.PositionFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.AbbreviationFilter) || e.Abbreviation.Contains(input.AbbreviationFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.StatusFilter) || e.Status == input.StatusFilter)
                                .Where(e => string.IsNullOrWhiteSpace(input.GenderFilter) || e.Gender == input.GenderFilter)
                                .Where(e => string.IsNullOrWhiteSpace(input.EmailFilter) || e.Email.Contains(input.EmailFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.AddressFilter) || e.Address.Contains(input.AddressFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.DescriptionFilter) || e.Description.Contains(input.DescriptionFilter))
                                .Where(e => string.IsNullOrWhiteSpace(input.PhoneFilter) || e.Phone.Contains(input.PhoneFilter))
                                .OrderByDescending(e => e.Ordering)

                                join personUserUnit in userOrganizationUnit on person.UserId equals personUserUnit.UserId

                                select new GetSalesPersonForViewDto
                                {
                                    FullName = person.FullName,
                                    Ordering = person.Ordering,
                                    Birthday = person.Birthday,
                                    Position = person.Position,
                                    Abbreviation = person.Abbreviation,
                                    Status = person.Status,
                                    Gender = person.Gender,
                                    Phone = person.Phone,
                                    Email = person.Email,
                                    Address = person.Address,
                                    Description = person.Description,
                                    Id = person.Id,
                                    UserId = person.UserId,
                                    IsContractApprove = person.IsContractApprove,
                                    IsProposalApprove = person.IsProposalApprove,
                                    IsViewCustomer = person.IsViewCustomer,
                                    IsApproveTestDrive = person.IsApproveTestDrive,
                                    IsApproveLostFreeze = person.IsApproveLostFreeze,
                                    IsReceiveAssignmentList = person.IsReceiveAssignmentList,
                                    IsViewDardboard = person.IsViewDardboard,
                                    IsReceiveTestDriveList = person.IsReceiveTestDriveList,
                                    //IsRepresent = person.IsRepresent,
                                    IsViewDuplicateCustomer = person.IsViewDuplicateCustomer,
                                    IsCreateTransferCustomer = person.IsCreateTransferCustomer,
                                    IsApproveFleet = person.IsApproveFleet,
                                    IsReviewFleet = person.IsReviewFleet
                                };

                        listSPQuery.Add(query);
                    }

                    listSPQuery.ForEach(e => { e.ForEach(p => listSPForCheck.Add(p)); });
                }

                var pagedAndFilteredMstSalesPerson = listSPForCheck.DistinctBy(e => e.UserId).AsQueryable().PageBy(input).ToList();
                var totalCount = listSPForCheck.DistinctBy(e => e.UserId).ToList().Count;
                return new PagedResultDto<GetSalesPersonForViewDto>(
                    totalCount,
                    pagedAndFilteredMstSalesPerson
                    );
            }
        }

        public async Task CreateOrEdit(CreateOrEditSalesPersonDto input)
        {
            if (input.Id == null)
            {
                await Create(input);
            }
            else
            {
                await Update(input);
            }
        }

        protected virtual async Task Update(CreateOrEditSalesPersonDto input)
        {
            var currentTenant = AbpSession.TenantId;

            if (input.IsReceiveAssignmentList == true)
            {
                var salesPersonIsReceiveAssignment = _salesPersonRepository.GetAll().Where(s => s.IsReceiveAssignmentList == true && s.Id != (long)input.Id).ToList();

                if (salesPersonIsReceiveAssignment.Any())
                {
                    foreach (var item in salesPersonIsReceiveAssignment)
                    {
                        item.IsReceiveAssignmentList = false;

                        await _salesPersonRepository.UpdateAsync(item);
                    }
                }
            }

            if (input.IsReceiveTestDriveList == true)
            {
                var salesPersonIsReceiveAssignment = _salesPersonRepository.GetAll().Where(s => s.IsReceiveTestDriveList == true && s.Id != (long)input.Id).ToList();

                if (salesPersonIsReceiveAssignment.Any())
                {
                    foreach (var item in salesPersonIsReceiveAssignment)
                    {
                        item.IsReceiveTestDriveList = false;

                        await _salesPersonRepository.UpdateAsync(item);
                    }
                }
            }

            //if (input.IsRepresent == true)
            //{
            //    var salesPersonIsReceiveAssignment = _salesPersonRepository.GetAll().Where(s => s.IsRepresent == true && s.Id != (long)input.Id).ToList();

            //    if (salesPersonIsReceiveAssignment.Any())
            //    {
            //        foreach (var item in salesPersonIsReceiveAssignment)
            //        {
            //            item.IsRepresent = false;

            //            await _salesPersonRepository.UpdateAsync(item);
            //        }
            //    }
            //}

            var listUserOrg = _userOrganizationUnitRepository.GetAll().Where(e => e.TenantId == input.TenantId && e.UserId == input.UserId);
            listUserOrg.ForEach(e =>
            {
                _userOrganizationUnitRepository.Delete(e.Id);
            });

            var salesPerson = await _salesPersonRepository.FirstOrDefaultAsync((long)input.Id);

            input.TenantId = currentTenant;

            ObjectMapper.Map(input, salesPerson);

            var orgs = await _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == input.UserId).ToListAsync();

            if (orgs.Any())
            {
                foreach (var org in orgs)
                {
                    await _userOrganizationUnitRepository.HardDeleteAsync(org);
                }
                foreach (int item in input.OrganizationUnits)
                {
                    var userOrganizationUnitSelected = new UserOrganizationUnit { UserId = input.UserId, OrganizationUnitId = item, TenantId = currentTenant };
                    await _userOrganizationUnitRepository.InsertAsync(userOrganizationUnitSelected);
                }
            }

            if (orgs.Count == 0)
            {
                foreach (int item in input.OrganizationUnits)
                {
                    var userOrganizationUnitSelected = new UserOrganizationUnit
                    {
                        UserId = input.UserId,
                        OrganizationUnitId = item,
                        TenantId = currentTenant
                    };
                    await _userOrganizationUnitRepository.InsertAsync(userOrganizationUnitSelected);
                }
            }
        }

        public async Task UpdateUserId(UpdateUserIdDto input)
        {
            if (input.Id == null)
            {
                throw new Exception();
            }
            else
            {
                var salesPerson = await _salesPersonRepository.FirstOrDefaultAsync((long)input.Id);

                ObjectMapper.Map(input, salesPerson);
            }
        }

        protected virtual async Task Create(CreateOrEditSalesPersonDto input)
        {
            var currentTenant = AbpSession.TenantId;
            var currentUser = AbpSession.UserId;
            if (input.IsReceiveAssignmentList == true)
            {
                var salesPersonIsReceiveAssignment = _salesPersonRepository.GetAll().Where(s => s.IsReceiveAssignmentList == true && s.Id != (long)input.Id).ToList();

                if (salesPersonIsReceiveAssignment.Any())
                {
                    foreach (var item in salesPersonIsReceiveAssignment)
                    {
                        item.IsReceiveAssignmentList = false;

                        await _salesPersonRepository.UpdateAsync(item);
                    }
                }
            }

            if (input.IsReceiveTestDriveList == true)
            {
                var salesPersonIsReceiveAssignment = _salesPersonRepository.GetAll().Where(s => s.IsReceiveTestDriveList == true && s.Id != (long)input.Id).ToList();

                if (salesPersonIsReceiveAssignment.Any())
                {
                    foreach (var item in salesPersonIsReceiveAssignment)
                    {
                        item.IsReceiveTestDriveList = false;

                        await _salesPersonRepository.UpdateAsync(item);
                    }
                }
            }

            //if (input.IsRepresent == true)
            //{
            //    var salesPersonIsReceiveAssignment = _salesPersonRepository.GetAll().Where(s => s.IsRepresent == true && s.Id != (long)input.Id).ToList();

            //    if (salesPersonIsReceiveAssignment.Any())
            //    {
            //        foreach (var item in salesPersonIsReceiveAssignment)
            //        {
            //            item.IsRepresent = false;

            //            await _salesPersonRepository.UpdateAsync(item);
            //        }
            //    }
            //}

            var salesPerson = ObjectMapper.Map<MstSleSalesPerson>(input);
            salesPerson.TenantId = currentTenant;
            salesPerson.CreatorUserId = currentUser;

            await _salesPersonRepository.InsertAsync(salesPerson);

            var orgs = await _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == input.UserId).ToListAsync();

            if (orgs.Any())
            {
                foreach (var org in orgs)
                {
                    await _userOrganizationUnitRepository.HardDeleteAsync(org);
                }
                foreach (int item in input.OrganizationUnits)
                {
                    var userOrganizationUnitSelected = new UserOrganizationUnit { UserId = input.UserId, OrganizationUnitId = item, TenantId = currentTenant };
                    await _userOrganizationUnitRepository.InsertAsync(userOrganizationUnitSelected);
                }

            }

            if (orgs.Count == 0)
            {
                foreach (int item in input.OrganizationUnits)
                {
                    var userOrganizationUnitSelected = new UserOrganizationUnit
                    {
                        UserId = input.UserId,
                        OrganizationUnitId = item,
                        TenantId = currentTenant
                    };
                    await _userOrganizationUnitRepository.InsertAsync(userOrganizationUnitSelected);
                }
            }
        }

        public async Task Delete(EntityDto<long> input)
        {
            await _salesPersonRepository.DeleteAsync(input.Id);
        }

        public async Task<GetSalesPersonForEditOutput> GetSalesPersonForEdit(EntityDto<long?> input)
        {
            var query = await (from person in _salesPersonRepository.GetAll().Where(e => e.Id == input.Id)
                               select new GetSalesPersonForEditOutput()
                               {
                                   FullName = person.FullName,
                                   Ordering = person.Ordering,
                                   Birthday = person.Birthday,
                                   Position = person.Position,
                                   Abbreviation = person.Abbreviation,
                                   Status = person.Status,
                                   Gender = person.Gender,
                                   Phone = person.Phone,
                                   Email = person.Email,
                                   Address = person.Address,
                                   Description = person.Description,
                                   Id = person.Id,
                                   UserId = person.UserId,
                                   IsContractApprove = person.IsContractApprove,
                                   IsProposalApprove = person.IsProposalApprove,
                                   IsViewCustomer = person.IsViewCustomer,
                                   IsApproveTestDrive = person.IsApproveTestDrive,
                                   IsApproveLostFreeze = person.IsApproveLostFreeze,
                                   IsViewStock = person.IsViewStock,
                                   IsSeekforSupervisor = person.IsSeekforSupervisor,
                                   IsReceiveAssignmentList = person.IsReceiveAssignmentList,
                                   IsViewDardboard = person.IsViewDardboard,
                                   IsReceiveTestDriveList = person.IsReceiveTestDriveList,
                                   //IsRepresent = person.IsRepresent,
                                   IsViewDuplicateCustomer = person.IsViewDuplicateCustomer,
                                   IsCreateTransferCustomer = person.IsCreateTransferCustomer,
                                   TenantId = person.TenantId,
                                   IsApproveFleet = person.IsApproveFleet,
                                   IsReviewFleet = person.IsReviewFleet
                               }).FirstOrDefaultAsync();
            return query;
        }

        public async Task<List<UserListInfoDto>> GetUserAll(bool created)
        {
            if (!created)
            {
                var query = from userInfor in UserManager.Users.Where(e => !_salesPersonRepository.GetAll().Select(e => e.UserId).Contains(e.Id))
                            select new UserListInfoDto
                            {
                                Id = userInfor.Id,
                                FullName = userInfor.FullName,
                                Email = userInfor.EmailAddress,
                                Phone = userInfor.PhoneNumber,

                            };
                return await query.ToListAsync();
            }

            if (created)
            {
                var query = from userInfor in UserManager.Users.Where(e => _salesPersonRepository.GetAll().Select(e => e.UserId).Contains(e.Id))
                            select new UserListInfoDto
                            {
                                Id = userInfor.Id,
                                FullName = userInfor.FullName,
                                Email = userInfor.EmailAddress,
                                Phone = userInfor.PhoneNumber

                            };
                return await query.ToListAsync();
            }

            return null;
        }

        public async Task<List<MstSleSalesPerson>> GetListSalePerson()
        {
            return await _salesPersonRepository.GetAll().ToListAsync();
        }

        public async Task<ListGroupByUserDto> GetGroupSalesPerson(long SelectedUserId, bool IsCreated, long? DealerId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var allOrganizationUnits = await _organizationUnitRepository.GetAll().Where(e => e.TenantId == DealerId).ToListAsync();

                var output = new ListGroupByUserDto
                {
                    AllOrganizationUnits = ObjectMapper.Map<List<OrganizationUnitDto>>(allOrganizationUnits),
                    MemberedOrganizationUnits = new List<string>()
                };

                if (IsCreated == false)
                {
                    var userDefine = await UserManager.GetUserByIdAsync(SelectedUserId);
                    var organizationUnits = await UserManager.GetOrganizationUnitsAsync(userDefine);
                    output.MemberedOrganizationUnits = organizationUnits.Select(ou => ou.Code).ToList();
                }

                if (IsCreated == true)
                {
                    var result = from unitUser in _userOrganizationUnitRepository.GetAll().Where(e => e.UserId == SelectedUserId && e.TenantId == DealerId)
                                 join organization in _organizationUnitRepository.GetAll()
                                 on unitUser.OrganizationUnitId equals organization.Id

                                 select new { organization.Code };

                    foreach (var item in result)
                    {
                        output.MemberedOrganizationUnits.Add(item.Code);
                    }
                }

                return output;
            }
        }

        public async Task<SalesPersonInfo> GetSalesPersonInfo(long userId)
        {
            var genDealer = _mstGenDealerRepository.GetAll().FirstOrDefault(g => g.Id == AbpSession.TenantId);
            var SalesPerson = await (from u in UserManager.Users.Where(u => u.Id == userId)
                                     join uo in _userOrganizationUnitRepository.GetAll() on u.Id equals uo.UserId
                                     join o in _organizationUnitRepository.GetAll() on uo.OrganizationUnitId equals o.Id
                                     select new SalesPersonInfo
                                     {
                                         FullName = u.FullName,
                                         OrganOrganizationName = o.DisplayName,
                                     }).FirstOrDefaultAsync();
            SalesPerson.DealerName = GetTenantsCurrentLogin().Name;
            SalesPerson.Abbreviation = genDealer.Abbreviation;
            SalesPerson.Address = genDealer.Address;
            SalesPerson.Fax = genDealer.Fax;
            SalesPerson.Phone = genDealer.Phone;
            SalesPerson.TaxCode = genDealer.TaxCode;

            return SalesPerson;
        }

        public async Task<long> GetUserId(long SalesPersonId)
        {
            var a = await _salesPersonRepository.GetAll().ToListAsync();

            var user = (await _salesPersonRepository.GetAll().FirstOrDefaultAsync(sp => sp.Id == SalesPersonId));
            var userId = user == null ? 0 : user.UserId;
            return (long)userId;
        }

        public bool CheckPerminssionTwoUser(long userId1, long userId2)
        {
            var listOrganization = new List<long>();
            var listUserID = new List<long>();
            var userOrganization = _userOrganizationUnitRepository.GetAll().Where(r => r.UserId == userId1);
            var organizationUnit = _organizationUnitRepository.GetAll().ToList();
            var organization = from Org in organizationUnit
                               join UserOrg in userOrganization.Where(r => r.UserId == userId1) on Org.Id equals UserOrg.OrganizationUnitId
                               select Org;
            // lấy ra danh sách Org
            foreach (var parent in organization)
            {
                var ListParent = _organizationUnitRepository.GetAll().Where(r => r.Id == parent.Id).ToList();
                while (ListParent.Any())
                {
                    foreach (var a in ListParent.ToList())
                    {
                        var check = _organizationUnitRepository.GetAll().Where(r => r.Id == a.ParentId).ToList();
                        if (check != null)
                        {
                            foreach (var c in check)
                            {
                                ListParent.Add(c);
                            }
                        }
                        listOrganization.AddIfNotContains(a.Id);
                        ListParent.Remove(a);
                    }
                }
            }
            if (listOrganization != null)
            {
                var a = (from user in _userOrganizationUnitRepository.GetAll()
                         where listOrganization.Any(a => user.OrganizationUnitId.Equals(a))
                         select user.UserId).ToList();
                listUserID = a;
            }
            // lấy danh sách saleperson có IsContractApprove = true
            if (listUserID.Contains(userId2))
            {
                return false;
            }
            return true;
        }

        public async Task<List<UserListInfoDto>> GetRefreshUser(long? UserId)
        {
            var users = from user in _userRepository.GetAll().Where(e => e.Id == UserId)
                        select new UserListInfoDto()
                        {
                            Id = user.Id,
                            FullName = user.FullName,
                            Phone = user.PhoneNumber,
                            Email = user.EmailAddress
                        };
            return await users.ToListAsync();
        }
    }
}
