﻿using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.LocalStorage;
using tmss.LocalStorage.Dto;
using tmss.Master;
using tmss.MstSle.Dto.LookUp;
using tmss.MstSle.IMstSleDistrict.Dto;

namespace tmss.Sales.LocalStorage
{
    public class LocalStorageAppService : tmssAppServiceBase, ILocalStorageAppService
    {
        private readonly IRepository<MstGenProvince, long> _mstGenProvinceRepository;
        private readonly IRepository<MstSleDistrict, long> _mstSleDistrictRepository;
        private readonly IRepository<MstSleLookUp, int> _mstSleLookupRepository;

        public LocalStorageAppService(
            IRepository<MstSleLookUp, int> mstSleLookupRepository,
            IRepository<MstGenProvince, long> mstGenProvinceRepository,
            IRepository<MstSleDistrict, long> mstSleDistrictRepository
        )
        {
            _mstSleLookupRepository = mstSleLookupRepository;
            _mstGenProvinceRepository = mstGenProvinceRepository;
            _mstSleDistrictRepository = mstSleDistrictRepository;
        }

        public async Task<PickList> GetLocalStorage()
        {
            PickList pickList = new();
            var districts = await (from district in _mstSleDistrictRepository.GetAll().OrderByDescending(e => e.Ordering)
                                   select new GetCodeDistrictDto()
                                   {
                                       Id = district.Id,
                                       Name = district.Name,
                                       Code = district.Code
                                   }).ToListAsync();

            var provinces = await (from province in _mstGenProvinceRepository.GetAll().OrderByDescending(e => e.Ordering)
                                   select new GetDistrictByProviceIdDto
                                   {
                                       Name = province.Name,
                                       Id = province.Id,
                                       ListDistrict = (from district in _mstSleDistrictRepository.GetAll().Where(r => r.ProvinceId == province.Id).OrderByDescending(r => r.Ordering)
                                                       select new MstSleDistrictsDto
                                                       {
                                                           Name = district.Name,
                                                           Id = district.Id,
                                                           ProvinceId = district.ProvinceId
                                                       }
                                       ).ToList()
                                   }).ToListAsync();

            var lookUps = await (from look in _mstSleLookupRepository.GetAll().OrderBy(e => e.Ordering)
                                 select new MstSleLookupDto
                                 {
                                     Value = look.Value,
                                     Code = look.Code,
                                     Name = look.Name,
                                     Id = look.Id,
                                     Description = look.Description
                                 }).ToListAsync();

            pickList.District = districts;
            pickList.Province = provinces;
            pickList.LookUp = lookUps;

            return pickList;
        }
    }
}
