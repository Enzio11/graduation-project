﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Localization;
using Abp.MultiTenancy;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using tmss.Authorization;
using tmss.Localization.Dto;
using tmss.Master;

namespace tmss.Localization
{
    public class LanguageAppService : tmssAppServiceBase, ILanguageAppService
    {
        private readonly IApplicationLanguageManager _applicationLanguageManager;
        private readonly IApplicationLanguageTextManager _applicationLanguageTextManager;
        private readonly IRepository<ApplicationLanguage> _languageRepository;
        private readonly IApplicationCulturesProvider _applicationCulturesProvider;
        private readonly IRepository<MstSleHotness, long> _MstSleHotnessrepo;
        private readonly IRepository<MstSleSource, long> _MstSleSourceRepo;
        private readonly IRepository<MstSleExpectedDelTiming, long> _MstSleExpectedDelTiming;
        private readonly IRepository<MstSleOccupation, long> _MstSleOccupationRepo;
        private readonly IRepository<MstSleHobby, long> _MstSleHobbyRepo;
        private readonly IRepository<MstSleSourceOfInfo, long> _MstSleSourceOfInfo;
        private readonly IRepository<MstSleSocialChannel, long> _MstSleSocialChannelRepo;
        private readonly IRepository<MstSleBusinessType, long> _MstSleBusinessTypeRepo;
        private readonly IRepository<MstSlePurpose, long> _MstSlePurposeRepo;
        private readonly IRepository<MstSleReasonOfChange, long> _MstSleReasonOfChangeRepo;
        private readonly IRepository<MstSleSalesStage, long> _MstSleSalesStageRepo;
        private readonly IRepository<MstSleReasonOfLost, long> _MstSleReasonOfLostRepo;
        private readonly IRepository<MstSleReasonOfLostType, long> _MstSleReasonOfLostTypeRepo;
        private readonly IRepository<MstSleContactBy, long> _MstSleContactByRepo;
        private readonly IRepository<MstSleReasonOfNA, long> _MstSleReasonOfNARepo;
        private readonly IRepository<MstSleCommissionSource, long> _MstSleCommissionSourceRepo;
        private readonly IRepository<MstSleActivity, long> _MstSleActivityRepo;
        private readonly IRepository<MstSleAgeRange, long> _MstSleAgeRangeRepo;
        private readonly IRepository<MstSleCompany, long> _MstSleCompanyRepo;
        private readonly IRepository<MstSleFAR, long> _MstSleFARRepo;
        private readonly IRepository<MstSleRelationship, long> _MstSleRelationshipRepo;
        private readonly IRepository<MstSleColors, long> _MstSleColorsRepo;
        private readonly IRepository<MstSleReasonOfNC, long> _MstSleReasonOfNCRepo;

        public LanguageAppService(IApplicationLanguageManager applicationLanguageManager,
                                  IApplicationLanguageTextManager applicationLanguageTextManager,
                                  IRepository<ApplicationLanguage> languageRepository,
                                  IApplicationCulturesProvider applicationCulturesProvider,
                                  IRepository<MstSleHotness, long> mstSleHotnessrepo,
                                  IRepository<MstSleSource, long> mstSleSourceRepo,
                                  IRepository<MstSleExpectedDelTiming, long> mstSleExpectedDelTiming,
                                  IRepository<MstSleOccupation, long> mstSleOccupationRepo,
                                  IRepository<MstSleHobby, long> mstSleHobbyRepo,
                                  IRepository<MstSleSourceOfInfo, long> mstSleSourceOfInfo,
                                  IRepository<MstSleSocialChannel, long> mstSleSocialChannelRepo,
                                  IRepository<MstSleBusinessType, long> mstSleBusinessTypeRepo,
                                  IRepository<MstSlePurpose, long> mstSlePurposeRepo,
                                  IRepository<MstSleReasonOfChange, long> mstSleReasonOfChangeRepo,
                                  IRepository<MstSleSalesStage, long> mstSleSalesStageRepo,
                                  IRepository<MstSleReasonOfLost, long> mstSleReasonOfLostRepo,
                                  IRepository<MstSleReasonOfLostType, long> mstSleReasonOfLostTypeRepo,
                                  IRepository<MstSleContactBy, long> mstSleContactByRepo,
                                  IRepository<MstSleReasonOfNA, long> mstSleReasonOfNARepo,
                                  IRepository<MstSleCommissionSource, long> mstSleCommissionSourceRepo,
                                  IRepository<MstSleActivity, long> mstSleActivityRepo,
                                  IRepository<MstSleAgeRange, long> mstSleAgeRangeRepo,
                                  IRepository<MstSleCompany, long> mstSleCompanyRepo,
                                  IRepository<MstSleFAR, long> mstSleFARRepo,
                                  IRepository<MstSleRelationship, long> mstSleRelationshipRepo,
                                  IRepository<MstSleReasonOfNC, long> MstSleReasonOfNCRepo,
                                  IRepository<MstSleColors, long> MstSleColorsRepo)
        {
            _applicationLanguageManager = applicationLanguageManager;
            _applicationLanguageTextManager = applicationLanguageTextManager;
            _languageRepository = languageRepository;
            _applicationCulturesProvider = applicationCulturesProvider;
            _MstSleHotnessrepo = mstSleHotnessrepo;
            _MstSleSourceRepo = mstSleSourceRepo;
            _MstSleExpectedDelTiming = mstSleExpectedDelTiming;
            _MstSleOccupationRepo = mstSleOccupationRepo;
            _MstSleHobbyRepo = mstSleHobbyRepo;
            _MstSleSourceOfInfo = mstSleSourceOfInfo;
            _MstSleSocialChannelRepo = mstSleSocialChannelRepo;
            _MstSleBusinessTypeRepo = mstSleBusinessTypeRepo;
            _MstSlePurposeRepo = mstSlePurposeRepo;
            _MstSleReasonOfChangeRepo = mstSleReasonOfChangeRepo;
            _MstSleSalesStageRepo = mstSleSalesStageRepo;
            _MstSleReasonOfLostRepo = mstSleReasonOfLostRepo;
            _MstSleReasonOfLostTypeRepo = mstSleReasonOfLostTypeRepo;
            _MstSleContactByRepo = mstSleContactByRepo;
            _MstSleReasonOfNARepo = mstSleReasonOfNARepo;
            _MstSleCommissionSourceRepo = mstSleCommissionSourceRepo;
            _MstSleActivityRepo = mstSleActivityRepo;
            _MstSleAgeRangeRepo = mstSleAgeRangeRepo;
            _MstSleCompanyRepo = mstSleCompanyRepo;
            _MstSleFARRepo = mstSleFARRepo;
            _MstSleRelationshipRepo = mstSleRelationshipRepo;
            _MstSleColorsRepo = MstSleColorsRepo;
            _MstSleReasonOfNCRepo = MstSleReasonOfNCRepo;
        }


        public async Task<GetLanguagesOutput> GetLanguages()
        {
            var languages = (await _applicationLanguageManager.GetLanguagesAsync(AbpSession.TenantId)).OrderBy(l => l.DisplayName);
            var defaultLanguage = await _applicationLanguageManager.GetDefaultLanguageOrNullAsync(AbpSession.TenantId);

            return new GetLanguagesOutput(
                ObjectMapper.Map<List<ApplicationLanguageListDto>>(languages),
                defaultLanguage?.Name
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_Create, AppPermissions.Pages_Administration_Languages_Edit)]
        public async Task<GetLanguageForEditOutput> GetLanguageForEdit(NullableIdDto input)
        {
            ApplicationLanguage language = null;
            if (input.Id.HasValue)
            {
                language = await _languageRepository.GetAsync(input.Id.Value);
            }

            var output = new GetLanguageForEditOutput
            {

                //Language
                Language = language != null
                ? ObjectMapper.Map<ApplicationLanguageEditDto>(language)
                : new ApplicationLanguageEditDto()
            };

            //Language names
            output.LanguageNames = _applicationCulturesProvider
                .GetAllCultures()
                .Select(c => new ComboboxItemDto(c.Name, c.EnglishName + " (" + c.Name + ")") { IsSelected = output.Language.Name == c.Name })
                .ToList();

            //Flags
            output.Flags = FamFamFamFlagsHelper
                .FlagClassNames
                .OrderBy(f => f)
                .Select(f => new ComboboxItemDto(f, FamFamFamFlagsHelper.GetCountryCode(f)) { IsSelected = output.Language.Icon == f })
                .ToList();

            return output;
        }

        public async Task CreateOrUpdateLanguage(CreateOrUpdateLanguageInput input)
        {
            if (input.Language.Id.HasValue)
            {
                await UpdateLanguageAsync(input);
            }
            else
            {
                await CreateLanguageAsync(input);
            }
        }

        public async Task DeleteLanguage(EntityDto input)
        {
            var language = await _languageRepository.GetAsync(input.Id);
            await _applicationLanguageManager.RemoveAsync(AbpSession.TenantId, language.Name);
        }

        public async Task SetDefaultLanguage(SetDefaultLanguageInput input)
        {
            await _applicationLanguageManager.SetDefaultLanguageAsync(
                AbpSession.TenantId,
               CultureHelper.GetCultureInfoByChecking(input.Name).Name
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_ChangeTexts)]
        public async Task<PagedResultDto<LanguageTextListDto>> GetLanguageTexts(GetLanguageTextsInput input)
        {
            /* Note: This method is used by SPA without paging, MPA with paging.
             * So, it can both usable with paging or not */

            //Normalize base language name
            if (input.BaseLanguageName.IsNullOrEmpty())
            {
                var defaultLanguage = await _applicationLanguageManager.GetDefaultLanguageOrNullAsync(AbpSession.TenantId);
                if (defaultLanguage == null)
                {
                    defaultLanguage = (await _applicationLanguageManager.GetLanguagesAsync(AbpSession.TenantId)).FirstOrDefault();
                    if (defaultLanguage == null)
                    {
                        throw new Exception("No language found in the application!");
                    }
                }

                input.BaseLanguageName = defaultLanguage.Name;
            }

            var source = LocalizationManager.GetSource(input.SourceName);
            var baseCulture = CultureInfo.GetCultureInfo(input.BaseLanguageName);
            var targetCulture = CultureInfo.GetCultureInfo(input.TargetLanguageName);

            var languageTexts = source
                .GetAllStrings()
                .Select(localizedString => new LanguageTextListDto
                {
                    Key = localizedString.Name,
                    BaseValue = _applicationLanguageTextManager.GetStringOrNull(AbpSession.TenantId, source.Name, baseCulture, localizedString.Name),
                    TargetValue = _applicationLanguageTextManager.GetStringOrNull(AbpSession.TenantId, source.Name, targetCulture, localizedString.Name, false)
                })
                .AsQueryable();

            //Filters
            if (input.TargetValueFilter == "EMPTY")
            {
                languageTexts = languageTexts.Where(s => s.TargetValue.IsNullOrEmpty());
            }

            if (!input.FilterText.IsNullOrEmpty())
            {
                languageTexts = languageTexts.Where(
                    l => (l.Key != null && l.Key.Contains(input.FilterText, StringComparison.CurrentCultureIgnoreCase)) ||
                         (l.BaseValue != null && l.BaseValue.Contains(input.FilterText, StringComparison.CurrentCultureIgnoreCase)) ||
                         (l.TargetValue != null && l.TargetValue.Contains(input.FilterText, StringComparison.CurrentCultureIgnoreCase))
                    );
            }

            var totalCount = languageTexts.Count();

            //Ordering
            if (!input.Sorting.IsNullOrEmpty())
            {
                languageTexts = languageTexts.OrderBy(input.Sorting);
            }

            //Paging
            if (input.SkipCount > 0)
            {
                languageTexts = languageTexts.Skip(input.SkipCount);
            }

            if (input.MaxResultCount > 0)
            {
                languageTexts = languageTexts.Take(input.MaxResultCount);
            }

            return new PagedResultDto<LanguageTextListDto>(
                totalCount,
                languageTexts.ToList()
                );
        }

        public async Task UpdateLanguageText(UpdateLanguageTextInput input)
        {
            var culture = CultureHelper.GetCultureInfoByChecking(input.LanguageName);
            var source = LocalizationManager.GetSource(input.SourceName);
            await _applicationLanguageTextManager.UpdateStringAsync(AbpSession.TenantId, source.Name, culture, input.Key, input.Value);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_Create)]
        protected virtual async Task CreateLanguageAsync(CreateOrUpdateLanguageInput input)
        {
            if (AbpSession.MultiTenancySide != MultiTenancySides.Host)
            {
                throw new UserFriendlyException(L("TenantsCannotCreateLanguage"));
            }

            var culture = CultureHelper.GetCultureInfoByChecking(input.Language.Name);

            await CheckLanguageIfAlreadyExists(culture.Name);

            await _applicationLanguageManager.AddAsync(
                new ApplicationLanguage(
                    AbpSession.TenantId,
                    culture.Name,
                    culture.DisplayName,
                    input.Language.Icon
                )
                {
                    IsDisabled = !input.Language.IsEnabled
                }
            );
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Languages_Edit)]
        protected virtual async Task UpdateLanguageAsync(CreateOrUpdateLanguageInput input)
        {
            Debug.Assert(input.Language.Id != null, "input.Language.Id != null");

            var culture = CultureHelper.GetCultureInfoByChecking(input.Language.Name);

            await CheckLanguageIfAlreadyExists(culture.Name, input.Language.Id.Value);

            var language = await _languageRepository.GetAsync(input.Language.Id.Value);

            language.Name = culture.Name;
            language.DisplayName = culture.DisplayName;
            language.Icon = input.Language.Icon;
            language.IsDisabled = !input.Language.IsEnabled;

            await _applicationLanguageManager.UpdateAsync(AbpSession.TenantId, language);
        }

        private async Task CheckLanguageIfAlreadyExists(string languageName, int? expectedId = null)
        {
            var existingLanguage = (await _applicationLanguageManager.GetLanguagesAsync(AbpSession.TenantId))
                .FirstOrDefault(l => l.Name == languageName);

            if (existingLanguage == null)
            {
                return;
            }

            if (expectedId != null && existingLanguage.Id == expectedId.Value)
            {
                return;
            }

            throw new UserFriendlyException(L("ThisLanguageAlreadyExists"));
        }

        public async Task<LanguagesOutput> GetLanguagesForTranslate()
        {
            var en_us = new Dictionary<string, string>();
            var vi_vn = new Dictionary<string, string>();
            var output = new LanguagesOutput(en_us, vi_vn);
            // source
            try
            {
                var source = await _MstSleSourceRepo.GetAll().OrderBy(s => s.Ordering).Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in source.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // hotness
            try
            {
                var hotness = await _MstSleHotnessrepo.GetAll().OrderBy(s => s.Ordering).Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in hotness.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // expectedTiming
            try
            {
                var expectedTiming = await _MstSleExpectedDelTiming.GetAll().OrderBy(s => s.Ordering).Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in expectedTiming.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // Color
            try
            {
                var color = await _MstSleColorsRepo.GetAll().Select(c => new
                {
                    c.EnName,
                    c.VnName
                }).ToListAsync();
                foreach (var s in color.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.EnName ?? s.VnName, s.EnName ?? s.VnName);
                        output.vi_vn.Add(s.EnName ?? s.VnName, s.VnName ?? s.EnName);
                    }
                    catch { }
                }
            }
            catch
            {

            }

            // Occupatation
            try
            {
                var occupatations = await _MstSleOccupationRepo.GetAll().OrderBy(s => s.Ordering).Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in occupatations.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // hobby
            try
            {
                var hobbies = await _MstSleHobbyRepo.GetAll().OrderBy(s => s.Ordering).Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in hobbies.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // sourceOfInfo
            try
            {
                var sourceOfInfo = await _MstSleSourceOfInfo.GetAll().OrderBy(s => s.Ordering).Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in sourceOfInfo.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            //social chanel
            try
            {
                var socialChanels = await _MstSleSocialChannelRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in socialChanels.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // Purpose
            try
            {
                var purposes = await _MstSlePurposeRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in purposes.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            //businessType
            try
            {
                var businessTypes = await _MstSleBusinessTypeRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in businessTypes.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // reason of changes
            try
            {
                var reasonOfChanges = await _MstSleReasonOfChangeRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in reasonOfChanges.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // reason of NA
            try
            {
                var reasonOfNA = await _MstSleReasonOfNARepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in reasonOfNA.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }

            // reason of changes
            try
            {
                var reasonOfChanges = await _MstSleReasonOfChangeRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in reasonOfChanges.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // sales stage
            try
            {
                var saleStages = await _MstSleSalesStageRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in saleStages.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            //Reason Of Lost
            try
            {
                var reasonlosts = await _MstSleReasonOfLostRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in reasonlosts.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // reason of lost type
            try
            {
                var reasonlostTypes = await _MstSleReasonOfLostTypeRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in reasonlostTypes.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {
            }
            // FAR
            try
            {
                var FARs = await _MstSleFARRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in FARs.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }

            // contract by
            try
            {
                var contractbies = await _MstSleContactByRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in contractbies.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }
            // reason of NA
            try
            {
                var contractbies = await _MstSleReasonOfNARepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in contractbies.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }
            // commission source
            try
            {
                var commissionSources = await _MstSleCommissionSourceRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in commissionSources.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }

            // activity
            try
            {
                var activities = await _MstSleActivityRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in activities.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }
            // age range
            try
            {
                var ageRanges = await _MstSleAgeRangeRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in ageRanges.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }
            // company
            try
            {
                var companies = await _MstSleCompanyRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in companies.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }

            // relationships
            try
            {
                var relationships = await _MstSleRelationshipRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in relationships.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }

            try
            {
                var relationships = await _MstSleReasonOfNCRepo.GetAll().Select(s => new
                {
                    s.Description,
                    s.Vi_Description
                }).ToListAsync();
                foreach (var s in relationships.ToList())
                {
                    try
                    {
                        output.en_us.Add(s.Description, s.Description);
                        output.vi_vn.Add(s.Description, s.Vi_Description);
                    }
                    catch { }
                }
            }
            catch (Exception)
            {

            }

            return output;
        }
    }
}