﻿using Abp.Application.Editions;
using Abp.Application.Features;
using Abp.Auditing;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.DynamicEntityParameters;
using Abp.EntityHistory;
using Abp.Localization;
using Abp.Notifications;
using Abp.Organizations;
using Abp.UI.Inputs;
using Abp.Webhooks;
using AutoMapper;
using tmss.Auditing.Dto;
using tmss.Authorization.Accounts.Dto;
using tmss.Authorization.Delegation;
using tmss.Authorization.Permissions.Dto;
using tmss.Authorization.Roles;
using tmss.Authorization.Roles.Dto;
using tmss.Authorization.Users;
using tmss.Authorization.Users.Delegation.Dto;
using tmss.Authorization.Users.Dto;
using tmss.Authorization.Users.Importing.Dto;
using tmss.Authorization.Users.Profile.Dto;
using tmss.Chat;
using tmss.Chat.Dto;
using tmss.Customer;
using tmss.DynamicEntityParameters.Dto;
using tmss.Editions;
using tmss.Editions.Dto;
using tmss.Friendships;
using tmss.Friendships.Cache;
using tmss.Friendships.Dto;
using tmss.Localization.Dto;
using tmss.Master;
using tmss.MstSle;
using tmss.MstSle.Dto.AgeRange;
using tmss.MstSle.Dto.Bank;
using tmss.MstSle.Dto.BusinessType;
using tmss.MstSle.Dto.Color;
using tmss.MstSle.Dto.ColorByGrade.Dto;
using tmss.MstSle.Dto.Company;
using tmss.MstSle.Dto.ContactBy;
using tmss.MstSle.Dto.District;
using tmss.MstSle.Dto.ExpectedDelTiming;
using tmss.MstSle.Dto.FAR;
using tmss.MstSle.Dto.Grade;
using tmss.MstSle.Dto.GradeProduction;
using tmss.MstSle.Dto.Hobby;
using tmss.MstSle.Dto.Hotness;
using tmss.MstSle.Dto.LookUp;
using tmss.MstSle.Dto.Make;
using tmss.MstSle.Dto.MakeCompetitor.Dto;
using tmss.MstSle.Dto.Model;
using tmss.MstSle.Dto.Occupation;
using tmss.MstSle.Dto.Province;
using tmss.MstSle.Dto.Purpose;
using tmss.MstSle.Dto.ReasonOfChanges;
using tmss.MstSle.Dto.ReasonOfLost;
using tmss.MstSle.Dto.ReasonOfLostType;
using tmss.MstSle.Dto.ReasonOfNA;
using tmss.MstSle.Dto.ReasonOfNC;
using tmss.MstSle.Dto.Relationship;
using tmss.MstSle.Dto.SalePerson;
using tmss.MstSle.Dto.SalesStage;
using tmss.MstSle.Dto.SocialChannel;
using tmss.MstSle.Dto.Source;
using tmss.MstSle.Dto.SourceOfInfo;
using tmss.MstSle.Dto.TestDrivePlace;
using tmss.MstSle.Dto.TestDriveRoute;
using tmss.MstSle.Dto.TestDriveStall;
using tmss.MstSle.Dto.TestDriveVehicle;
using tmss.MstSle.Dto.VehiclePrice;
using tmss.MstSle.IMstGenProvince.Dto;
using tmss.MstSle.IMstSleDistrict.Dto;
using tmss.MstSle.IMstSleGenDealer.Dto;
using tmss.MstSle.IMstSleGrade.Dto;
using tmss.MstSle.IMstSleGradeProduction.Dto;
using tmss.MstSle.IMstSleModel.Dto;
using tmss.MstSle.IMstSleModelPresent.Dto;
using tmss.MstSle.IMstSleReasonOfFreeze.Dto;
using tmss.MstSle.IMstSleTestDrivePlace.Dto;
using tmss.MstSle.IMtSleColor.Dto;
using tmss.MultiTenancy;
using tmss.MultiTenancy.Dto;
using tmss.MultiTenancy.HostDashboard.Dto;
using tmss.MultiTenancy.Payments;
using tmss.MultiTenancy.Payments.Dto;
using tmss.Notifications.Dto;
using tmss.Organizations.Dto;
using tmss.Sales;
using tmss.Sales.Customer.Dto;
using tmss.Sales.SalesPerson.Dtos;
using tmss.Sessions.Dto;
using tmss.WebHooks.Dto;

namespace tmss
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression configuration)
        {
            //Inputs
            configuration.CreateMap<CheckboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<SingleLineStringInputType, FeatureInputTypeDto>();
            configuration.CreateMap<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<IInputType, FeatureInputTypeDto>()
                .Include<CheckboxInputType, FeatureInputTypeDto>()
                .Include<SingleLineStringInputType, FeatureInputTypeDto>()
                .Include<ComboboxInputType, FeatureInputTypeDto>();
            configuration.CreateMap<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<ILocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>()
                .Include<StaticLocalizableComboboxItemSource, LocalizableComboboxItemSourceDto>();
            configuration.CreateMap<LocalizableComboboxItem, LocalizableComboboxItemDto>();
            configuration.CreateMap<ILocalizableComboboxItem, LocalizableComboboxItemDto>()
                .Include<LocalizableComboboxItem, LocalizableComboboxItemDto>();

            //Chat
            configuration.CreateMap<ChatMessage, ChatMessageDto>();
            configuration.CreateMap<ChatMessage, ChatMessageExportDto>();

            //Feature
            configuration.CreateMap<FlatFeatureSelectDto, Feature>().ReverseMap();
            configuration.CreateMap<Feature, FlatFeatureDto>();

            //Role
            configuration.CreateMap<RoleEditDto, Role>().ReverseMap();
            configuration.CreateMap<Role, RoleListDto>();

            configuration.CreateMap<UserRole, UserListRoleDto>();

            //Edition
            configuration.CreateMap<EditionEditDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<EditionCreateDto, SubscribableEdition>();
            configuration.CreateMap<EditionSelectDto, SubscribableEdition>().ReverseMap();
            configuration.CreateMap<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<Edition, EditionInfoDto>().Include<SubscribableEdition, EditionInfoDto>();

            configuration.CreateMap<SubscribableEdition, EditionListDto>();
            configuration.CreateMap<Edition, EditionEditDto>();
            configuration.CreateMap<Edition, SubscribableEdition>();
            configuration.CreateMap<Edition, EditionSelectDto>();

            //Payment
            configuration.CreateMap<SubscriptionPaymentDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPaymentListDto, SubscriptionPayment>().ReverseMap();
            configuration.CreateMap<SubscriptionPayment, SubscriptionPaymentInfoDto>();

            //Permission
            configuration.CreateMap<Permission, FlatPermissionDto>();
            configuration.CreateMap<Permission, FlatPermissionWithLevelDto>();

            //Language
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageListDto>();
            configuration.CreateMap<NotificationDefinition, NotificationSubscriptionWithDisplayNameDto>();
            configuration.CreateMap<ApplicationLanguage, ApplicationLanguageEditDto>()
                .ForMember(ldto => ldto.IsEnabled, options => options.MapFrom(l => !l.IsDisabled));

            //Tenant
            configuration.CreateMap<Tenant, RecentTenant>();
            configuration.CreateMap<Tenant, TenantLoginInfoDto>();
            configuration.CreateMap<Tenant, TenantListDto>();
            configuration.CreateMap<Tenant, TenantListLoginDto>();
            configuration.CreateMap<TenantEditDto, Tenant>().ReverseMap();
            configuration.CreateMap<CurrentTenantInfoDto, Tenant>().ReverseMap();

            //User
            configuration.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
            configuration.CreateMap<User, UserLoginInfoDto>();
            configuration.CreateMap<User, UserListDto>();
            //configuration.CreateMap<User, UserProfileDto>();
            configuration.CreateMap<User, ChatUserDto>();
            configuration.CreateMap<User, OrganizationUnitUserListDto>();
            configuration.CreateMap<Role, OrganizationUnitRoleListDto>();
            configuration.CreateMap<CurrentUserProfileEditDto, User>().ReverseMap();
            configuration.CreateMap<UserLoginAttemptDto, UserLoginAttempt>().ReverseMap();
            configuration.CreateMap<ImportUserDto, User>();

            //AuditLog
            configuration.CreateMap<AuditLog, AuditLogListDto>();
            configuration.CreateMap<EntityChange, EntityChangeListDto>();
            configuration.CreateMap<EntityPropertyChange, EntityPropertyChangeDto>();

            //Friendship
            configuration.CreateMap<Friendship, FriendDto>();
            configuration.CreateMap<FriendCacheItem, FriendDto>();

            //OrganizationUnit
            configuration.CreateMap<OrganizationUnit, OrganizationUnitDto>();

            //Webhooks
            configuration.CreateMap<WebhookSubscription, GetAllSubscriptionsOutput>();
            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOutput>()
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.WebhookName,
                    options => options.MapFrom(l => l.WebhookEvent.WebhookName))
                .ForMember(webhookSendAttemptListDto => webhookSendAttemptListDto.Data,
                    options => options.MapFrom(l => l.WebhookEvent.Data));

            configuration.CreateMap<WebhookSendAttempt, GetAllSendAttemptsOfWebhookEventOutput>();

            configuration.CreateMap<DynamicParameter, DynamicParameterDto>().ReverseMap();
            configuration.CreateMap<DynamicParameterValue, DynamicParameterValueDto>().ReverseMap();
            configuration.CreateMap<EntityDynamicParameter, EntityDynamicParameterDto>()
                .ForMember(dto => dto.DynamicParameterName,
                    options => options.MapFrom(entity => entity.DynamicParameter.ParameterName));
            configuration.CreateMap<EntityDynamicParameterDto, EntityDynamicParameter>();

            configuration.CreateMap<EntityDynamicParameterValue, EntityDynamicParameterValueDto>().ReverseMap();
            //User Delegations
            configuration.CreateMap<CreateUserDelegationDto, UserDelegation>();

            /* ADD YOUR OWN CUSTOM AUTOMAPPER MAPPINGS HERE */
            //configuration.CreateMap<Customer, CustomerListDto>();
            //configuration.CreateMap<UpdateCustomerInput, Customer>()
            //             .ForMember(c => c.Id, op => op.Ignore());

            // New Configuration Here

            configuration.CreateMap<SalesCustomer, SalesCustomerDetailDto>().ReverseMap();
            configuration.CreateMap<SalesCustomerTransactionInfo, CustomerStatusChangeDto>().ReverseMap();

            configuration.CreateMap<MstSleSalesStage, SalesStageListDto>();
            configuration.CreateMap<MstSleReasonOfLost, ReasonOfLostListDto>();
            configuration.CreateMap<MstSleModel, MstSleModelListDto>();
            configuration.CreateMap<MstSleModel, ModelListDto>();
            configuration.CreateMap<MstSleModel, MstSleModelGradeProductInfo>();

            configuration.CreateMap<MstSleModel, ModelDto>();
            configuration.CreateMap<MstGenDealer, MstSleDealerDto>();
            configuration.CreateMap<MstSlePurpose, PurposeListDto>();
            configuration.CreateMap<MstSleFAR, FARListDto>();
            configuration.CreateMap<MstSleReasonOfNA, ReasonOfNAListDto>();
            configuration.CreateMap<MstSleReasonOfNC, ReasonOfNCDto>();
            configuration.CreateMap<MstSleBank, MstSleBankDto>();
            configuration.CreateMap<MstSleBank, MstSleBankFinanceDto>();

            configuration.CreateMap<MstSleDistrict, MstSleDistrictListDto>();
            configuration.CreateMap<MstSleDistrict, MstSleDistrictByProvinceListDto>();
            configuration.CreateMap<MstSleDistrict, GetDistricContractDto>();
            configuration.CreateMap<MstSleFAR, MstSleFARDto>();
            configuration.CreateMap<MstSlePurpose, MstSlePurposeDto>();
            configuration.CreateMap<MstSleAgeRange, MstSleAgeRangeListDto>();
            configuration.CreateMap<MstSleSource, MstSleSourceListDto>();
            configuration.CreateMap<CreateOrEditMstSleSourceDto, MstSleSource>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleInteriorColorGradesProductionDto, MstSleInteriorColorGradesProduction>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleColorGradesProductionDto, MstSleColorGradesProduction>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleModelPresentDto, MstSleModel>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleMakeCompetitorDto, MstSleMake>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleColorDto, MstSleColors>().ReverseMap();
            configuration.CreateMap<MstSleHotness, MstSleHotnessListDto>();
            configuration.CreateMap<MstSleExpectedDelTiming, MstSleExpectedDelTimingListDto>();
            configuration.CreateMap<MstSleHobby, MstSleHobbyListDto>();
            configuration.CreateMap<MstSleOccupation, MstSleOccupationListDto>();
            configuration.CreateMap<MstSleRelationship, MstSleRelationshipListDto>();
            configuration.CreateMap<MstSleRelationship, MstSleRelationshipDto>();
            configuration.CreateMap<MstSleSocialChannel, MstSleSocialChannelListDto>();
            configuration.CreateMap<MstSleSalesPerson, MstSleSalesPersonListByManagerDto>();
            configuration.CreateMap<MstSleSalesPerson, MstSleSalesPersonDto>();
            configuration.CreateMap<MstSleColors, MstSleColorByGradeListDto>();
            configuration.CreateMap<MstSleColors, MstSleColorsCarAttentionDto>();
            configuration.CreateMap<MstSleColors, MstSleColorItemDto>();
            configuration.CreateMap<MstSleColors, ColorDto>();
            configuration.CreateMap<MstSleMake, MstSleMakeOfModelListDto>();
            configuration.CreateMap<MstSleMake, MakeDto>();
            configuration.CreateMap<MstSleTestDriveVehicle, TestDriveVehicleListDto>();
            configuration.CreateMap<MstSleTestDriveVehicle, TestDriverViehicleCusApp>();
            configuration.CreateMap<MstSleTestDrivePlace, TestDrivePlaceListDto>();
            configuration.CreateMap<MstSleTestDriveStall, TestDriveStallListDto>();
            configuration.CreateMap<MstSleTestDriveRoute, TestDriveRouteListDto>();
            configuration.CreateMap<MstSleSource, MstSleSourceDto>();
            configuration.CreateMap<MstGenProvince, MstSleProvinceDto>().ReverseMap();
            configuration.CreateMap<MstSleDistrict, MstSleDistrictDto>().ReverseMap();
            configuration.CreateMap<MstGenProvince, MstSleProvinceContractDto>();
            configuration.CreateMap<MstSleHotness, MstSleHotnessDto>();
            configuration.CreateMap<MstSleExpectedDelTiming, MstSleExpectedDelTimingDto>();
            configuration.CreateMap<MstSleOccupation, MstSleOccupationDto>();
            configuration.CreateMap<MstSleHobby, MstSleHobbyDto>();
            configuration.CreateMap<MstSleModel, MstSleModelListDto>();
            configuration.CreateMap<MstSleModel, MstSleModelCarAttentionDto>();
            configuration.CreateMap<MstSleModel, ModelGradeListDto>();
            configuration.CreateMap<MstSleBusinessType, MstSleBusinessTypeDto>();
            configuration.CreateMap<MstSleSocialChannel, MstSleSocialChannelDto>();
            configuration.CreateMap<MstSleGrades, MstSleGradeByModelListDto>();
            configuration.CreateMap<MstSleGrades, MstSleGradeCarAttentionDto>();
            configuration.CreateMap<MstSleGrades, MstSleGradeGetStockCheckingDto>();
            configuration.CreateMap<MstSleGrades, MstSleGradeItemDto>();
            configuration.CreateMap<MstSleGrades, GradeDto>();
            configuration.CreateMap<MstSleGrades, GradeListDto>();
            configuration.CreateMap<MstSleGrades, GradeProductionListDto>();
            configuration.CreateMap<MstSleGrades, MstSleGradeProduct>();
            configuration.CreateMap<MstSleGradeProduction, ProductionListDto>();
            configuration.CreateMap<MstSleMake, MakeListDto>();
            configuration.CreateMap<MstSleSourceOfInfo, MstSleSourceOfInfoDto>();
            configuration.CreateMap<MstSleAgeRange, MstSleAgeRangeDto>();
            configuration.CreateMap<MstSleRelationship, MstSleRelationshipDto>();
            configuration.CreateMap<MstSleContactBy, ContactByListDto>();

            configuration.CreateMap<MstSleReasonOfLostType, ReasonOfLostTypeListDto>();

            configuration.CreateMap<SalesCustomerSocialChannel, MstSleSocialChannelDto>();

            configuration.CreateMap<MstSleSalesPerson, CreateOrEditSalesPersonDto>();
            configuration.CreateMap<CreateOrEditSalesPersonDto, MstSleSalesPerson>();

            //MstSleExpectedDelTiming
            configuration.CreateMap<MstSleExpectedDelTiming, CreateOrEditMstSleExpectedDelTimingDto>().ReverseMap();

            //MstSleHotness
            configuration.CreateMap<MstSleHotness, CreateOrEditMstSleHotnessDto>().ReverseMap();

            //MstSleOccupation
            configuration.CreateMap<MstSleOccupation, CreateOrEditMstSleOccupationDto>().ReverseMap();

            //MstSleHobby
            configuration.CreateMap<MstSleHobby, CreateOrEditMstSleHobbyDto>().ReverseMap();

            //MstSleSourceOfInfo
            configuration.CreateMap<MstSleSourceOfInfo, CreateOrEditMstSleSourceOfInfoDto>().ReverseMap();

            //MstSleSocialChannel
            configuration.CreateMap<MstSleSocialChannel, CreateOrEditMstSleSocialChannelDto>().ReverseMap();

            //MstSleBussinessType
            configuration.CreateMap<MstSleBusinessType, CreateOrEditMstSleBusinessTypeDto>().ReverseMap();

            //MstSleTestDriveRoute
            configuration.CreateMap<MstSleTestDriveRoute, CreateOrEditMstSleTestDriveRouteDto>().ReverseMap();

            //MstSleTestDriveStall
            configuration.CreateMap<MstSleTestDriveStall, CreateOrEditMstSleTestDriveStallDto>().ReverseMap();

            //MstSleTestDriveVehicle
            configuration.CreateMap<MstSleTestDriveVehicle, CreateOrEditMstSleTestDriveVehicleDto>().ReverseMap();

            //MstSleTestVehiclePrice
            configuration.CreateMap<MstSleVehiclePrice, MstSleVehiclePriceDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleVehiclePriceDto, MstSleVehiclePrice>().ReverseMap();
            configuration.CreateMap<MstSleVehiclePrice, CreateOrEditMstSleVehiclePriceDto>().ReverseMap();

            //MstSleTestDrivePlace
            configuration.CreateMap<MstSleTestDrivePlace, CreateOrEditMstSleTestDrivePlaceDto>().ReverseMap();

            //MstSleCompany
            configuration.CreateMap<MstSleCompany, CreateOrEditMstSleCompanyDto>().ReverseMap();
            //MstSlePurpose
            configuration.CreateMap<MstSlePurpose, CreateOrEditMstSlePurposeDto>().ReverseMap();
            //MstSleFar
            configuration.CreateMap<MstSleFAR, CreateOrEditMstSleFarDto>().ReverseMap();
            //MstSleReasonOfChange
            configuration.CreateMap<MstSleReasonOfChange, CreateOrEditMstSleReasonOfChangesDto>().ReverseMap();
            //MstSleSalesStage
            configuration.CreateMap<MstSleSalesStage, CreateOrEditMstSleSalesStageDto>().ReverseMap();
            //MstSleReasonOfLostType
            configuration.CreateMap<MstSleReasonOfLostType, CreateOrEditMstSleReasonOfLostTypeDto>().ReverseMap();
            //MstSleReasonOfLostType
            configuration.CreateMap<MstSleReasonOfLost, CreateOrEditMstSleReasonOfLostDto>().ReverseMap();

            //MstSleReasonOfNA
            configuration.CreateMap<MstSleReasonOfNA, CreateOrEditMstSleReasonOfNADto>().ReverseMap();

            //MstSleBank
            configuration.CreateMap<MstSleBank, CreateOrEditMstSleBankDto>().ReverseMap();
            configuration.CreateMap<MstSleBank, CreateOrEditMstSleBankMasterDto>().ReverseMap();

            #region "Master Province"
            configuration.CreateMap<CreateOrEditMstGenProvinceDto, MstGenProvince>().ReverseMap();
            configuration.CreateMap<MstGenProvinceDto, MstGenProvince>().ReverseMap();
            #endregion
            #region "Master District"
            configuration.CreateMap<MstSleDistrict, MstSleDistrictsDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleDistrictsDto, MstSleDistrict>().ReverseMap();
            #endregion

            #region "Master Model"
            configuration.CreateMap<MstSleModel, MstSleModelsDto>().ReverseMap();
            configuration.CreateMap<CreateOrEditMstSleModelsDto, MstSleModel>().ReverseMap();
            #endregion
            #region "Master Grades"
            configuration.CreateMap<CreateOrEditMstSleGradesDto, MstSleGrades>().ReverseMap();
            configuration.CreateMap<MstSleGradesDto, MstSleGrades>().ReverseMap();
            #endregion
            #region "Master Grade Production"
            configuration.CreateMap<CreateOrEditMstSleGradeProductionDto, MstSleGradeProduction>().ReverseMap();
            configuration.CreateMap<MstSleGradeProductionDto, MstSleGradeProduction>().ReverseMap();
            #endregion

            #region "MstSleReasonOfNC"
            configuration.CreateMap<MstSleReasonOfNC, CreateOrEditMstSleReasonOfNCDto>().ReverseMap();
            #endregion


            #region "Master Contact By"
            configuration.CreateMap<MstSleContactBy, CreateOrEditMstSleContactByDto>().ReverseMap();
            #endregion

            configuration.CreateMap<MstGenDealer, CreateOrEditMstGenDealersDto>().ReverseMap();

            #region "Master Lookup"
            configuration.CreateMap<MstSleLookUp, CreateOrEditMstSleLookupDto>().ReverseMap();
            #endregion

            #region "Sales Customer Vehicle Info"
            configuration.CreateMap<SalesCustomerVehicleInfo, SalesCustomerCarAttentionDto>().ReverseMap();
            #endregion

            #region "Sales Customer Contact Info"
            configuration.CreateMap<SalesCustomerTransactionInfoContact, SalesCustomerContactInfoDto>().ReverseMap();
            #endregion

            #region "MstSleReasonOfFreeze"
            configuration.CreateMap<CreateOrEditMstSleReasonOfFreezeDto, MstSleReasonOfFreeze>().ReverseMap();
            #endregion
        }
    }
}