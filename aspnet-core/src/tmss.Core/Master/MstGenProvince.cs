﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstGenProvince")]
    public class MstGenProvince : FullAuditedEntity<long>, IEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Code { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
        [Required]
        [StringLength(1)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public decimal RegionId { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PopulationAmount { get; set; }
        [Column(TypeName = "numeric(5, 0)")]
        public decimal? PercentOwnershipTax { get; set; }
        [Column(TypeName = "numeric(15, 0)")]
        public long? RegistrationFee { get; set; }
        [Column(TypeName = "numeric(12, 2)")]
        public decimal? SquareAmount { get; set; }
        public decimal? PercentRegistrationFee { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public decimal? DealerId { get; set; }
        public long? SubRegionId { get; set; }
    }
}