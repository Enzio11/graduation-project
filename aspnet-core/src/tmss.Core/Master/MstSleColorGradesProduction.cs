﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleColorGradesProduction")]
    public class MstSleColorGradesProduction : FullAuditedEntity<long>, IEntity<long>
    {
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? ColorId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? ProduceId { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? DealerId { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
    }
}