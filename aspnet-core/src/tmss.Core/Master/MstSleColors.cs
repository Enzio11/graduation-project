﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleColors")]
    public class MstSleColors : FullAuditedEntity<long>, IEntity<long>
    {
        public string Code { get; set; }
        public string HexCode { get; set; }
        public string VnName { get; set; }
        public string EnName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }
        public decimal? OrderingRpt { get; set; }
        public string HexCode2 { get; set; }
    }
}