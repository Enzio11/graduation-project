﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleHotness")]
    public class MstSleHotness : FullAuditedEntity<long>, IEntity<long>
    {
        public string Code { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        public long? Ordering { get; set; }
        [MaxLength(255)]
        public string Vi_Description { get; set; }
    }
}
