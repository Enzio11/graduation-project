﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleReasonOfFreeze")]
    public partial class MstSleReasonOfFreeze : FullAuditedEntity<long>, IEntity<long>
    {
        [StringLength(50)]
        public string Code { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [Column("Vi_Description")]
        [StringLength(255)]
        public string ViDescription { get; set; }
        public long? Ordering { get; set; }
    }
}