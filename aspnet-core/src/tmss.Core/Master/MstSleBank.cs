﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle
{
    [Table("MstSleBank")]
    public class MstSleBank : FullAuditedEntity<long>, IEntity<long>
    {

        [StringLength(50)]
        public string BankCode { get; set; }
        [Required]
        [StringLength(255)]
        public string BankName { get; set; }
        [StringLength(50)]
        public string Address { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public long? BankTypeId { get; set; }
        [Column(TypeName = "numeric(1, 0)")]
        public long? IsFinance { get; set; }
        [Column(TypeName = "numeric(1, 0)")]
        public long? Other { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public long? Ordering { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? DealerId { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public long? AccountNumber { get; set; }

    }
}