﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleTestDriveVehicle")]
    public class MstSleTestDriveVehicle : FullAuditedEntity<long>, IEntity<long>
    {
        [MaxLength(255)]
        public string Name { get; set; }
        public long? DealerId { get; set; }
        public string Description { get; set; }
        public long? ModelId { get; set; }
        public long? GradeId { get; set; }
        public long? ColorId { get; set; }
    }
}
