﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleMake")]
    public class MstSleMake : FullAuditedEntity<long>, IEntity<long>
    {
        [MaxLength(255)]
        public string Description { get; set; }
        [MaxLength(255)]
        public string Code { get; set; }
        public long? Ordering { get; set; }
    }
}
