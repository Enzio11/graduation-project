﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleGradeProduction")]
    public class MstSleGradeProduction : FullAuditedEntity<long>, IEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string ProductionCode { get; set; }
        [StringLength(50)]
        public string EnName { get; set; }
        [StringLength(50)]
        public string VnName { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public long? Ordering { get; set; }
        [StringLength(50)]
        public string IsHasAudio { get; set; }
        [StringLength(50)]
        public string CbuCkd { get; set; }
        [Required]
        [StringLength(50)]
        public string ShortModel { get; set; }
        [StringLength(50)]
        public string Wmi { get; set; }
        [StringLength(50)]
        public string Vds { get; set; }
        [Required]
        [StringLength(50)]
        public string FullModel { get; set; }
        [StringLength(50)]
        public string Barcode { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? GasolineTypeId { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? GradeId { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FromDate { get; set; }
        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }
        [StringLength(50)]
        public string IsFirmColor { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? FrameNoLength { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public long? ManYear { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? Capacity { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? Length { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? Width { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? Height { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? Weight { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? TireSize { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? Payload { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? PullingWeight { get; set; }
        [Column(TypeName = "numeric(6, 0)")]
        public long? SeatNoStanding { get; set; }
        [Column(TypeName = "numeric(6, 0)")]
        public long? SeatNoLying { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? SeatNo { get; set; }
        [StringLength(50)]
        public string CaseSize { get; set; }
        [StringLength(50)]
        public string BaseLength { get; set; }
        [StringLength(50)]
        public string Status { get; set; }
        public long? FloId { get; set; }
        public string OriginFrom { get; set; }
        public string Fuel { get; set; }
        public string Engine { get; set; }
        public string DesignStyle { get; set; }
    }
}