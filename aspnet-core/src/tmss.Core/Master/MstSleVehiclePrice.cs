﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleVehiclePrice")]
    public class MstSleVehiclePrice : FullAuditedEntity<long>, IEntity<long>
    {
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? GradeProductionId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ColorId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ModelId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? GradeId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? InteriorColorId { get; set; }
        public virtual long? PriceAmount { get; set; }
        public virtual long? OrderPriceAmount { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public virtual long? Ordering { get; set; }
        [StringLength(1)]
        public virtual string Status { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? DealerId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? TransmissionId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? EngineTypeId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? FuelTypeId { get; set; }
        public virtual string Remarks { get; set; }
        public virtual string PartInfo { get; set; }
        public virtual string AccessoriesInfo { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? isCBU { get; set; }
        [Column(TypeName = "numeric(5, 0)")]
        public virtual long? StyleCarId { get; set; }
        public virtual string CommercialName { get; set; }
    }
}
