﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleDistrict")]
    public class MstSleDistrict : FullAuditedEntity<long>, IEntity<long>
    {
        [StringLength(50)]
        public string Code { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        public long? ProvinceId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? DealerId { get; set; }
        [StringLength(1)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
    }
}