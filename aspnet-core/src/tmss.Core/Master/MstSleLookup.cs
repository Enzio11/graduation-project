﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleLookUp")]
    public class MstSleLookUp : FullAuditedEntity, IEntity
    {
        [Required]
        public string Code { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(1)]
        public string Status { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
        public int? Value { get; set; }
    }
}