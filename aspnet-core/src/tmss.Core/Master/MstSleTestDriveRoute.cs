using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleTestDriveRoute")]
    public class MstSleTestDriveRoute : FullAuditedEntity<long>, IEntity<long>
    {
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        public long? DealerId { get; set; }
        public int? Type { get; set; }
    }
}
