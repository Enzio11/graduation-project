﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstGenDealer")]
    public class MstGenDealer : FullAuditedEntity<long>, IEntity<long>
    {
        [Required]
        [StringLength(50)]
        public string Code { get; set; }
        [StringLength(50)]
        public string AccountNo { get; set; }
        [StringLength(50)]
        public string Bank { get; set; }
        [StringLength(2000)]
        public string BankAddress { get; set; }
        public string VnName { get; set; }
        public string EnName { get; set; }
        [StringLength(2000)]
        public string Address { get; set; }
        [StringLength(50)]
        public string Abbreviation { get; set; }
        [StringLength(50)]
        public string ContactPerson { get; set; }
        [StringLength(25)]
        public string Phone { get; set; }
        [StringLength(50)]
        public string Status { get; set; }
        [StringLength(25)]
        public string Fax { get; set; }
        [StringLength(250)]
        public string Description { get; set; }
        [StringLength(1)]
        public string IsSpecial { get; set; }
        [Column(TypeName = "numeric(10, 0)")]
        public decimal? Ordering { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? ProvinceId { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? DistrictId { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? DealerParentsId { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? DealerTypeId { get; set; }
        [StringLength(50)]
        public string TaxCode { get; set; }
        [StringLength(1)]
        public string IsSumDealer { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? DealerGroupId { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? GroupDispatchFromId { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? GroupArrivalToId { get; set; }
        [StringLength(50)]
        public string BiServer { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? TfsAmount { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public decimal? PartLeadtime { get; set; }
        [StringLength(50)]
        public string IpAddress { get; set; }
        [StringLength(1)]
        public string Islexus { get; set; }
        [StringLength(1)]
        public string IsSellLexusPart { get; set; }
        [StringLength(1)]
        public string IsDlrSales { get; set; }
        [StringLength(2000)]
        public string RecievingAddress { get; set; }
        [StringLength(2000)]
        public string DlrFooter { get; set; }
        [StringLength(1)]
        public string IsPrint { get; set; }
        [StringLength(50)]
        public string PasswordSearchVin { get; set; }
        [StringLength(255)]
        public string AuthorizationInfo { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public decimal? PortRegion { get; set; }
        public decimal? IsDealer { get; set; }
        [StringLength(250)]
        public string ContractRole { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? GroupManagerId { get; set; }
        [Column(TypeName = "numeric(18, 8)")]
        public decimal? Lat { get; set; }
        [Column(TypeName = "numeric(18, 8)")]
        public decimal? Long { get; set; }
        public string ShortDescriptionName { get; set; }
        public string dealerModel { get; set; }
        #region -- Begin Add Custom Here
        public int? GroupSwapId { get; set; }
        #endregion
        public string MapUrl { get; set; }
        public string LinkWebsite { get; set; }
        public string OperatingTime { get; set; }
        public virtual bool? IsUsingApp { get; set; }
        public long? GroupInsuranceId { get; set; }
        public string ShortName { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public decimal? GroupLoyaltyId { get; set; }
        public string ClaimEmail { get; set; }
        public long? LoyaltyClaimRemindTime { get; set; }
    }
}