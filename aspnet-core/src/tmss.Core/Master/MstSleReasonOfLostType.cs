﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleReasonOfLostType")]
    public class MstSleReasonOfLostType : FullAuditedEntity<long>, IEntity<long>
    {
        [MaxLength(50)]
        public string Code { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public long? Ordering { get; set; }
    }
}
