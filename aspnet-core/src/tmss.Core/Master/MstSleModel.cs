﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleModel")]
    public class MstSleModel : FullAuditedEntity<long>, IEntity<long>
    {
        public string MarketingCode { get; set; }
        public string MarketingCodeWeb { get; set; }
        [StringLength(50)]
        public string ProductionCode { get; set; }
        [StringLength(50)]
        public string EnName { get; set; }
        [StringLength(50)]
        public string VnName { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        [Required]
        [StringLength(50)]
        public string Status { get; set; }
        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public decimal? OrderingRpt { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? MakeId { get; set; }
        [Column(TypeName = "numeric(15, 0)")]
        public long? VRFee { get; set; }
        [Column(TypeName = "numeric(15, 0)")]
        public long? CertificationFee { get; set; }
        [Column(TypeName = "numeric(5, 0)")]
        public long? PercentBalloon { get; set; }
        [Column(TypeName = "numeric(1, 0)")]
        public long? Pickup { get; set; }
        [StringLength(50)]
        public string Abbreviation { get; set; }
        [StringLength(255)]
        public string ProductSlogan { get; set; }
    }
}
