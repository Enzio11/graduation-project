﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleBusinessType")]
    public class MstSleBusinessType : FullAuditedEntity<long>, IEntity<long>
    {
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public string Code { get; set; }
        public long? Ordering { get; set; }
    }
}
