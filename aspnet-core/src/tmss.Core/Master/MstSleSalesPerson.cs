﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Master
{
    [Table("MstSleSalesPerson")]
    public class MstSleSalesPerson : FullAuditedEntity<long>, IEntity<long>, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        public long? SaleManagerId { get; set; }
        public long? UserId { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? SaleTeamId { get; set; }
        public string FullName { get; set; }
        public DateTime? Birthday { get; set; }
        public string Position { get; set; }
        public string Abbreviation { get; set; }
        public string Status { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Avatar { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? Ordering { get; set; }
        public bool? IsContractApprove { get; set; }
        public bool? IsProposalApprove { get; set; }
        public bool? IsViewCustomer { get; set; }
        public bool? IsApproveTestDrive { get; set; }
        public bool? IsApproveLostFreeze { get; set; }
        public bool? IsViewStock { get; set; }
        public bool? IsSeekforSupervisor { get; set; }
        public bool? IsReceiveAssignmentList { get; set; }
        public bool? IsViewDardboard { get; set; }
        public bool? IsReceiveTestDriveList { get; set; }
        public bool? IsRepresent { get; set; }
        public bool? IsViewDuplicateCustomer { get; set; }
        public bool? IsViewPhoneNumber { get; set; }
        public bool? IsReviewFleet { get; set; }
        public bool? IsApproveFleet { get; set; }
        public bool? IsCreateTransferCustomer { get; set; }
        public bool? IsSaleAdmin { get; set; }

        public long? EmpId { get; set; }
        public bool? IsActive { get; set; }
    }
}
