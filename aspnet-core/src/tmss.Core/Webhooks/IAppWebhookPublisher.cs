﻿using System.Threading.Tasks;

namespace tmss.WebHooks
{
    public interface IAppWebhookPublisher
    {
        Task PublishTestWebhook();
    }
}
