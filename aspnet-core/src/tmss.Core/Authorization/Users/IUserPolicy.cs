﻿using Abp.Domain.Policies;
using System.Threading.Tasks;

namespace tmss.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
