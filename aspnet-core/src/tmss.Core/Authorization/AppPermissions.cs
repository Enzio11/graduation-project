﻿namespace tmss.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        public const string Pages_Sales_FbLeads = "Pages.Sales.FbLeads";
        public const string Pages_Sales_FbDelete = "Pages.Sales.FbLeads.Delete";

        public const string Pages_SalesCustomerVehicleInfo = "Pages.SalesCustomerVehicleInfo";
        public const string Pages_SalesCustomerVehicleInfo_Create = "Pages.SalesCustomerVehicleInfo.Create";
        public const string Pages_SalesCustomerVehicleInfo_Edit = "Pages.SalesCustomerVehicleInfo.Edit";
        public const string Pages_SalesCustomerVehicleInfo_Delete = "Pages.SalesCustomerVehicleInfo.Delete";

        public const string Pages_SalesCustomerTransactionInfo = "Pages.SalesCustomerTransactionInfo";
        public const string Pages_SalesCustomerTransactionInfo_ApproveTransaction = "Pages.SalesCustomerTransactionInfo.GetListCustomerTranferLossFreezeApprover";
        public const string Pages_SalesCustomerTransactionInfo_GetListCustomerTranferLossFreezeApprover = "Pages.SalesCustomerTransactionInfo.ApproveTransaction";

        public const string Pages_SalesCustomerTestDrive = "Pages.SalesCustomerTestDrive";
        public const string Pages_SalesCustomerTestDrive_Create = "Pages.SalesCustomerTestDrive.Create";
        public const string Pages_SalesCustomerTestDrive_Edit = "Pages.SalesCustomerTestDrive.Edit";
        public const string Pages_SalesCustomerTestDrive_Delete = "Pages.SalesCustomerTestDrive.Delete";

        public const string Pages_SalesCustomerManagerComment = "Pages.SalesCustomerManagerComment";
        public const string Pages_SalesCustomerManagerComment_Create = "Pages.SalesCustomerManagerComment.Create";
        public const string Pages_SalesCustomerManagerComment_Edit = "Pages.SalesCustomerManagerComment.Edit";
        public const string Pages_SalesCustomerManagerComment_Delete = "Pages.SalesCustomerManagerComment.Delete";

        public const string Pages_SalesCustomerAppointment = "Pages.SalesCustomerAppointment";
        public const string Pages_SalesCustomerAppointment_Get = "Pages.SalesCustomerAppointment.Get";
        public const string Pages_SalesCustomerAppointment_Create = "Pages.SalesCustomerAppointment.Create";
        public const string Pages_SalesCustomerAppointment_Edit = "Pages.SalesCustomerAppointment.Edit";
        public const string Pages_SalesCustomerAppointment_Delete = "Pages.SalesCustomerAppointment.Delete";

        public const string Pages_SalesCustomer = "Pages.SalesCustomer";
        public const string Pages_SalesCustomer_View = "Pages.SalesCustomer.View";
        public const string Pages_SalesCustomer_Create = "Pages.SalesCustomer.Create";
        public const string Pages_SalesCustomer_Edit = "Pages.SalesCustomer.Edit";
        public const string Pages_SalesCustomer_Delete = "Pages.SalesCustomer.Delete";
        public const string Pages_SalesCustomer_View_Duplicate_Customer = "Pages.SalesCustomer.ViewDuplicateCustomer";

        public const string Pages_SalesCustomer_ChangeStatus = "Pages.SalesCustomer.ChangeStatus";
        public const string Pages_SalesCustomer_RequestApprove = "Pages.SalesCustomer.RequestApprove";
        public const string Pages_SalesCustomer_IsView = "Pages.SalesCustomer.IsView";

        public const string Pages_Sales_Menu_Person = "Pages.SalesPerson";
        public const string Pages_SalesPerson_Create = "Pages.SalesPerson.Create";
        public const string Pages_SalesPerson_View = "Pages.SalesPerson.View";
        public const string Pages_SalesPerson_Edit = "Pages.SalesPerson.Edit";
        public const string Pages_SalesPerson_Delete = "Pages.SalesPerson.Delete";


        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";
        public const string Mobile = "Mobile";
        public const string WebsiteApi = "WebsiteApi";


        public const string Pages_API_Dealers = "Pages.APIDealers";
        public const string Pages_API_Dealers_GetCustomerInformation = "Pages.APIDealers.GetCustomerInfo";
        public const string Pages_API_Dealers_UpdateCustomerStatus = "Pages.APIDealers.UpdateCustomerStatus";
        public const string Pages_API_Dealers_GetContractInformation = "Pages.APIDealers.GetContractInformation";
        public const string Pages_API_Dealers_UpdateContractInformation = "Pages.APIDealers.UpdateContractInformation";
        public const string Pages_API_Dealers_CreateNewLeadCustomer = "Pages.APIDealers.CreateNewLeadCustomer";

        public const string Pages_DemoUiComponents = "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";
        public const string Pages_Administration_Users_Unlock = "Pages.Administration.Users.Unlock";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_Menu = "Pages.Administration.OrganizationUnits.Menu";
        public const string Pages_Administration_OrganizationUnits_Employee = "Pages.Administration.OrganizationUnits.Employee";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";
        public const string Pages_Administration_OrganizationUnits_ManageRoles = "Pages.Administration.OrganizationUnits.ManageRoles";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        public const string Pages_Administration_WebhookSubscription = "Pages.Administration.WebhookSubscription";
        public const string Pages_Administration_WebhookSubscription_Create = "Pages.Administration.WebhookSubscription.Create";
        public const string Pages_Administration_WebhookSubscription_Edit = "Pages.Administration.WebhookSubscription.Edit";
        public const string Pages_Administration_WebhookSubscription_ChangeActivity = "Pages.Administration.WebhookSubscription.ChangeActivity";
        public const string Pages_Administration_WebhookSubscription_Detail = "Pages.Administration.WebhookSubscription.Detail";
        public const string Pages_Administration_Webhook_ListSendAttempts = "Pages.Administration.Webhook.ListSendAttempts";
        public const string Pages_Administration_Webhook_ResendWebhook = "Pages.Administration.Webhook.ResendWebhook";

        public const string Pages_Administration_DynamicParameters = "Pages.Administration.DynamicParameters";
        public const string Pages_Administration_DynamicParameters_Create = "Pages.Administration.DynamicParameters.Create";
        public const string Pages_Administration_DynamicParameters_Edit = "Pages.Administration.DynamicParameters.Edit";
        public const string Pages_Administration_DynamicParameters_Delete = "Pages.Administration.DynamicParameters.Delete";

        public const string Pages_Administration_DynamicParameterValue = "Pages.Administration.DynamicParameterValue";
        public const string Pages_Administration_DynamicParameterValue_Create = "Pages.Administration.DynamicParameterValue.Create";
        public const string Pages_Administration_DynamicParameterValue_Edit = "Pages.Administration.DynamicParameterValue.Edit";
        public const string Pages_Administration_DynamicParameterValue_Delete = "Pages.Administration.DynamicParameterValue.Delete";

        public const string Pages_Administration_EntityDynamicParameters = "Pages.Administration.EntityDynamicParameters";
        public const string Pages_Administration_EntityDynamicParameters_Create = "Pages.Administration.EntityDynamicParameters.Create";
        public const string Pages_Administration_EntityDynamicParameters_Edit = "Pages.Administration.EntityDynamicParameters.Edit";
        public const string Pages_Administration_EntityDynamicParameters_Delete = "Pages.Administration.EntityDynamicParameters.Delete";

        public const string Pages_Administration_EntityDynamicParameterValue = "Pages.Administration.EntityDynamicParameterValue";
        public const string Pages_Administration_EntityDynamicParameterValue_Create = "Pages.Administration.EntityDynamicParameterValue.Create";
        public const string Pages_Administration_EntityDynamicParameterValue_Edit = "Pages.Administration.EntityDynamicParameterValue.Edit";
        public const string Pages_Administration_EntityDynamicParameterValue_Delete = "Pages.Administration.EntityDynamicParameterValue.Delete";
        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";
        public const string Pages_Editions_MoveTenantsToAnotherEdition = "Pages.Editions.MoveTenantsToAnotherEdition";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";
        public const string Pages_Sales_Menu_Contract_Tmss_Management_For_TMV = "Pages.Sales.Menu.Tmss.Mannagement.For.TMV";
        public const string Pages_Sales_Contract_Cancel_For_TMV = "Pages.Sales.Contract.Cancel.For.TMV";
        public const string Pages_Sales_Contract_Change_For_TMV = "Pages.Sales.Contract.Change.For.TMV";



        #region Sales Person
        public const string Pages_Sales_SalesPerson = "Pages.Sales.SalesPerson";
        public const string Pages_Sales_SalesPerson_Search = "Pages.Sales.SalesPerson.Search";
        public const string Pages_Sales_SalesPerson_Create = "Pages.Sales.SalesPerson.Create";
        public const string Pages_Sales_SalesPerson_Edit = "Pages.Sales.SalesPerson.Edit";
        public const string Pages_Sales_SalesPerson_Delete = "Pages.Sales.SalesPerson.Delete";
        #endregion

        #region "Master"
        public const string Pages_Master = "Pages.Master";

        #region "Common"
        public const string Pages_Master_Common = "Pages.Master.Common";

        #endregion

        #region "Vehicle Info"
        public const string Pages_Master_Vehicle_Info = "Pages.Master.VehicleInfo";

        #endregion

        #region "Accessories"
        public const string Pages_Master_Accessories = "Pages.Master.Accessories";
        #endregion

        #region "Test Driver"
        public const string Pages_Master_TestDriver = "Pages.Master.TestDriver";
        #endregion

        #region "Insurance"
        public const string Pages_Master_Insurance = "Pages.Master.Insurance";
        #endregion

        #region "Finance"
        public const string Pages_Master_Finance = "Pages.Master.Finance";
        #endregion

        #region "Delivery"
        public const string Pages_Master_Delivery = "Pages.Master.Delivery";
        #endregion

        #region "Report Config"
        public const string Pages_Master_Sales_Report = "Pages.Master.Report";
        public const string Pages_Master_Sales_Report_Search = "Pages.Master.Report.Search";
        public const string Pages_Master_Sales_Report_Create = "Pages.Master.Report.Create";
        public const string Pages_Master_Sales_Report_Edit = "Pages.Master.Report.Edit";
        public const string Pages_Master_Sales_Report_Delete = "Pages.Master.Report.Delete";
        #endregion

        #region "Report Config Define"
        public const string Pages_Master_Sales_Report_Definition = "Pages.Master.ReportDefinition";
        public const string Pages_Master_Sales_Report_Definition_Search = "Pages.Master.ReportDefinition.Search";
        public const string Pages_Master_Sales_Report_Definition_Create = "Pages.Master.ReportDefinition.Create";
        public const string Pages_Master_Sales_Report_Definition_Edit = "Pages.Master.ReportDefinition.Edit";
        public const string Pages_Master_Sales_Report_Definition_Delete = "Pages.Master.ReportDefinition.Delete";
        #endregion

        #region "Ip Management"
        public const string Pages_Master_Sales_IP_Managemnet = "Pages.Master.IpManagement";
        public const string Pages_Master_Sales_IP_Managemnet_Search = "Pages.Master.IpManagement.Search";
        public const string Pages_Master_Sales_IP_Managemnet_Create = "Pages.Master.IpManagement.Create";
        public const string Pages_Master_Sales_IP_Managemnet_Edit = "Pages.Master.IpManagement.Edit";
        public const string Pages_Master_Sales_IP_Managemnet_Delete = "Pages.Master.IpManagement.Delete";
        #endregion

        #region "MstSleProductFinanceQuesstion"
        public const string Pages_Master_Sales_Finance_Knowledge = "Pages.Master.FinanceKnowledge";
        public const string Pages_Master_Sales_Insurance_Knowledge = "Pages.Master.InsuranceKnowledge";
        public const string Pages_Master_Sales_Product_Finance_Question = "Pages.Master.ProductFinanceQuestion";
        public const string Pages_Master_Sales_Product_Finance_Question_Search = "Pages.Master.ProductFinanceQuestion.Search";
        public const string Pages_Master_Sales_Product_Finance_Question_Create = "Pages.Master.ProductFinanceQuestion.Create";
        public const string Pages_Master_Sales_Product_Finance_Question_Edit = "Pages.Master.ProductFinanceQuestion.Edit";
        public const string Pages_Master_Sales_Product_Finance_Question_Delete = "Pages.Master.ProductFinanceQuestion.Delete";
        #endregion

        #region "MstSleProductInsuranceQuesstion"
        public const string Pages_Master_Sales_Product_Insurance_Question = "Pages.Master.ProductInsuranceQuestion";
        public const string Pages_Master_Sales_Product_Insurance_Question_Search = "Pages.Master.ProductInsuranceQuestion.Search";
        public const string Pages_Master_Sales_Product_Insurance_Question_Create = "Pages.Master.ProductInsuranceQuestion.Create";
        public const string Pages_Master_Sales_Product_Insurance_Question_Edit = "Pages.Master.ProductInsuranceQuestion.Edit";
        public const string Pages_Master_Sales_Product_Insurance_Question_Delete = "Pages.Master.ProductInsuranceQuestion.Delete";
        #endregion

        #region "Product Finance Type"
        public const string Pages_Master_Sales_ProductFinanceType = "Pages.Master.Sales.MstSleProductFinanceType";
        public const string Pages_Master_Sales_ProductFinanceType_Search = "Pages.Master.Sales.MstSleProductFinanceType.Search";
        public const string Pages_Master_Sales_ProductFinanceType_Create = "Pages.Master.Sales.MstSleProductFinanceType.Create";
        public const string Pages_Master_Sales_ProductFinanceType_Edit = "Pages.Master.Sales.MstSleProductFinanceType.Edit";
        public const string Pages_Master_Sales_ProductFinanceType_Delete = "Pages.Master.Sales.MstSleProductFinanceType.Delete";
        #endregion

        #region "Product Insurance Type"
        public const string Pages_Master_Sales_ProductInsuranceType = "Pages.Master.Sales.MstSleProductInsuranceType";
        public const string Pages_Master_Sales_ProductInsuranceType_Search = "Pages.Master.Sales.MstSleProductInsuranceType.Search";
        public const string Pages_Master_Sales_ProductInsuranceType_Create = "Pages.Master.Sales.MstSleProductInsuranceType.Create";
        public const string Pages_Master_Sales_ProductInsuranceType_Edit = "Pages.Master.Sales.MstSleProductInsuranceType.Edit";
        public const string Pages_Master_Sales_ProductInsuranceType_Delete = "Pages.Master.Sales.MstSleProductInsuranceType.Delete";
        #endregion

        #region "Product Finance Category"
        public const string Pages_Master_Sales_ProductFinanceCategory = "Pages.Master.Sales.MstSleProductFinanceCategory";
        public const string Pages_Master_Sales_ProductFinanceCategory_Search = "Pages.Master.Sales.MstSleProductFinanceCategory.Search";
        public const string Pages_Master_Sales_ProductFinanceCategory_Create = "Pages.Master.Sales.MstSleProductFinanceCategory.Create";
        public const string Pages_Master_Sales_ProductFinanceCategory_Edit = "Pages.Master.Sales.MstSleProductFinanceCategory.Edit";
        public const string Pages_Master_Sales_ProductFinanceCategory_Delete = "Pages.Master.Sales.MstSleProductFinanceCategory.Delete";
        #endregion

        #region "Product Insurance Category"
        public const string Pages_Master_Sales_ProductInsuranceCategory = "Pages.Master.Sales.MstSleProductInsuranceCategory";
        public const string Pages_Master_Sales_ProductInsuranceCategory_Search = "Pages.Master.Sales.MstSleProductInsuranceCategory.Search";
        public const string Pages_Master_Sales_ProductInsuranceCategory_Create = "Pages.Master.Sales.MstSleProductInsuranceCategory.Create";
        public const string Pages_Master_Sales_ProductInsuranceCategory_Edit = "Pages.Master.Sales.MstSleProductInsuranceCategory.Edit";
        public const string Pages_Master_Sales_ProductInsuranceCategory_Delete = "Pages.Master.Sales.MstSleProductInsuranceCategory.Delete";
        #endregion

        #region "Insurance Level"
        public const string Pages_Master_Sales_InsuranceLevel = "Pages.Master.Sales.MstSleInsuranceLevel";
        public const string Pages_Master_Sales_InsuranceLevel_Search = "Pages.Master.Sales.MstSleInsuranceLevel.Search";
        public const string Pages_Master_Sales_InsuranceLevel_Create = "Pages.Master.Sales.MstSleInsuranceLevel.Create";
        public const string Pages_Master_Sales_InsuranceLevel_Edit = "Pages.Master.Sales.MstSleInsuranceLevel.Edit";
        public const string Pages_Master_Sales_InsuranceLevel_Delete = "Pages.Master.Sales.MstSleInsuranceLevel.Delete";
        #endregion

        #region "Product Finance Answer"
        public const string Pages_Master_Sales_ProductFinanceAnswer = "Pages.Master.Sales.MstSleProductFinanceAnswer";
        public const string Pages_Master_Sales_ProductFinanceAnswer_Search = "Pages.Master.Sales.MstSleProductFinanceAnswer.Search";
        public const string Pages_Master_Sales_ProductFinanceAnswer_Create = "Pages.Master.Sales.MstSleProductFinanceAnswer.Create";
        public const string Pages_Master_Sales_ProductFinanceAnswer_Edit = "Pages.Master.Sales.MstSleProductFinanceAnswer.Edit";
        public const string Pages_Master_Sales_ProductFinanceAnswer_Delete = "Pages.Master.Sales.MstSleProductFinanceAnswer.Delete";
        #endregion

        #region "Product Insurance Answer"
        public const string Pages_Master_Sales_ProductInsuranceAnswer = "Pages.Master.Sales.MstSleProductInsuranceAnswer";
        public const string Pages_Master_Sales_ProductInsuranceAnswer_Search = "Pages.Master.Sales.MstSleProductInsuranceAnswer.Search";
        public const string Pages_Master_Sales_ProductInsuranceAnswer_Create = "Pages.Master.Sales.MstSleProductInsuranceAnswer.Create";
        public const string Pages_Master_Sales_ProductInsuranceAnswer_Edit = "Pages.Master.Sales.MstSleProductInsuranceAnswer.Edit";
        public const string Pages_Master_Sales_ProductInsuranceAnswer_Delete = "Pages.Master.Sales.MstSleProductInsuranceAnswer.Delete";
        #endregion

        #region "Product Finance Introduce"
        public const string Pages_Master_Sales_ProductFinanceIntroduce = "Pages.Master.Sales.MstSleProductFinanceIntroduce";
        public const string Pages_Master_Sales_ProductFinanceIntroduce_Search = "Pages.Master.Sales.MstSleProductFinanceIntroduce.Search";
        public const string Pages_Master_Sales_ProductFinanceIntroduce_Create = "Pages.Master.Sales.MstSleProductFinanceIntroduce.Create";
        public const string Pages_Master_Sales_ProductFinanceIntroduce_Edit = "Pages.Master.Sales.MstSleProductFinanceIntroduce.Edit";
        public const string Pages_Master_Sales_ProductFinanceIntroduce_Delete = "Pages.Master.Sales.MstSleProductFinanceIntroduce.Delete";
        #endregion

        #region "Product Insurance Introduce"
        public const string Pages_Master_Sales_ProductInsuranceIntroduce = "Pages.Master.Sales.MstSleProductInsuranceIntroduce";
        public const string Pages_Master_Sales_ProductInsuranceIntroduce_Search = "Pages.Master.Sales.MstSleProductInsuranceIntroduce.Search";
        public const string Pages_Master_Sales_ProductInsuranceIntroduce_Create = "Pages.Master.Sales.MstSleProductInsuranceIntroduce.Create";
        public const string Pages_Master_Sales_ProductInsuranceIntroduce_Edit = "Pages.Master.Sales.MstSleProductInsuranceIntroduce.Edit";
        public const string Pages_Master_Sales_ProductInsuranceIntroduce_Delete = "Pages.Master.Sales.MstSleProductInsuranceIntroduce.Delete";
        #endregion

        #region "Value Chain Source"
        public const string Pages_Master_Sales_ValueChainSource = "Pages.Master.Sales.ValueChainSource";
        public const string Pages_Master_Sales_ValueChainSource_Search = "Pages.Master.Sales.ValueChainSource.Search";
        public const string Pages_Master_Sales_ValueChainSource_Create = "Pages.Master.Sales.ValueChainSource.Create";
        public const string Pages_Master_Sales_ValueChainSource_Edit = "Pages.Master.Sales.ValueChainSource.Edit";
        public const string Pages_Master_Sales_ValueChainSource_Delete = "Pages.Master.Sales.ValueChainSource.Delete";
        #endregion

        #region "SalesContractCampaignByGrade"
        public const string Pages_Master_Sales_contractMaster = "Pages.Master.Sales.contractMaster";
        public const string Pages_Master_Sales_SalesContractCampaignByGrade = "Pages.Master.Sales.SalesContractCampaignByGrade";
        public const string Pages_Master_Sales_SalesContractCampaignByGrade_Search = "Pages.Master.Sales.SalesContractCampaignByGrade.Search";
        public const string Pages_Master_Sales_SalesContractCampaignByGrade_Create = "Pages.Master.Sales.SalesContractCampaignByGrade.Create";
        public const string Pages_Master_Sales_SalesContractCampaignByGrade_Edit = "Pages.Master.Sales.SalesContractCampaignByGrade.Edit";
        public const string Pages_Master_Sales_SalesContractCampaignByGrade_Delete = "Pages.Master.Sales.SalesContractCampaignByGrade.Delete";
        #endregion


        #endregion

        #region "Common Master"

        #region "Master Dealer"
        public const string Pages_Master_Common_MstGenDealers = "Pages.Master.Common.MstGenDealers";
        public const string Pages_Master_Sales_MstGenDealers_Search = "Pages.Master.Common.MstGenDealers.Search";
        public const string Pages_Master_Sales_MstGenDealers_Create = "Pages.Master.Common.MstGenDealers.Create";
        public const string Pages_Master_Sales_MstGenDealers_Edit = "Pages.Master.Common.MstGenDealers.Edit";
        public const string Pages_Master_Sales_MstGenDealers_Delete = "Pages.Master.Common.MstGenDealers.Delete";
        #endregion

        #region "Master District"
        public const string Pages_Master_Sales_District = "Pages.Master.Common.MstSleDistrict";
        public const string Pages_Master_Sales_District_Search = "Pages.Master.Common.MstSleDistrict.Search";
        public const string Pages_Master_Sales_District_Create = "Pages.Master.Common.MstSleDistrict.Create";
        public const string Pages_Master_Sales_District_Edit = "Pages.Master.Common.MstSleDistrict.Edit";
        public const string Pages_Master_Sales_District_Delete = "Pages.Master.Common.MstSleDistrict.Delete";
        #endregion

        #region "Master Province"
        public const string Pages_Master_Sales_MstGenProvinces = "Pages.Master.Sales.MstGenProvinces";
        public const string Pages_Master_Sales_MstGenProvinces_Search = "Pages.Master.Sales.MstGenProvinces.Search";
        public const string Pages_Master_Sales_MstGenProvinces_Create = "Pages.Master.Sales.MstGenProvinces.Create";
        public const string Pages_Master_Sales_MstGenProvinces_Edit = "Pages.Master.Sales.MstGenProvinces.Edit";
        public const string Pages_Master_Sales_MstGenProvinces_Delete = "Pages.Master.Sales.MstGenProvinces.Delete";
        #endregion

        #region "Master Group Dealer"
        public const string Pages_Master_Sales_MstSleDealerGroups = "Pages.Master.Sales.MstSleDealerGroups";
        public const string Pages_Master_Sales_MstSleDealerGroups_Search = "Pages.Master.Sales.MstSleDealerGroups.Search";
        public const string Pages_Master_Sales_MstSleDealerGroups_Create = "Pages.Master.Sales.MstSleDealerGroups.Create";
        public const string Pages_Master_Sales_MstSleDealerGroups_Edit = "Pages.Master.Sales.MstSleDealerGroups.Edit";
        public const string Pages_Master_Sales_MstSleDealerGroups_Delete = "Pages.Master.Sales.MstSleDealerGroups.Delete";

        #endregion

        #endregion

        #region "Sales Master"
        #region "Setup Parameter"
        public const string Pages_Setup_Parameter = "Pages.Setup.Parameter";

        #region "Target Setting"
        public const string Pages_Master_Sale_TargetSetting = "Pages.Master.Sale.TargetSetting";
        public const string Pages_Master_Sale_TargetSetting_Create = "Pages.Master.Sale.TargetSetting.Create";
        public const string Pages_Master_Sale_TargetSetting_Edit = "Pages.Master.Sale.TargetSetting.Edit";
        public const string Pages_Master_Sale_TargetSetting_Search = "Pages.Master.Sale.TargetSetting.Search";
        public const string Pages_Master_Sale_TargetSetting_Delete = "Pages.Master.Sale.TargetSetting.Delete";
        #endregion

        #region "Config"
        public const string Pages_Master_Sales_Config = "Pages.Master.Sales.Config";
        public const string Pages_Master_Sales_Config_Create = "Pages.Master.Sales.Config.Create";
        public const string Pages_Master_Sales_Config_Edit = "Pages.Master.Sales.Config.Edit";
        public const string Pages_Master_Sales_Config_Search = "Pages.Master.Sales.Config.Search";
        #endregion

        #region "Mobile Version"
        public const string Pages_Administration_Mobile_Version = "Pages.Administration.Mobile.Version";
        public const string Pages_Administration_Mobile_Version_Create = "Pages.Administration.Mobile.Version.Create";
        public const string Pages_Administration_Mobile_Version_Search = "Pages.Administration.Mobile.Version.Search";
        #endregion

        #region "Master Lookup"
        public const string Pages_Master_Sales_Lookup = "Pages.Master.Sales.MstSleLookup";
        public const string Pages_Master_Sales_Lookup_Create = "Pages.Master.Sales.MstSleLookup.Create";
        public const string Pages_Master_Sales_Lookup_Edit = "Pages.Master.Sales.MstSleLookup.Edit";
        public const string Pages_Master_Sales_Lookup_Delete = "Pages.Master.Sales.MstSleLookup.Delete";
        public const string Pages_Master_Sales_Lookup_Search = "Pages.Master.Sales.MstSleLookup.Search";
        #endregion

        #endregion

        #region "Master Supporter"
        public const string Pages_Master_Sales_MstSleSupporter = "Pages.Master.Sales.MstSleSupporter";
        public const string Pages_Master_Sales_MstSleSupporter_Search = "Pages.Master.Sales.MstSleSupporter.Search";
        public const string Pages_Master_Sales_MstSleSupporter_Create = "Pages.Master.Sales.MstSleSupporter.Create";
        public const string Pages_Master_Sales_MstSleSupporter_Edit = "Pages.Master.Sales.MstSleSupporter.Edit";
        public const string Pages_Master_Sales_MstSleSupporter_Delete = "Pages.Master.Sales.MstSleSupporter.Delete";
        #endregion

        #region "Master InsuranceDriverPassengerFee"
        public const string Pages_Master_Sales_MstSleInsuranceDriverPassengerFee = "Pages.Master.Sales.MstSleInsuranceDriverPassengerFee";
        public const string Pages_Master_Sales_MstSleInsuranceDriverPassengerFee_Search = "Pages.Master.Sales.MstSleInsuranceDriverPassengerFee.Search";
        public const string Pages_Master_Sales_MstSleInsuranceDriverPassengerFee_Edit = "Pages.Master.Sales.MstSleInsuranceDriverPassengerFee.Edit";
        #endregion
        #region "Master Test Drive Place"
        public const string Pages_Master_Sales_MstSleTestDrivePlace = "Pages.Master.Sales.MstSleTestDrivePlace";
        public const string Pages_Master_Sales_MstSleTestDrivePlace_Search = "Pages.Master.Sales.MstSleTestDrivePlace.Search";
        public const string Pages_Master_Sales_MstSleTestDrivePlace_Create = "Pages.Master.Sales.MstSleTestDrivePlace.Create";
        public const string Pages_Master_Sales_MstSleTestDrivePlace_Edit = "Pages.Master.Sales.MstSleTestDrivePlace.Edit";
        public const string Pages_Master_Sales_MstSleTestDrivePlace_Delete = "Pages.Master.Sales.MstSleTestDrivePlace.Delete";

        #endregion

        #region "Delivery Place"
        public const string Pages_Master_Sales_DeliveryPlace = "Pages.Master.Sales.MstSleDeliveryPlace";
        public const string Pages_Master_Sales_DeliveryPlace_Create = "Pages.Master.Sales.MstSleDeliveryPlace.Create";
        public const string Pages_Master_Sales_DeliveryPlace_Edit = "Pages.Master.Sales.MstSleDeliveryPlace.Edit";
        public const string Pages_Master_Sales_DeliveryPlace_Delete = "Pages.Master.Sales.MstSleDeliveryPlace.Delete";
        public const string Pages_Master_Sales_DeliveryPlace_Search = "Pages.Master.Sales.MstSleDeliveryPlace.Search";
        #endregion

        #region "Delivery Stall"
        public const string Pages_Master_Sales_DeliveryStall = "Pages.Master.Sales.MstSleDeliveryStall";
        public const string Pages_Master_Sales_DeliveryStall_Create = "Pages.Master.Sales.MstSleDeliveryStall.Create";
        public const string Pages_Master_Sales_DeliveryStall_Edit = "Pages.Master.Sales.MstSleDeliveryStall.Edit";
        public const string Pages_Master_Sales_DeliveryStall_Delete = "Pages.Master.Sales.MstSleDeliveryStall.Delete";
        public const string Pages_Master_Sales_DeliveryStall_Search = "Pages.Master.Sales.MstSleDeliveryStall.Search";
        #endregion

        #region "Company"
        public const string Pages_Master_Sales_Company = "Pages.Master.Sales.MstSleCompany";
        public const string Pages_Master_Sales_Company_Create = "Pages.Master.Sales.MstSleCompany.Create";
        public const string Pages_Master_Sales_Company_Edit = "Pages.Master.Sales.MstSleCompany.Edit";
        public const string Pages_Master_Sales_Company_Delete = "Pages.Master.Sales.MstSleCompany.Delete";
        public const string Pages_Master_Sales_Company_Search = "Pages.Master.Sales.MstSleCompany.Search";
        #endregion

        #region "Purpose"
        public const string Pages_Master_Sales_Purpose = "Pages.Master.Sales.MstSlePurpose";
        public const string Pages_Master_Sales_Purpose_Create = "Pages.Master.Sales.MstSlePurpose.Create";
        public const string Pages_Master_Sales_Purpose_Edit = "Pages.Master.Sales.MstSlePurpose.Edit";
        public const string Pages_Master_Sales_Purpose_Delete = "Pages.Master.Sales.MstSlePurpose.Delete";
        public const string Pages_Master_Sales_Purpose_Search = "Pages.Master.Sales.MstSlePurpose.Search";
        #endregion

        #region "FAR"
        public const string Pages_Master_Sales_FAR = "Pages.Master.Sales.MstSleFAR";
        public const string Pages_Master_Sales_FAR_Create = "Pages.Master.Sales.MstSleFAR.Create";
        public const string Pages_Master_Sales_FAR_Edit = "Pages.Master.Sales.MstSleFAR.Edit";
        public const string Pages_Master_Sales_FAR_Delete = "Pages.Master.Sales.MstSleFAR.Delete";
        public const string Pages_Master_Sales_FAR_Search = "Pages.Master.Sales.MstSleFAR.Search";
        #endregion

        #region "ReasonOfChange"
        public const string Pages_Master_Sales_ReasonOfChange = "Pages.Master.Sales.MstSleReasonOfChange";
        public const string Pages_Master_Sales_ReasonOfChange_Create = "Pages.Master.Sales.MstSleReasonOfChange.Create";
        public const string Pages_Master_Sales_ReasonOfChange_Edit = "Pages.Master.Sales.MstSleReasonOfChange.Edit";
        public const string Pages_Master_Sales_ReasonOfChange_Delete = "Pages.Master.Sales.MstSleReasonOfChange.Delete";
        public const string Pages_Master_Sales_ReasonOfChange_Search = "Pages.Master.Sales.MstSleReasonOfChange.Search";
        #endregion

        #region "ReasonOfFreeze"
        public const string Pages_Master_Sales_ReasonOfFreeze = "Pages.Master.Sales.MstSleReasonOfFreeze";
        public const string Pages_Master_Sales_ReasonOfFreeze_Create = "Pages.Master.Sales.MstSleReasonOfFreeze.Create";
        public const string Pages_Master_Sales_ReasonOfFreeze_Edit = "Pages.Master.Sales.MstSleReasonOfFreeze.Edit";
        public const string Pages_Master_Sales_ReasonOfFreeze_Delete = "Pages.Master.Sales.MstSleReasonOfFreeze.Delete";
        public const string Pages_Master_Sales_ReasonOfFreeze_Search = "Pages.Master.Sales.MstSleReasonOfFreeze.Search";
        #endregion

        #region "SalesStage"
        public const string Pages_Master_Sales_SalesStage = "Pages.Master.Sales.MstSleSalesStage";
        public const string Pages_Master_Sales_SalesStage_Create = "Pages.Master.Sales.MstSleSalesStage.Create";
        public const string Pages_Master_Sales_SalesStage_Edit = "Pages.Master.Sales.MstSleSalesStage.Edit";
        public const string Pages_Master_Sales_SalesStage_Delete = "Pages.Master.Sales.MstSleSalesStage.Delete";
        public const string Pages_Master_Sales_SalesStage_Search = "Pages.Master.Sales.MstSleSalesStage.Search";
        #endregion

        #region "ReasonOfLost"
        public const string Pages_Master_Sales_ReasonOfLost = "Pages.Master.Sales.MstSleReasonOfLost";
        public const string Pages_Master_Sales_ReasonOfLost_Create = "Pages.Master.Sales.MstSleReasonOfLost.Create";
        public const string Pages_Master_Sales_ReasonOfLost_Edit = "Pages.Master.Sales.MstSleReasonOfLost.Edit";
        public const string Pages_Master_Sales_ReasonOfLost_Delete = "Pages.Master.Sales.MstSleReasonOfLost.Delete";
        public const string Pages_Master_Sales_ReasonOfLost_Search = "Pages.Master.Sales.MstSleReasonOfLost.Search";
        #endregion

        #region "Delivery Venue"
        public const string Pages_Master_Sales_DeliveryVenue = "Pages.Master.Sales.MstSleDeliveryVenue";
        public const string Pages_Master_Sales_DeliveryVenue_Create = "Pages.Master.Sales.MstSleDeliveryVenue.Create";
        public const string Pages_Master_Sales_DeliveryVenue_Edit = "Pages.Master.Sales.MstSleDeliveryVenue.Edit";
        public const string Pages_Master_Sales_DeliveryVenue_Delete = "Pages.Master.Sales.MstSleDeliveryVenue.Delete";
        public const string Pages_Master_Sales_DeliveryVenue_Search = "Pages.Master.Sales.MstSleDeliveryVenue.Search";
        #endregion
        #region "Source"
        public const string Pages_Master_Sales_Source = "Pages.Master.Sales.MstSleSource";
        public const string Pages_Master_Sales_Source_Create = "Pages.Master.Sales.MstSleSource.Create";
        public const string Pages_Master_Sales_Source_Edit = "Pages.Master.Sales.MstSleSource.Edit";
        public const string Pages_Master_Sales_Source_Delete = "Pages.Master.Sales.MstSleSource.Delete";
        public const string Pages_Master_Sales_Source_Search = "Pages.Master.Sales.MstSleSource.Search";
        #endregion
        #region "ConfigNumericalNo"
        public const string Pages_Master_Config_Numberical_No = "Pages.Master.Sales.MstConfigNumericalNo";
        public const string Pages_Master_Config_Numberical_No_Create = "Pages.Master.Sales.MstConfigNumericalNo.Create";
        public const string Pages_Master_Config_Numberical_No_Edit = "Pages.Master.Sales.MstConfigNumericalNo.Edit";
        public const string Pages_Master_Config_Numberical_No_Delete = "Pages.Master.Sales.MstConfigNumericalNo.Delete";
        public const string Pages_Master_Config_Numberical_No_Search = "Pages.Master.Sales.MstConfigNumericalNo.Search";
        #endregion
        #region "ColorByGrade"
        public const string Pages_Master_Sales_Color_By_Grade = "Pages.Master.Sales.MstSleColorByGrade";
        public const string Pages_Master_Sales_Color_By_Grade_Delete = "Pages.Master.Sales.MstSleColorByGrade.Delete";
        public const string Pages_Master_Sales_Color_By_Grade_Create = "Pages.Master.Sales.MstSleColorByGrade.Create";
        public const string Pages_Master_Sales_Color_By_Grade_Edit = "Pages.Master.Sales.MstSleColorByGrade.Edit";
        public const string Pages_Master_Sales_Color_By_Grade_Search = "Pages.Master.Sales.MstSleColorByGrade.Search";
        #endregion
        #region "Make Competitor"
        public const string Pages_Master_Sales_Make_Competitor = "Pages.Master.Sales.MstSleMakeCompetitor";
        public const string Pages_Master_Sales_Make_Competitor_Create = "Pages.Master.Sales.MstSleMakeCompetitor.Create";
        public const string Pages_Master_Sales_Make_Competitor_Edit = "Pages.Master.Sales.MstSleMakeCompetitor.Edit";
        public const string Pages_Master_Sales_Make_Competitor_Delete = "Pages.Master.Sales.MstSleMakeCompetitor.Delete";
        public const string Pages_Master_Sales_Make_Competitor_Search = "Pages.Master.Sales.MstSleMakeCompetitor.Search";
        #endregion
        #region "Colors"
        public const string Pages_Master_Sales_MstSleColors = "Pages.Master.Sales.MstSleColor";
        public const string Pages_Master_Sales_MstSleColors_Create = "Pages.Master.Sales.MstSleColor.Create";
        public const string Pages_Master_Sales_MstSleColors_Edit = "Pages.Master.Sales.MstSleColor.Edit";
        public const string Pages_Master_Sales_MstSleColors_Delete = "Pages.Master.Sales.MstSleColor.Delete";
        public const string Pages_Master_Sales_MstSleColors_Search = "Pages.Master.Sales.MstSleColor.Search";
        #endregion
        #region "Role"
        public const string Pages_Master_Sales_Role = "Pages.Master.Sales.MstSleRole";
        public const string Pages_Master_Sales_Role_Create = "Pages.Master.Sales.MstSleRole.Create";
        public const string Pages_Master_Sales_Role_Edit = "Pages.Master.Sales.MstSleRole.Edit";
        public const string Pages_Master_Sales_Role_Delete = "Pages.Master.Sales.MstSleRole.Delete";
        public const string Pages_Master_Sales_Role_Search = "Pages.Master.Sales.MstSleRole.Search";
        #endregion
        #region "Business Type"
        public const string Pages_Master_Sales_BusinessType = "Pages.Master.Sales.MstSleBusinessType";
        public const string Pages_Master_Sales_BusinessType_Create = "Pages.Master.Sales.MstSleBusinessType.Create";
        public const string Pages_Master_Sales_BusinessType_Edit = "Pages.Master.Sales.MstSleBusinessType.Edit";
        public const string Pages_Master_Sales_BusinessType_Delete = "Pages.Master.Sales.MstSleBusinessType.Delete";
        public const string Pages_Master_Sales_BusinessType_Search = "Pages.Master.Sales.MstSleBusinessType.Search";
        #endregion
        #region "Test Drive Stall"
        public const string Pages_Master_Sales_TestDriveStall = "Pages.Master.Sales.MstSleTestDriveStall";
        public const string Pages_Master_Sales_TestDriveStall_Search = "Pages.Master.Sales.MstSleTestDriveStall.Search";
        public const string Pages_Master_Sales_TestDriveStall_Create = "Pages.Master.Sales.MstSleTestDriveStall.Create";
        public const string Pages_Master_Sales_TestDriveStall_Edit = "Pages.Master.Sales.MstSleTestDriveStall.Edit";
        public const string Pages_Master_Sales_TestDriveStall_Delete = "Pages.Master.Sales.MstSleTestDriveStall.Delete";
        #endregion
        #region "Test Drive Vehicle"
        public const string Pages_Master_Sales_TestDriveVehicle = "Pages.Master.Sales.MstSleTestDriveVehicle";
        public const string Pages_Master_Sales_TestDriveVehicle_Search = "Pages.Master.Sales.MstSleTestDriveVehicle.Search";
        public const string Pages_Master_Sales_TestDriveVehicle_Create = "Pages.Master.Sales.MstSleTestDriveVehicle.Create";
        public const string Pages_Master_Sales_TestDriveVehicle_Edit = "Pages.Master.Sales.MstSleTestDriveVehicle.Edit";
        public const string Pages_Master_Sales_TestDriveVehicle_Delete = "Pages.Master.Sales.MstSleTestDriveVehicle.Delete";
        #endregion
        #region "Test Drive Route"
        public const string Pages_Master_Sales_TestDriveRoute = "Pages.Master.Sales.MstSleTestDriveRoute";
        public const string Pages_Master_Sales_TestDriveRoute_Search = "Pages.Master.Sales.MstSleTestDriveRoute.Search";
        public const string Pages_Master_Sales_TestDriveRoute_Create = "Pages.Master.Sales.MstSleTestDriveRoute.Create";
        public const string Pages_Master_Sales_TestDriveRoute_Edit = "Pages.Master.Sales.MstSleTestDriveRoute.Edit";
        public const string Pages_Master_Sales_TestDriveRoute_Delete = "Pages.Master.Sales.MstSleTestDriveRoute.Delete";
        #endregion
        #region "Excepted Delivery Timing"
        public const string Pages_Master_Sales_ExceptedDeliveryTiming = "Pages.Master.Sales.MstSleExpectedDelTiming";
        public const string Pages_Master_Sales_ExceptedDeliveryTiming_Search = "Pages.Master.Sales.MstSleExpectedDelTiming.Search";
        public const string Pages_Master_Sales_ExceptedDeliveryTiming_Create = "Pages.Master.Sales.MstSleExpectedDelTiming.Create";
        public const string Pages_Master_Sales_ExceptedDeliveryTiming_Edit = "Pages.Master.Sales.MstSleExpectedDelTiming.Edit";
        public const string Pages_Master_Sales_ExceptedDeliveryTiming_Delete = "Pages.Master.Sales.MstSleExpectedDelTiming.Delete";
        #endregion
        #region "Hotness"
        public const string Pages_Master_Sales_Hotness = "Pages.Master.Sales.MstSleHotness";
        public const string Pages_Master_Sales_Hotness_Search = "Pages.Master.Sales.MstSleHotness.Search";
        public const string Pages_Master_Sales_Hotness_Create = "Pages.Master.Sales.MstSleHotness.Create";
        public const string Pages_Master_Sales_Hotness_Edit = "Pages.Master.Sales.MstSleHotness.Edit";
        public const string Pages_Master_Sales_Hotness_Delete = "Pages.Master.Sales.MstSleHotness.Delete";
        #endregion

        #region "Payment Type"
        public const string Pages_Master_Sales_PaymentType = "Pages.Master.Sales.MstSlePaymentType";
        public const string Pages_Master_Sales_PaymentType_Search = "Pages.Master.Sales.MstSlePaymentType.Search";
        public const string Pages_Master_Sales_PaymentType_Create = "Pages.Master.Sales.MstSlePaymentType.Create";
        public const string Pages_Master_Sales_PaymentType_Edit = "Pages.Master.Sales.MstSlePaymentType.Edit";
        public const string Pages_Master_Sales_PaymentType_Delete = "Pages.Master.Sales.MstSlePaymentType.Delete";
        #endregion
        #region "Occupation"
        public const string Pages_Master_Sales_Occupation = "Pages.Master.Sales.MstSleOccupation";
        public const string Pages_Master_Sales_Occupation_Search = "Pages.Master.Sales.MstSleOccupation.Search";
        public const string Pages_Master_Sales_Occupation_Create = "Pages.Master.Sales.MstSleOccupation.Create";
        public const string Pages_Master_Sales_Occupation_Edit = "Pages.Master.Sales.MstSleOccupation.Edit";
        public const string Pages_Master_Sales_Occupation_Delete = "Pages.Master.Sales.MstSleOccupation.Delete";
        #endregion
        #region "Hobby"
        public const string Pages_Master_Sales_Hobby = "Pages.Master.Sales.MstSleHobby";
        public const string Pages_Master_Sales_Hobby_Search = "Pages.Master.Sales.MstSleHobby.Search";
        public const string Pages_Master_Sales_Hobby_Create = "Pages.Master.Sales.MstSleHobby.Create";
        public const string Pages_Master_Sales_Hobby_Edit = "Pages.Master.Sales.MstSleHobby.Edit";
        public const string Pages_Master_Sales_Hobby_Delete = "Pages.Master.Sales.MstSleHobby.Delete";
        #endregion
        #region "Source Of Info"
        public const string Pages_Master_Sales_SourceOfInfo = "Pages.Master.Sales.MstSleSourceOfInfo";
        public const string Pages_Master_Sales_SourceOfInfo_Search = "Pages.Master.Sales.MstSleSourceOfInfo.Search";
        public const string Pages_Master_Sales_SourceOfInfo_Create = "Pages.Master.Sales.MstSleSourceOfInfo.Create";
        public const string Pages_Master_Sales_SourceOfInfo_Edit = "Pages.Master.Sales.MstSleSourceOfInfo.Edit";
        public const string Pages_Master_Sales_SourceOfInfo_Delete = "Pages.Master.Sales.MstSleSourceOfInfo.Delete";
        #endregion
        #region "Social Channel"
        public const string Pages_Master_Sales_SocialChannel = "Pages.Master.Sales.MstSleSocialChannel";
        public const string Pages_Master_Sales_SocialChannel_Search = "Pages.Master.Sales.MstSleSocialChannel.Search";
        public const string Pages_Master_Sales_SocialChannel_Create = "Pages.Master.Sales.MstSleSocialChannel.Create";
        public const string Pages_Master_Sales_SocialChannel_Edit = "Pages.Master.Sales.MstSleSocialChannel.Edit";
        public const string Pages_Master_Sales_SocialChannel_Delete = "Pages.Master.Sales.MstSleSocialChannel.Delete";
        #endregion

        #region "Customer Social Channel"
        public const string Pages_Master_CustomerSocialChannel = "Pages.Master.Sales.MstSleCustomerSocialChannel";
        public const string Pages_Master_CustomerSocialChannel_Search = "Pages.Master.Sales.MstSleCustomerSocialChannel.Search";
        public const string Pages_Master_CustomerSocialChannel_Create = "Pages.Master.Sales.MstSleCustomerSocialChannel.Create";
        public const string Pages_Master_CustomerSocialChannel_Edit = "Pages.Master.Sales.MstSleCustomerSocialChannel.Edit";
        public const string Pages_Master_CustomerSocialChannel_Delete = "Pages.Master.Sales.MstSleCustomerSocialChannel.Delete";
        #endregion

        #region "Finance Instalment Plan"
        public const string Pages_Master_Sales_FinanceInstalmentPlan = "Pages.Master.Sales.MstSleFinanceInstalmentPlan";
        public const string Pages_Master_Sales_FinanceInstalmentPlan_Search = "Pages.Master.Sales.MstSleFinanceInstalmentPlan.Search";
        public const string Pages_Master_Sales_FinanceInstalmentPlan_Create = "Pages.Master.Sales.MstSleFinanceInstalmentPlan.Create";
        public const string Pages_Master_Sales_FinanceInstalmentPlan_Edit = "Pages.Master.Sales.MstSleFinanceInstalmentPlan.Edit";
        public const string Pages_Master_Sales_FinanceInstalmentPlan_Delete = "Pages.Master.Sales.MstSleFinanceInstalmentPlan.Delete";
        public const string Pages_Master_Sales_CustomApi = "Pages.Master.Sales.CustomApi";
        #endregion
        #region "Finance TFS Package"
        public const string Pages_Master_Sales_FinanceTFSPackage = "Pages.Master.Sales.MstSleFinanceTFSPackage";
        public const string Pages_Master_Sales_FinanceTFSPackage_Search = "Pages.Master.Sales.MstSleFinanceTFSPackage.Search";
        public const string Pages_Master_Sales_FinanceTFSPackage_Create = "Pages.Master.Sales.MstSleFinanceTFSPackage.Create";
        public const string Pages_Master_Sales_FinanceTFSPackage_Edit = "Pages.Master.Sales.MstSleFinanceTFSPackage.Edit";
        public const string Pages_Master_Sales_FinanceTFSPackage_Delete = "Pages.Master.Sales.MstSleFinanceTFSPackage.Delete";

        #endregion
        #region "Finance Loan Value"
        public const string Pages_Master_Sales_FinanceLoanValue = "Pages.Master.Sales.MstSleFinanceLoanValue";
        public const string Pages_Master_Sales_FinanceLoanValue_Search = "Pages.Master.Sales.MstSleFinanceLoanValue.Search";
        public const string Pages_Master_Sales_FinanceLoanValue_Create = "Pages.Master.Sales.MstSleFinanceLoanValue.Create";
        public const string Pages_Master_Sales_FinanceLoanValue_Edit = "Pages.Master.Sales.MstSleFinanceLoanValue.Edit";
        public const string Pages_Master_Sales_FinanceLoanValue_Delete = "Pages.Master.Sales.MstSleFinanceLoanValue.Delete";

        #endregion
        #region "Finance Interest Rate"
        public const string Pages_Master_Sales_FinanceInterestRate = "Pages.Master.Sales.MstSleFinanceInterestRate";
        public const string Pages_Master_Sales_FinanceInterestRate_Search = "Pages.Master.Sales.MstSleFinanceInterestRate.Search";
        public const string Pages_Master_Sales_FinanceInterestRate_Create = "Pages.Master.Sales.MstSleFinanceInterestRate.Create";
        public const string Pages_Master_Sales_FinanceInterestRate_Edit = "Pages.Master.Sales.MstSleFinanceInterestRate.Edit";
        public const string Pages_Master_Sales_FinanceInterestRate_Delete = "Pages.Master.Sales.MstSleFinanceInterestRate.Delete";

        #endregion
        #region "Finance Params By Model"
        public const string Pages_Master_Sales_FinanceParamByModel = "Pages.Master.Sales.MstSleFinanceParamByModel";
        public const string Pages_Master_Sales_FinanceParamByModel_Search = "Pages.Master.Sales.MstSleFinanceParamByModel.Search";
        public const string Pages_Master_Sales_FinanceParamByModel_Create = "Pages.Master.Sales.MstSleFinanceParamByModel.Create";
        public const string Pages_Master_Sales_FinanceParamByModel_Edit = "Pages.Master.Sales.MstSleFinanceParamByModel.Edit";
        public const string Pages_Master_Sales_FinanceParamByModel_Delete = "Pages.Master.Sales.MstSleFinanceParamByModel.Delete";
        #endregion
        #region "Education"
        public const string Pages_Master_Sales_Education = "Pages.Master.Sales.MstSleEducation";
        public const string Pages_Master_Sales_Education_Search = "Pages.Master.Sales.MstSleEducation.Search";
        public const string Pages_Master_Sales_Education_Create = "Pages.Master.Sales.MstSleEducation.Create";
        public const string Pages_Master_Sales_Education_Edit = "Pages.Master.Sales.MstSleEducation.Edit";
        public const string Pages_Master_Sales_Education_Delete = "Pages.Master.Sales.MstSleEducation.Delete";
        #endregion
        #region "Jobs"
        public const string Pages_Master_Sales_Jobs = "Pages.Master.Sales.MstSleJobs";
        public const string Pages_Master_Sales_Jobs_Search = "Pages.Master.Sales.MstSleJobs.Search";
        public const string Pages_Master_Sales_Jobs_Create = "Pages.Master.Sales.MstSleJobs.Create";
        public const string Pages_Master_Sales_Jobs_Edit = "Pages.Master.Sales.MstSleJobs.Edit";
        public const string Pages_Master_Sales_Jobs_Delete = "Pages.Master.Sales.MstSleJobs.Delete";
        #endregion
        #region "Job Title"
        public const string Pages_Master_Sales_JobTitle = "Pages.Master.Sales.MstSleJobTitle";
        public const string Pages_Master_Sales_JobTitle_Search = "Pages.Master.Sales.MstSleJobTitle.Search";
        public const string Pages_Master_Sales_JobTitle_Create = "Pages.Master.Sales.MstSleJobTitle.Create";
        public const string Pages_Master_Sales_JobTitle_Edit = "Pages.Master.Sales.MstSleJobTitle.Edit";
        public const string Pages_Master_Sales_JobTitle_Delete = "Pages.Master.Sales.MstSleJobTitle.Delete";
        #endregion
        #region "Dealer Organization Chart"
        public const string Pages_Master_Sales_DealerOrganizationChart = "Pages.Master.Sales.MstDealerOrganizationChart";
        public const string Pages_Master_Sales_DealerOrganizationChart_Search = "Pages.Master.Sales.MstDealerOrganizationChart.Search";
        #endregion
        #region "Vehicle Product Image"
        public const string Pages_Master_Sales_VehicleProductImage = "Pages.Master.Sales.MstSleVehicleProductImage";
        public const string Pages_Master_Sales_VehicleProductImage_Search = "Pages.Master.Sales.MstSleVehicleProductImage.Search";
        public const string Pages_Master_Sales_VehicleProductImage_Create = "Pages.Master.Sales.MstSleVehicleProductImage.Create";
        public const string Pages_Master_Sales_VehicleProductImage_Edit = "Pages.Master.Sales.MstSleVehicleProductImage.Edit";
        public const string Pages_Master_Sales_VehicleProductImage_Delete = "Pages.Master.Sales.MstSleVehicleProductImage.Delete";
        #endregion
        #region "Vehicle Product accessory genuine"
        public const string Pages_Master_Sales_AccessoryGenuine = "Pages.Master.Sales.AccessoryGenuine";
        public const string Pages_Master_Sales_AccessoryGenuine_Search = "Pages.Master.Sales.AccessoryGenuine.Search";
        public const string Pages_Master_Sales_AccessoryGenuine_Edit = "Pages.Master.Sales.AccessoryGenuine.Edit";
        #endregion
        #region "Vehicle Product maintenance detail"
        public const string Pages_Master_Sales_MaintenanceDetail = "Pages.Master.Sales.MaintenanceDetail";
        public const string Pages_Master_Sales_MaintenanceDetail_Search = "Pages.Master.Sales.MaintenanceDetail.Search";
        public const string Pages_Master_Sales_MaintenanceDetail_Create = "Pages.Master.Sales.MaintenanceDetail.Create";
        public const string Pages_Master_Sales_MaintenanceDetail_Delete = "Pages.Master.Sales.MaintenanceDetail.Delete";
        public const string Pages_Master_Sales_MaintenanceDetail_Edit = "Pages.Master.Sales.MaintenanceDetail.Edit";
        #endregion

        #region "Vehicle Product BeatyCareServices "
        public const string Pages_Master_Sales_BeatyCareServices = "Pages.Master.Sales.BeatyCareServices";
        public const string Pages_Master_Sales_BeatyCareServices_Search = "Pages.Master.Sales.BeatyCareServices.Search";
        public const string Pages_Master_Sales_BeatyCareServices_Create = "Pages.Master.Sales.BeatyCareServices.Create";
        public const string Pages_Master_Sales_BeatyCareServices_Delete = "Pages.Master.Sales.BeatyCareServices.Delete";
        public const string Pages_Master_Sales_BeatyCareServices_Edit = "Pages.Master.Sales.BeatyCareServices.Edit";
        #endregion 
        #region "Vehicle Product TechnicalSpecInternalColor "
        public const string Pages_Master_Sales_TechnicalSpecInternalColor = "Pages.Master.Sales.TechnicalSpecInternalColor";
        public const string Pages_Master_Sales_TechnicalSpecInternalColor_Search = "Pages.Master.Sales.TechnicalSpecInternalColor.Search";
        public const string Pages_Master_Sales_TechnicalSpecInternalColor_Create = "Pages.Master.Sales.TechnicalSpecInternalColor.Create";
        public const string Pages_Master_Sales_TechnicalSpecInternalColor_Delete = "Pages.Master.Sales.TechnicalSpecInternalColor.Delete";
        public const string Pages_Master_Sales_TechnicalSpecInternalColor_Edit = "Pages.Master.Sales.TechnicalSpecInternalColor.Edit";
        #endregion

        #region "Vehicle Product Recruitment"
        public const string Pages_Master_Sales_Recruitment = "Pages.Master.Recruitment";
        #region "danh sách ứng viên"
        public const string Pages_Master_Sales_Vehicle_Product_Recruitment = "Pages.Master.Sales.VehicleProductRecruitment";
        public const string Pages_Master_Sales_Vehicle_Product_Recruitment_Search = "Pages.Master.Sales.VehicleProductRecruitment.Search";
        #endregion

        #region "Vehicle Product Recruitment Software"
        public const string Pages_Master_Sales_RecruitmentSoftware = "Pages.Master.Sales.Software";
        public const string Pages_Master_Sales_RecruitmentSoftware_Search = "Pages.Master.Sales.Software.Search";
        public const string Pages_Master_Sales_RecruitmentSoftware_Create = "Pages.Master.Sales.Software.Create";
        public const string Pages_Master_Sales_RecruitmentSoftware_Edit = "Pages.Master.Sales.Software.Edit";
        public const string Pages_Master_Sales_RecruitmentSoftware_Delete = "Pages.Master.Sales.Software.Delete";
        #endregion
        #region "Vehicle Product Skill Level"
        public const string Pages_Master_Sales_SkillLevel = "Pages.Master.Sales.SkillLevel";
        public const string Pages_Master_Sales_SkillLevel_Search = "Pages.Master.Sales.SkillLevel.Search";
        public const string Pages_Master_Sales_SkillLevel_Create = "Pages.Master.Sales.SkillLevel.Create";
        public const string Pages_Master_Sales_SkillLevel_Edit = "Pages.Master.Sales.SkillLevel.Edit";
        public const string Pages_Master_Sales_SkillLevel_Delete = "Pages.Master.Sales.SkillLevel.Delete";
        #endregion
        #region "Vehicle Product Applied Position"
        public const string Pages_Master_Sales_VehicleProductAppliedPosition = "Pages.Master.Sales.VehicleProductAppliedPosition";
        public const string Pages_Master_Sales_VehicleProductAppliedPosition_Search = "Pages.Master.Sales.VehicleProductAppliedPosition.Search";
        public const string Pages_Master_Sales_VehicleProductAppliedPosition_Create = "Pages.Master.Sales.VehicleProductAppliedPosition.Create";
        public const string Pages_Master_Sales_VehicleProductAppliedPosition_Edit = "Pages.Master.Sales.VehicleProductAppliedPosition.Edit";
        public const string Pages_Master_Sales_VehicleProductAppliedPosition_Delete = "Pages.Master.Sales.VehicleProductAppliedPosition.Delete";
        #endregion
        #region "Vehicle Product Education Level"
        public const string Pages_Master_Sales_EducationLevel = "Pages.Master.Sales.EducationLevel";
        public const string Pages_Master_Sales_EducationLevel_Search = "Pages.Master.Sales.EducationLevel.Search";
        public const string Pages_Master_Sales_EducationLevel_Create = "Pages.Master.Sales.EducationLevel.Create";
        public const string Pages_Master_Sales_EducationLevel_Edit = "Pages.Master.Sales.EducationLevel.Edit";
        public const string Pages_Master_Sales_EducationLevel_Delete = "Pages.Master.Sales.EducationLevel.Delete";
        #endregion

        #region "Vehicle Product Recruitment Location"
        public const string Pages_Master_Sales_RecruitmentLocation = "Pages.Master.Sales.RecruitmentLocation";
        public const string Pages_Master_Sales_RecruitmentLocation_Search = "Pages.Master.Sales.RecruitmentLocation.Search";
        public const string Pages_Master_Sales_RecruitmentLocation_Create = "Pages.Master.Sales.RecruitmentLocation.Create";
        public const string Pages_Master_Sales_RecruitmentLocation_Edit = "Pages.Master.Sales.RecruitmentLocation.Edit";
        public const string Pages_Master_Sales_RecruitmentLocation_Delete = "Pages.Master.Sales.RecruitmentLocation.Delete";
        #endregion
        #endregion
        #region "Vehicle Product Category"
        public const string Pages_Master_Sales_VehicleProductCategory = "Pages.Master.Sales.MstSleVehicleProductCategory";
        public const string Pages_Master_Sales_VehicleProductCategory_Search = "Pages.Master.Sales.MstSleVehicleProductCategory.Search";
        public const string Pages_Master_Sales_VehicleProductCategory_Create = "Pages.Master.Sales.MstSleVehicleProductCategory.Create";
        public const string Pages_Master_Sales_VehicleProductCategory_Edit = "Pages.Master.Sales.MstSleVehicleProductCategory.Edit";
        public const string Pages_Master_Sales_VehicleProductCategory_Delete = "Pages.Master.Sales.MstSleVehicleProductCategory.Delete";
        #endregion
        #region "Vehicle Product SaleTalk"
        public const string Pages_Master_Sales_VehicleProductSaleTalk = "Pages.Master.Sales.MstSleVehicleProductSaleTalk";
        public const string Pages_Master_Sales_VehicleProductSaleTalk_Search = "Pages.Master.Sales.MstSleVehicleProductSaleTalk.Search";
        public const string Pages_Master_Sales_VehicleProductSaleTalk_Create = "Pages.Master.Sales.MstSleVehicleProductSaleTalk.Create";
        public const string Pages_Master_Sales_VehicleProductSaleTalk_Edit = "Pages.Master.Sales.MstSleVehicleProductSaleTalk.Edit";
        public const string Pages_Master_Sales_VehicleProductSaleTalk_Delete = "Pages.Master.Sales.MstSleVehicleProductSaleTalk.Delete";
        #endregion
        #region "Vehicle Product Document"
        public const string Pages_Master_Sales_VehicleProductDocument = "Pages.Master.Sales.MstSleVehicleProductDocument";
        public const string Pages_Master_Sales_VehicleProductDocument_Search = "Pages.Master.Sales.MstSleVehicleProductDocument.Search";
        public const string Pages_Master_Sales_VehicleProductDocument_Create = "Pages.Master.Sales.MstSleVehicleProductDocument.Create";
        public const string Pages_Master_Sales_VehicleProductDocument_Edit = "Pages.Master.Sales.MstSleVehicleProductDocument.Edit";
        public const string Pages_Master_Sales_VehicleProductDocument_Delete = "Pages.Master.Sales.MstSleVehicleProductDocument.Delete";
        #endregion

        #region "Product Car Body Style"
        public const string Pages_Master_Sales_ProductCarBodyStyle = "Pages.Master.Sales.MstSleProductCarBodyStyle";
        public const string Pages_Master_Sales_ProductCarBodyStyle_Search = "Pages.Master.Sales.MstSleProductCarBodyStyle.Search";
        public const string Pages_Master_Sales_ProductCarBodyStyle_Create = "Pages.Master.Sales.MstSleProductCarBodyStyle.Create";
        public const string Pages_Master_Sales_ProductCarBodyStyle_Edit = "Pages.Master.Sales.MstSleProductCarBodyStyle.Edit";
        public const string Pages_Master_Sales_ProductCarBodyStyle_Delete = "Pages.Master.Sales.MstSleProductCarBodyStyle.Delete";
        #endregion


        #region "Product Car Body Style"
        public const string Pages_Master_Sales_ProductCarEngine = "Pages.Master.Sales.MstSleProductCarEngine";
        public const string Pages_Master_Sales_ProductCarEngine_Search = "Pages.Master.Sales.MstSleProductCarEngine.Search";
        public const string Pages_Master_Sales_ProductCarEngine_Create = "Pages.Master.Sales.MstSleProductCarEngine.Create";
        public const string Pages_Master_Sales_ProductCarEngine_Edit = "Pages.Master.Sales.MstSleProductCarEngine.Edit";
        public const string Pages_Master_Sales_ProductCarEngine_Delete = "Pages.Master.Sales.MstSleProductCarEngine.Delete";
        #endregion

        #region "Trip Request"
        public const string Pages_Master_Sales_TripRequest = "Pages.Master.Sales.MstSleTripRequest";
        public const string Pages_Master_Sales_TripRequest_Search = "Pages.Master.Sales.MstSleTripRequest.Search";
        public const string Pages_Master_Sales_TripRequest_Create = "Pages.Master.Sales.MstSleTripRequest.Create";
        public const string Pages_Master_Sales_TripRequest_Edit = "Pages.Master.Sales.MstSleTripRequest.Edit";
        public const string Pages_Master_Sales_TripRequest_Delete = "Pages.Master.Sales.MstSleTripRequest.Delete";
        #endregion

        #region "Genba"
        public const string Pages_Master_Sales_Genba = "Pages.Master.Sales.MstSleVehicleProductGenba";
        public const string Pages_Master_Sales_Genba_Search = "Pages.Master.Sales.MstSleVehicleProductGenba.Search";
        public const string Pages_Master_Sales_Genba_Create = "Pages.Master.Sales.MstSleVehicleProductGenba.Create";
        public const string Pages_Master_Sales_Genba_Edit = "Pages.Master.Sales.MstSleVehicleProductGenba.Edit";
        public const string Pages_Master_Sales_Genba_Delete = "Pages.Master.Sales.MstSleVehicleProductGenba.Delete";
        #endregion

        #region "Test Drive"
        public const string Pages_Master_Sales_VehicleProductTestDrive = "Pages.Master.Sales.MstSleVehicleProductTestDrive";
        public const string Pages_Master_Sales_VehicleProductTestDrive_Search = "Pages.Master.Sales.MstSleVehicleProductTestDrive.Search";
        public const string Pages_Master_Sales_VehicleProductTestDrive_Create = "Pages.Master.Sales.MstSleVehicleProductTestDrive.Create";
        public const string Pages_Master_Sales_VehicleProductTestDrive_Edit = "Pages.Master.Sales.MstSleVehicleProductTestDrive.Edit";
        public const string Pages_Master_Sales_VehicleProductTestDrive_Delete = "Pages.Master.Sales.MstSleVehicleProductTestDrive.Delete";
        #endregion

        #region "Csr"
        public const string Pages_Master_Sales_Csr = "Pages.Master.Sales.MstSleVehicleProductCsr";
        public const string Pages_Master_Sales_Csr_Search = "Pages.Master.Sales.MstSleVehicleProductCsr.Search";
        public const string Pages_Master_Sales_Csr_Create = "Pages.Master.Sales.MstSleVehicleProductCsr.Create";
        public const string Pages_Master_Sales_Csr_Edit = "Pages.Master.Sales.MstSleVehicleProductCsr.Edit";
        public const string Pages_Master_Sales_Csr_Delete = "Pages.Master.Sales.MstSleVehicleProductCsr.Delete";
        #endregion

        #region "RecruimentOverview"
        public const string Pages_Master_Sales_Recruitment_Overview = "Pages.Master.Sales.MstSleVehicleProductRecruitmentOverview";
        public const string Pages_Master_Sales_Recruitment_Overview_Search = "Pages.Master.Sales.MstSleVehicleProductRecruitmentOverview.Search";
        public const string Pages_Master_Sales_Recruitment_Overview_Create = "Pages.Master.Sales.MstSleVehicleProductRecruitmentOverview.Create";
        public const string Pages_Master_Sales_Recruitment_Overview_Edit = "Pages.Master.Sales.MstSleVehicleProductRecruitmentOverview.Edit";
        public const string Pages_Master_Sales_Recruitment_Overview_Delete = "Pages.Master.Sales.MstSleVehicleProductRecruitmentOverview.Delete";
        #endregion

        #region "VehicleProductSortBy"
        public const string Pages_Master_Sales_Sort_By = "Pages.Master.Sales.MstSleVehicleProductSortBy";
        public const string Pages_Master_Sales_Sort_By_Search = "Pages.Master.Sales.MstSleVehicleProductSortBy.Search";
        public const string Pages_Master_Sales_Sort_By_Create = "Pages.Master.Sales.MstSleVehicleProductSortBy.Create";
        public const string Pages_Master_Sales_Sort_By_Edit = "Pages.Master.Sales.MstSleVehicleProductSortBy.Edit";
        public const string Pages_Master_Sales_Sort_By_Delete = "Pages.Master.Sales.MstSleVehicleProductSortBy.Delete";
        #endregion

        #region "Recruiment Language"
        public const string Pages_Master_Sales_Recruiment_Language = "Pages.Master.Sales.MstSleVehicleProductRecruimentLanguage";
        public const string Pages_Master_Sales_Recruiment_Language_Search = "Pages.Master.Sales.MstSleVehicleProductRecruimentLanguage.Search";
        public const string Pages_Master_Sales_Recruiment_Language_Create = "Pages.Master.Sales.MstSleVehicleProductRecruimentLanguage.Create";
        public const string Pages_Master_Sales_Recruiment_Language_Edit = "Pages.Master.Sales.MstSleVehicleProductRecruimentLanguage.Edit";
        public const string Pages_Master_Sales_Recruiment_Language_Delete = "Pages.Master.Sales.MstSleVehicleProductRecruimentLanguage.Delete";
        #endregion

        #region "Hashtag"
        public const string Pages_Master_Sales_Hashtag = "Pages.Master.Sales.MstSleVehicleProductHashtag";
        public const string Pages_Master_Sales_Hashtag_Search = "Pages.Master.Sales.MstSleVehicleProductHashtag.Search";
        public const string Pages_Master_Sales_Hashtag_Create = "Pages.Master.Sales.MstSleVehicleProductHashtag.Create";
        public const string Pages_Master_Sales_Hashtag_Edit = "Pages.Master.Sales.MstSleVehicleProductHashtag.Edit";
        public const string Pages_Master_Sales_Hashtag_Delete = "Pages.Master.Sales.MstSleVehicleProductHashtag.Delete";
        #endregion

        #region "Sustainable Development"
        public const string Pages_Master_Sales_SustainableDevelopment = "Pages.Master.Sales.MstSleSustainableDevelopment";
        public const string Pages_Master_Sales_SustainableDevelopment_Search = "Pages.Master.Sales.MstSleSustainableDevelopment.Search";
        public const string Pages_Master_Sales_SustainableDevelopment_Create = "Pages.Master.Sales.MstSleSustainableDevelopment.Create";
        public const string Pages_Master_Sales_SustainableDevelopment_Edit = "Pages.Master.Sales.MstSleSustainableDevelopment.Edit";
        public const string Pages_Master_Sales_SustainableDevelopment_Delete = "Pages.Master.Sales.MstSleSustainableDevelopment.Delete";
        #endregion

        #region "Road tax Period"
        public const string Pages_Master_Sales_Road_Tax_Period = "Pages.Master.Sales.MstsleRoadTaxPeriod";
        public const string Pages_Master_Sales_Road_Tax_Period_Search = "Pages.Master.Sales.MstsleRoadTaxPeriod.Search";
        public const string Pages_Master_Sales_Road_Tax_Period_Create = "Pages.Master.Sales.MstsleRoadTaxPeriod.Create";
        public const string Pages_Master_Sales_Road_Tax_Period_Edit = "Pages.Master.Sales.MstsleRoadTaxPeriod.Edit";
        public const string Pages_Master_Sales_Road_Tax_Period_Delete = "Pages.Master.Sales.MstsleRoadTaxPeriod.Delete";
        #endregion

        #region "Road tax Amount"
        public const string Pages_Master_Sales_Road_Tax_Amount = "Pages.Master.Sales.MstsleRoadTaxAmount";
        public const string Pages_Master_Sales_Road_Tax_Amount_Search = "Pages.Master.Sales.MstsleRoadTaxAmount.Search";
        public const string Pages_Master_Sales_Road_Tax_Amount_Create = "Pages.Master.Sales.MstsleRoadTaxAmount.Create";
        public const string Pages_Master_Sales_Road_Tax_Amount_Edit = "Pages.Master.Sales.MstsleRoadTaxAmount.Edit";
        public const string Pages_Master_Sales_Road_Tax_Amount_Delete = "Pages.Master.Sales.MstsleRoadTaxAmount.Delete";
        #endregion

        #region "Vehicle Transportation"
        public const string Pages_Sales_Menu_Vehicle_Transportation = "Pages.Vehicle.Transportation";
        #region "Route"
        public const string Pages_Master_Sales_Route = "Pages.Master.Sales.MstSleRoute";
        public const string Pages_Master_Sales_Route_Search = "Pages.Master.Sales.MstSleRoute.Search";
        public const string Pages_Master_Sales_Route_Create = "Pages.Master.Sales.MstSleRoute.Create";
        public const string Pages_Master_Sales_Route_Edit = "Pages.Master.Sales.MstSleRoute.Edit";
        public const string Pages_Master_Sales_Route_Delete = "Pages.Master.Sales.MstSleRoute.Delete";
        #endregion

        #region "Transportation Type"
        public const string Pages_Master_Sales_Transportation_Type = "Pages.Master.Sales.MstSleTransporationType";
        public const string Pages_Master_Sales_Transportation_Type_Search = "Pages.Master.Sales.MstSleTransporationType.Search";
        public const string Pages_Master_Sales_Transportation_Type_Create = "Pages.Master.Sales.MstSleTransporationType.Create";
        public const string Pages_Master_Sales_Transportation_Type_Edit = "Pages.Master.Sales.MstSleTransporationType.Edit";
        public const string Pages_Master_Sales_Transportation_Type_Delete = "Pages.Master.Sales.MstSleTransporationType.Delete";
        #endregion

        #region "Transportation Route Setup"
        public const string Pages_Master_Sales_Transportation_Route_Setup = "Pages.Master.Sales.MstSleTransportationRouteSetup";
        public const string Pages_Master_Sales_Transportation_Route_Setup_Search = "Pages.Master.Sales.MstSleTransportationRouteSetup.Search";
        public const string Pages_Master_Sales_Transportation_Route_Setup_Create = "Pages.Master.Sales.MstSleTransportationRouteSetup.Create";
        public const string Pages_Master_Sales_Transportation_Route_Setup_Edit = "Pages.Master.Sales.MstSleTransportationRouteSetup.Edit";
        public const string Pages_Master_Sales_Transportation_Route_Setup_Delete = "Pages.Master.Sales.MstSleTransportationRouteSetup.Delete";
        #endregion

        #endregion


        #region "Product Car Fuel"
        public const string Pages_Master_Sales_ProductCarFuel = "Pages.Master.Sales.MstSleProductCarFuel";
        public const string Pages_Master_Sales_ProductCarFuel_Search = "Pages.Master.Sales.MstSleProductCarFuel.Search";
        public const string Pages_Master_Sales_ProductCarFuel_Create = "Pages.Master.Sales.MstSleProductCarFuel.Create";
        public const string Pages_Master_Sales_ProductCarFuel_Edit = "Pages.Master.Sales.MstSleProductCarFuel.Edit";
        public const string Pages_Master_Sales_ProductCarFuel_Delete = "Pages.Master.Sales.MstSleProductCarFuel.Delete";
        #endregion

        #region "Product Car Origin"
        public const string Pages_Master_Sales_ProductCarOrigin = "Pages.Master.Sales.MstSleProductCarOrigin";
        public const string Pages_Master_Sales_ProductCarOrigin_Search = "Pages.Master.Sales.MstSleProductCarOrigin.Search";
        public const string Pages_Master_Sales_ProductCarOrigin_Create = "Pages.Master.Sales.MstSleProductCarOrigin.Create";
        public const string Pages_Master_Sales_ProductCarOrigin_Edit = "Pages.Master.Sales.MstSleProductCarOrigin.Edit";
        public const string Pages_Master_Sales_ProductCarOrigin_Delete = "Pages.Master.Sales.MstSleProductCarOrigin.Delete";

        #endregion

        #region "Vehicle Product Document Son"
        public const string Pages_Master_Sales_VehicleProductDocumentSon = "Pages.Master.Sales.MstSleVehicleProductDocumentSon";
        public const string Pages_Master_Sales_VehicleProductDocumentSon_Search = "Pages.Master.Sales.MstSleVehicleProductDocumentSon.Search";
        public const string Pages_Master_Sales_VehicleProductDocumentSon_Create = "Pages.Master.Sales.MstSleVehicleProductDocumentSon.Create";
        public const string Pages_Master_Sales_VehicleProductDocumentSon_Edit = "Pages.Master.Sales.MstSleVehicleProductDocumentSon.Edit";
        public const string Pages_Master_Sales_VehicleProductDocumentSon_Delete = "Pages.Master.Sales.MstSleVehicleProductDocumentSon.Delete";
        #endregion

        #region "Loyalty"
        public const string Pages_Sales_Menu_Loyalty = "Pages.Loyalty";

        #region "LoyaltyCustomer"
        public const string Pages_Sales_Menu_Loyalty_Customer = "Pages.Loyalty.LoyaltyCustomer";
        public const string Pages_Sales_Menu_Loyalty_Customer_Search = "Pages.Loyalty.LoyaltyCustomer.Search";
        public const string Pages_Sales_Menu_Loyalty_Customer_Create = "Pages.Loyalty.LoyaltyCustomer.Create";
        public const string Pages_Sales_Menu_Loyalty_Customer_Edit = "Pages.Loyalty.LoyaltyCustomer.Edit";
        public const string Pages_Sales_Menu_Loyalty_Customer_Delete = "Pages.Loyalty.LoyaltyCustomer.Delete";
        #endregion


        #region "LoyaltyGift"
        public const string Pages_Sales_Menu_Loyalty_Gift = "Pages.Loyalty.MstSleLoyaltyGift";
        public const string Pages_Sales_Menu_Loyalty_Gift_Search = "Pages.Loyalty.MstSleLoyaltyGift.Search";
        public const string Pages_Sales_Menu_Loyalty_Gift_Create = "Pages.Loyalty.MstSleLoyaltyGift.Create";
        public const string Pages_Sales_Menu_Loyalty_Gift_Edit = "Pages.Loyalty.MstSleLoyaltyGift.Edit";
        public const string Pages_Sales_Menu_Loyalty_Gift_Delete = "Pages.Loyalty.MstSleLoyaltyGift.Delete";
        public const string Pages_Sales_Menu_Loyalty_Gift_TMV = "Pages.Loyalty.MstSleLoyaltyGiftTMV";
        #endregion

        #region "LoyaltyGiftItem"
        public const string Pages_Sales_Menu_Loyalty_Gift_Item = "Pages.Loyalty.MstSleLoyaltyGiftItem";
        public const string Pages_Sales_Menu_Loyalty_Gift_Item_Search = "Pages.Loyalty.MstSleLoyaltyGiftItem.Search";
        public const string Pages_Sales_Menu_Loyalty_Gift_Item_Create = "Pages.Loyalty.MstSleLoyaltyGiftItem.Create";
        public const string Pages_Sales_Menu_Loyalty_Gift_Item_Edit = "Pages.Loyalty.MstSleLoyaltyGiftItem.Edit";
        public const string Pages_Sales_Menu_Loyalty_Gift_Item_Delete = "Pages.Loyalty.MstSleLoyaltyGiftItem.Delete";

        #endregion

        #region "LoyaltRank"
        public const string Pages_Sales_Menu_Loyalty_Rank = "Pages.Loyalty.MstSleLoyaltyRank";
        public const string Pages_Sales_Menu_Loyalty_Rank_Search = "Pages.Loyalty.MstSleLoyaltyRank.Search";
        public const string Pages_Sales_Menu_Loyalty_Rank_Create = "Pages.Loyalty.MstSleLoyaltyRank.Create";
        public const string Pages_Sales_Menu_Loyalty_Rank_Edit = "Pages.Loyalty.MstSleLoyaltyRank.Edit";
        public const string Pages_Sales_Menu_Loyalty_Rank_Delete = "Pages.Loyalty.MstSleLoyaltyRank.Delete";
        #endregion

        #region "LoyaltGain"
        public const string Pages_Sales_Menu_Loyalty_Gain = "Pages.Loyalty.MstSleLoyaltyGain";
        public const string Pages_Sales_Menu_Loyalty_Gain_Search = "Pages.Loyalty.MstSleLoyaltyGain.Search";
        //public const string Pages_Sales_Menu_Loyalty_Gain_Create = "Pages.Loyalty.MstSleLoyaltyGain.Create";
        //public const string Pages_Sales_Menu_Loyalty_Gain_Edit = "Pages.Loyalty.MstSleLoyaltyGain.Edit";
        //public const string Pages_Sales_Menu_Loyalty_Gain_Delete = "Pages.Loyalty.MstSleLoyaltyGain.Delete";
        #endregion

        #region "LoyaltGain"
        public const string Pages_Sales_Menu_Loyalty_Gain_Rank = "Pages.Loyalty.MstSleLoyaltyGainRank";
        public const string Pages_Sales_Menu_Loyalty_Gain_Rank_Search = "Pages.Loyalty.MstSleLoyaltyGainRank.Search";
        //public const string Pages_Sales_Menu_Loyalty_Gain_Create = "Pages.Loyalty.MstSleLoyaltyGain.Create";
        //public const string Pages_Sales_Menu_Loyalty_Gain_Edit = "Pages.Loyalty.MstSleLoyaltyGain.Edit";
        //public const string Pages_Sales_Menu_Loyalty_Gain_Delete = "Pages.Loyalty.MstSleLoyaltyGain.Delete";
        #endregion

        #region "LoyaltScheme"
        public const string Pages_Sales_Menu_Loyalty_Scheme = "Pages.Loyalty.MstSleLoyaltyScheme";
        public const string Pages_Sales_Menu_Loyalty_Scheme_Search = "Pages.Loyalty.MstSleLoyaltyScheme.Search";
        public const string Pages_Sales_Menu_Loyalty_Scheme_Create = "Pages.Loyalty.MstSleLoyaltyScheme.Create";
        public const string Pages_Sales_Menu_Loyalty_Scheme_Edit = "Pages.Loyalty.MstSleLoyaltyScheme.Edit";
        public const string Pages_Sales_Menu_Loyalty_Scheme_Delete = "Pages.Loyalty.MstSleLoyaltyScheme.Delete";
        #endregion

        #region "MstLoyaltyRank"
        public const string Pages_Sales_Menu_Mst_Loyalty_Rank = "Pages.Loyalty.MstLoyaltyRank";
        public const string Pages_Sales_Menu_Mst_Loyalty_Rank_Search = "Pages.Loyalty.MstLoyaltyRank.Search";
        public const string Pages_Sales_Menu_Mst_Loyalty_Rank_Create = "Pages.Loyalty.MstLoyaltyRank.Create";
        public const string Pages_Sales_Menu_Mst_Loyalty_Rank_Edit = "Pages.Loyalty.MstLoyaltyRank.Edit";
        public const string Pages_Sales_Menu_Mst_Loyalty_Rank_Delete = "Pages.Loyalty.MstLoyaltyRank.Delete";
        #endregion

        #region "Loyalty Redeem"
        public const string Pages_Sales_Menu_Loyalty_Redeem = "Pages.Loyalty.MstLoyaltyRedeem";
        public const string Pages_Sales_Menu_Loyalty_Redeem_Search = "Pages.Loyalty.MstLoyaltyRedeem.Search";
        public const string Pages_Sales_Menu_Loyalty_Redeem_Create = "Pages.Loyalty.MstLoyaltyRedeem.Create";
        public const string Pages_Sales_Menu_Loyalty_Redeem_Edit = "Pages.Loyalty.MstLoyaltyRedeem.Edit";
        public const string Pages_Sales_Menu_Loyalty_Redeem_Delete = "Pages.Loyalty.MstLoyaltyRedeem.Delete";
        #endregion

        #region "Loyalty Transaction"
        public const string Pages_Sales_Menu_Loyalty_Transaction = "Pages.Loyalty.MstLoyaltyTransaction";
        public const string Pages_Sales_Menu_Loyalty_Transaction_Search = "Pages.Loyalty.MstLoyaltyTransaction.Search";
        public const string Pages_Sales_Menu_Loyalty_Transaction_Create = "Pages.Loyalty.MstLoyaltyTransaction.Create";
        public const string Pages_Sales_Menu_Loyalty_Transaction_Edit = "Pages.Loyalty.MstLoyaltyTransaction.Edit";
        public const string Pages_Sales_Menu_Loyalty_Transaction_Delete = "Pages.Loyalty.MstLoyaltyTransaction.Delete";
        #endregion

        #region "Loyalty Question"
        public const string Pages_Sales_Menu_Loyalty_Question = "Pages.Loyalty.MstLoyaltyQuestion";
        public const string Pages_Sales_Menu_Loyalty_Question_Search = "Pages.Loyalty.MstLoyaltyQuestion.Search";
        public const string Pages_Sales_Menu_Loyalty_Question_Create = "Pages.Loyalty.MstLoyaltyQuestion.Create";
        public const string Pages_Sales_Menu_Loyalty_Question_Edit = "Pages.Loyalty.MstLoyaltyQuestion.Edit";
        public const string Pages_Sales_Menu_Loyalty_Question_Delete = "Pages.Loyalty.MstLoyaltyQuestion.Delete";
        #endregion

        #region "Loyalty Claim"
        public const string Pages_Sales_Menu_Loyalty_Claim = "Pages.Loyalty.MstLoyaltyClaim";
        public const string Pages_Sales_Menu_Loyalty_Claim_Search = "Pages.Loyalty.MstLoyaltyClaim.Search";
        public const string Pages_Sales_Menu_Loyalty_Claim_Create = "Pages.Loyalty.MstLoyaltyClaim.Create";
        public const string Pages_Sales_Menu_Loyalty_Claim_Edit = "Pages.Loyalty.MstLoyaltyClaim.Edit";
        public const string Pages_Sales_Menu_Loyalty_Claim_Delete = "Pages.Loyalty.MstLoyaltyClaim.Delete";
        #endregion


        #endregion

        #region "Vehicle Product Document Parent"
        public const string Pages_Master_Sales_VehicleProductDocumentParent = "Pages.Master.Sales.MstSleVehicleProductDocumentParent";
        public const string Pages_Master_Sales_VehicleProductDocumentParent_Search = "Pages.Master.Sales.MstSleVehicleProductDocumentParent.Search";
        public const string Pages_Master_Sales_VehicleProductDocumentParent_Create = "Pages.Master.Sales.MstSleVehicleProductDocumentParent.Create";
        public const string Pages_Master_Sales_VehicleProductDocumentParent_Edit = "Pages.Master.Sales.MstSleVehicleProductDocumentParent.Edit";
        public const string Pages_Master_Sales_VehicleProductDocumentParent_Delete = "Pages.Master.Sales.MstSleVehicleProductDocumentParent.Delete";
        #endregion
        #region "Vehicle Product Ti Vendor"
        public const string Pages_Master_Sales_VehicleProductTiVendor = "Pages.Master.Sales.MstSleVehicleProductTiVendor";
        public const string Pages_Master_Sales_VehicleProductTiVendor_Search = "Pages.Master.Sales.MstSleVehicleProductTiVendor.Search";
        public const string Pages_Master_Sales_VehicleProductTiVendor_Create = "Pages.Master.Sales.MstSleVehicleProductTiVendor.Create";
        public const string Pages_Master_Sales_VehicleProductTiVendor_Edit = "Pages.Master.Sales.MstSleVehicleProductTiVendor.Edit";
        public const string Pages_Master_Sales_VehicleProductTiVendor_Delete = "Pages.Master.Sales.MstSleVehicleProductTiVendor.Delete";
        #endregion

        #region "Vehicle Product Tecnology Info"
        public const string Pages_Master_Sales_VehicleProductTecnologyInfo = "Pages.Master.Sales.MstSleVehicleProductTecnologyInfo";
        public const string Pages_Master_Sales_VehicleProductTecnologyInfo_Search = "Pages.Master.Sales.MstSleVehicleProductTecnologyInfo.Search";
        public const string Pages_Master_Sales_VehicleProductTecnologyInfo_Create = "Pages.Master.Sales.MstSleVehicleProductTecnologyInfo.Create";
        public const string Pages_Master_Sales_VehicleProductTecnologyInfo_Edit = "Pages.Master.Sales.MstSleVehicleProductTecnologyInfo.Edit";
        public const string Pages_Master_Sales_VehicleProductTecnologyInfo_Delete = "Pages.Master.Sales.MstSleVehicleProductTecnologyInfo.Delete";
        #endregion

        #region "SubCategory"
        public const string Pages_Master_Sales_SubCategory = "Pages.Master.Sales.MstSleSubCategory";
        public const string Pages_Master_Sales_SubCategory_Search = "Pages.Master.Sales.MstSleSubCategory.Search";
        public const string Pages_Master_Sales_SubCategory_Create = "Pages.Master.Sales.MstSleSubCategory.Create";
        public const string Pages_Master_Sales_SubCategory_Edit = "Pages.Master.Sales.MstSleSubCategory.Edit";
        public const string Pages_Master_Sales_SubCategory_Delete = "Pages.Master.Sales.MstSleSubCategory.Delete";
        #endregion

        #region "Vehicle Product TI Question"
        public const string Pages_Master_Sales_VehicleProductTIQuestion = "Pages.Master.Sales.MstSleVehicleProductTIQuestion";
        public const string Pages_Master_Sales_VehicleProductTIQuestion_Search = "Pages.Master.Sales.MstSleVehicleProductTIQuestion.Search";
        public const string Pages_Master_Sales_VehicleProductTIQuestion_Create = "Pages.Master.Sales.MstSleVehicleProductTIQuestion.Create";
        public const string Pages_Master_Sales_VehicleProductTIQuestion_Edit = "Pages.Master.Sales.MstSleVehicleProductTIQuestion.Edit";
        public const string Pages_Master_Sales_VehicleProductTIQuestion_Delete = "Pages.Master.Sales.MstSleVehicleProductTIQuestion.Delete";
        #endregion

        #region "Vehicle Product Introduce"
        public const string Pages_Master_Sales_VehicleProductIntroduce = "Pages.Master.Sales.MstSleVehicleProductIntroduce";
        public const string Pages_Master_Sales_VehicleProductIntroduce_Search = "Pages.Master.Sales.MstSleVehicleProductIntroduce.Search";
        public const string Pages_Master_Sales_VehicleProductIntroduce_Create = "Pages.Master.Sales.MstSleVehicleProductIntroduce.Create";
        public const string Pages_Master_Sales_VehicleProductIntroduce_Edit = "Pages.Master.Sales.MstSleVehicleProductIntroduce.Edit";
        public const string Pages_Master_Sales_VehicleProductIntroduce_Delete = "Pages.Master.Sales.MstSleVehicleProductIntroduce.Delete";
        #endregion

        #region "Vehicle Product TFS"
        public const string Pages_Master_Sales_VehicleProductTFS = "Pages.Master.Sales.MstSleVehicleProductTFS";
        public const string Pages_Master_Sales_VehicleProductTFS_Search = "Pages.Master.Sales.MstSleVehicleProductTFS.Search";
        public const string Pages_Master_Sales_VehicleProductTFS_Create = "Pages.Master.Sales.MstSleVehicleProductTFS.Create";
        public const string Pages_Master_Sales_VehicleProductTFS_Edit = "Pages.Master.Sales.MstSleVehicleProductTFS.Edit";
        public const string Pages_Master_Sales_VehicleProductTFS_Delete = "Pages.Master.Sales.MstSleVehicleProductTFS.Delete";
        #endregion

        #region "MstSleTradeSchool"
        public const string Pages_Master_Sales_TradeSchool = "Pages.Master.Sales.MstSleTradeSchool";
        public const string Pages_Master_Sales_TradeSchool_Search = "Pages.Master.Sales.MstSleTradeSchool.Search";
        public const string Pages_Master_Sales_TradeSchool_Create = "Pages.Master.Sales.MstSleTradeSchool.Create";
        public const string Pages_Master_Sales_TradeSchool_Edit = "Pages.Master.Sales.MstSleTradeSchool.Edit";
        public const string Pages_Master_Sales_TradeSchool_Delete = "Pages.Master.Sales.MstSleTradeSchool.Delete";
        #endregion

        #region "MstSleExamConditions"
        public const string Pages_Master_Sales_ExamConditions = "Pages.Master.Sales.MstSleExamConditions";
        public const string Pages_Master_Sales_ExamConditions_Search = "Pages.Master.Sales.MstSleExamConditions.Search";
        public const string Pages_Master_Sales_ExamConditions_Create = "Pages.Master.Sales.MstSleExamConditions.Create";
        public const string Pages_Master_Sales_ExamConditions_Edit = "Pages.Master.Sales.MstSleExamConditions.Edit";
        public const string Pages_Master_Sales_ExamConditions_Delete = "Pages.Master.Sales.MstSleExamConditions.Delete";
        #endregion

        #region "Vehicle Product News"
        public const string Pages_Master_Sales_VehicleProductNews = "Pages.Master.Sales.MstSleVehicleProductNews";
        public const string Pages_Master_Sales_VehicleProductNews_Search = "Pages.Master.Sales.MstSleVehicleProductNews.Search";
        public const string Pages_Master_Sales_VehicleProductNews_Create = "Pages.Master.Sales.MstSleVehicleProductNews.Create";
        public const string Pages_Master_Sales_VehicleProductNews_Edit = "Pages.Master.Sales.MstSleVehicleProductNews.Edit";
        public const string Pages_Master_Sales_VehicleProductNews_Delete = "Pages.Master.Sales.MstSleVehicleProductNews.Delete";
        #endregion

        #region "Vehicle Product News Category"
        public const string Pages_Master_Sales_VehicleProductNewsCategory = "Pages.Master.Sales.MstSleVehicleProductNewsCategory";
        public const string Pages_Master_Sales_VehicleProductNewsCategory_Search = "Pages.Master.Sales.MstSleVehicleProductNewsCategory.Search";
        public const string Pages_Master_Sales_VehicleProductNewsCategory_Create = "Pages.Master.Sales.MstSleVehicleProductNewsCategory.Create";
        public const string Pages_Master_Sales_VehicleProductNewsCategory_Edit = "Pages.Master.Sales.MstSleVehicleProductNewsCategory.Edit";
        public const string Pages_Master_Sales_VehicleProductNewsCategory_Delete = "Pages.Master.Sales.MstSleVehicleProductNewsCategory.Delete";
        #endregion

        #region "MstSleLocalizationAndExport"
        public const string Pages_Master_Sales_LocalizationAndExport = "Pages.Master.Sales.MstSleLocalizationAndExport";
        public const string Pages_Master_Sales_LocalizationAndExport_Search = "Pages.Master.Sales.MstSleLocalizationAndExport.Search";
        public const string Pages_Master_Sales_LocalizationAndExport_Create = "Pages.Master.Sales.MstSleLocalizationAndExport.Create";
        public const string Pages_Master_Sales_LocalizationAndExport_Edit = "Pages.Master.Sales.MstSleLocalizationAndExport.Edit";
        public const string Pages_Master_Sales_LocalizationAndExport_Delete = "Pages.Master.Sales.MstSleLocalizationAndExport.Delete";
        #endregion

        #region "MstSleVehicleProductNumberSeat"
        public const string Pages_Master_Sales_VehicleProductNumberSeat = "Pages.Master.Sales.MstSleVehicleProductNumberSeat";
        public const string Pages_Master_Sales_VehicleProductNumberSeat_Search = "Pages.Master.Sales.MstSleVehicleProductNumberSeat.Search";
        public const string Pages_Master_Sales_VehicleProductNumberSeat_Create = "Pages.Master.Sales.MstSleVehicleProductNumberSeat.Create";
        public const string Pages_Master_Sales_VehicleProductNumberSeat_Edit = "Pages.Master.Sales.MstSleVehicleProductNumberSeat.Edit";
        public const string Pages_Master_Sales_VehicleProductNumberSeat_Delete = "Pages.Master.Sales.MstSleVehicleProductNumberSeat.Delete";
        #endregion

        #region "MstSleVehicleProductPriceRange"
        public const string Pages_Master_Sales_VehicleProductPriceRange = "Pages.Master.Sales.MstSleVehicleProductPriceRange";
        public const string Pages_Master_Sales_VehicleProductPriceRange_Search = "Pages.Master.Sales.MstSleVehicleProductPriceRange.Search";
        public const string Pages_Master_Sales_VehicleProductPriceRange_Create = "Pages.Master.Sales.MstSleVehicleProductPriceRange.Create";
        public const string Pages_Master_Sales_VehicleProductPriceRange_Edit = "Pages.Master.Sales.MstSleVehicleProductPriceRange.Edit";
        public const string Pages_Master_Sales_VehicleProductPriceRange_Delete = "Pages.Master.Sales.MstSleVehicleProductPriceRange.Delete";
        #endregion

        #region "MstSleVehicleProductBanner"
        public const string Pages_Master_Sales_VehicleProductBanner = "Pages.Master.Sales.MstSleVehicleProductBanner";
        public const string Pages_Master_Sales_VehicleProductBanner_Search = "Pages.Master.Sales.MstSleVehicleProductBanner.Search";
        public const string Pages_Master_Sales_VehicleProductBanner_Create = "Pages.Master.Sales.MstSleVehicleProductBanner.Create";
        public const string Pages_Master_Sales_VehicleProductBanner_Edit = "Pages.Master.Sales.MstSleVehicleProductBanner.Edit";
        public const string Pages_Master_Sales_VehicleProductBanner_Delete = "Pages.Master.Sales.MstSleVehicleProductBanner.Delete";
        #endregion

        #region "MstSleVehicleProductRouter"
        public const string Pages_Master_Sales_VehicleProductRouter = "Pages.Master.Sales.MstSleVehicleProductRouter";
        public const string Pages_Master_Sales_VehicleProductRouter_Search = "Pages.Master.Sales.MstSleVehicleProductRouter.Search";
        public const string Pages_Master_Sales_VehicleProductRouter_Create = "Pages.Master.Sales.MstSleVehicleProductRouter.Create";
        public const string Pages_Master_Sales_VehicleProductRouter_Edit = "Pages.Master.Sales.MstSleVehicleProductRouter.Edit";
        public const string Pages_Master_Sales_VehicleProductRouter_Delete = "Pages.Master.Sales.MstSleVehicleProductRouter.Delete";
        #endregion

        #region "MstSleVehicleProductBooking"
        public const string Pages_Master_Sales_VehicleProductBooking = "Pages.Master.Sales.MstSleVehicleProductBooking";
        public const string Pages_Master_Sales_VehicleProductBooking_Search = "Pages.Master.Sales.MstSleVehicleProductBooking.Search";
        #endregion

        #region "MstSleErrorNotiInfomation"
        public const string Pages_Master_Sales_ErrorNotilnFomation = "Pages.Master.Sales.MstSleErrorNotilnFomation";
        public const string Pages_Master_Sales_ErrorNotilnFomation_Search = "Pages.Master.Sales.MstSleErrorNotilnFomation.Search";
        public const string Pages_Master_Sales_ErrorNotilnFomation_Create = "Pages.Master.Sales.MstSleErrorNotilnFomation.Create";
        public const string Pages_Master_Sales_ErrorNotiInfomation_Edit = "Pages.Master.Sales.MstSleErrorNotilnFomation.Edit";
        public const string Pages_Master_Sales_ErrorNotiInfomation_Delete = "Pages.Master.Sales.MstSleErrorNotilnFomation.Delete";
        #endregion

        #region "MstSleLocalizationCommunicate"
        public const string Pages_Master_Sales_LocalizationCommunicate = "Pages.Master.Sales.MstSleLocalizationCommunicate";
        public const string Pages_Master_Sales_LocalizationCommunicate_Search = "Pages.Master.Sales.MstSleLocalizationCommunicate.Search";
        public const string Pages_Master_Sales_LocalizationCommunicate_Create = "Pages.Master.Sales.MstSleLocalizationCommunicate.Create";
        public const string Pages_Master_Sales_LocalizationCommunicate_Edit = "Pages.Master.Sales.MstSleLocalizationCommunicate.Edit";
        public const string Pages_Master_Sales_LocalizationCommunicate_Delete = "Pages.Master.Sales.MstSleLocalizationCommunicate.Delete";
        #endregion

        #region "MstSleLocalizationAccessoryDetail"
        public const string Pages_Master_Sales_LocalizationAccessoryDetail = "Pages.Master.Sales.MstSleLocalizationAccessoryDetail";
        public const string Pages_Master_Sales_LocalizationAccessoryDetail_Search = "Pages.Master.Sales.MstSleLocalizationAccessoryDetail.Search";
        public const string Pages_Master_Sales_LocalizationAccessoryDetail_Create = "Pages.Master.Sales.MstSleLocalizationAccessoryDetail.Create";
        public const string Pages_Master_Sales_LocalizationAccessoryDetail_Edit = "Pages.Master.Sales.MstSleLocalizationAccessoryDetail.Edit";
        public const string Pages_Master_Sales_LocalizationAccessoryDetail_Delete = "Pages.Master.Sales.MstSleLocalizationAccessoryDetail.Delete";
        #endregion

        #region "MstSleLocalizationDocumentCategory"
        public const string Pages_Master_Sales_LocalizationDocumentCategory = "Pages.Master.Sales.MstSleLocalizationDocumentCategory";
        public const string Pages_Master_Sales_LocalizationDocumentCategory_Search = "Pages.Master.Sales.MstSleLocalizationDocumentCategory.Search";
        public const string Pages_Master_Sales_LocalizationDocumentCategory_Create = "Pages.Master.Sales.MstSleLocalizationDocumentCategory.Create";
        public const string Pages_Master_Sales_LocalizationDocumentCategory_Edit = "Pages.Master.Sales.MstSleLocalizationDocumentCategory.Edit";
        public const string Pages_Master_Sales_LocalizationDocumentCategory_Delete = "Pages.Master.Sales.MstSleLocalizationDocumentCategory.Delete";
        #endregion

        #region "MstSleLocalizationCategory"
        public const string Pages_Master_Sales_LocalizationCategory = "Pages.Master.Sales.MstSleLocalizationCategory";
        public const string Pages_Master_Sales_LocalizationCategory_Search = "Pages.Master.Sales.MstSleLocalizationCategory.Search";
        public const string Pages_Master_Sales_LocalizationCategory_Create = "Pages.Master.Sales.MstSleLocalizationCategory.Create";
        public const string Pages_Master_Sales_LocalizationCategory_Edit = "Pages.Master.Sales.MstSleLocalizationCategory.Edit";
        public const string Pages_Master_Sales_LocalizationCategory_Delete = "Pages.Master.Sales.MstSleLocalizationCategory.Delete";
        #endregion

        #region "MstSleVehicleProductIntroduceRouter"
        public const string Pages_Master_Sales_VehicleProductIntroduceRouter = "Pages.Master.Sales.MstSleVehicleProductIntroduceRouter";
        public const string Pages_Master_Sales_VehicleProductIntroduceRouter_Search = "Pages.Master.Sales.MstSleVehicleProductIntroduceRouter.Search";
        public const string Pages_Master_Sales_VehicleProductIntroduceRouter_Create = "Pages.Master.Sales.MstSleVehicleProductIntroduceRouter.Create";
        public const string Pages_Master_Sales_VehicleProductIntroduceRouter_Edit = "Pages.Master.Sales.MstSleVehicleProductIntroduceRouter.Edit";
        public const string Pages_Master_Sales_VehicleProductIntroduceRouter_Delete = "Pages.Master.Sales.MstSleVehicleProductIntroduceRouter.Delete";
        #endregion

        #region "MstSleVehicleProductIntroduceCategory"
        public const string Pages_Master_Sales_VehicleProductIntroduceCategory = "Pages.Master.Sales.MstSleVehicleProductIntroduceCategory";
        public const string Pages_Master_Sales_VehicleProductIntroduceCategory_Search = "Pages.Master.Sales.MstSleVehicleProductIntroduceCategory.Search";
        public const string Pages_Master_Sales_VehicleProductIntroduceCategory_Create = "Pages.Master.Sales.MstSleVehicleProductIntroduceCategory.Create";
        public const string Pages_Master_Sales_VehicleProductIntroduceCategory_Edit = "Pages.Master.Sales.MstSleVehicleProductIntroduceCategory.Edit";
        public const string Pages_Master_Sales_VehicleProductIntroduceCategory_Delete = "Pages.Master.Sales.MstSleVehicleProductIntroduceCategory.Delete";
        #endregion

        #region "MstSleVehicleProductCommunicate"
        public const string Pages_Master_Sales_VehicleProductCommunicate = "Pages.Master.Sales.MstSleVehicleProductCommunicate";
        public const string Pages_Master_Sales_VehicleProductCommunicate_Search = "Pages.Master.Sales.MstSleVehicleProductCommunicate.Search";
        public const string Pages_Master_Sales_VehicleProductCommunicate_Create = "Pages.Master.Sales.MstSleVehicleProductCommunicate.Create";
        public const string Pages_Master_Sales_VehicleProductCommunicate_Edit = "Pages.Master.Sales.MstSleVehicleProductCommunicate.Edit";
        public const string Pages_Master_Sales_VehicleProductCommunicate_Delete = "Pages.Master.Sales.MstSleVehicleProductCommunicate.Delete";
        #endregion

        #region "MstSleRecruitmentDepartmentUnit"
        public const string Pages_Master_Sales_RecruitmentDepartmentUnit = "Pages.Master.Sales.MstSleRecruitmentDepartmentUnit";
        public const string Pages_Master_Sales_RecruitmentDepartmentUnit_Search = "Pages.Master.Sales.MstSleRecruitmentDepartmentUnit.Search";
        public const string Pages_Master_Sales_RecruitmentDepartmentUnit_Create = "Pages.Master.Sales.MstSleRecruitmentDepartmentUnit.Create";
        public const string Pages_Master_Sales_RecruitmentDepartmentUnit_Edit = "Pages.Master.Sales.MstSleRecruitmentDepartmentUnit.Edit";
        public const string Pages_Master_Sales_RecruitmentDepartmentUnit_Delete = "Pages.Master.Sales.MstSleRecruitmentDepartmentUnit.Delete";
        #endregion

        #region "MstSleRecruitmentWeb"
        public const string Pages_Master_Sales_RecruitmentWeb = "Pages.Master.Sales.MstSleRecruitmentWeb";
        public const string Pages_Master_Sales_RecruitmentWeb_Search = "Pages.Master.Sales.MstSleRecruitmentWeb.Search";
        public const string Pages_Master_Sales_RecruitmentWeb_Create = "Pages.Master.Sales.MstSleRecruitmentWeb.Create";
        public const string Pages_Master_Sales_RecruitmentWeb_Edit = "Pages.Master.Sales.MstSleRecruitmentWeb.Edit";
        public const string Pages_Master_Sales_RecruitmentWeb_Delete = "Pages.Master.Sales.MstSleRecruitmentWeb.Delete";
        #endregion

        #region "Vehicle Product Value Chain"
        public const string Pages_Master_Sales_VehicleProductValueChain = "Pages.Master.Sales.MstSleVehicleProductValueChain";
        public const string Pages_Master_Sales_VehicleProductValueChain_Search = "Pages.Master.Sales.MstSleVehicleProductValueChain.Search";
        public const string Pages_Master_Sales_VehicleProductValueChain_Create = "Pages.Master.Sales.MstSleVehicleProductValueChain.Create";
        public const string Pages_Master_Sales_VehicleProductValueChain_Edit = "Pages.Master.Sales.MstSleVehicleProductValueChain.Edit";
        public const string Pages_Master_Sales_VehicleProductValueChain_Delete = "Pages.Master.Sales.MstSleVehicleProductValueChain.Delete";
        #endregion
        #region "Accessory Types"
        public const string Pages_Master_Sales_AccessoryTypes = "Pages.Master.Sales.MstSleAccessoryTypes";
        public const string Pages_Master_Sales_AccessoryTypes_Search = "Pages.Master.Sales.MstSleAccessoryTypes.Search";
        public const string Pages_Master_Sales_AccessoryTypes_Create = "Pages.Master.Sales.MstSleAccessoryTypes.Create";
        public const string Pages_Master_Sales_AccessoryTypes_Edit = "Pages.Master.Sales.MstSleAccessoryTypes.Edit";
        public const string Pages_Master_Sales_AccessoryTypes_Delete = "Pages.Master.Sales.MstSleAccessoryTypes.Delete";
        #endregion

        #region "Delivery Date Change Reason"
        public const string Pages_Master_Sales_Delivery_Date_Change_Reason = "Pages.Master.Sales.MstSleDeliveryDateChangeReason";
        public const string Pages_Master_Sales_DeliveryDateChangeReason_Search = "Pages.Master.Sales.MstSleDeliveryDateChangeReason.Search";
        public const string Pages_Master_Sales_DeliveryDateChangeReason_Create = "Pages.Master.Sales.MstSleDeliveryDateChangeReason.Create";
        public const string Pages_Master_Sales_DeliveryDateChangeReason_Edit = "Pages.Master.Sales.MstSleDeliveryDateChangeReason.Edit";
        public const string Pages_Master_Sales_DeliveryDateChangeReason_Delete = "Pages.Master.Sales.MstSleDeliveryDateChangeReason.Delete";
        #endregion

        #region "Accessories Manufactor"
        public const string Pages_Master_Sales_Accessory_Manufactor = "Pages.Master.Sales.MstSleAccessoryManufactor";
        public const string Pages_Master_Sales_Accessory_Manufactor_Search = "Pages.Master.Sales.MstSleAccessoryManufactor.Search";
        public const string Pages_Master_Sales_Accessory_Manufactor_Create = "Pages.Master.Sales.MstSleAccessoryManufactor.Create";
        public const string Pages_Master_Sales_Accessory_Manufactor_Edit = "Pages.Master.Sales.MstSleAccessoryManufactor.Edit";
        public const string Pages_Master_Sales_Accessory_Manufactor_Delete = "Pages.Master.Sales.MstSleAccessoryManufactor.Delete";
        #endregion

        #region "Finance Classic Package"
        public const string Pages_Master_Sales_FinanceClassicPackage = "Pages.Master.Sales.MstSleFinanceClassicPackage";
        public const string Pages_Master_Sales_FinanceClassicPackage_Search = "Pages.Master.Sales.MstSleFinanceClassicPackage.Search";
        public const string Pages_Master_Sales_FinanceClassicPackage_Create = "Pages.Master.Sales.MstSleFinanceClassicPackage.Create";
        public const string Pages_Master_Sales_FinanceClassicPackage_Edit = "Pages.Master.Sales.MstSleFinanceClassicPackage.Edit";
        public const string Pages_Master_Sales_FinanceClassicPackage_Delete = "Pages.Master.Sales.MstSleFinanceClassicPackage.Delete";

        #endregion
        #region "Master InSurance Company"
        public const string Pages_Master_Sales_MstSleInsuranceCompany = "Pages.Master.Sales.MstSleInsuranceCompany";
        public const string Pages_Master_Sales_MstSleInsuranceCompany_Search = "Pages.Master.Sales.MstSleInsuranceCompany.Search";
        public const string Pages_Master_Sales_MstSleInsuranceCompany_Create = "Pages.Master.Sales.MstSleInsuranceCompany.Create";
        public const string Pages_Master_Sales_MstSleInsuranceCompany_Edit = "Pages.Master.Sales.MstSleInsuranceCompany.Edit";
        public const string Pages_Master_Sales_MstSleInsuranceCompany_Delete = "Pages.Master.Sales.MstSleInsuranceCompany.Delete";
        #endregion

        #region "Master Vehicle Price"
        public const string Pages_Master_Sales_VehiclePrice = "Pages.Master.Sales.MstSleVehiclePrice";
        public const string Pages_Master_Sales_VehiclePrice_Search = "Pages.Master.Sales.MstSleVehiclePrice.Search";
        public const string Pages_Master_Sales_VehiclePrice_Create = "Pages.Master.Sales.MstSleVehiclePrice.Create";
        public const string Pages_Master_Sales_VehiclePrice_Edit = "Pages.Master.Sales.MstSleVehiclePrice.Edit";
        public const string Pages_Master_Sales_VehiclePrice_Delete = "Pages.Master.Sales.MstSleVehiclePrice.Delete";
        #endregion

        #region "Master Fleet Vehicle Price"
        public const string Pages_Master_Sales_FleetVehiclePrice = "Pages.Master.Sales.MstSleFleetVehiclePrice";
        public const string Pages_Master_Sales_FleetVehiclePrice_Search = "Pages.Master.Sales.MstSleFleetVehiclePrice.Search";
        public const string Pages_Master_Sales_FleetVehiclePrice_Create = "Pages.Master.Sales.MstSleFleetVehiclePrice.Create";
        public const string Pages_Master_Sales_FleetVehiclePrice_Edit = "Pages.Master.Sales.MstSleFleetVehiclePrice.Edit";
        public const string Pages_Master_Sales_FleetVehiclePrice_Delete = "Pages.Master.Sales.MstSleFleetVehiclePrice.Delete";
        #endregion
        #region "Master Vehicle Image"
        public const string Pages_Master_Sales_VehicleImage = "Pages.Master.Sales.MstSleVehicleImage";
        public const string Pages_Master_Sales_VehicleImage_Search = "Pages.Master.Sales.MstSleVehicleImage.Search";
        public const string Pages_Master_Sales_VehicleImage_Create = "Pages.Master.Sales.MstSleVehicleImage.Create";
        public const string Pages_Master_Sales_VehicleImage_Edit = "Pages.Master.Sales.MstSleVehicleImage.Edit";
        public const string Pages_Master_Sales_VehicleImage_Delete = "Pages.Master.Sales.MstSleVehicleImage.Delete";
        #endregion
        #region "Master Engien Type"
        public const string Pages_Master_Sales_EngineType = "Pages.Master.Sales.MstSleEngineType";
        public const string Pages_Master_Sales_EngineType_Search = "Pages.Master.Sales.MstSleEngineType.Search";
        public const string Pages_Master_Sales_EngineType_Create = "Pages.Master.Sales.MstSleEngineType.Create";
        public const string Pages_Master_Sales_EngineType_Edit = "Pages.Master.Sales.MstSleEngineType.Edit";
        public const string Pages_Master_Sales_EngineType_Delete = "Pages.Master.Sales.MstSleEngineType.Delete";
        #endregion

        #region "Master Insurance TI Product"
        public const string Pages_Master_Sales_MstSleInsuranceTIProduct = "Pages.Master.Sales.MstSleInsuranceTIProduct";
        public const string Pages_Master_Sales_MstSleInsuranceTIProduct_Search = "Pages.Master.Sales.MstSleInsuranceTIProduct.Search";
        public const string Pages_Master_Sales_MstSleInsuranceTIProduct_Create = "Pages.Master.Sales.MstSleInsuranceTIProduct.Create";
        public const string Pages_Master_Sales_MstSleInsuranceTIProduct_Edit = "Pages.Master.Sales.MstSleInsuranceTIProduct.Edit";
        public const string Pages_Master_Sales_MstSleInsuranceTIProduct_Delete = "Pages.Master.Sales.MstSleInsuranceTIProduct.Delete";

        #endregion
        #region "Master Insurance Non TI Product"
        public const string Pages_Master_Sales_MstSleInsuranceNonTIProduct = "Pages.Master.Sales.MstSleInsuranceNonTIProduct";
        public const string Pages_Master_Sales_MstSleInsuranceNonTIProduct_Search = "Pages.Master.Sales.MstSleInsuranceNonTIProduct.Search";
        public const string Pages_Master_Sales_MstSleInsuranceNonTIProduct_Create = "Pages.Master.Sales.MstSleInsuranceNonTIProduct.Create";
        public const string Pages_Master_Sales_MstSleInsuranceNonTIProduct_Edit = "Pages.Master.Sales.MstSleInsuranceNonTIProduct.Edit";
        public const string Pages_Master_Sales_MstSleInsuranceNonTIProduct_Delete = "Pages.Master.Sales.MstSleInsuranceNonTIProduct.Delete";
        public const string Pages_Master_Sales_MstSleInsuranceNonTIProduct_GetMstSleInsuranceNonTIProductToExcel = "Pages.Master.Sales.MstSleInsuranceNonTIProduct.GetMstSleInsuranceNonTIProductToExcel";

        #endregion


        #region "Gender"
        public const string Pages_Master_Sales_Gender = "Pages.Master.Sales.MstSleGender";
        public const string Pages_Master_Sales_Gender_Create = "Pages.Master.Sales.MstSleGender.Create";
        public const string Pages_Master_Sales_Gender_Edit = "Pages.Master.Sales.MstSleGender.Edit";
        public const string Pages_Master_Sales_Gender_Delete = "Pages.Master.Sales.MstSleGender.Delete";
        public const string Pages_Master_Sales_Gender_Search = "Pages.Master.Sales.MstSleGender.Search";
        #endregion

        #region "Marital Status"
        public const string Pages_Master_Sales_MaritalStatus = "Pages.Master.Sales.MstSleMaritalStatus";
        public const string Pages_Master_Sales_MaritalStatus_Create = "Pages.Master.Sales.MstSleMaritalStatus.Create";
        public const string Pages_Master_Sales_MaritalStatus_Edit = "Pages.Master.Sales.MstSleMaritalStatus.Edit";
        public const string Pages_Master_Sales_MaritalStatus_Delete = "Pages.Master.Sales.MstSleMaritalStatus.Delete";
        public const string Pages_Master_Sales_MaritalStatus_Search = "Pages.Master.Sales.MstSleMaritalStatus.Search";
        #endregion

        #region "Master Sale Unit"
        public const string Pages_Master_Sales_Unit = "Pages.Master.Sales.MstSleUnit";
        public const string Pages_Master_Sales_Unit_Create = "Pages.Master.Sales.MstSleUnit.Create";
        public const string Pages_Master_Sales_Unit_Edit = "Pages.Master.Sales.MstSleUnit.Edit";
        public const string Pages_Master_Sales_Unit_Delete = "Pages.Master.Sales.MstSleUnit.Delete";
        public const string Pages_Master_Sales_Unit_Search = "Pages.Master.Sales.MstSleUnit.Search";
        #endregion

        #region "Master Insurance Type"
        public const string Pages_Master_Sales_InsuranceType = "Pages.Master.Sales.MstSleInsuranceType";
        public const string Pages_Master_Sales_InsuranceType_Create = "Pages.Master.Sales.MstSleInsuranceType.Create";
        public const string Pages_Master_Sales_InsuranceType_Edit = "Pages.Master.Sales.MstSleInsuranceType.Edit";
        public const string Pages_Master_Sales_InsuranceType_Delete = "Pages.Master.Sales.MstSleInsuranceType.Delete";
        public const string Pages_Master_Sales_InsuranceType_Search = "Pages.Master.Sales.MstSleInsuranceType.Search";

        #endregion

        #region "Master Sale PPM Package"
        public const string Pages_Master_Sales_PPM_Package = "Pages.Master.Sales.MstSlePpmPackage";
        public const string Pages_Master_Sales_PPM_Package_Create = "Pages.Master.Sales.MstSlePpmPackage.Create";
        public const string Pages_Master_Sales_PPM_Package_Edit = "Pages.Master.Sales.MstSlePpmPackage.Edit";
        public const string Pages_Master_Sales_PPM_Package_Delete = "Pages.Master.Sales.MstSlePpmPackage.Delete";
        public const string Pages_Master_Sales_PPM_Package_Search = "Pages.Master.Sales.MstSlePpmPackage.Search";
        #endregion

        #region "Master Sale Dealer Contract Info"
        public const string Pages_Master_Sales_Dealer_Contract_Info = "Pages.Master.Sales.MstSleDealerContractInfo";
        public const string Pages_Master_Sales_Dealer_Contract_Info_Create = "Pages.Master.Sales.MstSleDealerContractInfo.Create";
        public const string Pages_Master_Sales_Dealer_Contract_Info_Edit = "Pages.Master.Sales.MstSleDealerContractInfo.Edit";
        public const string Pages_Master_Sales_Dealer_Contract_Info_Delete = "Pages.Master.Sales.MstSleDealerContractInfo.Delete";
        public const string Pages_Master_Sales_Dealer_Contract_Info_Search = "Pages.Master.Sales.MstSleDealerContractInfo.Search";
        public const string Pages_Master_Sales_Dealer_Contract_Info_Edit_Dealer = "Pages.Master.Sales.MstSleDealerContractInfo.Edit.Dealer";
        #endregion

        #region "Delivery Plan Lead Time"
        public const string Pages_Master_Sales_Delivery_Plan_Lead_Time_Info = "Pages.Master.Sales.MstSleDeliveryPlanLeadTimeInfo";
        public const string Pages_Master_Sales_Delivery_Plan_Lead_Time_Info_Create = "Pages.Master.Sales.MstSleDeliveryPlanLeadTimeInfo.Create";
        public const string Pages_Master_Sales_Delivery_Plan_Lead_Time_Info_Edit = "Pages.Master.Sales.MstSleDeliveryPlanLeadTimeInfo.Edit";
        public const string Pages_Master_Sales_Delivery_Plan_Lead_Time_Info_Delete = "Pages.Master.Sales.MstSleDeliveryPlanLeadTimeInfo.Delete";
        public const string Pages_Master_Sales_Delivery_Plan_Lead_Time_Info_Search = "Pages.Master.Sales.MstSleDeliveryPlanLeadTimeInfo.Search";
        public const string Pages_Master_Sales_Delivery_Plan_Lead_Time_Info_Dealer = "Pages.Master.Sales.MstSleDeliveryPlanLeadTimeInfo.Edit.Dealer";
        #endregion

        #region "Day Off"
        public const string Pages_Master_Sales_Day_Off_Info = "Pages.Master.Sales.MstSleDayOffInfo";
        public const string Pages_Master_Sales_Day_Off_Info_Create = "Pages.Master.Sales.MstSleDayOffInfo.Create";
        public const string Pages_Master_Sales_Day_Off_Info_Edit = "Pages.Master.Sales.MstSleDayOffInfo.Edit";
        public const string Pages_Master_Sales_Day_Off_Info_Delete = "Pages.Master.Sales.MstSleDayOffInfo.Delete";
        public const string Pages_Master_Sales_Day_Off_Info_Search = "Pages.Master.Sales.MstSleDayOffInfo.Search";
        public const string Pages_Master_Sales_Day_Off_Info_Dealer = "Pages.Master.Sales.MstSleDayOffInfo.Edit.Dealer";
        #endregion

        #region "Logistic Company"
        public const string Pages_Master_Sales_Logistic_Company_Info = "Pages.Master.Sales.MstSleLogisticCompanyInfo";
        public const string Pages_Master_Sales_Logistic_Company_Info_Create = "Pages.Master.Sales.MstSleLogisticCompanyInfo.Create";
        public const string Pages_Master_Sales_Logistic_Company_Info_Edit = "Pages.Master.Sales.MstSleLogisticCompanyInfo.Edit";
        public const string Pages_Master_Sales_Logistic_Company_Info_Delete = "Pages.Master.Sales.MstSleLogisticCompanyInfo.Delete";
        public const string Pages_Master_Sales_Logistic_Company_Info_Search = "Pages.Master.Sales.MstSleLogisticCompanyInfo.Search";
        #endregion

        #region " Yard"
        public const string Pages_Master_Sales_Yard = "Pages.Master.Sales.MstSleYard";
        public const string Pages_Master_Sales_Yard_Create = "Pages.Master.Sales.MstSleYard.Create";
        public const string Pages_Master_Sales_Yard_Edit = "Pages.Master.Sales.MstSleYard.Edit";
        public const string Pages_Master_Sales_Yard_Delete = "Pages.Master.Sales.MstSleYard.Delete";
        public const string Pages_Master_Sales_Yard_Search = "Pages.Master.Sales.MstSleYard.Search";
        #endregion



        #region "Master Accessories"
        public const string Pages_Master_Sales_Accessories = "Pages.Master.Sales.MstSleAccessories";
        public const string Pages_Master_Sales_Accessories_Search = "Pages.Master.Sales.MstSleAccessories.Search";
        public const string Pages_Master_Sales_Accessories_Create = "Pages.Master.Sales.MstSleAccessories.Create";
        public const string Pages_Master_Sales_Accessories_Edit = "Pages.Master.Sales.MstSleAccessories.Edit";
        public const string Pages_Master_Sales_Accessories_Delete = "Pages.Master.Sales.MstSleAccessories.Delete";
        #endregion

        #region "Master Accessories Package"
        public const string Pages_Master_Sales_Accessories_Package = "Pages.Master.Sales.MstSleAccessoriesPackage";
        public const string Pages_Master_Sales_Accessories_Package_Search = "Pages.Master.Sales.MstSleAccessoriesPackage.Search";
        public const string Pages_Master_Sales_Accessories_Package_Create = "Pages.Master.Sales.MstSleAccessoriesPackage.Create";
        public const string Pages_Master_Sales_Accessories_Package_Edit = "Pages.Master.Sales.MstSleAccessoriesPackage.Edit";
        public const string Pages_Master_Sales_Accessories_Package_Delete = "Pages.Master.Sales.MstSleAccessoriesPackage.Delete";
        #endregion

        #region "Master Model"
        public const string Pages_Master_Sales_Model_Grade = "Pages.Master.Sales.MstSleModelGrade";
        public const string Pages_Master_Sales_Model_Grade_Search = "Pages.Master.Sales.MstSleModelGrade.Search";
        public const string Pages_Master_Sales_Model_Grade_Create = "Pages.Master.Sales.MstSleModelGrade.Create";
        public const string Pages_Master_Sales_Model_Grade_Edit = "Pages.Master.Sales.MstSleModelGrade.Edit";
        public const string Pages_Master_Sales_Model_Grade_Delete = "Pages.Master.Sales.MstSleModelGrade.Delete";
        #endregion

        #region "Master Contract Roles"
        public const string Pages_Master_Sales_Contract_Roles = "Pages.Master.Sales.MstSleSaleContractRoles";
        public const string Pages_Master_Sales_Contract_Roles_Create = "Pages.Master.Sales.MstSleSaleContractRoles.Create";
        public const string Pages_Master_Sales_Contract_Roles_Edit = "Pages.Master.Sales.MstSleSaleContractRoles.Edit";
        public const string Pages_Master_Sales_Contract_Roles_Delete = "Pages.Master.Sales.MstSleSaleContractRoles.Delete";
        public const string Pages_Master_Sales_Contract_Roles_Search = "Pages.Master.Sales.MstSleSaleContractRoles.Search";
        #endregion


        #region "ReasonOfNA"
        public const string Pages_Master_Sales_ReasonOfNA = "Pages.Master.Sales.MstSleReasonOfNA";
        public const string Pages_Master_Sales_ReasonOfNA_Create = "Pages.Master.Sales.MstSleReasonOfNA.Create";
        public const string Pages_Master_Sales_ReasonOfNA_Edit = "Pages.Master.Sales.MstSleReasonOfNA.Edit";
        public const string Pages_Master_Sales_ReasonOfNA_Delete = "Pages.Master.Sales.MstSleReasonOfNA.Delete";
        public const string Pages_Master_Sales_ReasonOfNA_Search = "Pages.Master.Sales.MstSleReasonOfNA.Search";
        #endregion

        #region "InsurancePurpose"
        public const string Pages_Master_Sales_InsurancePurpose = "Pages.Master.Sales.MstSleInsurancePurpose";
        public const string Pages_Master_Sales_InsurancePurpose_Create = "Pages.Master.Sales.MstSleInsurancePurpose.Create";
        public const string Pages_Master_Sales_InsurancePurpose_Edit = "Pages.Master.Sales.MstSleInsurancePurpose.Edit";
        public const string Pages_Master_Sales_InsurancePurpose_Delete = "Pages.Master.Sales.MstSleInsurancePurpose.Delete";
        public const string Pages_Master_Sales_InsurancePurpose_Search = "Pages.Master.Sales.MstSleInsurancePurpose.Search";
        #endregion

        #region "Bank"
        public const string Pages_Master_Sales_Bank = "Pages.Master.Sales.MstSleBank";
        public const string Pages_Master_Sales_Bank_Search = "Pages.Master.Sales.MstSleBank.Search";
        public const string Pages_Master_Sales_Bank_Create = "Pages.Master.Sales.MstSleBank.Create";
        public const string Pages_Master_Sales_Bank_Edit = "Pages.Master.Sales.MstSleBank.Edit";
        public const string Pages_Master_Sales_Bank_Delete = "Pages.Master.Sales.MstSleBank.Delete";
        #endregion

        #region "MstSleActiveAccesoryFinance"
        public const string Pages_Master_Sales_ActiveAccesoryFinance = "Pages.Master.Sales.ActiveAccesoryFinance";
        public const string Pages_Master_Sales_ActiveAccesoryFinance_Search = "Pages.Master.Sales.ActiveAccesoryFinance.Search";
        public const string Pages_Master_Sales_ActiveAccesoryFinance_Create = "Pages.Master.Sales.ActiveAccesoryFinance.Create";
        public const string Pages_Master_Sales_ActiveAccesoryFinance_Delete = "Pages.Master.Sales.ActiveAccesoryFinance.Delete";
        public const string Pages_Master_Sales_ActiveAccesoryFinance_Edit = "Pages.Master.Sales.ActiveAccesoryFinance.Edit";
        #endregion    
        #region "MstSleActiveInsuranceFinance"
        public const string Pages_Master_Sales_ActiveInsuranceFinance = "Pages.Master.Sales.ActiveInsuranceFinance";
        public const string Pages_Master_Sales_ActiveInsuranceFinance_Search = "Pages.Master.Sales.ActiveInsuranceFinance.Search";
        public const string Pages_Master_Sales_ActiveInsuranceFinance_Create = "Pages.Master.Sales.ActiveInsuranceFinance.Create";
        public const string Pages_Master_Sales_ActiveInsuranceFinance_Delete = "Pages.Master.Sales.ActiveInsuranceFinance.Delete";
        public const string Pages_Master_Sales_ActiveInsuranceFinance_Edit = "Pages.Master.Sales.ActiveInsuranceFinance.Edit";
        #endregion

        #region "PaymentTypeCommission"
        public const string Pages_Master_Sales_PaymentTypeCommission = "Pages.Master.Sales.MstSlePaymentTypeCommission";
        public const string Pages_Master_Sales_PaymentTypeCommission_Search = "Pages.Master.Sales.MstSlePaymentTypeCommission.Search";
        public const string Pages_Master_Sales_PaymentTypeCommission_Create = "Pages.Master.Sales.MstSlePaymentTypeCommission.Create";
        public const string Pages_Master_Sales_PaymentTypeCommission_Edit = "Pages.Master.Sales.MstSlePaymentTypeCommission.Edit";
        public const string Pages_Master_Sales_PaymentTypeCommission_Delete = "Pages.Master.Sales.MstSlePaymentTypeCommission.Delete";
        #endregion

        #region "ReasonOfNC"
        public const string Pages_Master_Sales_ReasonOfNC = "Pages.Master.Sales.MstSleReasonOfNC";
        public const string Pages_Master_Sales_ReasonOfNC_Create = "Pages.Master.Sales.MstSleReasonOfNC.Create";
        public const string Pages_Master_Sales_ReasonOfNC_Edit = "Pages.Master.Sales.MstSleReasonOfNC.Edit";
        public const string Pages_Master_Sales_ReasonOfNC_Delete = "Pages.Master.Sales.MstSleReasonOfNC.Delete";
        public const string Pages_Master_Sales_ReasonOfNC_Search = "Pages.Master.Sales.MstSleReasonOfNC.Search";
        #endregion

        #region "Master Finance Customer Type"
        public const string Pages_Master_Sales_FinanceCustomerType = "Pages.Master.Sales.FinanceCustomerType";
        public const string Pages_Master_Sales_FinanceCustomerType_Create = "Pages.Master.Sales.FinanceCustomerType.Create";
        public const string Pages_Master_Sales_FinanceCustomerType_Edit = "Pages.Master.Sales.FinanceCustomerType.Edit";
        public const string Pages_Master_Sales_FinanceCustomerType_Delete = "Pages.Master.Sales.FinanceCustomerType.Delete";
        public const string Pages_Master_Sales_FinanceCustomerType_Search = "Pages.Master.Sales.FinanceCustomerType.Search";
        #endregion

        #region "Insurance Compulsory Third Party Liability Fee"
        public const string Pages_Master_Sales_InsuranceCompulsory = "Pages.Master.Sales.InsuranceCompulsory";
        public const string Pages_Master_Sales_InsuranceCompulsory_Create = "Pages.Master.Sales.InsuranceCompulsory.Create";
        public const string Pages_Master_Sales_InsuranceCompulsory_Edit = "Pages.Master.Sales.InsuranceCompulsory.Edit";
        public const string Pages_Master_Sales_InsuranceCompulsory_Delete = "Pages.Master.Sales.InsuranceCompulsory.Delete";
        public const string Pages_Master_Sales_InsuranceCompulsory_Search = "Pages.Master.Sales.InsuranceCompulsory.Search";

        #endregion

        #region "Insurance Physical Damage Fee"
        public const string Pages_Master_Sales_InsurancePhysical = "Pages.Master.Sales.InsurancePhysical";
        public const string Pages_Master_Sales_InsurancePhysical_Create = "Pages.Master.Sales.InsurancePhysical.Create";
        public const string Pages_Master_Sales_InsurancePhysical_Edit = "Pages.Master.Sales.InsurancePhysical.Edit";
        public const string Pages_Master_Sales_InsurancePhysical_Delete = "Pages.Master.Sales.InsurancePhysical.Delete";
        public const string Pages_Master_Sales_InsurancePhysical_Search = "Pages.Master.Sales.InsurancePhysical.Search";

        #endregion

        #region "Insurance Voluntarily Third Party Liability Fee"
        public const string Pages_Master_Sales_InsuranceVoluntarily = "Pages.Master.Sales.InsuranceVoluntarily";
        public const string Pages_Master_Sales_InsuranceVoluntarily_Create = "Pages.Master.Sales.InsuranceVoluntarily.Create";
        public const string Pages_Master_Sales_InsuranceVoluntarily_Edit = "Pages.Master.Sales.InsuranceVoluntarily.Edit";
        public const string Pages_Master_Sales_InsuranceVoluntarily_Delete = "Pages.Master.Sales.InsuranceVoluntarily.Delete";
        public const string Pages_Master_Sales_InsuranceVoluntarily_Search = "Pages.Master.Sales.InsuranceVoluntarily.Search";

        #endregion

        #region "Insurance Term"
        public const string Pages_Master_Sales_InsuranceTerm = "Pages.Master.Sales.InsuranceTerm";
        public const string Pages_Master_Sales_InsuranceTerm_Create = "Pages.Master.Sales.InsuranceTerm.Create";
        public const string Pages_Master_Sales_InsuranceTerm_Edit = "Pages.Master.Sales.InsuranceTerm.Edit";
        public const string Pages_Master_Sales_InsuranceTerm_Delete = "Pages.Master.Sales.InsuranceTerm.Delete";
        public const string Pages_Master_Sales_InsuranceTerm_Search = "Pages.Master.Sales.InsuranceTerm.Search";

        #endregion

        #region "Product Tecnical Spec"
        public const string Pages_Master_Sales_ProductTechnicalSpec = "Pages.Master.Sales.ProductTechnicalSpec";
        public const string Pages_Master_Sales_ProductTechnicalSpec_Create = "Pages.Master.Sales.ProductTechnicalSpec.Create";
        public const string Pages_Master_Sales_ProductTechnicalSpec_Edit = "Pages.Master.Sales.ProductTechnicalSpec.Edit";
        public const string Pages_Master_Sales_ProductTechnicalSpec_Delete = "Pages.Master.Sales.ProductTechnicalSpec.Delete";
        public const string Pages_Master_Sales_ProductTechnicalSpec_Search = "Pages.Master.Sales.ProductTechnicalSpec.Search";
        #endregion

        #region "Product Tecnical Spec BigGroup"
        public const string Pages_Master_Sales_ProductTechnicalSpecBigGroup = "Pages.Master.Sales.ProductTechnicalSpecBigGroup";
        public const string Pages_Master_Sales_ProductTechnicalSpecBigGroup_Create = "Pages.Master.Sales.ProductTechnicalSpecBigGroup.Create";
        public const string Pages_Master_Sales_ProductTechnicalSpecBigGroup_Edit = "Pages.Master.Sales.ProductTechnicalSpecBigGroup.Edit";
        public const string Pages_Master_Sales_ProductTechnicalSpecBigGroup_Delete = "Pages.Master.Sales.ProductTechnicalSpecBigGroup.Delete";
        public const string Pages_Master_Sales_ProductTechnicalSpecBigGroup_Search = "Pages.Master.Sales.ProductTechnicalSpecBigGroup.Search";
        #endregion

        #region "Product Tecnical Spec Group"
        public const string Pages_Master_Sales_ProductTechnicalSpecGroup = "Pages.Master.Sales.ProductTechnicalSpecGroup";
        public const string Pages_Master_Sales_ProductTechnicalSpecGroup_Create = "Pages.Master.Sales.ProductTechnicalSpecGroup.Create";
        public const string Pages_Master_Sales_ProductTechnicalSpecGroup_Edit = "Pages.Master.Sales.ProductTechnicalSpecGroup.Edit";
        public const string Pages_Master_Sales_ProductTechnicalSpecGroup_Delete = "Pages.Master.Sales.ProductTechnicalSpecGroup.Delete";
        public const string Pages_Master_Sales_ProductTechnicalSpecGroup_Search = "Pages.Master.Sales.ProductTechnicalSpecGroup.Search";
        #endregion

        #region "Product Tecnical Spec Detail"
        public const string Pages_Master_Sales_ProductTechnicalSpecDetail = "Pages.Master.Sales.ProductTechnicalSpecDetail";
        public const string Pages_Master_Sales_ProductTechnicalSpecDetail_Create = "Pages.Master.Sales.ProductTechnicalSpecDetail.Create";
        public const string Pages_Master_Sales_ProductTechnicalSpecDetail_Edit = "Pages.Master.Sales.ProductTechnicalSpecDetail.Edit";
        public const string Pages_Master_Sales_ProductTechnicalSpecDetail_Delete = "Pages.Master.Sales.ProductTechnicalSpecDetail.Delete";
        public const string Pages_Master_Sales_ProductTechnicalSpecDetail_Search = "Pages.Master.Sales.ProductTechnicalSpecDetail.Search";
        #endregion

        #region "Product Tecnical Spec Color"
        public const string Pages_Master_Sales_ProductTechnicalSpecColor = "Pages.Master.Sales.ProductTechnicalSpecColor";
        public const string Pages_Master_Sales_ProductTechnicalSpecColor_Create = "Pages.Master.Sales.ProductTechnicalSpecColor.Create";
        public const string Pages_Master_Sales_ProductTechnicalSpecColor_Edit = "Pages.Master.Sales.ProductTechnicalSpecColor.Edit";
        public const string Pages_Master_Sales_ProductTechnicalSpecColor_Delete = "Pages.Master.Sales.ProductTechnicalSpecColor.Delete";
        public const string Pages_Master_Sales_ProductTechnicalSpecColor_Search = "Pages.Master.Sales.ProductTechnicalSpecColor.Search";
        #endregion
        #region "vehicle product email register"
        public const string Pages_Master_Sales_EmailRegister = "Pages.Master.Sales.EmailRegister";
        public const string Pages_Master_Sales_EmailRegister_Search = "Pages.Master.Sales.EmailRegister.Search";
        #endregion

        #region "vehicle product promotion"

        public const string Pages_Master_Sales_ProductPromotion = "Pages.Master.Sales.ProductPromotion";
        public const string Pages_Master_Sales_ProductPromotion_Create = "Pages.Master.Sales.ProductPromotion.Create";
        public const string Pages_Master_Sales_ProductPromotion_Edit = "Pages.Master.Sales.ProductPromotion.Edit";
        public const string Pages_Master_Sales_ProductPromotion_Delete = "Pages.Master.Sales.ProductPromotion.Delete";
        public const string Pages_Master_Sales_ProductPromotion_Search = "Pages.Master.Sales.ProductPromotion.Search";
        #endregion


        #region "vehicle product recruiment language "
        public const string Pages_Master_Sales_VehicleProductRecruimentLanguage = "Pages.Master.Sales.VehicleProductRecruimentLanguage";
        public const string Pages_Master_Sales_VehicleProductRecruimentLanguage_Create = "Pages.Master.Sales.VehicleProductRecruimentLanguage.Create";
        public const string Pages_Master_Sales_VehicleProductRecruimentLanguage_Edit = "Pages.Master.Sales.VehicleProductRecruimentLanguage.Edit";
        public const string Pages_Master_Sales_VehicleProductRecruimentLanguage_Delete = "Pages.Master.Sales.VehicleProductRecruimentLanguage.Delete";
        public const string Pages_Master_Sales_VehicleProductRecruimentLanguage_Search = "Pages.Master.Sales.VehicleProductRecruimentLanguage.Search";
        #endregion
        #region "father perrmissions"
        public const string Pages_Master_Sales_NewsPromotion = "Pages.Master.Sales.NewsPromotion";
        public const string Pages_Master_Sales_VehicleSpecifications = "Pages.Master.Sales.VehicleSpecifications";
        public const string Pages_Master_Sales_ImageCar = "Pages.Master.Sales.ImageCar";
        public const string Pages_Master_Sales_ToyotaVN = "Pages.Master.Sales.ToyotaVN";
        public const string Pages_Master_Sales_InsuranceToyota = "Pages.Master.Sales.InsuranceToyota";
        public const string Pages_Master_Sales_LocalizationAndExportFather = "Pages.Master.Sales.LocalizationAndExportFather";
        public const string Pages_Master_Sales_CsrAndTvf = "Pages.Master.Sales.CsrAndTvf";
        public const string Pages_Master_Sales_Maintenance = "Pages.Master.Sales.Maintenance";

        #endregion

        #region "Contact By"
        public const string Pages_Master_Sales_ContactBy = "Pages.Master.Sales.ContactBy";
        public const string Pages_Master_Sales_ContactBy_Create = "Pages.Master.Sales.ContactBy.Create";
        public const string Pages_Master_Sales_ContactBy_Edit = "Pages.Master.Sales.ContactBy.Edit";
        public const string Pages_Master_Sales_ContactBy_Delete = "Pages.Master.Sales.ContactBy.Delete";
        public const string Pages_Master_Sales_ContactBy_Search = "Pages.Master.Sales.ContactBy.Search";
        #endregion

        #region "Department"
        public const string Pages_Master_Sales_Department = "Pages.Master.Sales.MstSleDepartment";
        public const string Pages_Master_Sales_Department_Create = "Pages.Master.Sales.MstSleDepartment.Create";
        public const string Pages_Master_Sales_Department_Edit = "Pages.Master.Sales.MstSleDepartment.Edit";
        public const string Pages_Master_Sales_Department_Delete = "Pages.Master.Sales.MstSleDepartment.Delete";
        public const string Pages_Master_Sales_Department_Search = "Pages.Master.Sales.MstSleDepartment.Search";
        #endregion

        #region "Certificate"
        public const string Pages_Master_Sales_MstSleCertificate = "Pages.Master.Sales.MstSleCertificate";
        public const string Pages_Master_Sales_MstSleCertificate_Create = "Pages.Master.Sales.MstSleCertificate.Create";
        public const string Pages_Master_Sales_MstSleCertificate_Edit = "Pages.Master.Sales.MstSleCertificate.Edit";
        public const string Pages_Master_Sales_MstSleCertificate_Delete = "Pages.Master.Sales.MstSleCertificate.Delete";
        public const string Pages_Master_Sales_MstSleCertificate_Search = "Pages.Master.Sales.MstSleCertificate.Search";
        #endregion

        #region "ScoringRules"
        public const string Pages_Master_Sales_MstSleScoringRules = "Pages.Master.Sales.MstSleScoringRules";
        public const string Pages_Master_Sales_MstSleScoringRules_Create = "Pages.Master.Sales.MstSleScoringRules.Create";
        public const string Pages_Master_Sales_MstSleScoringRules_Edit = "Pages.Master.Sales.MstSleScoringRules.Edit";
        public const string Pages_Master_Sales_MstSleScoringRules_Delete = "Pages.Master.Sales.MstSleScoringRules.Delete";
        public const string Pages_Master_Sales_MstSleScoringRules_Search = "Pages.Master.Sales.MstSleScoringRules.Search";
        #endregion

        #region "Regulation"
        public const string Pages_Master_Sales_MstSleRegulation = "Pages.Master.Sales.MstSleRegulation";
        public const string Pages_Master_Sales_MstSleRegulation_Create = "Pages.Master.Sales.MstSleRegulation.Create";
        public const string Pages_Master_Sales_MstSleRegulation_Edit = "Pages.Master.Sales.MstSleRegulation.Edit";
        public const string Pages_Master_Sales_MstSleRegulation_Delete = "Pages.Master.Sales.MstSleRegulation.Delete";
        public const string Pages_Master_Sales_MstSleRegulation_Search = "Pages.Master.Sales.MstSleRegulation.Search";
        #endregion

        #region "ServiceLocation"
        public const string Pages_Master_Sales_MstSleServiceLocation = "Pages.Master.Sales.MstSleServiceLocation";
        public const string Pages_Master_Sales_MstSleServiceLocation_Create = "Pages.Master.Sales.MstSleServiceLocation.Create";
        public const string Pages_Master_Sales_MstSleServiceLocation_Edit = "Pages.Master.Sales.MstSleServiceLocation.Edit";
        public const string Pages_Master_Sales_MstSleServiceLocation_Delete = "Pages.Master.Sales.MstSleServiceLocation.Delete";
        public const string Pages_Master_Sales_MstSleServiceLocation_Search = "Pages.Master.Sales.MstSleServiceLocation.Search";
        #endregion

        #region "Master Insurance People In Car"
        public const string Pages_Master_Sales_InsurancePeopleInCar = "Pages.Master.Sales.MstSleInsurancePeopleInCar";
        public const string Pages_Master_Sales_InsurancePeopleInCar_Search = "Pages.Master.Sales.MstSleInsurancePeopleInCar.Search";
        public const string Pages_Master_Sales_InsurancePeopleInCar_Create = "Pages.Master.Sales.MstSleInsurancePeopleInCar.Create";
        public const string Pages_Master_Sales_InsurancePeopleInCar_Edit = "Pages.Master.Sales.MstSleInsurancePeopleInCar.Edit";
        public const string Pages_Master_Sales_InsurancePeopleInCar_Delete = "Pages.Master.Sales.MstSleInsurancePeopleInCar.Delete";
        #endregion

        #region "Master Vehicle Product Maintenance Level"
        public const string Pages_Master_Sales_VehicleProductMaintenanceLevel = "Pages.Master.Sales.MstSleVehicleProductMaintenanceLevel";
        public const string Pages_Master_Sales_VehicleProductMaintenanceLevel_Search = "Pages.Master.Sales.MstSleVehicleProductMaintenanceLevel.Search";
        public const string Pages_Master_Sales_VehicleProductMaintenanceLevel_Create = "Pages.Master.Sales.MstSleVehicleProductMaintenanceLevel.Create";
        public const string Pages_Master_Sales_VehicleProductMaintenanceLevel_Edit = "Pages.Master.Sales.MstSleVehicleProductMaintenanceLevel.Edit";
        public const string Pages_Master_Sales_VehicleProductMaintenanceLevel_Delete = "Pages.Master.Sales.MstSleVehicleProductMaintenanceLevel.Delete";
        #endregion

        #region "Master Vehicle Product Maintenance By Model"
        public const string Pages_Master_Sales_VehicleProductMaintenanceByModel = "Pages.Master.Sales.MstSleVehicleProductMaintenanceByModel";
        public const string Pages_Master_Sales_VehicleProductMaintenanceByModel_Search = "Pages.Master.Sales.MstSleVehicleProductMaintenanceByModel.Search";
        public const string Pages_Master_Sales_VehicleProductMaintenanceByModel_Create = "Pages.Master.Sales.MstSleVehicleProductMaintenanceByModel.Create";
        public const string Pages_Master_Sales_VehicleProductMaintenanceByModel_Edit = "Pages.Master.Sales.MstSleVehicleProductMaintenanceByModel.Edit";
        public const string Pages_Master_Sales_VehicleProductMaintenanceByModel_Delete = "Pages.Master.Sales.MstSleVehicleProductMaintenanceByModel.Delete";
        #endregion

        #region "Cert Type"
        public const string Pages_Master_Sales_MstSleCertType = "Pages.Master.Sales.MstSleCertType";
        public const string Pages_Master_Sales_MstSleCertType_Create = "Pages.Master.Sales.MstSleCertType.Create";
        public const string Pages_Master_Sales_MstSleCertType_Edit = "Pages.Master.Sales.MstSleCertType.Edit";
        public const string Pages_Master_Sales_MstSleCertType_Delete = "Pages.Master.Sales.MstSleCertType.Delete";
        public const string Pages_Master_Sales_MstSleCertType_Search = "Pages.Master.Sales.MstSleCertType.Search";
        #endregion

        #region "Cert"
        public const string Pages_Master_Sales_MstSleCert = "Pages.Master.Sales.MstSleCert";
        public const string Pages_Master_Sales_MstSleCert_Create = "Pages.Master.Sales.MstSleCert.Create";
        public const string Pages_Master_Sales_MstSleCert_Edit = "Pages.Master.Sales.MstSleCert.Edit";
        public const string Pages_Master_Sales_MstSleCert_Delete = "Pages.Master.Sales.MstSleCert.Delete";
        public const string Pages_Master_Sales_MstSleCert_Search = "Pages.Master.Sales.MstSleCert.Search";
        #endregion

        #endregion

        #region "Sales Business"

        #region "Admin Column"
        public const string Pages_Sales_SalesAdminColumn = "Pages.Sales.SalesAdminColumn";
        public const string Pages_Sales_SalesAdminColumn_Create = "Pages.Sales.SalesAdminColumn.Create";
        public const string Pages_Sales_SalesAdminColumn_Edit = "Pages.Sales.SalesAdminColumn.Edit";
        public const string Pages_Sales_SalesAdminColumn_Delete = "Pages.Sales.SalesAdminColumn.Delete";
        public const string Pages_Sales_SalesAdminColumn_Search = "Pages.Sales.SalesAdminColumn.Search";
        #endregion

        #region "Admin Group"
        public const string Pages_Sales_SalesAdminGroup = "Pages.Sales.SalesAdminGroup";
        public const string Pages_Sales_SalesAdminGroup_Create = "Pages.Sales.SalesAdminGroup.Create";
        public const string Pages_Sales_SalesAdminGroup_Edit = "Pages.Sales.SalesAdminGroup.Edit";
        public const string Pages_Sales_SalesAdminGroup_Delete = "Pages.Sales.SalesAdminGroup.Delete";
        public const string Pages_Sales_SalesAdminGroup_Search = "Pages.Sales.SalesAdminGroup.Search";
        #endregion

        #region "Admin Group Column"
        public const string Pages_Sales_SalesAdminGroupColumn = "Pages.Sales.SalesAdminGroupColumn";
        public const string Pages_Sales_SalesAdminGroupColumn_Create = "Pages.Sales.SalesAdminGroupColumn.Create";
        public const string Pages_Sales_SalesAdminGroupColumn_Edit = "Pages.Sales.SalesAdminGroupColumn.Edit";
        public const string Pages_Sales_SalesAdminGroupColumn_Delete = "Pages.Sales.SalesAdminGroupColumn.Delete";
        public const string Pages_Sales_SalesAdminGroupColumn_Search = "Pages.Sales.SalesAdminGroupColumn.Search";
        #endregion

        #region "Admin Display Role"
        public const string Pages_Sales_SalesAdminRole = "Pages.Sales.SalesAdminDisplayRole";
        public const string Pages_Sales_SalesAdminRole_Edit = "Pages.Sales.SalesAdminDisplayRole.Edit";
        public const string Pages_Sales_SalesAdminRole_Search = "Pages.Sales.SalesAdminDisplayRole.Search";
        public const string Pages_Sales_SalesAdminRole_Delete = "Pages.Sales.SalesAdminDisplayRole.Delete";
        #endregion

        #endregion

        #region "Sale Contract"
        public const string Pages_Sales_Menu_Contract = "Pages.Sales.Contract";
        #endregion

        #region "Sale Customer"
        public const string Pages_Sales_Menu_Customer = "Pages.Sales.Customer";
        #endregion

        #region "Mst Sle Audio"
        public const string Pages_Sales_MstSleAudio = "Pages.Sales.Vehicle.MstSleAudio";
        public const string Pages_Sales_MstSleAudio_Create = "Pages.Sales.Vehicle.MstSleAudio.Create";
        public const string Pages_Sales_MstSleAudio_Edit = "Pages.Sales.Vehicle.MstSleAudio.Edit";
        public const string Pages_Sales_MstSleAudio_Delete = "Pages.Sales.Vehicle.MstSleAudio.Delete";
        public const string Pages_Sales_MstSleAudio_Search = "Pages.Sales.Vehicle.MstSleAudio.Search";
        #endregion


        #region Đề xuất bán hàng
        public const string Pages_Sales_Proposal_Menu = "Pages.Sales.Proposal.Menu";
        public const string Pages_Sales_Proposal_Main = "Pages.Sales.Proposal.Menu.Main";
        public const string Pages_Sales_Proposal_Policy = "Pages.Sales.Proposal.Menu.Policy";
        public const string Pages_Sales_Proposal_Approve = "Pages.Sales.Proposal.Menu.Approve";
        #endregion
    }
}