﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace tmss.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            #region "Web"

            // Quản lý khách hàng
            #region "Menu Customer"
            var customer = pages.CreateChildPermission(AppPermissions.Pages_Sales_Menu_Customer, L("SalesMenuCustomerPermission"));
            #region "Customer"
            var salesCustomer = customer.CreateChildPermission(AppPermissions.Pages_SalesCustomer, L("SalesCustomerPermissionWeb"));
            salesCustomer.CreateChildPermission(AppPermissions.Pages_SalesCustomer_Create, L("CreatePermission"));
            salesCustomer.CreateChildPermission(AppPermissions.Pages_SalesCustomer_View, L("ViewPermission"));
            salesCustomer.CreateChildPermission(AppPermissions.Pages_SalesCustomer_Edit, L("EditPermission"));
            salesCustomer.CreateChildPermission(AppPermissions.Pages_SalesCustomer_Delete, L("Delete"));
            salesCustomer.CreateChildPermission(AppPermissions.Pages_SalesCustomer_View_Duplicate_Customer, L("SalesCustomerViewDuplicateCustomer"));
            #endregion

            #region "Customer vehicle info"
            var salesCustomerVehicleInfo = customer.CreateChildPermission(AppPermissions.Pages_SalesCustomerVehicleInfo, L("SalesCustomerVehicleInfo"));
            salesCustomerVehicleInfo.CreateChildPermission(AppPermissions.Pages_SalesCustomerVehicleInfo_Create, L("CreateNewSalesCustomerVehicleInfo"));
            salesCustomerVehicleInfo.CreateChildPermission(AppPermissions.Pages_SalesCustomerVehicleInfo_Edit, L("EditSalesCustomerVehicleInfo"));
            salesCustomerVehicleInfo.CreateChildPermission(AppPermissions.Pages_SalesCustomerVehicleInfo_Delete, L("DeleteSalesCustomerVehicleInfo"));
            #endregion

            #endregion

            // Quản lý nhân viên
            #region Nhân viên bán hàng
            var salesPerson = pages.CreateChildPermission(AppPermissions.Pages_Sales_Menu_Person, L("SalesMenuPersonPermission"));
            salesPerson.CreateChildPermission(AppPermissions.Pages_SalesPerson_Create, L("CreatePermission"));
            salesPerson.CreateChildPermission(AppPermissions.Pages_SalesPerson_View, L("ViewPermission"));
            salesPerson.CreateChildPermission(AppPermissions.Pages_SalesPerson_Edit, L("EditPermission"));
            salesPerson.CreateChildPermission(AppPermissions.Pages_SalesPerson_Delete, L("Delete"));
            #endregion

            //Danh mục dùng chung
            #region "Master"
            var master = pages.CreateChildPermission(AppPermissions.Pages_Master, L("MasterPermission"));

            #region "MasterCommon"
            var masterCommon = master.CreateChildPermission(AppPermissions.Pages_Master_Common, L("MasterCommonPermission"));

            #region "Master Dealer"
            var mstGenDealers = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Common_MstGenDealers, L("MstGenDealersPermission"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenDealers_Search, L("ViewPermission"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenDealers_Create, L("CreatePermission"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenDealers_Edit, L("EditPermission"));
            mstGenDealers.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenDealers_Delete, L("Delete"));
            #endregion

            #region "Master Dealer Group"
            var mstSleDealerGroups = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleDealerGroups, L("MstSleDealerGroupsPermission"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleDealerGroups_Search, L("ViewPermission"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleDealerGroups_Create, L("CreatePermission"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleDealerGroups_Edit, L("EditPermission"));
            mstSleDealerGroups.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleDealerGroups_Delete, L("Delete"));
            #endregion

            #region "Master Province"
            var mstGenProvinces = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenProvinces, L("MstGenProvincesPermission"));
            mstGenProvinces.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenProvinces_Search, L("ViewPermission"));
            mstGenProvinces.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenProvinces_Create, L("CreatePermission"));
            mstGenProvinces.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenProvinces_Edit, L("EditPermission"));
            mstGenProvinces.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstGenProvinces_Delete, L("Delete"));
            #endregion

            #region "Master District"
            var mstSleDistrict = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_District, L("MstSleDistrictPermission"));
            mstSleDistrict.CreateChildPermission(AppPermissions.Pages_Master_Sales_District_Search, L("ViewPermission"));
            mstSleDistrict.CreateChildPermission(AppPermissions.Pages_Master_Sales_District_Create, L("CreatePermission"));
            mstSleDistrict.CreateChildPermission(AppPermissions.Pages_Master_Sales_District_Edit, L("EditPermission"));
            mstSleDistrict.CreateChildPermission(AppPermissions.Pages_Master_Sales_District_Delete, L("Delete"));
            #endregion

            #region "Source"
            var source = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Source, L("MstSleSourcePermission"));
            source.CreateChildPermission(AppPermissions.Pages_Master_Sales_Source_Create, L("CreatePermission"));
            source.CreateChildPermission(AppPermissions.Pages_Master_Sales_Source_Edit, L("EditPermission"));
            source.CreateChildPermission(AppPermissions.Pages_Master_Sales_Source_Delete, L("Delete"));
            source.CreateChildPermission(AppPermissions.Pages_Master_Sales_Source_Search, L("ViewPermission"));
            #endregion

            #region "Hotness"
            var hotness = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hotness, L("MstSleHotnessPermission"));
            hotness.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hotness_Search, L("ViewPermission"));
            hotness.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hotness_Create, L("CreatePermission"));
            hotness.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hotness_Edit, L("EditPermission"));
            hotness.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hotness_Delete, L("Delete"));
            #endregion

            #region "Excepted Delivery Timing"
            var exceptedDeliveryTiming = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming, L("MstSleExpectedDelTimingBannerPermission"));
            exceptedDeliveryTiming.CreateChildPermission(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Search, L("ViewPermission"));
            exceptedDeliveryTiming.CreateChildPermission(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Create, L("CreatePermission"));
            exceptedDeliveryTiming.CreateChildPermission(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Edit, L("EditPermission"));
            exceptedDeliveryTiming.CreateChildPermission(AppPermissions.Pages_Master_Sales_ExceptedDeliveryTiming_Delete, L("Delete"));
            #endregion

            #region "Payment Type"
            var paymentType = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentType, L("MstSlePaymentTypePermission"));
            paymentType.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentType_Search, L("ViewPermission"));
            paymentType.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentType_Create, L("CreatePermission"));
            paymentType.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentType_Edit, L("EditPermission"));
            paymentType.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentType_Delete, L("Delete"));
            #endregion

            #region "Role"
            var Role = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Role, L("MstSleRolePermission"));
            Role.CreateChildPermission(AppPermissions.Pages_Master_Sales_Role_Create, L("CreatePermission"));
            Role.CreateChildPermission(AppPermissions.Pages_Master_Sales_Role_Edit, L("EditPermission"));
            Role.CreateChildPermission(AppPermissions.Pages_Master_Sales_Role_Delete, L("Delete"));
            Role.CreateChildPermission(AppPermissions.Pages_Master_Sales_Role_Search, L("ViewPermission"));
            #endregion

            #region "Gender"
            var gender = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Gender, L("MstSleGenderPermission"));
            gender.CreateChildPermission(AppPermissions.Pages_Master_Sales_Gender_Create, L("CreatePermission"));
            gender.CreateChildPermission(AppPermissions.Pages_Master_Sales_Gender_Edit, L("EditPermission"));
            gender.CreateChildPermission(AppPermissions.Pages_Master_Sales_Gender_Delete, L("Delete"));
            gender.CreateChildPermission(AppPermissions.Pages_Master_Sales_Gender_Search, L("ViewPermission"));
            #endregion

            #region "Marital Status"
            var maritalStatus = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_MaritalStatus, L("MstSleMaritalStatusPermission"));
            maritalStatus.CreateChildPermission(AppPermissions.Pages_Master_Sales_MaritalStatus_Create, L("CreatePermission"));
            maritalStatus.CreateChildPermission(AppPermissions.Pages_Master_Sales_MaritalStatus_Edit, L("EditPermission"));
            maritalStatus.CreateChildPermission(AppPermissions.Pages_Master_Sales_MaritalStatus_Delete, L("Delete"));
            maritalStatus.CreateChildPermission(AppPermissions.Pages_Master_Sales_MaritalStatus_Search, L("ViewPermission"));
            #endregion

            #region "Occupation"
            var occupation = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Occupation, L("MstSleOccupationPermission"));
            occupation.CreateChildPermission(AppPermissions.Pages_Master_Sales_Occupation_Search, L("ViewPermission"));
            occupation.CreateChildPermission(AppPermissions.Pages_Master_Sales_Occupation_Create, L("CreatePermission"));
            occupation.CreateChildPermission(AppPermissions.Pages_Master_Sales_Occupation_Edit, L("EditPermission"));
            occupation.CreateChildPermission(AppPermissions.Pages_Master_Sales_Occupation_Delete, L("Delete"));
            #endregion

            #region "Hobby"
            var hobby = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hobby, L("MstSleHobbyPermission"));
            hobby.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hobby_Search, L("ViewPermission"));
            hobby.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hobby_Create, L("CreatePermission"));
            hobby.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hobby_Edit, L("EditPermission"));
            hobby.CreateChildPermission(AppPermissions.Pages_Master_Sales_Hobby_Delete, L("Delete"));
            #endregion

            #region "Soucre Of Info"
            var sourceOfInfo = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_SourceOfInfo, L("MstSleSourceOfInfoPermission"));
            sourceOfInfo.CreateChildPermission(AppPermissions.Pages_Master_Sales_SourceOfInfo_Search, L("ViewPermission"));
            sourceOfInfo.CreateChildPermission(AppPermissions.Pages_Master_Sales_SourceOfInfo_Create, L("CreatePermission"));
            sourceOfInfo.CreateChildPermission(AppPermissions.Pages_Master_Sales_SourceOfInfo_Edit, L("EditPermission"));
            sourceOfInfo.CreateChildPermission(AppPermissions.Pages_Master_Sales_SourceOfInfo_Delete, L("Delete"));
            #endregion

            #region "Social Channel"
            var socialChannel = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_SocialChannel, L("MstSleSocialChannelPermission"));
            socialChannel.CreateChildPermission(AppPermissions.Pages_Master_Sales_SocialChannel_Search, L("ViewPermission"));
            socialChannel.CreateChildPermission(AppPermissions.Pages_Master_Sales_SocialChannel_Create, L("CreatePermission"));
            socialChannel.CreateChildPermission(AppPermissions.Pages_Master_Sales_SocialChannel_Edit, L("EditPermission"));
            socialChannel.CreateChildPermission(AppPermissions.Pages_Master_Sales_SocialChannel_Delete, L("Delete"));
            #endregion

            #region "Customer Social Channel"
            var CustomerSocialChannel = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_CustomerSocialChannel, L("MstSleCustomerSocialChannelPermission"));
            CustomerSocialChannel.CreateChildPermission(AppPermissions.Pages_Master_CustomerSocialChannel_Search, L("ViewPermission"));
            CustomerSocialChannel.CreateChildPermission(AppPermissions.Pages_Master_CustomerSocialChannel_Create, L("CreatePermission"));
            CustomerSocialChannel.CreateChildPermission(AppPermissions.Pages_Master_CustomerSocialChannel_Edit, L("EditPermission"));
            CustomerSocialChannel.CreateChildPermission(AppPermissions.Pages_Master_CustomerSocialChannel_Delete, L("Delete"));
            #endregion

            #region "Company"
            var Company = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Company, L("MstSleCompanyPermission"));
            Company.CreateChildPermission(AppPermissions.Pages_Master_Sales_Company_Create, L("CreatePermission"));
            Company.CreateChildPermission(AppPermissions.Pages_Master_Sales_Company_Edit, L("EditPermission"));
            Company.CreateChildPermission(AppPermissions.Pages_Master_Sales_Company_Delete, L("Delete"));
            Company.CreateChildPermission(AppPermissions.Pages_Master_Sales_Company_Search, L("ViewPermission"));
            #endregion

            #region "Purpose"
            var Purpose = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Purpose, L("MstSlePurposePermission"));
            Purpose.CreateChildPermission(AppPermissions.Pages_Master_Sales_Purpose_Create, L("CreatePermission"));
            Purpose.CreateChildPermission(AppPermissions.Pages_Master_Sales_Purpose_Edit, L("EditPermission"));
            Purpose.CreateChildPermission(AppPermissions.Pages_Master_Sales_Purpose_Delete, L("Delete"));
            Purpose.CreateChildPermission(AppPermissions.Pages_Master_Sales_Purpose_Search, L("ViewPermission"));
            #endregion

            #region "FAR"
            var FAR = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_FAR, L("MstSleFARPermission"));
            FAR.CreateChildPermission(AppPermissions.Pages_Master_Sales_FAR_Create, L("CreatePermission"));
            FAR.CreateChildPermission(AppPermissions.Pages_Master_Sales_FAR_Edit, L("EditPermission"));
            FAR.CreateChildPermission(AppPermissions.Pages_Master_Sales_FAR_Delete, L("Delete"));
            FAR.CreateChildPermission(AppPermissions.Pages_Master_Sales_FAR_Search, L("ViewPermission"));
            #endregion

            #region "Business Type"
            var BusinessType = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_BusinessType, L("MstSleBusinessTypePermission"));
            BusinessType.CreateChildPermission(AppPermissions.Pages_Master_Sales_BusinessType_Create, L("CreatePermission"));
            BusinessType.CreateChildPermission(AppPermissions.Pages_Master_Sales_BusinessType_Edit, L("EditPermission"));
            BusinessType.CreateChildPermission(AppPermissions.Pages_Master_Sales_BusinessType_Delete, L("Delete"));
            BusinessType.CreateChildPermission(AppPermissions.Pages_Master_Sales_BusinessType_Search, L("ViewPermission"));
            #endregion

            #region "Finance Customer Type"
            var financeCustomerType = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_FinanceCustomerType, L("MstSleFinanceCustomerTypePermission"));
            financeCustomerType.CreateChildPermission(AppPermissions.Pages_Master_Sales_FinanceCustomerType_Create, L("CreatePermission"));
            financeCustomerType.CreateChildPermission(AppPermissions.Pages_Master_Sales_FinanceCustomerType_Edit, L("EditPermission"));
            financeCustomerType.CreateChildPermission(AppPermissions.Pages_Master_Sales_FinanceCustomerType_Delete, L("Delete"));
            financeCustomerType.CreateChildPermission(AppPermissions.Pages_Master_Sales_FinanceCustomerType_Search, L("ViewPermission"));
            #endregion

            #region "PaymentTypeCommission"
            var PaymentTypeCommission = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentTypeCommission, L("MstSlePaymentTypeCommissionPermission"));
            PaymentTypeCommission.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentTypeCommission_Search, L("ViewPermission"));
            PaymentTypeCommission.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentTypeCommission_Create, L("CreatePermission"));
            PaymentTypeCommission.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentTypeCommission_Edit, L("EditPermission"));
            PaymentTypeCommission.CreateChildPermission(AppPermissions.Pages_Master_Sales_PaymentTypeCommission_Delete, L("Delete"));
            #endregion

            #region "Contract Roles"
            var contractRoles = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Contract_Roles, L("MstSleSaleContractRolePermission"));
            contractRoles.CreateChildPermission(AppPermissions.Pages_Master_Sales_Contract_Roles_Create, L("CreatePermission"));
            contractRoles.CreateChildPermission(AppPermissions.Pages_Master_Sales_Contract_Roles_Edit, L("EditPermission"));
            contractRoles.CreateChildPermission(AppPermissions.Pages_Master_Sales_Contract_Roles_Delete, L("Delete"));
            contractRoles.CreateChildPermission(AppPermissions.Pages_Master_Sales_Contract_Roles_Search, L("ViewPermission"));
            #endregion

            #region "ReasonOfChange"
            var ReasonOfChange = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfChange, L("MstSleReasonOfChangePermission"));
            ReasonOfChange.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfChange_Create, L("CreatePermission"));
            ReasonOfChange.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfChange_Edit, L("EditPermission"));
            ReasonOfChange.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfChange_Delete, L("Delete"));
            ReasonOfChange.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfChange_Search, L("ViewPermission"));
            #endregion

            #region "SalesStage"
            var SalesStage = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_SalesStage, L("MstSleSalesStagePermission"));
            SalesStage.CreateChildPermission(AppPermissions.Pages_Master_Sales_SalesStage_Create, L("CreatePermission"));
            SalesStage.CreateChildPermission(AppPermissions.Pages_Master_Sales_SalesStage_Edit, L("EditPermission"));
            SalesStage.CreateChildPermission(AppPermissions.Pages_Master_Sales_SalesStage_Delete, L("Delete"));
            SalesStage.CreateChildPermission(AppPermissions.Pages_Master_Sales_SalesStage_Search, L("ViewPermission"));
            #endregion

            #region "Contact By"
            var contactBy = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ContactBy, L("MstSleContactByPermission"));
            contactBy.CreateChildPermission(AppPermissions.Pages_Master_Sales_ContactBy_Create, L("CreatePermission"));
            contactBy.CreateChildPermission(AppPermissions.Pages_Master_Sales_ContactBy_Edit, L("EditPermission"));
            contactBy.CreateChildPermission(AppPermissions.Pages_Master_Sales_ContactBy_Delete, L("Delete"));
            contactBy.CreateChildPermission(AppPermissions.Pages_Master_Sales_ContactBy_Search, L("ViewPermission"));
            #endregion

            #region "ReasonOfNA"
            var ReasonOfNA = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNA, L("MstSleReasonOfNAPermission"));
            ReasonOfNA.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNA_Create, L("CreatePermission"));
            ReasonOfNA.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNA_Edit, L("EditPermission"));
            ReasonOfNA.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNA_Delete, L("Delete"));
            ReasonOfNA.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNA_Search, L("ViewPermission"));
            #endregion

            #region "ReasonOfNC"
            var ReasonOfNC = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNC, L("MstSleReasonOfNCPermission"));
            ReasonOfNC.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNC_Create, L("CreatePermission"));
            ReasonOfNC.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNC_Edit, L("EditPermission"));
            ReasonOfNC.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNC_Delete, L("Delete"));
            ReasonOfNC.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfNC_Search, L("ViewPermission"));
            #endregion

            #region "ReasonOfLost"
            var ReasonOfLost = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfLost, L("MstSleReasonOfLostPermission"));
            ReasonOfLost.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfLost_Create, L("CreatePermission"));
            ReasonOfLost.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfLost_Edit, L("EditPermission"));
            ReasonOfLost.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfLost_Delete, L("Delete"));
            ReasonOfLost.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfLost_Search, L("ViewPermission"));
            #endregion


            #region "ReasonOfFreeze"
            var reasonOfFreeze = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfFreeze, L("MstSleReasonOfFreeze"));
            reasonOfFreeze.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Create, L("CreatePermission"));
            reasonOfFreeze.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Edit, L("EditPermission"));
            reasonOfFreeze.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Delete, L("Delete"));
            reasonOfFreeze.CreateChildPermission(AppPermissions.Pages_Master_Sales_ReasonOfFreeze_Search, L("ViewPermission"));
            #endregion

            #region "Road tax Period"
            var period = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Period, L("MstsleRoadTaxPeriodPermission"));
            period.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Period_Search, L("ViewPermission"));
            period.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Period_Create, L("CreatePermission"));
            period.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Period_Edit, L("EditPermission"));
            period.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Period_Delete, L("Delete"));
            #endregion

            #region "Road tax Amount"
            var amount = masterCommon.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Amount, L("MstsleRoadTaxAmountPermission"));
            amount.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Amount_Search, L("ViewPermission"));
            amount.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Amount_Create, L("CreatePermission"));
            amount.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Amount_Edit, L("EditPermission"));
            amount.CreateChildPermission(AppPermissions.Pages_Master_Sales_Road_Tax_Amount_Delete, L("Delete"));
            #endregion

            #endregion

            #region "Vehicle Info"
            var masterVehicle = master.CreateChildPermission(AppPermissions.Pages_Master_Vehicle_Info, L("MasterVehiclePermission"));
            #region "Make Competitor"
            var makeCompetitor = masterVehicle.CreateChildPermission(AppPermissions.Pages_Master_Sales_Make_Competitor, L("MstSleMakeCompetitorPermission"));
            makeCompetitor.CreateChildPermission(AppPermissions.Pages_Master_Sales_Make_Competitor_Create, L("CreatePermission"));
            makeCompetitor.CreateChildPermission(AppPermissions.Pages_Master_Sales_Make_Competitor_Edit, L("EditPermission"));
            makeCompetitor.CreateChildPermission(AppPermissions.Pages_Master_Sales_Make_Competitor_Delete, L("Delete"));
            makeCompetitor.CreateChildPermission(AppPermissions.Pages_Master_Sales_Make_Competitor_Search, L("ViewPermission"));
            #endregion

            #region "Master Model"
            var mstSleModel = masterVehicle.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_Grade, L("MstSleModelPermission"));
            mstSleModel.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_Grade_Search, L("ViewPermission"));
            mstSleModel.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_Grade_Create, L("CreatePermission"));
            mstSleModel.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_Grade_Edit, L("EditPermission"));
            mstSleModel.CreateChildPermission(AppPermissions.Pages_Master_Sales_Model_Grade_Delete, L("Delete"));
            #endregion

            #region "ColorByGrade"
            var colorGrade = masterVehicle.CreateChildPermission(AppPermissions.Pages_Master_Sales_Color_By_Grade, L("MstSleColorByGradePermission"));
            colorGrade.CreateChildPermission(AppPermissions.Pages_Master_Sales_Color_By_Grade_Delete, L("Delete"));
            colorGrade.CreateChildPermission(AppPermissions.Pages_Master_Sales_Color_By_Grade_Create, L("CreatePermission"));
            colorGrade.CreateChildPermission(AppPermissions.Pages_Master_Sales_Color_By_Grade_Edit, L("EditPermission"));
            colorGrade.CreateChildPermission(AppPermissions.Pages_Master_Sales_Color_By_Grade_Search, L("ViewPermission"));
            #endregion

            #region "Color"
            var color = masterVehicle.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleColors, L("MstSleColorPermission"));
            color.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleColors_Create, L("CreatePermission"));
            color.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleColors_Edit, L("EditPermission"));
            color.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleColors_Delete, L("Delete"));
            color.CreateChildPermission(AppPermissions.Pages_Master_Sales_MstSleColors_Search, L("ViewPermission"));
            #endregion
            #endregion


            #endregion

            //Giao diện mẫu demo
            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Unlock, L("Unlock"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_Menu, L("OrganizationUnitsMenu"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageRoles, L("ManagingRoles"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var webhooks = administration.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription, L("Webhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Create, L("CreatingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Edit, L("EditingWebhooks"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_ChangeActivity, L("ChangingWebhookActivity"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_WebhookSubscription_Detail, L("DetailingSubscription"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ListSendAttempts, L("ListingSendAttempts"));
            webhooks.CreateChildPermission(AppPermissions.Pages_Administration_Webhook_ResendWebhook, L("ResendingWebhook"));

            var dynamicParameters = administration.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters, L("DynamicParameters"));
            dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Create, L("CreatingDynamicParameters"));
            dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Edit, L("EditingDynamicParameters"));
            dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameters_Delete, L("DeletingDynamicParameters"));

            var dynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue, L("DynamicParameterValue"));
            dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Create, L("CreatingDynamicParameterValue"));
            dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Edit, L("EditingDynamicParameterValue"));
            dynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_DynamicParameterValue_Delete, L("DeletingDynamicParameterValue"));

            var entityDynamicParameters = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters, L("EntityDynamicParameters"));
            entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Create, L("CreatingEntityDynamicParameters"));
            entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Edit, L("EditingEntityDynamicParameters"));
            entityDynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameters_Delete, L("DeletingEntityDynamicParameters"));

            var entityDynamicParameterValues = dynamicParameters.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue, L("EntityDynamicParameterValue"));
            entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Create, L("CreatingEntityDynamicParameterValue"));
            entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Edit, L("EditingEntityDynamicParameterValue"));
            entityDynamicParameterValues.CreateChildPermission(AppPermissions.Pages_Administration_EntityDynamicParameterValue_Delete, L("DeletingEntityDynamicParameterValue"));

            //TENANT-SPECIFIC PERMISSIONS


            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_MoveTenantsToAnotherEdition, L("MoveTenantsToAnotherEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);



            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);

            #endregion
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, tmssConsts.LocalizationSourceName);
        }
    }

}