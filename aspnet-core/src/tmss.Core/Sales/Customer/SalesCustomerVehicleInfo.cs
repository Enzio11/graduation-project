﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomerVehicleInfo")]
    public class SalesCustomerVehicleInfo : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }

        [Column(TypeName = "numeric(20, 0)")]
        public virtual long? SalesCustomerId { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? FARId { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? PurposeId { get; set; }

        [Column(TypeName = "numeric(15, 0)")]
        public virtual long? MakeId { get; set; }
        public virtual string OtherMake { get; set; }

        [Column(TypeName = "numeric(15, 0)")]
        public virtual long? ModelId { get; set; }
        public virtual string OtherModel { get; set; }

        [Column(TypeName = "numeric(15, 0)")]
        public virtual long? GradeId { get; set; }
        public virtual string OtherGrade { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public virtual long? ColorId { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public virtual long? InteriorColorId { get; set; }
        public virtual bool? IsFavourite { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public virtual long? ReasonOfChangeId { get; set; }
        public virtual string OtherReasonToChange { get; set; }

        public virtual int? VehicleType { get; set; }

        public virtual int? YearOfPurchaseDate { get; set; }
        public virtual int? OtherYearOfPurchaseDate { get; set; }

        [MaxLength(255)]
        public virtual string ReasonToChangeVehicle { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfNATDId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfNADId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfNA1kId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfNAFinanceId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfNAInsuranceId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? PromotionId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? BankId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? InsuranceId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? PackageId { get; set; }
        public bool? IsSellUsedCar { get; set; }
    }
}
