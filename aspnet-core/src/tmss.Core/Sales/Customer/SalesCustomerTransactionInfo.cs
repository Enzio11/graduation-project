﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomerTransactionInfo")]
    public class SalesCustomerTransactionInfo : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        public virtual int TenantId { set; get; }
        [Column(TypeName = "numeric(20, 0)")]
        public virtual long? SalesCustomerId { set; get; }
        [Column(TypeName = "numeric(20, 0)")]
        public virtual long? SalesStageId { set; get; }
        public virtual DateTime? ActivityDate { set; get; }
        [MaxLength(255)]
        public virtual string ActivityName { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ActivityStatusId { get; set; }
        [MaxLength(255)]
        public virtual string ReasonLost { get; set; }
        public virtual string ReasonOfLostOther { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfLostId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? ReasonOfNCId { get; set; }
        [MaxLength(255)]
        public string ReasonOfNCOther { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfLostTypeId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? DeliveryPlaceId { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? RouteId { get; set; }
        public virtual long? SupporterId { get; set; }
        public virtual bool? IsShowRoomVisit { get; set; }
        public virtual DateTime? ShowRoomVisitDate { get; set; }
        public virtual DateTime? TestDriveDate { get; set; }
        public virtual bool? IsTestDrive { get; set; }
        [MaxLength(255)]
        public virtual string ReasonWhyNotTD { get; set; }
        [MaxLength(255)]
        public virtual string Note { get; set; }

        // start fields for approve transaction
        public long? RequestUserId { get; set; }
        public long? ApproverId { get; set; }
        public int? StatusApprove { get; set; }
        public bool? ContactStatus { get; set; }
        public bool? OnPassiveFreezing { get; set; }
        public long? PreTransactionId { get; set; }
        // end fields for approve transaction

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ReasonOfFreezeId { get; set; }
    }
}
