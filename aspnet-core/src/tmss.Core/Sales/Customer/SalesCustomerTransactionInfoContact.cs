﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomerTransactionInfoContact")]
    public class SalesCustomerTransactionInfoContact : FullAuditedEntity<long>, IEntity<long>
    {
        public int? TenantId { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? SalesCustomerId { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? ContactById { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? CurrentActionId { get; set; }
        public DateTime? CurrentDate { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? NextActionId { get; set; }
        public DateTime? NextDate { get; set; }
        public string Note { get; set; }
        public string CurrentAction { get; set; }
        public string NextAction { get; set; }
        public bool NextActionStatus { get; set; }

        public long? isLatest { get; set; }
        public IList<SalesCustomerManagerComment> ManagerComments { get; set; }
    }
}
