﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales.Customer
{
    [Table("SalesCustomerHotness")]
    public class SalesCustomerHotness : FullAuditedEntity<long>, IEntity<long>
    {
        [Column("HotnessId", TypeName = "numeric(15, 0)")]
        public long? HotnessId { get; set; }
        [Column("CustomerId", TypeName = "numeric(15, 0)")]
        public long? CustomerId { get; set; }
        public DateTime? DateChange { get; set; }
    }
}
