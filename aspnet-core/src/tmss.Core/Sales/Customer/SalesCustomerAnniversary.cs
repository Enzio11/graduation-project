﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomerAnniversary")]
    public class SalesCustomerAnniversary : FullAuditedEntity<long>, IEntity<long>
    {
        public virtual long? SalesCustomerId { get; set; }
        [MaxLength(255)]
        public string AnniversaryContent { get; set; }
        public DateTime? AnniversaryDate { get; set; }
        public DateTime? ContactTime { get; set; }
        public SalesCustomer SalesCustomer { get; set; }
    }
}
