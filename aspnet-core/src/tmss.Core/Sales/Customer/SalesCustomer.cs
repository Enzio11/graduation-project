﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomer")]
    public class SalesCustomer : FullAuditedEntity<long>, IEntity<long>, IMustHaveTenant
    {
        [Column(TypeName = "numeric(18, 0)")]
        public virtual long? SalesPersonId { get; set; }
        public virtual long? AssignPersonId { get; set; }
        public virtual DateTime? AssignDate { get; set; }

        public virtual int TenantId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? DealerId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? SourceId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? CustomerTypeId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ProvinceId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? DistrictId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? HotnessId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? ExpectedDelTimingId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? PaymentTypeId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? GenderId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? MaritalStatusId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public long? OccupationId { set; get; }
        [MaxLength(50)]
        public string No { set; get; }

        [Column(TypeName = "numeric(18, 2)")]
        public virtual long? Income { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? HobbyId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? SourceOfInfoId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? FARId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? PurposeId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? AgeRangeId { set; get; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? RelationshipId { set; get; }

        [Column(TypeName = "numeric(18, 0)")]
        public virtual long? Status { set; get; }

        [Column(TypeName = "bigint")]
        public virtual long? ReferenceId { set; get; }

        [MaxLength(255)]
        public virtual string CustomerName { set; get; }

        [MaxLength(255)]
        public virtual string CustomerPermanentAddress { set; get; }

        [MaxLength(255)]
        public virtual string CustomerTemporaryAddress { set; get; }

        [MaxLength(50)]
        public virtual string Telephone { set; get; }

        [MaxLength(50)]
        public virtual string TaxCode { set; get; }

        [MaxLength(50)]
        public virtual string IdCardNo { set; get; }

        [MaxLength(50)]
        public virtual string Fax { set; get; }

        [MaxLength(512)]
        public virtual string Remark { set; get; }

        [MaxLength(512)]
        public virtual string Email { set; get; }

        public DateTime? BirthDay { set; get; }

        [MaxLength(800)]
        public virtual string CompanyName { set; get; }

        [MaxLength(500)]
        public virtual string BusinessField { set; get; }

        public DateTime? ContactDate { get; set; }
        [MaxLength(50)]
        [Column("Mr/Mrs")]
        public string MrMrs { set; get; }

        [MaxLength(255)]
        [Column("Food/Drink")]
        public string FoodDrink { set; get; }

        [MaxLength(255)]
        public virtual string ContactName { set; get; }

        [MaxLength(255)]
        public virtual string Details { set; get; }

        [MaxLength(255)]
        public virtual string Owner { set; get; }

        [MaxLength(85)]
        public virtual string CustomerLastName { get; set; }

        [MaxLength(85)]
        public virtual string CustomerFirstName { get; set; }

        [MaxLength(85)]
        public virtual string CustomerMidName { get; set; }

        [MaxLength(20)]
        public virtual string CustomerTel1 { get; set; }

        [MaxLength(20)]
        public virtual string CustomerTel2 { get; set; }

        [Column(TypeName = "numeric(5, 0)")]
        public virtual long? BusinessTypeId { get; set; }

        [MaxLength(85)]
        public virtual string RepresLastName { get; set; }

        [MaxLength(85)]
        public virtual string RepresFirstName { get; set; }

        [MaxLength(85)]
        public virtual string RepresMidName { get; set; }

        [MaxLength(85)]
        public virtual string Title { get; set; }

        [Column(TypeName = "numeric(5, 0)")]
        public virtual long? RoleId { get; set; }

        [MaxLength(20)]
        public virtual string CompanyTel { get; set; }

        [MaxLength(255)]
        public virtual string CompanyBussinessArea { get; set; }

        [Column(TypeName = "numeric(5, 0)")]
        public virtual long? CompanySizeId { get; set; }

        [Column(TypeName = "numeric(15, 0)")]
        public virtual decimal? YearlyRevenue { get; set; }

        [MaxLength(255)]
        public virtual string CompanyWebsite { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? CompanySocialId1Type { get; set; }

        [MaxLength(50)]
        public virtual string CompanySocialId1Name { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public virtual long? CompanySocialId2Type { get; set; }

        [MaxLength(50)]
        public virtual string CompanySocialId2Name { get; set; }

        [Column(TypeName = "numeric(5, 0)")]
        public virtual long? CompanyMaritalStatusId { get; set; }

        [Column(TypeName = "numeric(5, 0)")]
        public virtual long? CompanyFavoriteId { get; set; }

        public virtual DateTime? CompanySpecialDate1 { set; get; }

        [MaxLength(255)]
        public virtual string CompanyNote1 { get; set; }

        public virtual DateTime? CompanySpecialDate2 { set; get; }

        [MaxLength(255)]
        public virtual string CompanyNote2 { get; set; }

        [MaxLength(255)]
        public virtual string CompanyRemark { get; set; }

        public int? ContactResult { get; set; }
        public virtual string RoleOther { get; set; }
        public virtual string MaritalStatusOther { get; set; }
        public virtual string OccupationOther { get; set; }
        public virtual string BusinessTypeOther { get; set; }
        public virtual string HobbyOther { get; set; }
        public virtual string SourceOfInfoOther { get; set; }
        public virtual DateTime? Date { get; set; }
        public virtual DateTime? DateNeedContactHWC { get; set; }
        public virtual bool? ResultContactHWC { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public virtual long? Time { set; get; }
        public virtual long? CusAppId { set; get; }
        public virtual int? ImportSource { set; get; }
        public virtual long? IsDuplicatePhone { set; get; }
        public virtual bool? IsWarning { set; get; }
        public virtual bool? IsSendNotificationHW { set; get; }
        [MaxLength(500)]
        public string ManagerComment { get; set; }

        [MaxLength(50)]
        public virtual string CustomerTel1Decode { get; set; }
        [MaxLength(500)]
        public virtual string Campaign { get; set; }
        public virtual int? SalesCustomerUIOId { get; set; }
        public virtual int? ReferenceCusId { get; set; }
        public virtual string CustomerPlaceIssuance { get; set; }
        public virtual DateTime? CustomerDateIssuance { get; set; }
        [MaxLength(25)]
        public virtual string AgencyLead { get; set; }
        public virtual bool? IsUIOTransferLatest { get; set; }

    }
}
