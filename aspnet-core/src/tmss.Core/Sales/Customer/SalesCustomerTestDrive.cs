﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomerTestDrive")]
    public class SalesCustomerTestDrive : FullAuditedEntity<long>, IEntity<long>
    {
        public int TenantId { set; get; }

        [Column(TypeName = "numeric(20, 0)")]
        public long? SalesCustomerId { get; set; }

        [Column(TypeName = "numeric(20, 0)")]
        public long? SalesCustomerTransactionInfoId { get; set; }

        public DateTime? TestDriveDate { get; set; }

        public decimal? TestDriveTime { get; set; }

        public long? SupporterId { get; set; }
    }
}
