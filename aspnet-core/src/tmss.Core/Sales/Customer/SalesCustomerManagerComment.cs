﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesCustomerManagerComment")]
    public class SalesCustomerManagerComment : FullAuditedEntity<long>, IEntity<long>
    {
        public int? TenantId { get; set; }
        [Column(TypeName = "numeric(20, 0)")]
        public long? SalesCustomerId { get; set; }
        [Column(TypeName = "numeric(20, 0)")]
        public long? SalesCustomerTransactionInfoContactId { get; set; }
        [MaxLength(1000)]
        public string SeekContentForManager { get; set; }
        public DateTime? SeekManagerTime { get; set; }
        public long? ParentId { get; set; }
    }
}
