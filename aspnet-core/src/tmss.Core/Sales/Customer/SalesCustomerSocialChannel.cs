﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Customer
{
    [Table("SalesCustomerSocialChannel")]
    public class SalesCustomerSocialChannel : FullAuditedEntity<long>, IEntity<long>
    {
        public long SalesCustomerId { get; set; }

        public long SocialChannelId { get; set; }
        public string SocialChannelOther { get; set; }

        [MaxLength(255)]
        public string SocialId { get; set; }
    }
}
