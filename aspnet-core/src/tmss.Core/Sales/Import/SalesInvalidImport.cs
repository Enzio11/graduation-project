﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales
{
    [Table("SalesInvalidImport")]
    public class SalesInvalidImport : FullAuditedEntity<long>, IEntity<long>, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        public int? UserId { get; set; }
        [StringLength(256)]
        public string Type { get; set; }
        [Column(TypeName = "numeric(1, 0)")]
        public decimal? Status { get; set; }
        public int? TotalCount { get; set; }
        public int? InvalidCount { get; set; }
        public int? ImportedCount { get; set; }
        [StringLength(256)]
        public string Source { get; set; }
    }
}