﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.Sales.Import
{
    [Table("SalesJobMonitor")]
    public class SalesJobMonitor : FullAuditedEntity<long>, IEntity<long>, IMayHaveTenant
    {
        public int? TenantId { get; set; }
        [StringLength(100)]
        public string Code { get; set; }
    }
}
