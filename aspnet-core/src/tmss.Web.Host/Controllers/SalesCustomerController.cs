﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using tmss.Sales.Import;
using tmss.Storage;

namespace tmss.Web.Controllers
{
    public class SalesCustomerController : SalesCustomerControllerBase
    {
        public SalesCustomerController(
            IBinaryObjectManager binaryObjectManager, 
            IBackgroundJobManager backgroundJobManager,
            IRepository<SalesJobMonitor, long> salesJobMonitorRepository
        )
        : base(binaryObjectManager, backgroundJobManager, salesJobMonitorRepository) { }
    }
}
