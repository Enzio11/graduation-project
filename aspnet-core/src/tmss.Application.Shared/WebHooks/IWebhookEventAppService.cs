﻿using Abp.Webhooks;
using System.Threading.Tasks;

namespace tmss.WebHooks
{
    public interface IWebhookEventAppService
    {
        Task<WebhookEvent> Get(string id);
    }
}
