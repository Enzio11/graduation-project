using Abp.Application.Services.Dto;

namespace tmss.Organizations.Dto
{
    public class OrganizationUnitDto : AuditedEntityDto<long>
    {
        public long? ParentId { get; set; }

        public string Code { get; set; }

        public string DisplayName { get; set; }

        public int MemberCount { get; set; }

        public int RoleCount { get; set; }

        public string Description { get; set; }

        public long? ManagerId { get; set; }

        public bool IsBP { get; set; }

        public bool IsActive { get; set; }

        public byte Ordering { get; set; }
    }
}