﻿namespace tmss.Organizations.Dto
{
    public class GetListUserInGroupDto
    {
        public long USerId { get; set; }
        public string UserName { get; set; }
    }
}
