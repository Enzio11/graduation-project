﻿using System.Collections.Generic;

namespace tmss.Organizations.Dto
{
    public class GetAllOrgAndUser
    {
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public List<GetListUserInGroupDto> ListUserInGroup { get; set; }
    }
}
