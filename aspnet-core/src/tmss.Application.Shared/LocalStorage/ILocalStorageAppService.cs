﻿using Abp.Application.Services;
using System.Threading.Tasks;
using tmss.LocalStorage.Dto;

namespace tmss.LocalStorage
{
    public interface ILocalStorageAppService : IApplicationService
    {
        Task<PickList> GetLocalStorage();
    }
}
