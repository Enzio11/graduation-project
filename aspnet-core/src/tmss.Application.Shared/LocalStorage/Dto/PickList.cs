﻿using System.Collections.Generic;
using tmss.MstSle.Dto.LookUp;
using tmss.MstSle.IMstSleDistrict.Dto;

namespace tmss.LocalStorage.Dto
{
    public class PickList
    {
        public List<GetCodeDistrictDto> District { get; set; }
        public List<GetDistrictByProviceIdDto> Province { get; set; }
        public List<MstSleLookupDto> LookUp { get; set; }
    }
}
