﻿using Abp.Application.Services;
using System.Threading.Tasks;
using tmss.Authorization.Accounts.Dto;

namespace tmss.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<int?> ResolveTenantId(ResolveTenantIdInput input);

        Task<RegisterOutput> Register(RegisterInput input);

        Task SendPasswordResetCode(SendPasswordResetCodeInput input);
        Task<long> SendPasswordResetCodeMobile(SendPasswordResetCodeMobileInput input);

        Task<ResetPasswordOutput> ResetPassword(ResetPasswordInput input);

        Task<ResetPasswordOutput> ResetPasswordMobile(ResetPasswordMobileInput input);

        Task SendEmailActivationLink(SendEmailActivationLinkInput input);

        Task ActivateEmail(ActivateEmailInput input);

        Task<ImpersonateOutput> Impersonate(ImpersonateInput input);

        Task<ImpersonateOutput> BackToImpersonator();

        Task<SwitchToLinkedAccountOutput> SwitchToLinkedAccount(SwitchToLinkedAccountInput input);
    }
}
