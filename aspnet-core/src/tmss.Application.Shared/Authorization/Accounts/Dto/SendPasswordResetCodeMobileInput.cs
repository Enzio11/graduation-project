﻿namespace tmss.Authorization.Accounts.Dto
{
    public class SendPasswordResetCodeMobileInput
    {
        public string UserNameOrEmailAddress { get; set; }
    }
}
