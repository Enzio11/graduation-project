﻿using Abp.Auditing;

namespace tmss.Authorization.Accounts.Dto
{
    public class ResetPasswordMobileInput
    {
        public long UserId { get; set; }

        public string ResetCode { get; set; }

        [DisableAuditing]
        public string Password { get; set; }

    }
}
