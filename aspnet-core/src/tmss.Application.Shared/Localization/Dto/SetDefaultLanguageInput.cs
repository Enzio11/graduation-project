﻿using Abp.Localization;
using System.ComponentModel.DataAnnotations;

namespace tmss.Localization.Dto
{
    public class SetDefaultLanguageInput
    {
        [Required]
        [StringLength(ApplicationLanguage.MaxNameLength)]
        public virtual string Name { get; set; }
    }
}