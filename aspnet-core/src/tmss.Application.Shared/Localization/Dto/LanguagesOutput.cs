﻿using System.Collections.Generic;

namespace tmss.Localization.Dto
{
    public class LanguagesOutput
    {
        public LanguagesOutput(Dictionary<string, string> en_us, Dictionary<string, string> vi_vn)
        {
            this.en_us = en_us;
            this.vi_vn = vi_vn;
        }

        public Dictionary<string, string> en_us { get; set; }
        public Dictionary<string, string> vi_vn { get; set; }
    }
}
