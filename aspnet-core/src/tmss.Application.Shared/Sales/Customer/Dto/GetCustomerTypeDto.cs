﻿using Abp.Application.Services.Dto;

namespace tmss.Sales.Customer.Dto
{
    public class GetCustomerTypeDto : EntityDto<long?>
    {
        public string Code { get; set; }
        public string DescriptionEng { get; set; }
        public string DescriptionVN { get; set; }
    }
}
