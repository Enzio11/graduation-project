﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerTestDriveDto : EntityDto<long?>
    {
        public DateTime? TestDriveDate { get; set; }
        public long? SupporterId { get; set; }
        public long? PlaceId { get; set; }
        public long? RouteId { get; set; }
        public long? VehicleId { get; set; }
        public long? StallId { get; set; }
        public long? SalesCustomerId { get; set; }
        public string TestDrivePlace { get; set; }
        public string TestDriveRoute { get; set; }
        public string TestDriveVehicle { get; set; }
        public string Supporter { get; set; }

    }
}
