﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetFilterAssignmentCustomerDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public long? FARId { set; get; }
        public long? SalesPersonId { set; get; }
        public long? AssignPersonId { set; get; }
        public long? HotnessId { set; get; }
        public long? SourceId { set; get; }
        public int? ImportSourceId { set; get; }
        public long? DistrictId { set; get; }
        public long? ProvinceId { set; get; }
        public string CustomerName { get; set; }
        public long? CustomerTypeId { get; set; }
        public int? DealerId { set; get; }
        public long? ModelId { set; get; }
        public DateTime? CustomerDropDateFrom { get; set; }
        public DateTime? CustomerDropDateTo { get; set; }
        public DateTime? CreationDateFrom { get; set; }
        public DateTime? CreationDateTo { get; set; }
        public DateTime? AssignDateFrom { get; set; }
        public DateTime? AssignDateTo { get; set; }
        public string CustomerTel1 { get; set; }
        public int? IsContacted { get; set; }
        public string Campaign { get; set; }
        public long? SalesStageId { set; get; }
        public string AgencyLead { set; get; }
    }
}