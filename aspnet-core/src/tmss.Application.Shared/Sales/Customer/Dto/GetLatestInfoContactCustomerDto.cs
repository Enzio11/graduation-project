﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace tmss.Sales.Customer.Dto
{
    public class GetLatestInfoContactCustomerDto : EntityDto<long?>
    {
        // Thông tin KH
        public SalesCustomerInfomationDto SalesCustomerInfo { get; set; }
        // Thông tin Liên hệ
        public SalesCustomersContactInfoDto SalesCustomersContactInfo { get; set; }
        // Thông tin xe quan tâm của Toyota
        public List<GetListCarAttentionDto> CarAttention { get; set; }
        // Thông tin lái thử xe
        public SalesCustomerTestDriveDto TestDrive { get; set; }
    }
}
