﻿using Abp.Application.Services.Dto;

namespace tmss.Sales.Customer.Dto
{
    public class InputSalesCustomerPotentialDto : PagedAndSortedResultRequestDto
    {
        public string Format { get; set; }
        public long? Department { set; get; }
        public long? SalesPerson { set; get; }
        public long? Status { set; get; }
        public long? Source { get; set; }
        public long? CustomerType { set; get; }
    }
}
