﻿using Abp.Application.Services.Dto;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerCarAttentionDto : EntityDto<long?>
    {
        public long? MakeId { get; set; }
        public long? ModelId { get; set; }
        public long? GradeId { get; set; }
        public long? ColorId { get; set; }
        public long? FARId { get; set; }
        public long? PurposeId { get; set; }
        public string ReasonToChangeVehicle { get; set; }
        public long? SalesCustomerId { get; set; }
        public bool? IsFavourite { get; set; }
    }
}
