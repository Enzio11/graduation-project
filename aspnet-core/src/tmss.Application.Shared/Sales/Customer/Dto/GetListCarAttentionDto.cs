﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetListCarAttentionDto
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public string Grade { get; set; }
        public string Far { get; set; }
        public string Color { get; set; }
        public DateTime? InvoiceDate { get; set; }

    }
}
