﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerContactInfoDto : EntityDto<long?>
    {
        public long? ContactById { get; set; }
        public DateTime? CurrentDate { get; set; }
        public DateTime? NextDate { get; set; }
        public string CurrentAction { get; set; }
        public string NextAction { get; set; }
        public bool NextActionStatus { get; set; }
        public DateTime? CreationTime { get; set; }
        public long? SalesCustomerId { get; set; }
    }
}
