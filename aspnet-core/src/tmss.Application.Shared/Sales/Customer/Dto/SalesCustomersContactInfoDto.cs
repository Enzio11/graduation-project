﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomersContactInfoDto
    {
        public DateTime? CurrentDate { get; set; }
        public string CurrentAction { get; set; }
        public DateTime? NextContact { get; set; }
        public string NextPlan { get; set; }
        public string ContactBy { get; set; }
    }
}
