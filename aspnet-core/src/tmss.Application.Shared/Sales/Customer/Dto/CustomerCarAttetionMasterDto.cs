﻿using System.Collections.Generic;
using tmss.MstSle.Color;
using tmss.MstSle.IMstSleGrade.Dto;
using tmss.MstSle.IMstSleModel.Dto;
using tmss.MstSle.Make;

namespace tmss.Sales.Customer.Dto
{
    public class CustomerCarAttetionMasterDto
    {
        public List<GetMakeForSalesCustomerDto> Makes { get; set; }
        public List<GetModelForSalesCustomerDto> Models { get; set; }
        public List<ListGradeCodeDto> Grades { get; set; }
        public List<GetColorByGradeDto> Colors { get; set; }
        public List<GetMakeForSalesCustomerDto> Fars { get; set; }
        public List<GetMakeForSalesCustomerDto> Purposes { get; set; }
        public List<GetMakeForSalesCustomerDto> ContactBy { get; set; }
        public List<GetMakeForSalesCustomerDto> Hotnesses { get; set; }
        public List<GetMakeForSalesCustomerDto> Hobbies { get; set; }
        public List<GetMakeForSalesCustomerDto> SourceOfInfos { get; set; }
        public List<GetMakeForSalesCustomerDto> Occupations { get; set; }
        public List<GetMakeForSalesCustomerDto> SalesStages { get; set; }
        public List<GetMakeForSalesCustomerDto> Sources { get; set; }
        public List<ListExpectedDelTimingForSalesCustomer> ExpectedDelTimings { get; set; }
    }
}
