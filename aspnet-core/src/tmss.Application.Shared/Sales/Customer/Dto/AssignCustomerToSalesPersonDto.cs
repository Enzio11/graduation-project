﻿using System.Collections.Generic;

namespace tmss.Sales.Customer.Dto
{
    public class AssignCustomerToSalesPersonDto
    {
        public List<int?> SalesPersonIds { get; set; }
        public List<int?> CustomerIds { get; set; }
    }
}
