﻿using System;
using System.Collections.Generic;
namespace tmss.Sales.Customer.Dto
{
    public class AssignmentListInput
    {
        public long UserId { get; set; }
        public bool? IsView { get; set; }
        public bool? IsManager { get; set; }
        public int SkipCount { get; set; }
        public int MaxResultCount { get; set; }
        public string Filter { get; set; }
        public int? ContactResult { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public long? SourceFilterId { get; set; }
        public bool BySelfCustomerDate { get; set; }
        public List<long> ModelID { get; set; }
        public bool IsShowDuplicate { get; set; }

    }
}
