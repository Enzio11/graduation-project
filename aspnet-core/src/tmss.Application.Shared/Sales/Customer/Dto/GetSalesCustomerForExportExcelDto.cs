﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetSalesCustomerForExportExcelDto : EntityDto<long?>
    {
        public string No { get; set; }
        public string Dealer { get; set; }
        public DateTime? FirstContactDate { get; set; }
        public string CustomerType { get; set; }
        public DateTime? CreationTime { get; set; }
        public string SalesPerson { get; set; }
        public string Name { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Remark { get; set; }
        public string Hotness { get; set; }
        public string Source { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }
        public string SalesStage { get; set; }
        public DateTime? CurrentDate { get; set; }
        public string CurrentAction { get; set; }
        public string ContactType { get; set; }
        public DateTime? NextContact { get; set; }
        public string NextPlan { get; set; }
        public bool? IsShowRoomVisit { get; set; }
        public bool? IsTestDrive { get; set; }
        public string ReasonWhyNotTD { get; set; }
        public DateTime? TestDriveDate { get; set; }
        public DateTime? SigningDate { get; set; }
        public string ExpectedDelTiming { get; set; }
        public string CustomerTel1 { get; set; }
        public string CompanyTel { get; set; }
        public string ManagerComment { get; set; }
        public DateTime? LostCustomerTime { get; set; }
        public DateTime? SignDate { get; set; }
        public long? CustomerTypeId { get; set; }
        public string ContractNo { get; set; }
        public string CarAttention { get; set; }
        public string CarPurchase { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Grade { get; set; }
        public string Color { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Far { get; set; }
        public string Department { get; set; }
        public int? YearOfPurchase { get; set; }
        public DateTime? FreezeDate { get; set; }
        public string ReasonLost { get; set; }

        public SalesCustomerTestDriveDto TestDrive { get; set; }

    }
}
