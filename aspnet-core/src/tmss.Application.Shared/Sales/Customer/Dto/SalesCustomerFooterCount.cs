﻿namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerFooterCount
    {
        public int TotalInquiryCustomer { get; set; }
        public int TotalCarAttention { get; set; }
    }
}
