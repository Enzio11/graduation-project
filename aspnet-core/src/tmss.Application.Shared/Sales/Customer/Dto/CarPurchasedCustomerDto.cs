﻿namespace tmss.Sales.Customer.Dto
{
    public class CarPurchasedCustomerDto
    {
        public string Make { get; set; }
        public string ModelPurchase { get; set; }
        public string GradePurchase { get; set; }
        public int? YearOfPurchaseDate { get; set; }
    }
}
