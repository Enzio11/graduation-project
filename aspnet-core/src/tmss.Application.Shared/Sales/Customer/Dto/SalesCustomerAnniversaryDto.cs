﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerAnniversaryDto : EntityDto<long?>
    {
        public DateTime? Date { get; set; }
        public string Anniversary { get; set; }
    }
}
