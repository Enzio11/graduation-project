﻿namespace tmss.Sales.Customer.Dto.Api
{
    public class AssignmentCustomerContactResultDto
    {
        public long SalesCustomerId { get; set; }
        public int? ContactResult { get; set; }
        public string ReasonLost { get; set; }
        public long ReasonOfFreezeId { get; set; }
        public long ReasonOfNCId { get; set; }
        public string ReasonOfNCOther { get; set; }
        public long ReasonOfLostId { get; set; }
        public bool IsMoveFreeze { get; set; }
    }
}
