﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetSalesListInput
    {
        public long UserId { get; set; }
        public string Filter { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int SkipCount { get; set; }
        public int MaxResultCount { get; set; }
        public bool? IsMine { get; set; }
        public long? UserFilterId { get; set; }
        public long? ModelFilterId { get; set; }
        public long? SourceFilterId { get; set; }
    }
}
