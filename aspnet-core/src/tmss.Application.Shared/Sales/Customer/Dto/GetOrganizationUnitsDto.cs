﻿namespace tmss.Sales.Customer.Dto
{
    public class GetOrganizationUnitsDto
    {
        public long Id { get; set; }
        public string DisplayName { get; set; }
    }
}
