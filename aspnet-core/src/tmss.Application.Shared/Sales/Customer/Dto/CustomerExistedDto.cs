﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class CustomerExistedDto
    {
        public long CustomerId { get; set; }
        public string SalesPersonName { get; set; }
        public DateTime? ContactDate { get; set; }
    }
}
