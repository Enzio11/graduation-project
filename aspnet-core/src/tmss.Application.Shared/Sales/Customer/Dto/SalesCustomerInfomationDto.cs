﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerInfomationDto : EntityDto<long?>
    {
        public string CustomerNo { get; set; }
        public string Name { get; set; }
        public string SalesPerson { get; set; }
        public string Status { set; get; }
        public DateTime? ContactDate { get; set; }
        public string Dealer { get; set; }
        public string CustomerType { get; set; }
        public DateTime? FirstContactDate { get; set; }
        public DateTime? CreationTime { get; set; }
        public long? ProvinceId { get; set; }
        public long? DistrictId { get; set; }
        public string Address { get; set; }
        public string Hotness { get; set; }
        public string Source { get; set; }
        public string CompanyName { get; set; }
        public string BusinessType { get; set; }
        public bool? IsShowRoomVisit { get; set; }
        public bool? IsTestDrive { get; set; }
        public string ReasonWhyNotTD { get; set; }
        public string ExpectedDelTiming { get; set; }
        public DateTime? TestDriveDate { get; set; }
        public string CustomerTel1 { get; set; }
        public bool? FreezeType { get; set; }

        // Thông tin Contact
        public DateTime? CurrentDate { get; set; }
        public string CurrentAction { get; set; }
        public string ContactRemark { get; set; }
        public DateTime? NextContact { get; set; }
        public string NextPlan { get; set; }
        public string ContactBy { get; set; }

        public string ManagerComment { get; set; }
        public string Department { get; set; }
        public long? CustomerTypeId { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public DateTime? LostCustomerTime { get; set; }
        public DateTime? SignDate { get; set; }
        public string ContractNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public long? SalesPersonId { get; set; }
        public long? StatusId { get; set; }

        public string ReasonLost { get; set; }
        public DateTime? ReasonLostDate { get; set; }
        public int TotalCarPurchaseRequest { get; set; }
        public DateTime? FreezeDate { get; set; }
    }
}
