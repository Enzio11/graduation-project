﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesTransferCustomerForViewDto : EntityDto<long?>
    {
        public string No { get; set; }
        public DateTime? CreationTime { get; set; }
        public DateTime? Date { get; set; }
        public long? Time { get; set; }
        public long? ModelId { get; set; }
        public string Model { get; set; }
        public long? SourceId { get; set; }
        public string Source { get; set; }
        public long? HotnessId { get; set; }
        public long? SalesPersonId { get; set; }
        public long? AssignPersonId { get; set; }
        public string Hotness { get; set; }
        public long? DealerId { get; set; }
        public string Dealer { get; set; }
        public string CustomerName { get; set; }
        public string CustomerTel1 { get; set; }
        public string Email { get; set; }
        public long? CustomerId { get; set; }
        public DateTime? ContactDate { get; set; }
        public DateTime? AssignDate { get; set; }
        public string ContactResult { get; set; }
        public string SalesPerson { get; set; }
        public string AssignPerson { get; set; }
        public string CustomerTime { get; set; }
        public string ExpectedDelTiming { get; set; }
        public string Campaign { get; set; }
        public string CarAttention { get; set; }
        public string Status { get; set; }
        public long? StatusId { get; set; }
        public string ReasonOfFailContact { get; set; }
        public string AgencyLead { get; set; }
    }
}
