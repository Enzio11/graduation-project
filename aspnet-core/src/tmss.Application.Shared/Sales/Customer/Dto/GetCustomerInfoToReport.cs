﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetCustomerInfoToReport : EntityDto<long?>
    {
        public DateTime? Date { get; set; }
        public String FirstContactDate { get; set; }
        public virtual long? Time { set; get; }
        public virtual int? CountNC { set; get; }

        public virtual string TimeString { set; get; }
        public virtual string ModelCode { get; set; }
        public virtual string Source { set; get; }
        public virtual string Hotness { set; get; }
        public virtual string DealerAbbreviation { set; get; }
        public virtual string CustomerName { set; get; }
        public virtual string CustomerTel1 { get; set; }
        public virtual string Email { set; get; }
        public String ContactDate { get; set; }
        public String TestDriverDate { get; set; }
        public String SalesDate { get; set; }
        public String CouldNot { get; set; }
        public String Contacted { get; set; }
        public string Reason { get; set; }
        public String InquiryDate { get; set; }
        public String ShowroomVisitDate { get; set; }
        public String TestDriveDate { get; set; }
        public String SigningDate { get; set; }
        public String TMVonlineTMSS { get; set; }
        public String TMSSSalesmanager { get; set; }
        public String SalesManagerSalesMan { get; set; }
        public String SalesmanCustomer { get; set; }
        public virtual string ContractNo { set; get; }
    }
}
