﻿namespace tmss.Sales.Customer.Dto
{
    public class ListExpectedDelTimingForSalesCustomer
    {
        public long? Id { get; set; }
        public string Description { get; set; }
        public long? HotnessId { get; set; }
    }
}
