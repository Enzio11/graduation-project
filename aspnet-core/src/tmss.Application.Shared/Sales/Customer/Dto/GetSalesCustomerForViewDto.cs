﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace tmss.Sales.Customer.Dto
{
    public class GetSalesCustomerForViewDto : EntityDto<long?>
    {
        public SalesCustomerInfomationDto SalesCustomerInfomation { get; set; }
        public List<GetListCarAttentionDto> CarAttention { get; set; }
        public List<GetListCarAttentionDto> OtherCarAttention { get; set; }
        public List<CarPurchasedCustomerDto> CarPurchased { get; set; }
    }
}
