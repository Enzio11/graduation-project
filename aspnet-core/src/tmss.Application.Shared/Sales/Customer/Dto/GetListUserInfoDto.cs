﻿namespace tmss.Sales.Customer.Dto
{
    public class GetListUserInfoDto
    {
        public long? UserId { get; set; }
    }
}
