﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerHistoryDto
    {
        public DateTime? Date { get; set; }
        public string Status { get; set; }
        public string Content { get; set; }
    }
}
