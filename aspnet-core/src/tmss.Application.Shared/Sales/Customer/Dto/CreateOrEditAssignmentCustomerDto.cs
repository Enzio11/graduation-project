﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class CreateOrEditAssignmentCustomerDto
    {
        public long? Id { get; set; }
        public DateTime? Date { get; set; }
        public long? ModelId { get; set; }
        public long? SourceId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerMidName { get; set; }
        public string CustomerLastName { get; set; }
        public long? ExpectedDelTimingId { get; set; }
        public long? AssignPersonId { get; set; }
        public long? CustomerTypeId { get; set; }
    }
}
