﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetListCustomerForContractDto : EntityDto<long>
    {
        public string No { get; set; }
        public string CustomerName { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerMidName { get; set; }
        public string CustomerLastName { get; set; }
        public string CustomerTel1 { get; set; }
        public string Hotness { get; set; }
        public long? DistrictId { get; set; }
        public long? ProvinceId { get; set; }
        public string Details { get; set; }
        public string CustomerNo { get; set; }
        public long? BusinessTypeId { get; set; }
        public string Email { get; set; }
        public string TaxCode { get; set; }
        public string IdCard { get; set; }
        public long? GenderId { get; set; }
        public long? CustomerTypeId { get; set; }
        public long? SourceId { get; set; }
        public long? AgeRangeId { get; set; }
        public string CustomerPlaceIssuance { get; set; }
        public DateTime? CustomerDateIssuance { get; set; }
    }
}
