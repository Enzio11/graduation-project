﻿using System;

namespace tmss.Sales.Customer.Dto
{
    public class CustomerStatusChangeDto
    {
        public long? SalesCustomerId { get; set; }
        public long? SalesStageId { get; set; }
        public DateTime? ActivityDate { set; get; }
        public string ActivityName { get; set; }
        public long ActivityStatusId { get; set; }
        public long ReasonOfLostId { get; set; }
        public long ReasonOfLostTypeId { get; set; }
        public long DeliveryPlaceId { get; set; }
        public long RouteId { get; set; }
        public long SupporterId { get; set; }
        public bool? IsShowRoomVisit { get; set; }
        public DateTime? ShowRoomVisitDate { get; set; }
        public DateTime? TestDriveDate { get; set; }
        public bool? IsTestDrive { get; set; }
        public string ReasonWhyNotTD { get; set; }
        public string Note { get; set; }
        public string ReasonLost { get; set; }
        public string ReasonOfLostOther { get; set; }
        public long RequestUserId { get; set; }
        public long ApproverId { get; set; }
        public int StatusApprove { get; set; }
    }
}
