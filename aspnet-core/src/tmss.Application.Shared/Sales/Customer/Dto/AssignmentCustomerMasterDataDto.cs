﻿﻿using System.Collections.Generic;
using tmss.MstSle.IMstSleGenDealer.Dto;
using tmss.MstSle.IMstSleModel.Dto;
using tmss.MstSle.Make;
using tmss.Sales.SalesPerson.Dtos;

namespace tmss.Sales.Customer.Dto
{
    public class AssignmentCustomerMasterDataDto
    {
        public List<GetModelForSalesCustomerDto> Models { get; set; }
        public List<GetMakeForSalesCustomerDto> Hotnesses { get; set; }
        public List<GetMakeForSalesCustomerDto> SalesStages { get; set; }
        public List<GetMakeForSalesCustomerDto> Sources { get; set; }
        public List<GetListSalesPersonNameDto> AssignPersons { get; set; }
        public List<GetListSalesPersonNameDto> SalesPersons { get; set; }
        public List<GetMstGenDealerForDropdownDto> Dealers { get; set; }
        public List<ListExpectedDelTimingForSalesCustomer> ExpectedDelTimings { get; set; }
    }
}