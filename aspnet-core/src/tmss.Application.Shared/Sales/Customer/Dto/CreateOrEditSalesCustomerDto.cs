﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class CreateOrEditSalesCustomerDto : EntityDto<long?>
    {
        public string No { get; set; }
        public DateTime? ContactDate { get; set; }
        public long? GenderId { get; set; }
        public long? CustomerTypeId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerMidName { get; set; }
        public string CustomerLastName { get; set; }
        public string Remark { get; set; }
        public string CustomerTel1 { get; set; }
        public string CustomerTel2 { get; set; }
        public long? ProvinceId { get; set; }
        public long? DistrictId { get; set; }
        public string CustomerPermanentAddress { get; set; }
        public long? SourceOfInfoId { get; set; }
        public long? HotnessId { get; set; }
        public long? HobbyId { get; set; }
        public long? ExpectedDelTimingId { get; set; }
        public long? PaymentTypeId { get; set; }
        public long? RoleId { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? Birthday { get; set; }
        public long? MaritalStatusId { get; set; }
        public long? OccupationId { get; set; }
        public string Email { get; set; }
        public string FoodDrink { get; set; }
        public long? Income { get; set; }
    }
}
