﻿using Abp.Application.Services.Dto;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerSocialDto : EntityDto<long?>
    {
        public long? SocialChanelId { get; set; }
        public string SocialId { get; set; }
    }
}
