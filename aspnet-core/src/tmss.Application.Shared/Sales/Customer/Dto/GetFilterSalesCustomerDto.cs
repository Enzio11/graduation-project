﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.Customer.Dto
{
    public class GetFilterSalesCustomerDto : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public long? UnitId { set; get; }
        public long? SalesPersonId { set; get; }
        public long? HotnessId { set; get; }
        public long? SourceId { set; get; }
        public long? DistrictId { set; get; }
        public long? ProvinceId { set; get; }
        public string CustomerName { get; set; }
        public long? CustomerTypeId { get; set; }
        public long? DealerId { set; get; }
        public long? ModelId { set; get; }
        public long? GradeId { set; get; }
        public long? CustomerClass { set; get; }
        public DateTime? CreationDateFrom { get; set; }
        public DateTime? CreationDateTo { get; set; }
        public DateTime? ContactHistoryDateFrom { get; set; }
        public DateTime? ContactHistoryDateTo { get; set; }
        public long? ContactById { set; get; }
        public string CustomerTel1 { get; set; }
        public int FreezeType { get; set; }
    }
}
