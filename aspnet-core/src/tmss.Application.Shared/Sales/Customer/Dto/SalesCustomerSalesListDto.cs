﻿using Abp.Domain.Entities;
using System;
using tmss.MstSle.Dto.Color;
using tmss.MstSle.Dto.Grade;
using tmss.MstSle.Dto.Model;

namespace tmss.Sales.Customer.Dto
{
    public class SalesCustomerSalesListDto : Entity<long?>
    {
        public string ContractNo { get; set; }
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public long? CustomerTypeId { get; set; }
        public string Telephone { get; set; }
        public string CustomerTel { get; set; }
        public string CompanyTel { get; set; }
        public string SalePersonName { get; set; }
        public long? SalePersonId { get; set; }
        public string CarIntention { get; set; }
        public long? ModelId { get; set; }
        public ModelDto Model { get; set; }
        public GradeDto Grade { get; set; }
        public ColorDto EColor { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public DateTime? PaymentActualInvoiceDate { get; set; }
        public long? CustomerId { get; set; }
    }
}
