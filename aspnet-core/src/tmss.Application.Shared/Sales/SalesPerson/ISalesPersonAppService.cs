﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.Sales.SalesPerson.Dtos;

namespace tmss.Sales.SalesPerson
{
    public interface ISalesPersonAppService : IApplicationService
    {
        PagedResultDto<GetSalesPersonForViewDto> GetAll(GetAllSalesPersonInput input);
        Task<GetSalesPersonForEditOutput> GetSalesPersonForEdit(EntityDto<long?> input);
        Task CreateOrEdit(CreateOrEditSalesPersonDto input);
        Task Delete(EntityDto<long> input);
        Task<List<UserListInfoDto>> GetUserAll(bool created);
    }
}
