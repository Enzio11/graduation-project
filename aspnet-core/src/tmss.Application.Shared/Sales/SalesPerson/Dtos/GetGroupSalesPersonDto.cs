﻿namespace tmss.Sales.SalesPerson.Dtos
{
    public class GetGroupSalesPersonDto
    {
        public string GroupName { get; set; }
        public long Id { get; set; }
        public long? ParentId { get; set; }

        public string Code { get; set; }

        public string DisplayName { get; set; }

        public int MemberCount { get; set; }
        public string ParentName { get; set; }

        public string Description { get; set; }
        public int RoleCount { get; set; }
        public int? TenantId { get; set; }
        public bool IsBP { get; set; }
        public bool IsActive { get; set; }
    }
}
