﻿using Abp.Application.Services.Dto;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class UpdateUserIdDto : EntityDto<long?>
    {
        public long UserId { get; set; }
    }
}
