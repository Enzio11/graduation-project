﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class GetSalesPersonForViewDto : EntityDto<long>
    {
        public string GroupName { get; set; }

        public decimal? GroupId { get; set; }
        public string FullName { get; set; }
        public decimal? Ordering { get; set; }
        public DateTime? Birthday { get; set; }
        public string Position { get; set; }
        public string Abbreviation { get; set; }
        public string Status { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public long? UserId { get; set; }
        public bool? IsContractApprove { get; set; }
        public bool? IsProposalApprove { get; set; }
        public bool? IsViewCustomer { get; set; }
        public bool? IsApproveTestDrive { get; set; }
        public bool? IsApproveLostFreeze { get; set; }
        public bool? IsReceiveAssignmentList { get; set; }
        public bool? IsViewDardboard { get; set; }
        public bool? IsReceiveTestDriveList { get; set; }
        public bool? IsViewDuplicateCustomer { get; set; }
        public bool? IsReviewFleet { get; set; }
        public bool? IsApproveFleet { get; set; }
        //public bool? IsRepresent { get; set; }
        public bool? IsCreateTransferCustomer { get; set; }

    }
}
