﻿using System.Collections.Generic;
using tmss.Organizations.Dto;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class ListGroupByUserDto
    {
        public List<OrganizationUnitDto> AllOrganizationUnits { get; set; }

        public List<string> MemberedOrganizationUnits { get; set; }
    }
}
