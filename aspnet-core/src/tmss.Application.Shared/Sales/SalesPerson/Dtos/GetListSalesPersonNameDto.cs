﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class GetListSalesPersonNameDto : EntityDto<long?>
    {
        public string FullName { get; set; }
        public string Position { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
