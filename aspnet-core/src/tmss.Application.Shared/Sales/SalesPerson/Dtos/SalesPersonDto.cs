﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class SalesPersonDto : EntityDto<long>
    {
        public string GroupName { get; set; }
        public string TeamName { get; set; }
        public string FullName { get; set; }

        public long GroupId { get; set; }

        public long SaleTeamId { get; set; }
        public decimal? Ordering { get; set; }
        public DateTime Birthday { get; set; }
        public string Position { get; set; }
        public string Abbreviation { get; set; }
        public string Status { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
    }
}
