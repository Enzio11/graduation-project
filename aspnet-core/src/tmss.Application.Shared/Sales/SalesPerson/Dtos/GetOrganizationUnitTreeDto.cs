﻿namespace tmss.Sales.SalesPerson.Dtos
{
    public class GetOrganizationUnitTreeDto
    {
        public long? SelectedUserId { get; set; }
        public bool IsCreated { get; set; }
        public long? DealerId { get; set; }
    }
}
