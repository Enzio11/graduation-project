﻿namespace tmss.Sales.SalesPerson.Dtos
{
    public class UserListInfoDto
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public int MyProperty { get; set; }
    }
}
