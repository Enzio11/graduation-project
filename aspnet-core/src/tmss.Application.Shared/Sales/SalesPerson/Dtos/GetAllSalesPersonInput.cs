﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class GetAllSalesPersonInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public List<decimal?> OrganizationUnitId { get; set; }
        public string FullNameFilter { get; set; }
        public decimal? OrderingFilter { get; set; }
        public string PositionFilter { get; set; }
        public string AbbreviationFilter { get; set; }
        public string StatusFilter { get; set; }
        public string GenderFilter { get; set; }
        public string EmailFilter { get; set; }
        public string AddressFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public string PhoneFilter { get; set; }
        public long? DealerId { get; set; }
    }
}
