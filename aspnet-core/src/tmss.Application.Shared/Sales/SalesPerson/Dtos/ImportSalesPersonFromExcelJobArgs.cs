﻿using Abp;
using System;

namespace tmss.Sales.SalesPerson.Dtos
{
    public class ImportSalesPersonFromExcelJobArgs
    {
        public int? TenantId { get; set; }

        public Guid BinaryObjectId { get; set; }

        public UserIdentifier User { get; set; }
    }
}
