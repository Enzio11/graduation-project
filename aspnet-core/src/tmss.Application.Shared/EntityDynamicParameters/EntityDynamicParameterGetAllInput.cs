﻿namespace tmss.Entity
{
    public class EntityDynamicParameterGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
