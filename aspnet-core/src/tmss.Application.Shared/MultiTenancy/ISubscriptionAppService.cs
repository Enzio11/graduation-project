﻿using Abp.Application.Services;
using System.Threading.Tasks;

namespace tmss.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task DisableRecurringPayments();

        Task EnableRecurringPayments();
    }
}
