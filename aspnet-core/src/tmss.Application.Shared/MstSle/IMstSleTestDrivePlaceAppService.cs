﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleTestDrivePlace.Dto;

namespace tmss.MstSle
{
    public interface IMstSleTestDrivePlaceAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleTestDrivePlaceForViewDto>> GetAll(GetAllMstSleTestDrivePlaceInput input);
        Task<GetMstSleTestDrivePlaceForEditOutput> GetMstSleTestDrivePlaceForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleTestDrivePlaceDto input);
        Task Delete(EntityDto<long> input);
    }
}
