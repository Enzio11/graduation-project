﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.Occupation;

namespace tmss.MstSle
{
    public interface IMstSleOccupationAppService : IApplicationService
    {
        ListResultDto<MstSleOccupationListDto> GetListOccupation(MstSleOccupationInput input);
        Task<PagedResultDto<GetMstSleOccupationForViewDto>> GetAll(GetMstSleOccupationInputDto input);
        Task CreateOrEdit(CreateOrEditMstSleOccupationDto input);
        Task<GetMstSleOccupationForEditOutput> GetMstSleOccupationForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
    }
}
