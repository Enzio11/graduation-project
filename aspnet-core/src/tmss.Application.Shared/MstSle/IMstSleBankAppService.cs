﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.Bank;

namespace tmss.MstSle
{
    public interface IMstSleBankAppService
    {
        public ListResultDto<MstSleBankDto> GetListBank();
        Task<PagedResultDto<GetMstSleBankForViewDto>> GetAll(GetMstSleBankInputDto input);
        Task<PagedResultDto<GetMstSleBankForViewDto>> CusAppGetAll(GetMstSleBankInputDto input);

    }
}
