﻿namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetMstSleModelsForEditOutput
    {
        public CreateOrEditMstSleModelsDto MstSleModels { get; set; }
    }
}
