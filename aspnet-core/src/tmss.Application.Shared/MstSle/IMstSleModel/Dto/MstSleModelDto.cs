﻿namespace tmss.MstSle.IMstSleModel.Dto
{
    public class MstSleModelDto
    {
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string Description { get; set; }
    }
}
