﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetAndFilterAllModel : PagedAndSortedResultRequestDto
    {
        public string MarketingCodeFilter { get; set; }
        public string ProductionCodeFilter { get; set; }
        public string VnNameFilter { get; set; }
        public string EnNameFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public string StatusFilter { get; set; }
    }
}
