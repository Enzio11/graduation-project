﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class MstSleModelsDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string MarketingCode { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string VnName { get; set; }

        [StringLength(50)]
        public string Abbreviation { get; set; }
        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public decimal? OrderingRpt { get; set; }

        public long? PercentBalloon { get; set; }
    }
}
