﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class CreateOrEditMstSleModelsDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string VnName { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public int Ordering { get; set; }

        [StringLength(50)]
        public string MarketingCode { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        [StringLength(50)]
        public string Abbreviation { get; set; }
        [Column(TypeName = "numeric(18, 0)")]
        public long? MakeId { get; set; }
        public string MarketingCodeWeb { get; set; }
    }
}
