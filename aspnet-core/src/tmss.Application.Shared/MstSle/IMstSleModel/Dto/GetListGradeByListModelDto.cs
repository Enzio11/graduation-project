﻿namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetListGradeByListModelDto
    {
        public long? ModelId { get; set; }
        public long? GradeId { get; set; }
    }
}
