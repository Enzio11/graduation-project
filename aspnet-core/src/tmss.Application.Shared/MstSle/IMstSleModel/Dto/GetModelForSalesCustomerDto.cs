﻿namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetModelForSalesCustomerDto
    {
        public long? Id { get; set; }
        public string MarketingCode { get; set; }
        public long? MakeId { get; set; }
    }
}
