﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetAllMstSleModelsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string MarketingCodeFilter { get; set; }
        public string ProductionCodeFilter { get; set; }
        public string AbbreviationFilter { get; set; }
        public string VnNameFilter { get; set; }
        public string EnNameFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public string StatusFilter { get; set; }
        public long? MakeIdFilter { get; set; }
    }
}
