﻿using System.Collections.Generic;
using tmss.MstSle.IMstSleGrade.Dto;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetListModelGradeProductionColorDto
    {
        public long? ModelId { get; set; }
        public string MarketingCode { get; set; }
        public string ModelVnName { get; set; }
        public string ModelEnName { get; set; }
        public List<GetListGradeNameDto> ListGrade { get; set; }
    }
}
