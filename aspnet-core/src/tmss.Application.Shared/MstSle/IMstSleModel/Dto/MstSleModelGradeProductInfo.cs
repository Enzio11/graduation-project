﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using tmss.MstSle.IMstSleGrade.Dto;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class MstSleModelGradeProductInfo : EntityDto<long>
    {
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string Description { get; set; }
        public List<MstSleGradeProduct> Grades { get; set; }

    }

}
