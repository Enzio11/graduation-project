﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleModel.Dto
{
    public class GetMstSleModelForContractDto : EntityDto<long?>
    {
        public string MarketingCode { get; set; }
    }
}
