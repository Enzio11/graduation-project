﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.Hobby;

namespace tmss.MstSle
{
    public interface IMstSleHobbyAppService : IApplicationService
    {
        ListResultDto<MstSleHobbyListDto> GetListHobby(MstSleHobbyInput input);
        Task CreateOrEdit(CreateOrEditMstSleHobbyDto input);
        Task<GetMstSleHobbyForEditOutput> GetMstSleHobbyForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
    }
}
