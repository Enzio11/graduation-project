﻿using Abp.Application.Services.Dto;


namespace tmss.MstSle.Dto.SalesStage
{
    public class GetMstSleSalesStageInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
