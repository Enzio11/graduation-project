﻿

namespace tmss.MstSle.Dto.SalesStage
{
    public class GetMstSleSalesStageForEditOutput
    {
        public CreateOrEditMstSleSalesStageDto mstSleSalesStageDto { get; set; }
    }
}
