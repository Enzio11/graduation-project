﻿

namespace tmss.MstSle.Dto.SalesStage
{
    public class GetMstSleSalesStageForExportDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public long? Ordering { get; set; }
    }
}
