﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.SalesStage
{
    public class SalesStageListDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
