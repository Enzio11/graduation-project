﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ColorByGrade.Dto
{
    public class GetProductionInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
    }
}
