﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.ColorByGrade.Dto
{
    public class GetMstSleColorGradeProductionForViewDto : Entity<long>
    {
        public decimal? Ordering { get; set; }
        public long? ColorId { get; set; }
        public string Color { get; set; }
        public long? ProduceId { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string ProductionCode { get; set; }
    }
}
