﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.ColorByGrade.Dto
{
    public class GetMstSleLookUpDto : Entity<long>
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal? Value { get; set; }
        public string Status { get; set; }
    }
}
