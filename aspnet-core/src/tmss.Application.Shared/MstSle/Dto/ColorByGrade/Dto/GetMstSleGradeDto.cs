﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.ColorByGrade.Dto
{
    public class GetMstSleGradeDto : Entity<long>
    {
        public string MarketingCode { get; set; }
    }
}
