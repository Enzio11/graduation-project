﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.MstSle.Dto.ColorByGrade.Dto;

namespace tmss.MstSle.Dto.ColorByGrade
{
    public interface IMstSleColorByGradeAppService
    {
        Task<List<GetMstSleGradeDto>> GetListGrade();
    }
}
