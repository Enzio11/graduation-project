﻿namespace tmss.MstSle.Dto.ReasonOfNC
{
    public class GetMstSleReasonOfNCForEditOutput
    {
        public CreateOrEditMstSleReasonOfNCDto MstSleReasonOfNC { get; set; }
    }
}
