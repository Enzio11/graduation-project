﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfNC
{
    public class GetMstSleReasonOfNCInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
