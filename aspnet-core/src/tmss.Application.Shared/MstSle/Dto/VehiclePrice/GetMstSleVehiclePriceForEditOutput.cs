﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetMstSleVehiclePriceForEditOutput : EntityDto<int?>
    {

        public CreateOrEditMstSleVehiclePriceDto MstSleVehiclePrice { get; set; }
    }
}