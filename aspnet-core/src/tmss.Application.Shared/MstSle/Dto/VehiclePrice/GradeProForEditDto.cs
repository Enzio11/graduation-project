﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GradeProForEditDto : EntityDto<long>
    {

        public int? GradeId { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        public List<ColorForEditDto> ListColor { get; set; }

        public List<ColorForEditDto> ListIColor { get; set; }
    }
}
