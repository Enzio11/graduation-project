﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetMstSleTransmissionForPriceDto : EntityDto<long>
    {
        [MaxLength(50)]
        public string Code { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public long? Ordering { get; set; }
    }
}
