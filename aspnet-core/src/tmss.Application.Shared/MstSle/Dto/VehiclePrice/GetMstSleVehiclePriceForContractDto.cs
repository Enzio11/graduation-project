﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetMstSleVehiclePriceForContractDto : EntityDto<long?>
    {
        public long? ModelId { get; set; }
        public long? GradeId { get; set; }
        public long? GradeProductionId { get; set; }
        public long? ColorId { get; set; }
        public long? InteriorColorId { get; set; }
        public decimal? PriceAmount { get; set; }
    }
}
