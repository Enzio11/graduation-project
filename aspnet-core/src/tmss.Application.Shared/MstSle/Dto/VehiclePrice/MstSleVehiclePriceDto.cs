﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class MstSleVehiclePriceDto : EntityDto<long>
    {

        public int? GradeProductionId { get; set; }


        public int? ColorId { get; set; }

        public int? InteriorColorId { get; set; }

        [Column(TypeName = "numeric(15, 0)")]
        public decimal? PriceAmount { get; set; }

        [Column(TypeName = "numeric(15, 0)")]
        public decimal? OrderPriceAmount { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
    }
}
