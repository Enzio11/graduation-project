﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class ColorForEditDto : EntityDto<int>
    {
        [StringLength(50)]
        public string ColorCode { get; set; }
    }
}
