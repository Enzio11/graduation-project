﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetAllMstSleVehiclePriceInput : PagedAndSortedResultRequestDto
    {
        public int? GradeProductionIdFilter { get; set; }
        public int? ColorIdFilter { get; set; }
        public int? InteriorColorIdFilter { get; set; }
        public long? PriceAmountMinFilter { get; set; }
        public long? PriceAmountMaxFilter { get; set; }
        public long? OrderPriceAmountMinFilter { get; set; }
        public long? OrderPriceAmountMaxFilter { get; set; }
        public long? TransmissionsFilter { get; set; }
        public long? FuelTypeFilter { get; set; }
        public long? EngineTypeFilter { get; set; }
        public long? IsCBUFilter { get; set; }

        public int? ModelIdFilter { get; set; }
        public string StatusFilter { get; set; }
    }
}