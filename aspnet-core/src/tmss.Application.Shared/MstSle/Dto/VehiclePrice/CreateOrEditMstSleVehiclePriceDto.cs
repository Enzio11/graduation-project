﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class CreateOrEditMstSleVehiclePriceDto : EntityDto<int?>
    {

        public int GradeId { get; set; }

        public int GradeProductionId { get; set; }

        public int? ColorId { get; set; }

        public int? InteriorColorId { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public long? PriceAmount { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public long? OrderPriceAmount { get; set; }

        [StringLength(50)]
        public string GradeCode { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        public int? ModelId { get; set; }

        [StringLength(1)]
        public string Status { get; set; }
        public long? TransmissionId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public long? EngineTypeId { get; set; }
        public long? FuelTypeId { get; set; }
        public string Remarks { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? IsCBU { get; set; }
        public virtual string CommercialName { get; set; }
    }
}