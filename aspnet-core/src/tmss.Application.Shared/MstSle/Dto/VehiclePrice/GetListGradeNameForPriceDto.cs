﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetListGradeNameForPriceDto : EntityDto<long?>
    {
        [StringLength(25)]
        public string MarketingCode { get; set; }

        [StringLength(25)]
        public string ProductionCode { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public long? ModelId { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string VnName { get; set; }
    }
}
