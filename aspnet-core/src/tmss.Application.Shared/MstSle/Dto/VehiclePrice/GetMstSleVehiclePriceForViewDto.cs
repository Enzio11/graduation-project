﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetMstSleVehiclePriceForViewDto : EntityDto<long>
    {

        public long? GradeProductionId { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        public long? GradeId { get; set; }

        [StringLength(50)]
        public string MarketingCode { get; set; }

        public long? ModelId { get; set; }

        public long? ColorId { get; set; }

        public long? InteriorColorId { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public decimal? PriceAmount { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public decimal? OrderPriceAmount { get; set; }

        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(50)]
        public string ModelCode { get; set; }

        [StringLength(50)]
        public string IColorCode { get; set; }

        [StringLength(50)]
        public string EColorCode { get; set; }
        public string Remarks { get; set; }
        [Column(TypeName = "numeric(8, 0)")]
        public long? IsCBU { get; set; }
        public long? SeatNo { get; set; }
        public virtual string CommercialName { get; set; }
    }
}