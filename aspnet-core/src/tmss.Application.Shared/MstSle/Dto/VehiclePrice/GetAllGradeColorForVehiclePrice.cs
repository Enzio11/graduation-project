﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.Dto.VehiclePrice
{
    public class GetAllGradeColorForVehiclePrice : EntityDto<long?>
    {
        [StringLength(50)]
        public string VnName { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        public long? GradeId { get; set; }
    }
}
