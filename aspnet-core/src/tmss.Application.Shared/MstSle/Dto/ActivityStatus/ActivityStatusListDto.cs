﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ActivityStatus
{
    public class ActivityStatusListDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}