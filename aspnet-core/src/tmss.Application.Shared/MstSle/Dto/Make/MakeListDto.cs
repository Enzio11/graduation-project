﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Make
{
    public class MakeListDto : EntityDto<long>
    {
        public string Description { get; set; }
        public string Code { get; set; }
    }
}
