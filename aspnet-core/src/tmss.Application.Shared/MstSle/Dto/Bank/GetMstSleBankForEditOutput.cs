﻿namespace tmss.MstSle.Dto.Bank
{
    public class GetMstSleBankForEditOutput
    {
        public CreateOrEditMstSleBankDto MstSleBank { get; set; }
    }
}
