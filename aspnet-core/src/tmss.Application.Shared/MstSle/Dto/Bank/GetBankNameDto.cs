﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Bank
{
    public class GetBankNameDto : EntityDto<long?>
    {
        public string BankName { get; set; }
    }
}
