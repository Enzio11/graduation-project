﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.Dto.Bank
{
    public class CreateOrEditMstSleBankDto : EntityDto<long?>
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string Address { get; set; }
        //[Column(TypeName = "numeric(4, 0)")]
        public long? BankTypeId { get; set; }
        public string Status { get; set; }
        //[Column(TypeName = "numeric(4, 0)")]
        public long? Ordering { get; set; }
        //[Column(TypeName = "numeric(8, 0)")]
        public long? DealerId { get; set; }
        [Column(TypeName = "numeric(18, 2)")]
        public decimal? AccountNumber { get; set; }
        //[Column(TypeName = "numeric(1, 0)")]
        public long? IsFinance { get; set; }
    }
}
