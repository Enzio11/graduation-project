﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Bank
{
    public class GetMstSleBankForViewDto : EntityDto<long>
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string Address { get; set; }
        public decimal? BankTypeId { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }
        public long DealerId { get; set; }
        public long AccountNumber { get; set; }
    }
}
