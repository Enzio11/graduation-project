using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Bank
{
    public class MstSleBankDto : EntityDto<int>
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string Address { get; set; }
        public long BankTypeId { get; set; }
        public string Status { get; set; }
        public long Ordering { get; set; }
        public long DealerId { get; set; }
        public long AccountNumber { get; set; }
    }
}