﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.Bank
{
    public class MstSleBankFinanceDto : Entity<long>
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
    }
}
