﻿namespace tmss.MstSle.Dto.Bank
{
    public class GetMstSleBankMasterForEditOutput
    {
        public CreateOrEditMstSleBankMasterDto MstSleBankMaster { get; set; }
    }
}
