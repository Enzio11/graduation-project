﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Bank
{
    public class GetMstSleBankInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
