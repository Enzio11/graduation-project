﻿namespace tmss.MstSle.Dto.SalePerson
{
    public class MstSleSalesPersonDto
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Position { get; set; }
        public long UserId { get; set; }
    }
}
