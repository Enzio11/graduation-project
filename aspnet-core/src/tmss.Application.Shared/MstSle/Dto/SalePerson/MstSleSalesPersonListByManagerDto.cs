﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.MstSle.Dto.SalePerson
{
    public class MstSleSalesPersonListByManagerDto : FullAuditedEntityDto<long>
    {

        public virtual long? SaleManagerId { get; set; }

        public virtual long? UserId { get; set; }
        public virtual long? SaleTeamId { get; set; }

        public virtual string FullName { get; set; }

        public virtual DateTime? Birthday { get; set; }

        public virtual string Position { get; set; }

        public virtual string Abbreviation { get; set; }

        public virtual string Status { get; set; }

        public virtual string Gender { get; set; }

        public virtual string Email { get; set; }

        public virtual string Address { get; set; }

        public virtual string Description { get; set; }

        public virtual string Phone { get; set; }

        public virtual string UnitName { get; set; }
    }
}
