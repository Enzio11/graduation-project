﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfNA
{
    public class GetReasonOfNAInput : PagedAndSortedResultRequestDto
    {
        public int Type { get; set; }
        public string filterText { get; set; }
    }
}
