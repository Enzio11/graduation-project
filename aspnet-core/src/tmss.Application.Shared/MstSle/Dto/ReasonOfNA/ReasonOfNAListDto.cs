﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfNA
{
    public class ReasonOfNAListDto : EntityDto<long>
    {
        public int Type { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }
    }
}
