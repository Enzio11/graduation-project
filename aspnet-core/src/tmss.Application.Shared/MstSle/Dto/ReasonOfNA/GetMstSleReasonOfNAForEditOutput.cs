﻿namespace tmss.MstSle.Dto.ReasonOfNA
{
    public class GetMstSleReasonOfNAForEditOutput
    {
        public CreateOrEditMstSleReasonOfNADto MstSleReasonOfNA { get; set; }
    }
}
