﻿namespace tmss.MstSle.Dto.TestDriveStall
{
    public class GetMstSleTestDriveStallForEditOutput
    {
        public CreateOrEditMstSleTestDriveStallDto MstSleTestDriveStall { get; set; }
    }
}
