﻿
using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveStall
{
    public class GetAllTestDriveStallInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
        public long? DealerIdFilter { get; set; }
    }
}
