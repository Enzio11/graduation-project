﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveStall
{
    public class TestDriveStallListDto : EntityDto<long>
    {
        public string Name { get; set; }
    }
}
