﻿
using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveStall
{
    public class CreateOrEditMstSleTestDriveStallDto : EntityDto<long?>
    {
        public string Name { get; set; }

        public string Description { get; set; }
        public long? DealerId { get; set; }
    }
}
