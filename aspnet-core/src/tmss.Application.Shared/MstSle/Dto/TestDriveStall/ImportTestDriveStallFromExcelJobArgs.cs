﻿using Abp;
using System;

namespace tmss.MstSle.Dto.TestDriveStall
{
    public class ImportTestDriveStallFromExcelJobArgs
    {
        public int? TenantId { get; set; }
        public Guid BinaryObjectId { get; set; }
        public UserIdentifier User { get; set; }
    }
}
