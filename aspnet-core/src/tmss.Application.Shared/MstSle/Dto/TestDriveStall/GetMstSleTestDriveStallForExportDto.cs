﻿
using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveStall
{
    public class GetMstSleTestDriveStallForExportDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Dealer { get; set; }
    }
}
