﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.BusinessType
{
    public class MstSleBusinessTypeDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
