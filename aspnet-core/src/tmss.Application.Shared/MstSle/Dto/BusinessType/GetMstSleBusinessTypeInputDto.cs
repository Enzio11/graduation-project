﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.BusinessType
{
    public class GetMstSleBusinessTypeInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
