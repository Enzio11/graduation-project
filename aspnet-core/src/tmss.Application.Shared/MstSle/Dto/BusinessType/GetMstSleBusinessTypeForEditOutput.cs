﻿namespace tmss.MstSle.Dto.BusinessType
{
    public class GetMstSleBusinessTypeForEditOutput
    {
        public CreateOrEditMstSleBusinessTypeDto MstSleBusinessType { get; set; }
    }
}
