﻿
namespace tmss.MstSle.Dto.ReasonOfChanges
{
    public class GetMstSleReasonOfChangesForEditOutput
    {
        public CreateOrEditMstSleReasonOfChangesDto MstSleReasonOfChangesDto { get; set; }
    }
}
