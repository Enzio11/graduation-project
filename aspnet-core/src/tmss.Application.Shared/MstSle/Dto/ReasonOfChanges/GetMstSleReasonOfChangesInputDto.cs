﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfChanges
{
    public class GetMstSleReasonOfChangesInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
