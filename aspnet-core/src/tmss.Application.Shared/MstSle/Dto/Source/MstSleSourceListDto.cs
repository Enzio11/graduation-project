﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Source
{
    public class MstSleSourceListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }

    }
}
