using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Source
{
    public class GetAllMstSleSourceInput : PagedAndSortedResultRequestDto
    {
        public string TextFilter { get; set; }
        public string CodeFilter { get; set; }
        public string DescriptionEnFilter { get; set; }
        public string DescriptionVnFilter { get; set; }
        public long? OrderingFilter { get; set; }
        public int? SourceTypeFilter { get; set; }
    }
}
