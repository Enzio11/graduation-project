﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.Source
{
    public class MstSleSourceDto : Entity<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public long? Ordering { get; set; }
        public int? SourceType { get; set; }
    }
}
