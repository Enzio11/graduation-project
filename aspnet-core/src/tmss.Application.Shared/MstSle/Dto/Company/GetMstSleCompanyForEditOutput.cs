﻿

namespace tmss.MstSle.Dto.Company
{
    public class GetMstSleCompanyForEditOutput
    {
        public CreateOrEditMstSleCompanyDto MstSleCompanyDto { get; set; }
    }
}
