using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Purpose
{
    public class PurposeListDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}