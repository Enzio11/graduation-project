﻿namespace tmss.MstSle.Dto.Purpose
{
    public class MstSlePurposeDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
