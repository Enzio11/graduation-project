﻿using Abp.Application.Services.Dto;
namespace tmss.MstSle.Dto.Purpose
{
    public class CreateOrEditMstSlePurposeDto : EntityDto<long?>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public long? Ordering { get; set; }
    }
}
