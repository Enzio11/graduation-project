﻿using Abp.Application.Services.Dto;


namespace tmss.MstSle.Dto.Purpose
{
    public class GetMstSlePurposeInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
