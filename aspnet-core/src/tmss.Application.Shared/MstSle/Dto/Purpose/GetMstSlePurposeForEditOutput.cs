﻿namespace tmss.MstSle.Dto.Purpose
{
    public class GetMstSlePurposeForEditOutput
    {
        public CreateOrEditMstSlePurposeDto mstSlePurposeDto { get; set; }
    }
}
