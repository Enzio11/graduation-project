﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.Relationship
{
    public class MstSleRelationshipDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
