﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Relationship
{
    public class MstSleRelationshipListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
