﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDrivePlace
{
    public class TestDrivePlaceListDto : EntityDto<long>
    {
        public string Name { get; set; }
    }
}
