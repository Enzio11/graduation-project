using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfInsurance
{
    public class ReasonOfInsuranceListDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}