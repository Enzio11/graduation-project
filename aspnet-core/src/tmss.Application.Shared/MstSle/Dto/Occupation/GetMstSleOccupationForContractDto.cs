﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Occupation
{
    public class GetMstSleOccupationForContractDto : EntityDto<long?>
    {
        public string Vi_Description { get; set; }
    }
}
