﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Occupation
{
    public class MstSleOccupationListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
