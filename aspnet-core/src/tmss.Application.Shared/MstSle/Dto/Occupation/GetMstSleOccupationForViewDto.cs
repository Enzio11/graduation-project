﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.Dto.Occupation
{
    public class GetMstSleOccupationForViewDto : EntityDto<long>
    {
        public string Code { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        public long? Ordering { get; set; }
        public string Vi_Description { get; set; }
    }
}
