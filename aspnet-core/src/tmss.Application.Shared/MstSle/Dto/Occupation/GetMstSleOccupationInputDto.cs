﻿
using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Occupation
{
    public class GetMstSleOccupationInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
