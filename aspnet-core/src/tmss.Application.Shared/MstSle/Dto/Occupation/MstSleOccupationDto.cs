﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.Occupation
{
    public class MstSleOccupationDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
