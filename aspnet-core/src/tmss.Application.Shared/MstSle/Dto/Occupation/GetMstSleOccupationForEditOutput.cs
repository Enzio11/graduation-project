﻿namespace tmss.MstSle.Dto.Occupation
{
    public class GetMstSleOccupationForEditOutput
    {
        public CreateOrEditMstSleOccupationDto MstSleOccupation { get; set; }
    }
}
