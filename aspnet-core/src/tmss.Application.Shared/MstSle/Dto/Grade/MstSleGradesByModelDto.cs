﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using tmss.MstSle.Dto.Color;

namespace tmss.MstSle.Dto.Grade
{
    public class MstSleGradesByModelDto : EntityDto<long?>
    {
        public string MarketingCode { get; set; }

        public List<MstSleColorByGradeDto> ListColorByGrade { get; set; }
    }
}
