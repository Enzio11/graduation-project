﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.MstSle.Dto.Grade
{
    public class GradeDto : EntityDto<long>
    {
        public string MarketingCode { get; set; }

        public string ProductionCode { get; set; }

        public string EnName { get; set; }

        public string VnName { get; set; }

        public string Determiner { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }

        public string IsHasAudio { get; set; }

        public string CbuCkd { get; set; }

        public string ShortModel { get; set; }

        public string Wmi { get; set; }

        public string Vds { get; set; }

        public decimal? GasolineTypeId { get; set; }

        public long ModelId { get; set; }

        public decimal? FloormatId { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string FullModel { get; set; }

        public string Barcode { get; set; }

        public string IsFirmColor { get; set; }

        public string IsHasFloormat { get; set; }

        public decimal? PriceAmount { get; set; }

        public string IsShowDeliveryPlan { get; set; }

        public decimal? OrderPrice { get; set; }
        public long? SeatNo { get; set; }
        public decimal? InsurancePayLoad { get; set; }
        public decimal? DealerId { get; set; }
    }
}
