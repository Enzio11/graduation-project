﻿namespace tmss.MstSle.Dto.Grade
{
    public class MstSleGradeCarAttentionDto
    {
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
    }
}
