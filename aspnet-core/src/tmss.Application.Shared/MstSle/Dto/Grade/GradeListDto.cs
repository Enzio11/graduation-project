using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Grade
{
    public class GradeListDto : EntityDto<long?>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string MarketingCode { get; set; }
    }
}