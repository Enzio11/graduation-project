﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class TestDriveRouteListDto : EntityDto<long>
    {
        public string Name { get; set; }
    }
}
