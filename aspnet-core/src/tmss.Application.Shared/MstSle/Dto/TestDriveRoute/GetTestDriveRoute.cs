﻿using System.Collections.Generic;

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class GetTestDriveRoute
    {
        public List<TestDriveRouteListDto> dealer;
        public List<TestDriveRouteListDto> outDlr;
    }
}
