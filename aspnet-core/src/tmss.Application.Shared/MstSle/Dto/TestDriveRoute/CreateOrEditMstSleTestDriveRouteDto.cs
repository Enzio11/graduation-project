﻿
using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class CreateOrEditMstSleTestDriveRouteDto : EntityDto<long?>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long? DealerId { get; set; }
        public int? Type { get; set; }
    }
}
