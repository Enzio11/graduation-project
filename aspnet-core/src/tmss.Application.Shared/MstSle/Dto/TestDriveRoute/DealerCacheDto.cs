﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class DealerCacheDto : EntityDto<long?>
    {
        public string Abbreviation { get; set; }
        public string TenancyName { get; set; }
        public string TenantCode { get; set; }
        public string TenantNameVN { get; set; }
        public decimal? DealerParentId { get; set; }
        public decimal? GroupManagerId { get; set; }
        public string DealerParent { get; set; }
    }
}
