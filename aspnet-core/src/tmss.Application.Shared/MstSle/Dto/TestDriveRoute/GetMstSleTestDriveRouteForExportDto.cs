﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class GetMstSleTestDriveRouteForExportDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Dealer { get; set; }
        public int? Type { get; set; }
    }
}
