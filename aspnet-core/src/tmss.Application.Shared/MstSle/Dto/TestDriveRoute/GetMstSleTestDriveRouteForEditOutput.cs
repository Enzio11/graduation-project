﻿

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class GetMstSleTestDriveRouteForEditOutput
    {
        public CreateOrEditMstSleTestDriveRouteDto MstSleTestDriveRoute { get; set; }
    }
}
