﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveRoute
{
    public class GetAllTestDriveRouteInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
        public long? DealerIdFilter { get; set; }
    }
}
