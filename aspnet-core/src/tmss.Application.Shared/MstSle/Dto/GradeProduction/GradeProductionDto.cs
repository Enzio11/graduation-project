﻿namespace tmss.MstSle.Dto.GradeProduction
{
    public class GradeProductionDto
    {
        public long? ListedPrice { get; set; }
        public long GradeProductionId { get; set; }
        public long ColorId { get; set; }
        public string ColorName { get; set; }
        public long? IColorId { get; set; }
        public string IColorName { get; set; }
        public string ProductCode { get; set; }
        public long? SeatNo { get; set; }
        public string ImageUrl { get; set; }
        public string Remark { get; set; }
        public decimal? InsurancePayLoad { get; set; }

        public decimal? PercentRegistrationFee { get; set; }

    }
}
