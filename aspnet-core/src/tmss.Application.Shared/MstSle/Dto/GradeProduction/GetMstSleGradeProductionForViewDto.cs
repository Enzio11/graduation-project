﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.GradeProduction
{
    public class GetMstSleColorGradeProductionForContractDto : EntityDto<long>
    {
        public long? ProduceId { get; set; }

        public long? ColorId { get; set; }
    }
}
