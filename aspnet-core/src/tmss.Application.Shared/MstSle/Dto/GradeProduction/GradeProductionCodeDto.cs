﻿namespace tmss.MstSle.Dto.GradeProduction
{
    public class GradeProductionCodeDto
    {
        public long? GradeProductionId { get; set; }
        public string GradeProductionName { get; set; }
    }
}
