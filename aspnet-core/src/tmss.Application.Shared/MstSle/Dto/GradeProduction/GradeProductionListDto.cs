﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace tmss.MstSle.Dto.GradeProduction
{
    public class GradeProductionListDto : EntityDto<long>
    {
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
        public string EnName { get; set; }
        public string VnName { get; set; }
        public List<ProductionListDto> GradeProductions { get; set; }
    }
}
