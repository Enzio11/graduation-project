﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.GradeProduction
{
    public class ProductionListDto : EntityDto<long>
    {
        public string ProductionCode { get; set; }
        public string EnName { get; set; }
        public string VnName { get; set; }
        public long? Ordering { get; set; }
        public string IsHasAudio { get; set; }
        public string CbuCkd { get; set; }
        public string ShortModel { get; set; }
        public string Wmi { get; set; }
        public string Vds { get; set; }
        public string FullModel { get; set; }
        public string Barcode { get; set; }
        public long? GasolineTypeId { get; set; }
        public string IsFirmColor { get; set; }
        public long? FrameNoLength { get; set; }
        public long? ManYear { get; set; }
        public long? Capacity { get; set; }
        public long? Length { get; set; }
        public long? Width { get; set; }
        public long? Height { get; set; }
        public long? Weight { get; set; }
        public long? TireSize { get; set; }
        public long? Payload { get; set; }
        public long? PullingWeight { get; set; }
        public long? SeatNoStanding { get; set; }
        public long? SeatNoLying { get; set; }
        public long? SeatNo { get; set; }
        public string CaseSize { get; set; }
        public string BaseLength { get; set; }
        public string Status { get; set; }
        public long? FloId { get; set; }

        public string OriginFrom { get; set; }
        public string Fuel { get; set; }
        public string Engine { get; set; }
        public string DesignStyle { get; set; }
    }
}
