﻿namespace tmss.MstSle.Dto.Color
{
    public class ColorByGradeProductCodeDto
    {
        public long? ColorId { get; set; }
        public long? Price { get; set; }
        public string ColorName { get; set; }
        public string ImageUrl { get; set; }
        public string Remark { get; set; }
        public string Code { get; set; }
    }
}
