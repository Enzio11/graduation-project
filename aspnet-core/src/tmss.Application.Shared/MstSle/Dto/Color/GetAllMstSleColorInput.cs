﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Color
{
    public class GetAllMstSleColorInput : PagedAndSortedResultRequestDto
    {
        public string TextFilter { get; set; }
    }
}
