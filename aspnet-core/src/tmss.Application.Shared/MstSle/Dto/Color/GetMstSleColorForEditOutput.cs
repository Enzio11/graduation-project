﻿namespace tmss.MstSle.Dto.Color
{
    public class GetMstSleColorForEditOutput
    {
        public CreateOrEditMstSleColorDto MstSleColors { get; set; }
    }
}
