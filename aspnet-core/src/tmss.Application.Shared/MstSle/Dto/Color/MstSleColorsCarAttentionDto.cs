﻿namespace tmss.MstSle.Dto.Color
{
    public class MstSleColorsCarAttentionDto
    {
        public string VnName { get; set; }
        public string EnName { get; set; }
        public string Description { get; set; }
    }
}
