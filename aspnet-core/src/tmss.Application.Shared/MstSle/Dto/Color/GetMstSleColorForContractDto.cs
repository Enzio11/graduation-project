﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Color
{
    public class GetMstSleColorForContractDto : EntityDto<long?>
    {
        public string VnName { get; set; }
    }
}
