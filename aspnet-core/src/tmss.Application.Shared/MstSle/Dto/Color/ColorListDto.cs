using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Color
{
    public class ColorListDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}