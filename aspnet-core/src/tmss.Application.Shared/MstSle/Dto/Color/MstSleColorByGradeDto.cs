﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Color
{
    public class MstSleColorByGradeDto : EntityDto<long>
    {
        public string Code { get; set; }
    }
}
