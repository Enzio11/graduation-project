﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.Dto.Color
{
    public class GetListMstSleColorCodeDto : EntityDto<long>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string VnName { get; set; }

        [StringLength(255)]
        public string EnName { get; set; }
        [StringLength(7)]
        public string HexCode { get; set; }
    }
}
