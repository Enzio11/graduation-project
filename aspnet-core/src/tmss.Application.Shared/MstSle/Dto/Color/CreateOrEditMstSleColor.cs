﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Color
{
    public class CreateOrEditMstSleColorDto : EntityDto<long?>
    {
        public string Code { get; set; }
        public string HexCode { get; set; }
        public string VnName { get; set; }
        public string EnName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }
        public decimal? OrderingRpt { get; set; }
        public string HexCode2 { get; set; }
    }
}
