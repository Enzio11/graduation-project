﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.District
{
    public class GetDistricContractDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
