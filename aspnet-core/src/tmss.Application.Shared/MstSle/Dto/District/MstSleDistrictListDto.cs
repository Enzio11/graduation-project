﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.District
{
    public class MstSleDistrictListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int MstSleProvinceId { get; set; }
    }
}
