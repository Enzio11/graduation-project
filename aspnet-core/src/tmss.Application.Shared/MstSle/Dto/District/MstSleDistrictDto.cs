﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.District
{
    public class MstSleDistrictDto : Entity<int>
    {

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
        public long? ProvinceId { get; set; }

        public decimal? DealerId { get; set; }

        public string Status { get; set; }

        public decimal? Ordering { get; set; }

    }
}
