﻿namespace tmss.MstSle.Dto.District
{
    public class MstSleDistrictByProvinceListDto
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? ProvinceId { get; set; }
        public decimal? DealerId { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }

    }
}
