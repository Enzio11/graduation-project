﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Model
{
    public class MstSleGradeByModelListDto : FullAuditedEntityDto<long>
    {
        public string MarketingCode { get; set; }

        public string ProductionCode { get; set; }

        public string EnName { get; set; }

        public string VnName { get; set; }

        public long ModelId { get; set; }
    }
}
