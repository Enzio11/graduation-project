﻿namespace tmss.MstSle.Dto.Model
{
    public class GetEColorId
    {
        public long GradeId { get; set; }
        public long EColorId { get; set; }
    }
}
