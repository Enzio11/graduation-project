using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Model
{
    public class ModelListDto : EntityDto<long>
    {
        public string EnName { get; set; }
        public string MarketingCode { get; set; }
    }
}