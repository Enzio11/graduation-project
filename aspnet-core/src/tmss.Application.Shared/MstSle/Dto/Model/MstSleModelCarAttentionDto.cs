﻿namespace tmss.MstSle.Dto.Model
{
    public class MstSleModelCarAttentionDto
    {
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string ProductionCode { get; set; }
    }
}
