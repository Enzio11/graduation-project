﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Model
{
    public class GetAllMstSleModelInput : PagedAndSortedResultRequestDto
    {
        public string TextFilter { get; set; }
    }
}
