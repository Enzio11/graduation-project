﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Model
{
    public class MstSleMakeOfModelListDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
