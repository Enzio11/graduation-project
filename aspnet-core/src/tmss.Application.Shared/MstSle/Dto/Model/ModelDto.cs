﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Model
{
    public class ModelDto : EntityDto<long>
    {
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }
        public decimal? OrderingRpt { get; set; }
        public long? VRFee { get; set; }
        public long? CertificationFee { get; set; }
        public long? PercentBalloon { get; set; }
        public long? Pickup { get; set; }

    }
}
