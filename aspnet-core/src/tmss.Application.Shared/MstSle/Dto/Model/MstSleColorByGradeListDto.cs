﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Model
{
    public class MstSleColorByGradeListDto : EntityDto<long>
    {
        public virtual string Code { get; set; }

        public virtual string VnName { get; set; }

        public virtual string EnName { get; set; }

        public virtual string Description { get; set; }
    }
}
