﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ExpectedDelTiming
{
    public class GetMstSleExpectedDelTimingInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
