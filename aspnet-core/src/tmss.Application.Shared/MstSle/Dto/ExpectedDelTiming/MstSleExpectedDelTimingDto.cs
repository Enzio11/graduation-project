﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.ExpectedDelTiming
{
    public class MstSleExpectedDelTimingDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
