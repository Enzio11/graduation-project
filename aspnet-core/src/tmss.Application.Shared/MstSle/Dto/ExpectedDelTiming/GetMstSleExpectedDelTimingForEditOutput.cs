﻿namespace tmss.MstSle.Dto.ExpectedDelTiming
{
    public class GetMstSleExpectedDelTimingForEditOutput
    {
        public CreateOrEditMstSleExpectedDelTimingDto MstSleExpectedDelTiming { get; set; }
    }
}
