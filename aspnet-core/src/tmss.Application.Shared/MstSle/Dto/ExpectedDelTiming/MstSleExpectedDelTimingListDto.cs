﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ExpectedDelTiming
{
    public class MstSleExpectedDelTimingListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
