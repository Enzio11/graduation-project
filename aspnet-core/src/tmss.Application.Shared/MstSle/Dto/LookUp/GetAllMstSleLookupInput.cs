﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.LookUp
{
    public class GetAllMstSleLookupInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string CodeFilter { get; set; }

        public string NameFilter { get; set; }

        public string StatusFilter { get; set; }

        public string DescriptionFilter { get; set; }

        public int? ValueFilter { get; set; }
    }
}