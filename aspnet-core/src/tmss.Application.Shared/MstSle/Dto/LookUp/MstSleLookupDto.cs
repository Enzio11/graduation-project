﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.LookUp
{
    public class MstSleLookupDto : EntityDto<int?>
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string Description { get; set; }

        public decimal? Ordering { get; set; }

        public int? Value { get; set; }
    }
}