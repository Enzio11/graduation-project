﻿namespace tmss.MstSle.Dto.LookUp
{
    public class GetListLookUpDto
    {
        public string Name { get; set; }

        public int? Value { get; set; }
    }
}
