﻿namespace tmss.MstSle.Dto.LookUp
{
    public class GetMstSleLookupForEditOutput
    {
        public CreateOrEditMstSleLookupDto MstSleLookup { get; set; }
    }
}