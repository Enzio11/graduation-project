﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.LookUp
{
    public class GetLookUpValueDto : EntityDto<int?>
    {
        public string Name { get; set; }
        public decimal? Ordering { get; set; }
        public decimal? Value { get; set; }
    }
}
