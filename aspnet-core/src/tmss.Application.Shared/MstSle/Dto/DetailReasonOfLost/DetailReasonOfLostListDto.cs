using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.DetailReasonOfLost
{
    public class DetailReasonOfLostListDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}