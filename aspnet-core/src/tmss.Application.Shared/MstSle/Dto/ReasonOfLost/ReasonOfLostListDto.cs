using Abp.Application.Services.Dto;


namespace tmss.MstSle.Dto.ReasonOfLost
{
    public class ReasonOfLostListDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}