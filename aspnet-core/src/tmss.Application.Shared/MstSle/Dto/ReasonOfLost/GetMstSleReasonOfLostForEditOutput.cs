﻿

namespace tmss.MstSle.Dto.ReasonOfLost
{
    public class GetMstSleReasonOfLostForEditOutput
    {
        public CreateOrEditMstSleReasonOfLostDto mstSleReasonOfLostDto { get; set; }
    }
}
