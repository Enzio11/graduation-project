﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfLost
{
    public class GetMstSleReasonOfLostInputDto : PagedAndSortedResultRequestDto
    {
        public long? TypeId { get; set; }
    }
}
