﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ContactBy
{
    public class GetAllMstSleContactByInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
    }
}
