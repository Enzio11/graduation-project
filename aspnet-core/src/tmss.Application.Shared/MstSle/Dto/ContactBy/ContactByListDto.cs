﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ContactBy
{
    public class ContactByListDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
