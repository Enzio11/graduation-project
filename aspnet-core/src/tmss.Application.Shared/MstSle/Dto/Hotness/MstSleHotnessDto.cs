﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Hotness
{
    public class MstSleHotnessDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }
    }
}
