﻿namespace tmss.MstSle.Dto.Hotness
{
    public class GetMstSleHotnessForEditOutput
    {
        public CreateOrEditMstSleHotnessDto MstSleHotness { get; set; }
    }
}
