﻿using Abp.Application.Services.Dto;
namespace tmss.MstSle.Dto.Hotness
{
    public class MstSleHotnessListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
