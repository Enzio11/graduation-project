﻿
using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Hotness
{
    public class GetMstSleHotnessInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
