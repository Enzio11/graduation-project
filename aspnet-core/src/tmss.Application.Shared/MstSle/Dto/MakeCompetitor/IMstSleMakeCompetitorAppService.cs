﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.MakeCompetitor.Dto;

namespace tmss.MstSle.Dto.MakeCompetitor
{
    public interface IMstSleMakeCompetitorAppService
    {
        Task<PagedResultDto<MstSleMakeCompetitorDto>> GetAllMstSleMakeCompetitor(GetAllMstSleMakeCompetitorInput input);
    }
}
