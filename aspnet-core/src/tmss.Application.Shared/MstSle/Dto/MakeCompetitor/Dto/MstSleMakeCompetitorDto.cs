﻿using Abp.Domain.Entities;
namespace tmss.MstSle.Dto.MakeCompetitor.Dto
{
    public class MstSleMakeCompetitorDto : Entity<long>
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public long? Ordering { get; set; }
    }
}
