﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.MakeCompetitor.Dto
{
    public class CreateOrEditMstSleMakeCompetitorDto : EntityDto<long?>
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public long? Ordering { get; set; }
    }
}
