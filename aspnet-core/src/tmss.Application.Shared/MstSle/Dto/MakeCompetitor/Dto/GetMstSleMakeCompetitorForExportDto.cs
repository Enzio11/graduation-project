﻿namespace tmss.MstSle.Dto.MakeCompetitor.Dto
{
    public class GetMstSleMakeCompetitorForExportDto
    {
        public string Description { get; set; }
        public string Code { get; set; }
        public long? Ordering { get; set; }
    }
}
