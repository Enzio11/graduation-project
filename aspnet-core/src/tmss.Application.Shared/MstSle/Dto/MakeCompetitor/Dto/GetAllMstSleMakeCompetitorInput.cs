﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.MakeCompetitor.Dto
{
    public class GetAllMstSleMakeCompetitorInput : PagedAndSortedResultRequestDto
    {
        public string TextFilter { get; set; }
    }
}
