﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.SocialChannel
{
    public class MstSleSocialChannelListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
