﻿namespace tmss.MstSle.Dto.SocialChannel
{
    public class GetMstSleSocialChannelForEditOutput
    {
        public CreateOrEditMstSleSocialChannelDto MstSleSocialChannel { get; set; }
    }
}
