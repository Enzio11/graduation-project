﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.SocialChannel
{
    public class GetMstSleSocialChannelForContractDto : EntityDto<long?>
    {
        public string Vi_Description { get; set; }
    }
}
