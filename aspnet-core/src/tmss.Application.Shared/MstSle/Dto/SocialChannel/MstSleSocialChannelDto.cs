﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.SocialChannel
{
    public class MstSleSocialChannelDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
