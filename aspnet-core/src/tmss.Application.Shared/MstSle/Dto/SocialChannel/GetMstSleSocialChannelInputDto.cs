﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.SocialChannel
{
    public class GetMstSleSocialChannelInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
