﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.Province
{
    public class MstSleProvinceDto : Entity<int>
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public decimal? Ordering { get; set; }
        public string Status { get; set; }
        public decimal RegionId { get; set; }

        public decimal? PopulationAmount { get; set; }
        public decimal? PercentRegistrationFee { get; set; }
        public decimal? RegistrationFee { get; set; }

        public decimal? SquareAmount { get; set; }
        public decimal? DealerId { get; set; }
        public long? SubRegionId { get; set; }
    }
}
