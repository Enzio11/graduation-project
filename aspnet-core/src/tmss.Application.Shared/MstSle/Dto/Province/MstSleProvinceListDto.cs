﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Province
{
    public class MstSleProvinceListDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
