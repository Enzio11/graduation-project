﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.AgeRange
{
    public class MstSleAgeRangeDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
