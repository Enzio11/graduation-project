﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.AgeRange
{
    public class MstSleAgeRangeListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
