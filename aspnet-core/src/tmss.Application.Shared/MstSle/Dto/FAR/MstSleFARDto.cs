﻿namespace tmss.MstSle.Dto.FAR
{
    public class MstSleFARDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
