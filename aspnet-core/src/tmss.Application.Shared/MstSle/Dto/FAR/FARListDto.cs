using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.FAR
{
    public class FARListDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}