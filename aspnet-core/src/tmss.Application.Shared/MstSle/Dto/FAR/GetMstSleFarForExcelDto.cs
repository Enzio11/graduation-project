﻿namespace tmss.MstSle.Dto.FAR
{
    public class GetMstSleFarForExcelDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Vi_Description { get; set; }
        public long? Ordering { get; set; }
    }
}
