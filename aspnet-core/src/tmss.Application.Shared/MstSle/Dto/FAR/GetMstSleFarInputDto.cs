﻿using Abp.Application.Services.Dto;


namespace tmss.MstSle.Dto.FAR
{
    public class GetMstSleFarInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
