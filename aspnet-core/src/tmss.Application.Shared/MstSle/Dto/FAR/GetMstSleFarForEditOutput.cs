﻿namespace tmss.MstSle.Dto.FAR
{
    public class GetMstSleFarForEditOutput
    {
        public CreateOrEditMstSleFarDto mstSleFar { get; set; }
    }
}
