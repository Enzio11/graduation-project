﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.SourceOfInfo
{
    public class MstSleSourceOfInfoDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
