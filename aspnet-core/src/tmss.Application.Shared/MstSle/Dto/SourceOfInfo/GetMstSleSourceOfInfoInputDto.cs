﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.SourceOfInfo
{
    public class GetMstSleSourceOfInfoInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
