﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.SourceOfInfo
{
    public class GetMstSleSourceOfInfoForContractDto : EntityDto<long?>
    {
        public string Vi_Description { get; set; }
    }
}
