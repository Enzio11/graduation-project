﻿namespace tmss.MstSle.Dto.SourceOfInfo
{
    public class GetMstSleSourceOfInfoForEditOutput
    {
        public CreateOrEditMstSleSourceOfInfoDto MstSleSourceOfInfo { get; set; }
    }
}
