﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Hobby
{
    public class GetMstSleHobbyInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
