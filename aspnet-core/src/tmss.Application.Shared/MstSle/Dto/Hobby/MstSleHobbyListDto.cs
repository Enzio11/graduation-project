﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Hobby
{
    public class MstSleHobbyListDto : FullAuditedEntityDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
