﻿namespace tmss.MstSle.Dto.Hobby
{
    public class GetMstSleHobbyForEditOutput
    {
        public CreateOrEditMstSleHobbyDto MstSleHobby { get; set; }
    }
}
