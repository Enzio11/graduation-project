﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.Hobby
{
    public class GetMstSleHobbyForContractDto : EntityDto<long?>
    {
        public string Vi_Description { get; set; }
    }
}
