﻿using Abp.Domain.Entities;

namespace tmss.MstSle.Dto.Hobby
{
    public class MstSleHobbyDto : Entity<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
