﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfLostType
{
    public class GetMstSleReasonOfLostTypeInputDto : PagedAndSortedResultRequestDto
    {
        public string filterText { get; set; }
    }
}
