﻿
namespace tmss.MstSle.Dto.ReasonOfLostType
{
    public class GetMstSleReasonOfLostTypeForEditOutput
    {
        public CreateOrEditMstSleReasonOfLostTypeDto MstSleReasonOfLostTypeDto { get; set; }
    }
}
