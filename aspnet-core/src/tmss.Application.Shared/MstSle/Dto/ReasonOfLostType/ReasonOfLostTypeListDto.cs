﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.ReasonOfLostType
{
    public class ReasonOfLostTypeListDto : EntityDto<long>
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}
