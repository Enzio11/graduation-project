﻿namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class GetMstSleTestDriveVehicleForEditOutput
    {
        public CreateOrEditMstSleTestDriveVehicleDto MstSleTestDriveVehicle { get; set; }
    }
}
