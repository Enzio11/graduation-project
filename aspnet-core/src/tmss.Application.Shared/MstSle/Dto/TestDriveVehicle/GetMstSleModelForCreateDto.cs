﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using tmss.MstSle.Dto.Grade;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class GetMstSleModelForCreateDto : EntityDto<long>
    {
        public string MarketingCode { get; set; }
        public List<MstSleGradesByModelDto> ListGradeByModel { get; set; }
    }
}
