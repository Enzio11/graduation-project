﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class MstSleTestDriveVehicleListDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string Dealer { get; set; }
        public string Description { get; set; }
        public string Model { get; set; }
        public string Grade { get; set; }
        public string Color { get; set; }
    }
}
