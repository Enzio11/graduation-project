﻿namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class TestDriverViehicleCusApp
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? DealerId { get; set; }
        public string Description { get; set; }
        public long? ModelId { get; set; }
        public long? GradeId { get; set; }
        public long? ColorId { get; set; }
    }
}
