﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class GetTestDriveVehicleNameDto : EntityDto<long?>
    {
        public string Name { get; set; }
    }
}
