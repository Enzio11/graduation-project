using System.Collections.Generic;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class TestDriverGroupByDealer
    {
        public string DealerName { get; set; }
#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
        public List<TestDriverViehicleCusApp>? ListVehicle { get; set; }
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
    }
}
