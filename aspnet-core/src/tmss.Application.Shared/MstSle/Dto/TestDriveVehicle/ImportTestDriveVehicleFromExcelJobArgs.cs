﻿using Abp;
using System;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class ImportTestDriveVehicleFromExcelJobArgs
    {
        public int? TenantId { get; set; }
        public Guid BinaryObjectId { get; set; }
        public UserIdentifier User { get; set; }
    }
}
