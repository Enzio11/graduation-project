﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class GetAllTenantListDto : EntityDto<long>
    {
        public string Abbreviation { get; set; }
    }
}
