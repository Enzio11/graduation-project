﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class TestDriveVehicleListDto : EntityDto<long>
    {
        public string Name { get; set; }
    }
}
