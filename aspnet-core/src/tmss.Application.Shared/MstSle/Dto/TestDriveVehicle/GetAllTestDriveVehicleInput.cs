﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Dto.TestDriveVehicle
{
    public class GetAllTestDriveVehicleInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
        public long? DealerIdFilter { get; set; }
    }
}
