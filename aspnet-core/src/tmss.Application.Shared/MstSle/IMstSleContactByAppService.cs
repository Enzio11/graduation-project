﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.ContactBy;

namespace tmss.MstSle
{
    public interface IMstSleContactByAppService : IApplicationService
    {
        Task<PagedResultDto<MstSleContactByDto>> GetAllContactBy(GetAllMstSleContactByInput input);

        Task<GetMstSleContactByForEditOutputDto> GetContactByForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditMstSleContactByDto input);

        Task Delete(EntityDto input);
    }
}
