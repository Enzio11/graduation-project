﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleDistrict.Dto;

namespace tmss.MstSle
{
    public interface IMstSleDistrictAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleDistrictsForViewDto>> GetAll(GetAllMstSleDistrictsInput input);
        Task<GetMstSleDistrictsForEditOutput> GetMstSleDistrictsForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditMstSleDistrictsDto input);
        Task Delete(EntityDto input);
        Task<PagedResultDto<GetMstSleDistrictsForViewDto>> GetDistrictsByProvinceId(GetFilterDistricts input);
    }
}
