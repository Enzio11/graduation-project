﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.LookUp;

namespace tmss.MstSle
{
    public interface IMstSleLookupAppService : IApplicationService
    {
        Task<PagedResultDto<MstSleLookupDto>> GetAll(GetAllMstSleLookupInput input);
        Task<GetMstSleLookupForEditOutput> GetMstSleLookupForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditMstSleLookupDto input);
        Task Delete(EntityDto input);
    }
}