﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.ReasonOfLost;

namespace tmss.MstSle
{
    public interface IMstSleReasonofLostAppService
    {
        Task<PagedResultDto<GetMstSleReasonOfLostForViewDto>> GetAll(GetMstSleReasonOfLostInputDto input);
        Task<GetMstSleReasonOfLostForEditOutput> GetMstSleReasonOfLostForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleReasonOfLostDto input);
        Task Delete(EntityDto<long> input);
    }
}
