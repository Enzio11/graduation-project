﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetMstGenDealersForOrgChartDto : EntityDto<long>
    {

        public decimal? DealerParentsId { get; set; }
        public string DealerParents { get; set; }
        public string Code { get; set; }
        public string VnName { get; set; }
        public string EnName { get; set; }
        public string Address { get; set; }
        public string Abbreviation { get; set; }
    }
}