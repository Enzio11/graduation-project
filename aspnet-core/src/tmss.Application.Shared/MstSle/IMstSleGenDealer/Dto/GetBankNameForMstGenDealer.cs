﻿
namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetBankNameForMstGenDealer
    {
        public long Id { get; set; }
        public string BankName { get; set; }
        public string Address { get; set; }


    }
}
