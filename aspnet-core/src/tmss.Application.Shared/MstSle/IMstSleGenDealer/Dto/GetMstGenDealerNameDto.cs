﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetMstGenDealerNameDto : EntityDto<long>
    {
        public string VnName { get; set; }

        public bool IsChecked { get; set; }
    }
}
