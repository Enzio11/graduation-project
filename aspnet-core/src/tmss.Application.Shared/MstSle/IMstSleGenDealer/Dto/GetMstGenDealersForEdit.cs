﻿namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetMstGenDealersForEditOutput
    {
        public CreateOrEditMstGenDealersDto MstGenDealers { get; set; }
    }
}