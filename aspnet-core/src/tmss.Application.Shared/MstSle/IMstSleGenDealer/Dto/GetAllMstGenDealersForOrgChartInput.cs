﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetAllMstGenDealersForOrgChartInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
    }
}