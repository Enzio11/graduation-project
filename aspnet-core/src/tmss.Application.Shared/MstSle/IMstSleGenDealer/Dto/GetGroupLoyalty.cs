﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetGroupLoyalty : EntityDto<long?>
    {
        public string GroupName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }
        public virtual int? GroupTypeId { get; set; }
    }
}
