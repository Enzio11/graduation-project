﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto


{
    public class GetMstGenDealersForViewDto : EntityDto<long>
    {
        public string DealerType { get; set; }
        public string ProvinceName { get; set; }
        public string DealerGroupName { get; set; }
        public string SwapGroupName { get; set; }
        public string DispatchGroupName { get; set; }
        public string ArrivalGroupName { get; set; }
        public string AuthorizationInfo { get; set; }
        public string DealerParent { get; set; }
        public string Code { get; set; }
        public string AccountNo { get; set; }
        public string Bank { get; set; }
        public string VnName { get; set; }
        public string EnName { get; set; }
        public string Address { get; set; }
        public string Abbreviation { get; set; }
        public string ContactPerson { get; set; }
        public string Phone { get; set; }
        public string Status { get; set; }
        public string Fax { get; set; }
        public string Description { get; set; }
        public string IsSpecial { get; set; }
        public decimal? Ordering { get; set; }
        public string TaxCode { get; set; }
        public string IsSumDealer { get; set; }
        public string BiServer { get; set; }
        public decimal? TfsAmount { get; set; }
        public decimal? PartLeadtime { get; set; }
        public string IpAddress { get; set; }
        public string Islexus { get; set; }
        public string IsSellLexusPart { get; set; }
        public string IsDlrSales { get; set; }
        public string RecievingAddress { get; set; }
        public string DlrFooter { get; set; }
        public string IsPrint { get; set; }
        public string PasswordSearchVin { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }

        public decimal? PortRegion { get; set; }
        public decimal? DealerId { get; set; }
        public decimal? ProvinceId { get; set; }
        public decimal? DistrictId { get; set; }
        public decimal? DealerTypeId { get; set; }
        public decimal? DealerGroupId { get; set; }
        public decimal? SwapGroupId { get; set; }
        public decimal? DispatchGroupId { get; set; }
        public decimal? ArrivalGroupId { get; set; }
        public decimal? DealerParentsId { get; set; }
        public decimal? IsDealer { get; set; }
        public string ContractRole { get; set; }
        public decimal? GroupManagerId { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Long { get; set; }
        public string MapUrl { get; set; }
        public string LinkWebsite { get; set; }
        public string OperatingTime { get; set; }
        public bool? IsUsingApp { get; set; }
        public long? GroupInsuranceId { get; set; }
        public decimal? GroupLoyaltyId { get; set; }
    }
}