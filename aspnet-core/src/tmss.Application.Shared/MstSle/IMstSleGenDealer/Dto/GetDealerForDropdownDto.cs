﻿namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetMstGenDealerForDropdownDto
    {
        public long Id { get; set; }
        public string Abbreviation { get; set; }
        public string Code { get; set; }
        public string Address { get; set; }
    }
}
