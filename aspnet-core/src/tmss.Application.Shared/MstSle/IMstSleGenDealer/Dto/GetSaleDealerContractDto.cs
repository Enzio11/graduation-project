﻿namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class GetSaleDealerContractDto
    {
        public long? id { get; set; }
        public string VnName { get; set; }
        public string EnName { get; set; }
        public string Abbreviation { get; set; }

    }
}
