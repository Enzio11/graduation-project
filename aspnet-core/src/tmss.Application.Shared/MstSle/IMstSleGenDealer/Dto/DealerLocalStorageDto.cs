﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto
{
    public class DealerLocalStorageDto : EntityDto<long?>
    {
        public string TenancyName { get; set; }
        public string TenantCode { get; set; }
        public string Name { get; set; }
        public string TenantNameVN { get; set; }
    }
}
