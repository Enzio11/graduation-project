﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGenDealer.Dto

{
    public class MstSleDealerDto : EntityDto<int>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
