﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleRelationship.Dto
{
    public class MstSleRelationshipDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
