﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.BusinessType;

namespace tmss.MstSle
{
    public interface IMstSleBusinessTypeAppService : IApplicationService
    {
        public ListResultDto<MstSleBusinessTypeDto> GetListBusinessType();
        Task<PagedResultDto<GetMstSleBusinessTypeForViewDto>> GetAll(GetMstSleBusinessTypeInputDto input);
        Task CreateOrEdit(CreateOrEditMstSleBusinessTypeDto input);
        Task<GetMstSleBusinessTypeForEditOutput> GetMstSleBusinessTypeForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
    }
}

