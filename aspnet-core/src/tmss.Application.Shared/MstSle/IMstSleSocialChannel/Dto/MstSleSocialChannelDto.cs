﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleSocialChannel.Dto
{
    public class MstSleSocialChannelDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
