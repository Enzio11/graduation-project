﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.TestDriveStall;

namespace tmss.MstSle
{
    public interface IMstSleTestDriveStallAppService
    {
        Task CreateOrEdit(CreateOrEditMstSleTestDriveStallDto input);
        Task<PagedResultDto<MstSleTestDriveStallListDto>> GetListViewTestDriveStall(GetAllTestDriveStallInput input);
        Task<GetMstSleTestDriveStallForEditOutput> GetMstSleTestDriveStallForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
    }
}
