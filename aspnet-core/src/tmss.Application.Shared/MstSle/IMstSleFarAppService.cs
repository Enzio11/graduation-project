﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.FAR;

namespace tmss.MstSle
{
    public interface IMstSleFarAppService
    {
        Task<PagedResultDto<GetMstSleFarForViewDto>> GetAll(GetMstSleFarInputDto input);
        Task<GetMstSleFarForEditOutput> GetMstSleFarForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleFarDto input);
        Task Delete(EntityDto<long> input);
    }
}
