﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.MstSle.Dto.LookUp;
using tmss.MstSle.Dto.ReasonOfNA;

namespace tmss.MstSle
{
    public interface IMstSleReasonOfNAAppService : IApplicationService
    {
        Task CreateOrEdit(CreateOrEditMstSleReasonOfNADto input);
        Task<GetMstSleReasonOfNAForEditOutput> GetMstSleReasonOfNAForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
        Task<List<GetLookUpValueDto>> GetLookUpValue(string Code);
    }
}
