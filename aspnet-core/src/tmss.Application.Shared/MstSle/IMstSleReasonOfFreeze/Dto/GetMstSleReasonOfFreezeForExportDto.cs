﻿namespace tmss.MstSle.IMstSleReasonOfFreeze.Dto
{
    public class GetMstSleReasonOfFreezeForExportDto
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string ViDescription { get; set; }
        public long? Ordering { get; set; }
    }
}
