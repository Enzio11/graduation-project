﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.IMstSleReasonOfFreeze.Dto
{
    public class GetMstSleReasonOfFreezeForViewDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string Code { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(255)]
        public string ViDescription { get; set; }
        public long? Ordering { get; set; }
    }
}
