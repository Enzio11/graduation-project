﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleReasonOfFreeze.Dto
{
    public class GetMstSleReasonOfFreezeInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
    }
}
