﻿namespace tmss.MstSle.IMstSleReasonOfFreeze.Dto
{
    public class GetMstSleReasonOfFreezeForEditOutput
    {
        public CreateOrEditMstSleReasonOfFreezeDto MstSleReasonOfFreezeDto { get; set; }
    }
}
