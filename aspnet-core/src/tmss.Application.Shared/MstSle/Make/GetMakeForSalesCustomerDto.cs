﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.Make
{
    public class GetMakeForSalesCustomerDto : EntityDto<long?>
    {
        public string Description { get; set; }
    }
}
