﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMtSleColor.Dto
{
    public class GetColorCodeDto : EntityDto<long?>
    {
        public string Code { get; set; }
    }
}
