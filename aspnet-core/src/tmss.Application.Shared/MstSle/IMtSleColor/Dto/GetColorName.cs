﻿namespace tmss.MstSle.IMtSleColor.Dto
{
    public class GetColorName
    {
        public long? ColorId { get; set; }
        public string ColorName { get; set; }
        public string ColorVNName { get; set; }
        public string ColorCode { get; set; }
    }
}
