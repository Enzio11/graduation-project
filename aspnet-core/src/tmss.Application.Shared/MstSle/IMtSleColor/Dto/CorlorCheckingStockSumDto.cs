﻿namespace tmss.MstSle.IMtSleColor.Dto
{
    public class CorlorCheckingStockSumDto
    {
        public string VnName { get; set; }
        public string EnName { get; set; }
        public int? SumReal { get; set; }
        public string HexCode { get; set; }
    }
}
