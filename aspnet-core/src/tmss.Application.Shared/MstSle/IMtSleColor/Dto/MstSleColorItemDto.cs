﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMtSleColor.Dto
{
    public class MstSleColorItemDto : EntityDto<long>
    {
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string HexCode { get; set; }
        public string Code { get; set; }

    }
}
