﻿namespace tmss.MstSle.IMtSleColor.Dto
{
    public class MstSleColorGetStockCheckingDto
    {
        public string EnName { get; set; }
        public string VnName { get; set; }
        public int? Instock { get; set; }
        public int? OnGoing { get; set; }
        public string HexCode { get; set; }
    }
}
