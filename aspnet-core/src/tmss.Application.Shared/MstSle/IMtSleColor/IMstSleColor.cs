﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.Color;

namespace tmss.MstSle.IMtSleColor
{
    public interface IMstSleColorsAppService : IApplicationService
    {
        Task<PagedResultDto<MstSleColorDto>> GetAllMstSleColor(GetAllMstSleColorInput input);
        //Task<GetMstSleColorsForEditOutput> GetMstSleColorsForEdit(EntityDto input);
        //Task CreateOrEdit(CreateOrEditMstSleColorsDto input);
        //Task Delete(EntityDto input);
        //Task<FileDto> GetMstSleColorToExcel(GetAllMstSleColorsInput input);
    }
}
