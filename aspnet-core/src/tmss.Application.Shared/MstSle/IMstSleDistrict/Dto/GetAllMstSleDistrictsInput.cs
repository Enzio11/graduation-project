﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class GetAllMstSleDistrictsInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string CodeFilter { get; set; }
        public string NameFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public int? ProvinceIdFilter { get; set; }
        public int? DealerIdFilter { get; set; }
        public string StatusFilter { get; set; }
        public decimal? OrderingFilter { get; set; }
    }
}
