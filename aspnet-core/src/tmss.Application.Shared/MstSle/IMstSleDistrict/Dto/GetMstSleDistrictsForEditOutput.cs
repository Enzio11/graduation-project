﻿namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class GetMstSleDistrictsForEditOutput
    {
        public CreateOrEditMstSleDistrictsDto MstSleDistricts { get; set; }
    }
}
