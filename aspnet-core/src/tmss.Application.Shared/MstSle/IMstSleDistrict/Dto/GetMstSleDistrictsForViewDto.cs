﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class GetMstSleDistrictsForViewDto : EntityDto<long>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }
        public long? ProvinceId { get; set; }

        public long? DealerId { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(255)]
        public string ProvinceName { get; set; }
    }
}
