﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class GetFilterDistricts : PagedAndSortedResultRequestDto
    {
        public int? Id { get; set; }
        public string CodeFilter { get; set; }
        public string NameFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public string StatusFilter { get; set; }
        public decimal? OrderingFilter { get; set; }
    }
}
