﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class GetCodeDistrictDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
