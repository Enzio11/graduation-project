﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class MstSleDistrictsDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public long? ProvinceId { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public int? DealerId { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }
    }
}
