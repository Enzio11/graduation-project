﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleDistrict.Dto
{
    public class GetDistrictByProviceIdDto : EntityDto<long>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PopulationAmount { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? SquareAmount { get; set; }


        public int? SubRegionId { get; set; }

        public List<MstSleDistrictsDto> ListDistrict { get; set; }
    }
}
