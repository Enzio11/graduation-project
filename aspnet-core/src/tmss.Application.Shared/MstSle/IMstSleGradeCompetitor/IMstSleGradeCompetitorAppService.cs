﻿using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleGradeCompetitor.Dto;

namespace tmss.MstSle.IMstSleGradeCompetitor
{
    public interface IMstSleGradeCompetitorAppService
    {
        Task<List<MstSleGradeCompetitorDto>> GetListModel();
    }
}
