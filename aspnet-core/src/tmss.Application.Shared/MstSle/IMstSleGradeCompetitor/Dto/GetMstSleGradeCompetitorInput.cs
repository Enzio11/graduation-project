﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGradeCompetitor.Dto
{
    public class GetMstSleGradeCompetitorInput : PagedAndSortedResultRequestDto
    {
        public long? ModelId { get; set; }
        public string TextFilter { get; set; }
    }
}
