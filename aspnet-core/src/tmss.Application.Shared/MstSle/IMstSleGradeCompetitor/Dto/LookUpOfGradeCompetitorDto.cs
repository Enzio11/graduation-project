﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGradeCompetitor.Dto
{
    public class MstLookUpDto : EntityDto<long>
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
