﻿using Abp.Application.Services.Dto;
using tmss.MstSle.Dto.Source;

namespace tmss.MstSle
{
    public interface IMstSleSourceAppService
    {
        ListResultDto<MstSleSourceListDto> GetListSource(MstSleSourceInput input);
    }
}
