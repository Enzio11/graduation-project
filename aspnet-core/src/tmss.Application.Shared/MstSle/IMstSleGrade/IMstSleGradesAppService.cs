﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleGrade.Dto;

namespace tmss.MstSle.IMstSleGrade
{
    public interface IMstSleGradesAppService : IApplicationService
    {
        Task<List<ListGradeNameDto>> GetListGrade();
        Task<PagedResultDto<GetMstSleGradesForViewDto>> GetAllGradeByModels(GetAllGradesByModelId input);
        Task<GetMstSleGradesForEditOutput> GetMstSleGradesForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditMstSleGradesDto input);
        Task Delete(EntityDto input);
        Task<List<ListGradeCodeDto>> GetListGradeCkdVehicle();
        Task<List<ListGradeCodeDto>> GetListGradeCbuVehicle();
    }
}
