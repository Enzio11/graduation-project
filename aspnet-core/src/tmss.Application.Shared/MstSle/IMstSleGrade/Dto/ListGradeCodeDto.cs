﻿namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class ListGradeCodeDto
    {
        public long? Id { get; set; }
        public string MarketingCode { get; set; }
        public long? ModelId { get; set; }
    }
}
