﻿using System.Collections.Generic;
using tmss.MstSle.IMstSleGradeProduction.Dto;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class GetListGradeNameDto
    {
        public long? GradeId { get; set; }
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
        public string GradeEnName { get; set; }
        public string GradeVnName { get; set; }
        public List<GetGradeProductionName> ListGradeProduction { get; set; }
    }
}
