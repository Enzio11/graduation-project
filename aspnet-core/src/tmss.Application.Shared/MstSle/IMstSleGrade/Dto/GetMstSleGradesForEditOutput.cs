﻿namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class GetMstSleGradesForEditOutput
    {
        public CreateOrEditMstSleGradesDto MstSleGrades { get; set; }
    }
}
