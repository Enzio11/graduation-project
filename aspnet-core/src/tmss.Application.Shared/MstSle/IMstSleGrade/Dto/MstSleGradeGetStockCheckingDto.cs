﻿using System.Collections.Generic;
using tmss.MstSle.IMtSleColor.Dto;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class MstSleGradeGetStockCheckingDto
    {
        public long? Id { get; set; }
        public string MarketingCode { get; set; }
        public List<MstSleColorGetStockCheckingDto> colorStockChecking { get; set; }
    }
}
