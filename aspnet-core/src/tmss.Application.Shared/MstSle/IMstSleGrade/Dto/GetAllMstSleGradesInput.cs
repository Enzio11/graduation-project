﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class GetAllMstSleGradesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string MarketingCodeFilter { get; set; }

        public string ProductionCodeFilter { get; set; }

        public string EnNameFilter { get; set; }

        public string VnNameFilter { get; set; }

        public string DeterminerFilter { get; set; }

        public string StatusFilter { get; set; }

        public decimal? OrderingFilter { get; set; }

        public string IsHasAudioFilter { get; set; }

        public string CbuCkdFilter { get; set; }

        public string ShortModelFilter { get; set; }

        public string WmiFilter { get; set; }

        public string VdsFilter { get; set; }

        public int? GasolineTypeId { get; set; }

        public DateTime? FromDateFilter { get; set; }

        public DateTime? ToDateFilter { get; set; }

        public string FullModelFilter { get; set; }

        public string BarcodeFilter { get; set; }

        public string IsFirmColorFilter { get; set; }

        public string IsHasFloormatFilter { get; set; }

        public decimal? PriceAmountFilter { get; set; }

        public string IsShowDeliveryPlanFilter { get; set; }

        public decimal? OrderPriceFilter { get; set; }
        public decimal? InsurancePayLoadFilter { get; set; }

    }
}
