﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class MstSleGradeProductInfoDto : EntityDto<long>
    {

        public long? GradeId { get; set; }
        public string OriginFrom { get; set; }

        public long? SeatNo { get; set; }

        public string FormStyle { get; set; }

        public string Fuel { get; set; }

        public string Code { get; set; }

        public string Descritpion { get; set; }
        public long? HandNo { get; set; }
    }
}
