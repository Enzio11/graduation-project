﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class MstSleGradeItemDto : EntityDto<long>
    {
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
        public string EnName { get; set; }

        public string VnName { get; set; }

    }
}
