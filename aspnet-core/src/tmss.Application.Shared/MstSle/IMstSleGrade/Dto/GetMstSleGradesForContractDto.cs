﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class GetMstSleGradesForContractDto : EntityDto<long?>
    {
        public string MarketingCode { get; set; }
        public long? ModelId { get; set; }
    }
}
