﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class GetAllGradesByModelId : PagedAndSortedResultRequestDto
    {
        public int? ModelId { get; set; }
        public string GradeFilter { get; set; }
    }
}
