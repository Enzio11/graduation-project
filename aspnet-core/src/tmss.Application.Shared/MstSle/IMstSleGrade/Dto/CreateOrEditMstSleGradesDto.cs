﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleGrade.Dto
{
    public class CreateOrEditMstSleGradesDto : EntityDto<long?>
    {
        [StringLength(25)]
        public string MarketingCode { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string VnName { get; set; }

        [StringLength(50)]
        public string Determiner { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string IsHasAudio { get; set; }

        [StringLength(50)]
        public string CbuCkd { get; set; }

        [StringLength(50)]
        public string ShortModel { get; set; }

        [StringLength(50)]
        public string Wmi { get; set; }

        [StringLength(50)]
        public string Vds { get; set; }

        [Column(TypeName = "numeric(8, 0)")]
        public decimal? GasolineTypeId { get; set; }


        public int ModelId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        [StringLength(50)]
        public string FullModel { get; set; }

        [StringLength(50)]
        public string Barcode { get; set; }

        [StringLength(1)]
        public string IsFirmColor { get; set; }

        [StringLength(1)]
        public string IsHasFloormat { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public decimal? PriceAmount { get; set; }

        [StringLength(1)]
        public string IsShowDeliveryPlan { get; set; }

        [Column(TypeName = "numeric(18, 0)")]
        public decimal? OrderPrice { get; set; }

        [Column(TypeName = "numeric(12, 3)")]
        public decimal? InsurancePayLoad { get; set; }
        [Column(TypeName = "numeric(12, 0)")]
        public long? SeatNo { get; set; }
        public bool? isPickup { get; set; }
    }
}
