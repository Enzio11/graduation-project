﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleGradeProduction.Dto;

namespace tmss.MstSle
{
    public interface IMstSleGradeProductionAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstSleGradeProducesForViewDto>> GetAll(GetAllMstSleGradeProductionInput input);

        Task<GetMstSleGradeProductionForEditOutput> GetMstSleGradeProducesForEdit(EntityDto input);

        Task CreateOrEdit(CreateOrEditMstSleGradeProductionDto input);

        Task Delete(EntityDto input);
        Task<PagedResultDto<GetMstSleGradeProducesForViewDto>> GetGradesproduceByGradesId(GetGradesProductionByGradesId input);
        Task<PagedResultDto<GetMstSleGradeProducesForViewDto>> GetAllGradesProduces(GetAllProductionInput input);
    }
}
