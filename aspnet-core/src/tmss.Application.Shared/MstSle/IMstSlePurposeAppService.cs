﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.Purpose;

namespace tmss.MstSle
{
    public interface IMstSlePurposeAppService
    {
        Task<PagedResultDto<GetMstSlePurposeForViewDto>> GetAll(GetMstSlePurposeInputDto input);
        Task<GetMstSlePurposeForEditOutput> GetMstSlePurposeForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSlePurposeDto input);
        Task Delete(EntityDto<long> input);
    }
}
