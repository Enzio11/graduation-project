﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.ReasonOfChanges;

namespace tmss.MstSle
{
    public interface IMstSleReasonOfChangesAppService
    {
        Task<PagedResultDto<GetMstSleReasonOfChangesForViewDto>> GetAll(GetMstSleReasonOfChangesInputDto input);
        Task<GetMstSleReasonOfChangesForEditOutput> GetMstSleReasonOfChangesForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleReasonOfChangesDto input);
        Task Delete(EntityDto<long> input);
    }
}
