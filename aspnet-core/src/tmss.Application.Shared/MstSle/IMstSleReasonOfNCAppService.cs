﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.Dto;
using tmss.MstSle.Dto.ReasonOfNC;

namespace tmss.MstSle
{
    public interface IMstSleReasonOfNCAppService : IApplicationService
    {
        public ListResultDto<ReasonOfNCDto> GetListReasonOfNC();
        Task<PagedResultDto<GetMstSleReasonOfNCForViewDto>> GetAll(GetMstSleReasonOfNCInputDto input);
        Task CreateOrEdit(CreateOrEditMstSleReasonOfNCDto input);
        Task<GetMstSleReasonOfNCForEditOutput> GetMstSleReasonOfNCForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
        Task<FileDto> GetMstSleReasonOfNCToExcel(GetMstSleReasonOfNCInputDto input);
    }
}
