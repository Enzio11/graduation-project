﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.SocialChannel;

namespace tmss.MstSle
{
    public interface IMstSleSocialChannelAppService : IApplicationService
    {
        ListResultDto<MstSleSocialChannelListDto> GetListSocialChannel(MstSleSocialChannelInput input);
        Task<PagedResultDto<GetMstSleSocialChannelForViewDto>> GetAll(GetMstSleSocialChannelInputDto input);
        Task CreateOrEdit(CreateOrEditMstSleSocialChannelDto input);
        Task<GetMstSleSocialChannelForEditOutput> GetMstSleSocialChannelForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
    }
}
