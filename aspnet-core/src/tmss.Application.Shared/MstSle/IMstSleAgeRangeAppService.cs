﻿using Abp.Application.Services.Dto;
using tmss.MstSle.Dto.AgeRange;

namespace tmss.MstSle
{
    public interface IMstSleAgeRangeAppService
    {
        ListResultDto<MstSleAgeRangeListDto> GetListAgeRange(MstSleAgeRangeInput input);
    }
}
