﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.Dto;
using tmss.MstSle.Dto.Model;
using tmss.MstSle.Dto.TestDriveVehicle;

namespace tmss.MstSle
{
    public interface IMstSleTestDriveVehicleAppService
    {
        ListResultDto<TestDriveVehicleListDto> GetTestDriveVehicle(long DealerId);
        Task CreateOrEdit(CreateOrEditMstSleTestDriveVehicleDto input);
        Task<PagedResultDto<MstSleTestDriveVehicleListDto>> GetListViewTestDriveVehicle(GetAllTestDriveVehicleInput input);
        Task<FileDto> GetListViewTestDriveVehicleToExcel(GetAllTestDriveVehicleInput input);
        Task<GetMstSleTestDriveVehicleForEditOutput> GetMstSleTestDriveVehicleForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
        Task<List<MstSleColorByGradeListDto>> GetColorByGradesId(long Id);
        Task<List<MstSleGradeByModelListDto>> GetGradeByModelId(long Id);
        Task<List<GetAllMstSleModelListDto>> GetAllModel();

    }
}
