﻿namespace tmss.MstSle.Color
{
    public class GetColorByGradeDto
    {
        public long? GradeId { get; set; }
        public long? ColorId { get; set; }
        public string Color { get; set; }
    }
}
