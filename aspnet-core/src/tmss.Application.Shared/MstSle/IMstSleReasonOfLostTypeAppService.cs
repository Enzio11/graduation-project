﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.ReasonOfLostType;

namespace tmss.MstSle
{
    public interface IMstSleReasonOfLostTypeAppService
    {
        Task<PagedResultDto<GetMstSleReasonOfLostTypeForViewDto>> GetAll(GetMstSleReasonOfLostTypeInputDto input);
        Task<GetMstSleReasonOfLostTypeForEditOutput> GetMstSleReasonOfLostTypeForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleReasonOfLostTypeDto input);
        Task Delete(EntityDto<long> input);
    }
}
