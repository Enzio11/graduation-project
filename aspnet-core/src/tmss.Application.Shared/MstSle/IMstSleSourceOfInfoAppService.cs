﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.SourceOfInfo;

namespace tmss.MstSle
{
    public interface IMstSleSourceOfInfoAppService
    {
        ListResultDto<MstSleSourceOfInfoDto> GetListSourceOfInfo(MstSleSourceOfInfoInput input);
        Task<PagedResultDto<GetMstSleSourceOfInfoForViewDto>> GetAll(GetMstSleSourceOfInfoInputDto input);
        Task<GetMstSleSourceOfInfoForEditOutput> GetMstSleSourceOfInfoForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleSourceOfInfoDto input);
        Task Delete(EntityDto<long> input);
    }
}
