﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleTestDrivePlace.Dto

{
    public class MstSleBanksDto : EntityDto<long>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public long? DealerId { get; set; }
    }
}
