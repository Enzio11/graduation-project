﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleTestDrivePlace.Dto

{
    public class GetAllMstSleTestDrivePlaceInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string NameFilter { get; set; }
        public string DescriptionFilter { get; set; }
        public long? DealerIdFilter { get; set; }
    }
}