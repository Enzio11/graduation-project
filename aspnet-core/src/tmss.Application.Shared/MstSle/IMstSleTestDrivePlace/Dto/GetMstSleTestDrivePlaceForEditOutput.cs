﻿namespace tmss.MstSle.IMstSleTestDrivePlace.Dto

{
    public class GetMstSleTestDrivePlaceForEditOutput
    {
        public CreateOrEditMstSleTestDrivePlaceDto MstSleTestDrivePlace { get; set; }
    }
}