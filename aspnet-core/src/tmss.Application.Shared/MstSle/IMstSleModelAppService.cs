﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleModel.Dto;

namespace tmss.MstSle
{
    public interface IMstSleModelAppService : IApplicationService
    {

        Task<PagedResultDto<GetMstSleModelsForViewDto>> GetAll(GetAllMstSleModelsInput input);
        Task<GetMstSleModelsForEditOutput> GetMstSleModelsForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditMstSleModelsDto input);
        Task Delete(EntityDto input);
    }
}
