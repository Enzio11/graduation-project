﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class MstGenProvinceDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        public int RegionId { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PopulationAmount { get; set; }

        [Column(TypeName = "numeric(12, 2)")]
        public decimal? SquareAmount { get; set; }

        public int? DealerId { get; set; }

        public int? SubRegionId { get; set; }
    }
}
