﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class GetPercentRegistrationFeeDto : EntityDto<long?>
    {
        public string Province { get; set; }
        public decimal? PercentOwnershipTax { get; set; }
        public decimal? PercentRegistrationFee { get; set; }
        public decimal? RegistrationFee { get; set; }
    }
}
