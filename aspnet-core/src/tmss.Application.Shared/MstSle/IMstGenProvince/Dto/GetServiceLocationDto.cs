﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class GetServiceLocationDto : EntityDto<long?>
    {
        public string Code { get; set; }
    }
}
