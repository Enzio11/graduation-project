﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class CreateOrEditMstGenProvinceDto : Entity<long?>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PopulationAmount { get; set; }

        [Column(TypeName = "numeric(12, 2)")]
        public decimal? SquareAmount { get; set; }

        public int SubRegionId { get; set; }
        [Column(TypeName = "numeric(5, 0)")]
        public decimal? PercentOwnershipTax { get; set; }
        [Column(TypeName = "numeric(15, 0)")]
        public long? RegistrationFee { get; set; }
        public long? GroupInsuranceId { get; set; }
        public long? ServiceLocationId { get; set; }
    }
}
