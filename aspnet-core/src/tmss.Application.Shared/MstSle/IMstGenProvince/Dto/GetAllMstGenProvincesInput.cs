﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class GetAllMstGenProvincesInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }
        public string CodeFilter { get; set; }
        public string NameFilter { get; set; }
        public decimal? OrderingFilter { get; set; }
        public string StatusFilter { get; set; }
        public int? SubRegionIdFilter { get; set; }
        public decimal? PopulationAmountMaxFilter { get; set; }
        public decimal? PopulationAmountMinFilter { get; set; }
        public decimal? SquareAmountMaxFilter { get; set; }
        public decimal? SquareAmountMinFilter { get; set; }
    }
}
