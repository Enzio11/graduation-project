﻿namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class GetMstGenProvincesForEditOutput
    {
        public CreateOrEditMstGenProvinceDto MstGenProvinces { get; set; }
    }
}
