﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class GetMstGenProvincesForViewDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(255)]
        public string Name { get; set; }
        public long RegionId { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string Status { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PopulationAmount { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? SquareAmount { get; set; }

        public long? SubRegionId { get; set; }

        [StringLength(255)]
        public string SubRegionName { get; set; }
        [Column(TypeName = "numeric(5, 0)")]
        public decimal? PercentOwnershipTax { get; set; }
        [Column(TypeName = "numeric(15, 0)")]
        public long? RegistrationFee { get; set; }
    }
}
