﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstGenProvince.Dto
{
    public class GetNameProvinceDto : EntityDto<long?>
    {
        public string Name { get; set; }
    }
}
