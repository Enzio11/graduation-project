﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.IMstGenProvince.Dto;

namespace tmss.MstSle.IMstGenProvince
{
    public interface IMstGenProvinceAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstGenProvincesForViewDto>> GetAll(GetAllMstGenProvincesInput input);
        Task<GetMstGenProvincesForEditOutput> GetMstGenProvincesForEdit(EntityDto input);
        Task CreateOrEdit(CreateOrEditMstGenProvinceDto input);
        Task Delete(EntityDto input);
    }
}
