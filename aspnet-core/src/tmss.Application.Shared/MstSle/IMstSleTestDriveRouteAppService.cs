﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.MstSle.Dto.TestDriveRoute;

namespace tmss.MstSle
{
    public interface IMstSleTestDriveRouteAppService
    {
        Task CreateOrEdit(CreateOrEditMstSleTestDriveRouteDto input);
        Task<PagedResultDto<MstSleTestDriveRouteListDto>> GetListViewTestDriveRoute(GetAllTestDriveRouteInput input);
        Task<GetMstSleTestDriveRouteForEditOutput> GetMstSleTestDriveRouteForEdit(EntityDto<long> input);
        Task Delete(EntityDto<long> input);
        Task<List<DealerCacheDto>> GetDealerForTestDrive();
    }
}
