﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;
using tmss.MstSle.IMstSleGenDealer.Dto;

namespace tmss.MstSle
{
    public interface IMstGenDealerAppService : IApplicationService
    {
        Task<PagedResultDto<GetMstGenDealersForViewDto>> GetAll(GetAllMstGenDealersInput input);
        Task CreateOrEdit(CreateOrEditMstGenDealersDto input);
        Task Delete(EntityDto<int> input);
        Task<List<DealerLocalStorageDto>> GetDealerCodeToCheck(long? inputId);
        Task<List<DealerLocalStorageDto>> GetListDealerParent(EntityDto<long?> input);
        Task<PagedResultDto<GetMstGenDealersForOrgChartDto>> GetAllDealerForOrgChart(GetAllMstGenDealersForOrgChartInput input);
    }
}
