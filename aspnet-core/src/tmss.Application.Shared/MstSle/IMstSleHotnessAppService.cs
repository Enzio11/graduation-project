﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.Hotness;

namespace tmss.MstSle
{
    public interface IMstSleHotnessAppService
    {
        Task<PagedResultDto<GetMstSleHotnessForViewDto>> GetAll(GetMstSleHotnessInputDto input);
        Task CreateOrEdit(CreateOrEditMstSleHotnessDto input);
        Task<GetMstSleHotnessForEditOutput> GetMstSleHotnessForEdit(EntityDto<long> input);

        Task Delete(EntityDto<long> input);
    }
}
