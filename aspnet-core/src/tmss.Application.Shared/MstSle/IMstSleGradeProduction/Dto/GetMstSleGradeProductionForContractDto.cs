﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetMstSleGradeProductionForContractDto : EntityDto<long?>
    {
        public string ProductionCode { get; set; }
        public long? GradeId { get; set; }
    }
}
