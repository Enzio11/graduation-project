﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetGradeProductionForSalesCustomerDto : EntityDto<long?>
    {
        public string ProductionCode { get; set; }
    }
}
