﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetAllCodeColorByGradesId : EntityDto<long?>
    {
        [StringLength(50)]
        public string VnName { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        public int? GradeId { get; set; }
    }
}
