﻿using Abp.Application.Services.Dto;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetAllGradesProductionDto : EntityDto<long?>
    {
        public int? GradeId { get; set; }

        [StringLength(50)]
        public string ProductionCode { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(50)]
        public string ShortModel { get; set; }

        [StringLength(50)]
        public string FullModel { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? FrameNoLength { get; set; }

        [StringLength(1)]
        public string Status { get; set; }
    }
}
