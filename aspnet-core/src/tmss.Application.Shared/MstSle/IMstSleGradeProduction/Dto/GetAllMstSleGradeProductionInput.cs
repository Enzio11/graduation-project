﻿using Abp.Application.Services.Dto;
using System;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetAllMstSleGradeProductionInput : PagedAndSortedResultRequestDto
    {
        public string Filter { get; set; }

        public string ProductionCodeFilter { get; set; }

        public string EnNameFilter { get; set; }

        public string VnNameFilter { get; set; }

        public decimal? OrderingFilter { get; set; }

        public string IsHasAudioFilter { get; set; }

        public string CbuCkdFilter { get; set; }

        public string ShortModelFilter { get; set; }

        public string WmiFilter { get; set; }

        public string VdsFilter { get; set; }

        public string FullModelFilter { get; set; }

        public string BarcodeFilter { get; set; }

        public DateTime? FromDateFilter { get; set; }

        public DateTime? ToDateFilter { get; set; }

        public string IsFirmColorFilter { get; set; }

        public decimal? FrameNoLengthFilter { get; set; }
        public decimal? ManYearFilter { get; set; }
        public decimal? CapacityFilter { get; set; }

        public decimal? LengthFilter { get; set; }

        public decimal? WidthFilter { get; set; }

        public decimal? HeightFilter { get; set; }

        public decimal? WeightFilter { get; set; }

        public decimal? TireSizeFilter { get; set; }

        public decimal? PayloadFilter { get; set; }

        public decimal? PullingWeightFilter { get; set; }

        public decimal? SeatNoStandingFilter { get; set; }

        public decimal? SeatNoLyingFilter { get; set; }

        public decimal? SeatNoFilter { get; set; }

        public string CaseSizeFilter { get; set; }

        public string BaseLengthFilter { get; set; }

        public string StatusFilter { get; set; }
    }
}
