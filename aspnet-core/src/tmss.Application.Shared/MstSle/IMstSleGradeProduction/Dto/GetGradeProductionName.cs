﻿using System.Collections.Generic;
using tmss.MstSle.IMtSleColor.Dto;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetGradeProductionName
    {
        public long? GradeProductionId { get; set; }
        public string ProductionCode { get; set; }
        public string EnName { get; set; }
        public string VnName { get; set; }
        public List<GetColorName> ListColor { get; set; }
    }
}
