﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class CreateOrEditMstSleGradeProductionDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string ProductionCode { get; set; }

        [StringLength(50)]
        public string EnName { get; set; }

        [StringLength(50)]
        public string VnName { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string IsHasAudio { get; set; }

        [StringLength(50)]
        public string CbuCkd { get; set; }

        [StringLength(50)]
        public string ShortModel { get; set; }

        [StringLength(50)]
        public string Wmi { get; set; }

        public int GasolineTypeId { get; set; }

        [StringLength(50)]
        public string Vds { get; set; }

        [StringLength(50)]
        public string FullModel { get; set; }

        [StringLength(50)]
        public string Barcode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        [StringLength(1)]
        public string IsFirmColor { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? FrameNoLength { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? ManYear { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? Capacity { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? Length { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? Width { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? Height { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? Weight { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? TireSize { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? Payload { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? PullingWeight { get; set; }

        [Column(TypeName = "numeric(6, 0)")]
        public decimal? SeatNoStanding { get; set; }

        [Column(TypeName = "numeric(6, 0)")]
        public decimal? SeatNoLying { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? SeatNo { get; set; }

        [StringLength(50)]
        public string CaseSize { get; set; }

        [StringLength(50)]
        public string BaseLength { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public int? GradeId { get; set; }
    }
}
