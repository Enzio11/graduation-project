﻿namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetMstSleGradeProductionForEditOutput
    {
        public CreateOrEditMstSleGradeProductionDto MstSleGradeProduces { get; set; }
    }
}
