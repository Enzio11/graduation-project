﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetGradesProductionByGradesId : PagedAndSortedResultRequestDto
    {
        public int? GradeId { get; set; }
    }
}
