﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetMstSleGradeProducesForViewDto : EntityDto<long?>
    {
        [StringLength(50)]
        public string ProductionCode { get; set; }

        [Column(TypeName = "numeric(4, 0)")]
        public decimal? Ordering { get; set; }

        [StringLength(1)]
        public string IsHasAudio { get; set; }

        [StringLength(50)]
        public string CbuCkd { get; set; }

        [StringLength(50)]
        public string ShortModel { get; set; }

        [StringLength(50)]
        public string Wmi { get; set; }

        [StringLength(50)]
        public string Vds { get; set; }

        [StringLength(50)]
        public string FullModel { get; set; }
        public long? GradeId { get; set; }

        [StringLength(50)]
        public string MarketingCode { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ToDate { get; set; }

        [Column(TypeName = "numeric(12, 0)")]
        public decimal? FrameNoLength { get; set; }

        [StringLength(50)]
        public string ModelName { get; set; }

        [StringLength(50)]
        public string GradeMarketing { get; set; }

        [StringLength(1)]
        public string Status { get; set; }


        public decimal? GasolineTypeId { get; set; }

        [StringLength(50)]
        public string GasolineType { get; set; }

        [StringLength(1)]
        public string IsFirmColor { get; set; }
    }
}
