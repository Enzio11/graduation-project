﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleGradeProduction.Dto
{
    public class GetAllProductionInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
        public int? GradeIdFilter { get; set; }
        public string ProductionCodeFilter { get; set; }
        public string EnNameFilter { get; set; }
        public string VnNameFilter { get; set; }
        public decimal? OrderingFilter { get; set; }
        public string ShortModelFilter { get; set; }
        public string FullModelFilter { get; set; }
        public decimal? FrameNoLengthFilter { get; set; }
        public string StatusFilter { get; set; }
    }
}
