﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using tmss.MstSle.Dto.SalesStage;

namespace tmss.MstSle
{
    public interface IMstSleSalesStageAppService
    {
        Task<PagedResultDto<GetMstSleSalesStageForViewDto>> GetAll(GetMstSleSalesStageInputDto input);
        Task<GetMstSleSalesStageForEditOutput> GetMstSleSalesStageForEdit(EntityDto<long> input);
        Task CreateOrEdit(CreateOrEditMstSleSalesStageDto input);
        Task Delete(EntityDto<long> input);
    }
}
