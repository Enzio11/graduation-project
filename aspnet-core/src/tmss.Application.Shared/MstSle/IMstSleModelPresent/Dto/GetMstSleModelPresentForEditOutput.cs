﻿namespace tmss.MstSle.IMstSleModelPresent.Dto
{
    public class GetMstSleModelPresentForEditOutput
    {
        public CreateOrEditMstSleModelPresentDto mstSleModelPresent { get; set; }
    }
}
