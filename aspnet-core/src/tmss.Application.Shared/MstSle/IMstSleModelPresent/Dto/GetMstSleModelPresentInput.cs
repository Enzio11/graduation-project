﻿using Abp.Application.Services.Dto;

namespace tmss.MstSle.IMstSleModelPresent.Dto
{
    public class GetMstSleModelPresentInput : PagedAndSortedResultRequestDto
    {
        public string FilterText { get; set; }
    }
}
