﻿namespace tmss.MstSle.IMstSleModelPresent.Dto
{
    public class GetMstSleModelPresentForExportDto
    {
        public string MarketingCode { get; set; }
        public string ProductionCode { get; set; }
        public string EnName { get; set; }
        public string VnName { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public decimal? Ordering { get; set; }
        public decimal? OrderingRpt { get; set; }
        public string Make { get; set; }
        public string Abbreviation { get; set; }
    }
}
