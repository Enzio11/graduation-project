﻿using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Security.Cryptography;

namespace tmss.Web.Controllers
{
    public class UploadControllerBase : tmssControllerBase
    {

        [HttpPost]
        public IActionResult UploadFileToFolder()
        {
            try
            {
                //var file = Request.Form.Files.First(); 

                var file = Request.Form.Files[0];

                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                var folderName = Path.Combine("wwwroot", "Resources", "Images");
                var folderNameDownload = Path.Combine("", "Resources", "Images");
                if (file == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (file.Length > 1048576 * 100) //100 MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }


                if (file.Length > 0)
                {
                    string resultFormat = "";
                    var intermediaries = file.FileName;
                    char[] array = intermediaries.ToCharArray();
                    for (int i = 1; i < array.Length; i++)
                    {
                        if (array[i - 1] == '.')
                        {
                            for (int j = i; j < array.Length; j++)
                            {
                                resultFormat += array[j];
                            }
                        }
                    }
                    if (resultFormat == "pdf")
                    {
                        folderName = Path.Combine("wwwroot", "PDFs");
                        folderNameDownload = Path.Combine("", "PDFs");
                    }
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                    var ms = new MemoryStream();
                    file.CopyTo(ms);
                    string fileName = GetMD5HashFromFile(ms) + Path.GetExtension(file.FileName);
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderNameDownload, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    return Ok(new { dbPath });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        [HttpPost]
        public IActionResult UploadFileToFolderAndGetName()
        {
            try
            {
                //var file = Request.Form.Files.First(); 
                var tenantId = AbpSession.TenantId;
                var userId = AbpSession.UserId;
                string DateTime = System.DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "");

                var file = Request.Form.Files[0];

                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                var folderName = Path.Combine("wwwroot", "Resources", "Images");
                var folderNameDownload = Path.Combine("", "Resources", "Images");
                if (file == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (file.Length > 1048576 * 100) //100 MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }


                if (file.Length > 0)
                {
                    string resultFormat = "";
                    var intermediaries = file.FileName;
                    char[] array = intermediaries.ToCharArray();
                    for (int i = 1; i < array.Length; i++)
                    {
                        if (array[i - 1] == '.')
                        {
                            for (int j = i; j < array.Length; j++)
                            {
                                resultFormat += array[j];
                            }
                        }
                    }
                    if (resultFormat == "xlsx")
                    {
                        folderName = Path.Combine("wwwroot", "Excel");
                        folderNameDownload = Path.Combine("", "Excel");
                    }
                    else if (resultFormat == "pdf")
                    {
                        folderName = Path.Combine("wwwroot", "PDFs");
                        folderNameDownload = Path.Combine("", "PDFs");
                    }
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

                    var ms = new MemoryStream();
                    file.CopyTo(ms);
                    string fileName = tenantId + "_" + userId.ToString() + "_" + DateTime + "_" + file.FileName;
                    var fullPath = Path.Combine(pathToSave, fileName);
                    var dbPath = Path.Combine(folderNameDownload, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }

                    return Ok(new { dbPath });
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }

        private static string GetMD5HashFromFile(MemoryStream ms)
        {
            using var md5 = MD5.Create();

            return System.BitConverter.ToString(md5.ComputeHash(ms.ToArray())).Replace("-", string.Empty);
        }
    }
}