﻿using Abp.BackgroundJobs;
using Abp.Domain.Repositories;
using Abp.IO.Extensions;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using tmss.Sales.Customer.Dto;
using tmss.Sales.Customer.Import.AssignmentCustomer;
using tmss.Sales.Import;
using tmss.Storage;

namespace tmss.Web.Controllers
{
    public class SalesCustomerControllerBase : tmssControllerBase
    {
        protected readonly IBinaryObjectManager BinaryObjectManager;
        protected readonly IBackgroundJobManager BackgroundJobManager;
        protected readonly IRepository<SalesJobMonitor, long> _salesJobMonitorRepository;

        protected SalesCustomerControllerBase(
            IBinaryObjectManager binaryObjectManager,
            IBackgroundJobManager backgroundJobManager,
            IRepository<SalesJobMonitor, long> salesJobMonitorRepository)
        {
            BinaryObjectManager = binaryObjectManager;
            BackgroundJobManager = backgroundJobManager;
            _salesJobMonitorRepository = salesJobMonitorRepository;
        }

        [HttpPost]
        public async Task<JsonResult> ImportFromExcel()
        {
            try
            {
                var file = Request.Form.Files[0];

                string sFileExtension = Path.GetExtension(file.FileName).ToLower();

                if (sFileExtension == ".xls")
                {
                    throw new UserFriendlyException(L("File_Support_Only_Extention_xlsx"));
                }

                if (file == null)
                {
                    throw new UserFriendlyException(L("File_Empty_Error"));
                }

                if (file.Length > 1048576 * 100) //100 MB
                {
                    throw new UserFriendlyException(L("File_SizeLimit_Error"));
                }

                var tenantId = AbpSession.TenantId;
                var userId = AbpSession.UserId;
                string Code = "ASG" + userId.ToString();
                var job = _salesJobMonitorRepository.GetAll().Where(e => e.Code == Code && e.TenantId == tenantId).ToList();
                if (job.Count > 0)
                {
                    throw new UserFriendlyException(L("ThereIsAProgessImport"));
                }

                if (job.Count == 0)
                {
                    SalesJobMonitor jobMonitor = new();
                    jobMonitor.Code = Code;
                    jobMonitor.CreatorUserId = userId;
                    jobMonitor.TenantId = tenantId;
                    var jobMonitorId = _salesJobMonitorRepository.InsertAndGetId(jobMonitor);

                    byte[] fileBytes;
                    using (var stream = file.OpenReadStream())
                    {
                        fileBytes = stream.GetAllBytes();
                    }

                    var fileObject = new BinaryObject(tenantId, fileBytes);

                    await BinaryObjectManager.SaveAsync(fileObject);

                    BackgroundJobManager.Enqueue<ImportNewAssignmentCustomersToExcelJob, ImportPotentialCustomersFromExcelJobArgs>(new ImportPotentialCustomersFromExcelJobArgs
                    {
                        TenantId = tenantId,
                        BinaryObjectId = fileObject.Id,
                        User = AbpSession.ToUserIdentifier(),
                        JobMonitorId = jobMonitorId
                    });
                }

                return Json(new AjaxResponse(new { }));
            }
            catch (UserFriendlyException ex)
            {
                return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            }
        }
    }
}
