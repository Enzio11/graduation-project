using Abp.IdentityServer4;
using Abp.Zero.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using tmss.Authorization.Delegation;
using tmss.Authorization.Roles;
using tmss.Authorization.Users;
using tmss.Chat;
using tmss.Customer;
using tmss.Editions;
using tmss.Friendships;
using tmss.Master;
using tmss.MstSle;
using tmss.MultiTenancy;
using tmss.MultiTenancy.Accounting;
using tmss.MultiTenancy.Payments;
using tmss.Sales;
using tmss.Sales.Customer;
using tmss.Sales.Import;
using tmss.Storage;

namespace tmss.EntityFrameworkCore
{
    public class tmssDbContext : AbpZeroDbContext<Tenant, Role, User, tmssDbContext>, IAbpPersistedGrantDbContext
    {
        /* Define an IDbSet for each entity of the application */
        #region Framework Entities
        public virtual DbSet<BinaryObject> BinaryObjects { get; set; }
        public virtual DbSet<Friendship> Friendships { get; set; }
        public virtual DbSet<ChatMessage> ChatMessages { get; set; }
        public virtual DbSet<SubscribableEdition> SubscribableEditions { get; set; }
        public virtual DbSet<SubscriptionPayment> SubscriptionPayments { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<SubscriptionPaymentExtensionData> SubscriptionPaymentExtensionDatas { get; set; }
        public virtual DbSet<UserDelegation> UserDelegations { get; set; }
        #endregion

        #region Master Entities
        // New Table Set Here
        public virtual DbSet<MstSleBusinessType> MstSleBusinessTypes { get; set; }
        public virtual DbSet<MstSleDistrict> MstSleDistricts { set; get; }
        public virtual DbSet<MstSleAgeRange> MstSleAgeRanges { set; get; }
        public virtual DbSet<MstSleSource> MstSleSources { set; get; }
        public virtual DbSet<MstSleHotness> MstSleHotnesses { set; get; }
        public virtual DbSet<MstSleExpectedDelTiming> MstSleExpectedDelTimings { set; get; }
        public virtual DbSet<MstSleHobby> MstSleHobbys { set; get; }
        public virtual DbSet<MstSleOccupation> MstSleOccupations { set; get; }
        public virtual DbSet<MstSleRelationship> MstSleRelationships { set; get; }
        public virtual DbSet<MstSleSocialChannel> MstSleSocialChannels { set; get; }
        public virtual DbSet<MstSleModel> MstSleModel { set; get; }
        public virtual DbSet<MstSleColors> MstSleColors { set; get; }
        public virtual DbSet<MstSleMake> MstSleMakes { set; get; }
        public virtual DbSet<MstSlePurpose> MstSlePurpose { set; get; }
        public virtual DbSet<MstSleFAR> MstSleFARs { set; get; }
        public virtual DbSet<MstSleSalesStage> MstSleSalesStages { set; get; }
        public virtual DbSet<MstSleBank> MstSleBanks { set; get; }
        public virtual DbSet<MstSleReasonOfLost> MstSleReasonOfLosts { set; get; }
        public virtual DbSet<MstSleActivity> MstSleActivities { set; get; }
        public virtual DbSet<MstSleReasonOfLostType> MstSleReasonOfLostTypes { set; get; }
        public virtual DbSet<MstSleSourceOfInfo> MstSleSourceOfInfos { set; get; }
        public virtual DbSet<MstGenDealer> MstGenDealers { set; get; }
        public virtual DbSet<MstGenProvince> MstGenProvinces { set; get; }
        public virtual DbSet<MstSleColorGradesProduction> MstSleColorGradesProductions { set; get; }
        public virtual DbSet<MstSleGradeProduction> MstSleGradeProductions { set; get; }
        public virtual DbSet<MstSleGrades> MstSleGrades { set; get; }
        public virtual DbSet<MstSleCommissionSource> MstSleCommissionSources { set; get; }
        public virtual DbSet<MstSleInteriorColorGradesProduction> MstSleInteriorColorGradesProductions { set; get; }
        public virtual DbSet<MstSleLookUp> MstSleLookups { set; get; }
        public virtual DbSet<MstSleModel> MstSleModels { set; get; }
        public virtual DbSet<MstSleSalesPerson> MstSleSalesPersons { set; get; }
        public virtual DbSet<MstSleContactBy> MstSleContactBys { set; get; }
        public virtual DbSet<MstSleVehiclePrice> MstSleVehiclePrices { set; get; }
        public virtual DbSet<MstSleReasonOfChange> MstSleReasonOfChanges { set; get; }
        public virtual DbSet<MstSleTestDrivePlace> MstSleTestDrivePlaces { set; get; }
        public virtual DbSet<MstSleTestDriveRoute> MstSleTestDriveRoutes { set; get; }
        public virtual DbSet<MstSleTestDriveStall> MstSleTestDriveStalls { set; get; }
        public virtual DbSet<MstSleTestDriveVehicle> MstSleTestDriveVehicles { set; get; }
        public virtual DbSet<MstSleReasonOfFreeze> MstSleReasonOfFreezes { get; set; }
        public virtual DbSet<MstSleReasonOfNA> MstSleReasonOfNAs { set; get; }
        public virtual DbSet<MstSleReasonOfNC> MstSleReasonOfNCs { set; get; }
        public virtual DbSet<MstSleCompany> MstSleCompanies { get; set; }
        #endregion

        #region Business Entities

        #region Import
        public virtual DbSet<SalesJobMonitor> SalesJobMonitors { set; get; }
        public virtual DbSet<SalesInvalidImport> SalesInvalidImports { set; get; }
        #endregion

        public virtual DbSet<SalesCustomer> SalesCustomers { set; get; }
        public virtual DbSet<SalesCustomerTransactionInfo> SalesCustomerTransactionInfos { set; get; }
        public virtual DbSet<SalesCustomerTransactionInfoContact> SalesCustomerTransactionInfoContacts { set; get; }
        public virtual DbSet<SalesCustomerManagerComment> SalesCustomerManagerComments { set; get; }
        public virtual DbSet<SalesCustomerTestDrive> SalesCustomerTestDrives { get; set; }
        public virtual DbSet<SalesCustomerHotness> SalesCustomerHotnesses { get; set; }
        public virtual DbSet<SalesCustomerVehicleInfo> SalesCustomerVehicleInfos { set; get; }
        public virtual DbSet<SalesCustomerSocialChannel> SalesCustomerSocialChannels { set; get; }
        public virtual DbSet<SalesCustomerAnniversary> SalesCustomerAnniversarys { set; get; }
        #endregion

        public DbSet<PersistedGrantEntity> PersistedGrants { get; set; }

        public tmssDbContext(DbContextOptions<tmssDbContext> options)
            : base(options)
        {
            Database.SetCommandTimeout(150000000);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BinaryObject>(b =>
            {
                b.HasIndex(e => new { e.TenantId });
            });

            modelBuilder.Entity<ChatMessage>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId, e.ReadState });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.TargetUserId, e.ReadState });
                b.HasIndex(e => new { e.TargetTenantId, e.UserId, e.ReadState });
            });

            modelBuilder.Entity<Friendship>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.UserId });
                b.HasIndex(e => new { e.TenantId, e.FriendUserId });
                b.HasIndex(e => new { e.FriendTenantId, e.UserId });
                b.HasIndex(e => new { e.FriendTenantId, e.FriendUserId });
            });

            modelBuilder.Entity<Tenant>(b =>
            {
                b.HasIndex(e => new { e.SubscriptionEndDateUtc });
                b.HasIndex(e => new { e.CreationTime });
            });

            modelBuilder.Entity<SubscriptionPayment>(b =>
            {
                b.HasIndex(e => new { e.Status, e.CreationTime });
                b.HasIndex(e => new { PaymentId = e.ExternalPaymentId, e.Gateway });
            });

            modelBuilder.Entity<SubscriptionPaymentExtensionData>(b =>
            {
                b.HasQueryFilter(m => !m.IsDeleted)
                    .HasIndex(e => new { e.SubscriptionPaymentId, e.Key, e.IsDeleted })
                    .IsUnique();
            });

            modelBuilder.Entity<UserDelegation>(b =>
            {
                b.HasIndex(e => new { e.TenantId, e.SourceUserId });
                b.HasIndex(e => new { e.TenantId, e.TargetUserId });
            });

            modelBuilder.ConfigurePersistedGrantEntity();
        }
    }
}
