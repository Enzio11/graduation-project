﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using tmss.EntityFrameworkCore;

namespace tmss.Migrations.Seed.baseHelper
{
    public class BaseController
    {
        protected readonly tmssDbContext _context;
        public long[] AllMstSleCustomerTypeID;
        public long[] AllMstGenProvinceID;
        public long[] AllMstSleDistrictID;
        public long[] AllMstSleHotnessID;
        public long[] AllMstSleExpectedDelTimingID;
        public long[] AllMstSleSalePersonID;
        public long[] AllMstSleSourceID;
        public long[] AllMstSlePaymentTypeID;
        public long[] AllMstSleGenderID;
        public long[] AllMstSleMaritalStatusID;
        public long[] AllMstSleOccupationID;
        public long[] AllMstSleHobbyID;
        public long[] AllMstSleSourceOfInfoID;
        public long[] AllMstSleFARID;
        public long[] AllMstSlePurposeID;
        public long[] AllMstSleAgeRangeID;
        public long[] AllMstSleRelationshipID;
        public long[] AllSalesCustomerID;
        public long[] AllMstSleSocialChannelID;
        public long[] AllMstSleSalesStageID;
        public long[] AllMstSleActivityStatusID;
        public long[] AllMstSleReasonofLostID;
        public long[] AllMstSleDeliveryPlaceID;
        public long[] AllMstSleRouteID;
        public long[] AllMstSleSupportersID;
        public long[] AllSalesCustomerTransactionInfoID;
        public long[] AllMstSleAppActivityID;
        public long[] AllMstSleAppPlaceID;
        public long[] AllMstSleContactByID;
        public long[] AllMstSleActionID;
        public long[] AllSalesCustomerTransactionInfoContactID;
        public long[] AllMstSleModelID;
        public long[] AllMstSleGradesID;
        public long[] AllMstSleColorGradesProductionID;
        public long[] AllMstSleColorsID;

        public BaseController(tmssDbContext context)
        {
            _context = context;
            AllMstGenProvinceID = getAllMstGenProvinceID();
            AllMstSleDistrictID = getAllMstSleDistrictID();
            AllMstSleHotnessID = getAllMstSleHotnessID();
            AllMstSleExpectedDelTimingID = getAllMstSleExpectedDelTimingID();
            AllMstSleSalePersonID = getAllMstSleSalePersonID();
            AllMstSleSourceID = getAllMstSleSourceID();
            AllMstSleOccupationID = getAllMstSleOccupationID();
            AllMstSleHobbyID = getAllMstSleHobbyID();
            AllMstSleSourceOfInfoID = getAllMstSleSourceOfInfoID();
            AllMstSleFARID = getAllMstSleFARID();
            AllMstSlePurposeID = getAllMstSlePurposeID();
            AllMstSleAgeRangeID = getAllMstSleAgeRangeID();
            AllMstSleRelationshipID = getAllMstSleRelationshipID();
            AllSalesCustomerID = getAllSalesCustomerID();
            AllMstSleSocialChannelID = getAllMstSleSocialChannelID();
            AllMstSleSalesStageID = getAllMstSleSalesStageID();
            AllMstSleReasonofLostID = getAllMstSleReasonofLostID();
            AllSalesCustomerTransactionInfoID = getAllSalesCustomerTransactionInfoID();
            AllMstSleContactByID = getAllMstSleContactByID();
            AllSalesCustomerTransactionInfoContactID = getAllSalesCustomerTransactionInfoContactID();
            AllMstSleModelID = getAllMstSleModelID();
            AllMstSleGradesID = getAllMstSleGradesID();
            AllMstSleColorGradesProductionID = getAllMstSleColorGradesProductionID();
            AllMstSleColorsID = getAllMstSleColorsID();
        }
        public long[] getAllMstSleSalePersonID()
        {
            var res = _context.MstSleSalesPersons.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstGenProvinceID()
        {
            var res = _context.MstGenProvinces.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleDistrictID()
        {
            var res = _context.MstSleDistricts.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleHotnessID()
        {
            var res = _context.MstSleHotnesses.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleExpectedDelTimingID()
        {
            var res = _context.MstSleExpectedDelTimings.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }

        public long[] getAllMstSleSourceID()
        {
            var res = _context.MstSleSources.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleOccupationID()
        {
            var res = _context.MstSleOccupations.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleHobbyID()
        {
            var res = _context.MstSleHobbys.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleSourceOfInfoID()
        {
            var res = _context.MstSleSourceOfInfos.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleFARID()
        {
            var res = _context.MstSleFARs.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSlePurposeID()
        {
            var res = _context.MstSlePurpose.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleAgeRangeID()
        {
            var res = _context.MstSleAgeRanges.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleRelationshipID()
        {
            var res = _context.MstSleRelationships.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllSalesCustomerID()
        {
            var res = _context.SalesCustomers.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleSocialChannelID()
        {
            var res = _context.MstSleSocialChannels.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleSalesStageID()
        {
            var res = _context.MstSleSalesStages.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleReasonofLostID()
        {
            var res = _context.MstSleReasonOfLosts.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }

        public long[] getAllSalesCustomerTransactionInfoID()
        {
            var res = _context.SalesCustomerTransactionInfos.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleContactByID()
        {
            var res = _context.MstSleContactBys.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllSalesCustomerTransactionInfoContactID()
        {
            var res = _context.SalesCustomerTransactionInfoContacts.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleModelID()
        {
            var res = _context.MstSleModels.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleGradesID()
        {
            var res = _context.MstSleGrades.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleColorGradesProductionID()
        {
            var res = _context.MstSleColorGradesProductions.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }
        public long[] getAllMstSleColorsID()
        {
            var res = _context.MstSleColors.IgnoreQueryFilters().Select(o => o.Id).ToArray();
            return res;
        }

        public static long randomValue(long[] arr)
        {
            if (arr.Length == 0)
            {
                return 0;
            }
            int index = new Random().Next(0, arr.Length);

            return arr[index];
        }
    }
};