import { PaginationParamsModel } from './../../models/base.model';
import { GridTableService } from './../../services/grid-table.service';
import {
    Component,
    OnInit,
    Input,
    ViewChild,
    Output,
    EventEmitter,
    Injector,
    ChangeDetectorRef,
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ceil } from 'lodash';

@Component({
    selector: 'tmss-select-grid-modal',
    templateUrl: './tmss-select-grid-modal.component.html',
    styleUrls: ['./tmss-select-grid-modal.component.less'],
})
export class TmssSelectGridModalComponent extends AppComponentBase
    implements OnInit {
    @ViewChild('modal', { static: false }) modal: ModalDirective;
    // tslint:disable-next-line:no-output-native
    @Output() close = new EventEmitter();
    @Output() cancel = new EventEmitter();
    @Output() setLoading: EventEmitter<any> = new EventEmitter();
    @Input() headerText: string;
    @Input() modalClass: string = 'tmss-modal-md';
    @Input() showPagination: boolean = true;
    // tslint:disable-next-line:ban-types
    @Input() apiCall: Function;
    @Input() isHideSearchField: boolean;
    @Input() value;
    @Input() columnDefs: Array<any>;
    @Input() height;
    @Input() listInput;
    @Input() defaultColDef;
    @Input() showInput: boolean = true;
    @Input() hideSaveButton;
    @Input() hideCancelButton;
    @Input() rowSelection = 'single';

    @Input() TextLeft = this.l('Save');
    @Input() TextRight = this.l('Cancel');

    @Input() showColorAppointment = false;
    @Input() showBsModal = false;
    dataStorage;
    pagedList;
    gridApi;
    params;
    gridColumnApi;
    paginationParams: PaginationParamsModel;
    list: Array<any> = new Array();
    totalPages: number;
    frameworkComponents;
    selectedData;
    filterName;

    constructor(
        injector: Injector,
        private gridTableService: GridTableService,
        private cdr: ChangeDetectorRef
    ) {
        super(injector);
    }

    ngOnInit() {
    }

    callBackGrid(params) {
        this.params = params;
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.gridApi.paginationSetPageSize(this.paginationParams?.pageSize);
        //this.onGridReady(this.paginationParams);
    }

    // onGridReady(paginationParams) {
    //     this.paginationParams = paginationParams;
    //     this.paginationParams.skipCount =
    //         (paginationParams.pageNum - 1) * paginationParams.pageSize;

    //     if (this.apiCall && this.apiCall !== null) {
    //         const api = !this.paginationParams
    //             ? this.apiCall(this.value)
    //             : this.apiCall(this.value, this.paginationParams);
    //         api.subscribe((result) => {
    //             if (result && result.items) {
    //                 this.list = result.items;
    //                 this.totalPages = ceil(
    //                     result.totalCount / this.paginationParams.pageSize
    //                 );
    //                 this.paginationParams.totalCount = result.totalCount;
    //                 this.paginationParams.totalPage = this.totalPages;
    //             } else {
    //                 this.list = result;
    //             }

    //             this.selectedData = undefined;
    //             this.gridApi.setRowData(this.list);
    //             this.gridTableService.selectFirstRow(this.gridApi);
    //             setTimeout(() => {
    //                 this.gridTableService.setFocusCell(
    //                     this.gridApi,
    //                     this.columnDefs[0].field
    //                 );
    //             }, 50);
    //         }, err => this.notify.error(err.error),
    //             () => this.setLoading.emit(false));
    //     }

    //     //If api call is undefined, pass a list for test
    //     this.totalPages = ceil(
    //         this.list ?
    //             this.list.length / this.paginationParams.pageSize
    //             : 0
    //     );
    //     this.paginationParams.totalCount = this.list ? this.list.length : 0;
    //     this.paginationParams.totalPage = this.totalPages;

    //     this.selectedData = undefined;
    //     this.pagedList = this.list ? this.list.slice(((this.paginationParams.pageNum - 1) * this.paginationParams.pageSize), this.paginationParams.pageNum * this.paginationParams.pageSize) : [];
    //     this.gridApi.setRowData(this.pagedList);
    //     this.gridTableService.selectFirstRow(this.gridApi);
    //     setTimeout(() => {
    //         this.gridTableService.setFocusCell(
    //             this.gridApi,
    //             this.columnDefs[0].field
    //         );
    //     }, 50);
    // }

    async search(value?, alwaysShowModal?) {
        this.value = value ?? '';
        if (this.apiCall && !this.isHideSearchField) {
            const api = !this.paginationParams
                ? this.apiCall(this.value)
                : this.apiCall(this.value, {
                    sorting: this.paginationParams.sorting ?? '',
                    skipCount: this.paginationParams.skipCount ?? 0,
                    pageSize: this.paginationParams.pageSize ?? 10,
                });
            await api.subscribe((result) => {
                this.setLoading.emit(true);
                if (result && result.items) {
                    this.list = result.items;
                    this.totalPages = ceil(
                        result.totalCount / this.paginationParams.pageSize
                    );
                    this.paginationParams.totalCount = result.totalCount;
                    this.paginationParams.totalPage = this.totalPages;
                    //Set page 0 by default
                    this.paginationParams.pageNum = 1;
                } else {
                    this.list = result;
                }
            }, err => this.notify.error(err.error), () => {
                if (!this.modal.isShown) {
                    if (alwaysShowModal) {
                        this.modal.show();
                    } else if (this.list && this.list.length === 1) {
                        this.close.emit(this.list[0]);
                    }  else {
                        this.modal.show();
                        setTimeout(() => {
                            this.selectedData = undefined;
                            this.gridApi?.setRowData(this.list);
                            this.gridTableService?.selectFirstRow(this.gridApi);
                            this.gridTableService.setFocusCell(
                                this.gridApi,
                                this.columnDefs[0].field
                            );
                        }, 50);
                    }
                } else {
                    setTimeout(() => {
                        this.selectedData = undefined;
                        this.gridApi.setRowData(this.list);
                        this.gridTableService.selectFirstRow(this.gridApi);
                        this.gridTableService.setFocusCell(
                            this.gridApi,
                            this.columnDefs[0].field
                        );
                    }, 50);
                }
                this.setLoading.emit(false);
            });
            return await null;
        }
        setTimeout(() => {
            this.list = this.dataStorage.filter(e => e[this.filterName.toString()] ? e[this.filterName.toString()].toString().includes(value ?? '') : ''.includes(value));
            this.gridApi?.setRowData(this.list);
            this.totalPages = ceil(
                this.list ?
                    this.list.length / this.paginationParams.pageSize
                    : 0
            );
            this.paginationParams.pageNum = 1;
            this.paginationParams.totalCount = this.list ? this.list.length : 0;
            this.paginationParams.totalPage = this.totalPages;
        }, 50);

        if (!this.modal.isShown) { //Check is show modal
            if (alwaysShowModal || (this.list && this.list.length === 1)) {
                this.close.emit(this.list[0]);
            } else {
                this.modal.show();
                setTimeout(() => {
                    this.pagedList = this.list ? this.list.slice(((this.paginationParams.pageNum - 1) * this.paginationParams.pageSize), this.paginationParams.pageNum * this.paginationParams.pageSize) : [];
                    if (!this.gridApi) {
                        setTimeout(() => {
                            this.gridApi.setRowData(this.pagedList);
                        }, 50);
                    } else {
                        this.gridApi.setRowData(this.pagedList);
                    }
                    this.gridTableService.setFocusCell(
                        this.gridApi,
                        this.columnDefs[0].field
                    );
                }, 50);
            }
        } else {
            setTimeout(() => {
                this.pagedList = this.list ? this.list.slice(((this.paginationParams.pageNum - 1) * this.paginationParams.pageSize), this.paginationParams.pageNum * this.paginationParams.pageSize) : [];
                this.gridApi?.setRowData(this.pagedList);
                this.gridTableService.setFocusCell(
                    this.gridApi,
                    this.columnDefs[0].field
                );
            }, 50);
        }
    }

    async show(val?, list?, total?, filterName?) {
        this.filterName = filterName ?? '';
        this.setLoading.emit(true);
        if (!this.isHideSearchField) {
            this.value = val ?? '';
        }
        if (list) {
            this.setLoading.emit(false);
            // Lưu toàn bộ mảng truyền vào
            this.dataStorage = list;
            // Nếu giá trị truyền vào bị null thì sẽ au
            this.list = filterName !== null ? list.filter(e => e[filterName.toString()] ? e[filterName.toString()].toString().includes(val ?? '') : ''.includes(val)) : list;
        }
        if (total) {
            this.paginationParams
                ? (this.paginationParams.totalCount = total)
                : (this.paginationParams = null);
        }
        this.paginationParams = { pageNum: 1, pageSize: !this.showPagination ? list?.length : 10, totalCount: 0 };
        // this.frameworkComponents = {
        //     numberFormatterComponent: NumberFormatterComponent,
        //     numericEditorComponent: NumericEditorComponent,
        // };

        if (list) {
            this.setLoading.emit(false);
            // Lưu toàn bộ mảng truyền vào
            this.dataStorage = list;
            // Nếu giá trị truyền vào bị null thì sẽ au
            this.list = filterName !== null ? list.filter(e => e[filterName.toString()] ? e[filterName.toString()].toString().includes(val ?? '') : ''.includes(val)) : list;
        }
        if (total) {
            this.paginationParams
                ? (this.paginationParams.totalCount = total)
                : (this.paginationParams = null);
        }
        this.paginationParams = { pageNum: 1, pageSize: !this.showPagination ? list?.length : 10, totalCount: 0 };
        // this.frameworkComponents = {
        //     numberFormatterComponent: NumberFormatterComponent,
        //     numericEditorComponent: NumericEditorComponent,
        // };

        this.cdr.detectChanges();
        this.search(val);
    }

    reset() {
        this.list = undefined;
        this.selectedData = undefined;
        this.paginationParams = undefined;
        // this.paginationParams.totalPage = this.totalPages;
    }

    onCancelBtn() {
        this.modal.hide();
        this.cancel.emit();
    }

    changePaginationParams(paginationParams: PaginationParamsModel) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount =
            (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.paginationParams.pageSize = paginationParams.pageSize;

        // Nếu truyền list vào, thì sẽ dùng hàm cắt mảng để hiển thị ra những bản ghi trong trang đó
        if (this.dataStorage) {
            this.pagedList = this.list ? this.list.slice(this.paginationParams.skipCount, this.paginationParams.pageNum * this.paginationParams.pageSize) : [];
            this.gridApi.setRowData(this.pagedList);
            return;
        }
        this.setLoading.emit(true);
        const api = !this.paginationParams
            ? this.apiCall(this.value)
            : this.apiCall(this.value, this.paginationParams);
        api.subscribe((result) => {
            this.list = result.items;
            this.paginationParams.totalPage = ceil(
                result.totalCount / this.paginationParams.pageSize
            );
        }, err => this.notify.error(err.error),
            () => this.setLoading.emit(false)
        );
    }

    // Confirm before close modal
    confirm(params?) {
        if (params && params.colDef.editable) {
            return;
        }
        // If save button is hidden, does not allow close modal when double click
        if (this.hideSaveButton) {
            return;
        }
        this.close.emit(params ? params.data : this.selectedData);
        this.modal.hide();
    }

    onChangeSelection() {
        const selectedData = this.gridApi.getSelectedRows()[0];
        if (selectedData) {
            this.selectedData = selectedData;
        }
    }

    agKeyUp(event: KeyboardEvent) {
        event.stopPropagation();
        event.preventDefault();
        const keyCode = event.key;
        const KEY_ENTER = 'Enter';

        // Press enter to search with modal
        if (keyCode === KEY_ENTER) {
            this.close.emit(this.selectedData);
            this.modal.hide();
        }
    }
}
