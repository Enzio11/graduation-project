export const StorageKeys = {
    currentTenant: 'CurrentTenant',
    pickList: 'PICKLIST',
    formCache: 'FormCache',
    currentTab: 'tmss/abpzerotemplate_local_storage/CURRENT_TAB',

};
  