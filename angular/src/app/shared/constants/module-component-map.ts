import { TABS } from '../constants/tab-keys';

export const MODULE_PATHS = {
    // TODO: Review Code. Duplicated declaration.
    APP_ADMIN_ADMIN_MODULE: 'app/admin/admin.module',

    // Master Sales
    APP_MASTER_SALE_REASON_OF_FREEZE_MODULE: 'app/admin/sales/master/mst-sle-reason-of-freeze/mst-sle-reason-of-freeze.module',
    APP_MASTER_SALE_GEN_DEALER_MODULE: 'app/admin/sales/master/mst-gen-dealers/mst-gen-dealers.module',
    APP_MASTER_SALE_PROVINCE_MODULE: 'app/admin/sales/master/mst-sle-province/mst-sle-province.module',
    APP_MASTER_SALE_DISTRICT_MODULE: 'app/admin/sales/master/mst-gen-district/mst-gen-district.module',
    APP_MASTER_SALE_SOCIAL_CHANNEL_MODULE: 'app/admin/sales/master/mst-sle-social-channel/mst-sle-social-channel.module',
    APP_MASTER_SALE_COMPANY_MODULE: 'app/admin/sales/master/mst-sle-company/mst-sle-company.module',
    APP_MASTER_SALE_PURPOSE_MODULE: 'app/admin/sales/master/mst-sle-purpose/mst-sle-purpose.module',
    APP_MASTER_SALE_FAR_MODULE: 'app/admin/sales/master/mst-sle-far/mst-sle-far.module',
    APP_MASTER_SALE_MODEL_MODULE: 'app/admin/sales/master/mst-sle-model/mst-sle-model.module',
    APP_MASTER_SALE_GRADES_MODULE: 'app/admin/sales/master/',
    APP_MASTER_SALE_COLOR_BY_GRADE_MODULE: 'app/admin/sales/master/mst-sle-color-by-grade/mst-sle-color-by-grade.module',
    APP_MASTER_SALE_COLOR_MODULE: 'app/admin/sales/master/mst-sle-color/mst-sle-color.module',
    APP_MASTER_SALE_MODEL_CARE_MODULE: 'app/admin/sales/master/mst-sle-make-competitor/mst-sle-make-competitor.module',
    APP_MASTER_SALE_GRADES_CARE_MODULE: 'app/admin/sales/master/mst-sle-grade-competitor/mst-sle-grade-competitor.module',
    APP_MASTER_SALE_MODEL_PRESENT_MODULE: 'app/admin/sales/master/mst-sle-model-present/mst-sle-model-present.module',
    APP_MASTER_SALE_REASON_OF_CHANGE_MODULE: 'app/admin/sales/master/mst-sle-reason-of-changes/mst-sle-reason-of-changes.module',
    APP_MASTER_SALE_SALES_STAGE_MODULE: 'app/admin/sales/master/mst-sle-sales-stage/mst-sle-sales-stage.module',
    APP_MASTER_SALE_REASON_OF_LOST_MODULE: 'app/admin/sales/master/mst-sle-reason-of-lost/mst-sle-reason-of-lost.module',
    APP_MASTER_SALE_CONTACT_BY_MODULE: 'app/admin/sales/master/mst-sle-contact-by/mst-sle-contact-by.module',
    APP_MASTER_SALE_TEST_DRIVER_PLACE_MODULE: 'app/admin/sales/master/mst-sle-test-drive-place/mst-sle-test-drive-place.module',
    APP_MASTER_SALE_TEST_DRIVER_ROUTE_MODULE: 'app/admin/sales/master/mst-sle-test-drive-route/mst-sle-test-drive-route.module',
    APP_MASTER_SALE_SUPPORTER_MODULE: 'app/admin/sales/master/mst-sle-supporter/mst-sle-supporter.module',
    APP_MASTER_SALE_TEST_DRIVER_STALL: 'app/admin/sales/master/mst-sle-test-drive-stall/mst-sle-test-drive-stall.module',
    APP_MASTER_SALE_TEST_DRIVER_VEHICLE_MODULE: 'app/admin/sales/master/mst-sle-test-drive-vehicle/mst-sle-test-drive-vehicle.module',
    APP_MASTER_SALE_VEHICLE_PRICE_MODULE: 'app/admin/sales/master/mst-sle-vehicle-price/mst-sle-vehicle-price.module',
    APP_MASTER_SALE_LOOKUP_MODULE: 'app/admin/sales/master/mst-sle-lookup/mst-sle-lookup.module',

    //SALES
    APP_SALE_CUSTOMER_MODULE: 'app/admin/sales/sales-customer/sales-customer-potential.module',
    APP_ASSIGNMENT_CUSTOMER_MODULE: 'app/admin/sales/assignment-customer/assignment-customer.module',
    APP_CUSTOMER_TEST_DRIVE_MODULE: 'app/admin/sales/customer-test-drive/customer-test-drive.module',
    APP_SALES_PERSON_MODULE: 'app/admin/sales/sales-person/sales-person.module',
};

export const MODULE_COMPONENT_MAP = {
    [MODULE_PATHS.APP_ADMIN_ADMIN_MODULE]: [
        TABS.ADMIN_ORGANIZATION_UNITS,
        TABS.ADMIN_USERS,
        TABS.ADMIN_ROLES,
    ],

    // Master Sales
    [MODULE_PATHS.APP_MASTER_SALE_REASON_OF_FREEZE_MODULE]: [TABS.MASTER_SALE_REASON_OF_FREEZE],
    [MODULE_PATHS.APP_MASTER_SALE_GEN_DEALER_MODULE]: [TABS.MASTER_SALE_GEN_DEALER],
    [MODULE_PATHS.APP_MASTER_SALE_PROVINCE_MODULE]: [TABS.MASTER_SALE_PROVINCE],
    [MODULE_PATHS.APP_MASTER_SALE_DISTRICT_MODULE]: [TABS.MASTER_SALE_DISTRICT],
    [MODULE_PATHS.APP_MASTER_SALE_FAR_MODULE]: [TABS.MASTER_SALE_FAR],
    [MODULE_PATHS.APP_MASTER_SALE_MODEL_MODULE]: [TABS.MASTER_SALE_MODEL],
    [MODULE_PATHS.APP_MASTER_SALE_GRADES_MODULE]: [TABS.MASTER_SALE_GRADES],
    [MODULE_PATHS.APP_MASTER_SALE_COLOR_BY_GRADE_MODULE]: [TABS.MASTER_SALE_COLOR_BY_GRADE],
    [MODULE_PATHS.APP_MASTER_SALE_COLOR_MODULE]: [TABS.MASTER_SALE_COLOR],
    [MODULE_PATHS.APP_MASTER_SALE_MODEL_CARE_MODULE]: [TABS.MASTER_SALE_MODEL_CARE],
    [MODULE_PATHS.APP_MASTER_SALE_GRADES_CARE_MODULE]: [TABS.MASTER_SALE_GRADES_CARE],
    [MODULE_PATHS.APP_MASTER_SALE_MODEL_PRESENT_MODULE]: [TABS.MASTER_SALE_MODEL_PRESENT],
    [MODULE_PATHS.APP_MASTER_SALE_REASON_OF_CHANGE_MODULE]: [TABS.MASTER_SALE_REASON_OF_CHANGE],
    [MODULE_PATHS.APP_MASTER_SALE_SALES_STAGE_MODULE]: [TABS.MASTER_SALE_SALES_STAGE],
    [MODULE_PATHS.APP_MASTER_SALE_REASON_OF_LOST_MODULE]: [TABS.MASTER_SALE_REASON_OF_LOST],
    [MODULE_PATHS.APP_MASTER_SALE_TEST_DRIVER_PLACE_MODULE]: [TABS.MASTER_SALE_TEST_DRIVER_PLACE],
    [MODULE_PATHS.APP_MASTER_SALE_TEST_DRIVER_ROUTE_MODULE]: [TABS.MASTER_SALE_TEST_DRIVER_ROUTE],
    [MODULE_PATHS.APP_MASTER_SALE_SUPPORTER_MODULE]: [TABS.MASTER_SALE_SUPPORTER],
    [MODULE_PATHS.APP_MASTER_SALE_TEST_DRIVER_STALL]: [TABS.MASTER_SALE_TEST_DRIVER_STALL],
    [MODULE_PATHS.APP_MASTER_SALE_TEST_DRIVER_VEHICLE_MODULE]: [TABS.MASTER_SALE_TEST_DRIVER_VEHICLE],
    [MODULE_PATHS.APP_MASTER_SALE_VEHICLE_PRICE_MODULE]: [TABS.MASTER_SALE_VEHICLE_PRICE],
    [MODULE_PATHS.APP_MASTER_SALE_LOOKUP_MODULE]: [TABS.MASTER_SALE_LOOKUP],
    [MODULE_PATHS.APP_MASTER_SALE_CONTACT_BY_MODULE]: [TABS.MASTER_SALE_CONTACT_BY],

    //SALES
    [MODULE_PATHS.APP_SALE_CUSTOMER_MODULE]: [TABS.SALE_CUSTOMER],
    [MODULE_PATHS.APP_ASSIGNMENT_CUSTOMER_MODULE]: [TABS.ASSIGNMENT_CUSTOMER],
    [MODULE_PATHS.APP_SALES_PERSON_MODULE]: [TABS.SALES_PERSON],
};
