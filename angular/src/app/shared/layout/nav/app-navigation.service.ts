﻿import { PermissionCheckerService } from 'abp-ng2-module';
import { AppSessionService } from '@shared/common/session/app-session.service';

import { Injectable } from '@angular/core';
import { AppMenu } from './app-menu';
import { AppMenuItem } from './app-menu-item';

@Injectable()
export class AppNavigationService {
    constructor(
        private _permissionCheckerService: PermissionCheckerService,
        private _appSessionService: AppSessionService
    ) { }

    getMenu(): AppMenu {
        return new AppMenu('MainMenu', 'MainMenu', [
            // Quản lý khách hàng
            new AppMenuItem(' Quản lý khách hàng', 'Pages.Sales.Customer', 'fas fa-users-cog', '', [], [
                new AppMenuItem(' Khách hàng tiềm năng', 'Pages.SalesCustomer', 'flaticon-more', 'SALE_CUSTOMER'),
                new AppMenuItem(' KH chuyển giao', '', 'flaticon-more', 'ASSIGNMENT_CUSTOMER'),
            ]),
            //Quản lý nhân viên
            new AppMenuItem(' Quản lý nhân viên', 'Pages.SalesPerson', 'flaticon-line-graph', '', [], [
                new AppMenuItem(' Nhân viên bán hàng', 'Pages.SalesPerson', 'flaticon-line-graph', 'SALES_PERSON'),
            ]),
            // Danh mục dùng chung
            new AppMenuItem(' Danh mục dùng chung ', 'Pages.Master', 'flaticon-line-graph', '', [], [
                new AppMenuItem(' Khai báo chung', 'Pages.Master.Common', 'flaticon-line-graph', '', [], [
                    new AppMenuItem(' Đại lý', 'Pages.Master.Common.MstGenDealers', 'flaticon-line-graph', 'MASTER_SALE_GEN_DEALER'),
                    new AppMenuItem(' Tỉnh/Thành phố', 'Pages.Master.Sales.MstGenProvinces', 'flaticon-line-graph', 'MASTER_SALE_PROVINCE'),
                    new AppMenuItem(' Quận/Huyện', 'Pages.Master.Common.MstSleDistrict', 'flaticon-line-graph', 'MASTER_SALE_DISTRICT'),
                ]),
                new AppMenuItem(' Thông tin Xe', 'Pages.Master.VehicleInfo', 'flaticon-line-graph', '', [], [
                    new AppMenuItem(' Loại xe', 'Pages.Master.Sales.MstSleModelGrade', 'flaticon-line-graph', 'MASTER_SALE_MODEL'),
                    new AppMenuItem(' Màu xe theo mẫu xe', "Pages.Master.Sales.MstSleColorByGrade", 'flaticon-line-graph', 'MASTER_SALE_COLOR_BY_GRADE'),
                    new AppMenuItem(' Màu chung', 'Pages.Master.Sales.MstSleColor', 'flaticon-line-graph', 'MASTER_SALE_COLOR'),
                ]),
                // new AppMenuItem(' Lái thử xe', 'Pages.Master.TestDriver', 'flaticon-line-graph', '', [], [
                //     new AppMenuItem(' Địa điểm lái thử xe', 'Pages.Master.Sales.MstSleTestDrivePlace', 'flaticon-line-graph', 'MASTER_SALE_TEST_DRIVER_PLACE'),
                //     new AppMenuItem(' Đường lái thử xe', 'Pages.Master.Sales.MstSleTestDriveRoute', 'flaticon-line-graph', 'MASTER_SALE_TEST_DRIVER_ROUTE'),
                //     new AppMenuItem(' Người hỗ trợ lái thử xe', 'Pages.Master.Sales.MstSleSupporter', 'flaticon-line-graph', 'MASTER_SALE_SUPPORTER'),
                //     new AppMenuItem(' Khoang lái thử', 'Pages.Master.Sales.MstSleTestDriveStall', 'flaticon-line-graph', 'MASTER_SALE_TEST_DRIVER_STALL'),
                //     new AppMenuItem(' Xe lái thử', 'Pages.Master.Sales.MstSleTestDriveVehicle', 'flaticon-line-graph', 'MASTER_SALE_TEST_DRIVER_VEHICLE'),
                // ]),
            ]),
            //Quản trị
            new AppMenuItem('Administration', 'Pages.Administration', 'flaticon-interface-8', '', [], [
                new AppMenuItem('OrganizationUnits', 'Pages.Administration.OrganizationUnits.Menu', 'flaticon-map', '/app/admin/organization-units'),
                //new AppMenuItem('Roles', 'Pages.Administration.Roles', 'flaticon-suitcase', '/app/admin/roles'),
                new AppMenuItem('Users', 'Pages.Administration.Users', 'flaticon-users', '/app/admin/users'),
                new AppMenuItem('Languages', 'Pages.Administration.Languages', 'flaticon-tabs', '/app/admin/languages', ['/app/admin/languages/{name}/texts']),
                //new AppMenuItem('AuditLogs', 'Pages.Administration.AuditLogs', 'flaticon-folder-1', '/app/admin/auditLogs'),
                //new AppMenuItem('Maintenance', 'Pages.Administration.Host.Maintenance', 'flaticon-lock', '/app/admin/maintenance'),
                //new AppMenuItem('Subscription', 'Pages.Administration.Tenant.SubscriptionManagement', 'flaticon-refresh', '/app/admin/subscription-management'),
                //new AppMenuItem('VisualSettings', 'Pages.Administration.UiCustomization', 'flaticon-medical', '/app/admin/ui-customization'),
                //new AppMenuItem('Settings', 'Pages.Administration.Host.Settings', 'flaticon-settings', '/app/admin/hostSettings'),
                //new AppMenuItem('Settings', 'Pages.Administration.Tenant.Settings', 'flaticon-settings', '/app/admin/tenantSettings'),
                //new AppMenuItem('WebhookSubscriptions', 'Pages.Administration.WebhookSubscription', 'flaticon2-world', '/app/admin/webhook-subscriptions'),
                // new AppMenuItem('DynamicParameters', '', 'flaticon-interface-8', '', [], [
                //     new AppMenuItem('Definitions', 'Pages.Administration.DynamicParameters', '', '/app/admin/dynamic-parameter'),
                //     new AppMenuItem('EntityDynamicParameters', 'Pages.Administration.EntityDynamicParameters', '', '/app/admin/entity-dynamic-parameter'),
                // ])
            ]),
        ], this._permissionCheckerService);
    }

    checkChildMenuItemPermission(menuItem): boolean {
        for (let i = 0; i < menuItem.items.length; i++) {
            let subMenuItem = menuItem.items[i];

            if (subMenuItem.permissionName === '' || subMenuItem.permissionName === null) {
                if (subMenuItem.route) {
                    return true;
                }
            } else if (this._permissionCheckerService.isGranted(subMenuItem.permissionName)) {
                return true;
            }

            if (subMenuItem.items && subMenuItem.items.length) {
                let isAnyChildItemActive = this.checkChildMenuItemPermission(subMenuItem);
                if (isAnyChildItemActive) {
                    return true;
                }
            }
        }
        return false;
    }

    showMenuItem(menuItem: AppMenuItem): boolean {
        if (menuItem.permissionName === 'Pages.Administration.Tenant.SubscriptionManagement' && this._appSessionService.tenant && !this._appSessionService.tenant.edition) {
            return false;
        }

        let hideMenuItem = false;

        if (menuItem.requiresAuthentication && !this._appSessionService.user) {
            hideMenuItem = true;
        }

        if (menuItem.permissionName && !this._permissionCheckerService.isGranted(menuItem.permissionName)) {
            hideMenuItem = true;
        }

        if (this._appSessionService.tenant || !abp.multiTenancy.ignoreFeatureCheckForHostUsers) {
            if (menuItem.hasFeatureDependency() && !menuItem.featureDependencySatisfied()) {
                hideMenuItem = true;
            }
        }

        if (!hideMenuItem && menuItem.items && menuItem.items.length) {
            return this.checkChildMenuItemPermission(menuItem);
        }

        return !hideMenuItem;
    }

    /**
     * Returns all menu items recursively
     */
    getAllMenuItems(): AppMenuItem[] {
        let menu = this.getMenu();
        let allMenuItems: AppMenuItem[] = [];
        menu.items.forEach(menuItem => {
            allMenuItems = allMenuItems.concat(this.getAllMenuItemsRecursive(menuItem));
        });
        return allMenuItems;
    }

    private getAllMenuItemsRecursive(menuItem: AppMenuItem): AppMenuItem[] {
        if (!menuItem.items) {
            return [menuItem];
        }

        let menuItems = [menuItem];
        menuItem.items.forEach(subMenu => {
            menuItems = menuItems.concat(this.getAllMenuItemsRecursive(subMenu));
        });

        return menuItems;
    }
}
