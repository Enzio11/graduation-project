import { Component, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AssignCustomerToSalesPersonDto, GetListSalesPersonNameDto, SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import { CustomColDef, FrameworkComponent, GridParams } from '@app/shared/common/models/base.model';
import { finalize } from 'rxjs/operators';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';

@Component({
    selector: 'assign-to-salesperson',
    templateUrl: './assign-to-salesperson.component.html',
    styleUrls: ['./assign-to-salesperson.component.less'],
})

export class AssignToSalesPersonModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    selectedRow: GetListSalesPersonNameDto =  new GetListSalesPersonNameDto();
    selectedId: number;
    customerId: number;
    frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent,
    };

    // For Ag Grid
    columnDefs: CustomColDef[] = [];
    defaultColDef = {
        flex: 1,
        resizable: true,
        sortable: true,
        suppressHorizontalScroll: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r: string | null) {
            if (r == null) return null;
            return r.toLowerCase();
        },
        minWidth: 100,
    };
    params!: GridParams;
    assignmentChecked: boolean = false;
    totalAssign: number = 0;
    listPersonId = [];
    listAssign: AssignCustomerToSalesPersonDto = new AssignCustomerToSalesPersonDto();
    InputAssign = new AssignCustomerToSalesPersonDto();

    //*Filter
    filterText: string = "";
    list: GetListSalesPersonNameDto[] = [];
    loading: boolean = false;

    constructor(
        injector: Injector,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
    ) {
        super(injector);

        this.columnDefs = [
            {
                headerName: this.l("SalesVehicleUioAssign"),
                headerTooltip: this.l("SalesVehicleUioAssign"),
                field: "assignChecked",
                cellClass: ["text-center"],
                data: [1, 0],
                minWidth: 1,
                cellRenderer: "agCheckboxRendererComponent",
            },
            {
                headerName: this.l('SalesPersonFullName'),
                headerTooltip: this.l('SalesPersonFullName'),
                field: 'fullName',
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonPosition'),
                headerTooltip: this.l('SalesPersonPosition'),
                field: 'position',
                flex: 1,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonPhone'),
                headerTooltip: this.l('SalesPersonPhone'),
                field: 'phone',
                flex: 1,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonEmail'),
                headerTooltip: this.l('SalesPersonEmail'),
                field: 'email',
                flex: 2,
                cellClass: ["text-left"],
            },
        ];
    }

    callBackGrid(params) {
        this.params = params;
    }

    onChangeSelection(params) {
        this.selectedRow = params.api.getSelectedRows()[0];
        if (this.selectedRow) {
            this.selectedId = this.selectedRow.id;
        }
    }

    onGridReady() {
        this.loading = true;
        this._salesCustomerWebServiceProxy.getListSalesPersonName(this.filterText)
        .pipe(finalize(() => this.loading = false))
        .subscribe(
            (result) => {
                this.list = result;
                this.params.api.setRowData(this.list);
            },
        );
    }

    show(listAssign): void {
        this.active = true;
        this.modal.show();
        this.listPersonId = [];
        this.listAssign = listAssign;

        this.onGridReady();
    }

    save(): void {
        this.saving = true;
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    search() {
        this.onGridReady();
    }

    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    assignCustomerToSalePerson() {
        this.InputAssign = new AssignCustomerToSalesPersonDto();
        this.InputAssign.salesPersonIds = [];
        this.InputAssign.salesPersonIds = this.listPersonId;

        this.InputAssign.customerIds = this.listAssign.customerIds;
        var messageUio = 'Bạn có chắc chắn chuyển giao ' + this.InputAssign.customerIds.length.toString() + ' khách hàng đã chọn cho ' +this.InputAssign.salesPersonIds.length.toString()+' TVBH này không?'
        this.message.confirm('', messageUio, (isConfirmed) => {
            if (isConfirmed) {
                this.loading = true;
                this._salesCustomerWebServiceProxy.assignCustomerToSalesPerson(this.InputAssign)
                    .pipe(finalize(() => this.loading = false))
                    .subscribe(() => {
                        this.notify.success(this.l("SavedSuccessfully"));
                        this.close();
                        this.modalSave.emit(null);
                    });
            }
        });
    }

    onCellValueChanged(params) {
        this.assignmentChecked = false;
        this.totalAssign = 0;

        if (params.column.colId == "assignChecked") {
            this.params.api.forEachNode((node) => {
                if (node.data.assignChecked == 1) {
                    this.totalAssign++;
                    this.assignmentChecked = true;
                };
            });
            if (params.data.assignChecked == 1) {
                this.listPersonId.push(params.data.id)
            }
            if (params.data.assignChecked == 0) {
                for (var i = 0; i < this.listPersonId.length; i++) {
                    if (this.listPersonId[i] == params.data.id) {
                        this.listPersonId.splice(i, 1);
                    }
                }
            }
        }
    }
}
