import { TABS } from "@app/shared/constants/tab-keys";
import { NgModule } from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { SalesCustomerPotentialComponent } from "./sales-customer-potential.component";
import { SalesCustomerDetailModalComponent } from "./sales-customer-detail/sales-customer-detail.component";
import { CompanyCustomerComponent } from "./company-customer/company-customer.component";
import { DuplicateCustomerComponent } from "./duplicate-customer/duplicate-customer.component";
import { SalesCustomerHistoryComponent } from "./sales-customer-history/sales-customer-history.component";
import { ContactToCustomerModal } from "./contact-to-customer/contact-to-customer.component";

const tabcode_component_dict = {
    [TABS.SALE_CUSTOMER]: SalesCustomerPotentialComponent
};
@NgModule({
    imports: [
        CommonDeclareModule,
    ],
    declarations: [
        SalesCustomerPotentialComponent,
        SalesCustomerDetailModalComponent,
        CompanyCustomerComponent,
        DuplicateCustomerComponent,
        SalesCustomerHistoryComponent,
        ContactToCustomerModal
    ],
})

export class SalesCustomerPotentialModule {
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
    }
}