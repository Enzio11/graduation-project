import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SalesCustomerCarAttentionDto, SalesCustomerDetailDto, SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import { CustomColDef, FrameworkComponent, GridParams } from '@app/shared/common/models/base.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { AgCellButtonRendererComponent } from '@app/shared/common/grid/ag-cell-button-renderer/ag-cell-button-renderer.component';

@Component({
    selector: 'sales-customer-detail-modal',
    templateUrl: './sales-customer-detail.component.html',
    styleUrls: ['./sales-customer-detail.component.less'],
})

export class SalesCustomerDetailModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    loading: boolean = false;
    canSave: boolean = false;

    isShowAdditionalInfo: boolean = false;
    isShowCarAttention: boolean = false;

    customerId: number;
    districts: { value: number, label: string }[] = [];
    provinces = [];
    hobbies: { value: number, label: string }[] = [];
    genders: { value: number, label: string }[] = [
        { value: 1, label: "Nam" },
        { value: 2, label: "Nữ" },
        { value: 3, label: "Khác" }
    ];
    hotnesses: { value: number, label: string }[] = [];
    customerTypes: { value: number, label: string }[] = [
        { value: 1, label: "Cá nhân" },
        { value: 2, label: "Doanh nghiệp" }
    ];
    sourceOfInfos: { value: number, label: string }[] = [];
    expectedDelTimings = [];
    expectedDelTimingsForCreate: { value: number, label: string }[] = [];
    maritalStatuses: { value: number, label: string }[] = [
        { value: 1, label: "Độc thân" },
        { value: 2, label: "Đã kết hôn" },
        { value: 3, label: "Khác" }
    ];
    occupations: { value: number, label: string }[] = [];
    makes = [];
    grades = [];
    colors = [];
    models = [];
    fars = [];
    purposes = [];
    nextActionStatus: { value: number, label: string }[] = [
        { value: undefined, label: "" },
        { value: 1, label: "Enable" },
        { value: 0, label: "Disable" }
    ];
    contactBy: { value: number, label: string }[] = [];
    salesStages: { value: number, label: string }[] = [];
    sources: { value: number, label: string }[] = [];

    rowData: SalesCustomerCarAttentionDto[] = [];
    params!: GridParams;
    defaultColDef = {
        rowHeight: 50,
        minWidth: 80,
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        singleClickEdit: true,
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
        headerComponentParams: {
            template:
                '<div class="ag-cell-label-container" role="presentation">' +
                '  <span ref="eMenu" class="ag-header-icon ag-header-cell-menu-button"></span>' +
                '  <div ref="eLabel" class="ag-header-cell-label" role="presentation">' +
                '    <span ref="eSortOrder" class="ag-header-icon ag-sort-order"></span>' +
                '    <span ref="eSortAsc" class="ag-header-icon ag-sort-ascending-icon"></span>' +
                '    <span ref="eSortDesc" class="ag-header-icon ag-sort-descending-icon"></span>' +
                '    <span ref="eSortNone" class="ag-header-icon ag-sort-none-icon"></span>' +
                '    <span ref="eText" class="ag-header-cell-text" role="columnheader" style="white-space: normal;"></span>' +
                '    <span ref="eFilter" class="ag-header-icon ag-filter-icon"></span>' +
                '  </div>' +
                '</div>',
        },
    };

    vehicleColumnDefs: CustomColDef[] = [
        {
            headerName: this.l('Action'),
            cellRenderer: 'agCellButtonComponent',
            buttonDef: {
                text: this.l('Delete'),
                useRowData: true,
                iconName: 'fa fa-trash',
                function: this.deleteCarAttention.bind(this),
            },
            maxWidth: 100,
            cellClass: ["text-center"]
        },
        {
            headerName: this.l("Hãng xe"),
            cellEditor: 'agRichSelectCellEditor',
            cellClass: ["text-center", 'cell-clickable'], field: "makeId",
            minWidth: 120,
            editable: true,
            cellRenderer: (params) => this.makes.find(e => e.id == params.value)?.description,
            cellEditorParams: (p) => {
                return {
                    cellRenderer: (params) => this.makes.find(e => e.id == params.value)?.description,
                    values: this.getListMaster(undefined, "makeId"),
                    searchDebounceDelay: 5000
                }
            },
        },
        {
            headerName: this.l("Model"),
            cellEditor: 'agRichSelectCellEditor',
            cellClass: ["text-center", 'cell-clickable'], field: "modelId",
            minWidth: 120,
            editable: true,
            cellRenderer: (params) => this.models.find(e => e.id == params.value)?.marketingCode,
            cellEditorParams: (p) => {
                return {
                    cellRenderer: (params) => this.models.find(e => e.id == params.value)?.marketingCode,
                    values: this.getListMaster(p.data.makeId, "modelId"),
                    searchDebounceDelay: 5000
                }
            },
        },
        {
            headerName: this.l("Grade"),
            cellEditor: 'agRichSelectCellEditor',
            cellClass: ["text-center", 'cell-clickable'], field: "gradeId", minWidth: 100,
            editable: true,
            cellRenderer: (params) => this.grades.find(e => e.id == params.value)?.marketingCode,
            cellEditorParams: (p) => {
                return {
                    cellRenderer: (params) => this.grades.find(e => e.id == params.value)?.marketingCode,
                    values: this.getListMaster(p.data.modelId, "gradeId")
                }
            },
        },
        {
            headerName: this.l("ExternalColor"),
            cellEditor: 'agRichSelectCellEditor',
            cellClass: ["text-center", 'cell-clickable'], field: "colorId", width: 155,
            editable: true,
            cellRenderer: (params) => this.colors.find(e => e.colorId == params.value)?.color,
            cellEditorParams: (p) => {
                return {
                    cellRenderer: (params) => this.colors.find(e => e.colorId == params.value)?.color,
                    values: this.getListMaster(p.data.gradeId, "colorId")
                }
            },
        },
        {
            headerName: this.l('Hình thức mua'),
            cellEditor: 'agRichSelectCellEditor',
            cellClass: ['text-left'], field: "farId",
            cellRenderer: (params) => this.fars.find(e => e.id == params.value)?.description,
            cellEditorParams: (p) => {
                return {
                    cellRenderer: (params) => this.fars.find(e => e.id == params.value)?.description,
                    values: this.getListMaster(undefined, "farId")
                }
            },
            editable: true
        },
        {
            headerName: this.l('Mục đích mua xe'),
            cellEditor: 'agRichSelectCellEditor',
            cellClass: ['text-left', 'cell-clickable'], field: "purposeId",
            cellRenderer: (params) => this.purposes.find(e => e.id == params.value)?.description,
            cellEditorParams: (p) => {
                return {
                    cellRenderer: (params) => this.purposes.find(e => e.id == params.value)?.description,
                    values: this.getListMaster(undefined, "purposeId")
                }
            },
            editable: true
        }
    ];

    frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent,
        agCellButtonComponent: AgCellButtonRendererComponent,
    };

    // Customer Detail
    customerDetail: SalesCustomerDetailDto = new SalesCustomerDetailDto();
    carAttention: SalesCustomerCarAttentionDto = new SalesCustomerCarAttentionDto();

    constructor(
        Injector: Injector,
        private _localStorage: LocalStorageService,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
    ) {
        super(Injector);
    }

    ngOnInit() {
        var provinceFromLocal = [];
        provinceFromLocal = this._localStorage.get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList).province;
        provinceFromLocal.forEach((element) => {
            this.provinces.push({ value: element.id, label: element.name, listDistrict: element.listDistrict });
        })

        this.getMasterData();
    }

    getListMaster(value, key) {
        if (key == "makeId") {
            let result = [...this.makes].map(e => e.id);
            return result;
        }
        if (key == "modelId") {
            let result = [...this.models].filter(e => e.makeId == value).map(e => e.id);
            return result;
        }
        if (key == "gradeId") {
            let result = [...this.grades].filter(e => e.modelId == value).map(e => e.id);
            return result;
        }
        if (key == "colorId") {
            let result = [...this.colors].filter(e => e.gradeId == value).map(e => e.colorId);
            return result;
        }
        if (key == "farId") {
            let result = [...this.fars].map(e => e.id);
            return result;
        }
        if (key == "purposeId") {
            let result = [...this.purposes].map(e => e.id);
            return result;
        }
    }

    getMasterData() {
        this.hotnesses.push({ value: undefined, label: this.l("") });
        this.hobbies.push({ value: undefined, label: this.l("") });
        this.sourceOfInfos.push({ value: undefined, label: this.l("") });
        this.occupations.push({ value: undefined, label: this.l("") });
        this.contactBy.push({ value: undefined, label: "" });
        this.salesStages.push({ value: undefined, label: "" });
        this.sources.push({ value: undefined, label: "" });

        this._salesCustomerWebServiceProxy.getCustomerDetailMasterData(undefined).subscribe((result) => {
            this.makes = result.makes;
            this.models = result.models;
            this.grades = result.grades;
            this.colors = result.colors;
            this.fars = result.fars;
            this.purposes = result.purposes;
            this.expectedDelTimings = result.expectedDelTimings;

            result.hotnesses.forEach(e => this.hotnesses.push({ value: e.id, label: e.description }));
            result.hobbies.forEach(e => this.hobbies.push({ value: e.id, label: e.description }));
            result.sourceOfInfos.forEach(e => this.sourceOfInfos.push({ value: e.id, label: e.description }));
            result.occupations.forEach(e => this.occupations.push({ value: e.id, label: e.description }));
            result.contactBy.forEach(e => this.contactBy.push({ value: e.id, label: e.description }));
            result.salesStages.forEach(e => this.salesStages.push({ value: e.id, label: e.description }));
            result.sources.forEach(e => this.sources.push({ value: e.id, label: e.description }));
        });
    }

    //! Sales Customer
    checkDataBeforeSave() {
        if (!this.customerDetail.customerTypeId) {
            this.notify.info(this.l('Xin hãy chọn Loại KH!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.contactDate) {
            this.notify.info(this.l('Xin hãy chọn Ngày Liên hệ!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.customerFirstName) {
            this.notify.info(this.l('Xin hãy nhập Tên Khách hàng!'));
            return this.canSave = false;
        }

        if (!this.customerDetail.customerTel1) {
            this.notify.info(this.l('Xin hãy nhập Số điện thoại!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.sourceId) {
            this.notify.info(this.l('Xin hãy chọn Nguồn khách hàng!'));
            return this.canSave = false;
        }

        if (!this.customerDetail.provinceId) {
            this.notify.info(this.l('Xin hãy chọn Tỉnh/TP!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.districtId) {
            this.notify.info(this.l('Xin hãy chọn Quận/Huyện!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.status) {
            this.notify.info(this.l('Xin hãy chọn Trạng thái KH!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.hotnessId) {
            this.notify.info(this.l('Xin hãy chọn Tình trạng KH!'));

            return this.canSave = false;
        }

        if (!this.customerDetail.expectedDelTimingId) {
            this.notify.info(this.l('Xin hãy chọn Thời gian dự kiến ký hợp đồng!'));

            return this.canSave = false;
        }

        this.canSave = true;
    }

    saveCustomerDetail(): void {
        this.checkDataBeforeSave();

        setTimeout(() => {
            if (this.canSave == true) {
                let result = true;

                if (this.rowData.length == 0) {
                    return this.notify.warn("Cần thêm thông tin xe quan tâm của khách hàng!");
                }
        
                for (let i = 0; i < this.rowData.length; i++) {
                    while (this.validateVehicleData(this.rowData[i], i + 1) == false) {
                        result = false;
                        break;
                    }
                }
                
                if (result == true) {
                    this.saving = true;
                    
                    this.customerDetail.customerCarAttentions = this.rowData;
                    this._salesCustomerWebServiceProxy.createOrEdit(this.customerDetail)
                        .pipe(finalize(() => { this.saving = false; }))
                        .subscribe(() => {
                            this.notify.info(this.l('SavedSuccessfully'));
                            this.close();
                            this.modalSave.emit(null);
                        });
                }
            }
        }, 10);
    }
    //! End Sales Customer

    //* Mastter Get Function
    getExpectedDelTimingByHotness(hotnessId) {
        this.expectedDelTimingsForCreate.length = 0;
        [...this.expectedDelTimings].filter(e => e.hotnessId == hotnessId).forEach(e => this.expectedDelTimingsForCreate.push({ value: e.id, label: e.description }));
    }

    getDistrictByProvince() {
        this.districts = [];

        this.districts = [...this.provinces.find(e => e.value == this.customerDetail.provinceId).listDistrict].map(e => { return { value: e.id, label: e.name } });
        this.districts.unshift({ value: -1000, label: "" })
    }

    //* Mastter Get Function

    show(salesCustomerId?: number): void {
        this.customerId = salesCustomerId;
        this.customerDetail = new SalesCustomerDetailDto();

        if (this.customerId) {
            this.loading = true;
            this._salesCustomerWebServiceProxy.getSalesCustomerDetails(this.customerId)
                .pipe(finalize(() => { this.loading = false; }))
                .subscribe((result) => {
                    this.customerDetail = result;
                    this.rowData = result.customerCarAttentions;

                    if (this.customerDetail.provinceId != null) {
                        this.getDistrictByProvince();
                    }

                    if (this.customerDetail.hotnessId != null) {
                        this.getExpectedDelTimingByHotness(this.customerDetail.hotnessId);
                    }
                });

            this.active = true;
            this.modal.show();
        }
        else {
            this.customerDetail.customerTypeId = 1;
            this.customerDetail.status = 3;
            this.customerDetail.creationTime = moment();
            this.customerDetail.contactDate = moment();

            this.active = true;
            this.modal.show();
        }
    }

    close(): void {
        this.active = false;

        this.modal.hide();
    }

    checkCustomerTel(event) { }

    deleteCarAttention(event) {
        this.message.confirm('', this.l('Bạn có chắc chắn xoá? Bản ghi đã xoá sẽ không thể khôi phục lại'), (isConfirmed) => {
            if (isConfirmed) {
                if (event.data.id) {
                    this._salesCustomerWebServiceProxy.deleteCarAttention(event.data.id).subscribe(() => {
                        this.notify.info(this.l("SuccessfullyDeleted"));
                    });
                }
                this.rowData.splice(event.node.rowIndex, 1);
                this.params.api.setRowData(this.rowData);
            }
        });
    }

    callBackGrid(params) {
        this.params = params;
    }

    onRowSelected(params) {
        //this.selectedVehicle = params.api.getSelectedRows()[0];
    }

    onCellValueChanged(cellParams) {
        if (cellParams.colDef.field === "makeId") {
            this.rowData[cellParams.rowIndex].modelId = undefined;
            this.rowData[cellParams.rowIndex].gradeId = undefined;
            this.rowData[cellParams.rowIndex].colorId = undefined;

            this.params.api.setRowData(this.rowData);
        }

        if (cellParams.colDef.field === "modelId") {
            this.rowData[cellParams.rowIndex].gradeId = undefined;
            this.rowData[cellParams.rowIndex].colorId = undefined;

            this.params.api.setRowData(this.rowData);
        }

        if (cellParams.colDef.field === "gradeId") {
            this.rowData[cellParams.rowIndex].colorId = undefined;

            this.params.api.setRowData(this.rowData);
        }
    }

    createVehicle() {
        this.carAttention = new SalesCustomerCarAttentionDto();

        this.rowData.push(this.carAttention)
        this.params.api.setRowData(this.rowData);
    }

    validateVehicleData(item, index) {
        if (!item?.makeId) {
            this.notify.warn("Cần nhập thông tin Hãng xe của xe thứ " + index.toString());
            return false;
        }
        else if (!item?.modelId) {
            this.notify.warn("Cần nhập thông tin Mẫu xe của xe thứ " + index.toString());
            return false;
        } else if (!item?.gradeId) {
            this.notify.warn("Cần nhập thông tin Mã xe của xe thứ " + index.toString());
            return false;
        } else if (!item?.colorId) {
            this.notify.warn("Cần nhập thông tin Màu ngoại thất của xe thứ " + index.toString());
            return false;
        }
        else if (!item?.purposeId) {
            this.notify.warn("Cần nhập thông tin Mục đích mua xe của xe thứ " + index.toString());
            return false;
        }
        else {
            return true;
        }
    }
}
