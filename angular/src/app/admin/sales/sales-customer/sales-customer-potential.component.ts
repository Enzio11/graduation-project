import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUpload } from "primeng/fileupload";
import { GetFilterSalesCustomerDto, GetSalesCustomerForViewDto, MstSleTestDriveRouteServiceProxy, SalesCustomerWebServiceProxy, SessionServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { ceil } from 'lodash';
import { AgDateEditorComponent } from '@app/shared/common/grid/ag-datepicker-editor/ag-date-editor.component';
import { DataFormatService } from "@app/shared/common/services/data-format.service";
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { SalesCustomerDetailModalComponent } from './sales-customer-detail/sales-customer-detail.component';
import { AgTimeEditorComponent } from '@app/shared/common/grid/ag-timepicker-editor/ag-time-editor.component';
import * as moment from 'moment';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { ValueGetterParams } from '@ag-grid-enterprise/all-modules';
import { CompanyCustomerComponent } from './company-customer/company-customer.component';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { DuplicateCustomerComponent } from './duplicate-customer/duplicate-customer.component';
import { SalesCustomerHistoryComponent } from './sales-customer-history/sales-customer-history.component';
import { ContactToCustomerModal } from './contact-to-customer/contact-to-customer.component';

@Component({
    selector: 'sales-customer-potential',
    templateUrl: './sales-customer-potential.component.html',
    styleUrls: ['./sales-customer-potential.component.less'],
})

export class SalesCustomerPotentialComponent extends AppComponentBase implements OnInit {
    @ViewChild("ExcelFileUpload", { static: false }) excelFileUpload: FileUpload;
    @ViewChild('showSalesCustomerDetail') showSalesCustomerDetail: SalesCustomerDetailModalComponent;
    @ViewChild('CompanyCustomer') CompanyCustomer: CompanyCustomerComponent;
    @ViewChild('DuplicateCustomer') DuplicateCustomer: DuplicateCustomerComponent;
    @ViewChild('ContactToCustomer') ContactToCustomer: ContactToCustomerModal;
    
    @ViewChild("customerHistory") customerHistory: SalesCustomerHistoryComponent;

    customerSelected: boolean = true;
    dupicateSeleted: boolean = false;
    sendCommentToSalesPerson: GetSalesCustomerForViewDto = new GetSalesCustomerForViewDto();
    companySelected: boolean = false;
    showCustomerHistory: boolean = true;
    cellParams: any;
    loading: boolean = false;
    hotnesses: { value: number, label: string }[] = [];
    models: { value: number, label: string }[] = [];
    sources: { value: number, label: string }[] = [];
    salesPersons: { value: number, label: string }[] = [];
    districts: { value: number, label: string }[] = [];
    provinces: { value: number, label: string }[] = [];
    grades: { value: number, label: string }[] = [];
    units: { value: number, label: string }[] = [];
    contactBy: { value: number, label: string }[] = [];
    customerClass: { value: number, label: string }[] = [
        {value: 1, label: this.l("SalesCustomerInquiryCustomer")},
        {value: 2, label: this.l("SalesCustomerFreezeCustomer")},
        {value: 3, label: this.l("SalesCustomerLostCustomer")},
        {value: 4, label: this.l("SalesCustomerInvoiceCustomer")}
    ];
    freezeTypes: { value?: number, label: string }[] = [
        {value: 3, label: this.l("All")},
        {value: 2, label: this.l("SalesCustomerFreezeProactive")},
        {value: 1, label: this.l("SalesCustomerFreezePassive")},
    ];
    listObject: any;
    objectData = [];

    columnDefs: CustomColDef[] = [];

    defaultColDef = {
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };

    countCarPurchaseRequestFilter: GetFilterSalesCustomerDto = new GetFilterSalesCustomerDto();

    sideBar = {
        toolPanels: [
            {
                id: 'columns',
                labelDefault: this.l("Columns"),
                labelKey: 'columns',
                iconKey: 'columns',
                toolPanel: 'agColumnsToolPanel',
                toolPanelParams: {
                    suppressRowGroups: true,
                    suppressValues: true,
                    suppressPivots: true,
                    suppressPivotMode: true,
                    suppressColumnFilter: false
                },
            },
        ],
        defaultToolPanel: '',
    };

    frameworkComponents: FrameworkComponent = {
        agDateEditorComponent: AgDateEditorComponent,
        agTimeEditorComponent: AgTimeEditorComponent,
        agCheckboxRendererComponent: AgCheckboxRendererComponent
    };

    //? Individual Customer
    advancedFiltersAreShown: boolean = false;
    paginationParams: PaginationParamsModel = { pageNum: 1, pageSize: 20, totalCount: 0 };

    params!: GridParams;
    totalPages: number = 0;
    filterText: string = "";
    hotnessId: number;
    customerName: string = "";
    skipCount: number = 0;
    maxResultCount: number = 20;
    sorting: string = "";
    list = [];
    selectedRow: GetSalesCustomerForViewDto = new GetSalesCustomerForViewDto();
    selectedCustomerId: number;
    customerTypeId: number;
    dealerId: number;
    creationDateTo = moment();
    creationDateFrom = moment('2021-01-01');
    contactDateFrom = moment().add(-1, 'days');
    contactDateTo = moment();
    unitId: number;
    salePersonId: number;
    sourceId: number;
    districtId: number = -1;
    provinceId: number = -1;
    modelId: number;
    customerTel1: string = '';
    gradeId: number;
    customerClassId: number = 1;
    contactById: number = 0;
    freezeTypeId: number = 3;

    getLatestContact: boolean = true;
    carAttention: string = '';
    listDealer = [];
    disableDealer: boolean = false;
    isCreationDate: boolean = true;
    isContactDate: boolean = false;
    totalCarPurchaseRequest: number = 0;
    totalInquiryCustomer: number = 0;

    //#region Constructor
    constructor(
        Injector: Injector,
        private dataFormatService: DataFormatService,
        private _localStorage: LocalStorageService,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
        private _mstSleTestDriveRouteServiceProxy: MstSleTestDriveRouteServiceProxy,
        private _sessionService: SessionServiceProxy,
    ) {
        super(Injector);
        
        //#region ColumnDefs
        this.columnDefs = [
            {
                headerName: this.l('SalesCustomerSalesPerson'),
                field: 'salesPerson',
                minWidth: 150,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerDepartment'),
                field: 'department',
                maxWidth: 120,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('CreationDate'),
                field: 'creationTime',
                maxWidth: 140,
                cellClass: ["text-left"],
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerFirstContactDate'),
                field: 'firstContactDate',
                maxWidth: 140,
                cellClass: ["text-left"],
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerCode'),
                field: 'customerNo',
                minWidth: 130,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerName'),
                field: 'name',
                minWidth: 130,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerPhoneNo'),
                field: 'customerTel1',
                maxWidth: 130,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Province'),
                field: 'provinceId',
                maxWidth: 100,
                cellClass: ["text-left"],
                valueGetter: (params: ValueGetterParams) => this.provinces.find(p => p.value === params.data.provinceId)
                    ? (this.provinces.find(p => p.value === params.data.provinceId).label ?? null) : null
            },
            {
                headerName: this.l('District'),
                field: 'districtId',
                maxWidth: 120,
                cellClass: ["text-left"],
                valueGetter: (params: ValueGetterParams) => this.districts.find(p => p.value === params.data.districtId)
                    ? (this.districts.find(p => p.value === params.data.districtId).label ?? null) : null
            },
            {
                headerName: this.l('Address'),
                field: 'address',
                minWidth: 160,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerLeadSource'),
                field: 'source',
                maxWidth: 180,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerSalesStage'),
                field: 'status',
                minWidth: 130,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerReasonLostDate'),
                field: 'reasonLostDate',
                maxWidth: 130,
                cellClass: ["text-left"],
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
                hide: true
            },
            {
                headerName: this.l('SalesCustomerReasonLost'),
                field: 'reasonLost',
                minWidth: 130,
                cellClass: ["text-left"],
                hide: true
            },
            {
                headerName: this.l('SalesCustomerFreezeType'),
                field: 'freezeType',
                valueFormatter: params => {
                    let res = '';
                    if (params.data.freezeType == true) res = this.freezeTypes.find(e => e.value == 1).label;
                    if (params.data.freezeType == false) res = this.freezeTypes.find(e => e.value == 2).label;
                    if (params.data.freezeType == null) res = '';
                    return res;
                },
                minWidth: 130,
                cellClass: ["text-left"],
                hide: true
            },
            {
                headerName: this.l('SalesCustomerReasonFreeze'),
                field: 'reasonLost',
                minWidth: 130,
                cellClass: ["text-left"],
                hide: true
            },
            {
                headerName: this.l('SalesCustomerHotness'),
                field: 'hotness',
                minWidth: 130,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerExpectedDelTiming'),
                field: 'expectedDelTiming',
                maxWidth: 140,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerCarAttention'),
                field: 'carAttention',
                minWidth: 200,
                cellClass: ["text-left"],
            },
            // {
            //     headerName: this.l('SalesCustomerIsShowRoomVisit'),
            //     field: 'isShowRoomVisit',
            //     maxWidth: 100,
            //     cellRenderer: "agCheckboxRendererComponent",
            //     data: [true, false],
            //     cellClass: ["text-center"],
            //     disableCheckbox: true
            // },
            // {
            //     headerName: this.l('SalesCustomerIsTestDrive'),
            //     field: 'isTestDrive',
            //     maxWidth: 100,
            //     cellRenderer: "agCheckboxRendererComponent",
            //     data: [true, false],
            //     cellClass: ["text-center"],
            //     disableCheckbox: true
            // },
            // {
            //     headerName: this.l('SalesCustomerReasonWhyNotTD'),
            //     field: 'reasonWhyNotTestDrive',
            //     maxWidth: 120,
            //     cellClass: ["text-left"]
            // },
            // {
            //     headerName: this.l('SalesCustomerTestDriveDate'),
            //     field: 'testDriveDate',
            //     maxWidth: 140,
            //     cellClass: ["text-left"],
            //     valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            // },
            {
                headerName: this.l('SalesCustomerCurrentDate'),
                field: 'currentDate',
                maxWidth: 140,
                cellClass: ["text-left"],
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerCurrentAction'),
                field: 'currentAction',
                minWidth: 100,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerNextDate'),
                field: 'nextContact',
                maxWidth: 140,
                cellClass: ["text-left"],
                valueFormatter: (params) => this.dataFormatService.dateFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerNextAction'),
                field: 'nextPlan',
                minWidth: 100,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerManagerNoti'),
                field: 'managerComment',
                minWidth: 200,
                editable: true,
                cellClass: ['text-left', 'cell-clickable'],
            }
        ];
        //#endregion
    }
    //#endregion

    //#region OnInit
    ngOnInit(): void {
        this.list = [];
        this.contactBy.push({ value: 0, label: this.l("All")});
        this._salesCustomerWebServiceProxy.getContactByForSalesCustomer().subscribe((result) => {
            result.forEach(e => this.contactBy.push({ value: e.id, label: e.description }));
        });

        this.hotnesses.push({ value: null, label: this.l("")});
        this._salesCustomerWebServiceProxy.getHotnessForSalesCustomer().subscribe((result) => {
            result.forEach(e => this.hotnesses.push({ value: e.id, label: e.description }));
        });

        this.models.push({ value: null, label: this.l("")});
        this._salesCustomerWebServiceProxy.getModelForSalesCustomer(1).subscribe((result) => {
            //? 1:Chỉ lấy xe của Toyota
            result.forEach(e => this.models.push({ value: e.id, label: e.marketingCode }));
        });

        this.sources.push({ value: null, label: this.l("All") })
        this._salesCustomerWebServiceProxy.getSourceForSalesCustomer().subscribe((result) => {
            result.forEach(e => this.sources.push({ value: e.id, label: e.description }));
        });

        this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).district.forEach(e => this.districts.push({ value: e.id, label: e.name }));

        this.provinces.push({ value: -1, label: this.l("All") })
        this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).province.forEach(e => this.provinces.push({ value: e.id, label: e.name }));

        this.units.push({ value: null, label: this.l("") })
        this._salesCustomerWebServiceProxy.getOrganiztionUnitsByCurrentUser().subscribe((result) => {
            result.forEach(e => this.units.push({ value: e.id, label: e.displayName }));
        });

        this._salesCustomerWebServiceProxy.getListSalesPersonName("").subscribe((result) => {
            if (result != null) {
                result.forEach(e => this.salesPersons.push({ value: e.id, label: e.fullName }));
            }
        });
        
        this._sessionService.getCurrentLoginInformations().subscribe((res) => {
            if (!res.tenant) {
                this._mstSleTestDriveRouteServiceProxy
                    .getDealerForTestDrive()
                    .subscribe((result) => {
                        this.listDealer.push({ value: null, label: this.l("All") })
                        result.forEach(e => this.listDealer.push({ value: e.id, label: e.abbreviation }));
                    });
            } else {
                this.listDealer.push({
                    value: res.tenant.id,
                    label: res.tenant.tenancyName,
                });
                this.dealerId = res.tenant.id;
                this.disableDealer = true;
            }
        });
    }
    //#endregion

    getDistrictByProvince(event){
        this.districts = [];
        this.districts.push({ value: -1, label: this.l("All") })
        var listDistrict = this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).province.find(p => p.id == event);
        listDistrict.listDistrict.forEach(e => this.districts.push({ value: e.id, label: e.name }));
    }

    //#region  Sales Customer
    callBackGrid(params) {
        this.params = params;
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);

        this.onGridReady(this.paginationParams);
    }

    onGridReady(paginationParams) {
        this.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;

        this.getAllIndividualCustomer();
    };

    changePage(paginationParams){
        this.paginationParams = paginationParams;
        this.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;
        this.getAllIndividualCustomer();
    }

    onChangeSelection(params) {
        this.selectedCustomerId = undefined;

        this.selectedRow = params.api.getSelectedRows()[0];
        if (this.selectedRow){
            this.selectedCustomerId = this.selectedRow.id;
            if (this.showCustomerHistory == true) this.customerHistory.getData(this.selectedCustomerId)
        }
    }

    getAllIndividualCustomer(){
        this.listObject = [];
        this.objectData = [];
        this.list = [];

        this.countCarPurchaseRequestFilter.filter = this.filterText;
        this.countCarPurchaseRequestFilter.unitId = this.unitId;
        this.countCarPurchaseRequestFilter.salesPersonId = this.salePersonId;
        this.countCarPurchaseRequestFilter.hotnessId = this.hotnessId;
        this.countCarPurchaseRequestFilter.sourceId = this.sourceId;
        this.countCarPurchaseRequestFilter.districtId = this.districtId;
        this.countCarPurchaseRequestFilter.provinceId = this.provinceId;
        this.countCarPurchaseRequestFilter.customerName = this.customerName;
        this.countCarPurchaseRequestFilter.customerTypeId = 1; // khách hàng cá nhân
        this.countCarPurchaseRequestFilter.dealerId = this.dealerId;
        this.countCarPurchaseRequestFilter.modelId = this.modelId;
        this.countCarPurchaseRequestFilter.gradeId = this.gradeId;
        this.countCarPurchaseRequestFilter.customerClass = this.customerClassId;
        this.countCarPurchaseRequestFilter.creationDateFrom = this.isCreationDate == true ? this.creationDateFrom : null;
        this.countCarPurchaseRequestFilter.creationDateTo = this.isCreationDate == true ? this.creationDateTo : null;
        this.countCarPurchaseRequestFilter.contactHistoryDateFrom = this.isContactDate == true ? this.contactDateFrom : null;
        this.countCarPurchaseRequestFilter.contactHistoryDateTo = this.isContactDate == true ? this.contactDateTo : null;
        this.countCarPurchaseRequestFilter.contactById = this.contactById;
        this.countCarPurchaseRequestFilter.customerTel1 = this.customerTel1;
        this.countCarPurchaseRequestFilter.sorting = this.sorting;
        this.countCarPurchaseRequestFilter.skipCount = this.skipCount;
        this.countCarPurchaseRequestFilter.maxResultCount = this.maxResultCount

        //Không lấy liên hệ mới nhất
        if (this.getLatestContact == false) {
            this.loading = true;
            this._salesCustomerWebServiceProxy.getAllCustomerForView(
                this.filterText,
                this.unitId,
                this.salePersonId,
                this.hotnessId,
                this.sourceId,
                this.districtId,
                this.provinceId,
                this.customerName,
                this.customerTypeId = 1, // khách hàng cá nhân
                this.dealerId,
                this.modelId,
                this.gradeId,
                this.customerClassId,
                this.isCreationDate == true ? this.creationDateFrom : null,
                this.isCreationDate == true ? this.creationDateTo : null,
                this.isContactDate == true ? this.contactDateFrom : null,
                this.isContactDate == true ? this.contactDateTo : null,
                this.contactById,
                this.customerTel1,
                this.customerClassId == 2 ? this.freezeTypeId : 3,
                this.sorting,
                this.skipCount,
                this.maxResultCount
            )
            .pipe(finalize(() => this.loading = false))
            .subscribe(
                (result) => {
                    result.items.forEach((element) => {
                        this.carAttention = '';
                        for (let i = 0; i < element.carAttention.length; i++) {
                            this.carAttention = this.carAttention + element.carAttention[i].model + " - " + element.carAttention[i].grade + ', ';
                        }
                        this.carAttention = this.carAttention.slice(0, this.carAttention.length - 2);

                        this.listObject = Object.assign(
                            {},
                            element.salesCustomerInfomation,
                            {carAttention: this.carAttention},
                        );

                        this.objectData.push(this.listObject);
                    });
                    this.list = this.objectData;
                    this.totalPages = ceil(result.totalCount / this.maxResultCount);
                    this.paginationParams.totalCount = result.totalCount;
                    this.paginationParams.totalPage = this.totalPages;
                }
            );
        }

        //Lấy liên hệ mới nhất
        if (this.getLatestContact) {
            this.loading = true;
            this._salesCustomerWebServiceProxy.getLatestContactCustomer(
                this.filterText,
                this.unitId,
                this.salePersonId,
                this.hotnessId,
                this.sourceId,
                this.districtId,
                this.provinceId,
                this.customerName,
                this.customerTypeId = 1, // khách hàng cá nhân
                this.dealerId,
                this.modelId,
                this.gradeId,
                this.customerClassId,
                this.isCreationDate == true ? this.creationDateFrom : null,
                this.isCreationDate == true ? this.creationDateTo : null,
                this.isContactDate == true ? this.contactDateFrom : null,
                this.isContactDate == true ? this.contactDateTo : null,
                this.contactById,
                this.customerTel1,
                this.customerClassId == 2 ? this.freezeTypeId : 3,
                this.sorting,
                this.skipCount,
                this.maxResultCount
            )
            .pipe(finalize(() => this.loading = false))
            .subscribe(
                (result) => {
                    result.items.forEach((element) => {
                        this.carAttention = '';
                        for (let i = 0; i < element.carAttention.length; i++) {
                            this.carAttention = this.carAttention + element.carAttention[i].model + " - " + element.carAttention[i].grade + ', ';
                        }
                        this.carAttention = this.carAttention.slice(0, this.carAttention.length - 2);

                        this.listObject = Object.assign(
                            {},
                            element.salesCustomerInfo,
                            {carAttention: this.carAttention},
                        );
                        this.objectData.push(this.listObject);
                    });
                    this.list = this.objectData;
                    this.totalPages = ceil(result.totalCount / this.maxResultCount);
                    this.paginationParams.totalCount = result.totalCount;
                    this.paginationParams.totalPage = this.totalPages;
                }
            );
        }

        this._salesCustomerWebServiceProxy.countCarPurchaseRequest(this.countCarPurchaseRequestFilter)
        .subscribe((result) => {
            this.totalCarPurchaseRequest = result.totalCarAttention;
            this.totalInquiryCustomer = result.totalInquiryCustomer;
        });
    }
    //#endregion

    //Show Customer Detail
    //#region Update
    edit(){
        if (this.dupicateSeleted == true) {
            this.showSalesCustomerDetail.show(this.DuplicateCustomer.selectedCustomerId);
        }

        if (this.customerSelected == true) {
            this.showSalesCustomerDetail.show(this.selectedCustomerId);
        }

        if (this.companySelected == true) {
            this.showSalesCustomerDetail.show(this.CompanyCustomer.selectedCustomerId);
        }
    }

    //#endregion

    create(){
        this.showSalesCustomerDetail.show();
    }

    selectIndividualTab(){
        this.customerSelected = true;
        this.dupicateSeleted = false;
        this.companySelected = false;
        this.showCustomerHistory = true;

        this.onGridReady(this.paginationParams);
    }

    selectCompanyTab(){
        this.customerSelected = false;
        this.companySelected = true;
        this.dupicateSeleted = false;
        this.showCustomerHistory = false;

        this.CompanyCustomer.show();
    }

    selectDupilcateTab(){
        this.customerSelected = false;
        this.dupicateSeleted = true;
        this.companySelected = true;
        this.showCustomerHistory = false;

        this.DuplicateCustomer.show();
    }

    //#region  Filter
    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        if (!this.advancedFiltersAreShown) {
            this.clearValueFilter();
        }

        this.CompanyCustomer.onChangeFilterShown();
        this.DuplicateCustomer.onChangeFilterShown();
    }

    clearValueFilter(): void {
        this.filterText = "";
        this.hotnessId = undefined;
        this.customerName = "";
        this.dealerId = undefined;
        this.creationDateTo = moment();
        this.creationDateFrom = moment('2021-01-01');
        this.contactDateFrom = moment('2021-01-01');
        this.contactDateTo = moment();
        this.unitId = undefined;
        this.salePersonId = undefined;
        this.sourceId = undefined;
        this.districtId = -1;
        this.provinceId = -1;
        this.modelId = undefined;
        this.customerTel1 = '';
        this.customerClassId = 1;
        this.freezeTypeId = 3;

        this.CompanyCustomer.clearValueFilter();
        this.DuplicateCustomer.clearValueFilter();
    }

    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    search() {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
        if (this.customerClassId == 3) {
            this.columnDefs[12].hide = false; // cột ReasonLost - Lý do mất khách
            this.columnDefs[13].hide = false; // cột ReasonLost - Lý do mất khách
            this.columnDefs[14].hide = true;
            this.columnDefs[15].hide = true;
            this.params.api.setColumnDefs(this.columnDefs);
        }
        else if (this.customerClassId == 2) {
            this.columnDefs[12].hide = true;
            this.columnDefs[13].hide = true;
            this.columnDefs[14].hide = false; // cột đóng băng
            this.columnDefs[15].hide = false;
            this.params.api.setColumnDefs(this.columnDefs);
        }
        else {
            this.columnDefs[12].hide = true;
            this.columnDefs[13].hide = true;
            this.columnDefs[14].hide = true;
            this.columnDefs[15].hide = true;
            this.params.api.setColumnDefs(this.columnDefs);
        }

        this.onGridReady(this.paginationParams);
        this.selectedCustomerId = undefined;
        if (this.showCustomerHistory == true) this.customerHistory.getData(this.selectedCustomerId)
    }
    //#endregion

    onCellValueChanged(params){
        this.cellParams = [];
        this.cellParams = params;

        if (this.cellParams.column.colId == 'managerComment') {
            this.loading = true;
            this._salesCustomerWebServiceProxy
                .sendCommentToSalesPerson(this.cellParams.data.id, this.cellParams.data.managerComment, this.cellParams.data.salesPersonId)
                .pipe(finalize(() => this.loading = false))
                .subscribe(
                    () => {
                        this.notify.info(this.l('SavedSuccessfully'));
                    }
                );
        }
    }

    getGradeByModel(event){
        this.grades = [];
        this.grades.push({ value: undefined, label: this.l("")});
        this._salesCustomerWebServiceProxy.getGradeForSalesCustomer(event).subscribe((result) => {
            result.forEach(e => this.grades.push({ value: e.id, label: e.marketingCode }));
        });
    }

    contactResult() {
        if(!this.selectedCustomerId) return this.notify.info(this.l('Xin hãy chọn 1 Khách hàng'));

        this.ContactToCustomer.show(this.selectedCustomerId, this.contactBy);
    }
}
