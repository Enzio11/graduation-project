import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUpload } from "primeng/fileupload";
import { GetSalesCustomerForViewDto, SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import { AgDateEditorComponent } from '@app/shared/common/grid/ag-datepicker-editor/ag-date-editor.component';
import { DataFormatService } from "@app/shared/common/services/data-format.service";
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { AgTimeEditorComponent } from '@app/shared/common/grid/ag-timepicker-editor/ag-time-editor.component';
import * as moment from 'moment';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { ValueGetterParams } from '@ag-grid-enterprise/all-modules';
import { ceil } from 'lodash';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'duplicate-customer',
    templateUrl: './duplicate-customer.component.html',
    styleUrls: ['./duplicate-customer.component.less'],
})

export class DuplicateCustomerComponent extends AppComponentBase implements OnInit {
    @ViewChild("ExcelFileUpload", { static: false }) excelFileUpload: FileUpload;

    cellParams: any;
    loading: boolean = false;
    hotnesses: { value: number, label: string }[] = [];
    models: { value: number, label: string }[] = [];
    sources: { value: number, label: string }[] = [];
    salesPersons: { value: number, label: string }[] = [];
    districts: { value: number, label: string }[] = [];
    districtsFilter: { value: number, label: string }[] = [];
    provinces: { value: number, label: string }[] = [];
    units: { value: number, label: string }[] = [];
    grades: { value: number, label: string }[] = [];

    columnDefs: CustomColDef[] = [];

    defaultColDef = {
        fflex: 1,
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };

    sideBar = {
        toolPanels: [
            {
                id: 'columns',
                labelDefault: this.l("Columns"),
                labelKey: 'columns',
                iconKey: 'columns',
                toolPanel: 'agColumnsToolPanel',
                toolPanelParams: {
                    suppressRowGroups: true,
                    suppressValues: true,
                    suppressPivots: true,
                    suppressPivotMode: true,
                    suppressColumnFilter: false
                },
            },
        ],
        defaultToolPanel: '',
    };

    frameworkComponents: FrameworkComponent = {
        agDateEditorComponent: AgDateEditorComponent,
        agTimeEditorComponent: AgTimeEditorComponent,
        agCheckboxRendererComponent: AgCheckboxRendererComponent
    };

    //? Individual Customer
    advancedFiltersAreShown: boolean = true;
    paginationParams: PaginationParamsModel;
    params!: GridParams;
    totalPages: number = 0;
    filterText: string = "";
    hotnessId: number;
    customerName: string = "";
    skipCount: number = 0;
    maxResultCount: number = 20;
    sorting: string = "";
    list: GetSalesCustomerForViewDto[] = [];
    selectedRow: GetSalesCustomerForViewDto = new GetSalesCustomerForViewDto();
    selectedCustomerId: number;
    customerTypeId: number;
    dealerId: number;
    toDate = moment();
    fromDate = moment('2021-01-01');
    unitId: number;
    salePersonId: number;
    sourceId: number;
    districtId: -1;
    provinceId: -1;
    modelId: number;
    gradeId: number;
    customerClassId: number;

    listObject: any;
    objectData = [];
    carAttention: string = '';
    customerTel1: string = "";

    constructor(
        Injector: Injector,
        private dataFormatService: DataFormatService,
        private _localStorage: LocalStorageService,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy
    ) {
        super(Injector);

        this.columnDefs = [
            {
                headerName: this.l('SalesCustomerSalesPerson'),
                headerTooltip: this.l('SalesCustomerSalesPerson'),
                field: 'salesPerson',
                minWidth: 150,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerDepartment'),
                headerTooltip: this.l('SalesCustomerDepartment'),
                field: 'department',
                maxWidth: 120,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('CreationDate'),
                headerTooltip: this.l('CreationDate'),
                field: 'creationTime',
                maxWidth: 140,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerFirstContactDate'),
                headerTooltip: this.l('SalesCustomerFirstContactDate'),
                field: 'firstContactDate',
                maxWidth: 140,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerCode'),
                headerTooltip: this.l('SalesCustomerCode'),
                field: 'customerNo',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerName'),
                headerTooltip: this.l('SalesCustomerName'),
                field: 'name',
                minWidth: 130,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerPhoneNo'),
                headerTooltip: this.l('SalesCustomerPhoneNo'),
                field: 'customerTel1',
                maxWidth: 120,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Province'),
                headerTooltip: this.l('Province'),
                field: 'provinceId',
                maxWidth: 100,
                cellClass: ["text-left"],
                valueGetter: (params: ValueGetterParams) => this.provinces.find(p => p.value === params.data.provinceId)
                    ? (this.provinces.find(p => p.value === params.data.provinceId).label ?? null) : null
            },
            {
                headerName: this.l('District'),
                headerTooltip: this.l('District'),
                field: 'districtId',
                maxWidth: 100,
                cellClass: ["text-left"],
                valueGetter: (params: ValueGetterParams) => this.districts.find(p => p.value === params.data.districtId)
                    ? (this.districts.find(p => p.value === params.data.districtId).label ?? null) : null
            },
            {
                headerName: this.l('Address'),
                headerTooltip: this.l('Address'),
                field: 'address',
                maxWidth: 150,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerLeadSource'),
                headerTooltip: this.l('SalesCustomerLeadSource'),
                field: 'source',
                maxWidth: 120,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerSalesStage'),
                headerTooltip: this.l('SalesCustomerSalesStage'),
                field: 'status',
                maxWidth: 120,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerHotness'),
                headerTooltip: this.l('SalesCustomerHotness'),
                field: 'hotness',
                maxWidth: 120,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerExpectedDelTiming'),
                headerTooltip: this.l('SalesCustomerExpectedDelTiming'),
                field: 'expectedDelTiming',
                maxWidth: 140,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerCarAttention'),
                headerTooltip: this.l('SalesCustomerCarAttention'),
                field: 'carAttention',
                minWidth: 200,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerIsShowRoomVisit'),
                headerTooltip: this.l('SalesCustomerIsShowRoomVisit'),
                field: 'isShowRoomVisit',
                maxWidth: 100,
                cellRenderer: "agCheckboxRendererComponent",
                data: [true, false],
                cellClass: ["text-center"],
                disableCheckbox: true
            },
            {
                headerName: this.l('SalesCustomerIsTestDrive'),
                headerTooltip: this.l('SalesCustomerIsTestDrive'),
                field: 'isTestDrive',
                maxWidth: 100,
                cellRenderer: "agCheckboxRendererComponent",
                data: [true, false],
                cellClass: ["text-center"],
                disableCheckbox: true
            },
            {
                headerName: this.l('SalesCustomerReasonWhyNotTD'),
                headerTooltip: this.l('SalesCustomerReasonWhyNotTD'),
                field: 'reasonWhyNotTestDrive',
                maxWidth: 100,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerTestDriveDate'),
                headerTooltip: this.l('SalesCustomerTestDriveDate'),
                field: 'testDriveDate',
                maxWidth: 140,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerCurrentDate'),
                headerTooltip: this.l('SalesCustomerCurrentDate'),
                field: 'currentDate',
                maxWidth: 140,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerCurrentAction'),
                headerTooltip: this.l('SalesCustomerCurrentAction'),
                field: 'currentAction',
                maxWidth: 100,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerNextDate'),
                headerTooltip: this.l('SalesCustomerNextDate'),
                field: 'nextContact',
                maxWidth: 140,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerNextAction'),
                headerTooltip: this.l('SalesCustomerNextAction'),
                field: 'nextPlan',
                maxWidth: 100,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerManagerNoti'),
                headerTooltip: this.l('SalesCustomerManagerNoti'),
                field: 'managerComment',
                minWidth: 200,
                editable: true,
                cellClass: ['text-left', 'cell-clickable'],
            }
        ];
    }

    ngOnInit(): void {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        
        this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).province.forEach(e => this.provinces.push({ value: e.id, label: e.name }));
        this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).district.forEach(e => this.districts.push({ value: e.id, label: e.name }));
    }

    getDistrictByProvince(event){
        this.districtsFilter = [];
        var listDistrict = this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).province.find(p => p.id == event);
        listDistrict.listDistrict.forEach(e => this.districtsFilter.push({ value: e.id, label: e.name }));
    }

    //! Sales Customer
    show(){
        this.hotnesses.push({ value: null, label: this.l("")});
        this._salesCustomerWebServiceProxy.getHotnessForSalesCustomer().subscribe((result) => {
            result.forEach(e => this.hotnesses.push({ value: e.id, label: e.description }));
        });

        this.models.push({ value: null, label: this.l("")});
        this._salesCustomerWebServiceProxy.getModelForSalesCustomer(1).subscribe((result) => {
            // 1: chỉ lấy mẫu xe thuộc Toyota
            result.forEach(e => this.models.push({ value: e.id, label: e.marketingCode }));
        });

        this._salesCustomerWebServiceProxy.getSourceForSalesCustomer().subscribe((result) => {
            result.forEach(e => this.sources.push({ value: e.id, label: e.description }));
        });

        this._salesCustomerWebServiceProxy.getListSalesPersonName("").subscribe((result) => {
            if (result != null) {
                result.forEach(e => this.salesPersons.push({ value: e.id, label: e.fullName }));
            }
        });

        this.onGridReady(this.paginationParams);

        this.units.push({ value: null, label: this.l("") })
        this._salesCustomerWebServiceProxy.getOrganiztionUnitsByCurrentUser().subscribe((result) => {
            result.forEach(e => this.units.push({ value: e.id, label: e.displayName }));
        });
    };

    callBackGrid(params) {
        this.params = params;
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
    }

    onGridReady(paginationParams) {
        this.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;

        this.getAllDuplicateCustomer();
    };

    changePage(paginationParams){
        this.paginationParams = paginationParams;
        this.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;
        this.getAllDuplicateCustomer();
    }

    onChangeSelection(params) {
        this.selectedCustomerId = undefined;

        this.selectedRow = params.api.getSelectedRows()[0];
        if (this.selectedRow){
            this.selectedCustomerId = this.selectedRow.id;
        }
    }

    getAllDuplicateCustomer(){
        this.listObject = [];
        this.objectData = [];
        this.list = [];
        this.loading = true;
        this._salesCustomerWebServiceProxy.getAllDuplicateCustomer(
            this.filterText,
            this.unitId,
            this.salePersonId,
            this.hotnessId,
            this.sourceId,
            this.districtId,
            this.provinceId,
            this.customerName,
            this.customerTypeId,
            this.dealerId,
            this.gradeId,
            this.modelId,
            this.customerClassId,
            this.fromDate,
            this.toDate,
            null,
            null,
            0,
            this.customerTel1,
            3,
            this.sorting,
            this.skipCount,
            this.maxResultCount
        )
        .pipe(finalize(() => this.loading = false))
        .subscribe(
            (result) => {
                result.items.forEach((element) => {
                    this.carAttention = '';
                    for (let i = 0; i < element.carAttention.length; i++) {
                        this.carAttention = this.carAttention + element.carAttention[i].model + ', ';
                    }
                    this.carAttention = this.carAttention.slice(0, this.carAttention.length - 2);

                    this.listObject = Object.assign(
                        {},
                        element.salesCustomerInfo,
                        element.salesCustomersContactInfo,
                        {carAttention: this.carAttention},
                    );
                    this.objectData.push(this.listObject);
                });
                this.list = this.objectData;

                this.totalPages = ceil(result.totalCount / this.maxResultCount);
                this.paginationParams.totalCount = result.totalCount;
                this.paginationParams.totalPage = this.totalPages;
            }
        );
    }
    //! End Sales Customer

    //! Filter
    onChangeFilterShown(): void {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        if (!this.advancedFiltersAreShown) {
            this.clearValueFilter();
        }
    }

    clearValueFilter(): void {
        this.filterText = "";
        this.hotnessId = undefined;
        this.customerName = "";
        this.dealerId = undefined;
        this.fromDate = moment('2021-01-01');
        this.toDate = moment();
        this.unitId = undefined;
        this.salePersonId = undefined;
        this.sourceId = undefined;
        this.districtId = -1;
        this.provinceId = -1;
        this.modelId = undefined;
        this.customerTel1 = "";
    }

    eventEnter(event): void {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    search() {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);

        this.onGridReady(this.paginationParams);
        this.selectedCustomerId = undefined;
    }

    onCellValueChanged(params){
        this.cellParams = [];
        this.cellParams = params;

        if (this.cellParams.column.colId == 'managerComment') {
            this.loading = true;
            // this._salesCustomerWebServiceProxy
            //     .sendCommentToSalesPerson(this.cellParams.data.id, this.cellParams.data.managerComment, this.cellParams.data.salesPersonId)
            //     .pipe(finalize(() => this.loading = false))
            //     .subscribe(
            //         () => {
            //             this.notify.info(this.l('SavedSuccessfully'));
            //         }
            //     );
        }
    }

    getGradeByModel(event){
        this.grades = [];
        this.grades.push({ value: undefined, label: this.l("")});
        this._salesCustomerWebServiceProxy.getGradeForSalesCustomer(event).subscribe((result) => {
            result.forEach(e => this.grades.push({ value: e.id, label: e.marketingCode }));
        });
    }
}
