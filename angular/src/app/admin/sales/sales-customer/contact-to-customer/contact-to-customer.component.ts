import { Component, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SalesCustomerContactInfoDto, SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import { CustomColDef, FrameworkComponent, GridParams } from '@app/shared/common/models/base.model';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import { AgDateEditorComponent } from '@app/shared/common/grid/ag-datepicker-editor/ag-date-editor.component';

@Component({
    selector: 'contact-to-customer',
    templateUrl: './contact-to-customer.component.html',
    styleUrls: ['./contact-to-customer.component.less'],
})

export class ContactToCustomerModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() openCustomerUpdate: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    loading: boolean = false;

    contactBy: { value: number, label: string }[] = [];

    customerContactResult: SalesCustomerContactInfoDto = new SalesCustomerContactInfoDto();

    frameworkComponents: FrameworkComponent = {
        agDateEditorComponent: AgDateEditorComponent,
    };

    params!: GridParams;
    defaultColDef = {
        minWidth: 80,
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };

    columnDefs: CustomColDef[] = [
        {
            headerName: this.l('MstSleContactBy'),
            headerTooltip: this.l('MstSleContactBy'),
            field: 'contactById',
            valueGetter: params => this.contactBy.find(p => p.value === params.data.contactById)
                ? (this.contactBy.find(p => p.value === params.data.contactById).label ?? null) : null,
            cellClass: ["text-left"]
        },
        {
            headerName: this.l('SalesCustomerCreationTime'),
            headerTooltip: this.l('SalesCustomerCreationTime'),
            field: 'creationTime',
            cellClass: ["text-left"],
            cellEditor: "agDateEditorComponent",
            valueFormatter: (params) => this.dataFormatService.dateFormat(params.value),
        },
        {
            headerName: this.l('SalesCustomerCurrentAction'),
            headerTooltip: this.l('SalesCustomerCurrentAction'),
            field: 'currentAction',
            cellClass: ["text-left"]
        },
        {
            headerName: this.l('SalesCustomerCurrentDate'),
            headerTooltip: this.l('SalesCustomerCurrentDate'),
            field: 'currentDate',
            cellClass: ["text-left"],
            cellEditor: "agDateEditorComponent",
            valueFormatter: (params) => this.dataFormatService.dateFormat(params.value),
        },
        {
            headerName: this.l('SalesCustomerNextAction'),
            headerTooltip: this.l('SalesCustomerNextAction'),
            field: 'nextAction',
            cellClass: ["text-left"],
        },
        {
            headerName: this.l('SalesCustomerNextDate'),
            headerTooltip: this.l('SalesCustomerNextDate'),
            field: 'nextDate',
            cellClass: ["text-left"],
            cellEditor: "agDateEditorComponent",
            valueFormatter: (params) => this.dataFormatService.dateFormat(params.value),
        }
    ];

    rowData: SalesCustomerContactInfoDto[] = [];

    constructor(
        injector: Injector,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
        private dataFormatService: DataFormatService,
    ) {
        super(injector);
    }

    callBackGrid(params) {
        this.params = params;
    }

    show(customerId: number, contactBy: { value: number, label: string }[]): void {
        this.customerContactResult = new SalesCustomerContactInfoDto();
        this.customerContactResult.salesCustomerId = customerId;
        this.customerContactResult.creationTime = moment();
        this.customerContactResult.currentDate = moment();
        this.contactBy = contactBy;

        this._salesCustomerWebServiceProxy.getCustomerContactInfo(customerId)
        .pipe(finalize(() => this.loading = false))
        .subscribe((result) => {
            this.rowData = result
        });

        this.active = true;
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        this.loading = true;

        this._salesCustomerWebServiceProxy.createOrEditContactHistory(this.customerContactResult)
        .pipe(finalize(() => this.loading = false))
        .subscribe(() => {
            this.saving = false;
            this.notify.info(this.l('SavedSuccessfully'));
            this.modalSave.emit(null);
            this.close()
        });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
