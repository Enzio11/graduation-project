import { Component, Injector } from '@angular/core';
import { GridParams } from '@app/shared/common/models/base.model';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import { AppComponentBase } from '@shared/common/app-component-base';
import { SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
	selector: 'sales-customer-history',
	templateUrl: './sales-customer-history.component.html',
	styleUrls: ['./sales-customer-history.component.less']
})
export class SalesCustomerHistoryComponent extends AppComponentBase {
	columnDefs = [
		{
			headerName: this.l('Lịch sử thay đổi'),
			headerTooltip: this.l('Lịch sử thay đổi'),
			children: [
				{
					headerName: this.l('Date'),
					headerTooltip: this.l('Date'),
					field: "hotnessHistoryDate",
					minWidth: 80,
					cellClass: ["text-center"],
					valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
				},
				{
					headerName: this.l('Status'),
					headerTooltip: this.l('Status'),
					field: "hotnessHistoryStatus",
					minWidth: 80,
					cellClass: ["text-center"],
				}
			],
		},
		{
			headerName: this.l('SalesCustomerContactHistory'),
			headerTooltip: this.l('SalesCustomerContactHistory'),
			children: [
				{
					headerName: this.l('Date'),
					headerTooltip: this.l('Date'),
					field: "contactHistoryDate",
					minWidth: 80,
					cellClass: ["text-center"],
					valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
				},
				{
					headerName: this.l('Hình thức liên hệ'),
					headerTooltip: this.l('Hình thức liên hệ'),
					field: "contactHistoryForm",
					minWidth: 80,
					cellClass: ["text-center"],
				},
				{
					headerName: this.l('Nội dung'),
					headerTooltip: this.l('Nội dung'),
					field: "contactHistoryContent",
					minWidth: 80,
					cellClass: ["text-center"],
				}
			],
		},
		{
			headerName: this.l('Quy trình bán hàng'),
			headerTooltip: this.l('Quy trình bán hàng'),
			children: [
				{
					headerName: this.l('Date'),
					headerTooltip: this.l('Date'),
					field: "transactionHistoryDate",
					minWidth: 80,
					cellClass: ["text-center"],
					valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
				},
				{
					headerName: this.l('Quy trình'),
					headerTooltip: this.l('Quy trình'),
					field: "transactionHistoryProcess",
					minWidth: 80,
					cellClass: ["text-center"],
				}
			],
		},
		// {
		// 	headerName: this.l('Lịch sử yêu cầu hỗ trợ'),
		// 	headerTooltip: this.l('Lịch sử yêu cầu hỗ trợ'),
		// 	children: [
		// 		{
		// 			headerName: this.l('Date'),
		// 			headerTooltip: this.l('Date'),
		// 			field: "managerHistoryDate",
		// 			minWidth: 80,
		// 			cellClass: ["text-center"],
		// 			valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
		// 		},
		// 		{
		// 			headerName: this.l('Nội dung yêu cầu'),
		// 			headerTooltip: this.l('Nội dung yêu cầu'),
		// 			field: "managerHistoryContent",
		// 			minWidth: 80,
		// 			cellClass: ["text-center"],
		// 		}
		// 	],
		// }
	];

	defaultColDef = {
		flex: 1,
		floatingFilter: false,
		filter: 'agTextColumnFilter',
		resizable: true,
		sortable: true,
		floatingFilterComponentParams: { suppressFilterButton: true },
		textFormatter: function (r) {
			if (r == null) return null;
			return r.toLowerCase();
		},
	};

	params!: GridParams;
	isLoading: boolean;
	rowData = [];
	loading: boolean = false;

	selectedCustomerId;

	constructor(
		injector: Injector,
		private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
		private dataFormatService: DataFormatService,
	) {
		super(injector);
	}

	getData(salesCustomerId) {
		this.rowData.length = 0;
		this.selectedCustomerId = salesCustomerId
		if (this.selectedCustomerId == undefined) this.params.api.setRowData(this.rowData);
		else {
			this._salesCustomerWebServiceProxy.getSalesCustomerHistory(this.selectedCustomerId)
				.pipe(finalize(() => this.isLoading = false))
				.subscribe(result => {
					const n = Math.max(result[0].length, result[1].length, result[2].length, result[3].length);
					for (var i = 0; i < n; i++) {
						this.rowData.push({
							hotnessHistoryDate: result[0][i]?.date,
							hotnessHistoryStatus: result[0][i]?.status,
							contactHistoryDate: result[1][i]?.date,
							contactHistoryForm: result[1][i]?.status,
							contactHistoryContent: result[1][i]?.content,
							transactionHistoryDate: result[2][i]?.date,
							transactionHistoryProcess: result[2][i]?.status,
							managerHistoryDate: result[3][i]?.date,
							managerHistoryContent: result[3][i]?.status
						})
					}
					this.params.api.setRowData(this.rowData);
				})
		}
	}

	callBackGrid(params) {
		this.params = params;
	}
}
