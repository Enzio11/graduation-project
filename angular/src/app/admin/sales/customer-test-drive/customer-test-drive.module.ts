import { NgModule } from "@angular/core";
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { TABS } from "@app/shared/constants/tab-keys";
import { CustomerTestDriveComponent } from "./customer-test-drive.component";

const tabcode_component_dict = {
    [TABS.CUSTOMER_TEST_DRIVE]: CustomerTestDriveComponent
};

@NgModule({
    imports: [
        CommonDeclareModule,
    ],
    declarations: [
        CustomerTestDriveComponent
    ],
})

export class CustomerTestDriveModule {
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
    }
}