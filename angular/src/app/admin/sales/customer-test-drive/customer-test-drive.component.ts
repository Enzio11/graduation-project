import { Component, Injector } from '@angular/core';
import { AppComponentBase } from "@shared/common/app-component-base";

@Component({
    selector: 'customer-test-drive',
    templateUrl: './customer-test-drive.component.html',
    styleUrls: ['./customer-test-drive.component.less'],
})


export class CustomerTestDriveComponent extends AppComponentBase {

    constructor(
        Injector: Injector,
    ) {
        super(Injector);
    }
}