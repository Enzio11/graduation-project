import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { PaginationParamsModel } from '@app/shared/common/models/base.model';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MstSleModelPresentServiceProxy } from '@shared/service-proxies/service-proxies';
import { ceil } from 'lodash';
import { CreateOrEditMstSleModelPresentModalComponent } from './create-or-edit-mst-sle-model-present/create-or-edit-mst-sle-model-present.component';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { FileDownloadService } from '@shared/utils/file-download.service';

@Component({
  selector: 'app-mst-sle-model-present',
  templateUrl: './mst-sle-model-present.component.html',
  styleUrls: ['./mst-sle-model-present.component.less'],
})
export class MstSleModelPresentComponent extends AppComponentBase implements OnInit {
  @ViewChild('createOrEditMstSleModelPresentModal') createOrEditMstSleModelPresentModal: CreateOrEditMstSleModelPresentModalComponent;
  skipCount;
  filterText = "";
  columnDefs;
  params;
  defaultColDef;
  isLoading: boolean;
  list: any;
  totalPages: number;
  maxResultCount: number;
  rowData: any;
  selectedMstSleModelPresentId;
  selectedMstSleModelPresent;
  sorting = '';
  paginationParams: PaginationParamsModel;
  frameworkComponents;
  constructor(
    injector: Injector,
    private _mstSleModelPresentServiceProxy: MstSleModelPresentServiceProxy,
    private _fileDownloadService: FileDownloadService,
  ) {
    super(injector);
    this.columnDefs = [
      {
        headerName: this.l('MstSleModelPresentMake'),
        headerTooltip: this.l('MstSleModelPresentMake'),
        field: 'make',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresentMarketingCode'),
        headerTooltip: this.l('MstSleModelPresentMarketingCode'),
        field: 'marketingCode',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresentProductionCode'),
        headerTooltip: this.l('MstSleModelPresentProductionCode'),
        field: 'productionCode',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresentEnName'),
        headerTooltip: this.l('MstSleModelPresentEnName'),
        field: 'enName',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresentVnName'),
        headerTooltip: this.l('MstSleModelPresentVnName'),
        field: 'vnName',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresentAbbreviation'),
        headerTooltip: this.l('MstSleModelPresentAbbreviation'),
        field: 'abbreviation',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresentDescription'),
        headerTooltip: this.l('MstSleModelPresentDescription'),
        field: 'description',
        flex: 2,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleModelPresenttatus'),
        headerTooltip: this.l('MstSleModelPresenttatus'),
        field: 'status',
        flex: 2,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l('MstSleModelPresentOrdering'),
        headerTooltip: this.l('MstSleModelPresentOrdering'),
        field: 'ordering',
        cellClass: ["text-center"],
        flex: 2,
      },
      {
        headerName: this.l('MstSleModelPresentOrderingRpt'),
        headerTooltip: this.l('MstSleModelPresentOrderingRpt'),
        field: 'orderingRpt',
        flex: 2,
        cellClass: ["text-center"],
      },

    ];
    this.defaultColDef = {
      flex: 1,
      floatingFilter: false,
      filter: 'agTextColumnFilter',
      resizable: true,
      sortable: true,
      floatingFilterComponentParams: { suppressFilterButton: true },
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
    this.frameworkComponents = {
      agCheckboxRendererComponent: AgCheckboxRendererComponent
    };
  }

  ngOnInit() {
    this.rowData = [];
    this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
  }
  onGridReady(paginationParams) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;

    this.getAllMstSleModelPresent(this.paginationParams).subscribe(
      (result) => {
        this.isLoading = false;
        this.rowData = result.items;
        this.totalPages = ceil(result.totalCount / this.paginationParams.pageSize);
        this.paginationParams.totalCount = result.totalCount;
        this.paginationParams.totalPage = this.totalPages;
        this.params.api.setRowData(this.list);
      },
    );
  }

  onChangeSelection(params) {
    this.selectedMstSleModelPresent = params.api.getSelectedRows()[0];
    if (this.selectedMstSleModelPresent)
      this.selectedMstSleModelPresentId = this.selectedMstSleModelPresent.id;
  }
  getAllMstSleModelPresent(paginationParams?: PaginationParamsModel) {
    return this._mstSleModelPresentServiceProxy
      .getAllMstSleModelPresent(
        this.filterText,
        this.sorting ?? null,
        paginationParams ? paginationParams.skipCount : 0,
        paginationParams ? paginationParams.pageSize : 20
      );
  }
  callBackGrid(params) {
    this.params = params;
    this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
    this.onGridReady(this.paginationParams);
    this.selectedMstSleModelPresentId = undefined;

  }

  changePage(paginationParams: PaginationParamsModel) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
    this.maxResultCount = paginationParams.pageSize;

    this.getAllMstSleModelPresent(this.paginationParams).subscribe(
      (result) => {
        this.list = result.items;
        this.paginationParams.totalPage = ceil(
          result.totalCount / this.maxResultCount
        );
        this.params.api.setRowData(this.list);
      }
    );
  }

  search() {
    this.onGridReady(this.paginationParams);
  }
  deleteMstSleModelPresent() {
    this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
      if (isConfirmed) {
        this._mstSleModelPresentServiceProxy
          .delete(this.selectedMstSleModelPresentId)
          .subscribe(() => {
            this.onGridReady(this.paginationParams);
            this.notify.success(this.l('SuccessfullyDeleted'));
          });
      }
    });
  }
  eventEnter(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }
  createMstSleModelPresent() {
    this.createOrEditMstSleModelPresentModal.show();
  }
  editMstSleModelPresent() {
    this.createOrEditMstSleModelPresentModal.show(this.selectedMstSleModelPresentId);
  }

}