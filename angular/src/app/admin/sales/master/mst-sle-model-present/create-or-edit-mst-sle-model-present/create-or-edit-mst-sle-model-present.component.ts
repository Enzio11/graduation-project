import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditMstSleModelPresentDto, MstSleModelPresentServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'createOrEditMstSleModelPresentModal',
  templateUrl: './create-or-edit-mst-sle-model-present.component.html',
  styleUrls: ['./create-or-edit-mst-sle-model-present.component.less']
})
export class CreateOrEditMstSleModelPresentModalComponent extends AppComponentBase {
  @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  makeId;
  active = false;
  saving = false;
  listMake = [];
  mstSleModelPresent: CreateOrEditMstSleModelPresentDto = new CreateOrEditMstSleModelPresentDto();
  constructor(
    injector: Injector,
    private _mstSleModelPresentServiceProxy: MstSleModelPresentServiceProxy,
  ) {
    super(injector);

  }
  ngOnInit() {
    this._mstSleModelPresentServiceProxy.getListMake().subscribe(result => {
      return this.listMake = result;
    })
  }
  show(modelPresentId?: number): void {
    if (!modelPresentId) {
      this.mstSleModelPresent = new CreateOrEditMstSleModelPresentDto();
      this.mstSleModelPresent.id = modelPresentId;
      this.mstSleModelPresent.makeId = this.makeId;
      this.active = true;
      this.modal.show();
    } else {
      this._mstSleModelPresentServiceProxy.getMstSleModelPresentForEdit(modelPresentId)
        .subscribe((result) => {
          this.mstSleModelPresent = result.mstSleModelPresent;
          this.active = true;
          this.modal.show();
        });
    }
  }

  save(): void {
    this.saving = true;
    this._mstSleModelPresentServiceProxy.createOrEdit(this.mstSleModelPresent).pipe(
      finalize(() => {
        this.saving = false;
      })).subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close();
        this.modalSave.emit(null);
        this.mstSleModelPresent = null;
        this.saving = false;
      });
  }
  close(): void {
    this.active = false;
    this.modal.hide();
  }
}
