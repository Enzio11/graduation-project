import { TABS } from "@app/shared/constants/tab-keys";
import {NgModule} from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleModelPresentComponent } from "./mst-sle-model-present.component";
import { CreateOrEditMstSleModelPresentModalComponent } from "./create-or-edit-mst-sle-model-present/create-or-edit-mst-sle-model-present.component";

const tabcode_component_dict = {
    [TABS.MASTER_SALE_MODEL_PRESENT]: MstSleModelPresentComponent
  };
@NgModule({
    imports: [
      CommonDeclareModule,
    ],
    declarations: [
        MstSleModelPresentComponent,
        CreateOrEditMstSleModelPresentModalComponent
    ],
})

export class  MstSleModelPresentModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}