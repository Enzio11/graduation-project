import { TABS } from "@app/shared/constants/tab-keys";
import {NgModule} from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstGenDistrictComponent } from "./mst-gen-district.component";
import { CreateOrEditMstSleDistrictsModalComponent } from "./create-or-edit-district/create-or-edit-mstSleDistricts-modal.component";

const tabcode_component_dict = {
    [TABS.MASTER_SALE_DISTRICT]: MstGenDistrictComponent
  };
@NgModule({
    imports: [
      CommonDeclareModule,
    ],
    declarations: [
        MstGenDistrictComponent,
        CreateOrEditMstSleDistrictsModalComponent
    ],
})

export class MstGenDistrictModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}