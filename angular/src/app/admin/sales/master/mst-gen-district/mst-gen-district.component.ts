import { Component, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from "@shared/common/app-component-base";
import { MstSleDistrictServiceProxy, GetMstGenProvincesForViewDto, MstGenProvinceServiceProxy, GetMstSleDistrictsForViewDto, MstSleLookupDto } from '@shared/service-proxies/service-proxies';
import * as _ from "lodash";
import { ceil } from 'lodash';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { FileDownloadService } from '@shared/utils/file-download.service';
import { CreateOrEditMstSleDistrictsModalComponent } from './create-or-edit-district/create-or-edit-mstSleDistricts-modal.component';
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';

@Component({
    templateUrl: "./mst-gen-district.component.html",
    styleUrls: ["./mst-gen-district.component.less"],
})

export class MstGenDistrictComponent extends AppComponentBase {
    @ViewChild("createOrEditMstSleDistrictsModal", { static: true }) createOrEditMstSleDistrictsModal: CreateOrEditMstSleDistrictsModalComponent;

    // Ag Grid
    defaultColDef = {
        flex: 1,
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };

    frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent
    };

    // Province Table
    provinceTable: CustomColDef[];
    codeProvinceFilter: string = "";
    nameProvinceFilter: string = "";
    subRegionIdProvinceFilter: number;
    provincePaginationParams: PaginationParamsModel;
    listProvince: GetMstGenProvincesForViewDto[] = [];
    provinceData: any[] = [];
    selectProvinceId: number;
    provinceRowSelected: GetMstGenProvincesForViewDto = new GetMstGenProvincesForViewDto();
    provinceIdSend: number;
    provinceFilterText: string = '';
    provinceCodeFilter: string = '';
    provinceNameFilter: string = '';
    provinceOrderingFilter: number;
    provinceStatusFilter: string = 'Y';
    provinceSubRegionIdFilter: number;
    populationAmountMaxFilter: number;
    populationAmountMinFilter: number
    squareAmountMinFilter: number;
    squareAmountMaxFilter: number;
    skipCount: number = 0;

    // District Table
    districtTable: CustomColDef[];
    districtPaginationParams: PaginationParamsModel;
    advancedFiltersAreShown: boolean = false;
    filterText: string = "";
    codeFilter: string = "";
    nameFilter: string = "";
    descriptionFilter: string = "";
    provinceIdFilter: number;
    statusFilter: string = "Y";
    orderingFilter: number;
    sorting: string = '';
    districtData: any[] = [];
    districtParams!: GridParams;
    selectedDistrictId: number;
    districtRowSelected: GetMstSleDistrictsForViewDto = new GetMstSleDistrictsForViewDto();
    isLoading: boolean = false;
    totalPages: number = 0;
    provinceParams!: GridParams;
    maxResultCount: number = 20;
    regions: MstSleLookupDto[] = []; 

    constructor(
        injector: Injector,
        private _mstSleDistrictsServiceProxy: MstSleDistrictServiceProxy,
        private _mstGenProvincesServiceProxy: MstGenProvinceServiceProxy,
        private _localStorage: LocalStorageService,
        private _fileDownloadService: FileDownloadService
    ) {
        super(injector);
        this.provinceTable = [
            {
                headerName: this.l('Code'),
                headerTooltip: this.l('Code'),
                field: 'code',
                cellClass: ["text-right"],
                flex: 1
            },
            {
                headerName: this.l('Name'),
                headerTooltip: this.l('Name'),
                field: 'name',
                cellClass: ["text-left"],
                flex: 2
            },
            {
                headerName: this.l('Region'),
                headerTooltip: this.l('Region'),
                field: 'subRegionId',
                cellClass: ["text-left"],
                flex: 2,
                valueGetter: params => this.regions.find(p => p.id === params.data.subRegionId)
                    ? (this.regions.find(p => p.id === params.data.subRegionId).name ?? null) : null,
            },
        ];
        this.districtTable = [
            {
                headerName: this.l('Code'),
                headerTooltip: this.l('Code'),
                field: 'code',
                maxWidth: 100,
                flex: 2,
                cellClass: ["text-right"],
            },
            {
                headerName: this.l('Name'),
                headerTooltip: this.l('Name'),
                field: 'name',
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Ordering'),
                headerTooltip: this.l('Ordering'),
                field: 'ordering',
                flex: 1,
                cellClass: ["text-right"],
            },
            {
                headerName: this.l('Description'),
                headerTooltip: this.l('Description'),
                field: 'description',
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Status'),
                headerTooltip: this.l('Status'),
                field: 'status',
                flex: 1,
                cellRenderer: "agCheckboxRendererComponent",
                data: ['Y', 'N'],
                cellClass: ["text-center"],
                disableCheckbox: true,
            }
        ];
    }

    ngOnInit() {
        this.provincePaginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.districtPaginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.regions = this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.regions);

        this.districtData = [];
    }

    getMstGenProvinces(provincePaginationParams?) {
        return this._mstGenProvincesServiceProxy
            .getAll(
                this.provinceFilterText,
                this.provinceCodeFilter,
                this.provinceNameFilter,
                this.provinceOrderingFilter,
                this.provinceStatusFilter,
                this.provinceSubRegionIdFilter,
                this.populationAmountMaxFilter,
                this.populationAmountMinFilter,
                this.squareAmountMaxFilter,
                this.squareAmountMinFilter,
                this.sorting ?? null,
                provincePaginationParams ? provincePaginationParams.skipCount : 0,
                provincePaginationParams ? provincePaginationParams.pageSize : 20
            );
    }

    getDistricts(districtPaginationParams?) {
        return this._mstSleDistrictsServiceProxy
            .getDistrictsByProvinceId(
                this.provinceIdSend,
                this.codeFilter,
                this.nameFilter,
                this.descriptionFilter,
                this.statusFilter,
                this.orderingFilter,
                this.sorting ?? null,
                districtPaginationParams ? districtPaginationParams.skipCount : 0,
                districtPaginationParams ? districtPaginationParams.pageSize : 20
            );
    }

    onGridReady(provincePaginationParams) {
        this.isLoading = true;
        this.provincePaginationParams = provincePaginationParams;
        this.provincePaginationParams.skipCount = (provincePaginationParams.pageNum - 1) * provincePaginationParams.pageSize;

        this.getMstGenProvinces(this.provincePaginationParams).subscribe(
            (result) => {
                this.isLoading = false;
                this.listProvince = result.items;
                this.totalPages = ceil(
                    result.totalCount / this.provincePaginationParams.pageSize
                );
                this.provincePaginationParams.totalCount = result.totalCount;
                this.provincePaginationParams.totalPage = this.totalPages;
            },
            (err) => this.notify.error(err.error),
            () => {
                this.provinceParams.api.setRowData(this.listProvince);
            }
        );
    }

    onChangeProvinceSelection(params) {
        this.provinceRowSelected = params.api.getSelectedRows()[0];
        if (this.provinceRowSelected){
            this.districtPaginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
            this.selectProvinceId = this.provinceRowSelected.id;
            this.getDistrictsByProvinceId();
        }
        this.districtRowSelected = new GetMstSleDistrictsForViewDto();
        this.selectedDistrictId = undefined;
    }

    onChangeDistrictSelection(params) {
        this.districtRowSelected = params.api.getSelectedRows()[0];
        if (this.districtRowSelected)
            this.selectedDistrictId = this.districtRowSelected.id;
    }

    getDistrictsByProvinceId(districtPaginationParams?) {
        this.districtRowSelected = undefined;
        this._mstSleDistrictsServiceProxy
            .getDistrictsByProvinceId(
                this.selectProvinceId,
                this.codeFilter,
                this.nameFilter,
                this.descriptionFilter,
                this.statusFilter,
                this.orderingFilter,
                this.sorting ?? null,
                districtPaginationParams ? districtPaginationParams.skipCount : 0,
                districtPaginationParams ? districtPaginationParams.pageSize : 20
            )
            .subscribe((result) => {
                this.districtData = result.items;
                this.totalPages = ceil(result.totalCount / this.maxResultCount);
                this.districtPaginationParams.totalCount = result.totalCount;
                this.districtPaginationParams.totalPage = this.totalPages;
            });
        this.provinceIdSend = this.selectProvinceId;
    }
    callBackProvinceGrid(params) {
        this.provinceParams = params;
        this.provinceParams.api.paginationSetPageSize(this.provincePaginationParams.pageSize);
        this.onGridReady(this.provincePaginationParams);
        this.selectedDistrictId = undefined;
    }

    changePPage(provincePaginationParams) {
        this.provincePaginationParams = provincePaginationParams;
        this.provincePaginationParams.skipCount =
            (provincePaginationParams.pageNum - 1) * provincePaginationParams.pageSize;
        this.maxResultCount = provincePaginationParams.pageSize;

        this.getMstGenProvinces(this.provincePaginationParams).subscribe(
            (result) => {
                this.listProvince = result.items;
                this.provinceData = result.items;
                this.provincePaginationParams.totalPage = ceil(
                    result.totalCount / this.maxResultCount
                );
            }
        );
    }

    changeDPage(districtPaginationParams) {
        this.districtPaginationParams = districtPaginationParams;
        this.districtPaginationParams.skipCount =
            (districtPaginationParams.pageNum - 1) * districtPaginationParams.pageSize;
        this.maxResultCount = districtPaginationParams.pageSize;

        this.getDistricts(this.districtPaginationParams).subscribe(
            (result) => {
                this.districtData = result.items;
                this.districtPaginationParams.totalPage = ceil(
                    result.totalCount / this.maxResultCount
                );
            }
        );
    }

    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    search() {
        this.provincePaginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.districtData = [];
        this.provinceParams.api.paginationSetPageSize(
            this.provincePaginationParams.pageSize
        );

        this.onGridReady(this.provincePaginationParams);
        this.selectedDistrictId = undefined;
    }

    createMstSleDistricts(): void {
        this.createOrEditMstSleDistrictsModal.show();
    }

    editMstSleDistricts(): void {
        this.createOrEditMstSleDistrictsModal.show(this.selectedDistrictId);
    }

    deleteMstSleDistricts(): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleDistrictsServiceProxy
                    .delete(this.selectedDistrictId)
                    .subscribe(() => {
                        this.getDistrictsByProvinceId(this.districtPaginationParams);
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
                this.selectedDistrictId = undefined;
            }
        });
    }

    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        this.clearValueFilter();
    }

    clearValueFilter() {
        this.provinceCodeFilter = '';
        this.provinceNameFilter = '';
        this.descriptionFilter = '';
        this.statusFilter = 'Y';
        this.orderingFilter = undefined;
    }
}
