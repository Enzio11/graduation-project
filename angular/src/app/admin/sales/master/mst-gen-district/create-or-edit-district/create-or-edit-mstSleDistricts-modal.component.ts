import { Component, ViewChild, Output, EventEmitter, Injector, Input } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap/modal";
import { CreateOrEditMstSleDistrictsDto, MstSleDistrictServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "create-or-edit-districts-modal",
    templateUrl: "./create-or-edit-mstSleDistricts-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleDistricts-modal.component.less"],
})

export class CreateOrEditMstSleDistrictsModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() provinceIdPick: number;

    active: boolean = false;
    saving: boolean = false;
    listStatus: { value: string, label: string }[] = [
        { value: "Y", label: this.l("MstSleEnable")},
        { value: "N", label: this.l("MstSleDisable")}
    ];
    mstSleDistricts: CreateOrEditMstSleDistrictsDto = new CreateOrEditMstSleDistrictsDto();

    constructor(
        injector: Injector,
        private _mstSleDistrictsServiceProxy: MstSleDistrictServiceProxy
    ) {
        super(injector);
    }

    show(mstSleDistrictsId?: number): void {
        if (!mstSleDistrictsId) {
            this.mstSleDistricts = new CreateOrEditMstSleDistrictsDto();
            this.mstSleDistricts.id = mstSleDistrictsId;
            this.mstSleDistricts.provinceId = this.provinceIdPick;

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleDistrictsServiceProxy
                .getMstSleDistrictsForEdit(mstSleDistrictsId)
                .subscribe((result) => {
                    this.mstSleDistricts = result.mstSleDistricts;

                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this._mstSleDistrictsServiceProxy
            .createOrEdit(this.mstSleDistricts)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
