import { TABS } from "@app/shared/constants/tab-keys";
import {NgModule} from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleLookupComponent } from "./mst-sle-lookup.component";
import { CreateOrEditMstSleLookupComponent } from "./create-or-edit-mst-sle-lookup/create-or-edit-mst-sle-lookup.component";

const tabcode_component_dict = {
    [TABS.MASTER_SALE_LOOKUP]: MstSleLookupComponent
  };
@NgModule({
    imports: [
      CommonDeclareModule,
    ],
    declarations: [
        MstSleLookupComponent,
        CreateOrEditMstSleLookupComponent
    ],
})

export class MstSleLookupModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}