import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditMstSleLookupDto, MstSleLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'create-or-edit-mst-sle-lookup',
  templateUrl: './create-or-edit-mst-sle-lookup.component.html',
  styleUrls: ['./create-or-edit-mst-sle-lookup.component.less']
})
export class CreateOrEditMstSleLookupComponent extends AppComponentBase {
  @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  mstSleLookup: CreateOrEditMstSleLookupDto = new CreateOrEditMstSleLookupDto();

  constructor(
    injector: Injector,
    private _mstSleLookupServiceProxy: MstSleLookupServiceProxy
  ) {
    super(injector);
  }

  show(mstSleLookupId?: number): void {
    if (!mstSleLookupId) {
      this.mstSleLookup = new CreateOrEditMstSleLookupDto();
      this.mstSleLookup.id = mstSleLookupId;

      this.active = true;
      this.modal.show();
    } else {
      this._mstSleLookupServiceProxy
        .getMstSleLookupForEdit(mstSleLookupId)
        .subscribe((result) => {
          this.mstSleLookup = result.mstSleLookup;
          this.active = true;
          this.modal.show();
        });
    }
  }

  save(): void {
    this.saving = true;
    this._mstSleLookupServiceProxy
      .createOrEdit(this.mstSleLookup)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }

}
