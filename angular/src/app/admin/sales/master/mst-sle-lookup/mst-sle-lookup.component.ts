import { Component, Injector, ViewChild } from '@angular/core';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { PaginationParamsModel } from '@app/shared/common/models/base.model';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MstSleLookupDto, MstSleLookupServiceProxy } from '@shared/service-proxies/service-proxies';
import { ceil } from 'lodash';
import { CreateOrEditMstSleLookupComponent } from './create-or-edit-mst-sle-lookup/create-or-edit-mst-sle-lookup.component';

@Component({
  selector: 'app-mst-sle-lookup',
  templateUrl: './mst-sle-lookup.component.html',
  styleUrls: ['./mst-sle-lookup.component.less'],
})
export class MstSleLookupComponent extends AppComponentBase {
  @ViewChild('createOrEditMstSleLookupModal') createOrEditMstSleLookupModal: CreateOrEditMstSleLookupComponent;

  advancedFiltersAreShown = false;
  filterText = "";
  codeFilter = "";
  nameFilter = "";
  statusFilter = "Y";
  descriptionFilter = "";
  valueFilter: number;
  sorting = '';
  selectedLookup: MstSleLookupDto = new MstSleLookupDto();
  columnDefs;
  params;
  defaultColDef;
  paginationParams: PaginationParamsModel;
  isLoading: boolean;
  list: any;
  totalPages: number;
  maxResultCount: number;
  rowData: any;
  selectLookup;
  skipCount;
  frameworkComponents: any;

  constructor(
    injector: Injector,
    private _mstSleLookupServiceProxy: MstSleLookupServiceProxy,
  ) {
    super(injector);

    this.columnDefs = [
      {
        headerName: this.l("Code"), headerTooltip: this.l("Code"), field: "code",
        flex: 3, cellClass: ["text-left"],
      },
      {
        headerName: this.l("Name"), headerTooltip: this.l("Name"), field: "name",
        flex: 3, cellClass: ["text-left"],
      },
      {
        headerName: this.l("SaleValue"), headerTooltip: this.l("SaleValue"), field: "value",
        flex: 1, cellClass: ["text-right"],
      },
      {
        headerName: this.l("Description"), headerTooltip: this.l("Description"), field: "description",
        flex: 4, cellClass: ["text-left"],
      },
      {
        headerName: this.l("Ordering"), headerTooltip: this.l("Ordering"), field: "ordering",
        cellClass: ["text-right"], flex: 2
      },
      {
        headerName: this.l('Status'), headerTooltip: this.l('Status'), field: 'status',
        cellRenderer: "agCheckboxRendererComponent", data: ['Y', 'N'],
        cellClass: ["text-center"], disableCheckbox: true, flex: 4
      },
    ];
    this.defaultColDef = {
      flex: 1,
      floatingFilter: false,
      filter: 'agTextColumnFilter',
      resizable: true,
      sortable: true,
      floatingFilterComponentParams: { suppressFilterButton: true },
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
    this.frameworkComponents = { agCheckboxRendererComponent: AgCheckboxRendererComponent };
  }

  ngOnInit() {
    this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
  }

  getMstSleLookups(paginationParams?: PaginationParamsModel) {
    return this._mstSleLookupServiceProxy
      .getAll(
        this.filterText,
        this.codeFilter,
        this.nameFilter,
        this.statusFilter,
        this.descriptionFilter,
        this.valueFilter,
        this.sorting ?? null,
        paginationParams ? paginationParams.skipCount : 0,
        paginationParams ? paginationParams.pageSize : 20
      );
  }

  onGridReady(paginationParams) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;

    this.getMstSleLookups(this.paginationParams).subscribe(
      (result) => {
        this.isLoading = false;
        this.list = result.items;
        this.totalPages = ceil(
          result.totalCount / this.paginationParams.pageSize
        );
        this.paginationParams.totalCount = result.totalCount;
        this.paginationParams.totalPage = this.totalPages;
      },
      (err) => this.notify.error(err.error),
      () => {
        this.params.api.setRowData(this.list);
      }
    );
  }

  onChangeSelection(params) {
    this.selectedLookup = params.api.getSelectedRows()[0];
    if (this.selectedLookup)
      this.selectLookup = this.selectedLookup.id;
  }

  callBackLookupGrid(params) {
    this.params = params;
    this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
    this.onGridReady(this.paginationParams);
    this.selectLookup = undefined;
  }

  changePage(paginationParams: PaginationParamsModel) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount =
      (paginationParams.pageNum - 1) * paginationParams.pageSize;
    this.maxResultCount = paginationParams.pageSize;

    this.getMstSleLookups(this.paginationParams).subscribe(
      (result) => {
        this.list = result.items;
        this.paginationParams.totalPage = ceil(
          result.totalCount / this.maxResultCount
        );
      },
      (err) => this.notify.error(err.error),
      () => {
        this.params.api.setRowData(this.list);
      }
    );
  }

  eventEnter(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }

  search() {
    this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
    this.params.api.paginationSetPageSize(
      this.paginationParams.pageSize
    )
    this.onGridReady(this.paginationParams);
    this.selectLookup = undefined;

  }
  createMstSleLookups(): void {
    this.createOrEditMstSleLookupModal.show();
  }

  editMstSleLookups(): void {
    this.createOrEditMstSleLookupModal.show(this.selectLookup);
  }

  deleteMstSleLookups(): void {
    this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
      if (isConfirmed) {
        this._mstSleLookupServiceProxy
          .delete(this.selectLookup)
          .subscribe(() => {
            this.callBackLookupGrid(this.params);
            this.notify.success(this.l('SuccessfullyDeleted'));
            this.selectLookup = undefined;
          });
      }
    });
  }

  onChangeFilterShown() {
    this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
    this.clearValueFilter();
  }

  clearValueFilter() {
    this.filterText = "";
    this.codeFilter = "";
    this.nameFilter = "";
    this.statusFilter = "";
    this.descriptionFilter = "";
    this.valueFilter = undefined;
  }

}
