import {
    Component,
    ViewChild,
    Injector,
    Output,
    EventEmitter,
    Input,
} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import {
    CreateOrEditMstGenDealersDto,
    DealerLocalStorageDto,
    MstGenDealerServiceProxy,
} from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';

@Component({
    selector: 'createOrEditMstGenDealersModal',
    templateUrl: './create-mst-gen-dealer.component.html',
    styleUrls: ['./create-mst-gen-dealer.component.less'],
})
export class CreateOrEditMstGenDealersModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: false }) modal: ModalDirective;
    @Input() dealerParentId;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    checkNegativeNumber: boolean = false;

    mstGenDealers: CreateOrEditMstGenDealersDto = new CreateOrEditMstGenDealersDto();
    listProvinceAndDistrict = [];
    listProvince = [];
    listDistrict = [];
    listLoyalty = [];
    listDealerType = [];
    listGroupDealer = [];
    listInsurance = [];
    bankAddress: string = '';
    listParent = [];
    listDealer = [];
    listDealerCode: DealerLocalStorageDto[] = [];
    listGroupSwap = [];
    listGroupDispatchFrom = [];
    listGroupArrivalTo = [];
    listDealerTmv = [];
    listLexus = [];
    listStatus = [];

    constructor(
        injector: Injector,
        private _mstGenDealersServiceProxy: MstGenDealerServiceProxy,
        private _localStorage: LocalStorageService
    ) {
        super(injector);
    }
    ngOnInit() {
        this.listDealerType = this._localStorage
            .get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList)
            .lookUp.filter((e) => e.code === LookUpKeys.dealerType).map(e => {return {value: e.value, label: e.name}});

        this.listProvinceAndDistrict = this._localStorage.get(
            'tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList
        ).province;

        this.listProvince = [...this.listProvinceAndDistrict].map(e => {return {value: e.id, label: e.name}});


        this._mstGenDealersServiceProxy.getDealerName().subscribe((result) => {
            result.forEach(e => this.listDealer.push({value: e.id, label: e.abbreviation}))
        })

        this.listDealerTmv = [...this._localStorage.get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.dealerTmv)].map(e => { return {value: e.value, label: e.name}});
        // this.listDealerTmv.forEach(e => {
        //     this.ListDealerTmv2.push({value: e.value, label: e.name})
        // })
        // console.log(this.ListDealerTmv2);

        this.listLexus = [{value: "Y", label: this.l('Yes')}, {value: "N", label: this.l('No')}];
        this.listStatus = [{value: "1", label: this.l('MstSleEnable')}, {value: "0", label: this.l('MstSleDisable')}]
    }
    onChangeProvince() {
        this.listDistrict = [];
        this.listProvinceAndDistrict.forEach(e => {
            if (e.id == this.mstGenDealers.provinceId) {
                this.listDistrict = e.listDistrict.map(e => {return {value: e.provinceId, label: e.name}});

            }
        })
    }
    show(mstGenDealersId?: number): void {
        this.listParent = [];
        if (!mstGenDealersId) {
            this._mstGenDealersServiceProxy
                .getDealerCodeToCheck(mstGenDealersId)
                .subscribe((result) => {
                    this.listDealerCode = result;
                });

            this._mstGenDealersServiceProxy
                .getListDealerParent(mstGenDealersId)
                .subscribe((result) => {
                    //this.listParent = result;
                    result.forEach(e => this.listParent.push({value: e.id, label: e.tenancyName}));
                });

            this.mstGenDealers = new CreateOrEditMstGenDealersDto();
            this.mstGenDealers.id = mstGenDealersId;

            this.active = true;
            this.modal.show();
        } else {
            this._mstGenDealersServiceProxy
                .getDealerCodeToCheck(mstGenDealersId)
                .subscribe((result) => {
                    this.listDealerCode = result;
                });
            this._mstGenDealersServiceProxy
                .getListDealerParent(mstGenDealersId)
                .subscribe((result) => {
                    //this.listParent = result;
                   result.forEach(e => this.listParent.push({value: e.id, label: e.tenancyName}));
                });

            this._mstGenDealersServiceProxy
                .getMstGenDealersForEdit(mstGenDealersId)
                .subscribe((result) => {
                    this.mstGenDealers = result.mstGenDealers;
                    this.active = true;
                    this.modal.show();
                });
        }
    }


    save(): void {
        this.saving = true;

        var checkCode: boolean;
        var checkAbbreviation: boolean;

        this.listDealerCode.forEach((element) => {
            if (this.mstGenDealers.code === element.tenantCode) {
                checkCode = false;
            }
            if (this.mstGenDealers.code === element.tenancyName) {
                checkAbbreviation = false;
            }
        });

        if (checkCode == false) {
            this.saving = false;
            this.message.error(this.l('CodeIsExisted'));
        } else if (checkAbbreviation == false) {
            this.saving = false;
            this.message.error(this.l('MstSleDealerAbbreviationIsExisted'));
        } else {
            console.log(this.mstGenDealers.groupInsuranceId);
            this._mstGenDealersServiceProxy
                .createOrEdit(this.mstGenDealers)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.info(this.l('SavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
        }
    }
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
