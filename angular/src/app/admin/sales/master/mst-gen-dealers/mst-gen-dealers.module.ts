import { NgModule } from '@angular/core';
import { CommonDeclareModule } from '@app/shared/common-declare.module';
import { TABS } from '@app/shared/constants/tab-keys';
import { CreateOrEditMstGenDealersModalComponent } from './create-or-edit-mstGenDealer-modal/create-mst-gen-dealer.component';
import { MstGenDealersComponent } from './mst-gen-dealers.component';

const tabcode_component_dict = {
    [TABS.MASTER_SALE_GEN_DEALER]: MstGenDealersComponent,
};
@NgModule({
    imports: [CommonDeclareModule],
    declarations: [
        MstGenDealersComponent,
        CreateOrEditMstGenDealersModalComponent,
    ],
})
export class MstGenDealersModule {
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
    }
}
