import { Component, EventEmitter, Input, Output, Injector, ViewChild } from '@angular/core';
import { MstGenDealerServiceProxy, GetMstGenDealersForViewDto, GetMstGenDealerForDropdownDto, MstSleLookupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as _ from 'lodash';
import { CustomColDef, PaginationParamsModel, GridParams, FrameworkComponent } from '@app/shared/common/models/base.model';
import { ceil } from 'lodash';
import { EntityTypeHistoryModalComponent } from '@app/shared/common/entityHistory/entity-type-history-modal.component';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { CreateOrEditMstGenDealersModalComponent } from './create-or-edit-mstGenDealer-modal/create-mst-gen-dealer.component';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';

@Component({
    templateUrl: './mst-gen-dealers.component.html',
    styleUrls: ['./mst-gen-dealers.component.less'],
})

export class MstGenDealersComponent extends AppComponentBase {
    @ViewChild('entityTypeHistoryModal', { static: true })
    entityTypeHistoryModal: EntityTypeHistoryModalComponent;
    @ViewChild('createOrEditMstGenDealersModal', { static: true })
    createOrEditMstGenDealersModal: CreateOrEditMstGenDealersModalComponent;
    @Input() suppressRowClickSelection: boolean;
    @Output() keyup = new EventEmitter();

    //dealerAddress
    areShownAddress: boolean = true;
    insuranceDealerShow: boolean = false;
    //ContactInfo
    areShownContact: boolean = false;
    advancedFiltersAreShown: boolean = false;
    filterText: string = '';
    codeFilter: string = '';
    accountNoFilter: string = '';
    bankFilter: string = '';
    bankAddressFilter: string = '';
    vnNameFilter: string = '';
    enNameFilter: string = '';
    addressFilter: string = '';
    abbreviationFilter: string = '';
    contactPersonFilter: string = '';
    phoneFilter: string = '';
    statusFilter: string = '';
    faxFilter: string = '';
    descriptionFilter: string = '';
    isSpecialFilter: string = '';
    maxOrderingFilter: number;
    isDealerFilter: number;
    maxOrderingFilterEmpty: number;
    minOrderingFilter: number;
    minOrderingFilterEmpty: number;
    taxCodeFilter: string = '';
    isSumDealerFilter: string = '';
    biServerFilter: string = '';
    tfsAmountFilter: number;
    partLeadtimeFilter: number;
    ipAddressFilter: string = '';
    isLexusFilter: string = '';
    isSellLexusPartFilter: string = '';
    isDlrSalesFilter: string = '';
    receivingAddressFilter: string = '';
    dlrFooterFilter: string = '';
    dealerIdFilter: number;
    dealerTypeIdFilter: number;
    dealerGroupIdFilter: number;
    provinceIdFilter: number;
    isPrintFilter: string = '';
    passwordSearchVinFilter: string = '';
    maxPortRegionFilter: number;
    maxPortRegionFilterEmpty: number;
    minPortRegionFilter: number;
    minPortRegionFilterEmpty: number;
    selectAddress: number;
    selectContact: number;
    selectDealer: number;
    listInsurance = [];
    defaultColDef = {
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        editable: false,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
        flex: 1,
    };
    columnDefsDealer: CustomColDef[];
    columnDefsAddress: CustomColDef[];
    columnDefsContact: CustomColDef[];
    columnDefsInsurance: CustomColDef[];
    dealerPaginationParams: PaginationParamsModel;
    addressPaginationParams: PaginationParamsModel;
    contactPaginationParams: PaginationParamsModel;
    insurancePaginationParams: PaginationParamsModel;
    dealerParams!: GridParams;
    sorting: string = '';
    skipCount: number = 0;
    isLoading: boolean = false;
    list: GetMstGenDealersForViewDto[] = [];
    totalPages: number = 0;
    maxResultCount: number = 10;
    selectedDealer: GetMstGenDealersForViewDto = new GetMstGenDealersForViewDto();
    dealerIdPicked: number = undefined;
    listDealer: GetMstGenDealerForDropdownDto[] = [];
    listProvince = [];
    listDistrict = [];
    //DealerInsurance
    frameworkComponents: FrameworkComponent;
    dealerParentId: number = undefined;
    selectInsurance: number = undefined;
    listDealerType: MstSleLookupDto[] = [];
    listDealerTmv: MstSleLookupDto[] = [];

    rowDataDealer = [];
    constructor(
        injector: Injector,
        private _mstGenDealersServiceProxy: MstGenDealerServiceProxy,
        private _localStorage: LocalStorageService,
    ) {
        super(injector);

        (this.columnDefsInsurance = [
            {
                headerName: this.l('MstSleDealerInsuranceDealer'),
                field: 'dealerAbbreviation',
                flex: 1,
                cellClass: ['text-center'],
            },
            {
                headerName: this.l('Ordering'),
                field: 'ordering',
                flex: 2,
                cellClass: ['text-center'],
            },
            {
                headerName: this.l('MstSleDealerInsurance'),
                field: 'insuranceCode',
                flex: 1,
                cellClass: ['text-center'],
            },
            {
                headerName: this.l('MstSleDealerInsuranceRemark'),
                field: 'remark',
                flex: 2,
                cellClass: ['text-center'],
            },
        ]),
            (this.columnDefsDealer = [
                {
                    headerName: this.l('STT'),
                    field: 'stt',
                    valueGetter: 'node.rowIndex + 1',
                    minWidth: 70,
                    cellClass: ['text-center'],
                },
                {
                    headerName: this.l('MstGenDealersIsLexus'),
                    headerTooltip: this.l('MstGenDealersIsLexus'),
                    field: 'islexus',
                    minWidth: 80,
                    cellRenderer: 'agCheckboxRendererComponent',
                    data: ['Y', 'N'],
                    cellClass: ['text-center'],
                    disableCheckbox: true,
                },
                {
                    headerName: this.l('MstGenDealersIsDealer'),
                    headerTooltip: this.l('MstGenDealersIsDealer'),
                    field: 'isDealer',
                    minWidth: 125,
                    cellClass: ['text-left'],
                    valueGetter: params => this.listDealerTmv.find(p => p.value == params.data.isDealer) ? (this.listDealerTmv.find(p => p.value == params.data.isDealer).name ?? null) : null,
                },
                {
                    headerName: this.l('MstGenDealersCode'),
                    headerTooltip: this.l('MstGenDealersCode'),
                    field: 'code',
                    cellClass: ['text-left'],
                    minWidth: 100,
                },
                {
                    headerName: this.l('MstGenDealersDealerParent'),
                    headerTooltip: this.l('MstGenDealersDealerParent'),
                    field: 'dealerParentsId',
                    minWidth: 125,
                    cellClass: ['text-left'],
                    valueGetter: (params) =>
                        this.listDealer.find(
                            (p) =>
                                p.id == params.data.dealerParentsId &&
                                params.data
                        )
                            ? this.listDealer.find(
                                (p) => p.id == params.data.dealerParentsId
                            ).abbreviation ?? null
                            : '',
                },
                {
                    headerName: this.l('MstGenDealersGroupManager'),
                    headerTooltip: this.l('MstGenDealersGroupManager'),
                    field: 'groupManagerId',
                    minWidth: 125,
                    cellClass: ['text-left'],
                    valueGetter: (params) =>
                        this.listDealer.find(
                            (p) =>
                                p.id == params.data.groupManagerId &&
                                params.data
                        )
                            ? this.listDealer.find(
                                (p) => p.id == params.data.groupManagerId
                            ).abbreviation ?? null
                            : '',
                },
                {
                    headerName: this.l('MstGenDealersAbbreviation'),
                    headerTooltip: this.l('MstGenDealersAbbreviation'),
                    field: 'abbreviation',
                    cellClass: ['text-left'],
                    minWidth: 125,
                },
                {
                    headerName: this.l('MstGenDealersDealerType'),
                    headerTooltip: this.l('MstGenDealersDealerType'),
                    field: 'dealerTypeId',
                    cellClass: ['text-left'],
                    minWidth: 100,
                    valueGetter: (params) =>
                        this.listDealerType.find(
                            (p) =>
                                p.value == params.data.dealerTypeId &&
                                params.data
                        )
                            ? this.listDealerType.find(
                                (p) => p.value == params.data.dealerTypeId
                            ).name ?? null
                            : '',
                },
                {
                    headerName: this.l('MstGenDealersVnName'),
                    headerTooltip: this.l('MstGenDealersVnName'),
                    field: 'vnName',
                    cellClass: ['text-left'],
                    minWidth: 255,
                },
                {
                    headerName: this.l('MstGenDealersEnName'),
                    headerTooltip: this.l('MstGenDealersEnName'),
                    field: 'enName',
                    cellClass: ['text-left'],
                    minWidth: 255,
                },
                {
                    headerName: this.l('MstGenDealersTaxCode'),
                    headerTooltip: this.l('MstGenDealersTaxCode'),
                    field: 'taxCode',
                    cellClass: ['text-left'],
                    minWidth: 150,
                },
                {
                    headerName: this.l('MstGenDealersAccountNo'),
                    headerTooltip: this.l('MstGenDealersAccountNo'),
                    field: 'accountNo',
                    cellClass: ['text-left'],
                    minWidth: 125,
                },
                {
                    headerName: this.l('MstGenDealersOrdering'),
                    headerTooltip: this.l('MstGenDealersOrdering'),
                    field: 'ordering',
                    cellClass: ['text-center'],
                    minWidth: 100,
                },
                {
                    headerName: this.l('MstGenDealersContactPerson'),
                    headerTooltip: this.l('MstGenDealersContactPerson'),
                    field: 'contactPerson',
                    cellClass: ['text-left'],
                    minWidth: 175,
                },
                {
                    headerName: this.l('MstGenDealersContractRole'),
                    headerTooltip: this.l('MstGenDealersContractRole'),
                    field: 'contractRole',
                    cellClass: ['text-left'],
                    minWidth: 175,
                },
                {
                    headerName: this.l('MstGenDealersPhone'),
                    headerTooltip: this.l('MstGenDealersPhone'),
                    field: 'phone',
                    cellClass: ['text-left'],
                    minWidth: 125,
                },
                {
                    headerName: this.l('MstGenDealersFax'),
                    headerTooltip: this.l('MstGenDealersFax'),
                    field: 'fax',
                    cellClass: ['text-left'],
                    minWidth: 125,
                },
                {
                    headerName: this.l('MstGenDealersAddress'),
                    headerTooltip: this.l('MstGenDealersAddress'),
                    field: 'address',
                    cellClass: ['text-left'],
                    minWidth: 255,
                },
                {
                    headerName: this.l('MstGenDealersProvince'),
                    headerTooltip: this.l('MstGenDealersProvince'),
                    field: 'provinceId',
                    valueGetter: (params) =>
                        this.listProvince.find(
                            (p) => p.id == params.data.provinceId && params.data
                        )
                            ? this.listProvince.find(
                                (p) => p.id == params.data.provinceId
                            ).name ?? null
                            : '',
                    cellClass: ['text-left'],
                    minWidth: 150,
                },
                {
                    headerName: this.l('District'),
                    headerTooltip: this.l('District'),
                    field: 'districtId',
                    valueGetter: (params) =>
                        this.listDistrict.find(
                            (p) => p.id == params.data.districtId && params.data
                        )
                            ? this.listDistrict.find(
                                (p) => p.id == params.data.districtId
                            ).name ?? null
                            : '',
                    cellClass: ['text-left'],
                    minWidth: 150,
                },
                {
                    headerName: this.l('MstGenDealersBank'),
                    headerTooltip: this.l('MstGenDealersBank'),
                    field: 'bank',
                    cellClass: ['text-left'],
                    minWidth: 175,
                },
                {
                    headerName: this.l('MstGenDealersBankAddress'),
                    headerTooltip: this.l('MstGenDealersBankAddress'),
                    field: 'bankAddress',
                    cellClass: ['text-left'],
                    minWidth: 175,
                },
                {
                    headerName: this.l('MstGenDealersDescription'),
                    headerTooltip: this.l('MstGenDealersDescription'),
                    field: 'description',
                    cellClass: ['text-left'],
                    minWidth: 100,
                },
                {
                    headerName: this.l('MstGenDealersAuthorizationInfo'),
                    headerTooltip: this.l('MstGenDealersAuthorizationInfo'),
                    field: 'authorizationInfo',
                    cellClass: ['text-left'],
                    minWidth: 100,
                },
                {
                    headerName: this.l('SalesLong'),
                    headerTooltip: this.l('SalesLong'),
                    field: 'long',
                    minWidth: 100,
                    cellClass: ['text-right'],
                },
                {
                    headerName: this.l('SalesLat'),
                    headerTooltip: this.l('SalesLat'),
                    field: 'lat',
                    minWidth: 100,
                    cellClass: ['text-right'],
                },               
                {
                    headerName: this.l('Status'),
                    headerTooltip: this.l('Status'),
                    field: 'status',
                    minWidth: 150,
                    cellRenderer: 'agCheckboxRendererComponent',
                    data: ['1', '2'],
                    cellClass: ['text-center'],
                    disableCheckbox: true,
                },
                {
                    headerName: this.l('MstSleGenDealerOperatingTime'),
                    headerTooltip: this.l('MstSleGenDealerOperatingTime'),
                    field: 'operatingTime',
                    minWidth: 200,
                    cellClass: ['text-right'],
                },
                {
                    headerName: this.l('MstSleGenDealerLinkWebsite'),
                    headerTooltip: this.l('MstSleGenDealerLinkWebsite'),
                    field: 'linkWebsite',
                    minWidth: 200,
                    cellClass: ['text-right'],
                },
                {
                    headerName: this.l('MstSleGenDealerMapUrl'),
                    headerTooltip: this.l('MstSleGenDealerMapUrl'),
                    field: 'mapUrl',
                    minWidth: 200,
                    cellClass: ['text-right'],
                },
                {
                    headerName: this.l('IsUsingApp'),
                    headerTooltip: this.l('IsUsingApp'),
                    field: 'isUsingApp',
                    minWidth: 150,
                    cellRenderer: 'agCheckboxRendererComponent',
                    data: [true, false],
                    cellClass: ['text-center'],
                    disableCheckbox: true,
                },
                {
                    headerName: this.l('MstSleGenDealerGroupInsurance'),
                    headerTooltip: this.l('MstSleGenDealerGroupInsurance'),
                    field: 'groupInsuranceId',
                    minWidth: 180,
                    cellClass: ['text-left'],
                    valueGetter: params => 
                    this.listInsurance.find(p => p.value == params.data.groupInsuranceId) 
                    ? (this.listInsurance.find(p => p.value == params.data.groupInsuranceId).label ?? null) : null,
                },
            ]);
        this.columnDefsAddress = [
            {
                headerName: this.l('Abbreviation'),
                headerTooltip: this.l('Abbreviation'),
                valueGetter: (params) =>
                    this.listDealer.find(
                        (p) => p.id == params.data.dealerId && params.data
                    )
                        ? this.listDealer.find(
                            (p) => p.id == params.data.dealerId
                        ).abbreviation ?? null
                        : '',
                cellClass: ['text-left'],
                flex: 2,
            },
            {
                headerName: this.l('MstSleOutletDealer'),
                headerTooltip: this.l('MstSleOutletDealer'),
                valueGetter: (params) =>
                    this.listDealer.find(
                        (p) => p.id == params.data.otherDealerId && params.data
                    )
                        ? this.listDealer.find(
                            (p) => p.id == params.data.otherDealerId
                        ).abbreviation ?? null
                        : '',
                cellClass: ['text-left'],
                flex: 2,
            },
            {
                headerName: this.l('Ordering'),
                field: 'ordering',
                flex: 2,
                cellClass: ['text-center'],
            },
            {
                headerName: this.l('Priority'),
                headerTooltip: this.l('Priority'),
                field: 'priority',
                flex: 1,
                cellClass: ['text-center'],
            },
            {
                headerName: this.l('Address'),
                headerTooltip: this.l('Address'),
                field: 'address',
                flex: 2,
                cellClass: ['text-left'],
            },
            {
                headerName: this.l('Status'),
                headerTooltip: this.l('Status'),
                field: 'status',
                flex: 1,
                cellRenderer: 'agCheckboxRendererComponent',
                data: ['Y', 'N'],
                cellClass: ['text-center'],
                disableCheckbox: true,
            },
        ];
        this.columnDefsContact = [
            {
                headerName: this.l('Name'),
                headerTooltip: this.l('Name'),
                field: 'name',
                cellClass: ['text-left'],
                flex: 2,
            },
            {
                headerName: this.l('Phone'),
                headerTooltip: this.l('Phone'),
                field: 'phone',
                cellClass: ['text-left'],
                flex: 2,
            },
            {
                headerName: this.l('Email'),
                headerTooltip: this.l('Email'),
                field: 'email',
                cellClass: ['text-left'],
                flex: 2,
            },
            {
                headerName: this.l('MstGenDealersEmailGroup'),
                headerTooltip: this.l('MstGenDealersEmailGroup'),
                field: 'contactGroupName',
                cellClass: ['text-left'],
                flex: 2,
            },
            {
                headerName: this.l('Description'),
                headerTooltip: this.l('Description'),
                field: 'description',
                flex: 2,
                cellClass: ['text-left'],
            },
          
            {
                headerName: this.l('Status'),
                headerTooltip: this.l('Status'),
                field: 'status',
                flex: 1,
                cellRenderer: 'agCheckboxRendererComponent',
                data: ['Y', 'N'],
                cellClass: ['text-center'],
                disableCheckbox: true,
            },
        ];

        this.frameworkComponents = {
            agCheckboxRendererComponent: AgCheckboxRendererComponent,
        };
    }

    ngOnInit() {
        this.dealerPaginationParams = {
            pageNum: 1,
            pageSize: 10,
            totalCount: 0,
        };
        this.addressPaginationParams = {
            pageNum: 1,
            pageSize: 10,
            totalCount: 0,
        };
        this.contactPaginationParams = {
            pageNum: 1,
            pageSize: 10,
            totalCount: 0,
        };
        this.insurancePaginationParams = {
            pageNum: 1,
            pageSize: 10,
            totalCount: 0,
        };

        this.listDistrict = this._localStorage.get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList).district;
        this.listDealerTmv = this._localStorage.get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.dealerTmv);
    }

    callBackGridDealer(params) {
        this._mstGenDealersServiceProxy.getDealerName().subscribe((re) => {
            this.listDealer = re;
        });
        this.listProvince = this._localStorage.get(
            'tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList
        ).province;

        this.dealerParams = params;
        this.dealerParams.api.paginationSetPageSize(
            this.dealerPaginationParams.pageSize
        );
        // this._mstSleDealerGroupsServiceProxy
        //     .getGroupDealerName()
        //     .subscribe((result) => {
        //         this.listGroupDealer = result;
        //     });
        this.listDealerType = this._localStorage
            .get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList)
            .lookUp.filter((e) => e.code === LookUpKeys.dealerType);
        this.onGridReadyDealer(this.dealerPaginationParams);
    }
    eventEnter(event) {
        if (event.keyCode === 13) {
            this.searchDealer();
        }
    }
    searchDealer() {
        this.dealerPaginationParams = {
            pageNum: 1,
            pageSize: 10,
            totalCount: 0,
        };
        this.dealerParams.api.paginationSetPageSize(
            this.dealerPaginationParams.pageSize
        );
        this.onGridReadyDealer(this.dealerPaginationParams);
    }

    onGridReadyDealer(dealerPaginationParams) {
        this.dealerPaginationParams = dealerPaginationParams;
        this.dealerPaginationParams.skipCount =
            (dealerPaginationParams.pageNum - 1) *
            dealerPaginationParams.pageSize;
        this.getMstGenDealers(this.dealerPaginationParams).subscribe(
            (result) => {
                this.isLoading = false;
                this.list = result.items;
                this.totalPages = ceil(
                    result.totalCount / this.dealerPaginationParams.pageSize
                );
                this.dealerPaginationParams.totalCount = result.totalCount;
                this.dealerPaginationParams.totalPage = this.totalPages;
            },
            (err) => this.notify.error(err.error),
            () => {
                this.dealerParams.api.setRowData(this.list);
            }
        );
    }

    changePageDealer(params) {
        this.dealerPaginationParams = params;
        this.dealerPaginationParams.skipCount =
            (params.pageNum - 1) * params.pageSize;
        this.maxResultCount = params.pageSize;
        this.getMstGenDealers(this.dealerPaginationParams).subscribe(
            (result) => {
                this.list = result.items;
                this.rowDataDealer = result.items;

                this.dealerPaginationParams.totalPage = ceil(
                    result.totalCount / this.maxResultCount
                );
            }
        );
    }

    getMstGenDealers(dealerPaginationParams?) {
        return this._mstGenDealersServiceProxy.getAll(
            this.filterText,
            this.codeFilter,
            this.accountNoFilter,
            this.bankFilter,
            this.bankAddressFilter,
            this.vnNameFilter,
            this.enNameFilter,
            this.addressFilter,
            this.abbreviationFilter,
            this.contactPersonFilter,
            this.phoneFilter,
            this.statusFilter,
            this.faxFilter,
            this.descriptionFilter,
            this.isSpecialFilter,
            this.maxOrderingFilter == null
                ? this.maxOrderingFilterEmpty
                : this.maxOrderingFilter,
            this.minOrderingFilter == null
                ? this.minOrderingFilterEmpty
                : this.minOrderingFilter,
            this.taxCodeFilter,
            this.isSumDealerFilter,
            this.biServerFilter,
            this.tfsAmountFilter,
            this.partLeadtimeFilter,
            this.ipAddressFilter,
            this.isLexusFilter,
            this.isSellLexusPartFilter,
            this.isDlrSalesFilter,
            this.receivingAddressFilter,
            this.dlrFooterFilter,
            this.isPrintFilter,
            this.passwordSearchVinFilter,
            this.maxPortRegionFilter == null
                ? this.maxPortRegionFilterEmpty
                : this.maxPortRegionFilter,
            this.minPortRegionFilter == null
                ? this.minPortRegionFilterEmpty
                : this.minPortRegionFilter,
            this.isDealerFilter,
            this.sorting ?? null,
            dealerPaginationParams ? dealerPaginationParams.skipCount : 0,
            dealerPaginationParams ? dealerPaginationParams.pageSize : 10
        );
    }

    createMstGenDealers(): void {
        this.createOrEditMstGenDealersModal.show();
    }

    editMstGenDealers(): void {
        this.createOrEditMstGenDealersModal.show(this.selectDealer);
    }

    deleteMstGenDealers(): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._mstGenDealersServiceProxy
                    .delete(this.selectDealer)
                    .subscribe(() => {
                        this.notify.success(this.l('SuccessfullyDeleted'));
                        this.selectDealer = undefined;
                    });
            }
        });
    }

    onChangeSelectionDealer(params) {
        this.selectedDealer = params.api.getSelectedRows()[0];
        if (this.selectedDealer) {
            this.selectDealer = this.selectedDealer.id;
            this.dealerIdPicked = this.selectDealer;
            this.dealerParentId = this.selectedDealer.dealerParentsId;
        }
    }

    onKeyUpDownDealer($event: KeyboardEvent) {
        this.keyup.emit($event);
        const KEY_UP = 'ArrowUp';
        const KEY_DOWN = 'ArrowDown';
        // select dòng nếu bấm nút lên hoặc xuống
        const focusCell = this.dealerParams.api.getFocusedCell();
        if ($event.code === KEY_DOWN) {
            this.dealerParams.api.forEachNode((node) => {
                if (focusCell.rowIndex === node.rowIndex) {
                    node.setSelected(true);
                }
            });
        }
        if ($event.code === KEY_UP) {
            this.dealerParams.api.forEachNode((node) => {
                if (focusCell.rowIndex === node.rowIndex) {
                    node.setSelected(true);
                }
            });
        }
    }

    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        this.clearValueFilter();
    }

    clearValueFilter() {
        this.filterText = '';
        this.codeFilter = '';
        this.accountNoFilter = '';
        this.bankFilter = '';
        this.bankAddressFilter = '';
        this.vnNameFilter = '';
        this.enNameFilter = '';
        this.addressFilter = '';
        this.abbreviationFilter = '';
        this.contactPersonFilter = '';
        this.phoneFilter = '';
        this.statusFilter = '';
        this.faxFilter = '';
        this.descriptionFilter = '';
        this.isSpecialFilter = '';
        this.maxOrderingFilter = undefined;
        this.minOrderingFilter = undefined;
        this.taxCodeFilter = '';
        this.isSumDealerFilter = '';
        this.biServerFilter = '';
        this.tfsAmountFilter = undefined;
        this.partLeadtimeFilter = undefined;
        this.ipAddressFilter = '';
        this.isLexusFilter = '';
        this.isSellLexusPartFilter = '';
        this.isDlrSalesFilter = '';
        this.receivingAddressFilter = '';
        this.dlrFooterFilter = '';
        this.isPrintFilter = '';
        this.passwordSearchVinFilter = '';
        this.maxPortRegionFilter = undefined;
        this.minPortRegionFilter = undefined;
    }    
}
