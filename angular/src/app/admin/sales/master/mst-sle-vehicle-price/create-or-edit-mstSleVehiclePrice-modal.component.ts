import { Component, ViewChild, Output, EventEmitter, Injector, Input } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap/modal";
import { CreateOrEditMstSleVehiclePriceDto, MstSleVehiclePriceServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "create-or-edit-vehicle-price-modal",
    templateUrl: "./create-or-edit-mstSleVehiclePrice-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleVehiclePrice-modal.component.less"],
})
export class CreateOrEditMstSleVehiclePriceModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() vehiclePriceSelected;
    active: boolean = false;
    saving: boolean = false;

    mstSleVehiclePrice: CreateOrEditMstSleVehiclePriceDto = new CreateOrEditMstSleVehiclePriceDto();
    listGrade: any[] = [];

    listGradePro: any[] = [];
    listGradeProByGrade: any[] = [];
    listModel: any[] = [];
    listGradeByModel: any[] = [];
    listColor: any[] = [];
    listColorGP: any[] = [];
    listIColorGP: any[] = [];
    listColorByGradePro: any[] = [];
    listIColorByGradePro: any[] = [];
    listTransmissions: any[] = [];
    listEngineType: any[] = [];
    listFuleType: any[] = [];
    modelStatusFilter: string = "Y";
    listStatus: { value: string, label: string }[] = [
        { value: undefined, label: this.l("")},
        { value: "Y", label: this.l("MstSleEnable")},
        { value: "N", label: this.l("MstSleDisable")}
    ];
    listCBU: { value: number, label: string }[] = [
        { value: undefined, label: this.l("")},
        { value: 0, label: this.l("CKD")},
        { value: 1, label: this.l("CBU")},
        { value: 2, label: this.l("Lexus")}
    ];
    constructor(
        injector: Injector,
        private _mstSleVehiclePricesrviceProxy: MstSleVehiclePriceServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this.getApi();
        this.getAllColorGP();
        this.getAllIColorGP();
    }

    show(id?: number): void {    
        if (!id) {
            this.getApi();
            this.mstSleVehiclePrice = new CreateOrEditMstSleVehiclePriceDto();
            this.mstSleVehiclePrice.id = id;
            this.active = true;
            this.modal.show();
        } else {
           
            this._mstSleVehiclePricesrviceProxy.getMstSleVehiclePriceForEdit(id)
                .subscribe((result) => {
                    this.mstSleVehiclePrice = result.mstSleVehiclePrice;
                    // console.log(this.mstSleVehiclePrice)
                    this.getGradeByModel( this.mstSleVehiclePrice.modelId);
                    this.getGradeProByGrade( this.mstSleVehiclePrice.gradeId);
                    this.getColorByGradePro( this.mstSleVehiclePrice.gradeProductionId);
                    this.active = true;
                    this.modal.show();
                });
        }
    }
    save(): void {
        this.saving = true;
        this._mstSleVehiclePricesrviceProxy
            .createOrEdit(this.mstSleVehiclePrice)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    private getApi() {
        this._mstSleVehiclePricesrviceProxy.getListGrade().subscribe((result) => {
           this.listGrade = result;
            result.forEach(e=> this.listGradeByModel.push({value: e.id, label: e.marketingCode}))
        });
        this._mstSleVehiclePricesrviceProxy.getListColor().subscribe((result) => {
            this.listColor =result;
            result.forEach(e=> this.listColorByGradePro.push({value: e.id, label: e.code}));
            result.forEach(e=> this.listIColorByGradePro.push({value: e.id, label: e.code}));
        });
        this._mstSleVehiclePricesrviceProxy.getListGradeProduces().subscribe((result) => {
            this.listGradePro = result;
            result.forEach(e=> this.listGradeProByGrade.push({value: e.id, label: e.productionCode}));
        });
        // this._mstSleTestDriveVehicleServiceProxy.getAllModel().subscribe((result) => {
        //     result.forEach(e=> this.listModel.push({value: e.id, label: e.marketingCode}));
        // })
    }

    private getAllColorGP() {
        this._mstSleVehiclePricesrviceProxy.getAllColorGP().subscribe((result) => {
           this.listColorGP = result;
        })
    }
    
    private getAllIColorGP() {
        this._mstSleVehiclePricesrviceProxy.getAllIColorGP().subscribe((result) => {
        this.listIColorGP =result;
        })
    }

    getGradeProByGrade(gradeId: number) {
        this.listGradeProByGrade =  [{ key: null, value: "" }];
        this.listColorByGradePro =  [{ key: null, value: "" }];
        this.listIColorByGradePro = [{ key: null, value: "" }];
        this.listGradePro.forEach(ele => {
            if (ele.gradeId == gradeId)
                this.listGradeProByGrade.push({value: ele.id, label: ele.productionCode});
        })

    }

    getGradeByModel(modelId: number) {
        //console.log( this.listGrade)
        this.listGradeByModel =  [{ key: null, value: "" }];
        this.listGradeProByGrade = [{ key: null, value: "" }];
        this.listIColorByGradePro =  [{ key: null, value: "" }];
        this.listColorByGradePro =  [{ key: null, value: "" }];
        this.listGrade.forEach(ele => {
            if (ele.modelId == modelId)           
                this.listGradeByModel.push({value: ele.id, label: ele.marketingCode});
        })
    }

    getColorByGradePro(gradeProId) {
        this.listColorByGradePro =  [{ key: null, value: "" }];
        var listColorId = [];
        for (let item of this.listColorGP)
            if (item.produceId == gradeProId)
                listColorId.push(item);

        for (let item of listColorId) {
            for (let color of this.listColor) {
                if (item.colorId == color.id) {
                    this.listColorByGradePro.push({value: color.id, label: color.code});
                }
            }
        }

        this.listIColorByGradePro =  [{ key: null, value: "" }];
        var listIColorId = [];
        for (let item of this.listIColorGP)
            if (item.produceId == gradeProId) {
                //console.log(555)
                listIColorId.push(item);
            }

        for (let item of listIColorId) {
            for (let color of this.listColor) {
                if (item.colorId == color.id) {
                    this.listIColorByGradePro.push({value: color.id, label: color.code});
                }
            }
        }
    }
}
