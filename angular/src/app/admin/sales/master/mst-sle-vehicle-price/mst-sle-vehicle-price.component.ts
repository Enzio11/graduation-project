import { Component, ViewChild, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditMstSleVehiclePriceDto, GetAllGradeColorForVehiclePrice, GetListMstSleVehiclePriceColorDto, MstSleVehiclePriceServiceProxy } from '@shared/service-proxies/service-proxies';
import { PaginationParamsModel } from '@app/shared/common/models/base.model';
import { ceil } from 'lodash';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { CreateOrEditMstSleVehiclePriceModalComponent } from './create-or-edit-mstSleVehiclePrice-modal.component';

@Component({
    templateUrl: './mst-sle-vehicle-price.component.html',
    styleUrls: ['./mst-sle-vehicle-price.component.less'],
})

export class MstSleVehiclePriceComponent extends AppComponentBase {
    @ViewChild('createOrEditMstSleVehiclePriceModal')
    createOrEditMstSleVehiclePriceModal: CreateOrEditMstSleVehiclePriceModalComponent;
    skipCount: number = 0;
    advancedFiltersAreShown : boolean = false;
    gradeProductionIdFilter: number;
    colorIdFilter: number;
    interiorColorIdFilter: number;
    priceAmountMinFilter: number;
    priceAmountMaxFilter: number;
    orderPriceAmountMinFilter: number;
    orderPriceAmountMaxFilter: number;
    statusFilter : string = "";
    transmissionsFilter : number;
    fuleTypeFilter :number;
    engineTypeFilter :number;
    isCBUFilter : number;
    orderingFilter: number;
    dealerIdFilter: number;
    selectedVehiclePrice: CreateOrEditMstSleVehiclePriceDto = new CreateOrEditMstSleVehiclePriceDto();
    vehiclePriceToSend;
    columnDefs;
    VehiclePriceParams;
    defaultColDef;
    paginationParams: PaginationParamsModel = { pageNum: 1, pageSize: 20, totalCount: 0 };
    isLoading: boolean; 
    list: any;
    totalPages: number;
    partParams;
    maxResultCount = 20;
    rowData: any;
    rowSelection;
    selectVehiclePrice;
    sorting: string = '';
    listColor:GetListMstSleVehiclePriceColorDto[] = [];
    listGradePro:GetAllGradeColorForVehiclePrice[] = [];
    frameworkComponents;
    modelIdFilter: number;
    listModel: [] = [];
    modelStatusFilter: string = "Y";

    constructor(
        injector: Injector,
        private _mstSleVehiclePriceServiceProxy:MstSleVehiclePriceServiceProxy,
        private dataFormatService: DataFormatService,
    ) {
        super(injector);
        this.columnDefs = [
            {
                headerName: this.l('MstSleModelCode'),
                headerTooltip: this.l('MstSleModelCode'),
                field: 'modelCode',
                minWidth: 80,
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Grade'),
                headerTooltip: this.l('Grade'),
                field: 'marketingCode',
                flex: 2,
                minWidth: 75,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('GradeProduction'),
                headerTooltip: this.l('GradeProduction'),
                field: 'productionCode',
                flex: 2,
                minWidth: 75,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Color'),
                headerTooltip: this.l('Color'),
                field: 'colorId',
                flex: 2,
                minWidth: 75,
                cellClass: ["text-left"],
                valueGetter: (params) =>
                    this.listColor.find((p) => p.id === params.data.colorId) ? this.listColor.find((p) => p.id === params.data.colorId).code ?? null
                        : null,
            },
            {
                headerName: this.l('MstSleVehiclePriceColorName'),
                headerTooltip: this.l('Color'),
                field: 'colorId',
                flex: 2,
                minWidth: 100,
                cellClass: ["text-left"],
                valueGetter: (params) =>
                    this.listColor.find((p) => p.id === params.data.colorId) ? this.listColor.find((p) => p.id === params.data.colorId).vnName ?? null
                        : null,
            },
            {
                headerName: this.l('InteriorColor'), 
                headerTooltip: this.l('InteriorColor'),
                field: 'interiorColorId',
                flex: 2,
                minWidth: 75,
                cellClass: ["text-left"],
                valueGetter: (params) =>
                    this.listColor.find((p) => p.id === params.data.interiorColorId) ? this.listColor.find((p) => p.id === params.data.interiorColorId).code ?? null
                        : null,
            },
            {
                headerName: this.l('MstSleVehiclePriceTransmission'),
                headerTooltip: this.l('transmission'),
                field: 'transmission',
                minWidth: 100,
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('MstSleVehiclePriceEngineType'),
                headerTooltip: this.l('engineType'),
                field: 'engineType',
                minWidth: 100,
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('MstSleVehiclePriceFuel'),
                headerTooltip: this.l('fuelType'), 
                field: 'fuelType',
                minWidth: 100,
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SeatNo'),
                headerTooltip: this.l('SeatNo'), 
                field: 'seatNo',
                minWidth: 100,
                flex: 2,
                cellStyle: { "padding-right": "5px" },
            },
            {
                headerName: this.l('MstSleVehiclePriceCommercialName'),
                headerTooltip: this.l('MstSleVehiclePriceCommercialName'), 
                field: 'commercialName',
                minWidth: 170,
                flex: 3,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('MstSleVehiclePriceIsCBU'),
                headerTooltip: this.l('isCBU'),
                field: 'isCBU',
                minWidth: 100,
                flex: 2,
                valueGetter: (params) => 
               params.data.isCBU == 0 ? this.l('CKD') :(  params.data.isCBU == 1? this.l('CBU') :  ( params.data.isCBU == 2? this.l('Lexus') : null) ),
                cellClass: ["text-left"],
            },
          
            {
                headerName: this.l('PriceAmount'),
                headerTooltip: this.l('PriceAmount'),
                field: 'priceAmount',
                minWidth: 100,
                flex: 2,
                cellClass: ["text-right"],
                cellStyle: { "padding-right": "5px" },
                cellRenderer: params => this.dataFormatService.moneyFormat(params.value)
            },
            {
                headerName: this.l('OrderPriceAmount'),
                headerTooltip: this.l('OrderPriceAmount'),
                field: 'orderPriceAmount',
                cellClass: ["text-right"],
                minWidth: 100,
                flex: 2,
                cellStyle: { "padding-right": "5px" },
                cellRenderer: params => this.dataFormatService.moneyFormat(params.value)
            },
            {
                headerName: this.l('MstSleVehiclePriceRemarks'),
                headerTooltip: this.l('remarks'),
                field: 'remarks',
                minWidth: 120,
                flex: 3,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Status'),
                headerTooltip: this.l('Status'),
                field: 'status',
                cellRenderer: "agCheckboxRendererComponent",
                data: ['Y', 'N'],
                cellClass: ["text-center"],
                disableCheckbox: true,
                flex: 2
            },
        ];
        this.defaultColDef = {
            flex: 1,
            floatingFilter: false,
            filter: 'agTextColumnFilter',
            resizable: true,
            sortable: true,
            floatingFilterComponentParams: { suppressFilterButton: true },
            textFormatter: function (r) {
                if (r == null) return null;
                return r.toLowerCase();
            },
            minWidth: 75,
        };

        this.frameworkComponents = {
            agCheckboxRendererComponent: AgCheckboxRendererComponent,
        };
    }

    ngOnInit() {}

    getMstSleVehiclePrice(paginationParams?: PaginationParamsModel) {
        if (this.priceAmountMinFilter == null) this.priceAmountMinFilter = undefined;
        if (this.priceAmountMaxFilter == null) this.priceAmountMaxFilter = undefined;
        if (this.orderPriceAmountMinFilter == null) this.orderPriceAmountMinFilter = undefined;
        if (this.orderPriceAmountMaxFilter == null) this.orderPriceAmountMaxFilter = undefined;

        return this._mstSleVehiclePriceServiceProxy
            .getAll(
                this.gradeProductionIdFilter,
                this.colorIdFilter,
                this.interiorColorIdFilter,
                this.priceAmountMinFilter,
                this.priceAmountMaxFilter,
                this.orderPriceAmountMinFilter,
                this.orderPriceAmountMaxFilter,
                this.transmissionsFilter,
                this.fuleTypeFilter,
                this.engineTypeFilter,
                this.isCBUFilter,
                this.modelIdFilter,
                this.statusFilter,
                this.sorting ?? null,
                paginationParams ? paginationParams.skipCount : 0,
                paginationParams ? paginationParams.pageSize : 20
            );
    }

    onGridReady(paginationParams) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.getMstSleVehiclePrice(this.paginationParams).subscribe(
            (result) => {
                this.isLoading = false;
                this.list = result.items;
                this.VehiclePriceParams.api.setRowData(this.list);
                this.totalPages = ceil(
                    result.totalCount / this.paginationParams.pageSize
                );
                this.paginationParams.totalCount = result.totalCount;
                this.paginationParams.totalPage = this.totalPages;
            },
           // (err) => this.notify.error(err.error),
            // () => {
               
            // }
        );
    }

    onChangeSelection(params) {
        this.selectedVehiclePrice = params.api.getSelectedRows()[0];
        if (this.selectedVehiclePrice)
            this.selectVehiclePrice = this.selectedVehiclePrice.id;
            //console.log( this.selectVehiclePrice );
        this.vehiclePriceToSend = this.selectedVehiclePrice;
    }

    callBackVehiclePriceGrid(params) {
        // this._mstSleTestDriveVehicleServiceProxy.getAllModel().subscribe((result) => {
        //     this.listModel = result;
        // });
        this._mstSleVehiclePriceServiceProxy.getListColor().subscribe((result) => {
                this.listColor = result;
            });
         this._mstSleVehiclePriceServiceProxy.getListGradeProduces().subscribe((result) => {
            this.listGradePro = result;
        });    
        this.VehiclePriceParams = params;
        this.VehiclePriceParams.api.paginationSetPageSize(this.paginationParams.pageSize);
        this.onGridReady(this.paginationParams);
        this.selectVehiclePrice = undefined;
    }

    changePage(paginationParams: PaginationParamsModel) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount =
            (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;

        this.getMstSleVehiclePrice(this.paginationParams).subscribe(
            (result) => {
                this.list = result.items;
                this.rowData = result.items;
                this.paginationParams.totalPage = ceil(
                    result.totalCount / this.maxResultCount
                );
            }
        );
    }

    createMstSleVehiclePrice(): void {
        this.createOrEditMstSleVehiclePriceModal.show();
    }

    editMstSleVehiclePrice(): void {
        this.createOrEditMstSleVehiclePriceModal.show(this.selectVehiclePrice);
    }

    deleteMstSleVehiclePrice(): void {
        this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleVehiclePriceServiceProxy
                    .delete(this.selectVehiclePrice)
                    .subscribe(() => {
                        this.callBackVehiclePriceGrid(this.VehiclePriceParams);
                        this.notify.success(this.l('SuccessfullyDeleted'));
                        this.selectVehiclePrice = undefined;
                    });
            }
        });
    }
    eventEnter(event) {
        if (event.keyCode === 13) {
            this.VehiclePriceParams.api.paginationSetPageSize(this.paginationParams.pageSize);
            this.onGridReady(this.paginationParams);
        }
    }
    onChangeDropdown() {
        this.VehiclePriceParams.api.paginationSetPageSize(this.paginationParams.pageSize);
        this.onGridReady(this.paginationParams);
    }
    search() {
        this.VehiclePriceParams.api.paginationSetPageSize(this.paginationParams.pageSize);
        this.onGridReady(this.paginationParams);
        this.selectVehiclePrice = undefined;

    }

    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        this.clearValueFilter();
    }

    clearValueFilter(){
        this.gradeProductionIdFilter = undefined;
        this.colorIdFilter = undefined;
        this.interiorColorIdFilter = undefined;
        this.priceAmountMinFilter = undefined;
        this.priceAmountMaxFilter = undefined;
        this.orderPriceAmountMinFilter = undefined;
        this.orderPriceAmountMaxFilter = undefined;
        this.modelIdFilter = undefined;
    }
}
