import { TABS } from "@app/shared/constants/tab-keys";
import {NgModule} from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleVehiclePriceComponent } from "./mst-sle-vehicle-price.component";
import { CreateOrEditMstSleVehiclePriceModalComponent } from "./create-or-edit-mstSleVehiclePrice-modal.component";

const tabcode_component_dict = {
    [TABS.MASTER_SALE_VEHICLE_PRICE]: MstSleVehiclePriceComponent
  };
@NgModule({
    imports: [
      CommonDeclareModule,
    ],
    declarations: [
        MstSleVehiclePriceComponent,
        CreateOrEditMstSleVehiclePriceModalComponent
    ],
})

export class MstSleVehiclePriceModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}