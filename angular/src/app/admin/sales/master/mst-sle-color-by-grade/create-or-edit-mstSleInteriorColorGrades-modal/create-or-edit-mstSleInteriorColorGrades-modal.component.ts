import {
    Component,
    ViewChild,
    Output,
    EventEmitter,
    Injector,
    Input,
    OnInit,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { CreateOrEditMstSleInteriorColorGradesProductionDto, MstSleColorByGradeServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";

@Component({
    selector: "createOrEditMstSleInteriorColorGradesModal",
    templateUrl: "./create-or-edit-mstSleInteriorColorGrades-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleInteriorColorGrades-modal.component.less"],
})

export class CreateOrEditMstSleInteriorColorGradesProducesModalComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() gradeProduceIdPick: number;

    active = false;
    saving = false;

    colorName;
    mstSleInteriorColorGradesProduces: CreateOrEditMstSleInteriorColorGradesProductionDto = new CreateOrEditMstSleInteriorColorGradesProductionDto();
    listColors: any[]=[];
    listProduction: any[]=[];
    listStatus: { value: string, label: string }[] = [
        { value: "Y", label: this.l("MstSleEnable")},
        { value: "N", label: this.l("MstSleDisable")}
    ];

    constructor(
        injector: Injector,
        private _mstSleColorByGradeServiceProxy: MstSleColorByGradeServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._mstSleColorByGradeServiceProxy.getListColor().subscribe((re) => {
            re.forEach(e=> this.listColors.push({value: e.colorId, label: e.color}))
        });
        this._mstSleColorByGradeServiceProxy.getListProduction().subscribe(re => {
            re.forEach(e=> this.listProduction.push({value: e.id, label: e.productionCode}))
        });
    }

    show(mstSleInteriorColorGradesProducesId?: number): void {
        if (!mstSleInteriorColorGradesProducesId) {
            this.colorName = undefined;
            this.mstSleInteriorColorGradesProduces = new CreateOrEditMstSleInteriorColorGradesProductionDto();
            this.mstSleInteriorColorGradesProduces.id = mstSleInteriorColorGradesProducesId;
            this.mstSleInteriorColorGradesProduces.produceId = this.gradeProduceIdPick;
            this.active = true;
            this.modal.show();
        } else {
            this._mstSleColorByGradeServiceProxy
                .getInteriorColorGradesProducesForEdit(mstSleInteriorColorGradesProducesId)
                .subscribe((result) => {
                    this.mstSleInteriorColorGradesProduces = result;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;

        this.mstSleInteriorColorGradesProduces.produceId = this.gradeProduceIdPick;

        this._mstSleColorByGradeServiceProxy
            .createOrEditInterior(this.mstSleInteriorColorGradesProduces)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("MstSleColorByGradeSavedSuccessfully"));
                this.close();
                this.modalSave.emit();
            });
    }
    onChangeColor(event){
        this.colorName = this.listColors.find(e => e.id == event).vnName  
    }
    
    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
