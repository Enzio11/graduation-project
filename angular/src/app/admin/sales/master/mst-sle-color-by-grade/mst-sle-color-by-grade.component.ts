import { Component, Injector, ViewChild } from '@angular/core';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { GetMstSleColorGradeProductionForViewDto, GetMstSleGradeProduceDto, MstSleColorByGradeServiceProxy } from '@shared/service-proxies/service-proxies';
import { ceil } from 'lodash';
import { CreateOrEditMstSleColorGradesProducesModalComponent } from './create-or-edit-mstSleColorGrades-modal/create-or-edit-mstSleColorGrades-modal.component';
import { CreateOrEditMstSleInteriorColorGradesProducesModalComponent } from './create-or-edit-mstSleInteriorColorGrades-modal/create-or-edit-mstSleInteriorColorGrades-modal.component';

@Component({
  selector: 'app-mst-sle-color-by-grade',
  templateUrl: './mst-sle-color-by-grade.component.html',
  styleUrls: ['./mst-sle-color-by-grade.component.less'],
  animations: [appModuleAnimation()]
})
export class MstSleColorByGradeComponent extends AppComponentBase {
  @ViewChild("createOrEditMstSleColorGradesModal", { static: true })
  createOrEditMstSleColorGradesModal: CreateOrEditMstSleColorGradesProducesModalComponent;
  @ViewChild("createOrEditMstSleInteriorColorGradesModal", { static: true })
  createOrEditMstSleInteriorColorGradesModal: CreateOrEditMstSleInteriorColorGradesProducesModalComponent;
  //Grades Produces
  filterText;
  gradeIdFilter: number;
  productionCodeFilter = "";
  enNameFilter = "";
  shortModelFilter = "";
  fullModelFilter = "";
  frameNoLengthFilter: number;
  statusFilter = "Y";
  orderingFilter: number;
  gradeProduceIdSent: number;
  selectProduce: GetMstSleGradeProduceDto = new GetMstSleGradeProduceDto();
  selectedProduce: number;
  vnNameFilter = "";
  advancedFiltersAreShown = false;
  skipCount;
  sorting;
  //Color Grades 
  selectColor;

  //Interior Color Grades
  interiorColorIdFilter: number;
  orderingInteriorFilter: number;
  statusInteriorFilter = "Y";
  descriptionInteriorFilter = "";
  selectInterior: GetMstSleColorGradeProductionForViewDto = new GetMstSleColorGradeProductionForViewDto();
  isLoading: boolean;
  maxResultCount: number;

  //production tables
  productionTables;
  defaultColDef;
  productionData: any;
  paginationProductionParams: PaginationParamsModel;
  productionParam!:GridParams;
  listProduction;
  totalPages: number;
  productionSorting;
  listModel;

  //color
  colorData: any;
  colorTables;
  selectedColorId: number;
  listColor;
  listGasolineType;

  // Interior
  interiorData: any;
  interiorTables;
  selectedInteriorId: number;
  listInterior;
  frameworkComponents: FrameworkComponent;
  listGrade;
  modelStatusFilter = "Y";
  constructor(
    injector: Injector,
    private dataFormatService: DataFormatService,
    private _mstSleColorByGradeServiceProxy: MstSleColorByGradeServiceProxy
  ) {
    super(injector);
    this.productionTables = [
      {
        headerName: this.l("MstSleColorByGradeProductionCode"),
        headerTooltip: this.l("MstSleColorByGradeProductionCode"),
        field: "productionCode",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeEnName"),
        headerTooltip: this.l("MstSleColorByGradeEnName"),
        field: "enName",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeVnName"),
        headerTooltip: this.l("MstSleColorByGradeVnName"),
        field: "vnName",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeOrdering"),
        headerTooltip: this.l("MstSleColorByGradeOrdering"),
        field: "ordering",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l('MstSleColorByGradeIsHasAudio'),
        headerTooltip: this.l('MstSleColorByGradeIsHasAudio'),
        field: 'isHasAudio',
        minWidth: 100,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l('MstSleColorByGradeCbuCkd'),
        headerTooltip: this.l('MstSleColorByGradeCbuCkd'),
        field: 'cbuCkd',
        minWidth: 100,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l("MstSleColorByGradeShortModel"),
        headerTooltip: this.l("MstSleColorByGradeShortModel"),
        field: "shortModel",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeWmi"),
        headerTooltip: this.l("MstSleColorByGradeWmi"),
        field: "wmi",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeVds"),
        headerTooltip: this.l("MstSleColorByGradeVds"),
        field: "vds",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeFullModel"),
        headerTooltip: this.l("MstSleColorByGradeFullModel"),
        field: "fullModel",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeBarcode"),
        headerTooltip: this.l("MstSleColorByGradeBarcode"),
        field: "barcode",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeGasolineType"),
        headerTooltip: this.l("MstSleColorByGradeGasolineType"),
        valueGetter: (params) =>
          this.listGasolineType.find((p) => p.id == params.data.gasolineTypeId && params.data) ? this.listGasolineType.find((p) => p.id == params.data.gasolineTypeId).name ?? null : '',
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeGrade"),
        headerTooltip: this.l("MstSleColorByGradeGrade"),
        valueGetter: (params) =>
          this.listGrade.find((p) => p.id == params.data.gradeId && params.data) ? this.listGrade.find((p) => p.id == params.data.gradeId).marketingCode ?? null : '',
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeFromDate"),
        headerTooltip: this.l("MstSleColorByGradeFromDate"),
        field: "fromDate",
        valueFormatter: (params) => {
          return this.dataFormatService.dateFormat(params.value)
        },
        minWidth: 100,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l("MstSleColorByGradeToDate"),
        headerTooltip: this.l("MstSleColorByGradeToDate"),
        field: "toDate",
        valueFormatter: (params) => {
          return this.dataFormatService.dateFormat(params.value)
        },
        minWidth: 100,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleColorByGradeIsFirmColor'),
        headerTooltip: this.l('MstSleColorByGradeIsFirmColor'),
        field: 'isFirmColor',
        minWidth: 100,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l("MstSleColorByGradeFrameNoLength"),
        headerTooltip: this.l("MstSleColorByGradeFrameNoLength"),
        field: "frameNoLength",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeManYeah"),
        headerTooltip: this.l("MstSleColorByGradeManYeah"),
        field: "manYeah",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeCapacity"),
        headerTooltip: this.l("MstSleColorByGradeCapacity"),
        field: "capacity",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeLength"),
        headerTooltip: this.l("MstSleColorByGradeLength"),
        field: "length",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeWidth"),
        headerTooltip: this.l("MstSleColorByGradeWidth"),
        field: "width",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeHeight"),
        headerTooltip: this.l("MstSleColorByGradeHeight"),
        field: "height",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeWeight"),
        headerTooltip: this.l("MstSleColorByGradeWeight"),
        field: "weight",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeTireSize"),
        headerTooltip: this.l("MstSleColorByGradeTireSize"),
        field: "tireSize",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradePayload"),
        headerTooltip: this.l("MstSleColorByGradePayload"),
        field: "payload",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradePullingWeight"),
        headerTooltip: this.l("MstSleColorByGradePullingWeight"),
        field: "pullingWeight",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeSeatNoStanding"),
        headerTooltip: this.l("MstSleColorByGradeSeatNoStanding"),
        field: "seatNoStanding",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeSeatNoLying"),
        headerTooltip: this.l("MstSleColorByGradeSeatNoLying"),
        field: "seatNoLying",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeSeatNo"),
        headerTooltip: this.l("MstSleColorByGradeSeatNo"),
        field: "seatNo",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeCaseSize"),
        headerTooltip: this.l("MstSleColorByGradeCaseSize"),
        field: "caseSize",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeBaseLength"),
        headerTooltip: this.l("MstSleColorByGradeBaseLength"),
        field: "baseLength",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l('MstSleColorByGradeStatus'),
        headerTooltip: this.l('MstSleColorByGradeStatus'),
        field: 'status',
        minWidth: 100,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l("MstSleColorByGradeFlold"),
        headerTooltip: this.l("MstSleColorByGradeFlold"),
        field: "flold",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeOriginFrom"),
        headerTooltip: this.l("MstSleColorByGradeOriginFrom"),
        field: "originFrom",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeFuel"),
        headerTooltip: this.l("MstSleColorByGradeFuel"),
        field: "fuel",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeEngine"),
        headerTooltip: this.l("MstSleColorByGradeEngine"),
        field: "engine",
        cellClass: ["text-left"],
        minWidth: 100
      },
      {
        headerName: this.l("MstSleColorByGradeDesignStyle"),
        headerTooltip: this.l("MstSleColorByGradeDesignStyle"),
        field: "designStyle",
        cellClass: ["text-left"],
        minWidth: 100
      },
    ];
    this.colorTables = [
      {
        headerName: this.l("MstSleColorByGradeColor"),
        headerTooltip: this.l("MstSleColorByGradeColor"),
        valueGetter: (params) =>
          this.listColor.find((p) => p.colorId == params.data.colorId && params.data) ? this.listColor.find((p) => p.colorId == params.data.colorId).color ?? null : '',
        cellClass: ["text-left"],
        flex: 2
      },
      {
        headerName: this.l("MstSleColorByGradeProduce"),
        headerTooltip: this.l("MstSleColorByGradeProduce"),
        flex: 2,
        valueGetter: (params) =>
          this.listProduction.find((p) => p.id == params.data.produceId && params.data) ? this.listProduction.find((p) => p.id == params.data.produceId).productionCode ?? null : '',
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleColorByGradeStatus'),
        headerTooltip: this.l('MstSleColorByGradeStatus'),
        field: 'status',
        flex: 2,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l("MstSleColorByGradeDescription"),
        headerTooltip: this.l("MstSleColorByGradeDescription"),
        field: "description",
        cellClass: ["text-left"],
        flex: 2
      },
    ];
    this.interiorTables = [
      {
        headerName: this.l("MstSleColorByGradeColor"),
        headerTooltip: this.l("MstSleColorByGradeColor"),
        valueGetter: (params) =>
          this.listColor.find((p) => p.colorId == params.data.colorId && params.data) ? this.listColor.find((p) => p.colorId == params.data.colorId).color ?? null : '',
        cellClass: ["text-left"],
        flex: 2
      },
      {
        headerName: this.l("MstSleColorByGradeProduce"),
        headerTooltip: this.l("MstSleColorByGradeProduce"),
        flex: 2,
        valueGetter: (params) =>
          this.listProduction.find((p) => p.id == params.data.produceId && params.data) ? this.listProduction.find((p) => p.id == params.data.produceId).productionCode ?? null : '',
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleColorByGradeStatus'),
        headerTooltip: this.l('MstSleColorByGradeStatus'),
        field: 'status',
        flex: 2,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
      {
        headerName: this.l("MstSleColorByGradeDescription"),
        headerTooltip: this.l("MstSleColorByGradeDescription"),
        field: "description",
        cellClass: ["text-left"],
        flex: 2
      },
    ];

    this.defaultColDef = {
      flex: 1,
      floatingFilter: false,
      filter: 'agTextColumnFilter',
      resizable: true,
      sortable: true,
      floatingFilterComponentParams: { suppressFilterButton: true },
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
    this.frameworkComponents = {
      agCheckboxRendererComponent: AgCheckboxRendererComponent
    };
  }

  ngOnInit() {
    this.colorData = [];
    this.interiorData = [];
    this.paginationProductionParams = { pageNum: 1, pageSize: 10, totalCount: 0 };

    this._mstSleColorByGradeServiceProxy.getListGrade().subscribe((re) => {
      this.listGrade = re;
    });
    this._mstSleColorByGradeServiceProxy.getListProduction().subscribe(re => {
      this.listProduction = re;
    });
    this._mstSleColorByGradeServiceProxy.getListGasolineType().subscribe(re => {
      this.listGasolineType = re;
    });
    this._mstSleColorByGradeServiceProxy.getListColor().subscribe(re => {
      this.listColor = re;
    });
  }

  callBackGrid(params) {
    this.productionParam = params;
    this.productionParam.api.paginationSetPageSize(this.paginationProductionParams.pageSize);
    this.onGridReady(this.paginationProductionParams);
  }

  onGridReady(paginationProductionParams) {
    this.paginationProductionParams = paginationProductionParams;
    this.paginationProductionParams.skipCount = (paginationProductionParams.pageNum - 1) * paginationProductionParams.pageSize;
    this.getMstGradesProduces(this.paginationProductionParams).subscribe(
      (result) => {
        this.isLoading = false;
        this.listProduction = result.items;
        this.totalPages = ceil(result.totalCount / this.paginationProductionParams.pageSize);
        this.paginationProductionParams.totalCount = result.totalCount;
        this.paginationProductionParams.totalPage = this.totalPages;
      },
      (err) => this.notify.error(err.error),
      () => {
        this.productionParam.api.setRowData(this.listProduction);
      }
    );
  }

  changeProductionPage(params) {
    this.paginationProductionParams = params;
    this.paginationProductionParams.skipCount = (params.pageNum - 1) * params.pageSize;
    this.maxResultCount = params.pageSize;
    this.getMstGradesProduces(this.paginationProductionParams).subscribe(
      (result) => {
        this.listProduction = result.items;
        this.productionData = result.items;
        this.paginationProductionParams.totalPage = ceil(result.totalCount / this.maxResultCount);
      }
    );
  }

  getMstGradesProduces(paginationProductionParams?) {
    return this._mstSleColorByGradeServiceProxy.getAllGradesProduces(
      this.filterText,
      this.productionSorting ?? null,
      paginationProductionParams ? paginationProductionParams.skipCount : 0,
      paginationProductionParams ? paginationProductionParams.pageSize : 10
    );
  }

  onChangeProductionSelection(params) {
    this.selectedColorId = undefined;
    this.selectedInteriorId = undefined;

    this.selectProduce = params.api.getSelectedRows()[0];
    if (this.selectProduce)
      this.selectedProduce = this.selectProduce.id;
    this.getExternalColor();
    this.getInteriorColor();
  }

  getExternalColor(paginationColorParams?) {
    this.selectedInteriorId = undefined;
    this.maxResultCount = paginationColorParams ? paginationColorParams.pageSize : 10
    this._mstSleColorByGradeServiceProxy
      .getColorGradesProducesByProduceId(
        this.selectedProduce
      )
      .subscribe((result) => {
        this.colorData = result;
      });
    this.gradeProduceIdSent = this.selectedProduce;
  }

  getColorGradesProducesByProduceId() {
    return this._mstSleColorByGradeServiceProxy.getColorGradesProducesByProduceId(
      this.selectedProduce
    );
  }

  onChangeColorSelection(params) {
    this.selectColor = params.api.getSelectedRows()[0];
    if (this.selectColor)
      this.selectedColorId = this.selectColor.id;
  }

  getInteriorColor(paginationInteriorParams?) {
    this.selectedInteriorId = undefined;
    this.maxResultCount = paginationInteriorParams ? paginationInteriorParams.pageSize : 10
    this._mstSleColorByGradeServiceProxy
      .getInteriorColorGradesProducesByProduceId(this.selectedProduce)
      .subscribe((result) => {
        this.interiorData = result;
      });
    this.gradeProduceIdSent = this.selectedProduce;
  }

  onChangeInteriorSelection(params) {
    this.selectInterior = params.api.getSelectedRows()[0];
    if (this.selectInterior)
      this.selectedInteriorId = this.selectInterior.id;
  }

  getInteriorColorGradesProducesByProduceId() {
    return this._mstSleColorByGradeServiceProxy.getInteriorColorGradesProducesByProduceId(this.selectedProduce);
  }

  createMstSleColorGradeProduces() {
    this.createOrEditMstSleColorGradesModal.show();
  }

  editMstSleColorGradesProduces() {
    this.createOrEditMstSleColorGradesModal.show(this.selectedColorId);
  }

  deleteMstSleColorGradeProduces() {
    this.message.confirm("", this.l("MstSleColorByGradeAreYouSure"), (isConfirmed) => {
      if (isConfirmed) {
        this._mstSleColorByGradeServiceProxy
          .deleteColor(this.selectedColorId)
          .subscribe(() => {
            this.getExternalColor();
            this.notify.success(this.l("MstSleColorByGradeSuccessfullyDeleted"));
          });
      }
    });
  }

  createMstSleInteriorColorGradeProduces() {
    this.createOrEditMstSleInteriorColorGradesModal.show();
  }

  editMstSleInteriorColorGradesProduces() {
    this.createOrEditMstSleInteriorColorGradesModal.show(this.selectedInteriorId);
  }

  deleteMstSleInteriorColorGradeProduces(): void {
    this.message.confirm("", this.l("MstSleColorByGradeAreYouSure"), (isConfirmed) => {
      if (isConfirmed) {
        this._mstSleColorByGradeServiceProxy
          .deleteInterior(this.selectedInteriorId)
          .subscribe(() => {
            this.getInteriorColor();
            this.notify.success(this.l("MstSleColorByGradeSuccessfullyDeleted"));
          });
      }
    });
  }

  eventEnter(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }

  search() {
    this.paginationProductionParams = { pageNum: 1, pageSize: 10, totalCount: 0 };
    this.productionParam.api.paginationSetPageSize(this.paginationProductionParams.pageSize);
    this.onGridReady(this.paginationProductionParams);
  }

  onChangeFilterShown() {
    this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
    this.clearValueFilter();
  }

  clearValueFilter() {
    this.filterText = '';
    this.gradeIdFilter = undefined;
    this.productionCodeFilter = '';
    this.enNameFilter = '';
    this.vnNameFilter = '';
    this.orderingFilter = undefined;
    this.shortModelFilter = '';
    this.fullModelFilter = '';
    this.frameNoLengthFilter = undefined;
    this.statusFilter = 'Y';
  }


}