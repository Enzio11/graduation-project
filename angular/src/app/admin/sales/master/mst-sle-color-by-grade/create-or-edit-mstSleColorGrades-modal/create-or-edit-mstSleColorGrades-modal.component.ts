import {
    Component,
    ViewChild,
    Output,
    EventEmitter,
    Injector,
    Input,
    OnInit,
} from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { CreateOrEditMstSleColorGradesProductionDto, MstSleColorByGradeServiceProxy } from "@shared/service-proxies/service-proxies";
import { ModalDirective } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";

@Component({
    selector: "createOrEditMstSleColorGradesModal",
    templateUrl: "./create-or-edit-mstSleColorGrades-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleColorGrades-modal.component.less"],
})

export class CreateOrEditMstSleColorGradesProducesModalComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() gradeProduceIdPick: number;

    active = false;
    saving = false;

    mstSleColorGradesProduces: CreateOrEditMstSleColorGradesProductionDto = new CreateOrEditMstSleColorGradesProductionDto();
    listColors: any[] = [];
    listProduction: any[] = [];
    colorName;
    produceId;
    listStatus: { value: string, label: string }[] = [
        { value: undefined, label: this.l("")},
        { value: "Y", label: this.l("MstSleEnable")},
        { value: "N", label: this.l("MstSleDisable")}
    ];

    constructor(
        injector: Injector,
        private _mstSleColorByGradeServiceProxy: MstSleColorByGradeServiceProxy,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._mstSleColorByGradeServiceProxy.getListColor().subscribe((re) => {
            re.forEach(e=> this.listColors.push({value: e.colorId, label: e.color}))
        });
        this._mstSleColorByGradeServiceProxy.getListProduction().subscribe(re => {
            re.forEach(e=> this.listProduction.push({value: e.id, label: e.productionCode}))
        });
    }

    show(mstSleColorGradesProducesId?: number): void {
        if (!mstSleColorGradesProducesId) {
            this.colorName = []
            this.mstSleColorGradesProduces = new CreateOrEditMstSleColorGradesProductionDto();
            this.mstSleColorGradesProduces.id = mstSleColorGradesProducesId;
            this.mstSleColorGradesProduces.produceId = this.gradeProduceIdPick;
            this.active = true;
            this.modal.show();
        } else {
            this._mstSleColorByGradeServiceProxy
                .getColorGradesProducesForEdit(mstSleColorGradesProducesId)
                .subscribe((result) => {
                    this.mstSleColorGradesProduces = result;
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this.mstSleColorGradesProduces.produceId = this.gradeProduceIdPick;

        this._mstSleColorByGradeServiceProxy.createOrEditColor(this.mstSleColorGradesProduces)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("MstSleColorByGradeSavedSuccessfully"));
                this.close();
                this.modalSave.emit();
            });
    }

    onChangeColor(event) {
        this.colorName = this.listColors.find(e => e.id == event).vnName
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
