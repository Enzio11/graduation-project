import { TABS } from "@app/shared/constants/tab-keys";
import {NgModule} from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleColorByGradeComponent } from "./mst-sle-color-by-grade.component";
import { CreateOrEditMstSleInteriorColorGradesProducesModalComponent } from "./create-or-edit-mstSleInteriorColorGrades-modal/create-or-edit-mstSleInteriorColorGrades-modal.component";
import { CreateOrEditMstSleColorGradesProducesModalComponent } from "./create-or-edit-mstSleColorGrades-modal/create-or-edit-mstSleColorGrades-modal.component";

const tabcode_component_dict = {
    [TABS.MASTER_SALE_COLOR_BY_GRADE]: MstSleColorByGradeComponent
  };
@NgModule({
    imports: [
      CommonDeclareModule,
    ],
    declarations: [
        MstSleColorByGradeComponent,
        CreateOrEditMstSleInteriorColorGradesProducesModalComponent,
        CreateOrEditMstSleColorGradesProducesModalComponent
    ],
})

export class MstSleColorByGradeModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}