import { Component, Injector, ViewChild } from '@angular/core';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { AppComponentBase } from '@shared/common/app-component-base';
import { MstSleColorDto, MstSleColorServiceProxy } from '@shared/service-proxies/service-proxies';
import { ceil } from 'lodash';
import { CreateOrEditMstSleColorsModalComponent } from './create-or-edit-mst-sle-color/create-or-edit-mst-sle-color.component';

@Component({
  selector: 'app-mst-sle-color',
  templateUrl: './mst-sle-color.component.html',
  styleUrls: ['./mst-sle-color.component.less'],
})
export class MstSleColorComponent extends AppComponentBase {
  @ViewChild('createOrEditMstSleColorsModal')
  createOrEditMstSleColorsModal: CreateOrEditMstSleColorsModalComponent;
  skipCount;
  advancedFiltersAreShown = false;
  filterText = "";
  codeFilter = "";
  vnNameFilter = "";
  enNameFilter = "";
  descriptionFilter = "";
  statusFilter = "Y";
  selectedColors: MstSleColorDto = new MstSleColorDto();
  columnDefs;
  params!: GridParams;
  defaultColDef;
  paginationParams: PaginationParamsModel;
  isLoading: boolean;
  list: any;
  totalPages: number;
  maxResultCount: number;
  rowData: any;
  selectColors;
  sorting = '';
  hexCodeFilter;
  frameworkComponents: FrameworkComponent;
  colorRate: number;

  constructor(
    injector: Injector,
    private _mstSleColorServiceProxy: MstSleColorServiceProxy,
  ) {
    super(injector);
    this.columnDefs = [
      {
        headerName: this.l('STT'),
        headerTooltip: this.l('STT'),
        cellRenderer: params=> params.rowIndex + (this.paginationParams.pageNum! - 1) * this.paginationParams.pageSize! + 1,
        field: "no",
        cellClass: ["text-center"],
        flex: 1
    },
      {
        headerName: this.l('MstSleColorCode'),
        headerTooltip: this.l('MstSleColorCode'),
        field: 'code',
        flex: 1,
        cellClass: ["text-center"],
      },
      {
        headerName: this.l('MstSleColorHexCode'),
        headerTooltip: this.l('MstSleColorHexCode'),
        field: 'hexCode',
        flex: 1.2,
        cellClass: ["text-center"],
        cellStyle: params => {
          if (params.data.hexCode !== undefined) {
            return { 'background-color': params.data.hexCode };
          }
        },
      },
      {
        headerName: this.l('MstSleColorHexCode2'),
        headerTooltip: this.l('MstSleColorHexCode2'),
        field: 'hexCode2',
        flex: 1.2,
        cellClass: ["text-center"],
        cellStyle: params => {
          if (params.data.hexCode2 !== undefined) {
            return { 'background-color': params.data.hexCode2 };
          }
        },
      },
      {
        headerName: this.l('MstSleColorOrdering'),
        headerTooltip: this.l('MstSleColorOrdering'),
        field: 'ordering',
        cellClass: ["text-center"],
        flex: 1,
      },
      {
        headerName: this.l('MstSleColorOrderingRpt'),
        headerTooltip: this.l('MstSleColorOrderingRpt'),
        field: 'orderingRpt',
        cellClass: ["text-center"],
        flex: 1,
      },
      {
        headerName: this.l('MstSleColorEnName'),
        headerTooltip: this.l('MstSleColorEnName'),
        field: 'enName',
        flex: 2.5,
        cellClass: ["text-left"],
      },

      {
        headerName: this.l('MstSleColorVnName'),
        headerTooltip: this.l('MstSleColorVnName'),
        field: 'vnName',
        flex: 2.5,
        cellClass: ["text-left"],
      },
      {
        headerName: this.l('MstSleColorDescription'),
        headerTooltip: this.l('MstSleColorDescription'),
        field: 'description',
        cellClass: ["text-left"],
        flex: 2.5,
      },
      {
        headerName: this.l('MstSleColorStatus'),
        headerTooltip: this.l('MstSleColorStatus'),
        field: 'status',
        flex: 1,
        cellRenderer: "agCheckboxRendererComponent",
        data: ['Y', 'N'],
        cellClass: ["text-center"],
        disableCheckbox: true
      },
    ];

    this.defaultColDef = {
      flex: 1,
      floatingFilter: false,
      filter: 'agTextColumnFilter',
      resizable: true,
      sortable: true,
      floatingFilterComponentParams: { suppressFilterButton: true },
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
    this.frameworkComponents = {
      agCheckboxRendererComponent: AgCheckboxRendererComponent
    };
  }

  ngOnInit() {
    this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
  }

  getMstSleColor(paginationParams?: PaginationParamsModel) {
    return this._mstSleColorServiceProxy
      .getAllMstSleColor(
        this.filterText,
        this.sorting ?? null,
        paginationParams ? paginationParams.skipCount : 0,
        paginationParams ? paginationParams.pageSize : 20
      );
  }

  onGridReady(paginationParams) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;

    this.getMstSleColor(this.paginationParams).subscribe(
      (result) => {
        this.isLoading = false;
        this.list = result.items;
        this.totalPages = ceil(result.totalCount / this.paginationParams.pageSize);
        this.paginationParams.totalCount = result.totalCount;
        this.paginationParams.totalPage = this.totalPages;
        (err) => this.notify.error(err.error);
        this.params.api.setRowData(this.list);
      }
    );
  }

  onChangeSelection(params) {
    this.selectedColors = params.api.getSelectedRows()[0];
    if (this.selectedColors)
      this.selectColors = this.selectedColors.id;
  }

  callBackGrid(params) {
    this.params = params;
    this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
    this.onGridReady(this.paginationParams);
    this.selectColors = undefined;

  }

  changePage(paginationParams: PaginationParamsModel) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
    this.maxResultCount = paginationParams.pageSize;

    this.getMstSleColor(this.paginationParams).subscribe(
      (result) => {
        this.list = result.items;
        this.paginationParams.totalPage = ceil(
          result.totalCount / this.maxResultCount
        );
      },
      (err) => this.notify.error(err.error),
      () => {
        this.params.api.setRowData(this.list);
      }
    );
  }

  createMstSleColors(): void {
    this.createOrEditMstSleColorsModal.show();
  }

  editMstSleColors(): void {
    this.createOrEditMstSleColorsModal.show(this.selectColors);
  }

  deleteMstSleColors(): void {
    this.message.confirm('', this.l('MstSleColorAreYouSure'), (isConfirmed) => {
      if (isConfirmed) {
        this._mstSleColorServiceProxy
          .delete(this.selectColors)
          .subscribe(() => {
            this.callBackGrid(this.params);
            this.notify.success(this.l('MstSleColorSuccessfullyDeleted'));
            this.selectColors = undefined;
          });
      }
    });
  }

  eventEnter(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }

  search() {
    this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
    this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
    this.onGridReady(this.paginationParams);
  }

  onChangeFilterShown() {
    this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
    this.clearValueFilter();
  }

  clearValueFilter() {
    this.filterText = "";
    this.codeFilter = "";
    this.vnNameFilter = "";
    this.enNameFilter = "";
    this.hexCodeFilter = "";
    this.descriptionFilter = "";
    this.colorRate = undefined;
    this.statusFilter = "Y";
  }
}