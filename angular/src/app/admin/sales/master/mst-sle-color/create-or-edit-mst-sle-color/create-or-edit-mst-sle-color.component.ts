import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditMstSleColorDto, MstSleColorServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'createOrEditMstSleColorsModal',
  templateUrl: './create-or-edit-mst-sle-color.component.html',
  styleUrls: ['./create-or-edit-mst-sle-color.component.less']
})
export class CreateOrEditMstSleColorsModalComponent extends AppComponentBase {
  @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

  active = false;
  saving = false;

  mstSleColors: CreateOrEditMstSleColorDto = new CreateOrEditMstSleColorDto();
  listStatus: { value: string, label: string }[] = [
    { value: "Y", label: this.l("MstSleEnable")},
    { value: "N", label: this.l("MstSleDisable")}
];
  constructor(
    injector: Injector,
    private _mstSleColorServiceProxy: MstSleColorServiceProxy,
  ) {
    super(injector);
  }

  show(mstSleColorsId?: number): void {
    if (!mstSleColorsId) {
      this.mstSleColors = new CreateOrEditMstSleColorDto();
      this.mstSleColors.id = mstSleColorsId;
      this.active = true;
      this.modal.show();
    } else {
      this._mstSleColorServiceProxy
        .getMstSleColorsForEdit(mstSleColorsId)
        .subscribe((result) => {
          this.mstSleColors = result.mstSleColors;
          this.active = true;
          this.modal.show();
        });
    }
  }

  save(): void {
    this.saving = true;

    this._mstSleColorServiceProxy
      .createOrEdit(this.mstSleColors)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.close();
        this.modalSave.emit(null);
      });
  }

  close(): void {
    this.active = false;
    this.modal.hide();
  }
}