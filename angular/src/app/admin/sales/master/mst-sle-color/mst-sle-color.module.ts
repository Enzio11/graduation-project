import { TABS } from "@app/shared/constants/tab-keys";
import {NgModule} from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleColorComponent } from "./mst-sle-color.component";
import { CreateOrEditMstSleColorsModalComponent } from "./create-or-edit-mst-sle-color/create-or-edit-mst-sle-color.component";

const tabcode_component_dict = {
    [TABS.MASTER_SALE_COLOR]: MstSleColorComponent
  };
@NgModule({
    imports: [
      CommonDeclareModule,
    ],
    declarations: [
        MstSleColorComponent,
        CreateOrEditMstSleColorsModalComponent
    ],
})

export class MstSleColorModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}