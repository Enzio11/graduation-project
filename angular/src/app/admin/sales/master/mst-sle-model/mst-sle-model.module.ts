import { TABS } from "@app/shared/constants/tab-keys";
import { NgModule } from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleModelComponent } from "./mst-sle-model.component";
import { CreateOrEditMstSleModelsModalComponent } from "./create-or-edit-mstSleModels-modal/create-or-edit-mstSleModels-modal.component";
import { CreateOrEditMstSleGradesModalComponent } from "./create-or-edit-mstSleGrades-modal/create-or-edit-mstSleGrades-modal.component";
import { CreateOrEditMstSleGradeProducesModalComponent } from "./create-or-edit-mstSleGradeProductions-modal/create-or-edit-mstSleGradeProduces-modal.component";
import { MstSleOtherMakeComponent } from "./mst-sle-other-make/mst-sle-other-make.component";
import { CreateOrEditMstSleOtherGradesModalComponent } from "./mst-sle-other-make/create-or-edit-other-grade/create-or-edit-other-grade.component";

const tabcode_component_dict = {
	[TABS.MASTER_SALE_MODEL]: MstSleModelComponent
};
@NgModule({
	imports: [
		CommonDeclareModule,
	],
	declarations: [
		MstSleModelComponent,
		CreateOrEditMstSleModelsModalComponent,
		CreateOrEditMstSleGradesModalComponent,
		CreateOrEditMstSleGradeProducesModalComponent,
		MstSleOtherMakeComponent,
		CreateOrEditMstSleOtherGradesModalComponent
	],
})

export class MstSleModelModule {
	static getComponent(tabCode: string) {
		return tabcode_component_dict[tabCode];
	}
}