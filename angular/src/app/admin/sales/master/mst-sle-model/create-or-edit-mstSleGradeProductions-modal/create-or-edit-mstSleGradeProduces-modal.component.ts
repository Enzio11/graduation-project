import { Component, ViewChild, Injector, Output, EventEmitter, Input} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { CreateOrEditMstSleGradeProductionDto, GetAllCodeColorByGradesId, MstSleGradeProductionServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';

@Component({
    selector: "createOrEditMstSleGradeProducesModal",
    templateUrl: "./create-or-edit-mstSleGradeProduces-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleGradeProduces-modal.component.less"],
})

export class CreateOrEditMstSleGradeProducesModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() gradeIdPick: number;

    active: boolean = false;
    saving: boolean = false;
    gasolineTypes: any[] =[];
    mstSleGradeProduces: CreateOrEditMstSleGradeProductionDto = new CreateOrEditMstSleGradeProductionDto();
    gradeProductions: GetAllCodeColorByGradesId[] = [];
    listStatus: { value: string, label: string }[] = [
        { value: "Y", label: this.l("MstSleEnable")},
        { value: "N", label: this.l("MstSleDisable")}
    ];
    listCbuCkd: { value: string, label: string }[] = [
        { value: "Y", label: this.l("CBU")},
        { value: "N", label: this.l("CKD")},
        { value: "L", label: this.l("Lexus")},
    ];
    listAudio: { value: string, label: string }[] = [
        { value: undefined , label: this.l("")},
        { value: "Y", label: this.l("Yes")},
        { value: "N", label: this.l("No")},
    ];
    listIsFirmColor: { value: string, label: string }[] = [
        { value: undefined , label: this.l("")},
        { value: "Y", label: this.l("Yes")},
        { value: "N", label: this.l("No")},
    ];
    fromDate: Date;
    toDate: Date;

    constructor(
        injector: Injector,
        private _localStorage: LocalStorageService,
        private _mstSleGradeProducesServiceProxy: MstSleGradeProductionServiceProxy
    ) {
        super(injector);
    }
    
    ngOnInit() {
        this._localStorage.get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList).lookUp.filter((r) => r.code === LookUpKeys.gasolineType).forEach(e => this.gasolineTypes.push({ value: e.id, label: e.name}));
    }

    show(mstSleGradeProducesId?: number): void {
        this.fromDate = null;
        this.toDate = null;

        if (!mstSleGradeProducesId) {
            this._mstSleGradeProducesServiceProxy.getGradeProductionCode(mstSleGradeProducesId).subscribe((result) => {
                this.gradeProductions = result;
            });
            this.mstSleGradeProduces = new CreateOrEditMstSleGradeProductionDto();
            this.active = true;
            this.modal.show();
        } else {
            this._mstSleGradeProducesServiceProxy.getGradeProductionCode(mstSleGradeProducesId).subscribe((result) => {
                this.gradeProductions = result;
            });
            this._mstSleGradeProducesServiceProxy
                .getMstSleGradeProducesForEdit(mstSleGradeProducesId)
                .subscribe((result) => {
                    this.mstSleGradeProduces = result.mstSleGradeProduces;

                    if (this.mstSleGradeProduces.fromDate) {
                        this.fromDate = this.mstSleGradeProduces.fromDate.toDate();
                    }
                    if (this.mstSleGradeProduces.toDate) {
                        this.toDate = this.mstSleGradeProduces.toDate.toDate();
                    }
                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        this.mstSleGradeProduces.gradeId = this.gradeIdPick;

        var checkProductionCode: boolean;

        this.gradeProductions.forEach(element => {
            if (this.mstSleGradeProduces.productionCode === element.productionCode) {
                checkProductionCode = false;
            }
        });

        if(checkProductionCode == false){
            this.saving = false;
            this.message.error(this.l("MstSleModelProductionCode"));
        } else {
            if (this.fromDate) {
                if (!this.mstSleGradeProduces.fromDate) {
                    this.mstSleGradeProduces.fromDate = moment(
                        this.fromDate
                    ).startOf("day");
                } else {
                    this.mstSleGradeProduces.fromDate = moment(this.fromDate);
                }
            } else {
                this.mstSleGradeProduces.fromDate = null;
            }
            if (this.toDate) {
                if (!this.mstSleGradeProduces.toDate) {
                    this.mstSleGradeProduces.toDate = moment(this.toDate).startOf("day");
                } else {
                    this.mstSleGradeProduces.toDate = moment(this.toDate);
                }
            } else {
                this.mstSleGradeProduces.toDate = null;
            }
            this._mstSleGradeProducesServiceProxy
                .createOrEdit(this.mstSleGradeProduces)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.info(this.l("SavedSuccessfully"));
                    this.close();
                    this.modalSave.emit(null);
                });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
