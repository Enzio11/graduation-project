import { Component, ViewChild, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { GetMstSleGradeProducesForViewDto, GetMstSleGradesForViewDto, GetMstSleModelsForViewDto, MstSleGradeProductionServiceProxy, MstSleGradeServiceProxy, MstSleLookupDto, MstSleModelServiceProxy } from "@shared/service-proxies/service-proxies";
import { ceil } from "lodash";
import { AgCheckboxRendererComponent } from "@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component";
import { DataFormatService } from "@app/shared/common/services/data-format.service";
import { CreateOrEditMstSleGradeProducesModalComponent } from "./create-or-edit-mstSleGradeProductions-modal/create-or-edit-mstSleGradeProduces-modal.component";
import { CreateOrEditMstSleGradesModalComponent } from "./create-or-edit-mstSleGrades-modal/create-or-edit-mstSleGrades-modal.component";
import { CreateOrEditMstSleModelsModalComponent } from "./create-or-edit-mstSleModels-modal/create-or-edit-mstSleModels-modal.component";
import { GridTableService } from "@app/shared/common/services/grid-table.service";
import { LookUpKeys } from "@app/shared/constants/lookUpKeys";
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { StorageKeys } from "@app/shared/constants/storageKeys";
import { LocalStorageService } from "@shared/utils/local-storage.service";
import { MstSleOtherMakeComponent } from "./mst-sle-other-make/mst-sle-other-make.component";

@Component({
    templateUrl: "./mst-sle-model.component.html",
    styleUrls: ["./mst-sle-model.component.less"],
})

export class MstSleModelComponent extends AppComponentBase {
    @ViewChild("createOrEditMstSleGradesModal", { static: true }) createOrEditMstSleGradesModal: CreateOrEditMstSleGradesModalComponent;
    @ViewChild("createOrEditMstSleModelsModal", { static: true }) createOrEditMstSleModelsModal: CreateOrEditMstSleModelsModalComponent;
    @ViewChild("createOrEditMstSleGradeProducesModal", { static: true }) createOrEditMstSleGradeProducesModal: CreateOrEditMstSleGradeProducesModalComponent;
    @ViewChild("otherMakeComponent", { static: true }) otherMakeComponent: MstSleOtherMakeComponent;

    toyotaSelected: boolean = true;

    defaultColDef = {
        flex: 1,
        floatingFilter: false,
        filter: "agTextColumnFilter",
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };
    frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent,
    };

    areShownGradeProduction: boolean = false;
    areShownGradeMarketing: boolean = true;
    advancedFiltersAreShown: boolean = false;
    filterText: string = "";
    marketingCodeFilter: string = "";
    productionCodeFilter: string = "";
    vnNameFilter: string = "";
    enNameFilter: string = "";
    descriptionFilter: string = "";
    statusFilter: string = "Y";
    abbreviationFilter: string = "";

    //Grade
    modelPaginationParams: PaginationParamsModel;
    gPaginationParams: PaginationParamsModel;
    gradeFilter: string = "";
    modelsColumnDefs: CustomColDef[];
    gradesColumnDefs: CustomColDef[];
    selectedModel: GetMstSleModelsForViewDto = new GetMstSleModelsForViewDto();
    sorting: string = "";
    isLoading: boolean = false;
    listM: GetMstSleModelsForViewDto[] = [];
    listG: GetMstSleGradesForViewDto[] = [];
    totalPages: number;
    modelParams!: GridParams;
    selectedGrade: GetMstSleGradesForViewDto = new GetMstSleGradesForViewDto();
    gradeParams!: GridParams;
    maxResultCount: number = 10;
    mData: GetMstSleModelsForViewDto[] = [];
    gData: GetMstSleGradesForViewDto[] = [];

    selectModelId: number;
    selectGradeId: number;
    modelIdsend: number;
    gasolineTypes: MstSleLookupDto[] = [];
    paginationProductionParams: PaginationParamsModel;

    //Grade Production
    productSorting: string = "";
    selectedRowProduction: GetMstSleGradeProducesForViewDto = new GetMstSleGradeProducesForViewDto();
    productData: GetMstSleGradeProducesForViewDto[] = [];
    gradeIdSend: number;
    productTables: CustomColDef[];
    selectProductionId: number;
    skipCount: number = 0;
    makeIdFilter: number = 1 //*chỉ lấy xe thuộc Toyota

    constructor(
        injector: Injector,
        private _mstSleGradesServiceProxy: MstSleGradeServiceProxy,
        private _mstSleModelsServiceProxy: MstSleModelServiceProxy,
        private _localStorage: LocalStorageService,
        private _mstSleGradeProducesServiceProxy: MstSleGradeProductionServiceProxy,
        private dataFormatService: DataFormatService,
        private gridTableService: GridTableService
    ) {
        super(injector);
        this.modelsColumnDefs = [
            {
                headerName: this.l("MarketingCode"),
                headerTooltip: this.l("MarketingCode"),
                field: "marketingCode",
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("ProductionCode"),
                headerTooltip: this.l("ProductionCode"),
                field: "productionCode",
                cellClass: ["text-left"],
                flex: 2,
            },
            {
                headerName: this.l("MstGenDealersAbbreviation"),
                headerTooltip: this.l("MstGenDealersAbbreviation"),
                field: "abbreviation",
                cellClass: ["text-left"],
                flex: 2,
            },
            {
                headerName: this.l("EnName"),
                headerTooltip: this.l("EnName"),
                field: "enName",
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("VnName"),
                headerTooltip: this.l("VnName"),
                field: "vnName",
                flex: 2,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("Ordering"),
                headerTooltip: this.l("Ordering"),
                field: "ordering",
                cellClass: ["text-right"],
                flex: 1,
            },
            {
                headerName: this.l("Description"),
                headerTooltip: this.l("Description"),
                field: "description",
                flex: 3,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("Status"),
                headerTooltip: this.l("Status"),
                field: "status",
                flex: 1,
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
            },
            {
                headerName: this.l("MarketingCodeWeb"),
                headerTooltip: this.l("MarketingCodeWeb"),
                field: "marketingCodeWeb",
                flex: 2,
                cellClass: ["text-left"],
            },
        ];

        this.gradesColumnDefs = [
            {
                headerName: this.l("MarketingCode"),
                headerTooltip: this.l("MarketingCode"),
                field: "marketingCode",
                minWidth: 125,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("EnName"),
                headerTooltip: this.l("EnName"),
                field: "enName",
                minWidth: 175,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("VnName"),
                headerTooltip: this.l("VnName"),
                field: "vnName",
                minWidth: 175,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("CbuCkd"),
                headerTooltip: this.l("CbuCkd"),
                field: "cbuCkd",
                minWidth: 125,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l("MstSleColorByGradeSeatNo"),
                headerTooltip: this.l("MstSleColorByGradeSeatNo"),
                field: "seatNo",
                minWidth: 125,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l("IsFirmColor"),
                headerTooltip: this.l("IsFirmColor"),
                field: "isFirmColor",
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
                minWidth: 125,
            },
            {
                headerName: this.l("IsHasAudio"),
                headerTooltip: this.l("IsHasAudio"),
                field: "isHasAudio",
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
                minWidth: 125,
            },
            {
                headerName: this.l("GasolineType"),
                headerTooltip: this.l("GasolineType"),
                field: "gasolineTypeId",
                minWidth: 150,
                valueGetter: (params) =>
                    this.gasolineTypes.find(
                        (p) => p.value == params.data.gasolineTypeId
                    )
                        ? this.gasolineTypes.find(
                              (p) => p.value == params.data.gasolineTypeId
                          ).name
                        : null,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("InsurancePaymentLoad"),
                headerTooltip: this.l("InsurancePaymentLoad"),
                field: "insurancePayLoad",
                cellClass: ["text-right"],
                minWidth: 100,
            },
            {
                headerName: this.l("Ordering"),
                headerTooltip: this.l("Ordering"),
                field: "ordering",
                cellClass: ["text-right"],
                minWidth: 100,
            },
            {
                headerName: this.l("Status"),
                headerTooltip: this.l("Status"),
                field: "status",
                minWidth: 125,
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
            },
            {
                headerName: this.l("MstSleGradeIsPickup"),
                headerTooltip: this.l("MstSleGradeIsPickup"),
                field: "isPickup",
                minWidth: 125,
                cellRenderer: "agCheckboxRendererComponent",
                data: [true, false],
                cellClass: ["text-center"],
                disableCheckbox: true,
            },
        ];

        this.productTables = [
            {
                headerName: this.l("ProductionCode"),
                headerTooltip: this.l("ProductionCode"),
                field: "productionCode",
                minWidth: 150,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("ShortModel"),
                headerTooltip: this.l("ShortModel"),
                field: "shortModel",
                minWidth: 125,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("FullModel"),
                headerTooltip: this.l("FullModel"),
                field: "fullModel",
                minWidth: 125,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("FrameNoLength"),
                headerTooltip: this.l("FrameNoLength"),
                field: "frameNoLength",
                minWidth: 150,
                cellClass: ["text-right"],
            },
            {
                headerName: this.l("Wmi"),
                headerTooltip: this.l("Wmi"),
                field: "wmi",
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("Vds"),
                headerTooltip: this.l("Vds"),
                field: "vds",
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("CbuCkd"),
                headerTooltip: this.l("CbuCkd"),
                field: "cbuCkd",
                minWidth: 125
            },
            {
                headerName: this.l("Fuel"),
                headerTooltip: this.l("Fuel"),
                field: "gasolineTypeId",
                minWidth: 100,
                cellClass: ["text-left"],
                valueGetter: (params) =>
                    this.gasolineTypes.find(
                        (p) => p.value == params.data.gasolineTypeId
                    )
                        ? this.gasolineTypes.find(
                              (p) => p.value == params.data.gasolineTypeId
                          ).name
                        : null,
            },
            {
                headerName: this.l("IsHasAudio"),
                headerTooltip: this.l("IsHasAudio"),
                field: "isHasAudio",
                minWidth: 125,
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
            },
            {
                headerName: this.l("IsFirmColor"),
                headerTooltip: this.l("IsFirmColor"),
                field: "isFirmColor",
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
                minWidth: 125,
            },
            {
                headerName: this.l("Ordering"),
                headerTooltip: this.l("Ordering"),
                field: "ordering",
                cellClass: ["text-right"],
                minWidth: 100,
            },
            {
                headerName: this.l("FromDate"),
                headerTooltip: this.l("FromDate"),
                field: "fromDate",
                minWidth: 125,
                cellClass: ["text-right"],
                valueFormatter: (params) =>
                    this.dataFormatService.dateFormat(params.value),
            },
            {
                headerName: this.l("ToDate"),
                headerTooltip: this.l("ToDate"),
                field: "toDate",
                minWidth: 125,
                cellClass: ["text-right"],
                valueFormatter: (params) =>
                    this.dataFormatService.dateFormat(params.value),
            },
            {
                headerName: this.l("Status"),
                headerTooltip: this.l("Status"),
                field: "status",
                minWidth: 125,
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
            }
        ];
    }

    ngOnInit() {
        this.modelPaginationParams = { pageNum: 1, pageSize: 10, totalCount: 0 };
        this.gPaginationParams = { pageNum: 1, pageSize: 10, totalCount: 0 };
        this.paginationProductionParams =  { pageNum: 1, pageSize: 10, totalCount: 0 };

        this.gasolineTypes = this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.gasolineType);
        this.productData = [];
        this.gData = [];
    }

    getModel(modelPaginationParams?) {
        return this._mstSleModelsServiceProxy.getAll(
            this.filterText,
            this.marketingCodeFilter,
            this.productionCodeFilter,
            this.abbreviationFilter,
            this.vnNameFilter,
            this.enNameFilter,
            this.descriptionFilter,
            this.statusFilter,
            this.makeIdFilter,
            this.sorting ?? null,
            modelPaginationParams ? modelPaginationParams.skipCount : 0,
            modelPaginationParams ? modelPaginationParams.pageSize : 10
        );
    }

    onGridReady(modelPaginationParams) {
        this.modelPaginationParams = modelPaginationParams;
        this.modelPaginationParams.skipCount =
            (modelPaginationParams.pageNum - 1) * modelPaginationParams.pageSize;

        this.getModel(this.modelPaginationParams).subscribe(
            (result) => {
                this.isLoading = false;
                this.listM = result.items;
                this.totalPages = ceil(
                    result.totalCount / this.modelPaginationParams.pageSize
                );
                this.modelPaginationParams.totalCount = result.totalCount;
                this.modelPaginationParams.totalPage = this.totalPages;
            },
            (err) => this.notify.error(err.error),
            () => {
                this.modelParams.api.setRowData(this.listM);
                this.gridTableService.selectFirstRow(this.modelParams.api);
                setTimeout(() => {
                    this.gridTableService.setFocusCell(
                        this.modelParams.api,
                        this.modelsColumnDefs[0].field
                    );
                }, 50);
            }
        );
    }

    onChangeModelSelection(params) {
        this.selectGradeId = undefined;
        this.selectedModel = params.api.getSelectedRows()[0];
        if (this.selectedModel) {
            this.selectModelId = this.selectedModel.id;
            this.gPaginationParams =  { pageNum: 1, pageSize: 10, totalCount: 0 };
            this.refreshGradesList();
            this.productData = [];
        }
    }

    onChangeGradeSelection(params) {
        this.selectProductionId = undefined;
        this.selectedGrade = params.api.getSelectedRows()[0];
        if (this.selectedGrade) {
            this.selectGradeId = this.selectedGrade.id;
            this.refreshProductList();
        }
    }

    refreshGradesList(gPaginationParams?) {
        this.selectGradeId = undefined;
        this._mstSleGradesServiceProxy
            .getAllGradeByModels(
                this.selectModelId,
                this.gradeFilter,
                this.sorting ?? null,
                gPaginationParams ? gPaginationParams.skipCount : 0,
                gPaginationParams ? gPaginationParams.pageSize : 10
            )
            .subscribe((result) => {
                this.gData = result.items;
                this.totalPages = ceil(result.totalCount / this.maxResultCount);
                this.gPaginationParams.totalCount = result.totalCount;
                this.gPaginationParams.totalPage = this.totalPages;
            });
        this.modelIdsend = this.selectModelId;
    }
    callBackModelGrid(params) {
        this.modelParams = params;
        this.modelParams.api.sizeColumnsToFit();
        this.modelParams.api.paginationSetPageSize(
            this.modelPaginationParams.pageSize
        );
        this.onGridReady(this.modelPaginationParams);
    }

    changeMPage(modelPaginationParams) {
        this.modelPaginationParams = modelPaginationParams;
        this.modelPaginationParams.skipCount =
            (modelPaginationParams.pageNum - 1) * modelPaginationParams.pageSize;
        this.maxResultCount = modelPaginationParams.pageSize;

        this.getModel(this.modelPaginationParams).subscribe((result) => {
            this.listM = result.items;
            this.mData = result.items;
            this.modelPaginationParams.totalPage = ceil(
                result.totalCount / this.maxResultCount
            );
        });
    }
    changeGPage(gPaginationParams) {
        this.gPaginationParams = gPaginationParams;
        this.gPaginationParams.skipCount =
            (gPaginationParams.pageNum - 1) * gPaginationParams.pageSize;
        this.maxResultCount = gPaginationParams.pageSize;

        this.refreshGradesList(this.gPaginationParams);
    }
    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }
    eventEnterGrade(event) {
        if (event.keyCode === 13) {
            this.gPaginationParams =  { pageNum: 1, pageSize: 10, totalCount: 0 };
            this.refreshGradesList();
        }
    }

    search() {
        this.modelPaginationParams = { pageNum: 1, pageSize: 10, totalCount: 0 };
        this.modelParams.api.paginationSetPageSize(
            this.modelPaginationParams.pageSize
        );
        this.onGridReady(this.modelPaginationParams);
    }

    createMstSleGrades(): void {
        this.createOrEditMstSleGradesModal.show();
    }

    editMstSleGrades(): void {
        this.createOrEditMstSleGradesModal.show(this.selectGradeId);
    }

    deleteMstSleGrades(): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleGradesServiceProxy
                    .delete(this.selectGradeId)
                    .subscribe(() => {
                        this.refreshGradesList();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    createMstSleModels(): void {
        this.createOrEditMstSleModelsModal.show(undefined, 1);
    }

    editMstSleModels(): void {
        this.createOrEditMstSleModelsModal.show(this.selectModelId, 1);
    }

    deleteMstSleModels(): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleModelsServiceProxy
                    .delete(this.selectModelId)
                    .subscribe(() => {
                        this.callBackModelGrid(this.modelParams);
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    refreshProductList(paginationProductionParams?) {
        this.selectProductionId = undefined;
        this._mstSleGradeProducesServiceProxy
            .getGradesproduceByGradesId(
                this.selectGradeId,
                this.productSorting ?? null,
                paginationProductionParams
                    ? paginationProductionParams.skipCount
                    : 0,
                paginationProductionParams
                    ? paginationProductionParams.pageSize
                    : 10
            )
            .subscribe((result) => {
                this.productData = result.items;
                this.totalPages = ceil(result.totalCount / this.maxResultCount);
                this.paginationProductionParams.totalCount = result.totalCount;
                this.paginationProductionParams.totalPage = this.totalPages;
            });
        this.gradeIdSend = this.selectGradeId;
    }

    getAllGradesProduceByGradeId(paginationProductionParams?) {
        return this._mstSleGradeProducesServiceProxy.getGradesproduceByGradesId(
            this.selectGradeId,
            this.productSorting ?? null,
            paginationProductionParams ? paginationProductionParams.skipCount : 0,
            paginationProductionParams ? paginationProductionParams.pageSize : 10
        );
    }

    changeProductPage(params) {
        this.paginationProductionParams = params;
        this.paginationProductionParams.skipCount =
            (params.pageNum - 1) * params.pageSize;
        this.maxResultCount = params.pageSize;
        this.getAllGradesProduceByGradeId(
            this.paginationProductionParams
        ).subscribe((result) => {
            this.productData = result.items;
            this.paginationProductionParams.totalPage = ceil(
                result.totalCount / this.maxResultCount
            );
        });
    }

    onChangeProductSelection(params) {
        this.selectedRowProduction = params.api.getSelectedRows()[0];
        if (this.selectedRowProduction) this.selectProductionId = this.selectedRowProduction.id;
    }

    createMstSleGradeProduces(): void {
        this.createOrEditMstSleGradeProducesModal.show();
    }

    editMstSleGradesProduces(): void {
        this.createOrEditMstSleGradeProducesModal.show(this.selectProductionId);
    }

    deleteMstSleGradeProduces(): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleGradeProducesServiceProxy
                    .delete(this.selectProductionId)
                    .subscribe(() => {
                        this.refreshProductList();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

    clearValueFilter() {
        this.filterText = "";
        this.marketingCodeFilter = "";
        this.productionCodeFilter = "";
        this.abbreviationFilter = "";
        this.vnNameFilter = "";
        this.enNameFilter = "";
        this.descriptionFilter = "";
        this.statusFilter = "Y";
        this.onGridReady(this.modelPaginationParams);
    }

    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        this.clearValueFilter();
    }

    selectToyotaModelTab(){
        this.toyotaSelected = true;
    }

    selectOtherModelTab(){
        this.toyotaSelected = false;
    }
}
