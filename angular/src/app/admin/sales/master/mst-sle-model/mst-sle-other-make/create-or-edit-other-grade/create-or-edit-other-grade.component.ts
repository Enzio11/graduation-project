import { Component, ViewChild, Injector, Output, EventEmitter, Input} from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';
import { MstSleGradeServiceProxy, CreateOrEditMstSleGradesDto, MstSleLookupDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as moment from 'moment';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';

@Component({
    selector: "create-or-edit-other-grade",
    templateUrl: "./create-or-edit-other-grade.component.html",
    styleUrls: ["./create-or-edit-other-grade.component.less"],
})

export class CreateOrEditMstSleOtherGradesModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() modelIdPick: number;

    active: boolean = false;
    saving: boolean = false;

    mstSleGrades: CreateOrEditMstSleGradesDto = new CreateOrEditMstSleGradesDto();
    modelId : number;
    fromDate: Date;
    toDate: Date;
    listStatus = [
        { value: "Y", label: "Còn hiệu lực" },
        { value: "N", label: "Hết hiệu lực" }
    ]
    gasolineTypes: any[] =[];
    // gasolineTypes: MstSleLookupDto[] = [];
    grades = [];

    constructor(
        injector: Injector,
        private _mstSleGradesServiceProxy: MstSleGradeServiceProxy,
        private _localStorage: LocalStorageService
    ) {
        super(injector);
    }

    ngOnInit() {
        this._localStorage.get('tmss/abpzerotemplate_local_storage/' + StorageKeys.pickList).lookUp.filter((r) => r.code === LookUpKeys.gasolineType).forEach(e => this.gasolineTypes.push({ value: e.id, label: e.name}));
    }

    show(mstSleGradesId?: number): void {
        this.fromDate = null;
        this.toDate = null;

        if (!mstSleGradesId) {

            this.mstSleGrades = new CreateOrEditMstSleGradesDto();
            this.mstSleGrades.id = mstSleGradesId;

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleGradesServiceProxy
                .getMstSleGradesForEdit(mstSleGradesId)
                .subscribe((result) => {
                    this.mstSleGrades = result.mstSleGrades;

                    if (this.mstSleGrades.fromDate) {
                        this.fromDate = this.mstSleGrades.fromDate.toDate();
                    }
                    if (this.mstSleGrades.toDate) {
                        this.toDate = this.mstSleGrades.toDate.toDate();
                    }

                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        var checkProductionCode: boolean;
        this.mstSleGrades.modelId = this.modelIdPick;
        this.grades.forEach(element => {
            if (this.mstSleGrades.productionCode === element.productionCode) {
                checkProductionCode = false;
            }
        });

        if(checkProductionCode == false){
            this.saving = false;
            this.message.error(this.l("MstSleModelProductionCode"));
        } else {
            if (this.fromDate) {
                if (!this.mstSleGrades.fromDate) {
                    this.mstSleGrades.fromDate = moment(this.fromDate).startOf("day");
                } else {
                    this.mstSleGrades.fromDate = moment(this.fromDate);
                }
            } else {
                this.mstSleGrades.fromDate = null;
            }
            if (this.toDate) {
                if (!this.mstSleGrades.toDate) {
                    this.mstSleGrades.toDate = moment(this.toDate).startOf("day");
                } else {
                    this.mstSleGrades.toDate = moment(this.toDate);
                }
            } else {
                this.mstSleGrades.toDate = null;
            }

            this._mstSleGradesServiceProxy
                .createOrEdit(this.mstSleGrades)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.info(this.l("SavedSuccessfully"));
                    this.close();
                    this.modalSave.emit(null);
                });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
