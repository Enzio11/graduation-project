import { Component, Injector, ViewChild } from '@angular/core';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { AppComponentBase } from '@shared/common/app-component-base';
import { GetMstSleGradesForViewDto, GetMstSleModelsForViewDto, MstSleGradeServiceProxy, MstSleLookupDto, MstSleMakeCompetitorDto, MstSleModelServiceProxy } from '@shared/service-proxies/service-proxies';
import { LocalStorageService } from '@shared/utils/local-storage.service';
import { AbpSessionService } from 'abp-ng2-module';
import { ceil } from 'lodash';
import { CreateOrEditMstSleModelsModalComponent } from '../create-or-edit-mstSleModels-modal/create-or-edit-mstSleModels-modal.component';
import { CreateOrEditMstSleOtherGradesModalComponent } from './create-or-edit-other-grade/create-or-edit-other-grade.component';

@Component({
	selector: 'mst-sle-other-make',
	templateUrl: './mst-sle-other-make.component.html',
	styleUrls: ['./mst-sle-other-make.component.less']
})

export class MstSleOtherMakeComponent extends AppComponentBase {
	@ViewChild('createOrEditMstSleModelsModal') createOrEditMstSleModelsModal: CreateOrEditMstSleModelsModalComponent;
	@ViewChild("createOrEditMstSleGradesModal", { static: true }) createOrEditMstSleGradesModal: CreateOrEditMstSleOtherGradesModalComponent;

	isTMV: boolean = false;
	columnDefs: CustomColDef[] = [];
	columnDefsModel: CustomColDef[] = [];
	rowData: MstSleMakeCompetitorDto[] = [];
	advancedFiltersAreShown: boolean = false;
	rowDataModel: GetMstSleModelsForViewDto[] = [];
	totalPagesModel: number;
	areShownGradeMarketing: boolean = true;
	gradesColumnDefs: CustomColDef[];
	gasolineTypes: MstSleLookupDto[] = [];

	defaultColDef = {
		flex: 1,
		floatingFilter: false,
		filter: 'agTextColumnFilter',
		resizable: true,
		sortable: true,
		floatingFilterComponentParams: { suppressFilterButton: true },
		textFormatter: function (r) {
			if (r == null) return null;
			return r.toLowerCase();
		},
	};

	frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent,
    };

	dealerId: number;
	code: string = "";
	name: string = "";
	vnName: string = "";
	maxResultCount: number = 20;
	skipCount: number = 0;
	sorting: string = "";
	totalPages: number = 0;
	paginationParams: PaginationParamsModel = { pageNum: 1, pageSize: 10, totalCount: 0 };
	paginationParamsModel: PaginationParamsModel = { pageNum: 1, pageSize: 10, totalCount: 0 };
	params!: GridParams;
	paramsModel!: GridParams;

	makeCompetitorDto: MstSleMakeCompetitorDto = new MstSleMakeCompetitorDto();

	selectedModel: GetMstSleModelsForViewDto = new GetMstSleModelsForViewDto();
	seletedModelId: number;

	seletedId: number;
	filterText: string = "";
    marketingCodeFilter: string = "";
    productionCodeFilter: string = "";
    vnNameFilter: string = "";
    enNameFilter: string = "";
    descriptionFilter: string = "";
    statusFilter: string = "Y";
    abbreviationFilter: string = "";
	makeIdFilter: number;

	sortingModel: string = "";
	skipCountModel: number = 0;
	maxResultCountModel: number = 20;

	gPaginationParams: PaginationParamsModel= { pageNum: 1, pageSize: 10, totalCount: 0 };
	selectedGrade: GetMstSleGradesForViewDto = new GetMstSleGradesForViewDto();
    gradeParams!: GridParams;
	gradeFilter: string = "";
	gData: GetMstSleGradesForViewDto[] = [];
	selectGradeId: number;
	modelIdsend: number;

	constructor(
		injector: Injector,
		private _mstSleModelsServiceProxy: MstSleModelServiceProxy,
		private _sessionService: AbpSessionService,
		private _localStorage: LocalStorageService,
		private _mstSleGradesServiceProxy: MstSleGradeServiceProxy,
	) {
		super(injector);
		this.columnDefs = [
			{
				headerName: this.l('MstSleMakeCompetitorCode'),
				headerTooltip: this.l('MstSleMakeCompetitorCode'),
				field: 'code',
				flex: 2,
				cellClass: ["text-left"],
			},
			{
				headerName: this.l('MstSleMakeCompetitorDescription'),
				headerTooltip: this.l('MstSleMakeCompetitorDescription'),
				field: 'description',
				flex: 5,
				cellClass: ["text-left"],
			},
		];

		this.columnDefsModel = [
			{
				headerName: this.l("MarketingCode"),
				headerTooltip: this.l("MarketingCode"),
				field: "marketingCode",
				flex: 2,
				cellClass: ["text-left"],
			},
			{
				headerName: this.l("ProductionCode"),
				headerTooltip: this.l("ProductionCode"),
				field: "productionCode",
				cellClass: ["text-left"],
				flex: 2,
			},
			{
				headerName: this.l("MstGenDealersAbbreviation"),
				headerTooltip: this.l("MstGenDealersAbbreviation"),
				field: "abbreviation",
				cellClass: ["text-left"],
				flex: 2,
			},
			{
				headerName: this.l("EnName"),
				headerTooltip: this.l("EnName"),
				field: "enName",
				flex: 3,
				cellClass: ["text-left"],
			},
			{
				headerName: this.l("VnName"),
				headerTooltip: this.l("VnName"),
				field: "vnName",
				flex: 3,
				cellClass: ["text-left"],
			},
			{
				headerName: this.l("Ordering"),
				headerTooltip: this.l("Ordering"),
				field: "ordering",
				cellClass: ["text-right"],
				flex: 1,
			},
			{
				headerName: this.l("Description"),
				headerTooltip: this.l("Description"),
				field: "description",
				flex: 3,
				cellClass: ["text-left"],
			},
			{
				headerName: this.l("Status"),
				headerTooltip: this.l("Status"),
				field: "status",
				flex: 1,
				cellRenderer: "agCheckboxRendererComponent",
				data: ["Y", "N"],
				cellClass: ["text-center"],
				disableCheckbox: true,
			},
		];

		this.gradesColumnDefs = [
            {
                headerName: this.l("MarketingCode"),
                headerTooltip: this.l("MarketingCode"),
                field: "marketingCode",
                minWidth: 125,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("EnName"),
                headerTooltip: this.l("EnName"),
                field: "enName",
                minWidth: 175,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("VnName"),
                headerTooltip: this.l("VnName"),
                field: "vnName",
                minWidth: 175,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("MstSleColorByGradeSeatNo"),
                headerTooltip: this.l("MstSleColorByGradeSeatNo"),
                field: "seatNo",
                minWidth: 125,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l("GasolineType"),
                headerTooltip: this.l("GasolineType"),
                field: "gasolineTypeId",
                minWidth: 150,
                valueGetter: (params) =>
                    this.gasolineTypes.find(
                        (p) => p.value == params.data.gasolineTypeId
                    )
                        ? this.gasolineTypes.find(
                              (p) => p.value == params.data.gasolineTypeId
                          ).name
                        : null,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("InsurancePaymentLoad"),
                headerTooltip: this.l("InsurancePaymentLoad"),
                field: "insurancePayLoad",
                cellClass: ["text-right"],
                minWidth: 100,
            },
            {
                headerName: this.l("Ordering"),
                headerTooltip: this.l("Ordering"),
                field: "ordering",
                cellClass: ["text-right"],
                minWidth: 100,
            },
            {
                headerName: this.l("Status"),
                headerTooltip: this.l("Status"),
                field: "status",
                minWidth: 125,
                cellRenderer: "agCheckboxRendererComponent",
                data: ["Y", "N"],
                cellClass: ["text-center"],
                disableCheckbox: true,
            },
        ];
	}

	ngOnInit() {
		if (this._sessionService.tenantId == null) {
			this.isTMV = true;
		}

		this.gasolineTypes = this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.gasolineType);
	}

	onGridReady(paginationParams) {
		this.seletedId = undefined;
		this.seletedModelId = undefined;

		this.rowData = [];
		this.paginationParams = paginationParams;
		this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
		this.maxResultCount = paginationParams.pageSize;

		this.GetAllMake();
	}

	GetAllMake() {
		this._mstSleModelsServiceProxy.getAllMstSleMakeCompetitor(
            this.filterText,
            this.sorting ?? null,
            this.skipCount,
            this.maxResultCount
        ).subscribe((result) => {
			this.rowData = result.items;
			this.totalPages = ceil(result.totalCount / this.maxResultCount);
			this.paginationParams.totalCount = result.totalCount;
			this.paginationParams.totalPage = this.totalPages;
		});
	}

	callBackGrid(params) {
		this.params = params;
		this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
		this.onGridReady(this.paginationParams);
	}

	changePage(paginationParams) {
		this.paginationParams = paginationParams;
		this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
		this.maxResultCount = paginationParams.pageSize;
		this.GetAllMake();
	}

	eventEnter(event) {
		if (event.keyCode === 13) {
			this.search()
		}
	}

	search() {
		this.onGridReady(this.paginationParams);
	}

	onChangeSelection(params) {
		this.makeCompetitorDto = params.api.getSelectedRows()[0];
		if (this.makeCompetitorDto) {
			this.seletedId = this.makeCompetitorDto.id;
		}

		this.onGridReadyModel(this.paginationParamsModel);
	}

	callBackGridModel(params) {
		this.paramsModel = params;
		this.paramsModel.api.paginationSetPageSize(this.paginationParamsModel.pageSize);
	}

	changePageModel(paginationParams) {
		this.paginationParamsModel = paginationParams;
		this.paginationParamsModel.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
		this.maxResultCountModel = paginationParams.pageSize;
		this.getAllModelByMakeId();
	}

	onChangeSelectionModel(params) {
		this.selectedModel = params.api.getSelectedRows()[0];
		if (this.selectedModel) {
			this.seletedModelId = this.selectedModel.id;
		}
		this.modelIdsend = this.seletedModelId;

		this.refreshGradesList(this.gPaginationParams)
	}

	onGridReadyModel(paginationParams) {
		this.rowDataModel = [];
		this.paginationParamsModel = paginationParams;
		this.paginationParamsModel.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
		this.maxResultCount = paginationParams.pageSize;

		this.getAllModelByMakeId();
	}

	getAllModelByMakeId() {
		this._mstSleModelsServiceProxy.getAllModelFromOtherMake(
            this.filterText,
            this.marketingCodeFilter,
            this.productionCodeFilter,
            this.abbreviationFilter,
            this.vnNameFilter,
            this.enNameFilter,
            this.descriptionFilter,
            this.statusFilter,
            this.seletedId,
            this.sortingModel ?? null,
            this.skipCountModel,
            this.maxResultCountModel
        )
		.subscribe((result) => {
			this.rowDataModel = result.items;
			this.totalPagesModel = ceil(result.totalCount / this.maxResultCountModel);
			this.paginationParamsModel.totalCount = result.totalCount;
			this.paginationParamsModel.totalPage = this.totalPagesModel;
		});
	}

	create() {
		this.createOrEditMstSleModelsModal.show(undefined, this.seletedId);
	}

	edit() {
		this.createOrEditMstSleModelsModal.show(this.seletedModelId, this.seletedId);
	}

	delete() {
		this.message.confirm('', this.l('AreYouSure'), (isConfirmed) => {
			if (isConfirmed) {
				this._mstSleModelsServiceProxy
					.delete(this.seletedModelId)
					.subscribe(() => {
						this.onGridReady(this.paginationParams);
						this.notify.success(this.l('SuccessfullyDeleted'));
					});
			}
		});
	}

	onChangeGradeSelection(params) {
        this.selectedGrade = params.api.getSelectedRows()[0];
        if (this.selectedGrade) {
            this.selectGradeId = this.selectedGrade.id;
        }
    }

    refreshGradesList(gPaginationParams?) {
        this.selectGradeId = undefined;
        this._mstSleGradesServiceProxy
            .getAllGradeByModels(
                this.seletedModelId,
                this.gradeFilter,
                this.sorting ?? null,
                gPaginationParams ? gPaginationParams.skipCount : 0,
                gPaginationParams ? gPaginationParams.pageSize : 10
            )
            .subscribe((result) => {
                this.gData = result.items;
                this.totalPages = ceil(result.totalCount / this.maxResultCount);
                this.gPaginationParams.totalCount = result.totalCount;
                this.gPaginationParams.totalPage = this.totalPages;
            });
        this.modelIdsend = this.seletedModelId;
    }

	createMstSleGrades(): void {
        this.createOrEditMstSleGradesModal.show();
    }

    editMstSleGrades(): void {
        this.createOrEditMstSleGradesModal.show(this.selectGradeId);
    }

    deleteMstSleGrades(): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstSleGradesServiceProxy
                    .delete(this.selectGradeId)
                    .subscribe(() => {
                        this.refreshGradesList();
                        this.notify.success(this.l("SuccessfullyDeleted"));
                    });
            }
        });
    }

	changeGPage(gPaginationParams) {
        this.gPaginationParams = gPaginationParams;
        this.gPaginationParams.skipCount =
            (gPaginationParams.pageNum - 1) * gPaginationParams.pageSize;
        this.maxResultCount = gPaginationParams.pageSize;

        this.refreshGradesList(this.gPaginationParams);
    }
}
