import { Component, ViewChild, Output, EventEmitter, Injector } from "@angular/core";
import { AppComponentBase } from "@shared/common/app-component-base";
import { ModalDirective } from "ngx-bootstrap/modal";
import { CreateOrEditMstSleModelsDto, MstSleModelsDto, MstSleModelServiceProxy } from "@shared/service-proxies/service-proxies";
import { finalize } from "rxjs/operators";

@Component({
    selector: "create-or-edit-mst-sle-models-modal",
    templateUrl: "./create-or-edit-mstSleModels-modal.component.html",
    styleUrls: ["./create-or-edit-mstSleModels-modal.component.less"],
})
export class CreateOrEditMstSleModelsModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    models: MstSleModelsDto[] = [];
    mstSleModels: CreateOrEditMstSleModelsDto = new CreateOrEditMstSleModelsDto();
    makeId: number;
    listStatus: { value: string, label: string }[] = [
        { value: "Y", label: this.l("MstSleEnable")},
        { value: "N", label: this.l("MstSleDisable")}
    ];
    constructor(
        injector: Injector,
        private _mstSleModelsServiceProxy: MstSleModelServiceProxy
    ) {
        super(injector);
    }

    show(mstSleModelsId?: number, makeId?: number): void {
        this.makeId = makeId;
        
        if (!mstSleModelsId) {
            this._mstSleModelsServiceProxy.getModelCode(mstSleModelsId).subscribe((result) => {
                this.models = result;
            });
            this.mstSleModels = new CreateOrEditMstSleModelsDto();
            this.mstSleModels.id = mstSleModelsId;

            this.active = true;
            this.modal.show();
        } else {
            this._mstSleModelsServiceProxy.getModelCode(mstSleModelsId).subscribe((result) => {
                this.models = result;
            });
            this._mstSleModelsServiceProxy.getMstSleModelsForEdit(mstSleModelsId)
                .subscribe((result) => {
                    this.mstSleModels = result.mstSleModels;

                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;
        var checkMarketingCode: boolean;
        var checkAbbreviation: boolean;
        var checkProductionCode: boolean;

        this.models.forEach(element => {
            if(this.mstSleModels.marketingCode === element.marketingCode && this.mstSleModels.id != element.id) {
                checkMarketingCode = false;
            };

            if(this.mstSleModels.abbreviation === element.abbreviation && element.abbreviation != null && this.mstSleModels.id != element.id) {
                checkAbbreviation = false;
            };

            if (this.mstSleModels.productionCode === element.productionCode && this.mstSleModels.id != element.id) {
                checkProductionCode = false;
            }
        });

        if(checkMarketingCode == false) {
            this.saving = false;
            this.message.error(this.l("MstSleModelMarketingCodeisExisted"));
        }
        else if(checkAbbreviation == false){
            this.saving = false;
            this.message.error(this.l("MstSleModelAbbreviationIsExisted"));
        } else if(checkProductionCode == false){
            this.saving = false;
            this.message.error(this.l("MstSleModelProductionCode"));
        } else {
            this.mstSleModels.makeId = this.makeId;
            this._mstSleModelsServiceProxy
                .createOrEdit(this.mstSleModels)
                .pipe(
                    finalize(() => {
                        this.saving = false;
                    })
                )
                .subscribe(() => {
                    this.notify.info(this.l("SavedSuccessfully"));
                    this.close();
                    this.modalSave.emit(null);
                });
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}