import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as _ from 'lodash';
import { GetMstGenProvincesForViewDto, MstGenProvinceServiceProxy, MstSleLookupDto } from '@shared/service-proxies/service-proxies';
import { ceil } from 'lodash';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import { CreateOrEditMstGenProvincesModalComponent } from './create-or-edit-province/create-or-edit-province.component';
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';

@Component({
    selector: 'mst-gen-province',
    templateUrl: './mst-sle-province.component.html',
    styleUrls: ['./mst-sle-province.component.less'],
})

export class MstSleProvinceComponent extends AppComponentBase implements OnInit {
    @ViewChild("createOrEditMstGenProvincesModal") createOrEditMstGenProvincesModal: CreateOrEditMstGenProvincesModalComponent;

    defaultColDef = {
        floatingFilter: false,
        filter: "agTextColumnFilter",
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };

    frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent
    };

    filterText: string = "";
    codeFilter: string = "";
    nameFilter: string = "";
    orderingFilter: number;
    statusFilter: string = "Y";
    populationAmountMaxFilter: number;
    populationAmountMinFilter: number;
    squareAmountMaxFilter: number;
    squareAmountMinFilter: number;
    subRegionIdFilter: number;
    sorting: string = "";
    advancedFiltersAreShown: boolean = false;
    selectedRow: GetMstGenProvincesForViewDto = new GetMstGenProvincesForViewDto();
    columnDefs: CustomColDef[];
    params!: GridParams;
    paginationParams: PaginationParamsModel;
    isLoading: boolean = false;
    list: GetMstGenProvincesForViewDto[] = [];
    totalPages: number = 0;
    maxResultCount: number = 20;
    rowData: any[] = [];
    selectProvinceId: number;
    regions: MstSleLookupDto[] = [];
    listInsurance = [];
    listServiceLocation= [];
    skipCount: number = 0;

    constructor(
        injector: Injector,
        private _mstGenProvinceServiceProxy: MstGenProvinceServiceProxy,
        private dataFormatService: DataFormatService,
        private _localStorage: LocalStorageService,
    ) {
        super(injector);
        this.columnDefs = [
            {
                headerName: this.l("Code"),
                headerTooltip: this.l("Code"),
                field: "code",
                cellClass: ["text-right"],
                flex: 2
            },
            {
                headerName: this.l("Name"),
                headerTooltip: this.l("Name"),
                field: "name",
                cellClass: ["text-left"],
                flex: 3
            },
            {
                headerName: this.l("PopulationAmount"),
                headerTooltip: this.l("PopulationAmount"),
                field: "populationAmount",
                cellClass: ["text-right"],
                cellStyle: { "padding-right": "5px" },
                flex: 3,
                cellRenderer: params => this.dataFormatService.moneyFormat(params.value)
            },
            {
                headerName: this.l("SquareAmount"),
                headerTooltip: this.l("SquareAmount"),
                field: "squareAmount",
                cellClass: ["text-right"],
                cellStyle: { "padding-right": "5px" },
                flex: 2,
                cellRenderer: params => this.dataFormatService.moneyFormat(params.value)

            },
            {
                headerName: this.l("MstGenProvincesPercentOwnershipTax"),
                headerTooltip: this.l("MstGenProvincesPercentOwnershipTax"),
                field: "percentOwnershipTax",
                cellClass: ["text-right"],
                cellStyle: { "padding-right": "5px" },
                flex: 4,
                cellRenderer: params => this.dataFormatService.moneyFormat(params.value) + ' %',
            },
            {
                headerName: this.l("MstGenProvincesRegistrationFee"),
                headerTooltip: this.l("MstGenProvincesRegistrationFee"),
                field: "registrationFee",
                cellClass: ["text-right"],
                cellStyle: { "padding-right": "5px" },
                flex: 3,
                cellRenderer: params => this.dataFormatService.moneyFormat(params.value) ,
            },
            {
                headerName: this.l("Region"),
                headerTooltip: this.l("Region"),
                field: "subRegionId",
                valueGetter: params => this.regions.find(p => p.id === params.data.subRegionId)
                    ? (this.regions.find(p => p.id === params.data.subRegionId).name ?? null) : null,
                cellClass: ["text-left"],
                flex: 2
            },
            {
                headerName: this.l("Status"),
                headerTooltip: this.l("Status"),
                field: "status",
                cellRenderer: "agCheckboxRendererComponent",
                data: ['Y', 'N'],
                cellClass: ["text-center"],
                disableCheckbox: true,
                flex: 5
            },
            {
                headerName: this.l("Ordering"),
                headerTooltip: this.l("Ordering"),
                field: "ordering",
                cellClass: ["text-right"],
                flex:2
            },
            {
				headerName: this.l('MstSleGenDealerGroupInsurance'),
				headerTooltip: this.l('MstSleGenDealerGroupInsurance'),
				field: 'groupInsuranceId',
				flex: 4,
				cellClass: ["text-left"],
				valueGetter: (params) => 
				this.listInsurance.find(p => p.value == params.data.groupInsuranceId) 
                ? (this.listInsurance.find(p => p.value == params.data.groupInsuranceId).label ?? null) : null,
			},
            {
				headerName: this.l('ServiceLocationCode'),
				headerTooltip: this.l('ServiceLocationCode'),
				field: 'serviceLocationId',
				flex: 4,
				cellClass: ["text-left"],
				valueGetter: (params) => 
				this.listServiceLocation.find(p => p.value == params.data.serviceLocationId) 
                ? (this.listServiceLocation.find(p => p.value == params.data.serviceLocationId).label ?? null) : null,
			},
        ];
    }

    ngOnInit() {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.regions = this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.regions);
    }

    getMstGenProvinces(paginationParams?: PaginationParamsModel) {
        if (this.orderingFilter == null) this.orderingFilter = undefined;
        if (this.populationAmountMaxFilter == null) this.populationAmountMaxFilter = undefined;
        if (this.populationAmountMinFilter == null) this.populationAmountMinFilter = undefined;
        if (this.squareAmountMaxFilter == null) this.squareAmountMaxFilter = undefined;
        if (this.squareAmountMinFilter == null) this.squareAmountMinFilter = undefined;

        return this._mstGenProvinceServiceProxy.getAll(
            this.filterText,
            this.codeFilter,
            this.nameFilter,
            this.orderingFilter,
            this.statusFilter,
            this.subRegionIdFilter,
            this.populationAmountMaxFilter,
            this.populationAmountMinFilter,
            this.squareAmountMaxFilter,
            this.squareAmountMinFilter,
            this.sorting ?? null,
            paginationParams ? paginationParams.skipCount : 0,
            paginationParams ? paginationParams.pageSize : 20,
        );
    }

    onGridReady(paginationParams) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount =
            (paginationParams.pageNum - 1) * paginationParams.pageSize;

        this.getMstGenProvinces(this.paginationParams).subscribe(
            (result) => {
                this.isLoading = false;
                this.list = result.items;
                this.totalPages = ceil(
                    result.totalCount / this.paginationParams.pageSize
                );
                this.paginationParams.totalCount = result.totalCount;
                this.paginationParams.totalPage = this.totalPages;
            },
            (err) => this.notify.error(err.error),
            () => {
                this.params.api.setRowData(this.list);
            }
        );
    }
    onChangeSelection(params) {
        this.selectedRow = params.api.getSelectedRows()[0];
        if (this.selectedRow)
            this.selectProvinceId = this.selectedRow.id;
    }
    callBackProvinceGrid(params) {
        this.params = params;
        this.params.api.paginationSetPageSize(
            this.paginationParams.pageSize
        );
        this.onGridReady(this.paginationParams);
        this.selectProvinceId = undefined;
    }

    changePage(paginationParams: PaginationParamsModel) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount =
            (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;

        this.getMstGenProvinces(this.paginationParams).subscribe((result) => {
            this.list = result.items;
            this.rowData = result.items;
            this.paginationParams.totalPage = ceil(
                result.totalCount / this.maxResultCount
            );
        });
    }

    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    search() {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);

        this.onGridReady(this.paginationParams);
        this.selectProvinceId = undefined;
    }

    createMstGenProvinces(): void {
        this.createOrEditMstGenProvincesModal.show();
    }

    editMstGenProvinces(): void {
        this.createOrEditMstGenProvincesModal.show(this.selectProvinceId);
    }

    deleteMstGenProvinces(): void {
        this.message.confirm("", this.l("AreYouSure"), (isConfirmed) => {
            if (isConfirmed) {
                this._mstGenProvinceServiceProxy
                    .delete(this.selectProvinceId)
                    .subscribe(() => {
                        this.callBackProvinceGrid(this.params);
                        this.notify.success(this.l("SuccessfullyDeleted"));
                        this.selectProvinceId = undefined;
                    });
            }
        });
    }

    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        this.clearValueFilter();
    }

    clearValueFilter() {
        this.filterText = "";
        this.codeFilter = "";
        this.nameFilter = "";
        this.orderingFilter = undefined;
        this.statusFilter = 'Y';
        this.subRegionIdFilter = undefined;
        this.populationAmountMaxFilter = undefined;
        this.populationAmountMinFilter = undefined;
        this.squareAmountMaxFilter = undefined;
        this.squareAmountMinFilter = undefined;
    }
}
