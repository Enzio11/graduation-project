import { TABS } from "@app/shared/constants/tab-keys";
import { NgModule } from '@angular/core';
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { MstSleProvinceComponent } from "./mst-sle-province.component";
import { CreateOrEditMstGenProvincesModalComponent } from "./create-or-edit-province/create-or-edit-province.component";

const tabcode_component_dict = {
  [TABS.MASTER_SALE_PROVINCE]: MstSleProvinceComponent
};
@NgModule({
  imports: [
    CommonDeclareModule,
  ],
  declarations: [
    MstSleProvinceComponent,
    CreateOrEditMstGenProvincesModalComponent
  ],
})

export class MstSleProvinceModule {
  static getComponent(tabCode: string) {
    return tabcode_component_dict[tabCode];
  }
}