import { Component, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CreateOrEditMstGenProvinceDto, MstGenProvinceServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { LookUpKeys } from '@app/shared/constants/lookUpKeys';
import { StorageKeys } from '@app/shared/constants/storageKeys';
import { LocalStorageService } from '@shared/utils/local-storage.service';

@Component({
    selector: 'create-or-edit-province-modal',
    templateUrl: './create-or-edit-province.component.html',
    styleUrls: ['./create-or-edit-province.component.less'],
})

export class CreateOrEditMstGenProvincesModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;
    listStatus: { value: string, label: string }[] = [
        { value: "Y", label: this.l("MstSleEnable") },
        { value: "N", label: this.l("MstSleDisable")}
    ];
    mstGenProvinces: CreateOrEditMstGenProvinceDto = new CreateOrEditMstGenProvinceDto();
    regions: { value: boolean, label: string }[] = [];
    listInsurance = []
    listServiceLocation = []

    constructor(
        injector: Injector,
        private _mstGenProvincesServiceProxy: MstGenProvinceServiceProxy,
        private _localStorage: LocalStorageService,
    ) {
        super(injector);
    }

    ngOnInit() {
        this._localStorage.get("tmss/abpzerotemplate_local_storage/" + StorageKeys.pickList).lookUp.filter((e) => e.code === LookUpKeys.regions).forEach(e => this.regions.push({ value: e.id, label: e.name }));
    }

    show(mstGenProvincesId?: number): void {
        if (!mstGenProvincesId) {
            this.mstGenProvinces = new CreateOrEditMstGenProvinceDto();
            this.mstGenProvinces.id = mstGenProvincesId;

            this.active = true;
            this.modal.show();
        } else {
            this._mstGenProvincesServiceProxy.getMstGenProvincesForEdit(mstGenProvincesId)
                .subscribe((result) => {
                    this.mstGenProvinces = result.mstGenProvinces;

                    this.active = true;
                    this.modal.show();
                });
        }
    }

    save(): void {
        this.saving = true;

        this._mstGenProvincesServiceProxy
            .createOrEdit(this.mstGenProvinces)
            .pipe(
                finalize(() => {
                    this.saving = false;
                })
            )
            .subscribe(() => {
                this.notify.info(this.l("SavedSuccessfully"));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}

