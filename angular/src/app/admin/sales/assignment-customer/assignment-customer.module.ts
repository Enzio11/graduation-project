import { NgModule } from "@angular/core";
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { TABS } from "@app/shared/constants/tab-keys";
import { AssignToSalesPersonModal } from "../assign-to-salesperson/assign-to-salesperson.component";
import { AssignmentCustomerContactModal } from "./assignment-customer-contact/assignment-customer-contact.component";
import { AssignmentCustomerComponent } from "./assignment-customer.component";
import { CreateOrEditAssignmentCustomerModalComponent } from "./create-or-edit-assignment-customer/create-or-edit-assignment-customer.component";

const tabcode_component_dict = {
    [TABS.ASSIGNMENT_CUSTOMER]: AssignmentCustomerComponent
};

@NgModule({
    imports: [
        CommonDeclareModule,
    ],
    declarations: [
        AssignmentCustomerComponent,
        AssignToSalesPersonModal,
        CreateOrEditAssignmentCustomerModalComponent,
        AssignmentCustomerContactModal,
    ],
})

export class AssignmentCustomerModule {
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
    }
}