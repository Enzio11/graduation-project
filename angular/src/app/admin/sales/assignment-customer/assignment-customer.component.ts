import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { FileUpload } from "primeng/fileupload";
import { AssignCustomerToSalesPersonDto, MstSleTestDriveRouteServiceProxy, SalesCustomerWebServiceProxy, SessionServiceProxy } from '@shared/service-proxies/service-proxies';
import { AgDateEditorComponent } from '@app/shared/common/grid/ag-datepicker-editor/ag-date-editor.component';
import { DataFormatService } from "@app/shared/common/services/data-format.service";
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { AgTimeEditorComponent } from '@app/shared/common/grid/ag-timepicker-editor/ag-time-editor.component';
import * as moment from 'moment';
import { ValueGetterParams } from '@ag-grid-enterprise/all-modules';
import { ceil } from 'lodash';
import { finalize } from 'rxjs/operators';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { CreateOrEditAssignmentCustomerModalComponent } from './create-or-edit-assignment-customer/create-or-edit-assignment-customer.component';
import { AssignToSalesPersonModal } from '../assign-to-salesperson/assign-to-salesperson.component';
import { AssignmentCustomerContactModal } from './assignment-customer-contact/assignment-customer-contact.component';
import { AppConsts } from '@shared/AppConsts';
import { HttpClient } from "@angular/common/http";

@Component({
    selector: 'assignmenr-customer',
    templateUrl: './assignment-customer.component.html',
    styleUrls: ['./assignment-customer.component.less'],
})

export class AssignmentCustomerComponent extends AppComponentBase implements OnInit {
    @ViewChild("ExcelFileUpload", { static: false }) excelFileUpload: FileUpload;
    @ViewChild('createOrEditAssignment') createOrEditAssignment: CreateOrEditAssignmentCustomerModalComponent;
    @ViewChild('assignToSalesPerson') assignToSalesPerson: AssignToSalesPersonModal;
    @ViewChild("assignmentCustomerContact") assignmentCustomerContact: AssignmentCustomerContactModal;

    campaign: string = "";
    isContacted: number = 2;
    loading: boolean = false;
    hotnesses: { value: number, label: string }[] = [];
    dealers: { value: number, label: string }[] = [];
    isTMV: boolean = false;
    models: { value: number, label: string }[] = [];
    sources: { value: number, label: string }[] = [];
    contactStaus: { value: number, label: string }[] = [
        {value: 2, label: this.l("All")},
        {value: 1, label: this.l("SalesCustomerContactedFail")},
        {value: 0, label: this.l("SalesCustomerNotContacted")},
        {value: 3, label: this.l("SalesCustomerContactedSuccess")}
    ];
    salesPersons: { value: number, label: string }[] = [];
    assignPersons: { value: number, label: string }[] = [];
    salesStages: { value: number, label: string }[] = [];
    importSources: { value: number, label: string }[] = [
        {value: 0, label: this.l("All")},
        {value: 1, label: this.l("TMV")},
        {value: 2, label: this.l("Dealer")},
        {value: 3, label: this.l("SalesCustomerApiLead")},
        {value: 4, label: this.l("SalesCustomerAssignmentManual")},
    ];
    listCount = [];

    columnDefs: CustomColDef[] = [];

    defaultColDef = {
        minWidth: 60,
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };

    sideBar = {
        toolPanels: [
            {
                id: 'columns',
                labelDefault: this.l("Columns"),
                labelKey: 'columns',
                iconKey: 'columns',
                toolPanel: 'agColumnsToolPanel',
                toolPanelParams: {
                    suppressRowGroups: true,
                    suppressValues: true,
                    suppressPivots: true,
                    suppressPivotMode: true,
                    suppressColumnFilter: false
                },
            },
        ],
        defaultToolPanel: '',
    };

    frameworkComponents: FrameworkComponent = {
        agDateEditorComponent: AgDateEditorComponent,
        agTimeEditorComponent: AgTimeEditorComponent,
        agCheckboxRendererComponent: AgCheckboxRendererComponent
    };

    //? Individual Customer
    advancedFiltersAreShown: boolean = true;
    paginationParams: PaginationParamsModel = { pageNum: 1, pageSize: 20, totalCount: 0 };
    params!: GridParams;
    totalPages: number = 0;
    filterText: string = "";
    hotnessId: number = 0;
    customerName: string = "";
    skipCount: number = 0;
    maxResultCount: number = 20;
    sorting: string = "";
    list = [];
    selectedRow;
    selectedCustomerId: number;
    customerTypeId: number;
    dealerId: number = 0;
    assignDateTo = moment();
    assignDateFrom = moment('2021-01-01');
    creationDateTo = moment();
    creationDateFrom = moment('2021-01-01');
    dropDateTo = moment();
    dropDateFrom = moment('2021-01-01');
    fARId: number;
    salePersonId: number = 0;
    assignPersonId : number = 0;
    sourceId: number = 0;
    districtId: number;
    provinceId: number;
    modelId: number = 0;
    customerTel1: string = "";
    isDropDate: boolean = false;
    isCreationDate: boolean = true;
    isAssignDate: boolean = false;
    importSourceId: number = 0;
    salesStageId: number = 0;
    agencyLead: string = "";
    agencyLeads: { value: string, label: string }[] = [
        {value: "", label: this.l("All") },
        {value: "AAA", label: "AAA" },
        {value: "AWING", label: "AWING" },
        {value: "NOVAON", label: "NOVAON"}
    ];
    assignArray: any[];
    assignmentChecked: boolean = false;
    totalAssign: number = 0;
    isAssignAll = false;
    listAssign: AssignCustomerToSalesPersonDto;
    expectedDelTimings: { value: number, label: string }[] = [];
    uploadUrl = "";

    constructor(
        Injector: Injector,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
        private dataFormatService: DataFormatService,
        private _mstSleTestDriveRouteServiceProxy: MstSleTestDriveRouteServiceProxy,
        private _sessionService: SessionServiceProxy,
        private _httpClient: HttpClient,
    ) {
        super(Injector);

        this.uploadUrl = AppConsts.remoteServiceBaseUrl + "/SalesCustomer/ImportFromExcel";
        
        this.columnDefs = [
            {
                headerName: this.l("SalesVehicleUioAssign"),
                headerTooltip: this.l("SalesVehicleUioAssign"),
                field: "assignChecked",
                cellClass: ["text-center"],
                data: [1, 0],
                width: 110,
                cellRenderer: "agCheckboxRendererComponent",
            },
            {
                headerName: this.l('SalesCustomerAssignPerson'),
                headerTooltip: this.l('SalesCustomerAssignPerson'),
                field: 'assignPerson',
                minWidth: 150,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerSalesPerson'),
                headerTooltip: this.l('SalesCustomerSalesPerson'),
                field: 'salesPerson',
                minWidth: 150,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerContactResult'),
                headerTooltip: this.l('SalesCustomerContactResult'),
                field: 'contactResult',
                minWidth: 70,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerReasonOfContactFail'),
                headerTooltip: this.l('SalesCustomerReasonOfContactFail'),
                field: 'reasonOfFailContact',
                minWidth: 70,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerSalesStage'),
                headerTooltip: this.l('SalesCustomerSalesStage'),
                field: 'status',
                minWidth: 70,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerCode'),
                headerTooltip: this.l('SalesCustomerCode'),
                field: 'no',
                maxWidth: 160,
                resizable: true,
                cellClass: ["text-left"]
            },
            {
                headerName: this.l('SalesCustomerDateTime'),
                headerTooltip: this.l('SalesCustomerDateTime'),
                field: 'dateTime',
                maxWidth: 150,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerImportDate'),
                headerTooltip: this.l('SalesCustomerImportDate'),
                field: 'creationTime',
                maxWidth: 130,
                resizable: true,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.dateFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerAssignDate'),
                headerTooltip: this.l('SalesCustomerAssignDate'),
                field: 'assignDate',
                maxWidth: 140,
                resizable: true,
                cellClass: ["text-left"],
                cellEditor: "agDateEditorComponent",
                valueFormatter: (params) => this.dataFormatService.datetimeFormat(params.value),
            },
            {
                headerName: this.l('SalesCustomerName'),
                headerTooltip: this.l('SalesCustomerName'),
                field: 'customerName',
                minWidth: 160,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Phone'),
                headerTooltip: this.l('Phone'),
                field: 'customerTel1',
                maxWidth: 120,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerEmail'),
                headerTooltip: this.l('SalesCustomerEmail'),
                field: 'email',
                maxWidth: 150,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('Model'),
                headerTooltip: this.l('Model'),
                field: 'carAttention',
                maxWidth: 100,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerLeadSource'),
                headerTooltip: this.l('SalesCustomerLeadSource'),
                field: 'sourceId',
                minWidth: 90,
                resizable: true,
                cellClass: ["text-left"],
                valueGetter: (params: ValueGetterParams) => this.sources.find(p => p.value === params.data.sourceId)
                    ? (this.sources.find(p => p.value === params.data.sourceId).label ?? null) : null,
            },
            {
                headerName: this.l('SalesCustomerHotness'),
                headerTooltip: this.l('SalesCustomerHotness'),
                field: 'hotnessId',
                maxWidth: 120,
                resizable: true,
                cellClass: ["text-left"],
                valueGetter: (params: ValueGetterParams) => this.hotnesses.find(p => p.value == params.data.hotnessId)
                    ? (this.hotnesses.find(p => p.value == params.data.hotnessId).label ?? null) : null,
            },
            {
                headerName: this.l('SalesCustomerExpectedDelTiming'),
                headerTooltip: this.l('SalesCustomerExpectedDelTiming'),
                field: 'expectedDelTiming',
                maxWidth: 140,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerCampaign'),
                headerTooltip: this.l('SalesCustomerCampaign'),
                field: 'campaign',
                minWidth: 70,
                resizable: true,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesCustomerAgencyLead'),
                headerTooltip: this.l('SalesCustomerAgencyLead'),
                field: 'agencyLead',
                minWidth: 70,
                resizable: true,
                cellClass: ["text-left"],
            },
        ];
    }

    ngOnInit(): void {
        this._sessionService.getCurrentLoginInformations().subscribe((res) => {
            if (!res.tenant) {
                this._mstSleTestDriveRouteServiceProxy
                    .getDealerForTestDrive()
                    .subscribe((result) => {
                        this.dealers.push({ value: null, label: this.l("All") })
                        result.forEach(e => this.dealers.push({ value: e.id, label: e.abbreviation }));
                    });
            } else {
                this.dealers.push({
                    value: res.tenant.id,
                    label: res.tenant.tenancyName,
                });
                this.dealerId = res.tenant.id;
                this.isTMV = true;
            }
        });

        this.salesPersons.push({ value: 0, label: this.l("All")});
        this.assignPersons.push({ value: 0, label: this.l("All")});
        this.hotnesses.push({ value: 0, label: this.l("All")});
        this.salesStages.push({ value: 0, label: this.l("All")});
        this.dealers.push({ value: 0, label: this.l("All")});
        this.models.push({ value: 0, label: this.l("All")});
        this.sources.push({ value: 0, label: this.l("All")});

        this._salesCustomerWebServiceProxy.getAssignmentCustomerMasterData().subscribe((result) => {
            result.salesPersons.forEach(e => this.salesPersons.push({ value: e.id, label: e.fullName }));
            result.assignPersons.forEach(e => this.assignPersons.push({ value: e.id, label: e.fullName }));

            result.hotnesses.forEach(e => this.hotnesses.push({ value: e.id, label: e.description }));
            result.salesStages.forEach(e => this.salesStages.push({ value: e.id, label: e.description }));
            result.dealers.forEach(e => this.dealers.push({ value: e.id, label: e.abbreviation }));
            result.models.forEach(e => this.models.push({ value: e.id, label: e.marketingCode }));
            result.sources.forEach(e => this.sources.push({ value: e.id, label: e.description }));
            result.expectedDelTimings.forEach(e => this.expectedDelTimings.push({ value: e.id, label: e.description }));
        })
    }

    callBackGrid(params) {
        this.params = params;
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);

        this.onGridReady(this.paginationParams)
    }

    onGridReady(paginationParams) {
        this.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;

        this.getAllAssignmentCustomer();
    };

    changePage(paginationParams){
        this.paginationParams = paginationParams;
        this.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;
        this.getAllAssignmentCustomer();
    }

    onChangeSelection(params) {
        this.selectedCustomerId = undefined;

        this.selectedRow = params.api.getSelectedRows()[0];
        if (this.selectedRow){
            this.selectedCustomerId = this.selectedRow.id;
        }
    }

    getAllAssignmentCustomer(){
        this.list = [];
        
        this.loading = true;
        this._salesCustomerWebServiceProxy.getAllAssignmentCustomerForView(
            this.filterText,
            this.fARId,
            this.salePersonId,
            this.assignPersonId,
            this.hotnessId,
            this.sourceId,
            this.importSourceId,
            this.districtId,
            this.provinceId,
            this.customerName,
            this.customerTypeId,
            this.dealerId,
            this.modelId,
            this.isDropDate == true ? this.dropDateFrom : null,
            this.isDropDate == true ? this.dropDateTo : null,
            this.isCreationDate == true ? this.creationDateFrom : null,
            this.isCreationDate == true ? this.creationDateTo : null,
            this.isAssignDate == true ? this.assignDateFrom : null,
            this.isAssignDate == true ? this.assignDateTo : null,
            this.customerTel1,
            this.isContacted,
            this.campaign,
            this.salesStageId,
            this.agencyLead,
            this.sorting ?? null,
            this.skipCount,
            this.maxResultCount
        )
        .pipe(finalize(() => this.loading = false))
        .subscribe(
            (result) => {
                result.items.forEach(e => this.getDateTime(e));
                setTimeout(() => {
                    this.list = result.items;
                    this.totalPages = ceil(result.totalCount / this.maxResultCount);
                    this.paginationParams.totalCount = result.totalCount;
                    this.paginationParams.totalPage = this.totalPages;
                }, 0);
            }
        );

        this._salesCustomerWebServiceProxy.getCountSalesAssignmentCustomer(
            this.filterText,
            this.fARId,
            this.salePersonId,
            this.assignPersonId,
            this.hotnessId,
            this.sourceId,
            this.importSourceId,
            this.districtId,
            this.provinceId,
            this.customerName,
            this.customerTypeId,
            this.dealerId,
            this.modelId,
            this.isDropDate == true ? this.dropDateFrom : null,
            this.isDropDate == true ? this.dropDateTo : null,
            this.isCreationDate == true ? this.creationDateFrom : null,
            this.isCreationDate == true ? this.creationDateTo : null,
            this.isAssignDate == true ? this.assignDateFrom : null,
            this.isAssignDate == true ? this.assignDateTo : null,
            this.customerTel1,
            this.isContacted,
            this.campaign,
            this.salesStageId,
            this.agencyLead,
            this.sorting ?? null,
            this.skipCount,
            this.maxResultCount
        )
        .subscribe(
            (result) => {
                this.listCount = result;
            }
        );
    }

    getDateTime(item){
        return item['dateTime'] = (!this.dataFormatService.dateFormat(item['date']) ? "" : this.dataFormatService.dateFormat(item['date'])) + " " + (!this.dataFormatService.formatHoursSecond(item['time']) ? "" : this.dataFormatService.formatHoursSecond(item['time']))
    }
    //! End Sales Customer

    //#region  Filter
    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        if (!this.advancedFiltersAreShown) {
            this.clearValueFilter();
        }
    }

    clearValueFilter(): void {
        this.filterText = "";
        this.hotnessId = 0;
        this.customerName = "";
        this.dealerId = 0;
        this.dropDateFrom = moment('2021-01-01');
        this.dropDateTo = moment();
        this.fARId = undefined;
        this.salePersonId = 0;
        this.sourceId = 0;
        this.districtId = undefined;
        this.provinceId = undefined;
        this.modelId = 0;
        this.isContacted = 2;
        this.customerTel1 = "";
        this.assignDateTo = moment();
        this.assignDateFrom = moment('2021-01-01');
        this.creationDateTo = moment();
        this.creationDateFrom = moment('2021-01-01');
        this.salesStageId = 0;
        this.assignPersonId = 0;
        this.agencyLead = "";
    }

    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }

    search() {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };

        this.onGridReady(this.paginationParams);
        this.selectedCustomerId = undefined;
    }
    //#endregion

    createNewAssignmentCustomer(){
        this.createOrEditAssignment.show(this.salesPersons, this.models, this.sources, this.expectedDelTimings);
    }

    isAssignChange(event) {
        let count = 0;
        this.assignArray = [];
        this.assignmentChecked = false;
        this.params.api.forEachNode((node) => {
          event == true ? node.data.assignChecked = 1 : node.data.assignChecked = 0;
          this.assignArray.push(node.data);
        });
        this.params.api.forEachNode((node) => {
            if (node.data.assignChecked == 1) {
                count++;
                return this.assignmentChecked = true;
            };
        });
        if (this.isAssignAll) this.totalAssign = count;
        if (!this.isAssignAll) this.totalAssign = 0;
        this.params.api.setRowData(this.assignArray);
    }

    onCellValueChanged(cellParams) {
        this.assignmentChecked = false;
        this.totalAssign = 0;
        if (cellParams.column.colId == "assignChecked") {
            this.params.api.forEachNode((node) => {
            if (node.data.assignChecked == 1) {
                this.assignmentChecked = true;
                this.totalAssign = this.totalAssign + 1;
            };
            });
        }
    }

    assignToPerson() {
        if(!this.selectedCustomerId) return this.notify.info(this.l('Xin hãy chọn 1 Khách hàng'));

        this.listAssign = new AssignCustomerToSalesPersonDto();
        this.listAssign.customerIds = [];
        this.params.api.forEachNode((node) => {
            if (node.data.assignChecked == 1) {
                this.listAssign.customerIds.push(node.data.id)
            }
        });
        this.assignToSalesPerson.show(this.listAssign);
    }

    contactResult() {
        if(!this.selectedCustomerId) return this.notify.info(this.l('Xin hãy chọn 1 Khách hàng'));

        this.assignmentCustomerContact.show(this.selectedCustomerId);
    }

    uploadExcel(data: { files: File }): void {
        const formData: FormData = new FormData();
        const file = data.files[0];
        formData.append("file", file, file.name);

        this._httpClient.post<any>(this.uploadUrl, formData)
            .pipe(finalize(() => this.excelFileUpload.clear()))
            .subscribe((response) => {
				if (response.success) {
					this.notify.success(this.l("ImportMstSleAccessoriesProcessStarted"));
				} else if (response.error != null) {
					this.notify.error(response.error.message);
				}
			});
    }

    onUploadExcelError(): void {
        this.notify.error(this.l("ImportUsersUploadFailed"));
    }
}
