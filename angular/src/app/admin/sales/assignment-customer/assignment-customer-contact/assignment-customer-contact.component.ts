import { Component, ViewChild, Output, EventEmitter, Injector } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AssignmentCustomerContactResultDto, SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'assignment-customer-contact',
    templateUrl: './assignment-customer-contact.component.html',
    styleUrls: ['./assignment-customer-contact.component.less'],
})

export class AssignmentCustomerContactModal extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Output() openCustomerUpdate: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
    saving: boolean = false;

    reasonOfNCs: { value: number, label: string }[] = [];
    reasonOfNCId: number;
    contactOK: boolean = false;
    contactNotOK: boolean = false;
    isOtherReason: boolean = false;
    isMoveFreeze: boolean = false;
    isOtherFreeze: boolean = false;
    freezeReason: string = "";
    freezeReasons: { value: number, label: string }[] = [];
    customerContactResult: AssignmentCustomerContactResultDto = new AssignmentCustomerContactResultDto();

    loading: boolean = false;

    constructor(
        injector: Injector,
        private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
    ) {
        super(injector);
    }

    show(customerId: number): void {
        this.customerContactResult = new AssignmentCustomerContactResultDto();
        this.customerContactResult.salesCustomerId = customerId;

        if (this.reasonOfNCs.length == 0) {
            this.loading = true;

            this._salesCustomerWebServiceProxy.getReasonOfNCForSalesCustomer().pipe(finalize(() => this.loading = false))
            .subscribe((result) => {
                result.forEach(e => this.reasonOfNCs.push({ value: e.id, label: e.description }));
            });
        }

        if (this.freezeReasons.length == 0) {
            this.loading = true;

            this._salesCustomerWebServiceProxy.getReasonOfFreezeForSalesCustomer().pipe(finalize(() => this.loading = false))
            .subscribe((result) => {
                result.forEach(e => this.freezeReasons.push({ value: e.id, label: e.description }));
            });
        }

        this.active = true;
        this.modal.show();
    }

    save(): void {
        this.saving = true;
        this.loading = true;

        if (this.contactNotOK) {
            this.customerContactResult.contactResult = 0;
        
            this._salesCustomerWebServiceProxy.assignmentCustomerContactResult(this.customerContactResult)
            .pipe(finalize(() => this.loading = false))
            .subscribe(() => {
                this.saving = false;
                this.notify.info(this.l('SavedSuccessfully'));
                this.modalSave.emit(null);
                this.close()
            });
        }

        if (this.contactOK) {
            this.openCustomerUpdate.emit(this.customerContactResult.salesCustomerId);
            this.saving = false;
            this.close();
        }
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    changeReason() {
		if (Number(this.customerContactResult.reasonOfNCId) == this.reasonOfNCs.find(e => e.label == 'Khác' || e.label == 'Khác (free text)' || e.label == 'Other').value) {
			return this.isOtherReason = true;
		}
		return this.isOtherReason = false;
	}

    changeOKContactResult() {
        if(this.contactOK) this.contactNotOK = false;
        if(!this.contactOK) this.contactNotOK = true;
    }

    changeNotOKContactResult() {
        if(this.contactNotOK) this.contactOK = false;
        if(!this.contactNotOK) this.contactOK = true;
    }

    changeFreeze() {
        if (Number(this.customerContactResult.reasonOfFreezeId) == this.freezeReasons.find(e => e.label == 'Khác' || e.label == 'Khác (free text)' || e.label == 'Other').value) {
			return this.isOtherFreeze = true;
		}
		return this.isOtherFreeze = false;
    }
}
