import { Component, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrEditAssignmentCustomerDto, SalesCustomerWebServiceProxy } from '@shared/service-proxies/service-proxies';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { finalize } from 'rxjs/operators';

@Component({
	selector: 'create-or-edit-assignment-customer',
	templateUrl: './create-or-edit-assignment-customer.component.html',
	styleUrls: ['./create-or-edit-assignment-customer.component.less']
})

export class CreateOrEditAssignmentCustomerModalComponent extends AppComponentBase {
    @ViewChild("createOrEditModal", { static: true }) modal: ModalDirective;
	@Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active: boolean = false;
	saving: boolean = false;
	loading: boolean = false;
    canSave: boolean = false;

    assignmentCustomer: CreateOrEditAssignmentCustomerDto = new CreateOrEditAssignmentCustomerDto();
    salesPersons: { value: number, label: string }[] = [];
    sources: { value: number, label: string }[] = [];
    models: { value: number, label: string }[] = [];
    expectedDelTimings: { value: number, label: string }[] = [];
    customerTypes = [
        { value: 1, label: "Cá nhân" },
        { value: 2, label: "Doanh Nghiệp" },
    ];

    constructor(
		injector: Injector,
		private _salesCustomerWebServiceProxy: SalesCustomerWebServiceProxy,
	) {
		super(injector);
	}

	ngOnInit() {}

    show(salesPersons: { value: number, label: string }[], models: { value: number, label: string }[], sources: { value: number, label: string }[], expectedDelTimings: { value: number, label: string }[],) {
        this.refresh();

        this.salesPersons = salesPersons.splice(1, salesPersons?.length > 0 ? salesPersons.length : 0);

        this.sources = sources.splice(1, sources?.length > 0 ? sources.length : 0);

        this.expectedDelTimings = expectedDelTimings;

        this.models = models.splice(1, models?.length > 0 ? models.length : 0);

		this.active = true;
        this.modal.show();
    }

    close() {
		this.modalSave.emit(null);
		this.active = false;
		this.modal.hide();
	}

    refresh() {
        this.assignmentCustomer = new CreateOrEditAssignmentCustomerDto();
        this.assignmentCustomer.customerTypeId = 1;
    }

    validate() {
        this.canSave = false;

        if(!this.assignmentCustomer.customerTypeId) {
            this.notify.warn(this.l("Xin Hãy chọn loại KH!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.assignPersonId) {
            this.notify.warn(this.l("Xin Hãy chọn 1 Tư vấn bán hàng!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.modelId) {
            this.notify.warn(this.l("Xin Hãy chọn 1 Mẫu xe!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.sourceId) {
            this.notify.warn(this.l("Xin Hãy chọn Nguồn Khách hàng!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.expectedDelTimingId) {
            this.notify.warn(this.l("Xin Hãy chọn Khoảng thời gian KH muốn mua xe!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.customerFirstName) {
            this.notify.warn(this.l("Xin Hãy điền tên KH!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.phoneNumber) {
            this.notify.warn(this.l("Xin Hãy điền số điện thoại của KH!"));
            return this.canSave = false;
        }

        if(!this.assignmentCustomer.companyName && this.assignmentCustomer.customerTypeId == 2) {
            this.notify.warn(this.l("Xin Hãy điền tên KH!"));
            return this.canSave = false;
        }

        if(this.assignmentCustomer.date == null) {
            this.notify.warn(this.l("Xin KH ngày KH để lại"));
            return this.canSave = false;
        }

        return this.canSave = true;
    }

    save() {
        this.validate();

        if(this.canSave) {
            this.loading = true;
            
            this.assignmentCustomer.date = moment(this.assignmentCustomer.date);

            this._salesCustomerWebServiceProxy.createOrEditAssignmentCustomer(this.assignmentCustomer)
            .pipe(finalize(() => this.loading = false))
            .subscribe(() => {
                this.notify.success(this.l("SavedSuccessfully"));
    
                this.modalSave.emit(null);
                this.active = false;
                this.modal.hide();
            })
        }
    }
}