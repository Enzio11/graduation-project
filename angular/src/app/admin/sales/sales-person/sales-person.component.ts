import { Component, Injector, ViewChild, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import * as _ from 'lodash';
import { AbpSessionService } from 'abp-ng2-module';

import { ceil } from "lodash";
import { SalePersonServiceProxy, GetSalesPersonForViewDto,OrganizationUnitDto, MstGenDealerServiceProxy, GetMstGenDealerForDropdownDto } from '@shared/service-proxies/service-proxies';
import { AgCheckboxRendererComponent } from '@app/shared/common/grid/ag-checkbox-renderer/ag-checkbox-renderer.component';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import { CreateOrEditSalesPersonModalComponent } from './create-or-edit-sales-person/create-or-edit-sales-person-modal.component';
import { GroupSalesPersonForViewComponent } from './group-sales-person/group-sales-person-for-view.component';
import { IGroupSalesPersonTreeComponentData } from './group-sales-person/group-sales-person.component';
import { CustomColDef, FrameworkComponent, GridParams, PaginationParamsModel } from '@app/shared/common/models/base.model';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'sales-person',
    templateUrl: './sales-person.component.html',
    styleUrls: ['./sales-person.component.less'],
})

export class SalesPersonComponent extends AppComponentBase implements OnInit {
    @ViewChild('createOrEditSalesPersonModal') createOrEditSalesPersonModal: CreateOrEditSalesPersonModalComponent;
    @ViewChild('groupUnitTreeForView') groupSalesPersonForView: GroupSalesPersonForViewComponent;

    loading: boolean = false;
    defaultColDef = {
        flex: 1,
        floatingFilter: false,
        filter: 'agTextColumnFilter',
        resizable: true,
        sortable: true,
        floatingFilterComponentParams: { suppressFilterButton: true },
        textFormatter: function (r) {
            if (r == null) return null;
            return r.toLowerCase();
        },
    };
    frameworkComponents: FrameworkComponent = {
        agCheckboxRendererComponent: AgCheckboxRendererComponent
    };
    columnDefs: CustomColDef[] = [];

    advancedFiltersAreShown: boolean = false;
    filterText: string = "";
    fullNameFilter: string = "";
    positionFilter: string = "";
    abbreviationFilter: string = "";
    statusFilter: string = "Y";
    descriptionFilter: string = "";
    genderFilter: string = "";
    emailFilter: string = "";
    addressFilter: string = "";
    phoneFilter: string = "";
    orderingFilter: number = undefined;
    groupIdFilter: Array<number> = [];
    selectSalesPersonId: number;
    selectedPersonUserId: number;
    selectedSalesPerson: GetSalesPersonForViewDto = new GetSalesPersonForViewDto;
    paginationParams: PaginationParamsModel;
    maxResultCount: number = 20;
    skipCount: number = 0;
    sorting: string = '';
    list: GetSalesPersonForViewDto[] = [];
    totalPages: number = 0;
    isLoading: boolean;
    params!: GridParams;
    default;
    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    nodeSelectId: any;
    dealers: GetMstGenDealerForDropdownDto[] = [];
    dealerIdForTree: number;
    isTMV: boolean = false;

    constructor(
        injector: Injector,
        private _salesPersonServiceProxy: SalePersonServiceProxy,
        private dataFormatService: DataFormatService,
        private _mstGenDealerAppService: MstGenDealerServiceProxy,
        private _sessionService: AbpSessionService,
    ) {
        super(injector);

        this.columnDefs = [
            {
                headerName: this.l('SalesPersonFullName'),
                headerTooltip: this.l('SalesPersonFullName'),
                field: 'fullName',
                minWidth: 150,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonBirthday'),
                headerTooltip: this.l('SalesPersonBirthday'),
                field: 'birthday',
                valueFormatter: (params) => {
                    if (params.value != null) {
                        return this.dataFormatService.formatDateForSale(params.value)
                    }

                    return "";
                },
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonPosition'),
                headerTooltip: this.l('SalesPersonPosition'),
                field: 'position',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonAbbreviation'),
                headerTooltip: this.l('SalesPersonAbbreviation'),
                field: 'abbreviation',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonGender'),
                headerTooltip: this.l('SalesPersonGender'),
                field: 'gender',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonPhone'),
                headerTooltip: this.l('SalesPersonPhone'),
                field: 'phone',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonEmail'),
                headerTooltip: this.l('SalesPersonEmail'),
                field: 'email',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l('SalesPersonAddress'),
                headerTooltip: this.l('SalesPersonAddress'),
                field: 'address',
                minWidth: 100,
                cellClass: ["text-left"],
            },
            {
                headerName: this.l("SalesPersonAllIsViewCustomer"),
                headerTooltip: this.l("SalesPersonAllIsViewCustomer"),
                field: "isViewCustomer",
                minWidth: 100,
                cellRenderer: "agCheckboxRendererComponent",
                data: [true, false],
                cellClass: ["text-center"],
                disableCheckbox: true
            },
            // {
            //     headerName: this.l("SalesPersonAllIsReceiveAssignmentList"),
            //     headerTooltip: this.l("SalesPersonAllIsReceiveAssignmentList"),
            //     field: "isReceiveAssignmentList",
            //     minWidth: 100,
            //     cellRenderer: "agCheckboxRendererComponent",
            //     data: [true, false],
            //     cellClass: ["text-center"],
            //     disableCheckbox: true
            // },
            // {
            //     headerName: this.l("SalesPersonIsViewDuplicateCustomer"),
            //     headerTooltip: this.l("SalesPersonIsViewDuplicateCustomer"),
            //     field: "isViewDuplicateCustomer",
            //     minWidth: 100,
            //     cellRenderer: "agCheckboxRendererComponent",
            //     data: [true, false],
            //     cellClass: ["text-center"],
            //     disableCheckbox: true
            // },
            {
                headerName: this.l("SalesPersonStatus"),
                headerTooltip: this.l("SalesPersonStatus"),
                field: "status",
                minWidth: 80,
                cellRenderer: "agCheckboxRendererComponent",
                data: ['Y', 'N'],
                cellClass: ["text-center"],
                disableCheckbox: true
            },
        ];
    }

    ngOnInit() {
        var currentTenantId = this._sessionService.tenantId;
        if (currentTenantId == null) {
            this.dealerIdForTree = undefined;
            this.isTMV = true;
        }
        if (currentTenantId != null) {
            this.dealerIdForTree = currentTenantId;
            this.isTMV = false;
        }
        
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0 };
        this._mstGenDealerAppService.getDealerName().subscribe((result) => {
            this.dealers = result;
        });

        this.getOrganizationUnitTree();
    }

    getOrganizationUnitTree(){
        this._salesPersonServiceProxy.getGroupSalesPerson(this.default, true, this.dealerIdForTree).subscribe(
            (result) => {
                this.allOrganizationUnits = result.allOrganizationUnits;
                this.memberedOrganizationUnits = result.memberedOrganizationUnits;
            },
            () => { },
            () => {
                this.groupSalesPersonForView.data = <IGroupSalesPersonTreeComponentData>{
                    allOrganizationUnits: this.allOrganizationUnits,
                    selectedOrganizationUnits: this.memberedOrganizationUnits
                };
            }
        );
    }

    onNodeSelected(value) {
        this.nodeSelectId = value;
        this.groupIdFilter.push(Number(this.nodeSelectId.node.data.id));
        this.getSalesPersons();
        this.onGridReady(this.paginationParams);
    }

    onNodeUnSelected(value) {
        this.nodeSelectId = value;
        _.remove(this.groupIdFilter, x => x === this.nodeSelectId.node.data.id);
        this.getSalesPersons();
        this.onGridReady(this.paginationParams);
    }

    getSalesPersons() {
        this.loading = true;
        this._salesPersonServiceProxy.getAll(
            this.filterText,
            this.groupIdFilter,
            this.fullNameFilter,
            this.orderingFilter,
            this.positionFilter,
            this.abbreviationFilter,
            this.statusFilter,
            this.genderFilter,
            this.emailFilter,
            this.addressFilter,
            this.descriptionFilter,
            this.phoneFilter,
            this.dealerIdForTree,
            this.sorting ?? null,
            this.skipCount,
            this.maxResultCount
        )
        .pipe(finalize(() => this.loading = false))
        .subscribe(
            (result) => {
                this.isLoading = false;
                this.list = result.items;
                this.totalPages = ceil(result.totalCount / this.paginationParams.pageSize);
                this.paginationParams.totalCount = result.totalCount;
                this.paginationParams.totalPage = this.totalPages;
            }
        );
    }

    onGridReady(paginationParams) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;

        this.getSalesPersons();
    }

    onChangeSelection(params) {
        let selectedSalesPerson = params.api.getSelectedRows()[0];
        if (selectedSalesPerson) {
            this.selectSalesPersonId = selectedSalesPerson.id;
            this.selectedPersonUserId = selectedSalesPerson.userId
        }
    }

    callBackGrid(params) {
        this.params = params;
        this.params.api.sizeColumnsToFit();
        this.params.api.paginationSetPageSize(this.paginationParams.pageSize);
        this.onGridReady(this.paginationParams);

        this.selectSalesPersonId = null;
        this.selectedPersonUserId = null;
    }

    changePage(paginationParams: PaginationParamsModel) {
        this.paginationParams = paginationParams;
        this.paginationParams.skipCount = (paginationParams.pageNum - 1) * paginationParams.pageSize;
        this.maxResultCount = paginationParams.pageSize;

        this.getSalesPersons();
    }

    createMstSalesPersons(): void {
        this.createOrEditSalesPersonModal.show();
    }

    editMstSalesPersons(): void {
        this.createOrEditSalesPersonModal.show(this.selectSalesPersonId);
    }

    deleteMstSalesPersons(): void {
        this.message.confirm('', this.l('SalesPersonAreYouSure'), (isConfirmed) => {
            if (isConfirmed) {
                this._salesPersonServiceProxy
                    .delete(this.selectSalesPersonId)
                    .subscribe(() => {
                        this.onGridReady(this.paginationParams);
                        this.notify.success(this.l('SalesPersonSuccessfullyDeleted'));
                        this.createOrEditSalesPersonModal.getListUser();

                    });
            }
        });
    }

    onChangeFilterShown() {
        this.advancedFiltersAreShown = !this.advancedFiltersAreShown;
        if (!this.advancedFiltersAreShown) {
            this.clearValueFilter();
            this.clearGroupFilter();
        }
    }

    clearValueFilter(): void {
        this.filterText = "";
        this.fullNameFilter = "";
        this.genderFilter = "";
        this.emailFilter = "";
        this.addressFilter = "";
        this.abbreviationFilter = "";
        this.addressFilter = "";
        this.phoneFilter = "";
        this.statusFilter = "";
        this.positionFilter = "";
        this.descriptionFilter = "";
        this.orderingFilter = undefined;
    }

    clearGroupFilter(): void {
        this.groupIdFilter = [];
        this.groupSalesPersonForView.resetAllGroup();
    }

    search() {
        this.paginationParams = { pageNum: 1, pageSize: 20, totalCount: 0, };
        this.onGridReady(this.paginationParams);
    }

    eventEnter(event) {
        if (event.keyCode === 13) {
            this.search();
        }
    }
}
