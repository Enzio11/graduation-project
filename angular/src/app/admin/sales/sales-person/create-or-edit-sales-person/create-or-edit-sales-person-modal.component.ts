import { Component, ViewChild, Output, EventEmitter, Injector, Input } from '@angular/core';
import { SalePersonServiceProxy, GetSalesPersonForEditOutput, OrganizationUnitDto, CreateOrEditSalesPersonDto } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { DataFormatService } from '@app/shared/common/services/data-format.service';
import * as _ from 'lodash';
import { GroupSalesPersonComponent } from '../group-sales-person/group-sales-person.component';
import { IGroupSalesPersonTreeComponentData } from '../group-sales-person/group-sales-person-for-view.component';
import * as moment from 'moment';
import { AbpSessionService } from 'abp-ng2-module';
import { AppComponentBase } from '@shared/common/app-component-base';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'create-or-edit-sales-person-modal',
    templateUrl: './create-or-edit-sales-person-modal.component.html',
    styleUrls: ['./create-or-edit-sales-person-modal.component.less'],
})
export class CreateOrEditSalesPersonModalComponent extends AppComponentBase {
    @ViewChild('createOrEditModal', { static: true }) modal: ModalDirective;
    @ViewChild('groupSalesPerson') groupSalesPerson: GroupSalesPersonComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
    @Input() selectedId: number;

    loading: boolean = false;
    active: boolean = false;
    saving: boolean = false;
    salesPerson: GetSalesPersonForEditOutput = new GetSalesPersonForEditOutput();
    updateSalesPerson: CreateOrEditSalesPersonDto = new CreateOrEditSalesPersonDto();
    users = [];
    allOrganizationUnits: OrganizationUnitDto[];
    memberedOrganizationUnits: string[];
    disableSelection: boolean = false;
    listGroupCheckSave = [];
    birthday;
    dealerIdForTree: number;
    isTMV: boolean = false;

    constructor(
        injector: Injector,
        private _salesPersonServiceProxy: SalePersonServiceProxy,
        private dataFormatService: DataFormatService,
        private _sessionService: AbpSessionService,
    ) {
        super(injector);
    }

    ngOnInit() {
        var currentTenantId = this._sessionService.tenantId;
        if (currentTenantId == null) {
            this.dealerIdForTree = undefined;
            this.isTMV = true;
        }
        if (currentTenantId != null) {
            this.dealerIdForTree = currentTenantId;
            this.isTMV = false;
        }
    }

    refeshtUserId(){
        this.loading = true;
        this._salesPersonServiceProxy.getRefreshUser(this.updateSalesPerson.userId)
        .pipe(finalize(() => this.loading = false))
        .subscribe((result) => {
            result.forEach(element => {
                this.updateSalesPerson.fullName = element.fullName;
                this.updateSalesPerson.phone = element.phone;
                this.updateSalesPerson.email = element.email;
            });
        })
    }

    //Lấy list UserId
    getListUser() {
        this.loading = true;
        this.users = [];
        this._salesPersonServiceProxy.getUserAll(false)
        .pipe(finalize(() => this.loading = false))
        .subscribe((result) => {
            result.forEach(element => {
                var description = element.description ? ' - ' + element.description : '';
                var fullName = element.fullName ? element.fullName : '';
                var email = element.email ? ' - ' + element.email : '';
                var address = element.address ? ' - ' + element.address : '';

                this.users.push({
                    infor: element.id + " " + fullName + description + email + address,
                    id: element.id,
                    fullName: element.fullName,
                    email: element.email,
                    phone: element.phone
                });
            });
        });
    }

    getListUserForEdit() {
        this.loading = true;
        this.users = [];
        this._salesPersonServiceProxy.getUserAll(true)
        .pipe(finalize(() => this.loading = false))
        .subscribe((result) => {
            result.forEach(element => {
                var description = element.description ? ' - ' + element.description : '';
                var fullName = element.fullName ? element.fullName : '';
                var email = element.email ? ' - ' + element.email : '';
                var address = element.address ? ' - ' + element.address : '';

                this.users.push({
                    infor: element.id + " " + fullName + description + email + address,
                    id: element.id,
                    fullName: element.fullName,
                    email: element.email,
                    phone: element.phone
                });
            });
        });
    }

    show(salePersonId?: number): void {
        this.updateSalesPerson.userId = this.selectedId;
        
        if (!salePersonId) {
            this.getListUser();

            this.disableSelection = false;
            this.updateSalesPerson = new CreateOrEditSalesPersonDto();
            this.updateSalesPerson.id = salePersonId;

            this.salesPerson.id = salePersonId;
            this.active = true;
            
            this.modal.show();
        }
        else {
            this.getListUserForEdit();

            this.disableSelection = true;
            this.active = true;

            this._salesPersonServiceProxy.getSalesPersonForEdit(salePersonId).subscribe(result => {
                this.updateSalesPerson = result;

                if(this.updateSalesPerson.birthday != null) {
                    this.birthday = this.dataFormatService.dateFormatMonth(this.updateSalesPerson.birthday);
                }

                this.modal.show();
            });

            this._salesPersonServiceProxy.getGroupSalesPerson(this.selectedId, true, this.dealerIdForTree).subscribe(result => {
                this.allOrganizationUnits = result.allOrganizationUnits;
                this.memberedOrganizationUnits = result.memberedOrganizationUnits;

                this.updateSalesPerson.memberedOrganizationUnits = this.memberedOrganizationUnits;
            },
                () => { },
                () => {
                    // setTimeout(() => {
                    this.groupSalesPerson.data = <IGroupSalesPersonTreeComponentData>{
                        allOrganizationUnits: this.allOrganizationUnits,
                        selectedOrganizationUnits: this.memberedOrganizationUnits
                    };
                    // }, 0.0);
                }
            );
        }
    }

    onNodeSelected(value) {
        this.listGroupCheckSave.push(value.node.data.code);
    }

    unNodeSelected(value){
        _.remove(this.listGroupCheckSave, x => x === value.node.data.code);
    }

    save(): void {
        this.saving = true;

        this.updateSalesPerson.birthday = moment(this.birthday);
        if (!this.updateSalesPerson.birthday.isValid()) {
            this.saving = false;
            this.notify.warn(this.l("SalesPersonBirthdayFormatIsMonthDayYear"));
        }
        else{
            this.updateSalesPerson.organizationUnits = this.groupSalesPerson.getSelectedOrganizations();

            if (this.updateSalesPerson.isContractApprove == null) {
                this.updateSalesPerson.isContractApprove = false;
            }
            if (this.updateSalesPerson.isProposalApprove == null) {
                this.updateSalesPerson.isProposalApprove = false;
            }
            if (this.updateSalesPerson.isViewCustomer == null) {
                this.updateSalesPerson.isViewCustomer = false;
            }
            if (this.updateSalesPerson.isApproveTestDrive == null) {
                this.updateSalesPerson.isApproveTestDrive = false;
            }
            if (this.updateSalesPerson.isApproveLostFreeze == null) {
                this.updateSalesPerson.isApproveLostFreeze = false;
            }
            if (this.updateSalesPerson.isViewStock == null) {
                this.updateSalesPerson.isViewStock = false;
            }
            if (this.updateSalesPerson.isSeekforSupervisor == null) {
                this.updateSalesPerson.isSeekforSupervisor = false;
            }
            if (this.updateSalesPerson.isReceiveAssignmentList == null) {
                this.updateSalesPerson.isReceiveAssignmentList = false;
            }
            if (this.updateSalesPerson.isViewDardboard == null) {
                this.updateSalesPerson.isViewDardboard = false;
            }
            if (this.updateSalesPerson.isReceiveTestDriveList == null) {
                this.updateSalesPerson.isReceiveTestDriveList = false;
            }
            if (this.updateSalesPerson.isRepresent == null) {
                this.updateSalesPerson.isRepresent = false;
            }

            if (this.updateSalesPerson.isViewDuplicateCustomer == null) {
                this.updateSalesPerson.isViewDuplicateCustomer = false;
            }

            if (this.updateSalesPerson.isReviewFleet == null) {
                this.updateSalesPerson.isReviewFleet = false;
            }

            if (this.updateSalesPerson.isApproveFleet == null) {
                this.updateSalesPerson.isApproveFleet = false;
            }

            this._salesPersonServiceProxy.createOrEdit(this.updateSalesPerson)
                .pipe(finalize(() => { this.saving = false; }))
                .subscribe(() => {
                    this.notify.info(this.l('SalesPersonSavedSuccessfully'));
                    this.close();
                    this.modalSave.emit(null);
                });
        }

    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }

    // Chọn UserId
    getUserId() {
        var selectedUser = this.users.find((p) => p.id == Number(this.updateSalesPerson.userId));
        this.updateSalesPerson.fullName = selectedUser.fullName;
        this.updateSalesPerson.userId = selectedUser.id;
        this.updateSalesPerson.phone = selectedUser.phone;
        this.updateSalesPerson.email = selectedUser.email;
        this._salesPersonServiceProxy.getGroupSalesPerson(selectedUser.id, false, this.dealerIdForTree).subscribe(result => {
            this.allOrganizationUnits = result.allOrganizationUnits;
            this.memberedOrganizationUnits = result.memberedOrganizationUnits;
            this.updateSalesPerson.memberedOrganizationUnits = this.memberedOrganizationUnits;
            this.listGroupCheckSave = this.updateSalesPerson.memberedOrganizationUnits;
        },
            () => { },
            () => {
                // setTimeout(() => {
                this.groupSalesPerson.data = <IGroupSalesPersonTreeComponentData>{
                    allOrganizationUnits: this.allOrganizationUnits,
                    selectedOrganizationUnits: this.memberedOrganizationUnits
                };
                // }, 0.0);
            }
        );
    }
}

