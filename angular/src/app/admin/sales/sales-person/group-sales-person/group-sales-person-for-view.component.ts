import { Component, EventEmitter, Injector, Input, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { OrganizationUnitDto } from '@shared/service-proxies/service-proxies';
import { ArrayToTreeConverterService } from '@shared/utils/array-to-tree-converter.service';
import { TreeDataHelperService } from '@shared/utils/tree-data-helper.service';
import { TreeNode } from 'primeng/api';
import * as _ from 'lodash';
import { Paginator } from 'primeng/paginator';

export interface IGroupSalesPersonTreeComponentData {
    allOrganizationUnits: OrganizationUnitDto[];
    selectedOrganizationUnits: string[];
}

@Component({
    selector: 'group-unit-tree-for-view',
    templateUrl: './group-sales-person-for-view.component.html',
    styleUrls: ['./group-sales-person.component.less'],
})

export class GroupSalesPersonForViewComponent extends AppComponentBase {
    @Input() cascadeSelectEnabled: boolean = true;

    @Output() nodeSelected = new EventEmitter();
    @Output() nodeUnSelected = new EventEmitter();

    @ViewChild('paginator', { static: true }) paginator: Paginator;

    private _allOrganizationUnits: OrganizationUnitDto[];
    public _selectedOrganizationUnits: string[];

    set data(data: IGroupSalesPersonTreeComponentData) {
        this.setTreeData(data.allOrganizationUnits);
        this.setSelectedNodes(data.selectedOrganizationUnits);

        this._allOrganizationUnits = data.allOrganizationUnits;
        this._selectedOrganizationUnits = data.selectedOrganizationUnits;
    }

    treeData: TreeNode[];
    selectedOus: TreeNode[] = [];

    constructor(
        private _arrayToTreeConverterService: ArrayToTreeConverterService,
        private _treeDataHelperService: TreeDataHelperService,
        injector: Injector
    ) {
        super(injector);
    }

    setTreeData(organizationUnits: OrganizationUnitDto[]) {
        this.treeData = this._arrayToTreeConverterService.createTree(
            organizationUnits, 'parentId', 'id', null, 'children',
            [
                {
                    target: 'label',
                    source: 'displayName'
                }, 
                {
                    target: 'expandedIcon',
                    value: 'fa fa-folder-open m--font-warning'
                },
                {
                    target: 'collapsedIcon',
                    value: 'fa fa-folder m--font-warning'
                },
                {
                    target: 'expanded',
                    value: true
                }
            ]
        );
    }

    setSelectedNodes(selectedOrganizationUnits: string[]) {
        this.selectedOus = [];
        _.forEach(selectedOrganizationUnits, ou => {
            let item = this._treeDataHelperService.findNode(this.treeData, { data: { code: ou } });
            if (item) {
                this.selectedOus.push(item);
            }
        });
    }

    getSelectedOrganizations(): number[] {
        if (!this.selectedOus) {
            return [];
        }

        let organizationIds = [];

        _.forEach(this.selectedOus, ou => {
            organizationIds.push(ou.data.id);
        });

        return organizationIds;
    }

    showParentNodes(node): void {
        if (!node.parent) {
            return;
        }

        node.parent.styleClass = '';
        this.showParentNodes(node.parent);
    }

    onNodeSelect(event) {
        this.nodeSelected.emit(event);
        if (!this.cascadeSelectEnabled) {
            return;
        }

        let parentNode = this._treeDataHelperService.findParent(this.treeData, { data: { id: event.node.data.id } });

        while (parentNode != null) {
            this.selectedOus.push(parentNode);
            parentNode = this._treeDataHelperService.findParent(this.treeData, { data: { id: parentNode.data.id } });
        }
    }

    onNodeUnselect(event) {
        this.nodeUnSelected.emit(event);
        let childrenNodes = this._treeDataHelperService.findChildren(this.treeData, { data: { name: event.node.data.name } });
        childrenNodes.push(event.node.data.name);
    }

    resetAllGroup(): void{
        this.selectedOus = [];
    }
}
