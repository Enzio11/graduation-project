import { NgModule } from "@angular/core";
import { SalesPersonComponent } from './sales-person.component';
import { CreateOrEditSalesPersonModalComponent } from "./create-or-edit-sales-person/create-or-edit-sales-person-modal.component";
import { GroupSalesPersonForViewComponent } from "./group-sales-person/group-sales-person-for-view.component";
import { GroupSalesPersonComponent } from "./group-sales-person/group-sales-person.component";
import { CommonDeclareModule } from "@app/shared/common-declare.module";
import { TABS } from "@app/shared/constants/tab-keys";

const tabcode_component_dict = {
    [TABS.SALES_PERSON]: SalesPersonComponent
  };
@NgModule({
    imports: [
        CommonDeclareModule,
    ],
    declarations: [
        SalesPersonComponent,
        CreateOrEditSalesPersonModalComponent,
        GroupSalesPersonComponent,
        GroupSalesPersonForViewComponent
    ],
})

export class SalesPersonModule{
    static getComponent(tabCode: string) {
        return tabcode_component_dict[tabCode];
      }
}
