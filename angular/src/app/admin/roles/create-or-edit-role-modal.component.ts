import { Component, ElementRef, EventEmitter, Injector, Output, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { CreateOrUpdateRoleInput, RoleEditDto, RoleServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PermissionTreeComponent } from '../shared/permission-tree.component';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'createOrEditRoleModal',
    templateUrl: './create-or-edit-role-modal.component.html'
})
export class CreateOrEditRoleModalComponent extends AppComponentBase {

    @ViewChild('createOrEditModal', {static: true}) modal: ModalDirective;
    @ViewChild('permissionTree') permissionTree: PermissionTreeComponent;

    @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();

    active = false;
    saving = false;

    role: RoleEditDto = new RoleEditDto();
    constructor(
        injector: Injector,
        private _roleService: RoleServiceProxy
    ) {
        super(injector);
    }

    // sắp xếp lại theo STT
    split(params) {
        return params.split(" ")[0].split(".").map(e => parseInt(e));
    }
    compare(item1, item2) {
        var newItem1 = this.split(item1.displayName), newItem2 = this.split(item2.displayName), x;
        for (var i = 0; i < newItem1.length; i++) {
            x = newItem1[i] - newItem2[i];
            if (x > 0) return 2;
            if (x < 0) return 1;
        }
    }
    sort(object) {
        var tg;
        for (var i = 0; i < object.permissions.length - 1; i++) {
            for (var j = i + 1; j < object.permissions.length; j++) {
                var x = this.compare(object.permissions[i], object.permissions[j]);
                if (x == 2) {
                    tg = object.permissions[i];
                    object.permissions[i] = object.permissions[j];
                    object.permissions[j] = tg;
                }
            }
        }
        return object;
    }
    // End sắp xếp lại theo STT
    show(roleId?: number): void {
        const self = this;
        self.active = true;

        self._roleService.getRoleForEdit(roleId).subscribe(result => {
            self.role = result.role;
            this.permissionTree.editData = this.sort(result);

            self.modal.show();
        });
    }

    onShown(): void {
        document.getElementById('RoleDisplayName').focus();
    }

    save(): void {
        const self = this;

        const input = new CreateOrUpdateRoleInput();
        input.role = self.role;
        input.grantedPermissionNames = self.permissionTree.getGrantedPermissionNames();

        this.saving = true;
        this._roleService.createOrUpdateRole(input)
            .pipe(finalize(() => this.saving = false))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
                this.modalSave.emit(null);
            });
    }

    close(): void {
        this.active = false;
        this.modal.hide();
    }
}
