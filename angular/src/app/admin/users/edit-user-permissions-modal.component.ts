import { Component, Injector, ViewChild } from '@angular/core';
import { AppComponentBase } from '@shared/common/app-component-base';
import { EntityDtoOfInt64, UpdateUserPermissionsInput, UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PermissionTreeComponent } from '../shared/permission-tree.component';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'editUserPermissionsModal',
    templateUrl: './edit-user-permissions-modal.component.html'
})
export class EditUserPermissionsModalComponent extends AppComponentBase {

    @ViewChild('editModal', { static: true }) modal: ModalDirective;
    @ViewChild('permissionTree') permissionTree: PermissionTreeComponent;

    saving = false;
    resettingPermissions = false;

    userId: number;
    userName: string;

    constructor(
        injector: Injector,
        private _userService: UserServiceProxy
    ) {
        super(injector);
    }
    // sắp xếp lại theo STT
    split(params) {
        return params.split(" ")[0].split(".").map(e => parseInt(e));
    }
    compare(item1, item2) {
        var newItem1 = this.split(item1.displayName), newItem2 = this.split(item2.displayName), x;
        for (var i = 0; i < newItem1.length; i++) {
            x = newItem1[i] - newItem2[i];
            if (x > 0) return 2;
            if (x < 0) return 1;
        }
    }
    sort(object) {
        var tg;
        for (var i = 0; i < object.permissions.length - 1; i++) {
            for (var j = i + 1; j < object.permissions.length; j++) {
                var x = this.compare(object.permissions[i], object.permissions[j]);
                if (x == 2) {
                    tg = object.permissions[i];
                    object.permissions[i] = object.permissions[j];
                    object.permissions[j] = tg;
                }
            }
        }
        return object;
    }
    // End sắp xếp lại theo STT
    show(userId: number, userName?: string): void {
        this.userId = userId;
        this.userName = userName;

        this._userService.getUserPermissionsForEdit(userId).subscribe(result => {
            this.permissionTree.editData = this.sort(result);
            this.modal.show();
        });
    }

    save(): void {
        let input = new UpdateUserPermissionsInput();

        input.id = this.userId;
        input.grantedPermissionNames = this.permissionTree.getGrantedPermissionNames();

        this.saving = true;
        this._userService.updateUserPermissions(input)
            .pipe(finalize(() => { this.saving = false; }))
            .subscribe(() => {
                this.notify.info(this.l('SavedSuccessfully'));
                this.close();
            });
    }

    resetPermissions(): void {

        let input = new EntityDtoOfInt64();

        input.id = this.userId;

        this.resettingPermissions = true;
        this._userService.resetUserSpecificPermissions(input).subscribe(() => {
            this.notify.info(this.l('ResetSuccessfully'));
            this._userService.getUserPermissionsForEdit(this.userId).subscribe(result => {
                this.permissionTree.editData = result;
            });
        }, undefined, () => {
            this.resettingPermissions = false;
        });
    }

    close(): void {
        this.modal.hide();
    }
}
