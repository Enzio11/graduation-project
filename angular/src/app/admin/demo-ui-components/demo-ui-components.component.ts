import { HttpClient } from '@angular/common/http';
import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/common/app-component-base';
import { saveAs } from 'file-saver';

@Component({
    templateUrl: './demo-ui-components.component.html',
    animations: [appModuleAnimation()]
})
export class DemoUiComponentsComponent extends AppComponentBase {
    constructor(
        injector: Injector,
        private _http: HttpClient
    ) {
        super(injector);
    }
    selectedFormat = 'pdf';

    onChange($event) {
        this.selectedFormat = $event.target.value;
    }
    downloadFile() {
        this._http.get('https://localhost:44381/api/Demo/Export', {
            params: { "format": this.selectedFormat }, responseType: 'blob'
        }).subscribe(blob => {
            saveAs(blob, 'DemoReport.' + this.selectedFormat.toLowerCase());
        });
    }
}

