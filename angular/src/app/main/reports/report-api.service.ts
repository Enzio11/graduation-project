import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Injector } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { AppComponentBase } from '@shared/common/app-component-base';

@Injectable()
export class ReportApiService extends AppComponentBase {
  private http: HttpClient;
  urlBase: string = AppConsts.remoteServiceBaseUrl;
  constructor(@Inject(HttpClient) http: HttpClient,
    injector: Injector) {
    super(injector);
    this.http = http;
  }


  // function call api controller report
}
