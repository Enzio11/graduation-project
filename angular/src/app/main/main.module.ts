﻿import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {AppCommonModule} from '@app/shared/common/app-common.module';

import {AutoCompleteModule} from 'primeng/autocomplete';
import {PaginatorModule} from 'primeng/paginator';
import {EditorModule} from 'primeng/editor';
import {InputMaskModule} from 'primeng/inputmask';
import {FileUploadModule} from 'primeng/fileupload';
import {TableModule} from 'primeng/table';

import {UtilsModule} from '@shared/utils/utils.module';
import {CountoModule} from 'angular2-counto';
import {ModalModule} from 'ngx-bootstrap/modal';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {MainRoutingModule} from './main-routing.module';
import {NgxChartsModule} from '@swimlane/ngx-charts';

import {BsDatepickerConfig, BsDaterangepickerConfig, BsLocaleService} from 'ngx-bootstrap/datepicker';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {NgxBootstrapDatePickerConfigService} from 'assets/ngx-bootstrap/ngx-bootstrap-datepicker-config.service';
import { HomeComponent } from '@app/shared/components/home/home.component';
import { KeyboardShortcutsModule } from 'ng-keyboard-shortcuts';
import { DynamicTabComponent } from '@app/shared/components/dynamic-tab/dynamic-tab.component';
import { TabDisplayDirective } from '@app/shared/components/tab-display/tab-display.directive';

NgxBootstrapDatePickerConfigService.registerNgxBootstrapDatePickerLocales();

@NgModule({
    imports: [
        FileUploadModule,
        AutoCompleteModule,
        PaginatorModule,
        EditorModule,
        InputMaskModule, TableModule,
        KeyboardShortcutsModule.forRoot(),
        CommonModule,
        FormsModule,
        ModalModule,
        TabsModule,
        TooltipModule,
        AppCommonModule,
        UtilsModule,
        MainRoutingModule,
        CountoModule,
        NgxChartsModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        PopoverModule.forRoot(),
    ],
    exports: [
        KeyboardShortcutsModule
    ],
    declarations: [
        HomeComponent,
        DynamicTabComponent,
        TabDisplayDirective
    ],
    providers: [
        {provide: BsDatepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerConfig},
        {provide: BsDaterangepickerConfig, useFactory: NgxBootstrapDatePickerConfigService.getDaterangepickerConfig},
        {provide: BsLocaleService, useFactory: NgxBootstrapDatePickerConfigService.getDatepickerLocale}
    ]
})
export class MainModule {
}
